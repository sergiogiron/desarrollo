$(document).ready(function(){
	$( ".publicationrecord" ).click(function(event){
		event.preventDefault();
		openLoading();
		var request = $.ajax({
			url: $(this).attr('href'),
		}).done(function(msg) {
			closeLoading();
			$('.ligth-box-modal').css('display','block');
			$('.modal-title').html('Historial de publicaciones');
			$('.modal-body').html(msg).promise().done(function(){
				$('#datetable').DataTable({
					"order": [[ 1, "desc" ]],
					"bLengthChange": false,
					"bInfo": false,
					"bAutoWidth": false,
					"iDisplayLength": 6,
					"bPaginate":true,
					"bFilter": false,
					"bInfo": false,		
					"language": {
						"sSearch":"Buscar :",
						"paginate": {
						  "previous": "Anterior",
						  "next": "Siguiente"
						}
					}
				});
			});
		});	
	});	
});	
