function delete_element(element){
	element.parent().remove();
	$('#'+element.attr('data-id')).remove();
	position=$(element).attr('data-position');
	eval("v_slider_"+position).reloadSlider();
	if($('.bslide_'+position).length==0){
		eval("v_slider_"+position).destroySlider();	
	}
}
function edit_element(element){
	$('.ligth-box-modal').css('display','none');	
	$('.ligth-box-modal-images').css('display','block');	
	$('.ligth-box-modal-images').append('<img src="'+homeurl+'/img/ajax_loader.gif"	 class="loader_gif_img">');
	position=$(element).attr('data-position');
	$.ajax({
	  method: "POST",
	  data:JSON.parse($('#'+element.attr('data-id')).val()),
	  url: homeurl+"/configimages/uploadedit/id_menu/"+id_menu+"/menu/"+menu+"/position/"+position,
	}).done(function( msg ) {	
		$('.ligth-box-modal').css('display','none');	
		$('#content_result_img').html(msg);
		$('.loader_gif_img').remove();
		$('#modal-dialog-images').css('display','block');
		$('#modal-content-images').css('display','block');
		$('#image_jcrop').Jcrop({
		},function() {
			$('#first_selected').trigger('click');
		});	
	});				
}
$( document ).ready(function() {
	$( ".more_img" ).each(function( index, element ) {
		$('.close').css('visibility','visible');
		position=$(element).attr('data-position');
		$( "#slider_"+position ).sortable({
			update: function( event, ui ) {	
				$('.slide').each(function( index, element ) {
					var id=$(this).find('i').attr('data-id');
					var json=JSON.parse($('#'+id).val());				
					json.order=index;
					$('#'+id).val(JSON.stringify(json));
				});
				window['v_slider_' + position].destroySlider();
				window['v_slider_' + position] = $( "#slider_"+position ).bxSlider({
					slideWidth: 250,
					minSlides: 4,
					maxSlides: 4,
					infiniteLoop: false,
					slideMargin:9
				});			  
			}
		});
		if(eval("cantidad_images_"+position)>0){
			window['v_slider_' + position]= $( "#slider_"+position ).bxSlider({
				slideWidth: 250,
				minSlides: 4,
				maxSlides: 4,
				infiniteLoop: false,
				slideMargin:8
			});				
		}
	});
	$( ".action_img" ).click(function() {
		$('.close').css('visibility','visible');
		position=$(this).attr('data-position');
		var ok=0;
		if($('#data-d-content_'+position+' :input').length<eval("limit_images_"+position)){
			ok=1;
		}else{
			ok=0;
		}
		if(ok==1){
			$('.modal-dialog').css('display','none');
			$('.ligth-box-modal-images').css('display','block');
			$('.ligth-box-modal-images').append('<img src="'+homeurl+'/img/ajax_loader.gif"	 class="loader_gif_img">');				
			$.ajax({
			  method: "POST",
			  url: homeurl+"/configimages/upload/id_menu/"+id_menu+"/position/"+position,
			}).done(function( msg ) {		
				$('#content_result_img').html(msg);
				$('.loader_gif_img').remove();
				$('.modal-dialog').css('display','block');				
			 }).fail(function() {
				 $('.loader_gif_img').remove();
				 $('.ligth-box-modal-images').css('display','none');	
				sweetAlert("Necesita permiso para realizar esta acción", "Consulte con un administrador", "error");
			 });				
		}else{
			sweetAlert("No se pudo cargar la imagen", "Alcanzó el máximo de cargas permitidas", "error");
		}
	});	
});		