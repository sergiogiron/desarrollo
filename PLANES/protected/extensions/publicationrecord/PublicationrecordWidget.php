<?php

class PublicationrecordWidget extends CWidget
{
	public $controller_custom;
	
	public function run(){
		if($this->controller_custom!=''){
			$controller=$this->controller_custom;
		}else{
			$controller=Yii::app()->controller->id;
		}
		Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/publicationrecord.js',CClientScript::POS_END); 			
		$name=Yii::app()->controller->id;
		$model=new $controller;
		$table_name=$model->tableName();
		$last_publication=Publications::model()->findByAttributes(array('model'=>$table_name),array('order'=> 'created_at desc'));
		if(count($last_publication)>0){
			echo '<span class="lastPublication">El '.Utils::datetime_spa($last_publication->created_at).' '.ucfirst($last_publication->user->name).' '.ucfirst($last_publication->user->lastname).' publicó para el '.Utils::datetime_spa($last_publication->publication_date).'. <a href="'.Yii::app()->request->baseUrl.'/publications/publicationrecord/table/'.$table_name.'" class="publicationrecord">Ver historial completo</a></span>';				
		}	else { echo '<span class="last_p">Estos datos nunca han sido publicados.</span>';}		
	}	
}
