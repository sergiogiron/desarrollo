<?php

/**
 * HighmapsWidget class file.
 */
require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'HighchartsWidget.php');

/**
 * @see HighchartsWidget
 *
 * En el posible caso en que se necesite usar
 */
class HighmapsWidget extends HighchartsWidget
{

    protected $_constr = 'Map';
    protected $_baseScript = 'highcharts';

    /**
     * Highmaps debe ser usado como un modulo si el script de highcharts esta en
     * la misma pagina. Como no sabemos si va a ser incluido alguno en el futuro,
	 * asumimos siempre a highmaps como un modulo.
     */
    public function run()
    {
        array_unshift($this->scripts, 'modules/map');
        parent::run();
    }
}

