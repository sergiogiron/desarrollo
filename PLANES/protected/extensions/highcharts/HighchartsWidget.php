<?php

/**
 * HighchartsWidget class file.
 */

/**
 * HighchartsWidget encapsula el Objeto {@link http://www.highcharts.com/ Highcharts} 
 * de la libreria de HighCharts.
 *
 * Para usar el widget, se debe insertar el sig codigo en la vista:
 * <pre>
 * $this->Widget('ext.highcharts.HighchartsWidget', array(
 *    'options'=>array(
 *       'title' => array('text' => 'Titulo del grafico'),
 *       'xAxis' => array(
 *          'categories' => array('Col_1', 'Col_2', 'Col_3')
 *       ),
 *       'yAxis' => array(
 *          'title' => array('text' => 'Valores eje Y')
 *       ),
 *       'series' => array(
 *          array('name' => 'Usuario_1', 'data' => array(1, 0, 4)),
 *          array('name' => 'Usuario_2', 'data' => array(5, 7, 3))
 *       )
 *    )
 * ));
 * </pre>
 *
 * Configurando {@link $options}, se pueden especificar las opciones
 * que necesitan ser pasadas al objeto de js. Referencias
 * en la documentacion {link http://http://api.highcharts.com/highcharts doc}
 *
 * Aparte, se puede usar un JSON valido en lugar de una arreglo
 * asociativo para especificar las opciones:
 *
 * <pre>
 * $this->Widget('ext.highcharts.HighchartsWidget', array(
 *    'options'=>'{
 *       "title": { "text": "Titulo del grafico" },
 *       "xAxis": {
 *          "categories": ["Col_1", "Col_2", "Col_3"]
 *       },
 *       "yAxis": {
 *          "title": { "text": "Valores eje Y" }
 *       },
 *       "series": [
 *          { "name": "Usuario_1", "data": [1, 0, 4] },
 *          { "name": "Usuario_2", "data": [5, 7,3] }
 *       ]
 *    }'
 * ));
 * </pre>
 *
 * Nota: Se debe proveer un JSON valido (Vease comillas dobles: "")
 * {@link http://jsonlint.com/ JSONLint}.
 *
 * Nota: No se necesita especificar la opcion <code>chart->renderTo</code> como
 * se muestra en muchos ejemplos de la doc del sitio de Highcharts. Este valor es completado
 * automaticamente con el id del contenedor del widget.
 */
class HighchartsWidget extends CWidget
{
    protected $_constr = 'Chart';
    protected $_baseScript = 'highcharts';
    public $options = array();
    public $htmlOptions = array();
    public $setupOptions = array();
    public $scripts = array();
    public $callback = false;
    public $scriptPosition = null;

    /**
     * Renders
     */
    public function run()
    {
        if (isset($this->htmlOptions['id'])) {
            $this->id = $this->htmlOptions['id'];
        } else {
            $this->htmlOptions['id'] = $this->getId();
        }

        echo CHtml::openTag('div', $this->htmlOptions);
        echo CHtml::closeTag('div');

        // check si el parametro options es un json string valido
        if (is_string($this->options)) {
            if (!$this->options = CJSON::decode($this->options)) {
                throw new CException('The options parameter is not valid JSON.');
            }
        }

        // merge las opciones con los valores default
        $defaultOptions = array('chart' => array('renderTo' => $this->id));
        $this->options = CMap::mergeArray($defaultOptions, $this->options);
        array_unshift($this->scripts, $this->_baseScript);

        $this->registerAssets();
    }

    /**
     * publica y registra los scripts necesarios.
     */
    protected function registerAssets()
    {
        $basePath = dirname(__FILE__) . DIRECTORY_SEPARATOR . 'assets' . DIRECTORY_SEPARATOR;
        $baseUrl = Yii::app()->getAssetManager()->publish($basePath, false, 1, YII_DEBUG);

        $cs = Yii::app()->clientScript;
        $cs->registerCoreScript('jquery');

        // registra scripts adicionales
        $extension = YII_DEBUG ? '.src.js' : '.js';
        foreach ($this->scripts as $script) {
            $cs->registerScriptFile("{$baseUrl}/{$script}{$extension}", $this->scriptPosition);
        }

        // prepara y registra el bloque de JavaScript
        $jsOptions = CJavaScript::encode($this->options);
        $setupOptions = CJavaScript::encode($this->setupOptions);
        $js = "Highcharts.setOptions($setupOptions); var chart = new Highcharts.{$this->_constr}($jsOptions);";
        $key = __CLASS__ . '#' . $this->id;
        if (is_string($this->callback)) {
            $callbackScript = "function {$this->callback}(data) {{$js}}";
            $cs->registerScript($key, $callbackScript, CClientScript::POS_END);
        } else {
            $cs->registerScript($key, $js, CClientScript::POS_LOAD);
        }
    }
}
