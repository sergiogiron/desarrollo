<?php
/* @var $this ProfilewidgetsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Profilewidgets',
);

$this->menu=array(
	array('label'=>'Create Profilewidgets', 'url'=>array('create')),
	array('label'=>'Manage Profilewidgets', 'url'=>array('admin')),
);
?>

<h1>Profilewidgets</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
