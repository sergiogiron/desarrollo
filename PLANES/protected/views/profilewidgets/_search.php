<?php
/* @var $this ProfilewidgetsController */
/* @var $model Profilewidgets */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>
<div class="col-md-12" style="clear: both">
	<div class="row">
		<div class="col-md-3">
			<div class="row row_space">
		<?php echo $form->label($model,'title'); ?>
		<?php echo $form->textField($model,'title',array('size'=>20,'maxlength'=>20,'class'=>'form-control')); ?>
			</div>
		</div>

		<div class="col-md-3">
			<div class="row row_space">
		<?php echo $form->label($model,'description'); ?>
		<?php echo $form->textField($model,'description',array('size'=>60,'maxlength'=>60,'class'=>'form-control')); ?>
			</div>
		</div>

		<div class="col-md-3">
			<div class="row row_space">
		<?php echo $form->label($model,'url'); ?>
		<?php echo $form->textField($model,'url',array('size'=>60,'maxlength'=>128,'class'=>'form-control')); ?>
			</div>
		</div>
	</div>
	<div class="row buttons">
		<?php echo CHtml::submitButton('Buscar',array('class'=>'btn btn-default b_customize')); ?>
	</div>
<br />
<?php $this->endWidget(); ?>
</div>
</div><!-- search-form -->