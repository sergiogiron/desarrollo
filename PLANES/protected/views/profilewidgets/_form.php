<?php
/* @var $this ProfilewidgetsController */
/* @var $model Profilewidgets */
/* @var $form CActiveForm */
?>

<div class="box-body">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'profilewidgets-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<span class="right_f active-button">
		<?php echo $form->checkBox($model,'enabled',array('class'=>'lcs_check enabled')); ?>
	</span>
	<br>
	<div class="col-md-12 col-data">
	<div class="col-xs-6 form-group required">
		<?php echo $form->labelEx($model,'title'); ?>
		<?php echo $form->textField($model,'title',array('size'=>20,'maxlength'=>20,'class'=>'form-control')); ?>
		<?php echo $form->error($model,'title'); ?>
	</div>

	<div class="col-xs-6 form-group required">
		<?php echo $form->labelEx($model,'description'); ?>
		<?php echo $form->textField($model,'description',array('size'=>60,'maxlength'=>60,'class'=>'form-control')); ?>
		<?php echo $form->error($model,'description'); ?>
	</div>

	<div class="form-group col-md-6 required">
			<?php echo $form->labelEx($model,'url'); ?>
			<div class="input-group">		
				<?php echo $form->textField($model,'url',array('size'=>260,'maxlength'=>260,'placeholder'=>'http://www.ttsoffice.com','class'=>'form-control')); ?>
				<div class="input-group-addon"><i class="fa fa-globe"></i></div>						
			</div>
			<?php echo $form->error($model,'url'); ?>						
		</div>
	
	<div class="col-xs-6 form-group required">
		<?php echo $form->labelEx($model,'size'); ?>
		<?php 
		$size=array(array('id'=>3,'title'=>'pequeño'),array('id'=>6,'title'=>'mediano'),array('id'=>9,'title'=>'grande'),array('id'=>12,'title'=>'full'));
		echo $form->dropDownList($model,'size',CHtml::listData($size, 'id', 'title'),array('empty' => 'Seleccione','class'=>'form-control')); ?>
		<?php echo $form->error($model,'size'); ?>
	</div>
	<div class="box-footer">
		<?php
			$title = (Yii::app()->controller->action->id!='view') ? 'Volver sin guardar cambios' : 'Volver';
			echo '<a href="'.Yii::app()->request->baseUrl.'/'.strtolower(Yii::app()->controller->id).'" class="btn btn-secundary" data-toggle="tooltip" title="'.$title.'" >Cerrar</a>';
			if(Yii::app()->controller->action->id!='view'){
				echo CHtml::submitButton('Guardar',array('class'=>'btn btn-primary','data-toggle'=>'tooltip','title'=>'Guardar cambios'));
			}
		?>
	</div>
	</div>
	</div>
<?php $this->endWidget(); ?>

</div><!-- form -->
<?php
	// Activo/Inactivo
	Yii::app()->clientScript->registerCssFile(Yii::app()->params['cdnUrl'].'/plugins/lcswich/lc_switch-tts.css'); 
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/lcswich/lc_switch.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/lcswich/lc_switch-tts.js',CClientScript::POS_END);
	?>
<script type="text/javascript">
	<?php
		if(Yii::app()->controller->action->id=='view'){ echo "$('form').find('input, textarea, checkbox, select').attr('disabled','disabled');"; }
	?>	
</script>