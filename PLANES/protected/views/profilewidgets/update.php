<?php
/* @var $this ProfilemenuController */
/* @var $model Profilemenu */

$this->breadcrumbs=array(
	'Profilemenus'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Profilemenu', 'url'=>array('index')),
	array('label'=>'Manage Profilemenu', 'url'=>array('admin')),
);
?>
<section class="content-header">
	<?php echo Yii::app()->user->Title(); ?>
	<?php echo Yii::app()->user->Breadcrumb(); ?>
</section>
<section class="content">
	<div class="box box-danger">
		<div class="box-header with-border">
			<?php $this->renderPartial('_form', array('model'=>$model)); ?>
		</div>
	</div>
</section>