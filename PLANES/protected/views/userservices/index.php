<?php
/* @var $this UserServicesController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'User Services',
);

$this->menu=array(
	array('label'=>'Create UserServices', 'url'=>array('create')),
	array('label'=>'Manage UserServices', 'url'=>array('admin')),
);
?>

<h1>User Services</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
