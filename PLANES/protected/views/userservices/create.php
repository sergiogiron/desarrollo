<?php
/* @var $this UserServicesController */
/* @var $model UserServices */

$this->breadcrumbs=array(
	'User Services'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List UserServices', 'url'=>array('index')),
	array('label'=>'Manage UserServices', 'url'=>array('admin')),
);
?>

<h1>Create UserServices</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>