<?php
/* @var $this UserServicesController */
/* @var $model UserServices */

$this->breadcrumbs=array(
	'User Services'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List UserServices', 'url'=>array('index')),
	array('label'=>'Create UserServices', 'url'=>array('create')),
	array('label'=>'View UserServices', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage UserServices', 'url'=>array('admin')),
);
?>

<h1>Update UserServices <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>