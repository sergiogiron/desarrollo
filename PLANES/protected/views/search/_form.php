<?php
/* @var $this SearchController */
/* @var $model Search */
/* @var $form CActiveForm */
?>

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'search-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>
<br />
<div class="col-md-4 form-group required  unique">
	<?php echo $form->labelEx($model,'id_channel'); ?>
	<?php 
	if(!$model->isNewRecord){ $disable='disabled'; }else{ $disable='enabled'; }	
	echo $form->dropDownList($model, 'id_channel',CHtml::listData(Channels::model()->findAll(), 'id', 'title'),array($disable=>$disable,'empty' => 'Seleccione','class'=>'form-control'));?>
	<?php echo $form->error($model,'id_channel'); ?>
</div>

<div class="col-md-4 form-group required  unique">
	<?php echo $form->labelEx($model,'id_subchannel'); ?>
	<?php echo $form->dropDownList($model, 'id_subchannel',CHtml::listData(Subchannels::model()->findAll(), 'id', 'title'),array($disable=>$disable,'empty' => 'Seleccione','class'=>'form-control'));?>
	<?php echo $form->error($model,'id_subchannel'); ?>
</div>

<div class="col-md-4 form-group required  unique">
	<?php echo $form->labelEx($model,'id_product'); ?>
	<?php echo $form->dropDownList($model, 'id_product',CHtml::listData(Products::model()->findAll(), 'id', 'title'),array($disable=>$disable,'empty' => 'Seleccione','class'=>'form-control'));?>
	<?php echo $form->error($model,'id_product'); ?>
</div>

	<div class="col-md-4 form-group">
		<?php echo $form->labelEx($model,'phone'); ?>
		<div class="input-group">
			<?php 
			if(isset($model_subchannel->phone)){
				$telefono_precargado=$model_subchannel->phone;
			}else{
				$telefono_precargado='';
			}		
			echo $form->textField($model,'phone',array('size'=>35,'maxlength'=>35,'class'=>'form-control phone','placeholder'=>$telefono_precargado)); ?>
			<div class="input-group-addon"><i class="fa fa-phone"></i></div>
		</div>
		<div class="errorMessage"><?php if(isset($errors['phone'])){ echo $errors['phone']; } ?></div>
	</div>	
	
	<div class="col-md-4 form-group">
		<?php 
		if(isset($model_subchannel->phone_description)){
			$phone_precargado=$model_subchannel->phone_description;
		}else{
			$phone_precargado='';
		}				
		echo $form->labelEx($model,'phone_description'); 
		?>
		<?php echo $form->textField($model,'phone_description',array('size'=>64,'maxlength'=>64,'class'=>'form-control letras','placeholder'=>$phone_precargado)); ?>
		<div class="errorMessage"><?php if(isset($errors['phone_description'])){ echo $errors['phone_description']; } ?></div>
	</div>	
	<div class="col-md-4 form-group">
		<?php echo $form->labelEx($model,'office_hours'); ?>
		<?php
		if(isset($model_subchannel->office_hours)){
			$office_hours=$model_subchannel->office_hours;
		}else{
			$office_hours='';
		}				
		echo $form->textField($model,'office_hours',array('size'=>64,'maxlength'=>64,'class'=>'form-control','placeholder'=>$office_hours)); ?>
		<?php echo $form->error($model,'office_hours'); ?>
	</div>	
	<div class="col-md-4 form-group required">
		<?php echo $form->labelEx($model,'anticipation_from_min_days'); ?>*
		<?php echo $form->textField($model,'anticipation_from_min_days',array('class'=>'form-control numeros number_input')); ?>
		<div class="errorMessage"><?php if(isset($errors['anticipation_from_min_days'])){ echo $errors['anticipation_from_min_days']; } ?></div>
	</div>	
	<div class="col-md-4 form-group required">
		<?php echo $form->labelEx($model,'anticipation_to_max_days'); ?>*
		<?php echo $form->textField($model,'anticipation_to_max_days',array('class'=>'form-control numeros number_input')); ?>
		<div class="errorMessage"><?php if(isset($errors['anticipation_to_max_days'])){ echo $errors['anticipation_to_max_days']; } ?></div>
	</div>	
	<div class="col-md-4 form-group required">
		<?php echo $form->labelEx($model,'max_search_months'); ?>*
		<?php echo $form->textField($model,'max_search_months',array('class'=>'form-control numeros number_input')); ?>
		<div class="errorMessage"><?php if(isset($errors['max_search_months'])){ echo $errors['max_search_months']; } ?></div>		
	</div>		
	
	<div class="col-md-4 form-group required">
		<?php echo $form->labelEx($model,'order'); ?>*
		<?php 
		$order=count($order);
		$order_array=array();
		for($i=1;$i<=$order;$i++){
			$order_array[]=array('id'=>$i,'title'=>$i);
		}
		echo $form->dropDownList($model, 'order',CHtml::listData($order_array, 'id', 'title'),array('empty' => 'Seleccione...','class'=>'form-control '));?>		
		<div class="errorMessage"><?php if(isset($errors['order'])){ echo $errors['order']; } ?></div>		
	</div>	
	<div class="col-md-4 form-group col-data">
		<?php echo $form->labelEx($model,'default_view'); ?>
		<?php echo $form->checkBox($model,'default_view',array('class'=>'checkbox_form')); ?>
		<?php echo $form->error($model,'default_view'); ?>
	</div>
	<div class="col-md-4 form-group col-data">
		<?php echo $form->labelEx($model,'online_payment'); ?>
		<?php echo $form->checkBox($model,'online_payment',array('class'=>'checkbox_form')); ?>
		<?php echo $form->error($model,'online_payment'); ?>
	</div>			
	<?php echo $form->hiddenField($model,'filename'); ?>
	<?php echo $form->hiddenField($model,'ext'); ?>
	<div id="data-d-content_1">
		<?php
			foreach($model->imagenes as $imagenes){
				if($imagenes->position==1){
					$data=array('name'=>$imagenes->filename,'position'=>$imagenes->position,'ext'=>$imagenes->ext,'title'=>$imagenes->title,'link'=>$imagenes->link,'order'=>$imagenes->order,'coords'=>json_decode($imagenes->coords));
					echo "<input type='hidden' id=".$imagenes->filename." name='image[]' class='element_data' value='".json_encode($data)."'>";
				}
			}
		?>	
	</div>	
	<div class="form-group col-md-12 ">
		<?php echo $form->labelEx($model,'legal'); ?>
		<?php echo $form->textArea($model,'legal',array('rows'=>6, 'cols'=>50,'class'=>'form-control ckeditor required')); ?>
		<div class="errorMessage"><?php if(isset($errors['legal'])){ echo $errors['legal']; } ?></div>
	</div>			
<div class="form-group col-md-12 ">
		<?php echo $form->labelEx($model,'thankyou_text'); ?>*
		<?php echo $form->textArea($model,'thankyou_text',array('rows'=>6, 'cols'=>50,'class'=>'form-control ckeditor required')); ?>
		<div class="errorMessage"><?php if(isset($errors['thankyou_text'])){ echo $errors['thankyou_text']; } ?></div>
	</div>
<div class="form-group col-md-12 ">
		<?php echo $form->labelEx($model,'error_text'); ?>*
		<?php echo $form->textArea($model,'error_text',array('rows'=>6, 'cols'=>50,'class'=>'form-control ckeditor required')); ?>
		<div class="errorMessage"><?php if(isset($errors['error_text'])){ echo $errors['error_text']; } ?></div>
	</div>				
	<div class="box-footer">
		<?php
			$title = (Yii::app()->controller->action->id!='view') ? 'Volver sin guardar cambios' : 'Volver';
			echo '<a href="'.Yii::app()->request->baseUrl.'/'.strtolower(Yii::app()->controller->id).'" class="btn btn-secundary cancel" data-toggle="tooltip" title="'.$title.'" >Cerrar</a>';
			if(Yii::app()->controller->action->id!='view'){
				echo CHtml::submitButton('Guardar',array('class'=>'btn btn-primary save','data-toggle'=>'tooltip','title'=>'Guardar cambios'));
			}
		?>
	</div>
<?php $this->endWidget(); ?>
<div class="col-md-4 f2_custom">
	<?php echo $form->labelEx($model,'filename'); ?>
	<form class="dropzone needsclick dz-clickable" id="dropzone">
		<?php
			if($model->filename!=''){
				echo '<div class="file_uploaded_class" style="display:block;"><i class="fa fa-times delete_img_i" onclick="$(\'#Search_filename\').val(\'\');$(\'#Search_ext\').val(\'\');$(\'.file_uploaded_class\').html(\'\');$(\'.file_uploaded_class\').css(\'display\',\'none\');"></i><img src="'.Yii::app()->request->baseUrl.'/uploads/tmp/'.Yii::app()->user->id.'/'.$model->filename.'/'.$model->filename.'.'.$model->ext.'" class="center_image" height="100%"/></div>';
			}else{
				echo '<div class="file_uploaded_class"></div>';
			}
		?>		
	  <div class="dz-message needsclick">
		Arrastra los archivos aca o clickea para subir un archivo.<br>
		<span class="note needsclick">(No hay ningun archivo seleccionado.)</span>
	  </div>
	</form>
	<div class="errorMessage"><?php if(isset($errors['filename'])){ echo $errors['filename']; } ?></div>		
</div>
<div class="col-md-12">
	<label class="label_image">Imagen de banners</label>
	<div class="col-md-1">
		<div class="more_img action_img action_img" data-position="1" title="Agregar imagen" data-toggle="tooltip">+</div>
	</div>
	<div class="col-md-11 content-images">
		<ul class="slider1" id="slider_1">
			<?php
				foreach($model->imagenes as $imagenes){
					if($imagenes->position==1){
						echo "<li id='".$imagenes->filename."_slide'  title='Titulo:".$imagenes->title." Enlace:".$imagenes->link."' class='bslide_1 slide ui-state-default ui-sortable-handle'><i class='fa fa-edit edit_img_i' data-position=".$imagenes->position." data-id=".$imagenes->filename." onclick='edit_element($(this))'></i><i class='fa fa-times delete_img_i' data-id=".$imagenes->filename."   data-position=".$imagenes->position." onclick='delete_element($(this))'></i><img src='".Yii::app()->request->baseUrl."/uploads/tmp/".Yii::app()->user->id."/".$imagenes->filename."/".$imagenes->filename.".".$imagenes->ext."'></li>";
					}
				}
			?>	
		</ul>			
	</div>	
</div>
<?php
	// Activo/Inactivo
	echo '<script>viewPage = "'.Yii::app()->controller->action->id.'";</script>';
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/js/form-validators.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerCssFile(Yii::app()->params['cdnUrl'].'/css/dropzone.css'); 	
	Yii::app()->clientScript->registerCssFile(Yii::app()->params['cdnUrl'].'/css/dropzone_one.css');
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/js/dropzone.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerCssFile(Yii::app()->params['cdnUrl'].'/plugins/lcswich/lc_switch-tts.css');
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/lcswich/lc_switch.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/lcswich/lc_switch-tts.js',CClientScript::POS_END);
 ?>
 <script>
id_menu=<?=Yii::app()->user->id_menu()?>;
menu='<?=strtolower(Yii::app()->controller->id)?>';
eval("var cantidad_images_"+1+" = "+$('#data-d-content_1 :input').length);
eval("var limit_images_"+1+" = '5'");
 $( document ).ready(function() {
 	$("#Search_id_channel").change(function(){
		$("#Search_id_subchannel").load(homeurl+"/filtersubchannel/"+$(this).val());
	});
	Dropzone.autoDiscover = false;
	$("#dropzone").dropzone({
		url: homeurl+"/configimages/uploadone/1",
		maxFilesize: 100,
		uploadMultiple:false,
		maxFiles:100,
		previewsContainer:false,
		paramName: "file",
		maxThumbnailFilesize: 5,
		init: function() {
		 this.on('success', function(file, responseText) {
			result=JSON.parse(responseText);
				$('#Search_filename').val(result[0].name);
				$('#Search_ext').val(result[0].ext);
				$('.file_uploaded_class').css('display','block');
				$('.file_uploaded_class').html('<i class="fa fa-times delete_img_i" onclick="$(\'#Search_filename\').val(\'\');$(\'#Search_ext\').val(\'\');$(\'.file_uploaded_class\').html(\'\');$(\'.file_uploaded_class\').css(\'display\',\'none\');"></i><img src="'+homeurl+'/'+result[0].url+'" class="center_image" height="100%"/>');			
		  });
		}
	});
	editor = CKEDITOR.replace( 'Search_thankyou_text', {
		language: 'es',
		height: '100px',
		/*uiColor: '#9AB8F3',*/
		toolbar: [    
		{ name: 'clipboard', groups: [ 'clipboard', 'undo' ], items: [ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ] },	
		{ name: 'document', groups: [ 'mode', 'document', 'doctools' ], items: [ 'Source', '-', 'Save', 'NewPage', 'Preview', 'Print', '-', 'Templates' ] },
		{ name: 'links', items: [ 'Link', 'Unlink', 'Anchor' ] },
		{ name: 'insert', items: [ 'Image', 'Flash', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe' ] },	
		{ name: 'editing', groups: [ 'find', 'selection', 'spellchecker' ], items: [ 'Find', 'Replace', '-', 'SelectAll', '-', 'Scayt' ] },
		{ name: 'forms', items: [ 'Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField' ] },	
		'/',
		{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat' ] },
		{ name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl', 'Language' ] },
		{ name: 'styles', items: [ 'Styles', 'Format', 'Font', 'FontSize' ] },
		{ name: 'colors', items: [ 'TextColor', 'BGColor' ] },
		{ name: 'tools', items: [ 'Maximize', 'ShowBlocks' ] }    
		],  
	}); 	
	editor = CKEDITOR.replace( 'Search_legal', {
		language: 'es',
		height: '100px',
		/*uiColor: '#9AB8F3',*/
		toolbar: [    
		{ name: 'clipboard', groups: [ 'clipboard', 'undo' ], items: [ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ] },	
		{ name: 'document', groups: [ 'mode', 'document', 'doctools' ], items: [ 'Source', '-', 'Save', 'NewPage', 'Preview', 'Print', '-', 'Templates' ] },
		{ name: 'links', items: [ 'Link', 'Unlink', 'Anchor' ] },
		{ name: 'insert', items: [ 'Image', 'Flash', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe' ] },	
		{ name: 'editing', groups: [ 'find', 'selection', 'spellchecker' ], items: [ 'Find', 'Replace', '-', 'SelectAll', '-', 'Scayt' ] },
		{ name: 'forms', items: [ 'Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField' ] },	
		'/',
		{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat' ] },
		{ name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl', 'Language' ] },
		{ name: 'styles', items: [ 'Styles', 'Format', 'Font', 'FontSize' ] },
		{ name: 'colors', items: [ 'TextColor', 'BGColor' ] },
		{ name: 'tools', items: [ 'Maximize', 'ShowBlocks' ] }    
		],  
	});
	editor = CKEDITOR.replace( 'Search_error_text', {
		language: 'es',
		height: '100px',
		/*uiColor: '#9AB8F3',*/
		toolbar: [    
		{ name: 'clipboard', groups: [ 'clipboard', 'undo' ], items: [ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ] },	
		{ name: 'document', groups: [ 'mode', 'document', 'doctools' ], items: [ 'Source', '-', 'Save', 'NewPage', 'Preview', 'Print', '-', 'Templates' ] },
		{ name: 'links', items: [ 'Link', 'Unlink', 'Anchor' ] },
		{ name: 'insert', items: [ 'Image', 'Flash', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe' ] },	
		{ name: 'editing', groups: [ 'find', 'selection', 'spellchecker' ], items: [ 'Find', 'Replace', '-', 'SelectAll', '-', 'Scayt' ] },
		{ name: 'forms', items: [ 'Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField' ] },	
		'/',
		{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat' ] },
		{ name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl', 'Language' ] },
		{ name: 'styles', items: [ 'Styles', 'Format', 'Font', 'FontSize' ] },
		{ name: 'colors', items: [ 'TextColor', 'BGColor' ] },
		{ name: 'tools', items: [ 'Maximize', 'ShowBlocks' ] }    
		],  
	});  	
});
</script>
<?php
	Yii::app()->clientScript->registerScriptFile('https://cdn.ckeditor.com/4.5.7/standard/ckeditor.js',CClientScript::POS_END);	
	//logica uploades de imagenes
	Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/uploader_image.js',CClientScript::POS_END); 
	//logica uploades de imagenes
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/js/dropzone.js',CClientScript::POS_END); 
	Yii::app()->clientScript->registerCssFile(Yii::app()->params['cdnUrl'].'/css/jquery.Jcrop.css');
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/js/jquery.Jcrop.js',CClientScript::POS_END); 
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/js/jquery.bxslider.js',CClientScript::POS_END); 
	Yii::app()->clientScript->registerCssFile(Yii::app()->params['cdnUrl'].'/plugins/jQueryUI/jquery-ui.min.css');
	Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/jquery-ui.js',CClientScript::POS_END);		
	Yii::app()->clientScript->registerCssFile(Yii::app()->params['cdnUrl'].'/css/jquery.bxslider.css');
?>	