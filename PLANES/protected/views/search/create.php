<?php
/* @var $this SearchController */
/* @var $model Search */
?>
<section class="content-header">
	<?php echo Yii::app()->user->Title(); ?>
	<?php echo Yii::app()->user->Breadcrumb(); ?>
</section>
<section class="content">
	<div class="box box-danger">
		<div class="box-header with-border">
			<?php $this->renderPartial('_form', array('model'=>$model,'errors'=>$errors,'order'=>$order,'model_subchannel'=>$model_subchannel)); ?>
		</div>
	</div>
</section>
