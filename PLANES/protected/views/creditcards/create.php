<?php
/* @var $this CreditCardsController */
/* @var $model CreditCards */

$this->breadcrumbs=array(
	'Credit Cards'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List CreditCards', 'url'=>array('index')),
	array('label'=>'Manage CreditCards', 'url'=>array('admin')),
);
?>


<section class="content-header">
	<?php echo Yii::app()->user->Title(); ?>
	<?php echo Yii::app()->user->Breadcrumb(); ?>
	<p class="registros">(<?=$model->count();?> registros encontrados)</p>
</section>
<section class="content">
	<div class="box box-danger">
		<div class="box-header with-border">
			<?php $this->renderPartial('_form', array('model'=>$model,'error'=>$error)); ?>
		</div>
	</div>
</section>