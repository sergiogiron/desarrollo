<?php
/* @var $this CreditCardsController */
/* @var $model CreditCards */
/* @var $form CActiveForm */
?>

<div class="box-body">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'credit-cards-form',
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
	'enableAjaxValidation'=>false,
)); ?>

	<span class="right_f active-button">
				<?php echo $form->checkBox($model,'enabled',array('class'=>'lcs_check enabled')); ?>
		</span>
		<br>
	<div class="col-md-12 col-data">

		<div class="col-xs-6 form-group required">
			<label>Tarjeta </label> <i class="fa fa-key" aria-hidden="true"></i>
			<?php echo $form->textField($model,'name',array('size'=>45,'maxlength'=>45 ,'class'=>'form-control letras')); ?>
			<?php echo $form->error($model,'name'); ?>
		</div>


		<div class="col-xs-6 form-group required">
			<?php echo $form->labelEx($model,'brand'); ?>
			<?php echo $form->textField($model,'brand',array('size'=>45,'maxlength'=>45 ,'class'=>'form-control letras')); ?>
			<?php echo $form->error($model,'brand'); ?>
		</div>

		<div class="col-md-4 form-group col-data">
			<?php echo $form->labelEx($model,'creditcards_highlight'); ?>
			<?php echo $form->checkBox($model,'creditcards_highlight',array('class'=>'checkbox_form')); ?>
			<?php echo $form->error($model,'creditcards_highlight'); ?>
		</div>
		<div class="col-md-12">
			<label class="label_image">Logo</label>
			<div class="col-md-1">
				<div class="more_img action_img action_img" data-position="1" title="Agregar imagen" data-toggle="tooltip">+</div>
			</div>
			<div class="col-md-11 content-images">
				<ul class="slider1" id="slider_1">
					<?php
						foreach($model->imagenes as $imagenes){
							if($imagenes->position==1){
								echo "<li id='".$imagenes->filename."_slide' class='bslide_1 slide ui-state-default ui-sortable-handle'><i class='fa fa-edit edit_img_i' data-position=".$imagenes->position." data-id=".$imagenes->filename." onclick='edit_element($(this))'></i><i class='fa fa-times delete_img_i' data-id=".$imagenes->filename."   data-position=".$imagenes->position." onclick='delete_element($(this))'></i><img src='".Yii::app()->request->baseUrl."/uploads/tmp/".Yii::app()->user->id."/".$imagenes->filename."/".$imagenes->filename.".".$imagenes->ext."'></li>";
							}
						}
					?>
				</ul>
				<div id="data-d-content_1">
					<?php
						foreach($model->imagenes as $imagenes){
							if($imagenes->position==1){
								$data=array('name'=>$imagenes->filename,'position'=>$imagenes->position,'ext'=>$imagenes->ext,'title'=>$imagenes->title,'link'=>$imagenes->link,'order'=>$imagenes->order,'coords'=>json_decode($imagenes->coords));
								echo "<input type='hidden' id=".$imagenes->filename." name='image[]' class='element_data' value='".json_encode($data)."'>";
							}
						}
					?>
				</div>
			</div>
		</div>
		<div style="color: red!important;float: left;margin-left: 12px;clear: both;width: 100%;margin-top: 10px;"><?=$error?></div>
	<a href="#" class="btn btn-primary" id="add_financial" style="display: block;float: left;margin-top: 25px;">Añadir financiación</a>
	<table id="example" class="table table-bordered table-striped  table-list">
        <thead>
            <tr>
                <th>Cantidad de cuotas</th>
                <th>Interés</th>
								<th>Porcentaje Posnet</th>
                <th>Texto CFTNA</th>
								<th>Productos</th>
								<th>Bancos</th>
								<th>Aplica a Sitios</th>
                <th>Vigencia desde</th>
                <th>Vigencia hasta</th>
                <th></th>
                <th></th>
            </tr>
        </thead>
        <tbody id="result">
			<?php
			$financing_value='[]';
			if(count($model->financing)>0){
				$financing_value=array();
				foreach($model->financing as $financing){
					$services_selected=array();
					$services_list='';
					foreach($financing->services as $service_select){
						$services_selected[]=(object) array('id'=>$service_select->id_product,'title'=>$service_select->product->title);
						$services_list.=$service_select->product->title.'-';
					}
					$entitys_selected=array();
					$entitys_list='';
					foreach($financing->entitys as $entity_select){
						$entitys_selected[]=(object) array('id'=>$entity_select->id_entity,'title'=>$entity_select->entity->title);
						$entitys_list.=$entity_select->entity->title.'-';
					}
					if(!isset($financing->rewards)){
						$rewards = 'Todos los sitios';
					}else if($financing->rewards==1){
						$rewards = 'Sitios Con Puntos';
					}else{
						$rewards = 'Sitios Con Cash';
					}
					$financing_value[]= (object) array(
						'id'=>$financing->id,
						'services' =>$services_selected,
						'entitys'  =>  $entitys_selected,
						'amount_dues' => $financing->amount_dues,
						'interests' => $financing->interests,
						'cftna' => $financing->cftna,
						'posnet_cost' => $financing->posnet_cost,
						'rewards' => $financing->rewards,
						'legal' => $financing->legal,
						'date_validity_start' => $financing->date_validity_start,
						'date_validity_end' => $financing->date_validity_end,
					  );
					  if(substr($services_list,0,-1)==''){
						  $services_list='Todos los productos';
					  }else{
						   $services_list=substr($services_list,0,-1);
					  }
						if(substr($entitys_list,0,-1)==''){
						  $entitys_list='Todos los bancos';
					  }else{
						   $entitys_list=substr($entitys_list,0,-1);
					  }
					echo '<tr><td>'.$financing->amount_dues.'</td><td>'.$financing->interests.'</td><td>'.$financing->posnet_cost.'</td><td>'.$financing->cftna.'</td><td>'.$services_list.'</td><td>'.$entitys_list.'</td><td>'.$rewards.'</td><td>'.$financing->date_validity_start.'</td><td>'.$financing->date_validity_end.'</td><td><a href="#" id="'.$financing->id.'" class="edit_credit_cards" data-toggle="tooltip" data-original-title="Editar"><i class="fa fa-edit icon_cbutton"></i></a></td><td><a href="#" id="'.$financing->id.'" class="delete_credit_cards" data-toggle="tooltip" data-original-title="Borrar"><i class="fa fa-trash-o icon_cbutton"></i></a></td></tr>';
				}
				$financing_value=json_encode($financing_value);
			}
			?>
        </tbody>
    </table>
	<input type='hidden' id='financing' name='financing' value='<?php echo $financing_value;?>'/>
	<a href="#" class="btn btn-primary" id="add_airliners" style="display: block;float: left;margin-top: 25px;">Añadir compañia aerea</a>
	<table id="example3" class="table table-bordered table-striped  table-list">
        <thead>
            <tr>
                <th>Aerolíneas</th>
                <th>Cantidad de cuotas</th>
                <th>Interés</th>
                <th>Texto CFTNA</th>
								<th>Porcentaje Posnet</th>
								<th>Aplica a Sitios</th>
                <th>Bancos</th>
                <th>Vigencia desde</th>
                <th>Vigencia hasta</th>
                <th></th>
                <th></th>
            </tr>
        </thead>
        <tbody id="result3">
			<?php
			$airliners_value='[]';
			if(count($model->airliners)>0){
				$airliners_value=array();
				foreach($model->airliners as $airliners){
					$airliners_selected=array();
					$airliners_list='';
					foreach($airliners->airliners as $airliners_select){
						$airliners_selected[]=(object) array('id'=>$airliners_select->id_airliner,'title'=>$airliners_select->airliners->title);
						$airliners_list.=$airliners_select->airliners->title.'-';
					}
					if(!isset($airliners->rewards)){
						$rewards = 'Todos los sitios';
					}else if($airliners->rewards==1){
						$rewards = 'Sitios Con Puntos';
					}else{
						$rewards = 'Sitios Con Cash';
					}
					$airliners_value[]= (object) array(
						'id'=>$airliners->id,
						'airliners' =>$airliners_selected,
						'amount_dues' => $airliners->amount_dues,
						'interests' => $airliners->interests,
						'cftna' => $airliners->cftna,
						'rewards' => $rewards,
						'posnet_cost' => $airliners->posnet_cost,
						'legal' => $airliners->legal,
						'date_validity_start' => $airliners->date_validity_start,
						'date_validity_end' => $airliners->date_validity_end,
						'bank'=>$airliners->bank,
					  );
					  if($airliners->bank==''){
						  $bank='Todos los bancos';
					  }else{
						   $bank=$airliners->bank;
					  }
					echo '<tr><td>'.substr($airliners_list,0,-1).'</td><td>'.$airliners->amount_dues.'</td><td>'.$airliners->interests.'</td><td>'.$airliners->cftna.'</td><td>'.$airliners->posnet_cost.'</td><td>'.$rewards.'</td><td>'.$bank.'</td><td>'.$airliners->date_validity_start.'</td><td>'.$airliners->date_validity_end.'</td><td><a href="#" id="'.$airliners->id.'" class="edit_airliners" data-toggle="tooltip" data-original-title="Editar"><i class="fa fa-edit icon_cbutton"></i></a></td><td><a href="#" id="'.$airliners->id.'" class="delete_airliners" data-toggle="tooltip" data-original-title="Borrar"><i class="fa fa-trash-o icon_cbutton"></i></a></td></tr>';
				}
				$airliners_value=json_encode($airliners_value);
			}
			?>
        </tbody>
    </table>
	<input type='hidden' id='airliners' name='airliners' value='<?=$airliners_value?>'/>
	<a href="#" class="btn btn-primary" id="add_cruisessupplier" style="display: block;float: left;margin-top: 25px;">Añadir naviera</a>
	<table id="example4" class="table table-bordered table-striped  table-list">
        <thead>
            <tr>
                <th>Navieras</th>
								<th>Cantidad de cuotas</th>
                <th>Interés</th>
                <th>Texto CFTNA</th>
								<th>Porcentaje Posnet</th>
								<th>Aplica a Sitios</th>
                <th>Bancos</th>
                <th>Vigencia desde</th>
                <th>Vigencia hasta</th>
                <th></th>
                <th></th>
            </tr>
        </thead>
        <tbody id="result4">
			<?php
			$cruisessupplier_value='[]';
			if(count($model->cruisessupplier)>0){
				$cruisessupplier_value=array();
				foreach($model->cruisessupplier as $cruisessupplier){
					$cruisessupplier_selected=array();
					$cruisessupplier_list='';
					foreach($cruisessupplier->cruisessupplier as $cruisessupplier_select){
						$cruisessupplier_selected[]=(object) array('id'=>$cruisessupplier_select->id_cruises_supplier,'title'=>$cruisessupplier_select->cruisessupplier->title);
						$cruisessupplier_list.=$cruisessupplier_select->cruisessupplier->title.'-';
					}
					if(!isset($airliners->rewards)){
						$rewards = 'Todos los sitios';
					}else if($airliners->rewards==1){
						$rewards = 'Sitios Con Puntos';
					}else{
						$rewards = 'Sitios Con Cash';
					}
					$cruisessupplier_value[]= (object) array(
						'id'=>$cruisessupplier->id,
						'cruisessupplier' =>$cruisessupplier_selected,
						'amount_dues' => $cruisessupplier->amount_dues,
						'interests' => $cruisessupplier->interests,
						'cftna' => $cruisessupplier->cftna,
						'rewards' => $rewards,
						'posnet_cost' => $cruisessupplier->posnet_cost,
						'bank'=>$cruisessupplier->bank,
						'legal' => $cruisessupplier->legal,
						'date_validity_start' => $cruisessupplier->date_validity_start,
						'date_validity_end' => $cruisessupplier->date_validity_end,
					  );
					  if($cruisessupplier->bank==''){
						  $bank='Todos los bancos';
					  }else{
						   $bank=$cruisessupplier->bank;
					  }
					echo '<tr><td>'.substr($cruisessupplier_list,0,-1).'</td><td>'.$cruisessupplier->amount_dues.'</td><td>'.$cruisessupplier->interests.'</td><td>'.$cruisessupplier->cftna.'</td><td>'.$cruisessupplier->posnet_cost.'</td><td>'.$rewards.'</td><td>'.$bank.'</td><td>'.$cruisessupplier->date_validity_start.'</td><td>'.$cruisessupplier->date_validity_end.'</td><td><a href="#" id="'.$cruisessupplier->id.'" class="edit_cruisessupplier" data-toggle="tooltip" data-original-title="Editar"><i class="fa fa-edit icon_cbutton"></i></a></td><td><a href="#" id="'.$cruisessupplier->id.'" class="delete_cruisessupplier"  data-toggle="tooltip" data-original-title="Borrar"><i class="fa fa-trash-o icon_cbutton"></i></a></td></tr>';
				}
				$cruisessupplier_value=json_encode($cruisessupplier_value);
			}
			?>
        </tbody>
    </table>
	<input type='hidden' id='cruisessupplier' name='cruisessupplier' value='<?=$cruisessupplier_value?>'/>
		<div class="box-footer">
			<?php
			$title = (Yii::app()->controller->action->id!='view') ? 'Volver sin guardar cambios' : 'Volver';
			echo '<a href="'.Yii::app()->request->baseUrl.'/'.strtolower(Yii::app()->controller->id).'" class="btn btn-secundary" data-toggle="tooltip" title="'.$title.'" >Cerrar</a>';
			if(Yii::app()->controller->action->id!='view'){
				echo CHtml::submitButton('Guardar',array('class'=>'btn btn-primary','data-toggle'=>'tooltip','title'=>'Guardar cambios'));
			}
			?>
		</div>
		</div>
	</div>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<?php
echo '<script>viewPage = "'.Yii::app()->controller->action->id.'";</script>';
Yii::app()->clientScript->registerScriptFile('https://cdn.ckeditor.com/4.5.7/standard/ckeditor.js',CClientScript::POS_END);
Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/js/form-validators.js',CClientScript::POS_END);
Yii::app()->clientScript->registerScriptFile('//cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js',CClientScript::POS_END);
 Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/plugins/select2/select2.full.js',CClientScript::POS_END);
 Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl.'/js/plugins/select2/select2.css'); ?>
	<script>
id_menu=<?=Yii::app()->user->id_menu()?>;
menu='<?=strtolower(Yii::app()->controller->id)?>';
eval("var cantidad_images_"+1+" = "+$('#data-d-content_1 :input').length);
//eval("var cantidad_images_"+2+" = "+$('#data-d-content_2 :input').length);
eval("var limit_images_"+1+" = '1'");
//eval("var limit_images_"+2+" = '2'");
$( ".delete_credit_cards" ).on( "click", function(event) {
	event.preventDefault();
	var id=$(this).attr('id');
	var financing=JSON.parse($('#financing').val());
	var financing_new=[];
	$.each( financing, function( key, value ) {
		if(value.id!=id){
			financing_new.push(value);
		}
	});
	$('#financing').val(JSON.stringify(financing_new));
	$(this).parent().parent().remove();
});
$( ".edit_credit_cards" ).on( "click", function(event) {
	event.preventDefault();
	var id=$(this).attr('id');
	var financing_edit;
	var financing=JSON.parse($('#financing').val());
	$.each( financing, function( key, value ) {
		if(value.id==id){
			financing_edit=value;
		}
	});
	var request = $.ajax({
		url: homeurl+'/creditcards/financingedit',
		method: "POST",
		data: financing_edit,
	}).done(function(msg) {
		$('.ligth-box-modal').css('display','block');
		$('.modal-content').css('display','block');
		$('.modal-title').html('Financiación');
		$('.modal-body').html(msg);
		$('.modal-footer').html('<a href="#" onclick="refresh_datos(event);" class="btn btn-primary">Actualizar</a>');
	});
});
$( ".edit_cruisessupplier" ).on( "click", function(event) {
	event.preventDefault();
	var id=$(this).attr('id');
	var cruisessupplier_edit;
	var cruisessupplier=JSON.parse($('#cruisessupplier').val());
	$.each( cruisessupplier, function( key, value ) {
		if(value.id==id){
			cruisessupplier_edit=value;
		}
	});
	var request = $.ajax({
		url: homeurl+'/creditcards/cruisessupplieredit',
		method: "POST",
		data: cruisessupplier_edit,
	}).done(function(msg) {
		$('.ligth-box-modal').css('display','block');
		$('.modal-title').html('Navieras');
		$('.modal-content').css('display','block');
		$('.modal-body').html(msg);
		$('.modal-footer').html('<a href="#" onclick="refresh_datos(event);" class="btn btn-primary">Actualizar</a>');
	});
});
$( ".delete_cruisessupplier" ).on( "click", function(event) {
	event.preventDefault();
	var id=$(this).attr('id');
	var cruisessupplier=JSON.parse($('#cruisessupplier').val());
	var cruisessupplier_new=[];
	$.each( cruisessupplier, function( key, value ) {
		if(value.id!=id){
			cruisessupplier_new.push(value);
		}
	});
	$('#cruisessupplier').val(JSON.stringify(cruisessupplier_new));
	$(this).parent().parent().remove();
});
$( ".edit_airliners" ).on( "click", function(event) {
	event.preventDefault();
	var id=$(this).attr('id');
	var airliners_edit;
	var airliners=JSON.parse($('#airliners').val());
	$.each( airliners, function( key, value ) {
		if(value.id==id){
			airliners_edit=value;
		}
	});
	var request = $.ajax({
		url: homeurl+'/creditcards/airlinersedit',
		method: "POST",
		data: airliners_edit,
	}).done(function(msg) {
		$('.ligth-box-modal').css('display','block');
		$('.modal-title').html('Aerolíneas');
		$('.modal-content').css('display','block');
		$('.modal-body').html(msg);
		$('.modal-footer').html('<a href="#" onclick="refresh_datos(event);" class="btn btn-primary">Actualizar</a>');
	});
});
$( ".delete_airliners" ).on( "click", function(event) {
	event.preventDefault();
	var id=$(this).attr('id');
	var airliners=JSON.parse($('#airliners').val());
	var airliners_new=[];
	$.each( airliners, function( key, value ) {
		if(value.id!=id){
			airliners_new.push(value);
		}
	});
	$('#airliners').val(JSON.stringify(airliners_new));
	$(this).parent().parent().remove();
});
$( document ).ready(function() {
	 $('#add_financial').click(function(event){
		event.preventDefault();
		var request = $.ajax({
			url: homeurl+'/creditcards/financing',
			method: "POST",
		}).done(function(msg) {
			$('.ligth-box-modal').css('display','block');
			$('.modal-title').html('Financiación');
			$('.modal-content').css('display','block');
			$('.modal-body').html(msg);
			$('.modal-footer').html('<a href="#" onclick="refresh_datos(event);" class="btn btn-primary">Actualizar</a>');
		});
	});
	 $('#add_airliners').click(function(event){
		event.preventDefault();
		var request = $.ajax({
			url: homeurl+'/creditcards/airliners',
		}).done(function(msg) {
			$('.ligth-box-modal').css('display','block');
			$('.modal-title').html('Aerolíneas');
			$('.modal-content').css('display','block');
			$('.modal-body').html(msg);
			$('.modal-footer').html('<a href="#" onclick="refresh_datos(event);" class="btn btn-primary">Actualizar</a>');
		});
	});
	 $('#add_cruisessupplier').click(function(event){
		event.preventDefault();
		var request = $.ajax({
			url: homeurl+'/creditcards/cruisessupplier',
		}).done(function(msg) {
			$('.ligth-box-modal').css('display','block');
			$('.modal-title').html('Navieras');
			$('.modal-content').css('display','block');
			$('.modal-body').html(msg);
			$('.modal-footer').html('<a href="#" onclick="refresh_datos(event);" class="btn btn-primary">Actualizar</a>');
		});
	});
	$('#example3').dataTable({
		"bPaginate": false,
		"bLengthChange": false,
		"bFilter": true,
		"bInfo": false,
		"bAutoWidth": false,
		"language": {
			"sSearch": "Buscar:",
		}
	});
	$('#example4').dataTable({
		"bPaginate": false,
		"bLengthChange": false,
		"bFilter": true,
		"bInfo": false,
		"bAutoWidth": false,
		"language": {
			"sSearch": "Buscar:",
		}
	});
	$('#example').dataTable({
		"bPaginate": false,
		"bLengthChange": false,
		"bFilter": true,
		"bInfo": false,
		"bAutoWidth": false,
		"language": {
			"sSearch": "Buscar:",
		}
	});
});
</script>

<?php $this->endWidget(); ?>

</div>

<?php
	// Activo/Inactivo
	Yii::app()->clientScript->registerCssFile(Yii::app()->params['cdnUrl'].'/plugins/lcswich/lc_switch-tts.css');
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/lcswich/lc_switch.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/lcswich/lc_switch-tts.js',CClientScript::POS_END);
	// Datepicker con la mascara
	Yii::app()->clientScript->registerCssFile(Yii::app()->params['cdnUrl'].'/plugins/datepicker/datepicker3.css');
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/datepicker/bootstrap-datepicker.js',CClientScript::POS_END);
		Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/input-mask/jquery.inputmask.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/input-mask/jquery.inputmask.date.extensions.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/datepicker/datepicker-tts.js',CClientScript::POS_END);	
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/input-mask/jquery.inputmask.numeric.extensions.js',CClientScript::POS_END);

	//logica uploades de imagenes
	Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/uploader_image.js',CClientScript::POS_END);
	//logica uploades de imagenes
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/js/dropzone.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerCssFile(Yii::app()->params['cdnUrl'].'/css/jquery.Jcrop.css');
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/js/jquery.Jcrop.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/js/jquery.bxslider.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerCssFile(Yii::app()->params['cdnUrl'].'/plugins/jQueryUI/jquery-ui.min.css');
	Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/jquery-ui.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerCssFile(Yii::app()->params['cdnUrl'].'/css/jquery.bxslider.css');
	// Timepicker con la mascara
	/*
	Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/../plugins/timepicker/bootstrap-timepicker.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/../plugins/input-mask/jquery.inputmask.bundle.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl.'/../plugins/timepicker/bootstrap-timepicker.css');
	Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/../plugins/timepicker/timepicker-tts.js',CClientScript::POS_END);
	// Editor de texto enriquesido
	Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/../plugins/ckeditor-standard/ckeditor.js',CClientScript::POS_END);
	*/
 ?>
   <script type="text/javascript">
	<?php
		if(Yii::app()->controller->action->id=='view'){ echo "$('form').find('input, textarea, checkbox, select').attr('disabled','disabled'); $('.fa-pencil-square-o').hide()"; }
	?>
</script>
