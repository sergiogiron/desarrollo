<form id="financing_form">
<div style="color:red;display:none;" id="required">Debe completar todos los campos.<br/></div>
<div class="row">
<div class="col-md-3 required">
		<label>Cantidad de cuotas <span class="required">*</span></label>
		<input size="62" maxlength="16" class="form-control cuotas" name="amount_dues" id="amount_dues" type="text">
</div>
<div class="col-md-3 required">
		<label>Interés <span class="required">*</span></label>
		<input size="62" maxlength="62" class="form-control" name="interests" id="interests" type="text">
</div>
<div class="col-md-3 required">
		<label>Texto CFTNA <span class="required">*</span></label>
		<input size="62" maxlength="62" class="form-control" name="cftna" id="cftna" type="text">
</div>
<div class="col-md-3 required">
		<label>Porcentaje Posnet <span class="required">*</span></label>
		<input size="6" maxlength="6" class="form-control decimal" name="posnet_cost" id="posnet_cost" type="text">
</div>
<div class="col-md-3 required">
	<label for="Entity_expire_date_from" class="required">Vigencia desde <span class="required">*</span></label>
	<div class="input-group">
		<input class="form-control datepicker" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="data-mask" name="date_validity_start" id="date_validity_start" type="text" >
		<div class="input-group-addon pointer"><i class="fa fa-calendar"></i></div>
	</div>
</div>
<div class="col-md-3 required">
	<label for="Entity_expire_date_from" class="required">Vigencia hasta <span class="required">*</span></label>
	<div class="input-group">
		<input class="form-control datepicker" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="data-mask" name="date_validity_end" id="date_validity_end" type="text" >
		<div class="input-group-addon pointer"><i class="fa fa-calendar"></i></div>
	</div>
</div>
<div class="col-md-6">
		<label>Servicios</label>
		<?php echo CHtml::dropDownList('services', array(),CHtml::listData(Products::model()->findAll(),'id','title'), array('class'=>'form-control select2','data-placeholder'=>'Todos los productos','multiple'=>'multiple','style'=>'width:100%')); ?>
</div>
<div class="col-md-6">
		<label>Bancos</label>
		<?php echo CHtml::dropDownList('entitys', array(),CHtml::listData(Entity::model()->findAll(),'id','title'), array('class'=>'form-control select2','data-placeholder'=>'Todos los bancos','multiple'=>'multiple','style'=>'width:100%')); ?>
</div>
<div class="col-md-6">
	<label>Aplica a :</label>
	<select name="rewards" id="rewards" class="form-control select2">
		<option value="">Todos los sitios</option>
		<option value="1">Solo sitios con puntos</option>
		<option value="0">Solo sitios Cash</option>
	</select>
</div>

<div class="col-md-12 required">
		<label>Legal <span class="required">*</span></label>
		<textarea class="ckeditor" name="legal" id="legal"></textarea>
</div>
</div>
</form>
<?php
	echo '<script>viewPage = "'.Yii::app()->controller->action->id.'";</script>';
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/js/form-validators.js',CClientScript::POS_END);
?>
<script>
function refresh_datos(event){
	event.preventDefault();
	var error=0;
	$('#financing_form :input').each( function () {
		if($(this).attr('type')!='search'){
			if ($(this).val()== '' ){
				if($(this).attr('id')=='legal'){
					if(editor.getData()== '' ){
						error=1;
					}
				}else{
					error=1;
				}
			}
		}
	});
	if($("#rewards").val() == ""){

		error = 0;
	}
	if(error>0){
		$('#required').css('display','block');
	}else{
		var formdata = $("#financing_form").serializeArray();
		var data = {};
		$(formdata).each(function(index, obj){
			if(obj.name!='services[]'){
				data[obj.name] = obj.value;
			}
		});
		data.legal=editor.getData();
		data.id='new_'+parseInt(Math.random()*30000000);
		id_services_array=[];
		$.each( $('#services').select2('data'), function( key, value ) {
			id_services_object=new Object;
			id_services_object.id=value.id;
			id_services_object.title=value.text;
			id_services_array.push(id_services_object);
		});

		id_entitys_array=[];
		$.each( $('#entitys').select2('data'), function( key, value ) {
			id_entitys_object=new Object;
			id_entitys_object.id=value.id;
			id_entitys_object.title=value.text;
			id_entitys_array.push(id_entitys_object);
		});

		data.services=id_services_array;
		data.entitys=id_entitys_array;

		var financing=JSON.parse($('#financing').val());
		financing.push(data);
		$('#financing').val(JSON.stringify(financing));
		$('#example').dataTable().fnDestroy();
		$('#result').html('');
		$.each( financing, function( key, value ) {
				services_text='';
				$.each( value.services, function( key2, value2 ) {
					services_text+=value2.title+'-';
				});
				if(services_text.substr(0,services_text.length-1)==''){
					services_text='Todos los productos';
				}else{
					services_text=services_text.substr(0,services_text.length-1);
				}
				entitys_text='';
				$.each( value.entitys, function( key3, value3 ) {
					entitys_text+=value3.title+'-';
				});
				if(entitys_text.substr(0,entitys_text.length-1)==''){
					entitys_text='Todos los productos';
				}else{
					entitys_text=entitys_text.substr(0,entitys_text.length-1);
				}
				console.log(entitys_text);
				var rewards;
				if(value.rewards ==1){
					rewards = 'Solo sitios con puntos';
				}else{
					rewards = 'Solo sitios cash';
				}
				if(value.rewards == ""){
					rewards = "Todos los sitios";
				}
				$('#result').append('<tr><td>'+value.amount_dues+'</td><td>'+value.interests+'</td><td>'+value.cftna+'</td><td>'+value.posnet_cost+'</td><td>'+services_text+'</td><td>'+entitys_text+'</td><td>'+rewards+'</td><td>'+value.date_validity_start+'</td><td>'+value.date_validity_end+'</td><td><a href="#" id="'+ value.id+'" class="edit_credit_cards"  data-toggle="tooltip" data-original-title="Editar"><i class="fa fa-edit icon_cbutton"></i></a></td><td><a href="#" id="'+ value.id+'" class="delete_credit_cards" data-toggle="tooltip" data-original-title="Borrar"><i class="fa fa-trash-o icon_cbutton"></i></a></td></tr>');
		});
		datatable=$('#example').dataTable({
			"bPaginate": false,
			"bLengthChange": false,
			"bFilter": true,
			"bInfo": false,
			"bAutoWidth": false,
			"language": {
				"sSearch": "Buscar:",
			}
		});
		$( ".delete_credit_cards" ).on( "click", function(event) {
			event.preventDefault();
			var id=$(this).attr('id');
			var financing=JSON.parse($('#financing').val());
			var financing_new=[];
			$.each( financing, function( key, value ) {
				if(value.id!=id){
					financing_new.push(value);
				}
			});
			$('#financing').val(JSON.stringify(financing_new));
			$(this).parent().parent().remove();
		});
		$( ".edit_credit_cards" ).on( "click", function(event) {
			event.preventDefault();
			var id=$(this).attr('id');
			var financing_edit;
			var financing=JSON.parse($('#financing').val());
			$.each( financing, function( key, value ) {
				if(value.id==id){
					financing_edit=value;
				}
			});
			var request = $.ajax({
				url: homeurl+'/entity/financingedit',
				method: "POST",
				data: financing_edit,
			}).done(function(msg) {
				$('.ligth-box-modal').css('display','block');
				$('.modal-title').html('Financiación');
				$('.modal-body').html(msg);
				$('.modal-footer').html('<a href="#" onclick="refresh_datos(event);" class="btn btn-primary">Actualizar</a>');
			});
		});
		$('.close_window').trigger('click');
	}
}
$( document ).ready(function() {
	$('#services').select2().on('select2:unselect', function(e) {
		$(this).next().find('.select2-selection').trigger('click');
	});
	$('#entitys').select2().on('select2:unselect', function(e) {
		$(this).next().find('.select2-selection').trigger('click');
	});
	$(".cuotas").keypress(function (key) {
		/*if(key.charCode == 45 || key.charCode == 44){
			if($(".cuotas").val().indexOf(',') != -1){
				return false;
			}
			if($(".cuotas").val().indexOf('-') != -1){
				return false;
			}
		}*/
		if ( (key.charCode < 48 || key.charCode > 57)//numeros
			&& (key.charCode != 44) //coma
			&& (key.charCode != 45) //guion medio
			)
			return false;
    });
    $('.datepicker').datepicker({
		format:'dd/mm/yyyy',
        autoclose: true,
    });
	$('.datepicker').inputmask();

    $('.fa-upload').click(function(){
		$("#file").click();
	});
	editor = CKEDITOR.replace( 'legal', {
		language: 'es',
		height: '100px',
		/*uiColor: '#9AB8F3',*/
		toolbar: [
		{ name: 'clipboard', groups: [ 'clipboard', 'undo' ], items: [ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ] },
		{ name: 'document', groups: [ 'mode', 'document', 'doctools' ], items: [ 'Source', '-', 'Save', 'NewPage', 'Preview', 'Print', '-', 'Templates' ] },
		{ name: 'links', items: [ 'Link', 'Unlink', 'Anchor' ] },
		{ name: 'insert', items: [ 'Image', 'Flash', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe' ] },
		{ name: 'editing', groups: [ 'find', 'selection', 'spellchecker' ], items: [ 'Find', 'Replace', '-', 'SelectAll', '-', 'Scayt' ] },
		{ name: 'forms', items: [ 'Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField' ] },
		'/',
		{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat' ] },
		{ name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl', 'Language' ] },
		{ name: 'styles', items: [ 'Styles', 'Format', 'Font', 'FontSize' ] },
		{ name: 'colors', items: [ 'TextColor', 'BGColor' ] },
		{ name: 'tools', items: [ 'Maximize', 'ShowBlocks' ] }
		],
	});
	$(".decimal").inputmask("decimal", {
  		//'groupSeparator': ',', 
  		'autoGroup': true, 
  		'digits': 2, 
  		'integerDigits':2,
  		'digitsOptional': false, 
  		'integerOpional': false, 
  		'placeholder': '0.00', 
  		rightAlign : true,
  		//clearMaskOnLostFocus: !1 
	  });
		$(".decimal").focusout(function(){
			var val = $(this).val();
			if(val>100){
				$(this).val('99.99');
			}
		})
});
</script>
