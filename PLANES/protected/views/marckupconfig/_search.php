<?php
/* @var $this MarckupConfigController */
/* @var $model MarckupConfig */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>


	<div class="col-md-12" style="clear: both">
	<div class="row">
	
		
	<div class="col-md-4">
		<div class="row row_space">
		<?php echo $form->labelEx($model,'id_marckup_concept'); ?>
		<?php echo $form->dropDownList($model, 'id_marckup_concept',CHtml::listData(Marckupconcept::model()->findAll(), 'id', 'concept'),array('empty' => 'Seleccione','class'=>'form-control'));?>
		<?php echo $form->error($model,'id_marckup_concept'); ?>
	</div>
	</div>

	<div class="col-md-4">
		<div class="row row_space">
		<?php echo $form->labelEx($model,'id_product'); ?>
		<?php echo $form->dropDownList($model, 'id_product',CHtml::listData(Products::model()->findAll(), 'id', 'description'),array('empty' => 'Seleccione','class'=>'form-control'));?>
		<?php echo $form->error($model,'id_product'); ?>
	</div>
	</div>
	

	<div class="col-md-4">
		<div class="row row_space">
		<?php echo $form->labelEx($model,'id_provider'); ?>
		<?php echo $form->dropDownList($model, 'id_provider',CHtml::listData(Providers::model()->findAll(), 'id', 'description'),array('empty' => 'Seleccione','class'=>'form-control'));?>
		<?php echo $form->error($model,'id_provider'); ?>
	</div>
	</div>
	
	<div class="col-md-4">
		<div class="row row_space">
		<?php echo $form->labelEx($model,'id_channel'); ?>
		<?php echo $form->dropDownList($model, 'id_channel',CHtml::listData(Channels::model()->findAll(), 'id', 'description'),array('empty' => 'Seleccione','class'=>'form-control'));?>
		<?php echo $form->error($model,'id_channel'); ?>
	</div>
	</div>
	
	<div class="col-md-4">
		<div class="row row_space">
		<?php echo $form->labelEx($model,'id_subchannel'); ?>
		<?php echo $form->dropDownList($model, 'id_subchannel',CHtml::listData(Subchannels::model()->findAll(), 'id', 'name'),array('empty' => 'Seleccione','class'=>'form-control'));?>
		<?php echo $form->error($model,'id_subchannel'); ?>
	</div>
	</div>

	<div class="col-md-4">
		<div class="row row_space">
		
		<?php echo $form->labelEx($model,'enabled'); ?>
		<?php echo $form->checkBox($model,'enabled',array('class'=>'checkbox_form')); ?>
		
	</div>
	</div>

	
	</div>
	<div class="row buttons">
		<?php echo CHtml::submitButton('Buscar',array('class'=>'btn btn-default b_customize')); ?>
	</div>
<br />
<?php $this->endWidget(); ?>
</div>
</div><!-- search-form -->