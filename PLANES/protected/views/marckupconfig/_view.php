<?php
/* @var $this MarckupConfigController */
/* @var $data MarckupConfig */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_marckup_concept')); ?>:</b>
	<?php echo CHtml::encode($data->id_marckup_concept); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_product')); ?>:</b>
	<?php echo CHtml::encode($data->id_product); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_provider')); ?>:</b>
	<?php echo CHtml::encode($data->id_provider); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_channel')); ?>:</b>
	<?php echo CHtml::encode($data->id_channel); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_subchannel')); ?>:</b>
	<?php echo CHtml::encode($data->id_subchannel); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('marckup_percentage')); ?>:</b>
	<?php echo CHtml::encode($data->marckup_percentage); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('enabled')); ?>:</b>
	<?php echo CHtml::encode($data->enabled); ?>
	<br />

	*/ ?>

</div>