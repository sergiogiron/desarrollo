<?php
/* @var $this MarckupConfigController */
/* @var $model MarckupConfig */
/* @var $form CActiveForm */
?>

<div class="box-body">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'marckup-config-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<span class="right_f active-button">
		<?php echo $form->checkBox($model,'enabled',array('class'=>'lcs_check enabled')); ?>
	</span>
	<br>
	<div class="col-md-12 col-data">
	<div class="form-group col-md-3 required unique">
		<?php echo $form->labelEx($model,'id_channel'); ?>
		<?php echo $form->dropDownList($model, 'id_channel',CHtml::listData(Channels::model()->findAll(), 'id', 'title'),array('empty' => 'Seleccione...','class'=>'form-control selectobject channel'));?>
		<?php echo $form->error($model,'id_channel'); ?>
	</div>

	<div class="form-group col-md-3 required unique">
		<?php echo $form->labelEx($model,'id_subchannel'); ?>
		<?php echo $form->dropDownList($model, 'id_subchannel',CHtml::listData(Subchannels::model()->findAll(), 'id', 'title'),array('empty' => 'Seleccione...','class'=>'form-control selectobject subchannel'));?>
		<?php echo $form->error($model,'id_subchannel'); ?>
	</div>
	<div class="form-group col-md-3 required  unique">
		<?php echo $form->labelEx($model,'id_product'); ?>
		<?php echo $form->dropDownList($model, 'id_product',CHtml::listData(Products::model()->findAll(), 'id', 'title'),array('empty' => 'Seleccione...','class'=>'form-control selectobject'));?>
		<?php echo $form->error($model,'id_product'); ?>
	</div>

	<div class="form-group col-md-3 required unique">
		<?php echo $form->labelEx($model,'id_provider'); ?>
		<?php echo $form->dropDownList($model, 'id_provider',CHtml::listData(Providers::model()->findAll(), 'id', 'title'),array('empty' => 'Seleccione...','class'=>'form-control selectobject'));?>
		<?php echo $form->error($model,'id_provider'); ?>
	</div>

	<div class="form-group col-md-4 required">
		<?php echo $form->labelEx($model,'id_marckup_concept'); ?>
		<?php echo $form->dropDownList($model, 'id_marckup_concept',CHtml::listData(Marckupconcept::model()->findAll(), 'id', 'title'),array('empty' => 'Seleccione...','class'=>'form-control selectobject'));?>
		<?php echo $form->error($model,'id_marckup_concept'); ?>
	</div>


	<div class="form-group col-md-4 required">
		<?php echo $form->labelEx($model,'marckup_percentage'); ?>
		<div class="input-group">

			<?php echo $form->textField($model,'marckup_percentage',array('size'=>6,'maxlength'=>5,'class'=>'form-control decimal', "style"=>"width: 56px; height: 34px;")); ?>
			<span class="input-group-addon" style="padding: 9px;background-color: #FFC !important;float: left;width: 30px;"><strong> % </strong></span>	
			<?php echo $form->error($model,'marckup_percentage'); ?>
		</div>
	</div>


</div>
	<div class="box-footer">
		<?php
			$title = (Yii::app()->controller->action->id!='view') ? 'Volver sin guardar cambios' : 'Volver';
			echo '<a href="'.Yii::app()->request->baseUrl.'/'.strtolower(Yii::app()->controller->id).'" class="btn btn-secundary cancel" data-toggle="tooltip" title="'.$title.'" >Cerrar</a>';
			if(Yii::app()->controller->action->id!='view'){
				echo CHtml::submitButton('Guardar',array('class'=>'btn btn-primary save','data-toggle'=>'tooltip','title'=>'Guardar cambios'));
			}
		?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
</div>
 <?php
	Yii::app()->clientScript->registerCssFile(Yii::app()->params['cdnUrl'].'/plugins/lcswich/lc_switch-tts.css');
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/lcswich/lc_switch.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/lcswich/lc_switch-tts.js',CClientScript::POS_END);

	Yii::app()->clientScript->registerCssFile(Yii::app()->params['cdnUrl'].'/plugins/datepicker/datepicker3.css');
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/datepicker/bootstrap-datepicker.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/input-mask/jquery.inputmask.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/input-mask/jquery.inputmask.numeric.extensions.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/datepicker/datepicker-tts.js',CClientScript::POS_END);

	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/timepicker/bootstrap-timepicker.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/input-mask/jquery.inputmask.bundle.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerCssFile(Yii::app()->params['cdnUrl'].'/plugins/timepicker/bootstrap-timepicker.css');
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/timepicker/timepicker-tts.js',CClientScript::POS_END);

	Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/plugins/select2/select2.full.js',CClientScript::POS_END);

	Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl.'/js/plugins/select2/select2.css');

	echo '<script>viewPage = "'.Yii::app()->controller->action->id.'";</script>';
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/js/form-validators.js',CClientScript::POS_END) ?>
<script>

$( document ).ready(function() {

	<?php if(Yii::app()->controller->action->id=='view'){ echo "$('form').find('input, textarea, button, checkbox, select').attr('disabled','disabled');"; } ?>
	$('[data-mask]').inputmask('dd/mm/yyyy', {'placeholder': 'dd/mm/aaaa'});
	$('.fa-calendar').datepicker().on('changeDate', function(e) {
		$(this).parent().prev().val(e.format('dd/mm/yyyy'));
	    $(this).datepicker('hide');
    });

	$('.select2').select2();

	$(".decimal").inputmask({
	  		'alias': 'decimal',
	  		//'groupSeparator': ',',
	  		'autoGroup': true,
	  		'digits': 2,
	  		'digitsOptional': false,
	  		'placeholder': '0.00',
	  		rightAlign : true,
	  		clearMaskOnLostFocus: !1
	  	});

	$('.datepicker').datepicker({
      autoclose: true,
	  //format: 'yyyy-mm-dd',
	  format: 'dd/mm/yyyy',
	  firstDay: 1
    });

   channelsFilter();

	if(viewPage=='update'){
		$('.unique input').prop('disabled',true);
		$('.unique select').prop('disabled',true);
		//$(".subchannel").prop('disabled',false);
	}
});
</script>
