<?php echo $this->renderPartial('//layouts/publish',array('channel'=>$channel,'title'=>$title,'breadcrumb'=>$breadcrumb));  ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/plugins/select2/select2.full.js'); ?>
<?php Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl.'/js/plugins/select2/select2.css'); ?>
<?php Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl.'/plugins/datepicker/datepicker3.css'); ?>
<?php Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl.'/plugins/timepicker/bootstrap-timepicker.css'); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/plugins/datepicker/bootstrap-datepicker.js'); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/plugins/timepicker/bootstrap-timepicker.js'); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/plugins/input-mask/jquery.inputmask.js'); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/plugins/input-mask/jquery.inputmask.date.extensions.js'); ?>
<script>
$( document ).ready(function() {
	/*$("#channel").change(function(){ 
		$("#subchannel").load(homeurl+"/filtersubchannel/"+$(this).val()); 
	});	*/
	$('.select2').select2().on('select2:unselect', function(e) {
		$('.select2-selection').trigger('click');
	});
	$("[data-mask]").inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/aaaa"});
	$( ".publicar" ).click(function(event){
		event.preventDefault();
		if($('#hour').val()!='' && $('#date').val()!='' && $('#channel').val()!='' && $('#subchannel').val()!=''){
			$.ajax({
			  method: "POST",
			  url: homeurl+'/marckupconfig/publish',
			  data: $('#publication_form').serialize(),
			}).complete(function() {
				$('.close_window').trigger('click');
			});
		}else{
			alert('Debe completar todos los campos');
		}
	});	
	$('.fa-calendar').datepicker().on('changeDate', function(e) {
		$(this).parent().prev().val(e.format("dd/mm/yyyy"));
	    $(this).datepicker('hide');
    });
    $(".timepicker").timepicker({
      showInputs: false,
	  showMeridian:false
    });
});	
</script>	
