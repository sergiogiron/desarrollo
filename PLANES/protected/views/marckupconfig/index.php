<?php
/* @var $this MarckupConfigController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Marckup Configs',
);

$this->menu=array(
	array('label'=>'Create MarckupConfig', 'url'=>array('create')),
	array('label'=>'Manage MarckupConfig', 'url'=>array('admin')),
);
?>

<h1>Marckup Configs</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
