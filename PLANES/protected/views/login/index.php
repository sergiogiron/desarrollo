<div class="login-box">
  <div class="login-logo">
	<img src="<?php echo Yii::app()->request->baseUrl; ?>/img/logo.png" alt="150" width="200"  />
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
	<p class="login-box-msg">Ingresa para iniciar sesion.</p>
	<?php
	$this->pageTitle=Yii::app()->name . ' - Login';
	$this->breadcrumbs=array(
		'Login',
	);
	?>
	<div class="form">
	<?php $form=$this->beginWidget('CActiveForm', array(
		'id'=>'login-form',
		'enableAjaxValidation'=>true,
		'htmlOptions' => array("autocomplete"=>"off")
	)); ?>

		<div class="form-group has-feedback">
			<?php echo $form->labelEx($model,'username'); ?>
			<?php echo $form->textField($model,'username',array('class'=>'form-control')); ?>
			<?php echo $form->error($model,'username'); ?>
		</div>

		<div class="form-group has-feedback">
			<?php echo $form->labelEx($model,'password'); ?>
			<?php echo $form->passwordField($model,'password',array('class'=>'form-control')); ?>
			<?php echo $form->error($model,'password'); ?>
		</div>
      <div class="row">
        <div class="col-xs-8">
          <div class="checkbox icheck">
            <label>
			<?php echo $form->checkBox($model,'rememberMe'); ?>
			<?php echo $form->label($model,'rememberMe',array('class'=>'icheck')); ?>
			<?php echo $form->error($model,'rememberMe'); ?>
            </label>
          </div>
        </div>
        <!-- /.col -->
        <div class="col-xs-4">
		<?php echo CHtml::submitButton('Ingresar',array('class'=>'btn btn-primary btn-block btn-flat')); ?>
        </div>
        <!-- /.col -->
      </div>
	  <div class="social-auth-links text-center">
		2016 © FEROZO SOLUCIONES INFORMATICAS
	</div>

	<?php $this->endWidget(); ?>
	</div><!-- form -->
  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box --> 
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/plugins/iCheck/icheck.min.js',CClientScript::POS_END); ?>
<?php Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl.'/js/plugins/iCheck/square/blue.css'); ?>
<script>
$(document).ready(function(){
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
 });	
</script>