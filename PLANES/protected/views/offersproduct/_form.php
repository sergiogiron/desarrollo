<?php
/* @var $this OffersproductController */
/* @var $model Offersproduct */
/* @var $form CActiveForm */
?>
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'offersproduct-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>
<div class="box-body">
<span class="right_f active-button">
	<?php echo $form->checkBox($model,'enabled',array('class'=>'lcs_check enabled')); ?>
</span>
<br />
<div class="col-md-12 col-data">
<div class="row">
<div class="row">
	<div class="form-group col-md-4 required unique">
	
		<?php 
	if(!$model->isNewRecord){ $disable='disabled'; }else{ $disable='enabled'; }	
	echo $form->labelEx($model,'id_channel'); ?>
	<?php echo $form->dropDownList($model, 'id_channel',CHtml::listData(Channels::model()->findAll(), 'id', 'title'),array($disable=>$disable,'empty' => 'Seleccione','class'=>'form-control channel'));?>
	<?php echo $form->error($model,'id_channel'); ?>
	
	</div>

	<div class="form-group col-md-4 required unique">
		<?php 

		if(!$model->isNewRecord){ $disable='disabled'; }else{ $disable='enabled'; }	
		echo $form->labelEx($model,'id_subchannel'); ?>
					<?php 
					if($model->isNewRecord){
						echo $form->dropDownList($model, 'id_subchannel',array(),array('empty' => 'Seleccione','class'=>'form-control subchannel'));						
					}else{
						echo $form->dropDownList($model, 'id_subchannel',CHtml::listData(Subchannels::model()->findAll("id_channel=".$model->id_channel), 'id', 'title'),array($disable=>$disable,'empty' => 'Seleccione','class'=>'form-control'));						
					}
					?>
					<?php echo $form->error($model,'id_subchannel'); ?>
	</div>
	
	<div class="form-group col-md-4 required unique">
		<?php if(!$model->isNewRecord){ $disable='disabled'; }else{ $disable='enabled'; }	
		echo $form->labelEx($model,'id_product'); ?>
		<?php echo $form->dropDownList($model, 'id_product',CHtml::listData(Products::model()->findAll(), 'id', 'title'),array($disable=>$disable,'empty' => 'Seleccione','class'=>'form-control'));?>
		<?php echo $form->error($model,'id_product'); ?>
	</div>
	<div class="col-md-4 form-group">
		<?php echo $form->labelEx($model,'phone'); ?>
		<div class="input-group">
			<?php echo $form->textField($model,'phone',array('size'=>35,'maxlength'=>35,'class'=>'form-control phone','placeholder'=>$model_subchannel->phone)); ?>
			<div class="input-group-addon"><i class="fa fa-phone"></i></div>
		</div>
		<div class="errorMessage"><?php if(isset($errors['phone'])){ echo $errors['phone']; } ?></div>
	</div>
	<div class="col-md-4 form-group">
		<?php echo $form->labelEx($model,'phone_description'); ?>
		<?php echo $form->textField($model,'phone_description',array('size'=>64,'maxlength'=>64,'class'=>'form-control letras','placeholder'=>$model_subchannel->phone_description)); ?>
		<div class="errorMessage"><?php if(isset($errors['phone_description'])){ echo $errors['phone_description']; } ?></div>
	</div>	
	<div class="col-md-4 form-group">
		<?php echo $form->labelEx($model,'office_hours'); ?>
		<?php echo $form->textField($model,'office_hours',array('size'=>64,'maxlength'=>64,'class'=>'form-control letras_numeros_u','placeholder'=>$model_subchannel->office_hours)); ?>
		<?php echo $form->error($model,'office_hours'); ?>
	</div>		
<div class="form-group col-md-6 required ">
	<?php echo $form->labelEx($model,'title'); ?>*
			<?php echo $form->textField($model,'title',array('size'=>65,'maxlength'=>65,'class'=>'form-control letras_numeros')); ?>
							<div class="errorMessage"><?php if(isset($errors['title'])){ echo $errors['title']; } ?></div>
</div>

<div class="form-group col-md-6 required">
	<?php echo $form->labelEx($model,'description'); ?>*
			<?php echo $form->textField($model,'description',array('size'=>65,'maxlength'=>65,'class'=>'form-control letras_numeros')); ?>
							<div class="errorMessage"><?php if(isset($errors['description'])){ echo $errors['description']; } ?></div>
</div>
<div class="form-group col-md-6 required"> 	
<?php echo $form->labelEx($model,'order'); ?>*
		<?php echo $form->dropDownList($model, 'order',CHtml::listData(array(array('id'=>1,'title'=>'Precio'),array('id'=>2,'title'=>'Descuento')), 'id', 'title'),array('empty' => 'Seleccione','class'=>'form-control'));?>
	<div class="errorMessage"><?php if(isset($errors['order'])){ echo $errors['order']; } ?></div>
		</div>
<div class="form-group col-md-12 ">
		<?php echo $form->labelEx($model,'legal'); ?>*
		<?php echo $form->textArea($model,'legal',array('rows'=>6, 'cols'=>50,'class'=>'form-control ckeditor required')); ?>
		<?php echo $form->error($model,'legal'); ?>
	</div>
<div class="form-group col-md-12 ">
		<?php echo $form->labelEx($model,'thankyou_text'); ?>*
		<?php echo $form->textArea($model,'thankyou_text',array('rows'=>6, 'cols'=>50,'class'=>'form-control ckeditor required')); ?>
		<div class="errorMessage"><?php if(isset($errors['thankyou_text'])){ echo $errors['thankyou_text']; } ?></div>
	</div>	
<div class="box-footer">
		<?php
		$title = (Yii::app()->controller->action->id!='view') ? 'Volver sin guardar cambios' : 'Volver';
		echo '<a href="'.Yii::app()->request->baseUrl.'/'.strtolower(Yii::app()->controller->id).'" class="btn btn-secundary" data-toggle="tooltip" title="'.$title.'" >Cerrar</a>';
		if(Yii::app()->controller->action->id!='view'){
			echo CHtml::submitButton('Guardar',array('class'=>'btn btn-primary','data-toggle'=>'tooltip','title'=>'Guardar cambios'));
		}
		?></div>
<?php $this->endWidget(); ?>

</div><!-- form -->
</div><!-- form -->
</div>
</div>

<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/plugins/select2/select2.full.js',CClientScript::POS_END); ?>
<?php Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl.'/js/plugins/select2/select2.css'); ?>
<?php 
	//logica uploades de imagenes
	Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/uploader_image.js',CClientScript::POS_END); 
	//logica uploades de imagenes	
	Yii::app()->clientScript->registerScriptFile('https://cdn.ckeditor.com/4.5.7/standard/ckeditor.js',CClientScript::POS_END);	
	Yii::app()->clientScript->registerCssFile(Yii::app()->params['cdnUrl'].'/plugins/lcswich/lc_switch-tts.css');
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/lcswich/lc_switch.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/lcswich/lc_switch-tts.js',CClientScript::POS_END);

	Yii::app()->clientScript->registerCssFile(Yii::app()->params['cdnUrl'].'/plugins/datepicker/datepicker3.css');
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/datepicker/bootstrap-datepicker.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/input-mask/jquery.inputmask.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/input-mask/jquery.inputmask.date.extensions.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/datepicker/datepicker-tts.js',CClientScript::POS_END);

	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/timepicker/bootstrap-timepicker.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/input-mask/jquery.inputmask.bundle.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerCssFile(Yii::app()->params['cdnUrl'].'/plugins/timepicker/bootstrap-timepicker.css');
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/timepicker/timepicker-tts.js',CClientScript::POS_END);

	//logica uploades de imagenes
	//logica uploades de imagenes
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/js/dropzone.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/js/jquery.bxslider.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerCssFile(Yii::app()->params['cdnUrl'].'/plugins/jQueryUI/jquery-ui.min.css');
	Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/jquery-ui.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerCssFile(Yii::app()->params['cdnUrl'].'/css/jquery.bxslider.css');

	echo '<script>viewPage = "'.Yii::app()->controller->action->id.'";</script>';
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/js/form-validators.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile('//cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js',CClientScript::POS_END);
 ?>



<script>
$( document ).ready(function() {
	<?php if(Yii::app()->controller->action->id=='view'){ echo "$('form').find('input, textarea, button, checkbox, select').attr('disabled','disabled');"; } ?>
	channelsFilter();

	$('.select2').select2();

	if(viewPage=='update'){
		$('.unique input').prop('disabled',true);
		$(".subchannel").prop('disabled',true);
	}

	
	editor = CKEDITOR.replace( 'Offersproduct_legal', {
		language: 'es',
		height: '100px',
		/*uiColor: '#9AB8F3',*/
		toolbar: [    
		{ name: 'clipboard', groups: [ 'clipboard', 'undo' ], items: [ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ] },	
		{ name: 'document', groups: [ 'mode', 'document', 'doctools' ], items: [ 'Source', '-', 'Save', 'NewPage', 'Preview', 'Print', '-', 'Templates' ] },
		{ name: 'links', items: [ 'Link', 'Unlink', 'Anchor' ] },
		{ name: 'insert', items: [ 'Image', 'Flash', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe' ] },	
		{ name: 'editing', groups: [ 'find', 'selection', 'spellchecker' ], items: [ 'Find', 'Replace', '-', 'SelectAll', '-', 'Scayt' ] },
		{ name: 'forms', items: [ 'Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField' ] },	
		'/',
		{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat' ] },
		{ name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl', 'Language' ] },
		{ name: 'styles', items: [ 'Styles', 'Format', 'Font', 'FontSize' ] },
		{ name: 'colors', items: [ 'TextColor', 'BGColor' ] },
		{ name: 'tools', items: [ 'Maximize', 'ShowBlocks' ] }    
		],  
	}); 	
	editor = CKEDITOR.replace( 'Offersproduct_thankyou_text', {
		language: 'es',
		height: '100px',
		/*uiColor: '#9AB8F3',*/
		toolbar: [    
		{ name: 'clipboard', groups: [ 'clipboard', 'undo' ], items: [ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ] },	
		{ name: 'document', groups: [ 'mode', 'document', 'doctools' ], items: [ 'Source', '-', 'Save', 'NewPage', 'Preview', 'Print', '-', 'Templates' ] },
		{ name: 'links', items: [ 'Link', 'Unlink', 'Anchor' ] },
		{ name: 'insert', items: [ 'Image', 'Flash', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe' ] },	
		{ name: 'editing', groups: [ 'find', 'selection', 'spellchecker' ], items: [ 'Find', 'Replace', '-', 'SelectAll', '-', 'Scayt' ] },
		{ name: 'forms', items: [ 'Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField' ] },	
		'/',
		{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat' ] },
		{ name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl', 'Language' ] },
		{ name: 'styles', items: [ 'Styles', 'Format', 'Font', 'FontSize' ] },
		{ name: 'colors', items: [ 'TextColor', 'BGColor' ] },
		{ name: 'tools', items: [ 'Maximize', 'ShowBlocks' ] }    
		],  
	}); 
	
});		
</script>