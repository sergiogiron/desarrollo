<section class="content-header">
	<?php
	echo Yii::app()->user->Title();
	echo Yii::app()->user->Breadcrumb();
	?></section>
<section class="content">
	<div class="box box-danger">
		<div class="box-header with-border">
			<?php $this->renderPartial('_form', array('errors'=>$errors,'model'=>$model,'model_subchannel'=>$model_subchannel)); ?>		</div>
	</div>
</section>


