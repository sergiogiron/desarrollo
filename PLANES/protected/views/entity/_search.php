<?php
/* @var $this EntityController */
/* @var $model Entity */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

<div class="col-md-12" style="clear: both">
	<div class="row">


	

	<div class="col-md-3">
		<div class="row row_space">
		<?php echo $form->label($model,'title'); ?>
		<?php echo $form->textField($model,'title',array('size'=>35,'maxlength'=>45)); ?>
	</div>
	</div>

	

	<div class="col-md-3">
		<div class="row row_space">
		<?php echo $form->label($model,'expire_date_from'); ?><br>
		<?php echo $form->textField($model,'expire_date_from',array('class'=>'form-control datepicker')); ?>
	</div>
	</div>

	<div class="col-md-3">
		<div class="row row_space">
		<?php echo $form->label($model,'expire_date_to'); ?><br>
		<?php echo $form->textField($model,'expire_date_to',array('class'=>'form-control datepicker')); ?>
	</div>
	</div>

	<div class="col-md-3">
		<div class="row row_space">
		<?php echo $form->label($model,'enabled'); ?>
		<?php echo $form->checkBox($model,'enabled',array('class'=>'checkbox_form')); ?>
	</div>
	</div>

	
	</div>
	<div class="row buttons">
		<?php echo CHtml::submitButton('Buscar',array('class'=>'btn btn-default b_customize')); ?>
	</div>
<br />
<?php $this->endWidget(); ?>
<?php Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl.'/plugins/datepicker/datepicker3.css'); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/plugins/datepicker/bootstrap-datepicker.js',CClientScript::POS_END); ?>
</div>
</div><!-- search-form -->
<script>
$( document ).ready(function() {
    $('.datepicker').datepicker({
      autoclose: true, 
	  format: 'yyyy-mm-dd',
	  firstDay: 1
    });
});	
</script>