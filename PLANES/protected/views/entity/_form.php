<?php
/* @var $this EntityController */
/* @var $model Entity */
/* @var $form CActiveForm */
?>


<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'entity-form',
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
	'enableAjaxValidation'=>false,
)); ?>

<span class="right_f active-button">
	<?php echo $form->checkBox($model,'enabled',array('class'=>'lcs_check enabled')); ?>
</span>
<br>
<div class="col-md-12 col-data">
	<div class="row">

	<div class="form-group col-md-12">
	<div class="col-md-4 required">
		<label>Entidad </label><i class="fa fa-key" aria-hidden="true"></i>
		<?php echo $form->textField($model,'title',array('size'=>62,'maxlength'=>62,'class'=>'form-control letras')); ?>
		<?php echo $form->error($model,'title'); ?>
	</div>

	<div class="col-md-3 required">
		<?php echo $form->labelEx($model,'expire_date_from'); ?>
		<div class="input-group">
			<?php echo $form->textField($model,'expire_date_from',array('class'=>'form-control datepicker','data-inputmask'=>'\'alias\': \'dd/mm/yyyy\'','data-mask'=>'data-mask')); ?>
			<div class="input-group-addon pointer"><i class="fa fa-calendar"></i></div>
		</div>
		<?php echo $form->error($model,'expire_date_from'); ?>
	</div>

	<div class="col-md-3 required">
		<?php echo $form->labelEx($model,'expire_date_to'); ?>
		<div class="input-group">
			<?php echo $form->textField($model,'expire_date_to',array('class'=>'form-control datepicker','data-inputmask'=>'\'alias\': \'dd/mm/yyyy\'','data-mask'=>'data-mask')); ?>
			<div class="input-group-addon pointer"><i class="fa fa-calendar"></i></div>
		</div>
		<?php echo $form->error($model,'expire_date_to'); ?>
	</div>
	<div class="col-md-4 form-group col-data">
		<?php echo $form->labelEx($model,'entity_highlight'); ?>
		<?php echo $form->checkBox($model,'entity_highlight',array('class'=>'checkbox_form')); ?>
		<?php echo $form->error($model,'entity_highlight'); ?>
	</div>
	<div id="data-d-content_1">
		<?php
			foreach($model->imagenes as $imagenes){
				if($imagenes->position==1){
					$data=array('name'=>$imagenes->filename,'position'=>$imagenes->position,'ext'=>$imagenes->ext,'title'=>$imagenes->title,'link'=>$imagenes->link,'order'=>$imagenes->order,'coords'=>json_decode($imagenes->coords));
					echo "<input type='hidden' id=".$imagenes->filename." name='image[]' class='element_data' value='".json_encode($data)."'>";
				}
			}
		?>
	</div>

	<?php echo $form->hiddenField($model,'filename'); ?>
	<?php echo $form->hiddenField($model,'ext'); ?>
	<div class="box-footer">
		<?php
			$title = (Yii::app()->controller->action->id!='view') ? 'Volver sin guardar cambios' : 'Volver';
			echo '<a href="'.Yii::app()->request->baseUrl.'/'.strtolower(Yii::app()->controller->id).'" class="btn btn-secundary cancel" data-toggle="tooltip" title="'.$title.'" >Cerrar</a>';
			if(Yii::app()->controller->action->id!='view'){
				echo CHtml::submitButton('Guardar',array('class'=>'btn btn-primary save','data-toggle'=>'tooltip','title'=>'Guardar cambios'));
			}
		?>
	</div>

<?php $this->endWidget(); ?>
	<div class="content_entity_c">
	<div class="col-md-4 f2_custom">
		<?php echo $form->labelEx($model,'filename'); ?> (180 x 60 px)
		<form class="dropzone needsclick dz-clickable" id="dropzone">
			<?php
				if($model->filename!=''){
					echo '<div class="file_uploaded_class" style="display:block;height:107px;"><i class="fa fa-times delete_img_i" onclick="$(\'#Entity_filename\').val(\'\');$(\'#Entity_ext\').val(\'\');$(\'.file_uploaded_class\').html(\'\');$(\'.file_uploaded_class\').css(\'display\',\'none\');"></i><img src="'.Yii::app()->request->baseUrl.'/uploads/tmp/'.Yii::app()->user->id.'/'.$model->filename.'/'.$model->filename.'.'.$model->ext.'" class="center_image" height="100%"/></div>';
				}else{
					echo '<div class="file_uploaded_class" style="height:107px;"></div>';
				}
			?>
		  <div class="dz-message needsclick">
			Arrastra los archivos aca o clickea para subir un archivo.<br>
			<span class="note needsclick">(No hay ningun archivo seleccionado.)</span>
		  </div>
		</form>
		<?php echo $form->error($model,'filename'); ?>
	</div>
	<div class="col-md-12">
		<label class="label_image">Imagen de banners</label>
		<div class="col-md-1">
			<div class="more_img action_img action_img" data-position="1" title="Agregar imagen" data-toggle="tooltip">+</div>
		</div>
		<div class="col-md-11 content-images">
			<ul class="slider1" id="slider_1">
				<?php
					foreach($model->imagenes as $imagenes){
						if($imagenes->position==1){
							echo "<li id='".$imagenes->filename."_slide'  title='Titulo:".$imagenes->title." Enlace:".$imagenes->link."' class='bslide_1 slide ui-state-default ui-sortable-handle'><i class='fa fa-edit edit_img_i' data-position=".$imagenes->position." data-id=".$imagenes->filename." onclick='edit_element($(this))'></i><i class='fa fa-times delete_img_i' data-id=".$imagenes->filename."   data-position=".$imagenes->position." onclick='delete_element($(this))'></i><img src='".Yii::app()->request->baseUrl."/uploads/tmp/".Yii::app()->user->id."/".$imagenes->filename."/".$imagenes->filename.".".$imagenes->ext."'></li>";
						}
					}
				?>
			</ul>
		</div>
	</div>
	</div>
	<a href="#" class="btn btn-primary" id="add_type_account" style="display: block;float: left;">Añadir tipo de cuenta</a>
	<table id="example2" class="table table-bordered table-striped  table-list">
        <thead>
            <tr>
                <th>Tarjeta de crédito</th>
                <th>Tipo de cuenta</th>
                <th></th>
                <th></th>
            </tr>
        </thead>
        <tbody id="result2">
			<?php
			$accounttype_value='[]';
			if(count($model->accounttype)>0){
				$accounttype_value=array();
				foreach($model->accounttype as $accounttype){
					$accounttype_value[]= (object) array(
						'id'=>$accounttype->id,
						'id_new'=>$accounttype->id,
						'id_credit_cards'=>$accounttype->id_credit_cards,
						'credit_cards_name' =>$accounttype->credito->name,
						'account_type' =>$accounttype->account_type,
					  );
					echo '<tr><td>'.$accounttype->credito->name.'</td><td>'.$accounttype->account_type.'</td><td><a href="#" id="'.$accounttype->id.'" class="edit_credit_cards2" data-toggle="tooltip" data-original-title="Editar"><i class="fa fa-edit icon_cbutton"></i></a></td><td><a href="#" id="'.$accounttype->id.'" class="delete_credit_cards2" data-toggle="tooltip" data-original-title="Borrar"><i class="fa fa-trash-o icon_cbutton"></i></a></td></tr>';
				}
				$accounttype_value=json_encode($accounttype_value);
			}
			?>
        </tbody>
    </table>
	<input type='hidden' id='accounttype' name='accounttype' value='<?=$accounttype_value?>'/>
	<a href="#" class="btn btn-primary" id="add_financial" style="display: block;float: left;margin-top: 25px;">Añadir financiación</a>
	<table id="example" class="table table-bordered table-striped  table-list">
        <thead>
            <tr>
                <th>Tarjetas</th>
                <th>Cantidad de cuotas</th>
                <th>Interés</th>
								<th>Porcentaje Posnet</th>
                <th>Texto CFTNA</th>
								<th>Productos</th>
								<th>Aplica a Sitios</th>
                <th>Vigencia desde</th>
                <th>Vigencia hasta</th>
                <th></th>
                <th></th>
            </tr>
        </thead>
        <tbody id="result">
			<?php
			$financing_value='[]';
			if(count($model->financing)>0){
				$financing_value=array();
				foreach($model->financing as $financing){
					$financing_selected=array();
					$financing_list='';
					foreach($financing->financing as $financing_select){
						$financing_selected[]=(object) array('id'=>$financing_select->id_payments_entity_account_type,'title'=>$financing_select->accountype->credito->name.' '.$financing_select->accountype->account_type);
						$financing_list.=$financing_select->accountype->credito->name.' '.$financing_select->accountype->account_type.'-';
					}
					$services_selected=array();
					$services_list='';
					foreach($financing->services as $service_select){
						$services_selected[]=(object) array('id'=>$service_select->id_product,'title'=>$service_select->product->title);
						$services_list.=$service_select->product->title.'-';
					}
					if(!isset($financing->rewards)){
						$rewards = 'Todos los sitios';
					}else if($financing->rewards==1){
						$rewards = 'Sitios Con Puntos';
					}else{
						$rewards = 'Sitios Con Cash';
					}
					$financing_value[]= (object) array(
						'id'=>$financing->id,
						'account_type' =>$financing_selected,
						'services' =>$services_selected,
						'amount_dues' => $financing->amount_dues,
						'interests' => $financing->interests,
						'cftna' => $financing->cftna,
						'posnet_cost' => $financing->posnet_cost,
						'rewards' => $financing->rewards,
						'legal' => $financing->legal,
						'date_validity_start' => $financing->date_validity_start,
						'date_validity_end' => $financing->date_validity_end,
					  );
					  if(substr($services_list,0,-1)==''){
						  $service_list='Todos los productos';
					  }else{
						  $service_list=substr($services_list,0,-1);
					  }
					echo '<tr><td>'.substr($financing_list,0,-1).'</td><td>'.$financing->amount_dues.'</td><td>'.$financing->interests.'</td><td>'.$financing->posnet_cost.'</td><td>'.$financing->cftna.'</td><td>'.$service_list.'</td><td>'.$rewards.'</td><td>'.$financing->date_validity_start.'</td><td>'.$financing->date_validity_end.'</td><td><a href="#" id="'.$financing->id.'" class="edit_credit_cards" data-toggle="tooltip" data-original-title="Editar"><i class="fa fa-edit icon_cbutton"></i></a></td><td><a href="#" id="'.$financing->id.'" class="delete_credit_cards" data-toggle="tooltip" data-original-title="Borrar"><i class="fa fa-trash-o icon_cbutton"></i></a></td></tr>';
				}
				$financing_value=json_encode($financing_value);
			}
			?>
        </tbody>
    </table>
	<input type='hidden' id='financing' name='financing' value='<?=$financing_value?>'/>
	<a href="#" class="btn btn-primary" id="add_airliners" style="display: block;float: left;margin-top: 25px;">Añadir compañia aerea</a>
	<table id="example3" class="table table-bordered table-striped  table-list">
        <thead>
            <tr>
                <th>Aerolíneas</th>
                <th>Cantidad de cuotas</th>
                <th>Interés</th>
                <th>Texto CFTNA</th>
								<th>Porcentaje Posnet</th>
								<th>Aplica a Sitios</th>
                <th>Tarjetas</th>
                <th>Vigencia desde</th>
                <th>Vigencia hasta</th>
                <th></th>
                <th></th>
            </tr>
        </thead>
        <tbody id="result3">
			<?php
			$airliners_value='[]';
			if(count($model->airliners)>0){
				$airliners_value=array();
				foreach($model->airliners as $airliners){
					$airliners_selected=array();
					$airliners_list='';
					foreach($airliners->airliners as $airliners_select){
						$airliners_selected[]=(object) array('id'=>$airliners_select->id_airliner,'title'=>$airliners_select->airliners->title);
						$airliners_list.=$airliners_select->airliners->title.'-';
					}
					$services_selected=array();
					$services_list='';
					foreach($airliners->services as $service_select){
						$services_selected[]=(object) array('id'=>$service_select->id_payments_entity_account_type,'title'=>$service_select->accountype->credito->name.' '.$service_select->accountype->account_type);
						$services_list.=$service_select->accountype->credito->name.' '.$service_select->accountype->account_type.'-';
					}
					if(!isset($airliners->rewards)){
						$rewards = 'Todos los sitios';
					}else if($airliners->rewards==1){
						$rewards = 'Sitios Con Puntos';
					}else{
						$rewards = 'Sitios Con Cash';
					}
					$airliners_value[]= (object) array(
						'id'=>$airliners->id,
						'airliners' =>$airliners_selected,
						'amount_dues' => $airliners->amount_dues,
						'interests' => $airliners->interests,
						'cftna' => $airliners->cftna,
						'rewards' => $rewards,
						'posnet_cost' => $airliners->posnet_cost,
						'account_type' =>$services_selected,
						'legal' => $airliners->legal,
						'date_validity_start' => $airliners->date_validity_start,
						'date_validity_end' => $airliners->date_validity_end,
					  );
					  if(substr($services_list,0,-1)==''){
						  $services_list='Todos las tarjetas';
					  }else{
						  $services_list=substr($services_list,0,-1);
					  }
					echo '<tr><td>'.substr($airliners_list,0,-1).'</td><td>'.$airliners->amount_dues.'</td><td>'.$airliners->interests.'</td><td>'.$airliners->cftna.'</td><td>'.$airliners->posnet_cost.'</td><td>'.$rewards.'</td><td>'.$services_list.'</td><td>'.$airliners->date_validity_start.'</td><td>'.$airliners->date_validity_end.'</td><td><a href="#" id="'.$airliners->id.'" class="edit_airliners" data-toggle="tooltip" data-original-title="Editar"><i class="fa fa-edit icon_cbutton"></i></a></td><td><a href="#" id="'.$airliners->id.'" class="delete_airliners" data-toggle="tooltip" data-original-title="Borrar"><i class="fa fa-trash-o icon_cbutton"></i></a></td></tr>';
				}
				$airliners_value=json_encode($airliners_value);
			}
			?>
        </tbody>
    </table>
	<input type='hidden' id='airliners' name='airliners' value='<?=$airliners_value?>'/>
	<a href="#" class="btn btn-primary" id="add_cruisessupplier" style="display: block;float: left;margin-top: 25px;">Añadir naviera</a>
	<table id="example4" class="table table-bordered table-striped  table-list">
        <thead>
            <tr>
                <th>Navieras</th>
                <th>Cantidad de cuotas</th>
                <th>Interés</th>
                <th>Texto CFTNA</th>
								<th>Porcentaje Posnet</th>
								<th>Aplica a Sitios</th>
                <th>Tarjetas</th>
                <th>Vigencia desde</th>
                <th>Vigencia hasta</th>
                <th></th>
                <th></th>
            </tr>
        </thead>
        <tbody id="result4">
			<?php
			$cruisessupplier_value='[]';
			if(count($model->cruisessupplier)>0){
				$cruisessupplier_value=array();
				foreach($model->cruisessupplier as $cruisessupplier){
					$cruisessupplier_selected=array();
					$cruisessupplier_list='';
					foreach($cruisessupplier->cruisessupplier as $cruisessupplier_select){
						$cruisessupplier_selected[]=(object) array('id'=>$cruisessupplier_select->id_cruises_supplier,'title'=>$cruisessupplier_select->cruisessupplier->title);
						$cruisessupplier_list.=$cruisessupplier_select->cruisessupplier->title.'-';
					}
					$services_selected=array();
					$services_list='';
					foreach($cruisessupplier->services as $service_select){
						$services_selected[]=(object) array('id'=>$service_select->id_payments_entity_account_type,'title'=>$service_select->accountype->credito->name.' '.$service_select->accountype->account_type);
						$services_list.=$service_select->accountype->credito->name.' '.$service_select->accountype->account_type.'-';
					}
					if(!isset($cruisessupplier->rewards)){
						$rewards = 'Todos los sitios';
					}else if($cruisessupplier->rewards==1){
						$rewards = 'Sitios Con Puntos';
					}else{
						$rewards = 'Sitios Con Cash';
					}
					$cruisessupplier_value[]= (object) array(
						'id'=>$cruisessupplier->id,
						'cruisessupplier' =>$cruisessupplier_selected,
						'amount_dues' => $cruisessupplier->amount_dues,
						'interests' => $cruisessupplier->interests,
						'cftna' => $cruisessupplier->cftna,
						'rewards' => $rewards,
						'posnet_cost' => $cruisessupplier->posnet_cost,
						'account_type' =>$services_selected,
						'legal' => $cruisessupplier->legal,
						'date_validity_start' => $cruisessupplier->date_validity_start,
						'date_validity_end' => $cruisessupplier->date_validity_end,
					  );
					  if(substr($services_list,0,-1)==''){
						  $services_list='Todos las tarjetas';
					  }else{
						  $services_list=substr($services_list,0,-1);
					  }
					echo '<tr><td>'.substr($cruisessupplier_list,0,-1).'</td><td>'.$cruisessupplier->amount_dues.'</td><td>'.$cruisessupplier->interests.'</td><td>'.$cruisessupplier->cftna.'</td><td>'.$cruisessupplier->posnet_cost.'</td><td>'.$rewards.'</td><td>'.$services_list.'</td><td>'.$cruisessupplier->date_validity_start.'</td><td>'.$cruisessupplier->date_validity_end.'</td><td><a href="#" id="'.$cruisessupplier->id.'" class="edit_cruisessupplier" data-toggle="tooltip" data-original-title="Editar"><i class="fa fa-edit icon_cbutton"></i></a></td><td><a href="#" id="'.$cruisessupplier->id.'" class="delete_cruisessupplier" data-toggle="tooltip" data-original-title="Borrar"><i class="fa fa-trash-o icon_cbutton"></i></a></td></tr>';
				}
				$cruisessupplier_value=json_encode($cruisessupplier_value);
			}
			?>
        </tbody>
    </table>
	<input type='hidden' id='cruisessupplier' name='cruisessupplier' value='<?=$cruisessupplier_value?>'/>
	</div>
  </div>
  </div>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
	<?php
	Yii::app()->clientScript->registerCssFile(Yii::app()->params['cdnUrl'].'/css/dropzone.css');
	Yii::app()->clientScript->registerCssFile(Yii::app()->params['cdnUrl'].'/css/dropzone_one.css');
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/js/dropzone.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerCssFile(Yii::app()->params['cdnUrl'].'/plugins/datepicker/datepicker3.css');
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/datepicker/bootstrap-datepicker.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/input-mask/jquery.inputmask.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/input-mask/jquery.inputmask.date.extensions.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/datepicker/datepicker-tts.js',CClientScript::POS_END);	
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/input-mask/jquery.inputmask.numeric.extensions.js',CClientScript::POS_END);
	?>
<script>
id_menu=<?=Yii::app()->user->id_menu()?>;
menu='<?=strtolower(Yii::app()->controller->id)?>';
eval("var cantidad_images_"+1+" = "+$('#data-d-content_1 :input').length);
eval("var limit_images_"+1+" = '10'");

$( ".delete_credit_cards" ).on( "click", function(event) {
	event.preventDefault();
	var id=$(this).attr('id');
	var financing=JSON.parse($('#financing').val());
	var financing_new=[];
	$.each( financing, function( key, value ) {
		if(value.id!=id){
			financing_new.push(value);
		}
	});
	$('#financing').val(JSON.stringify(financing_new));
	$(this).parent().parent().remove();
});
$( ".edit_credit_cards" ).on( "click", function(event) {
	event.preventDefault();
	var id=$(this).attr('id');
	var financing_edit;
	var financing=JSON.parse($('#financing').val());
	$.each( financing, function( key, value ) {
		if(value.id==id){
			financing_edit=value;
		}
	});
	var accounttype=$('#accounttype').val();
	financing_edit.select=accounttype;
	var request = $.ajax({
		url: homeurl+'/entity/financingedit',
		method: "POST",
		data: financing_edit,
	}).done(function(msg) {
		$('.ligth-box-modal').css('display','block');
		$('.modal-title').html('Financiación');
		$('.modal-content').css('display','block');
		$('.modal-body').html(msg);
		$('.modal-footer').html('<a href="#" onclick="refresh_datos(event);" class="btn btn-primary">Actualizar</a>');
	});
});
$( ".edit_cruisessupplier" ).on( "click", function(event) {
	event.preventDefault();
	var id=$(this).attr('id');
	var cruisessupplier_edit;
	var cruisessupplier=JSON.parse($('#cruisessupplier').val());
	$.each( cruisessupplier, function( key, value ) {
		if(value.id==id){
			cruisessupplier_edit=value;
		}
	});
	var accounttype=$('#accounttype').val();
	cruisessupplier_edit.select=accounttype;
	var request = $.ajax({
		url: homeurl+'/entity/cruisessupplieredit',
		method: "POST",
		data: cruisessupplier_edit,
	}).done(function(msg) {
		$('.ligth-box-modal').css('display','block');
		$('.modal-title').html('Aerolíneas');
		$('.modal-content').css('display','block');
		$('.modal-body').html(msg);
		$('.modal-footer').html('<a href="#" onclick="refresh_datos(event);" class="btn btn-primary">Actualizar</a>');
	});
});
$( ".delete_cruisessupplier" ).on( "click", function(event) {
	event.preventDefault();
	var id=$(this).attr('id');
	var cruisessupplier=JSON.parse($('#cruisessupplier').val());
	var cruisessupplier_new=[];
	$.each( cruisessupplier, function( key, value ) {
		if(value.id!=id){
			cruisessupplier_new.push(value);
		}
	});
	$('#cruisessupplier').val(JSON.stringify(cruisessupplier_new));
	$(this).parent().parent().remove();
});
$( ".edit_airliners" ).on( "click", function(event) {
	event.preventDefault();
	var id=$(this).attr('id');
	var airliners_edit;
	var airliners=JSON.parse($('#airliners').val());
	$.each( airliners, function( key, value ) {
		if(value.id==id){
			airliners_edit=value;
		}
	});
	var accounttype=$('#accounttype').val();
	airliners_edit.select=accounttype;
	var request = $.ajax({
		url: homeurl+'/entity/airlinersedit',
		method: "POST",
		data: airliners_edit,
	}).done(function(msg) {
		$('.ligth-box-modal').css('display','block');
		$('.modal-title').html('Aerolíneas');
		$('.modal-content').css('display','block');
		$('.modal-body').html(msg);
		$('.modal-footer').html('<a href="#" onclick="refresh_datos(event);" class="btn btn-primary">Actualizar</a>');
	});
});
$( ".delete_airliners" ).on( "click", function(event) {
	event.preventDefault();
	var id=$(this).attr('id');
	var airliners=JSON.parse($('#airliners').val());
	var airliners_new=[];
	$.each( airliners, function( key, value ) {
		if(value.id!=id){
			airliners_new.push(value);
		}
	});
	$('#airliners').val(JSON.stringify(airliners_new));
	$(this).parent().parent().remove();
});
$( ".delete_credit_cards2" ).on( "click", function(event) {
	event.preventDefault();
	var id=$(this).attr('id');
	var accounttype=JSON.parse($('#accounttype').val());
	var accounttype_new=[];
	$.each( accounttype, function( key, value ) {
		if(value.id!=id){
			accounttype_new.push(value);
		}
	});
	$('#accounttype').val(JSON.stringify(accounttype_new));
	$(this).parent().parent().remove();
});
$( ".edit_credit_cards2" ).on( "click", function(event) {
	event.preventDefault();
	var id=$(this).attr('id');
	var accounttype_edit;
	var accounttype=JSON.parse($('#accounttype').val());
	$.each( accounttype, function( key, value ) {
		if(value.id==id){
			accounttype_edit=value;
		}
	});
	var request = $.ajax({
		url: homeurl+'/entity/typeaccountedit',
		method: "POST",
		data: accounttype_edit,
	}).done(function(msg) {
		$('.ligth-box-modal').css('display','block');
		$('.modal-title').html('Tipo de cuenta');
		$('.modal-content').css('display','block');
		$('.modal-body').html(msg);
		$('.modal-footer').html('<a href="#" onclick="refresh_datos(event);" class="btn btn-primary">Actualizar</a>');
	});
});
$( document ).ready(function() {
	 $('#add_financial').click(function(event){
		event.preventDefault();
		var accounttype=$('#accounttype').val();
		var request = $.ajax({
			url: homeurl+'/entity/financing',
			method: "POST",
			data:{data:accounttype},
		}).done(function(msg) {
			$('.ligth-box-modal').css('display','block');
			$('.modal-title').html('Financiación');
			$('.modal-content').css('display','block');
			$('.modal-body').html(msg);
			$('.modal-footer').html('<a href="#" onclick="refresh_datos(event);" class="btn btn-primary">Actualizar</a>');
		});
	});
	 $('#add_airliners').click(function(event){
		event.preventDefault();
		var accounttype=$('#accounttype').val();
		var request = $.ajax({
			url: homeurl+'/entity/airliners',
			method: "POST",
			data:{data:accounttype},
		}).done(function(msg) {
			$('.ligth-box-modal').css('display','block');
			$('.modal-title').html('Aerolíneas');
			$('.modal-content').css('display','block');
			$('.modal-body').html(msg);
			$('.modal-footer').html('<a href="#" onclick="refresh_datos(event);" class="btn btn-primary">Actualizar</a>');
		});
	});
	 $('#add_cruisessupplier').click(function(event){
		event.preventDefault();
		var accounttype=$('#accounttype').val();
		var request = $.ajax({
			url: homeurl+'/entity/cruisessupplier',
			method: "POST",
			data:{data:accounttype},
		}).done(function(msg) {
			$('.ligth-box-modal').css('display','block');
			$('.modal-title').html('Aerolíneas');
			$('.modal-body').html(msg);
			$('.modal-footer').html('<a href="#" onclick="refresh_datos(event);" class="btn btn-primary">Actualizar</a>');
		});
	});
	 $('#add_type_account').click(function(event){
		event.preventDefault();
		var request = $.ajax({
			url: homeurl+'/entity/typeaccount',
		}).done(function(msg) {
			$('.ligth-box-modal').css('display','block');
			$('.modal-title').html('Tipo de cuenta');
			$('.modal-body').html(msg);
			$('.modal-footer').html('<a href="#" onclick="refresh_datos(event);" class="btn btn-primary">Actualizar</a>');
		});
	});
	$('#example3').dataTable({
		"bPaginate": false,
		"bLengthChange": false,
		"bFilter": true,
		"bInfo": false,
		"bAutoWidth": false,
		"language": {
			"sSearch": "Buscar:",
		}
	});
	$('#example4').dataTable({
		"bPaginate": false,
		"bLengthChange": false,
		"bFilter": true,
		"bInfo": false,
		"bAutoWidth": false,
		"language": {
			"sSearch": "Buscar:",
		}
	});
	$('#example').dataTable({
		"bPaginate": false,
		"bLengthChange": false,
		"bFilter": true,
		"bInfo": false,
		"bAutoWidth": false,
		"language": {
			"sSearch": "Buscar:",
		}
	});
	$('#example2').dataTable({
		"bPaginate": false,
		"bLengthChange": false,
		"bFilter": true,
		"bInfo": false,
		"bAutoWidth": false,
		"language": {
			"sSearch": "Buscar:",
		}
	});
	<?php
		if(Yii::app()->controller->action->id=='view'){ echo "$('form').find('input, textarea, checkbox, select').attr('disabled','disabled');"; }
	?>
	Dropzone.autoDiscover = false;
	$("#dropzone").dropzone({
		url: homeurl+"/configimages/uploadone/1",
		maxFilesize: 100,
		uploadMultiple:false,
		maxFiles:100,
		previewsContainer:false,
		paramName: "file",
		maxThumbnailFilesize: 5,
		init: function() {
		 this.on('success', function(file, responseText) {
			result=JSON.parse(responseText);
				$('#Entity_filename').val(result[0].name);
				$('#Entity_ext').val(result[0].ext);
				$('.file_uploaded_class').css('display','block');
				$('.file_uploaded_class').html('<i class="fa fa-times delete_img_i" onclick="$(\'#Entity_filename\').val(\'\');$(\'#Entity_ext\').val(\'\');$(\'.file_uploaded_class\').html(\'\');$(\'.file_uploaded_class\').css(\'display\',\'none\');"></i><img src="'+homeurl+'/'+result[0].url+'" class="center_image" height="100%"/>');
		  });
		}
	});
    $('.datepicker').datepicker({
      autoclose: true,
	  format: 'dd-mm-yyyy',
	  firstDay: 1
    });

    $('.fa-upload').click(function(){
		$("#file").click();
	});
	$('#trigger').click(function(){
		$("#file").click();
	});
	$("#file").change(function(){
		$('#trigger').text('Archivo Elegido');
	});
});
</script>
</script>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/plugins/select2/select2.full.js',CClientScript::POS_END); ?>
<?php Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl.'/js/plugins/select2/select2.css'); ?>
<?php
	//logica uploades de imagenes
	Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/uploader_image.js',CClientScript::POS_END);
	//logica uploades de imagenes
	Yii::app()->clientScript->registerScriptFile('https://cdn.ckeditor.com/4.5.7/standard/ckeditor.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerCssFile(Yii::app()->params['cdnUrl'].'/plugins/lcswich/lc_switch-tts.css');
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/lcswich/lc_switch.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/lcswich/lc_switch-tts.js',CClientScript::POS_END);

	Yii::app()->clientScript->registerCssFile(Yii::app()->params['cdnUrl'].'/plugins/datepicker/datepicker3.css');
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/datepicker/bootstrap-datepicker.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/input-mask/jquery.inputmask.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/input-mask/jquery.inputmask.date.extensions.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/datepicker/datepicker-tts.js',CClientScript::POS_END);

	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/timepicker/bootstrap-timepicker.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/input-mask/jquery.inputmask.bundle.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerCssFile(Yii::app()->params['cdnUrl'].'/plugins/timepicker/bootstrap-timepicker.css');
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/timepicker/timepicker-tts.js',CClientScript::POS_END);

	//logica uploades de imagenes
	//logica uploades de imagenes
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/js/dropzone.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/js/jquery.bxslider.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerCssFile(Yii::app()->params['cdnUrl'].'/plugins/jQueryUI/jquery-ui.min.css');
	Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/jquery-ui.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerCssFile(Yii::app()->params['cdnUrl'].'/css/jquery.bxslider.css');

	echo '<script>viewPage = "'.Yii::app()->controller->action->id.'";</script>';
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/js/form-validators.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile('//cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js',CClientScript::POS_END);
 ?>
<script>
$( document ).ready(function() {
	$('.select2').select2().on('select2:unselect', function(e) {
		$('.select2-selection').trigger('click');
	});
	<?php if(Yii::app()->controller->action->id=='view'){ echo "$('form').find('input, textarea, button, checkbox, select').attr('disabled','disabled');"; } ?>
	$('[data-mask]').inputmask('dd/mm/yyyy', {'placeholder': 'dd/mm/aaaa'});
	$('.fa-calendar').datepicker().on('changeDate', function(e) {
		$(this).parent().prev().val(e.format('dd/mm/yyyy'));
	    $(this).datepicker('hide');
    });
	$('.timepicker').timepicker({
      showInputs: false,
	  showMeridian:false
    });
	$('.timepicker').inputmask(
        'hh:mm',
        {
            mask: 'h:s',
            placeholder: 'hh:mm',
            alias: 'datetime',
			insertMode:false,
        }
     );
	$('.fa-clock-o').on('click', function() {
		$(this).parent().prev().trigger('click');
    });
    
});
</script>
