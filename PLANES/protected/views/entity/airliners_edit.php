<form id="airliners_form">
<div style="color:red;display:none;" id="required">Debe completar todos los campos.<br/></div>
<div class="row">
<div class="col-md-3 required">
<label>Aerolíneas <span class="required">*</span></label>
<?php
echo CHtml::dropDownList('id_airliner',CHtml::listData($data['airliners'],'id','id'),CHtml::listData(Airliners::model()->findAll(),'id','title'), array('class'=>'form-control select2','multiple'=>'multiple','data-placeholder'=>'Seleccione','prompt'=>'Seleccione','style'=>'width:100%')); ?>

</div>
<div class="col-md-3 required">
		<label>Cantidad de cuotas <span class="required">*</span></label>
		<input size="62" maxlength="62" class="form-control cuotas" name="amount_dues" id="amount_dues" type="text" value="<?=$data['amount_dues']?>">
		<input name="id" id="id" type="hidden" value="<?=$data['id']?>">
</div>
<div class="col-md-3 required">
		<label>Interés <span class="required">*</span></label>
		<input size="62" maxlength="3" class="form-control" name="interests" id="interests" type="text" value="<?=$data['interests']?>">
</div>
<div class="col-md-3 required">
		<label>Texto CFTNA <span class="required">*</span></label>
		<input size="62" maxlength="62" class="form-control" name="cftna" id="cftna" type="text" value="<?=$data['cftna']?>">
</div>
<div class="col-md-3 required">
		<label>Porcentaje Posnet <span class="required">*</span></label>
		<input size="62" maxlength="62" class="form-control" name="posnet_cost" id="posnet_cost" type="text" value="<?=$data['posnet_cost']?>">
</div>
<div class="col-md-3 required">
	<label for="Entity_expire_date_from" class="required">Vigencia desde <span class="required">*</span></label>
	<div class="input-group">
		<input class="form-control datepicker" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="data-mask" name="date_validity_start" id="date_validity_start" type="text" value="<?=$data['date_validity_start']?>">
		<div class="input-group-addon pointer"><i class="fa fa-calendar"></i></div>
	</div>
</div>
<div class="col-md-3 required">
	<label for="Entity_expire_date_from" class="required">Vigencia hasta <span class="required">*</span></label>
	<div class="input-group">
		<input class="form-control datepicker" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="data-mask" name="date_validity_end" id="date_validity_end" type="text"  value="<?=$data['date_validity_end']?>">
		<div class="input-group-addon pointer"><i class="fa fa-calendar"></i></div>
	</div>
</div>
<div class="col-md-3 ">
		<label>Tipo de cuenta </label>
		<?php
		if(isset($data['account_type'])){
			$account_type=CHtml::listData($data['account_type'],'id','id');
		}else{
			$account_type=array();
		}
		echo CHtml::dropDownList('account_type',$account_type,CHtml::listData($select_accounts,'id','title'), array('class'=>'form-control select2','data-placeholder'=>'Todas las tarjetas','multiple'=>'multiple','style'=>'width:100%')); ?>
</div>
<div class="col-md-6">
	<label>Aplica a :</label>
	<select name="rewards" id="rewardsA" class="form-control">
		<?php if(empty($data['rewards'])):?>
			<option value="" selected>Todos los sitios</option>
			<option value="1" >Solo sitios con puntos</option>
			<option value="0" >Solo sitios Cash</option>
		<?php elseif($data['rewards']==1):?>
			<option value="" >Todos los sitios</option>
			<option value="1" selected>Solo sitios con puntos</option>
			<option value="0" >Solo sitios Cash</option>
		<?php elseif($data['rewards']==0): ?>
			<option value="" >Todos los sitios</option>
			<option value="1" >Solo sitios con puntos</option>
			<option value="0" selected>Solo sitios Cash</option>
		<?php endif;?>
	</select>
</div>
<div class="col-md-12 required">
		<label>Legal <span class="required">*</span></label>
		<textarea class="ckeditor" name="legal" id="legal"><?=$data['legal']?></textarea>
</div>
</div>
</form>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/plugins/select2/select2.full.js',CClientScript::POS_END); ?>
<?php Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl.'/js/plugins/select2/select2.css'); ?>
<?php
	echo '<script>viewPage = "'.Yii::app()->controller->action->id.'";</script>';
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/js/form-validators.js',CClientScript::POS_END);
?>
<script>
function refresh_datos(event){
	event.preventDefault();
	var error=0;
	$('#airliners_form :input').each( function () {
		if($(this).attr('type')!='search'){
			if($('#id_airliner').select2('data').length==0){
				error=1;
			}
			if ($(this).val()== '' ){
				if($(this).attr('id')=='legal'){
					if(editor.getData()== '' ){
						error=1;
					}
				}else{
					error=1;
				}
			}
		}
	});
	if($("#rewardsA").val() == ""){

		error = 0;
	}
	if(error>0){
		$('#required').css('display','block');
	}else{
		var formdata = $("#airliners_form").serializeArray();
		var data = {};
		$(formdata).each(function(index, obj){
			if(obj.name!='id_airliner[]' && obj.name!='account_type[]'){
				data[obj.name] = obj.value;
			}
		});
		data.legal=editor.getData();
		id_airliner_array=[];
		$.each( $('#id_airliner').select2('data'), function( key, value ) {
			id_airliner_object=new Object;
			id_airliner_object.id=value.id;
			id_airliner_object.title=value.text;
			id_airliner_array.push(id_airliner_object);
		});
		data.airliners=id_airliner_array;
		id_services_array=[];
		$.each( $('#account_type').select2('data'), function( key, value ) {
			id_services_object=new Object;
			id_services_object.id=value.id;
			id_services_object.title=value.text;
			id_services_array.push(id_services_object);
		});
		data.account_type=id_services_array;
		var airliners=JSON.parse($('#airliners').val());
		var airliners_new=[];
		$.each( airliners, function( key, value ) {
			if(value.id!=data.id){
				airliners_new.push(value);
			}else{
				airliners_new.push(data);
			}
		});
		$('#airliners').val(JSON.stringify(airliners_new));

		$('#example3').dataTable().fnDestroy();
		$('#result3').html('');
		$.each( airliners_new, function( key, value ) {
				airliner_text='';
				$.each( value.airliners, function( key2, value2 ) {
					airliner_text+=value2.title+'-';
				});
				services_text='';
				$.each( value.account_type, function( key2, value2 ) {
					services_text+=value2.title+'-';
				});
				if(services_text.substr(0,services_text.length-1)==''){
					services_text='Todos los servicios';
				}else{
					services_text=services_text.substr(0,services_text.length-1);
				}var rewards;
				if(value.rewards ==1){
					rewards = 'Solo sitios con puntos';
				}else{
					rewards = 'Solo sitios cash';
				}
				if(value.rewards == ""){
					rewards = "Todos los sitios";
				}
				$('#result3').append('<tr><td>'+ airliner_text.substr(0,airliner_text.length-1)+'</td><td>'+value.amount_dues+'</td><td>'+value.interests+'</td><td>'+value.cftna+'</td><td>'+value.posnet_cost+'</td><td>'+rewards+'</td><td>'+services_text+'</td><td>'+value.date_validity_start+'</td><td>'+value.date_validity_end+'</td><td><a href="#" id="'+ value.id+'" class="edit_airliners"  data-toggle="tooltip" data-original-title="Editar"><i class="fa fa-edit icon_cbutton"></i></a></td><td><a href="#" id="'+ value.id+'" class="delete_airliners" data-toggle="tooltip" data-original-title="Borrar"><i class="fa fa-trash-o icon_cbutton"></i></a></td></tr>');
			});
		datatable=$('#example3').dataTable({
			"bPaginate": false,
			"bLengthChange": false,
			"bFilter": true,
			"bInfo": false,
			"bAutoWidth": false,
			"language": {
				"sSearch": "Buscar:",
			}
		});
		$( ".edit_airliners" ).on( "click", function(event) {
			event.preventDefault();
			var id=$(this).attr('id');
			var airliners_edit;
			var airliners=JSON.parse($('#airliners').val());
			$.each( airliners, function( key, value ) {
				if(value.id==id){
					airliners_edit=value;
				}
			});
			var accounttype=$('#accounttype').val();
			airliners_edit.select=accounttype;
			var request = $.ajax({
				url: homeurl+'/entity/airlinersedit',
				method: "POST",
				data: airliners_edit,
			}).done(function(msg) {
				$('.ligth-box-modal').css('display','block');
				$('.modal-title').html('Aerolíneas');
				$('.modal-body').html(msg);
				$('.modal-footer').html('<a href="#" onclick="refresh_datos(event);" class="btn btn-primary">Actualizar</a>');
			});
		});
		$( ".delete_airliners" ).on( "click", function(event) {
			event.preventDefault();
			var id=$(this).attr('id');
			var airliners=JSON.parse($('#airliners').val());
			var airliners_new=[];
			$.each( airliners, function( key, value ) {
				if(value.id!=id){
					airliners_new.push(value);
				}
			});
			$('#airliners').val(JSON.stringify(airliners_new));
			$(this).parent().parent().remove();
		});
		$('.close_window').trigger('click');
	}
}
$( document ).ready(function() {
	$('#id_airliner').select2().on('select2:unselect', function(e) {
		$(this).next().find('.select2-selection').trigger('click');
	});
	$('#account_type').select2().on('select2:unselect', function(e) {
		$(this).next().find('.select2-selection').trigger('click');
	});
	$(".cuotas").keypress(function (key) {
		/*if(key.charCode == 45 || key.charCode == 44){
			if($(".cuotas").val().indexOf(',') != -1){
				return false;
			}
			if($(".cuotas").val().indexOf('-') != -1){
				return false;
			}
		}*/
		if ( (key.charCode < 48 || key.charCode > 57)//numeros
			&& (key.charCode != 44) //coma
			&& (key.charCode != 45) //guion medio
			)
			return false;
    });
    $('.datepicker').datepicker({
		format:'dd/mm/yyyy',
        autoclose: true,
    });
	$('.datepicker').inputmask();

    $('.fa-upload').click(function(){
		$("#file").click();
	});
	editor = CKEDITOR.replace( 'legal', {
		language: 'es',
		height: '100px',
		/*uiColor: '#9AB8F3',*/
		toolbar: [
		{ name: 'clipboard', groups: [ 'clipboard', 'undo' ], items: [ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ] },
		{ name: 'document', groups: [ 'mode', 'document', 'doctools' ], items: [ 'Source', '-', 'Save', 'NewPage', 'Preview', 'Print', '-', 'Templates' ] },
		{ name: 'links', items: [ 'Link', 'Unlink', 'Anchor' ] },
		{ name: 'insert', items: [ 'Image', 'Flash', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe' ] },
		{ name: 'editing', groups: [ 'find', 'selection', 'spellchecker' ], items: [ 'Find', 'Replace', '-', 'SelectAll', '-', 'Scayt' ] },
		{ name: 'forms', items: [ 'Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField' ] },
		'/',
		{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat' ] },
		{ name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl', 'Language' ] },
		{ name: 'styles', items: [ 'Styles', 'Format', 'Font', 'FontSize' ] },
		{ name: 'colors', items: [ 'TextColor', 'BGColor' ] },
		{ name: 'tools', items: [ 'Maximize', 'ShowBlocks' ] }
		],
	});
});
</script>
