<form id="typeaccount_form">
<div style="color:red;display:none;" id="required">Debe completar todos los campos.<br/></div>
<div style="color:red;display:none;" id="required2">La combinación de campos ya fue cargada.<br/></div>
<div class="row">
<div class="col-md-3 required">
		<label>Tarjeta de crédito <span class="required">*</span></label>		
		<?php echo CHtml::dropDownList('id_credit_cards', array(),CHtml::listData(Creditcards::model()->findAll(),'id','name'), array('class'=>'form-control select2','data-placeholder'=>'Seleccione','prompt'=>'Seleccione','style'=>'width:100%')); ?>
</div>
<div class="col-md-3 required">	
	<div> <!--class="ui-widget"-->
		<label>Tipo de cuenta <span class="required">*</span></label>
		<input size="256" maxlength="256" class="form-control" name="account_type" id="account_type" type="text">	
	</div>			
</div>
</div>
</form>
<?php
	echo '<script>viewPage = "'.Yii::app()->controller->action->id.'";</script>';
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/js/form-validators.js',CClientScript::POS_END);
?>	
<script>
function refresh_datos(event){
	event.preventDefault();
	var error=0;
	$('#typeaccount_form :input').each( function () {
		if ($(this).val()== '' ) {
			error=1
		}
	});	
	var accounttype=JSON.parse($('#accounttype').val());
	$.each( accounttype, function( key, value ) {
		if(value.id_credit_cards==$('#id_credit_cards').val() && $('#account_type').val()==value.account_type){
			error=500;
		}
	});
	if(error==500){
		$('#required2').css('display','block');
		return false;
	}	
	if(error>0){
		$('#required2').css('display','none');
		$('#required').css('display','block');
	}else{
		var formdata = $("#typeaccount_form").serializeArray();
		var data = {};
		$(formdata).each(function(index, obj){
			data[obj.name] = obj.value;
		});
		data.id='new_'+parseInt(Math.random()*30000000);
		data.credit_cards_name=$('#id_credit_cards option:selected').html();
		var accounttype=JSON.parse($('#accounttype').val());		
		accounttype.push(data);		
		$('#accounttype').val(JSON.stringify(accounttype));
		$('#example2').dataTable().fnDestroy();
		$('#result2').html('');
		$.each( accounttype, function( key, value ) {
				$('#result2').append('<tr><td>'+ value.credit_cards_name+'</td><td>'+value.account_type+'</td><td><a href="#" id="'+ value.id+'" class="edit_credit_cards2" data-toggle="tooltip" data-original-title="Editar"><i class="fa fa-edit icon_cbutton"></i></a></td><td><a href="#" id="'+ value.id+'" class="delete_credit_cards2" data-toggle="tooltip" data-original-title="Borrar"><i class="fa fa-trash-o icon_cbutton"></i></a></td></tr>');
		});				
		datatable=$('#example2').dataTable({
			"bPaginate": false,
			"bLengthChange": false,
			"bFilter": true,
			"bInfo": false,
			"bAutoWidth": false,
			"language": {
				"sSearch": "Buscar:",
			}			
		});	
		$( ".delete_credit_cards2" ).on( "click", function(event) {
			event.preventDefault();
			var id=$(this).attr('id');
			var accounttype=JSON.parse($('#accounttype').val());
			var accounttype_new=[];
			$.each( accounttype, function( key, value ) {
				if(value.id!=id){
					accounttype_new.push(value);
				}
			});
			$('#accounttype').val(JSON.stringify(accounttype_new));
			$(this).parent().parent().remove();
		});	
		$( ".edit_credit_cards2" ).on( "click", function(event) {
			event.preventDefault();
			var id=$(this).attr('id');
			var accounttype_edit;
			var accounttype=JSON.parse($('#accounttype').val());
			$.each( accounttype, function( key, value ) {
				if(value.id==id){
					accounttype_edit=value;
				}
			});	
			var request = $.ajax({
				url: homeurl+'/entity/typeaccountedit',
				method: "POST",
				data: accounttype_edit,
			}).done(function(msg) {
				$('.ligth-box-modal').css('display','block');
				$('.modal-title').html('Tipo de cuenta');
				$('.modal-body').html(msg);	
				$('.modal-footer').html('<a href="#" onclick="refresh_datos(event);" class="btn btn-primary">Actualizar</a>');	
			});	
		});		
		$('.close_window').trigger('click');		
	}
}
$( document ).ready(function() {
	var accounttype=JSON.parse($('#accounttype').val());
	var accounttype_new=[];
	$.each( accounttype, function( key, value ) {
		existe=0;
		$.each( accounttype_new, function( key2, value2 ) {
			if(value2==value.account_type){
				existe=1;
			}
		});	
		if(existe==0){
			accounttype_new.push(value.account_type);
		}	
	});
	var availableTags =accounttype_new;
	$( "#account_type" ).autocomplete({
	  source: availableTags
	});
});		
</script>
