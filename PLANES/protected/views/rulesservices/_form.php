<?php
/* @var $this RulesservicesController */
/* @var $model Rulesservices */
/* @var $form CActiveForm */
?>
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'rulesservices-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>
<div class="box-body">
	<span class="right_f">
<?php echo $form->checkBox($model,'enabled',array('class'=>'lcs_check enabled')); ?>

	</span>

<div class="col-md-12 col-data">

	<div class="col-md-3 form-group required unique">
		<?php echo $form->labelEx($model,'id_channel'); ?>
		<?php echo $form->dropDownList($model, 'id_channel',CHtml::listData(Channels::model()->findAll(), 'id', 'title'),array('empty' => 'Seleccione','class'=>'form-control channel'));?>
		<?php echo $form->error($model,'id_channel'); ?>
	</div>
<div class="col-md-3 form-group required unique">
		<?php echo $form->labelEx($model,'id_subchannel'); ?>
		<?php echo $form->dropDownList($model, 'id_subchannel',CHtml::listData(Subchannels::model()->findAll(), 'id', 'title'),array('empty' => 'Seleccione','class'=>'form-control subchannel selectobject'));?>
		<?php echo $form->error($model,'id_subchannel'); ?>
	</div>
<div class="form-group col-md-3 required unique">
		<?php echo $form->labelEx($model,'id_product'); ?>
		<?php echo $form->dropDownList($model, 'id_product',CHtml::listData(Products::model()->findAll(), 'id', 'title'),array('empty' => 'Seleccione','class'=>'form-control'));?>
		<?php echo $form->error($model,'id_product'); ?>
</div>

<div class="form-group col-md-3 required unique">
	<?php echo $form->labelEx($model,'id_provider'); ?>
		<?php echo $form->dropDownList($model, 'id_provider',CHtml::listData(Providers::model()->findAll(), 'id', 'title'),array('empty' => 'Seleccione','class'=>'form-control'));?>
		<?php echo $form->error($model,'id_provider'); ?>
</div>
<div class="form-group col-md-12 required">
	<?php echo $form->labelEx($model,'description'); ?>
			<?php echo $form->textField($model,'description',array('size'=>45,'maxlength'=>45,'class'=>'form-control')); ?>
							<?php echo $form->error($model,'description'); ?>
</div>
<div class="form-group col-md-6 required">
	<?php echo $form->labelEx($model,'external_endpoint'); ?>
			<?php echo $form->textField($model,'external_endpoint',array('size'=>60,'maxlength'=>255,'class'=>'form-control')); ?>
							<?php echo $form->error($model,'external_endpoint'); ?>
</div>
<div class="form-group col-md-6 required">
	<?php echo $form->labelEx($model,'internal_endpoint'); ?>
	<?php echo $form->textField($model,'internal_endpoint',array('size'=>60,'maxlength'=>255,'class'=>'form-control')); ?>
	<?php echo $form->error($model,'internal_endpoint'); ?>
</div>

<div class="form-group col-md-6 ">
	<?php echo $form->labelEx($model,'client_user'); ?>
			<?php echo $form->textField($model,'client_user',array('size'=>45,'maxlength'=>45,'class'=>'form-control')); ?>
							<?php echo $form->error($model,'client_user'); ?>
</div>
<div class="form-group col-md-6 ">
	<?php echo $form->labelEx($model,'client_pass'); ?>
			<?php echo $form->textField($model,'client_pass',array('size'=>45,'maxlength'=>45,'class'=>'form-control')); ?>
							<?php echo $form->error($model,'client_pass'); ?>
</div>

<div class="form-group col-md-3 required">
	<?php echo $form->labelEx($model,'expire_date_from'); ?>
	<div class="input-group">
			<?php echo $form->textField($model,'expire_date_from',array('class'=>'form-control datepicker','data-inputmask'=>'\'alias\': \'dd/mm/yyyy\'','data-mask'=>'data-mask')); ?>
			<div class="input-group-addon pointer" data-toggle="tooltip" title="Abrir el calendario"><i class="fa fa-calendar"></i></div>
			</div>
							<?php echo $form->error($model,'expire_date_from'); ?>
</div>

<div class="form-group col-md-3 required">
	<?php echo $form->labelEx($model,'expire_date_to'); ?>
	<div class="input-group">
			<?php echo $form->textField($model,'expire_date_to',array('class'=>'form-control datepicker','data-inputmask'=>'\'alias\': \'dd/mm/yyyy\'','data-mask'=>'data-mask')); ?>
			<div class="input-group-addon pointer" data-toggle="tooltip" title="Abrir el calendario"><i class="fa fa-calendar"></i></div>
			</div>
							<?php echo $form->error($model,'expire_date_to'); ?>
</div>
		<div class="col-md-3 ">
			<?php echo $form->labelEx($model,'anticipation_from_min_days'); ?><br>
			<?php echo $form->numberField($model,'anticipation_from_min_days',array('class'=>'form-control','min'=>'0')); ?>
			<?php echo $form->error($model,'anticipation_from_min_days'); ?>
		</div>
		<div class="col-md-3 ">
			<?php echo $form->labelEx($model,'anticipation_to_max_days'); ?><br>
			<?php echo $form->numberField($model,'anticipation_to_max_days',array('class'=>'form-control','min'=>'0')); ?>
			<?php echo $form->error($model,'anticipation_to_max_days'); ?>
		</div>
		

	<div class="col-md-12 form-group">
		<div class="col-md-3 ">
			<?php echo $form->labelEx($model,'time_out'); ?><br>
			<?php echo $form->numberField($model,'time_out',array('class'=>'form-control','min'=>'0')); ?>
			<?php echo $form->error($model,'time_out'); ?>
		</div>
		<div class="col-md-9 ">
			<?php echo $form->labelEx($model,'time_out_msg'); ?>
			<?php echo $form->textField($model,'time_out_msg',array('size'=>60,'maxlength'=>255,'class'=>'form-control')); ?>
			<?php echo $form->error($model,'time_out_msg'); ?>
		</div>
	</div>
	<div class="col-md-12 form-group">
		<div class="col-md-2">
			<?php echo $form->labelEx($model,'cache'); ?>
			<?php echo $form->CheckBox($model,'cache',array('class'=>'lcs_check kcheck' )); ?>
			<?php echo $form->error($model,'cache'); ?>
		</div>
		<div class="col-md-10 cacheFields" style="display:none;">
			<div class="col-md-3 required">
				<?php echo $form->labelEx($model,'start_cache_days'); ?>
				<?php echo $form->numberField($model,'start_cache_days',array('class'=>'form-control','min'=>'0')); ?>
				<?php echo $form->error($model,'start_cache_days'); ?>
			</div>
			<div class="col-md-3 required">
				<?php echo $form->labelEx($model,'max_cache_days'); ?>
				<?php echo $form->numberField($model,'max_cache_days',array('class'=>'form-control','min'=>'0')); ?>
				<?php echo $form->error($model,'max_cache_days'); ?>
			</div>
			<div class="col-md-3 required">
				<?php echo $form->labelEx($model,'frequency'); ?>
				<?php echo $form->numberField($model,'frequency',array('class'=>'form-control','min'=>'0','max'=>'24')); ?>
				<?php echo $form->error($model,'frequency'); ?>
			</div>
			<div class="col-md-3 required">
				<div class="bootstrap-timepicker">
					<div class="form-group">
						<label for="Profile_title" class="required">Hora<span class="required">*</span></label>
						<div class="input-group">

							<input type="text" name="start_time" id="start_time" class="form-control timepicker" value="<?php if(isset($model->criterio["start_time"])){ echo $model->criterio["start_time"]; } ?>" >
							<div class="input-group-addon">
								<i class="fa fa-clock-o"  data-toggle="tooltip" title="Abrir el reloj"></i>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

<div class="box-footer">
		<?php
		$title = (Yii::app()->controller->action->id!='view') ? 'Volver sin guardar cambios' : 'Volver';
		echo '<a href="'.Yii::app()->request->baseUrl.'/'.strtolower(Yii::app()->controller->id).'" class="btn btn-secundary cancel" data-toggle="tooltip" title="'.$title.'" >Cerrar</a>';
		if(Yii::app()->controller->action->id!='view'){
			echo CHtml::submitButton('Guardar',array('class'=>'btn btn-primary save','data-toggle'=>'tooltip','title'=>'Guardar cambios'));
		}
		?></div>
<?php $this->endWidget(); ?>

</div><!-- form -->
</div>
<?php
	Yii::app()->clientScript->registerCssFile(Yii::app()->params['cdnUrl'].'/plugins/lcswich/lc_switch-tts.css');
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/lcswich/lc_switch.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/lcswich/lc_switch-tts.js',CClientScript::POS_END);

	Yii::app()->clientScript->registerCssFile(Yii::app()->params['cdnUrl'].'/plugins/datepicker/datepicker3.css');
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/datepicker/bootstrap-datepicker.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/input-mask/jquery.inputmask.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/input-mask/jquery.inputmask.date.extensions.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/datepicker/datepicker-tts.js',CClientScript::POS_END);

	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/timepicker/bootstrap-timepicker.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/input-mask/jquery.inputmask.bundle.js',CClientScript::POS_END);

	Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/plugins/select2/select2.full.js',CClientScript::POS_END);

	Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl.'/js/plugins/select2/select2.css'); 
	
	Yii::app()->clientScript->registerCssFile(Yii::app()->params['cdnUrl'].'/plugins/timepicker/bootstrap-timepicker.css');
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/timepicker/timepicker-tts.js',CClientScript::POS_END);
	echo '<script>viewPage = "'.Yii::app()->controller->action->id.'";</script>';
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/js/form-validators.js',CClientScript::POS_END)
	 ?>
<script>
$( document ).ready(function() {
	<?php if(Yii::app()->controller->action->id=='view'){ echo "$('form').find('input, textarea, button, checkbox, select').attr('disabled','disabled');"; } ?>
	$('[data-mask]').inputmask('dd/mm/yyyy', {'placeholder': 'dd/mm/aaaa'});

	if($('.kcheck').prop('checked')){
		$('.cacheFields').show(250);
	}else{
		$('#hour').val('');
	}

	$('.kcheck').change(function(){
		if($(this).prop('checked')){
			$('.cacheFields').show(250);
		}else{
			$('.cacheFields input').val('');
			$('.cacheFields').hide(250);
		}
	});

	/*
	$('.fa-calendar').datepicker().on('changeDate', function(e) {
		$(this).parent().prev().val(e.format('dd/mm/yyyy'));
	    $(this).datepicker('hide');
    });*/

/*
	$('input.datepicker[name*="from"]').datepicker().on('changeDate',function(e){
		$('input.datepicker[name*="from"]').prop()
	})
*/
/*
	$('input.datepicker[name*="from"]').datepicker({
		    	minDate:0,
		    	dateFormat:'dd/mm/yyyy',
		    	onClose:function(date){
		    		//console.log(date);
						$('input.datepicker[name*="to"]').datepicker({
							minDate:date,
							dateFormat:'dd/mm/yyyy',
						});
	    		}
		    		
		});*/

     channelsFilter();
     
});
</script>
