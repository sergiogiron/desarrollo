<?php
/* @var $this RulesservicesController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Rulesservices',
);

$this->menu=array(
	array('label'=>'Create Rulesservices', 'url'=>array('create')),
	array('label'=>'Manage Rulesservices', 'url'=>array('admin')),
);
?>

<h1>Rulesservices</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
