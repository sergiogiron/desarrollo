<?php
/* @var $this ExcludeRulesController */
/* @var $model ExcludeRules */
/* @var $form CActiveForm */
?>
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'exclude-rules-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>
<div class="box-body">
	<span class="right_f">
<?php echo $form->checkBox($model,'enabled',array('class'=>'lcs_check enabled')); ?>

	</span>


<div class="col-md-12 col-data">
	<div class="form-group col-md-3 required unique">
		<?php echo $form->labelEx($model,'id_channel'); ?>
		<?php echo $form->dropDownList($model, 'id_channel',CHtml::listData(Channels::model()->findAll(), 'id', 'title'),array('empty' => 'Seleccione...','class'=>'form-control selectobject channel'));?>
		<?php echo $form->error($model,'id_channel'); ?>
	</div>
	<div class="form-group col-md-3 required unique">
		<?php echo $form->labelEx($model,'id_subchannel'); ?>
		<?php echo $form->dropDownList($model, 'id_subchannel',CHtml::listData(Subchannels::model()->findAll(), 'id', 'title'),array('empty' => 'Seleccione...','class'=>'form-control selectobject subchannel'));?>
		<?php echo $form->error($model,'id_subchannel'); ?>
	</div>
	<div class="form-group col-md-6 required unique">
		<?php echo $form->labelEx($model,'id_product'); ?>
		<?php echo $form->dropDownList($model, 'id_product',CHtml::listData(Products::model()->findAll(), 'id', 'title'),array('multiple'=>'multiple','class'=>'form-control select2','style'=>'width:100%'));?>
		<?php echo $form->error($model,'id_product'); ?>
	</div>
	
	<div class="form-group col-md-12">
		<div class="form-group col-md-3 required">
			<?php echo $form->labelEx($model,'id_iata_country_master'); ?>
			<?php echo $form->dropDownList($model, 'id_iata_country_master',CHtml::listData(Iatacountrymaster::model()->findAll(), 'id', 'Country'),array('empty' => 'Seleccione...','class'=>'form-control selectobject'));?>
			<?php echo $form->error($model,'id_iata_country_master'); ?>
		</div>
		<div class="col-md-3 required">
			<?php echo $form->labelEx($model,'expire_date_from'); ?>
			<div class="input-group">
				<?php echo $form->textField($model,'expire_date_from',array('class'=>'form-control datepicker','data-inputmask'=>'\'alias\': \'dd/mm/yyyy\'','data-mask'=>'data-mask')); ?>
				<div class="input-group-addon pointer"><i class="fa fa-calendar"></i></div>
			</div>
			<?php echo $form->error($model,'expire_date_from'); ?>
		</div>
		<div class="col-md-3 required">
			<?php echo $form->labelEx($model,'expire_date_to'); ?>
			<div class="input-group">
				<?php echo $form->textField($model,'expire_date_to',array('class'=>'form-control datepicker','data-inputmask'=>'\'alias\': \'dd/mm/yyyy\'','data-mask'=>'data-mask')); ?>
				<div class="input-group-addon pointer"><i class="fa fa-calendar"></i></div>
			</div>
			<?php echo $form->error($model,'expire_date_to'); ?>
		</div>
	</div>
<div class="box-footer">
		<?php
		$title = (Yii::app()->controller->action->id!='view') ? 'Volver sin guardar cambios' : 'Volver';
		echo '<a href="'.Yii::app()->request->baseUrl.'/'.strtolower(Yii::app()->controller->id).'" class="btn btn-secundary cancel" data-toggle="tooltip" title="'.$title.'" >Cerrar</a>';
		if(Yii::app()->controller->action->id!='view'){
			echo CHtml::submitButton('Guardar',array('class'=>'btn btn-primary save','data-toggle'=>'tooltip','title'=>'Guardar cambios'));
		}
		?></div>
<?php $this->endWidget(); ?>

</div><!-- form -->
</div>
<?php
	Yii::app()->clientScript->registerCssFile(Yii::app()->params['cdnUrl'].'/plugins/lcswich/lc_switch-tts.css');
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/lcswich/lc_switch.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/lcswich/lc_switch-tts.js',CClientScript::POS_END);

	Yii::app()->clientScript->registerCssFile(Yii::app()->params['cdnUrl'].'/plugins/datepicker/datepicker3.css');
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/datepicker/bootstrap-datepicker.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/input-mask/jquery.inputmask.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/input-mask/jquery.inputmask.date.extensions.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/datepicker/datepicker-tts.js',CClientScript::POS_END);

	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/timepicker/bootstrap-timepicker.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/input-mask/jquery.inputmask.bundle.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerCssFile(Yii::app()->params['cdnUrl'].'/plugins/timepicker/bootstrap-timepicker.css');
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/timepicker/timepicker-tts.js',CClientScript::POS_END);

	Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/plugins/select2/select2.full.js',CClientScript::POS_END);

	Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl.'/js/plugins/select2/select2.css'); 

	echo '<script>viewPage = "'.Yii::app()->controller->action->id.'";</script>';
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/js/form-validators.js',CClientScript::POS_END) ?>
<script>

$( document ).ready(function() {

	<?php if(Yii::app()->controller->action->id=='view'){ echo "$('form').find('input, textarea, button, checkbox, select').attr('disabled','disabled');"; } ?>
	$('[data-mask]').inputmask('dd/mm/yyyy', {'placeholder': 'dd/mm/aaaa'});
	$('.fa-calendar').datepicker().on('changeDate', function(e) {
		$(this).parent().prev().val(e.format('dd/mm/yyyy'));
	    $(this).datepicker('hide');
    });

	$('.datepicker').datepicker({
      autoclose: true,
	  //format: 'yyyy-mm-dd',
	  format: 'dd/mm/yyyy',
	  firstDay: 1
    });
    
   channelsFilter();

	if(viewPage=='update'){
		$('.unique input').prop('disabled',true);
		$('.unique select').prop('disabled',true);
		$(".subchannel").prop('disabled',false);
	}
});
</script>