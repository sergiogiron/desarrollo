<?php
/* @var $this ProfileController */
/* @var $model Profile */
/* @var $form CActiveForm */
?>

<div class="box-body">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'profile-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>
	<span class="right_f active-button">
		<?php echo $form->checkBox($model,'enabled',array('class'=>'lcs_check enabled')); ?>
	</span>
	<br>
	<div class="col-md-12 col-data">
	<div class="form-group col-md-6 required">
		<?php echo $form->labelEx($model,'title'); ?>
		<?php echo $form->textField($model,'title',array('size'=>20,'maxlength'=>20,'class'=>'form-control')); ?>
		<?php echo $form->error($model,'title'); ?>
	</div>

	<div class="form-group col-md-6 required">
		<?php echo $form->labelEx($model,'description'); ?>
		<?php echo $form->textField($model,'description',array('size'=>60,'maxlength'=>60,'class'=>'form-control')); ?>
		<?php echo $form->error($model,'description'); ?>
	</div>
	<?php if(Yii::app()->controller->action->id!='view'):?>
	<div class="col-md-6">
		<a class="btn btn-primary" role="button" href="<?php echo Yii::app()->request->baseUrl; ?>/profile/assignroles" id="asignar_roles" style="text-decoration:none;margin-bottom: 20px;" >Asignar roles</a>
	</div>
	<?php endif;?>
	<br /><br />
	<div id="assign_result" class="col-md-12">
		<?php
		//MENU AGRUPACION
		$menues_array=array();
		$array_final=array();

		foreach ($model->menues as $menues){
			$roles_list=array();
			$roles_basic=array();
			foreach($menues->roles as $roles){
				$roles_list[]=array('id'=>$roles->id_role,'role'=>$roles->role->title);
				$roles_basic[]=$roles->id_role;
			}			
			$menues_array[$menues->id_menu]['data']=array('id'=>$menues->id_menu,'title'=>$menues->menu->title);
			$menues_array[$menues->id_menu]['roles']=$roles_list;
			$menues_array[$menues->id_menu]['update']=array('name'=>$menues->id_menu,'type'=>1,'roles'=>$roles_basic);			
		}
		foreach ($menues_array as $menues){
			echo '<div class="row" id="1'.$menues['data']['id'].'"><div class="col-md-12"><div class="box box_custom box-success box-solid collapsed-box">
			<div class="box-header_custom with-border"><h3 class="box-title_custom">Menu - '.$menues['data']['title'].'</h3>
			<div class="box-tools pull-right">
			<button type="button" class="btn btn-box-tool btn-box-tool_custom" data-widget="collapse">
			<i class="fa fa-plus"></i></button>';
			echo "<button type='button' class='btn btn-box-tool btn-box-tool_custom remove_element' onclick='pasadatos(".json_encode($menues['update']).")'><i class='fa fa-times'></i></button>";
			echo '</div></div><div class="box-body box_body_custom"><div class="roles_content_custom">';
			$rol='';
			foreach($menues['roles'] as $roles){
				$rol.= $roles['role'].'-';
			}
			$array_final[]=array('type'=>1,'name'=>$menues['data']['id'],'roles'=>$menues['update']['roles']);
			echo substr($rol,0,-1);
			echo '</div> <div class="box-tools pull-right">';
			echo "<a href='#' class='pasadatosedit' onclick='pasadatosedit(".json_encode($menues['update']).")'><i class='fa fa-pencil-square-o'></i></a>";
			echo '</div></div></div></div></div>';		
		}
		//WIDGETS AGRUPACION
		$widget_array=array();
		foreach ($model->widgets as $widgets){
			$roles_list=array();
			$roles_basic=array();
			foreach($widgets->roles as $roles){
				$roles_list[]=array('id'=>$roles->id_role,'role'=>$roles->role->title);
				$roles_basic[]=$roles->id_role;
			}			
			$widget_array[$widgets->id_widget]['data']=array('id'=>$widgets->id_widget,'title'=>$widgets->widget->title);
			$widget_array[$widgets->id_widget]['roles']=$roles_list;
			$widget_array[$widgets->id_widget]['update']=array('name'=>$widgets->id_widget,'type'=>2,'roles'=>$roles_basic);			
		}		
		foreach ($widget_array as $widget){
			echo '<div class="row" id="2'.$widget['data']['id'].'"><div class="col-md-12"><div class="box box_custom box-success box-solid collapsed-box">
			<div class="box-header_custom with-border"><h3 class="box-title_custom">Widget - '.$widget['data']['title'].'</h3>
			<div class="box-tools pull-right">
			<button type="button" class="btn btn-box-tool btn-box-tool_custom" data-widget="collapse">
			<i class="fa fa-plus"></i></button>';
			echo "<button type='button' class='btn btn-box-tool btn-box-tool_custom remove_element' onclick='pasadatos(".json_encode($widget['update']).")'><i class='fa fa-times'></i></button>";
			echo '</div></div><div class="box-body box_body_custom"><div class="roles_content_custom">';
			$rol='';
			foreach($widget['roles'] as $roles){
				$rol.= $roles['role'].'-';
			}	
			$array_final[]=array('type'=>2,'name'=>$widget['data']['id'],'roles'=>$widget['update']['roles']);
			echo substr($rol,0,-1);
			echo ' </div><div class="box-tools pull-right">';
			echo "<a href='#' class='pasadatosedit' onclick='pasadatosedit(".json_encode($widget['update']).")'><i class='fa fa-pencil-square-o'></i></a>";
			echo '</div></div></div></div></div>';		
		}
		?>	
	</div>
	<div class="box-footer">
		<?php
			$title = (Yii::app()->controller->action->id!='view') ? 'Volver sin guardar cambios' : 'Volver';
			echo '<a href="'.Yii::app()->request->baseUrl.'/'.strtolower(Yii::app()->controller->id).'" class="btn btn-secundary" data-toggle="tooltip" title="'.$title.'" >Cerrar</a>';
			if(Yii::app()->controller->action->id!='view'){
				echo CHtml::submitButton('Guardar',array('class'=>'btn btn-primary','data-toggle'=>'tooltip','title'=>'Guardar cambios'));
			}
		?>
	</div>
	<input type='hidden' id='result_roles' name='result_roles' value='<?php echo json_encode($array_final)?>'/>

<?php $this->endWidget(); ?>
</div>
</div><!-- form -->
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/plugins/select2/select2.full.js',CClientScript::POS_END); ?>
<?php Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl.'/js/plugins/select2/select2.css'); ?>
<script>
function pasadatosedit(datos){
	var request = $.ajax({
		url: homeurl+'/profile/assignrolesedit',
	}).done(function(msg) {
		$('.ligth-box-modal').css('display','block');
		$('.modal-title').html('Asignar roles');
		$('.modal-body').html(msg);	
		$.each(datos.roles, function(k, v) {			
			$('#roles_select> option[value='+v+']').prop('selected', 'selected');
		});	
		var edit_datos=new Object;
		edit_datos.type=datos.type;
		edit_datos.name=datos.name;
		$('#assign').attr('data-id',JSON.stringify(edit_datos));
	});	
}
function pasadatos(datos){	
	data=JSON.parse($('#result_roles').val());
	var roles_object_array=[];
	$.each(data, function(k, v) {
		if(datos.type!=v.type || datos.name!=v.name){
			var roles_object=new Object;	
			roles_object.type=v.type;
			roles_object.name=v.name;
			roles_object.roles=v.roles;
			roles_object_array.push(roles_object);			
		}		
	});
	$('#result_roles').val(JSON.stringify(roles_object_array));
	$("#"+datos.type+datos.name).remove();
}
$(document).ready(function(){
	$( "#asignar_roles" ).click(function(event){
		event.preventDefault();
		var request = $.ajax({
			url: $(this).attr('href'),
		}).done(function(msg) {
			$('.ligth-box-modal').css('display','block');
			$('.modal-title').html('Asignar roles');
			$('.modal-body').html(msg);
		});	
	});
});	
</script>
<?php
	// Activo/Inactivo
	Yii::app()->clientScript->registerCssFile(Yii::app()->params['cdnUrl'].'/plugins/lcswich/lc_switch-tts.css'); 
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/lcswich/lc_switch.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/lcswich/lc_switch-tts.js',CClientScript::POS_END); 
	/*Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl.'/../cdn/plugins/datepicker/datepicker3.css');
	Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/../cdn/plugins/datepicker/bootstrap-datepicker.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/../cdn/plugins/input-mask/jquery.inputmask.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/../cdn/plugins/input-mask/jquery.inputmask.date.extensions.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/../cdn/plugins/datepicker/datepicker-tts.js',CClientScript::POS_END);*/
	// Timepicker con la mascara
	/*
	Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/../cdn/plugins/timepicker/bootstrap-timepicker.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/../cdn/plugins/input-mask/jquery.inputmask.bundle.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl.'/../cdn/plugins/timepicker/bootstrap-timepicker.css');
	Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/../cdn/plugins/timepicker/timepicker-tts.js',CClientScript::POS_END);
	// Editor de texto enriquesido
	Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/../cdn/plugins/ckeditor-standard/ckeditor.js',CClientScript::POS_END);
	*/
 ?>
 <script type="text/javascript">
	<?php
		if(Yii::app()->controller->action->id=='view'){ echo "$('form').find('input, textarea, checkbox, select').attr('disabled','disabled'); $('.fa-pencil-square-o').hide()"; }
	?>	
</script>