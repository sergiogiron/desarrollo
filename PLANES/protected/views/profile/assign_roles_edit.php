<div class="form-group" id="roles">
	<label for="Profile_title" class="required">Roles <span class="required">*</span></label>
	<?php 
		$list=CHtml::listData(ProfileRole::model()->findAll(),'id','title');
		echo CHtml::dropDownList('roles', array(), $list, array('class'=>'form-control select2','data-placeholder'=>'Seleccione','id'=>'roles_select','multiple'=>'multiple','style'=>'width:100%')); 
	?>
</div>
<input class="btn btn-default" type="submit" data-id="" id="assign" value="Asignar">
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/plugins/select2/select2.full.js',CClientScript::POS_END); ?>
<?php Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl.'/js/plugins/select2/select2.css'); ?>
<script>
$(document).ready(function(){
	$('.select2').select2().on('select2:unselect', function(e) {
		$('.select2-selection').trigger('click');
	});
	var id_asign;
	$( "#assign" ).click(function(){
		data_id=JSON.parse($(this).attr('data-id'));
		var result=[];
		var roles_object_array=JSON.parse($("#result_roles").val());
		$.each(roles_object_array, function(k, v) {
			var roles_object_new=new Object;
			roles_object_new.type=v.type;
			roles_object_new.name=v.name;
			if(v.type==data_id.type && v.name==data_id.name){
				roles_object_new.roles=$("#roles_select").val();
			}else{
				roles_object_new.roles=v.roles;
			}			
			result.push(roles_object_new);				
		});
		var roles_value='';
		$.each($("#roles_select").val(), function(i, item) {
		   roles_value+=$("#roles_select > option[value="+item+"]:selected").text()+' - ';
		});		
		roles_value=roles_value.substring(0,roles_value.length-2);		
		$('#'+data_id.type+data_id.name).find('.roles_content_custom').html(roles_value);
		data_id.roles=$("#roles_select").val();
		$('#'+data_id.type+data_id.name).find('.pasadatosedit').attr('onclick','pasadatosedit('+JSON.stringify(data_id)+')');
		$('#result_roles').val(JSON.stringify(result));
		$('.close_window').trigger('click');
	});			
});	
</script>