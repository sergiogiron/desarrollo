<div class="form-group">
	<label for="Profile_title" class="required">Tipo <span class="required">*</span></label>
	<select name="type" id="type" class="form-control">
		<option value="">Seleccione</option>
		<option value="1">Menu</option>
		<option value="2">Widget</option>
	</select>	
</div>
<div class="form-group" style="display:none;" id="menues">
	<label for="Profile_title" class="required">Menues <span class="required">*</span></label>
	<?php 
		$list=CHtml::listData(Profilemenu::model()->findAll(),'id','title');
		echo CHtml::dropDownList('menues', array(), $list, array('class'=>'form-control select2','data-placeholder'=>'Seleccione','prompt'=>'Seleccione','id'=>'menues_select','style'=>'width:100%')); 
	?>
</div>
<div class="form-group"  id="widgets" style="display:none;">
	<label for="Profile_title" class="required">Widgets <span class="required">*</span></label>
	<?php 
		$list=CHtml::listData(Profilewidgets::model()->findAll(),'id','title');
		echo CHtml::dropDownList('widgets', array(), $list, array('class'=>'form-control select2','data-placeholder'=>'Seleccione','id'=>'widget_select','style'=>'width:100%')); 
	?>
</div>	
<div class="form-group" style="display:none;" id="roles">
	<label for="Profile_title" class="required">Roles <span class="required">*</span></label>
	<?php 
		$list=CHtml::listData(Profilerole::model()->findAll(),'id','title');
		echo CHtml::dropDownList('roles', array(), $list, array('class'=>'form-control select2','data-placeholder'=>'Seleccione','id'=>'roles_select','multiple'=>'multiple','style'=>'width:100%')); 
	?>
</div>
<input class="btn btn-default" type="submit" id="assign" value="Asignar">
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/plugins/select2/select2.full.js',CClientScript::POS_END); ?>
<?php Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl.'/js/plugins/select2/select2.css'); ?>
<script>
$(document).ready(function(){
	data=JSON.parse($('#result_roles').val());
	$.each(data, function(k, v) {
		if(v.type==1){
			$('#menues_select> option[value='+v.name+']').prop('disabled', 'disabled');
		}else{
			$('#widget_select> option[value='+v.name+']').prop('disabled', 'disabled');
		}	
	});		
	$('.select2').select2().on('select2:unselect', function(e) {
		$('.select2-selection').trigger('click');
	});
	var id_asign;
	$( "#assign" ).click(function(){
		var roles_object_array=JSON.parse($("#result_roles").val());
		var roles_object=new Object;				
		if($("#type").val()==1){
			roles_object.type='1';
			roles_object.name=$("#menues_select").val();
			var type_option=$("#menues_select option:selected").text();
			id_asign='1'+$("#menues_select").val();
		}else{			
			var type_option=$("#widget_select option:selected").text();
			roles_object.type='2';
			id_asign='2'+$("#widget_select").val();
			roles_object.name=$("#widget_select").val();
		}
		var roles='';
		roles_object.roles=$("#roles_select").val();
		
		roles_object_array.push(roles_object);
		$("#result_roles").val(JSON.stringify(roles_object_array));
		$.each($("#roles_select").val(), function(i, item) {
		   roles+=$("#roles_select > option[value="+item+"]:selected").text()+' - ';
		});		
		roles=roles.substring(0,roles.length-2);
		$('#assign_result').append("<div class='row' id='"+id_asign+"'><div class='col-md-12'><div class='box box_custom box-success box-solid collapsed-box'><div class='box-header_custom with-border'><h3 class='box-title_custom'>"+$('#type option:selected').text()+" - "+type_option+"</h3><div class='box-tools pull-right'><button type='button' class='btn btn-box-tool btn-box-tool_custom' data-widget='collapse'><i class='fa fa-plus'></i></button><button type='button' class='btn btn-box-tool btn-box-tool_custom remove_element' onclick='pasadatos("+JSON.stringify(roles_object)+")'><i class='fa fa-times'></i></button></div></div><div class='box-body box_body_custom'><div class='roles_content_custom'>"+roles+"</div><div class='box-tools pull-right'><a href='#' class='pasadatosedit' onclick='pasadatosedit("+JSON.stringify(roles_object)+")'><i class='fa fa-pencil-square-o'></i></a></div></div></div></div></div>");		
		$('.close_window').trigger('click');
	});		
	$( "#type" ).change(function(){
		if($(this).val()==2){
			$('#widgets').css('display','block');
			$('#menues').css('display','none');
			$('#roles').css('display','block');
		}else{
			$('#widgets').css('display','none');
			$('#menues').css('display','block');	
			$('#roles').css('display','block');
		}
	});	
});	
</script>