<?php
/* @var $this TransactionCostController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Transaction Costs',
);

$this->menu=array(
	array('label'=>'Create TransactionCost', 'url'=>array('create')),
	array('label'=>'Manage TransactionCost', 'url'=>array('admin')),
);
?>

<h1>Transaction Costs</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
