<?php
/* @var $this TransactioncostController */
/* @var $model Transactioncost */
/* @var $form CActiveForm */
?>

<div class="box-body">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'transactioncost-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<span class="right_f">
		<?php echo $form->checkBox($model,'enabled',array('class'=>'lcs_check enabled')); ?>
	</span>
	
	<div class="col-md-12 col-data">

		<div class="form-group col-md-4 required unique">
			<?php echo $form->labelEx($model,'id_payment_method'); ?>
			<?php echo $form->dropDownList($model, 'id_payment_method',CHtml::listData(Paymentmethod::model()->findAll(), 'id', 'rules'),array('empty' => 'Seleccione','class'=>'form-control'));?>
			<?php echo $form->error($model,'id_payment_method'); ?>
		</div>

		<div class="form-group col-md-4 required unique">
			<?php //echo $form->labelEx($model,'dues'); ?>
			<?php echo $form->labelEx($model,'dues',array('title'=>'Min. 1, Máx 24','data-toggle'=>'tooltip')); ?>
			<?php //echo $form->dropDownList($model, 'dues',array('1'=>'1','3'=>'3','6'=>'6','9'=>'9','12'=>'12','18'=>'18','24'=>'24'),array('empty' => 'Seleccione','class'=>'form-control'));?>
			<?php echo $form->numberField($model,'dues',array('class'=>'form-control','max'=>'24','min'=>'0')); ?>
			<?php echo $form->error($model,'dues'); ?>
		</div>

		<div class="form-group col-md-4 required">
			<?php echo $form->labelEx($model,'id_legal'); ?>
			<?php echo $form->dropDownList($model, 'id_legal',CHtml::listData(Legal::model()->findAll(), 'id', 'title'),array('empty' => 'Seleccione','class'=>'form-control'));?>
			<?php echo $form->error($model,'id_legal'); ?>
	</div>
	
	<div class="form-group col-md-12 ">
		<div class="col-md-2 required">
			<?php echo $form->labelEx($model,'interest'); ?>
			<?php echo $form->textField($model,'interest',array('size'=>6,'maxlength'=>6,'class'=>'form-control decimal')); ?>
			<?php echo $form->error($model,'interest'); ?>
		</div>

		<div class="col-md-2 required">
			<?php echo $form->labelEx($model,'cft'); ?>
			<?php echo $form->textField($model,'cft',array('size'=>6,'maxlength'=>6,'class'=>'form-control decimal')); ?>
			<?php echo $form->error($model,'cft'); ?>
		</div>
		
		<div class="col-md-3 required">
			<?php echo $form->labelEx($model,'expire_date_from'); ?>
			<div class="input-group">
				<?php echo $form->textField($model,'expire_date_from',array('class'=>'form-control datepicker','data-inputmask'=>'\'alias\': \'dd/mm/yyyy\'','data-mask'=>'data-mask')); ?>
				<div class="input-group-addon pointer"><i class="fa fa-calendar"></i></div>
			</div>
			<?php echo $form->error($model,'expire_date_from'); ?>
		</div>
		<div class="col-md-3 required">
			<?php echo $form->labelEx($model,'expire_date_to'); ?>
			<div class="input-group">
				<?php echo $form->textField($model,'expire_date_to',array('class'=>'form-control datepicker','data-inputmask'=>'\'alias\': \'dd/mm/yyyy\'','data-mask'=>'data-mask')); ?>
				<div class="input-group-addon pointer"><i class="fa fa-calendar"></i></div>
			</div>
			<?php echo $form->error($model,'expire_date_to'); ?>
		</div>


		<div class="col-md-2 required">
			<?php echo $form->labelEx($model,'interest_hide'); ?>
			<?php echo $form->textField($model,'interest_hide',array('class'=>'form-control decimal')); ?>
			<?php echo $form->error($model,'interest_hide'); ?>
		</div>
	</div>
	<div class="form-group col-md-12 ">

		<div class="col-md-2">
			<?php echo $form->labelEx($model,'rewards'); ?>
			<?php echo $form->CheckBox($model,'rewards',array('class'=>'lcs_check rewardcheck' )); ?>
			<?php echo $form->error($model,'rewards'); ?>
		</div>
		<div class="col-md-6 rewardsFields" style="display:none;">
			<div class="col-md-4 required">
				<?php echo $form->labelEx($model,'max_conv_amount',array('title'=>'Monto máximo de conversión','data-toggle'=>'tooltip')); ?>
				<?php echo $form->textField($model,'max_conv_amount',array('class'=>'form-control decimal')); ?>
				<?php echo $form->error($model,'max_conv_amount'); ?>
			</div>
			<div class="col-md-4 required">
				<?php echo $form->labelEx($model,'ratio_convertion',array('title'=>'Ratio de Conversión','data-toggle'=>'tooltip')); ?>
				<?php echo $form->textField($model,'ratio_convertion',array('class'=>'form-control decimal')); ?>
				<?php echo $form->error($model,'ratio_convertion'); ?>
			</div>
			<div class="col-md-4 required">
				<?php echo $form->labelEx($model,'percentage_total_amount',array('title'=>'Porcentaje total de Conversión','data-toggle'=>'tooltip')); ?>
				<?php echo $form->textField($model,'percentage_total_amount',array('size'=>10,'maxlength'=>10,'class'=>'form-control decimal')); ?>
				<?php echo $form->error($model,'percentage_total_amount'); ?>
			</div>
		</div>
		
		
	</div>

	</div>

	<div class="box-footer">
		<?php
			$title = (Yii::app()->controller->action->id!='view') ? 'Volver sin guardar cambios' : 'Volver';
			echo '<a href="'.Yii::app()->request->baseUrl.'/'.strtolower(Yii::app()->controller->id).'" class="btn btn-secundary cancel" data-toggle="tooltip" title="'.$title.'" >Cerrar</a>';
			if(Yii::app()->controller->action->id!='view'){
				echo CHtml::submitButton('Guardar',array('class'=>'btn btn-primary','data-toggle'=>'tooltip','title'=>'Guardar cambios'));
			}
		?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
</div>
<?php
	// Activo/Inactivo
	Yii::app()->clientScript->registerCssFile(Yii::app()->params['cdnUrl'].'/plugins/lcswich/lc_switch-tts.css'); 
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/lcswich/lc_switch.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/lcswich/lc_switch-tts.js',CClientScript::POS_END);
	
	
	Yii::app()->clientScript->registerCssFile(Yii::app()->params['cdnUrl'].'/plugins/datepicker/datepicker3.css');
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/datepicker/bootstrap-datepicker.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/input-mask/jquery.inputmask.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/input-mask/jquery.inputmask.date.extensions.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/input-mask/jquery.inputmask.numeric.extensions.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/datepicker/datepicker-tts.js',CClientScript::POS_END);

	echo '<script>viewPage = "'.Yii::app()->controller->action->id.'";</script>';
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/js/form-validators.js',CClientScript::POS_END)
 ?>
<script>
$( document ).ready(function() {	
	<?php
		if(Yii::app()->controller->action->id=='view'){ echo "$('form').find('input, textarea, checkbox, select').attr('disabled','disabled');"; }
	?>
    $('.datepicker').datepicker({
      autoclose: true, 
	  format: 'dd-mm-yyyy',
	  firstDay: 1
    });
if($('.rewardcheck').prop('checked')){
	$('.rewardsFields').show(250);
}
	 $(".decimal").inputmask({ 
	  		'alias': 'decimal', 
	  		//'groupSeparator': ',', 
	  		'autoGroup': true, 
	  		'digits': 2, 
	  		'digitsOptional': false, 
	  		'placeholder': '0.00', 
	  		rightAlign : true,
	  		clearMaskOnLostFocus: !1 
	  	});
	$('.rewardcheck').change(function(){
		if($(this).prop('checked')){
			$('.rewardsFields').show(250);
		}else{
			$('.rewardsFields input').val('');
			$('.rewardsFields').hide(250);
		}
	});

});	
</script>	
