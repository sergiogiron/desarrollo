<?php
/* @var $this TransactioncostController */
/* @var $data Transactioncost */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_payment_method')); ?>:</b>
	<?php echo CHtml::encode($data->id_payment_method); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('dues')); ?>:</b>
	<?php echo CHtml::encode($data->dues); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('interest')); ?>:</b>
	<?php echo CHtml::encode($data->interest); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cft')); ?>:</b>
	<?php echo CHtml::encode($data->cft); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('expire_date_from')); ?>:</b>
	<?php echo CHtml::encode($data->expire_date_from); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('expire_date_to')); ?>:</b>
	<?php echo CHtml::encode($data->expire_date_to); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('enabled')); ?>:</b>
	<?php echo CHtml::encode($data->enabled); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_legal')); ?>:</b>
	<?php echo CHtml::encode($data->id_legal); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('rewards')); ?>:</b>
	<?php echo CHtml::encode($data->rewards); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ratio_convertion')); ?>:</b>
	<?php echo CHtml::encode($data->ratio_convertion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('percentage_total_amount')); ?>:</b>
	<?php echo CHtml::encode($data->percentage_total_amount); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('max_conv_amount')); ?>:</b>
	<?php echo CHtml::encode($data->max_conv_amount); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('interest_hide')); ?>:</b>
	<?php echo CHtml::encode($data->interest_hide); ?>
	<br />

	*/ ?>

</div>