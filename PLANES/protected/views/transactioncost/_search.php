<?php
/* @var $this TransactioncostController */
/* @var $model Transactioncost */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>
<div class="col-md-12" style="clear: both">
<div class="row">
			
			
			<div class="col-md-3">
		<div class="row row_space">
		<?php echo $form->labelEx($model,'id_payment_method'); ?>
		<?php echo $form->dropDownList($model, 'id_payment_method',CHtml::listData(Paymentmethod::model()->findAll(), 'id', 'rules'),array('empty' => 'Todos','class'=>'form-control'));?>
		</div>
	</div>

	<div class="col-md-3">
		<div class="row row_space">
		<?php echo $form->labelEx($model,'id_legal'); ?>
		<?php echo $form->dropDownList($model, 'id_legal',CHtml::listData(Legal::model()->findAll(), 'id', 'title'),array('empty' => 'Todos','class'=>'form-control'));?>
		</div>
	</div>
			
			
			
			<div class="col-md-3">
				<div class="row row_space">
					<?php echo $form->label($model,'dues'); ?>
					<?php echo $form->textField($model,'dues',array('class'=>'form-control')); ?>
				</div>
			</div>
			
			</div>
			<div class="row">
			
			
			<div class="col-md-3">
				<div class="row row_space">
					<?php echo $form->label($model,'expire_date_from'); ?><br>
					<?php echo $form->textField($model,'expire_date_from',array('class'=>'form-control datepicker')); ?>
				</div>
			</div>
			
			<div class="col-md-3">
				<div class="row row_space">
					<?php echo $form->label($model,'expire_date_to'); ?><br>
					<?php echo $form->textField($model,'expire_date_to',array('class'=>'form-control datepicker')); ?>
				</div>
			</div>
			
			<div class="col-md-3">
				<div class="row row_space">
					<?php echo $form->label($model,'enabled'); ?>
					<?php echo $form->checkBox($model,'enabled',array('class'=>'checkbox_form')); ?>
				</div>
			</div>
			
			
			
			<div class="col-md-3">
				<div class="row row_space">
					<?php echo $form->label($model,'rewards'); ?>
					<?php echo $form->checkBox($model,'rewards',array('class'=>'checkbox_form')); ?>
				</div>
			</div>
			
			
			
			
</div>
	<div class="row buttons">
		<?php echo CHtml::submitButton('Buscar',array('class'=>'btn btn-default b_customize')); ?>
	</div>
<br />
<?php $this->endWidget(); ?>
</div>
</div><!-- search-form -->
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/plugins/datepicker/bootstrap-datepicker.js',CClientScript::POS_END); ?>
<script>
$( document ).ready(function() {	
    $('.datepicker').datepicker({
      autoclose: true, 
	  format: 'yyyy-mm-dd',
	  firstDay: 1
    });
});	
</script>	
