<?php
/* @var $this TransactioncostController */
/* @var $model Transactioncost */

Yii::app()->clientScript->registerScript('search', "search(); deleteBtn();"); 
?>
<section class="content-header">
	<?php echo Yii::app()->user->Title(); ?>
	<?php echo Yii::app()->user->Breadcrumb(); ?>
	<?php $this->widget('ext.publicationrecord.PublicationrecordWidget'); ?>
</section>
<section class="content">
	<div class="box box-danger">
		<div class="box-header with-border">	
			<?php			
			if(Yii::app()->user->checkAccess('create')){
				echo '<a href="'.Yii::app()->request->baseUrl.'/'.Yii::app()->controller->id.'/create"class="btn btn-default b_customize"><i class="fa fa-plus i_customize"></i> Agregar </a>';
			}
			?>

			<?php
				if(Yii::app()->user->checkAccess('publish')){		
					echo '<a href="'.Yii::app()->request->baseUrl.'/transactioncost/publish" class="publish btn btn-success a_customize"><i class="fa fa-cloud-upload i_customize"></i>Publicar</a>';
				}
			?>

			
			<?php echo Utils::SearchObject(); ?>	
			<?php echo CHtml::link('Búsqueda avanzada','#',array('class'=>'search-button')); ?>
			<p class="registros">(<span><?=$model->count();?></span> registros encontrados)</p>
			<div class="search-form" style="display:none">
			<?php $this->renderPartial('_search',array(
				'model'=>$model,
			)); ?>
			</div><!-- search-form -->

			<?php $this->widget('GridView', array(
			'id'=>'transactioncost-grid',
			'htmlOptions'=>array('class'=>'grid-view table-responsive'),

			'itemsCssClass' => 'table table-bordered table-striped  table-list',
			
			'pager' => array(
			'cssFile'=>false,
			'header'=> '',
			'firstPageLabel' => '<i class="fa fa-forward fa-flip-horizontal"></i>',
			'prevPageLabel'  => '<i class="fa fa-caret-right fa-flip-horizontal"></i>',
			'nextPageLabel'  => '<i class="fa fa-caret-right"></i>',
			'lastPageLabel'  => '<i class="fa fa-forward"></i>',

			),
			'ajaxUpdate'=>true, //AJAX enabled
                'loadingCssClass'=>'',
				'afterAjaxUpdate'=> 'function(){enableSwitch();deleteBtn(); closeLoading(); getTotalRows();}',
				'summaryText' => '', 
				'itemsCssClass' => 'table table-bordered table-striped table-list',				
				'dataProvider'=>$model->search(),
				'columns'=>array(
				array(
                    'name' => 'enabled',
                    'type' => 'raw',
                    'value' => 'Utils::activeSwitch($data)', 
                    'htmlOptions'=>array('class'=>'activeSwitch'),
                ),	
              	array(
							'name' => 'id_payment_method',
							'value'=>'$data->mediospago==null ? " " : $data->mediospago->rules',
						),
				array(
						'name' => 'dues',
						'value' => '$data->dues',
						'htmlOptions'=>array('style' => 'text-align: right;'),
					),
				array(
						'name' => 'id_legal',
						'value' => '$data->legales->title',
					),
				'expire_date_from',
				'expire_date_to',
				  array(
						'name' => 'rewards',
						 'type' => 'raw',
						 'htmlOptions'=>array('style' => 'text-align: center;'),
						'value' => '$data->rewards == 1 ? \'<i class="fa fa-check" aria-hidden="true"></i>\' : \'<i class="fa fa-close" aria-hidden="true"></i>\' ;',
					),
				  array(
						'name' => 'interest',
						'value' => '$data->interest',
						'htmlOptions'=>array('style' => 'text-align: right;'),
					),
				  array(
						'name' => 'cft',
						'value' => '$data->cft',
						'htmlOptions'=>array('style' => 'text-align: right;'),
					),
				  array(
						'name' => 'ratio_convertion',
						'value' => '$data->ratio_convertion',
						'htmlOptions'=>array('style' => 'text-align: right;'),
					),
				array(
						'class'=>'CButtonColumn',
						'template'=>'{view}{update}{erase}',	
						'buttons'=>array(	
							'view' => array(
								'label'=>'<i class="fa fa-eye icon_cbutton"></i>',
								'url'=>'Yii::app()->createUrl(strtolower(Yii::app()->controller->id).\'/view/id/\'. $data->id)',
								'visible'=>'Yii::app()->user->checkAccess(\'view\')',
								'options'=>array(
									'title'=>'Ver',
									'data-toggle'=>'tooltip',
								),
								'imageUrl'=>false,
							),					
							'update' => array(
								'label'=>'<i class="fa fa-edit icon_cbutton"></i>',
								'visible'=>'Yii::app()->user->checkAccess(\'update\')',
								'url'=>'Yii::app()->createUrl(strtolower(Yii::app()->controller->id).\'/update/id/\'. $data->id)',
								'options'=>array(
									'title'=>'Editar',
									'data-toggle'=>'tooltip',
								),
								'imageUrl'=>false,
							),					
							'erase' => array(
								'visible'=>'Yii::app()->user->checkAccess(\'delete\')',
									'label'=>'<i class="fa fa-trash-o icon_cbutton"></i>',
									'url'=>'Yii::app()->createUrl(strtolower(Yii::app()->controller->id).\'/delete/id/\'. $data->id)',
									'options' => array(
										'class' => 'delete_action',
										'title'=>'Borrar',
										'imageUrl'=>false,
										'data-toggle'=>'tooltip',
									)		
								),	
						),
					),
				)
			)); ?>
		</div>
	</div>
 </section>

  <script>
$(document).ready(function(){
	/*$( ".publish" ).click(function(event){
		event.preventDefault();
		var request = $.ajax({
			url: $(this).attr('href'),
		}).done(function(msg) {
			$('.ligth-box-modal').css('display','block');
			$('.modal-title').html('Publicar');
			$('.modal-body').html(msg);
		});	
	});	*/
});	
</script>

<?php
	// Activo/Inactivo
	
	// Activo/Inactivo
	Yii::app()->clientScript->registerCssFile(Yii::app()->params['cdnUrl'].'/plugins/lcswich/lc_switch-tts.css'); 
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/lcswich/lc_switch.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/lcswich/lc_switch-tts.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl.'/js/plugins/sweetalert/sweetalert.css'); 
	Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/plugins/sweetalert/sweetalert.min.js',CClientScript::POS_END); 
	/*
	// Datepicker con la mascara
	Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl.'/../cdn/plugins/datepicker/datepicker3.css');
	Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/../cdn/plugins/datepicker/bootstrap-datepicker.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/../cdn/plugins/input-mask/jquery.inputmask.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/../cdn/plugins/input-mask/jquery.inputmask.date.extensions.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/../cdn/plugins/datepicker/datepicker-tts.js',CClientScript::POS_END);
	// Timepicker con la mascara
	Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/../cdn/plugins/timepicker/bootstrap-timepicker.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/../cdn/plugins/input-mask/jquery.inputmask.bundle.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl.'/../cdn/plugins/timepicker/bootstrap-timepicker.css');
	Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/../cdn/plugins/timepicker/timepicker-tts.js',CClientScript::POS_END);
	// Editor de texto enriquesido
	Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/../cdn/plugins/ckeditor-standard/ckeditor.js',CClientScript::POS_END);

	*/
 ?>
