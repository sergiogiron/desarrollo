<?php
/* @var $this BusinessexceptionsController */
/* @var $model Businessexceptions */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>
<div class="col-md-12" style="clear: both">
<div class="row">
			
			<div class="col-md-3">
				<div class="row row_space">
					<?php echo $form->label($model,'id'); ?>
					<?php echo $form->textField($model,'id',array('class'=>'form-control')); ?>
				</div>
			</div>
			
			<div class="col-md-3">
				<div class="row row_space">
					<?php echo $form->label($model,'title'); ?>
					<?php echo $form->textArea($model,'title',array('class'=>'form-control ckeditor','rows'=>6,'cols'=>6)); ?>
				</div>
			</div>
			
			<div class="col-md-3">
				<div class="row row_space">
					<?php echo $form->label($model,'description'); ?>
					<?php echo $form->textField($model,'description',array('size'=>45,'maxlength'=>45,'class'=>'form-control')); ?>
				</div>
			</div>
			
			<div class="col-md-3">
				<div class="row row_space">
					<?php echo $form->label($model,'action'); ?>
					<?php echo $form->textField($model,'action',array('size'=>45,'maxlength'=>45,'class'=>'form-control')); ?>
				</div>
			</div>
			
			<div class="col-md-3">
				<div class="row row_space">
					<?php echo $form->label($model,'id_product'); ?>
					<?php echo $form->textField($model,'id_product',array('class'=>'form-control')); ?>
				</div>
			</div>
</div>
	<div class="row buttons">
		<?php echo CHtml::submitButton('Buscar',array('class'=>'btn btn-default b_customize')); ?>
	</div>
<br />
<?php $this->endWidget(); ?>
</div>
</div><!-- search-form -->