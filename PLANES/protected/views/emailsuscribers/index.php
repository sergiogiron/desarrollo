<?php
/* @var $this MailSuscribersController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Mail Suscribers',
);

$this->menu=array(
	array('label'=>'Create MailSuscribers', 'url'=>array('create')),
	array('label'=>'Manage MailSuscribers', 'url'=>array('admin')),
);
?>

<h1>Mail Suscribers</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
