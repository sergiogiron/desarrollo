<?php
/* @var $this EmailsuscribersController */
/* @var $model MailSuscribers */
/* @var $form CActiveForm */
?>
	<div class="box-body">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'email-suscribers-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Los campos con <span class="required">*</span> son requeridos.</p>

	<div class="form-group">
		<?php echo $form->labelEx($model,'email'); ?>
		<?php echo $form->textField($model,'email',array('size'=>60,'maxlength'=>255,'class'=>'form-control')); ?>
		<?php echo $form->error($model,'email'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>255,'class'=>'form-control')); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'lastname'); ?>
		<?php echo $form->textField($model,'lastname',array('size'=>60,'maxlength'=>255,'class'=>'form-control')); ?>
		<?php echo $form->error($model,'lastname'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'admin'); ?>
		<?php		
		$id_user=Yii::app()->user->id;
		$id_filtrados = Yii::app()->db->createCommand("select users.name,users.id from email_assistant inner join users on email_assistant.id_user=users.id where email_assistant.id_assistant=$id_user")->queryAll();
		$user = Yii::app()->db->createCommand("select name,id from users where id=$id_user")->queryRow();
		$id_filtrados[]=array('name'=>$user['name'],'id'=>$user['id']);
		?>
		<?php echo $form->dropDownList($model, 'admin',CHtml::listData($id_filtrados, 'id', 'name'),array('empty' => 'Seleccione','class'=>'form-control'));?>
		<?php echo $form->error($model,'admin'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'classified'); ?>
		<?php echo $form->textField($model,'classified',array('class'=>'form-control')); ?>
		<?php echo $form->error($model,'classified'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'origen'); ?>
		<?php echo $form->textField($model,'origen',array('size'=>60,'maxlength'=>255,'class'=>'form-control')); ?>
		<?php echo $form->error($model,'origen'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'share'); ?>
		<?php echo $form->Checkbox($model,'share',array('class'=>'checkbox_form')); ?>
		<?php echo $form->error($model,'share'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'suscripto'); ?>
		<?php echo $form->checkBox($model,'suscripto',array('class'=>'checkbox_form')); ?>
		<?php echo $form->error($model,'suscripto'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'admin_status'); ?>
		<?php echo $form->textField($model,'admin_status',array('class'=>'form-control')); ?>
		<?php echo $form->error($model,'admin_status'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'total_errors'); ?>
		<?php echo $form->textField($model,'total_errors',array('class'=>'form-control')); ?>
		<?php echo $form->error($model,'total_errors'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'start_date'); ?>
		<?php echo $form->textField($model,'start_date',array('class'=>'form-control datepicker')); ?>
		<?php echo $form->error($model,'start_date'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'date_unsubscribe'); ?>
		<?php echo $form->textField($model,'date_unsubscribe',array('class'=>'form-control datepicker')); ?>
		<?php echo $form->error($model,'date_unsubscribe'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'description'); ?>
		<?php echo $form->textArea($model,'description',array('rows'=>6, 'cols'=>50,'class'=>'form-control')); ?>
		<?php echo $form->error($model,'description'); ?>
	</div>

  <div class="box-footer">
	<?php echo CHtml::submitButton($model->isNewRecord ? 'Crear' : 'Guardar',array('class'=>'btn btn-primary')); ?>
  </div>
  </div>
<?php $this->endWidget(); ?>
<?php Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl.'/plugins/datepicker/datepicker3.css'); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/plugins/datepicker/bootstrap-datepicker.js',CClientScript::POS_END); ?>
<script>
$( document ).ready(function() {
    $('.datepicker').datepicker({
      autoclose: true, 
	  format: 'yyyy-mm-dd',
	  firstDay: 1
    });
});	
</script>
