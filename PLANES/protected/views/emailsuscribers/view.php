<?php
/* @var $this MailSuscribersController */
/* @var $model MailSuscribers */

$this->breadcrumbs=array(
	'Mail Suscribers'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List MailSuscribers', 'url'=>array('index')),
	array('label'=>'Create MailSuscribers', 'url'=>array('create')),
	array('label'=>'Update MailSuscribers', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete MailSuscribers', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage MailSuscribers', 'url'=>array('admin')),
);
?>

<h1>View MailSuscribers #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'mail',
		'nombre',
		'apellido',
		'administrador',
		'clasificado',
		'origen',
		'dk',
		'compartido',
		'estado_envio',
		'estado_admin',
		'errores_acum',
		'lista',
		'fecha_alta',
		'fecha_desuscripcion',
		'observaciones',
		'userstamp',
		'datestamp',
	),
)); ?>
