<?php
/* @var $this ContactsController */
/* @var $model Contacts */
 
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#contacts-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});


"); 
?>
<section class="content-header">
  <h1>
	Contactos
  </h1>
  <ol class="breadcrumb">
	<li><i class="fa fa-home"></i> Inicio</li>
	<li>Emailing</li>
  </ol>
</section>

	<section class="content">
		<div class="box box-danger">
		<div class="box-header with-border">
	 
<div class="nav-tabs">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#tab_1" data-toggle="tab">Resumen</a></li>
              <li><a href="#tab_2" data-toggle="tab">Administración</a></li> 
               
            </ul>
			 <div class="tab-content">
              <div class="tab-pane active" id="tab_1">
		  
<div style="background-color: #eee !important; overflow: auto; padding: 5px 0;">
			<div class="col-md-3" style="text-align:center; padding: 10px 0;">
				Contactos Propios:<br /><b style="font-size: 50px;"> <?php echo !empty($contactos_totales)?$contactos_totales:0; ?></b>
			</div>
			<?php if(!empty($contactos_totales)){?>
			<div class="col-md-3">
				 <?php				
					$this->widget('ext.highcharts.HighchartsWidget', array(
						'scripts' => array(
							#'modules/exporting',
							'themes/grid-light',
						),
						'options' => array(
							'title' => array(
								'text' => '',
							),
							'chart' => array(
								'plotBackgroundColor' => '#eee',
								'plotBorderWidth' => null,
								'plotShadow' => false,
								'height' => 150, 
							  ),
							'plotOptions'=> array( 'pie'=>array('shadow'=>false,'center'=>array(['50%','50%']))),
							'tooltip'=>array(
								'headerFormat'=> '',
								'pointFormat'=> '{point.name}<br /><strong>{point.y}</strong> ({point.dataPer}%)',
								'backgroundColor'=> 'rgba(0, 0, 0, 0.85)',
								'borderColor'=>  '#000',
								'borderRadius'=>  5,
								'style'=>array( 'color'=> '#e0e0e0','fontSize'=> '13px','padding'=> '6px 8px'),
							),
							
							'credits' => array('enabled' => false),
									
							'series' => array(			
								array(
									'type' => 'pie',
									'name' => 'Contactos',
									'data' => array(
										array(
											'name' => 'Suscritos',
											'y' => round(!empty($contactos_suscriptos)?$contactos_suscriptos:0),
											'dataPer'=> round(!empty($contactos_suscriptos)?$contactos_suscriptos*100/$contactos_totales:0),
											'color'=> '#edc240',
											'dataLabels'=>  array('enabled'=>false),
										),
										array(
											'name' => 'Desuscritos',
											'y' => round(!empty($contactos_desuscriptos)?$contactos_desuscriptos:0),
											'dataPer'=> round(!empty($contactos_desuscriptos)?$contactos_desuscriptos*100/$contactos_totales:0),
											'color'=>'#cb744a',
											'dataLabels'=>  array('enabled'=> false),
										),
										array(
											'name' => 'Invalidos',
											'y' => round(!empty($contactos_invalidos)?$contactos_invalidos:0),
											'dataPer'=> round(!empty($contactos_invalidos)?$contactos_invalidos*100/$contactos_totales:0),
											'color' =>  '#cb4b4a',
											'dataLabels' =>  array('enabled'=> false),
										),
										array(
											'name' => 'Bloqueados',
											'y' => round(!empty($contactos_bloqueados)?$contactos_bloqueados:0),
											'dataPer'=> round(!empty($contactos_bloqueados)?$contactos_bloqueados*100/$contactos_totales:0),
											'color' =>  '#378BEB',
											'dataLabels' =>  array('enabled'=> false),
										),
										array(
											'name' => 'Desuscritos por el usuario',
											'y' => round(!empty($contactos_desuscriptosxusr)?$contactos_desuscriptosxusr:0),
											'dataPer'=> round(!empty($contactos_desuscriptosxusr)?$contactos_desuscriptosxusr*100/$contactos_totales:0),
											'color' =>  '#E837EB',
											'dataLabels' =>  array('enabled'=> false),
										),
									),
									'innerSize' => '55%'
								),
							),
						)
					));
				?>
			</div>
			
			
			<div class="col-md-3" style="padding: 15px !important;">
				<ul class="list-unstyled">
					<li>Total suscriptos: <b><?php echo !empty($contactos_suscriptos)?$contactos_suscriptos:0; ?> </b>(<small><?php echo round(!empty($contactos_suscriptos)?$contactos_suscriptos*100/$contactos_totales:0);?> %</small>)</li>
					<li>Total desuscriptos: <b><?php echo !empty($contactos_desuscriptos)?$contactos_desuscriptos:0; ?> </b>(<small><?php echo round(!empty($contactos_desuscriptos)?$contactos_desuscriptos*100/$contactos_totales:0);?> %</small>)</li>
					<li>Total inválidos: <b><?php echo !empty($contactos_invalidos)?$contactos_invalidos:0; ?> </b>(<small><?php echo round(!empty($contactos_invalidos)?$contactos_invalidos*100/$contactos_totales:0);?> %</small>)</li>
					<li>Total bloqueados: <b><?php echo !empty($contactos_bloqueados)?$contactos_bloqueados:0; ?> </b>(<small><?php echo round(!empty($contactos_bloqueados)?$contactos_bloqueados*100/$contactos_totales:0);?> %</small>)</li>
					<li>Desuscripto por el usuario: <b><?php echo !empty($contactos_desuscriptosxusr)?$contactos_desuscriptosxusr:0; ?> </b>(<small><?php echo round(!empty($contactos_desuscriptosxusr)?$contactos_desuscriptosxusr*100/$contactos_totales:0);?> %</small>)</li>
				</ul>			
			</div>
			<?php } ?>
</div>

<?php if(!empty($array_contadores)){
	
	foreach($array_contadores as $key => $val){
		?>
		<div style="background-color: #eee !important; overflow: auto; padding: 5px 0;">
			<div class="col-md-3" style="text-align:center; padding: 10px 0;">Contactos de <b><?php echo $key; ?></b>:<br /><b style="font-size: 50px;"> <?php echo !empty($val['contactos_totales'])?$val['contactos_totales']:0; ?></b></div>
		<?php if (!empty($val['contactos_totales'])){?>	
			<div class="col-md-3">
				 <?php				
					$this->widget('ext.highcharts.HighchartsWidget', array(
						'scripts' => array(
							#'modules/exporting',
							'themes/grid-light',
						),
						'options' => array(
							'title' => array(
								'text' => '',
							),
							'chart' => array(
								'plotBackgroundColor' => '#eee',
								'plotBorderWidth' => null,
								'plotShadow' => false,
								'height' => 150, 
							  ),
							'plotOptions'=> array( 'pie'=>array('shadow'=>false,'center'=>array(['50%','50%']))),
							'tooltip'=>array(
								'headerFormat'=> '',
								'pointFormat'=> '{point.name}<br /><strong>{point.y}</strong> ({point.dataPer}%)',
								'backgroundColor'=> 'rgba(0, 0, 0, 0.85)',
								'borderColor'=>  '#000',
								'borderRadius'=>  5,
								'style'=>array( 'color'=> '#e0e0e0','fontSize'=> '13px','padding'=> '6px 8px'),
							),
							
							'credits' => array('enabled' => false),
									
							'series' => array(			
								array(
									'type' => 'pie',
									'name' => 'Contactos',
									'data' => array(
										array(
											'name' => 'Suscritos',
											'y' => round(!empty($val['contactos_suscriptos'])?$val['contactos_suscriptos']:0),
											'dataPer'=> round(!empty($val['contactos_suscriptos'])?$val['contactos_suscriptos']*100/$val['contactos_totales']:0),
											'color'=> '#edc240',
											'dataLabels'=>  array('enabled'=>false),
										),
										array(
											'name' => 'Desuscritos',
											'y' => round(!empty($val['contactos_desuscriptos'])?$val['contactos_desuscriptos']:0),
											'dataPer'=> round(!empty($val['contactos_desuscriptos'])?$val['contactos_desuscriptos']*100/$val['contactos_totales']:0),
											'color'=>'#cb744a',
											'dataLabels'=>  array('enabled'=> false),
										),
										array(
											'name' => 'Invalidos',
											'y' => round(!empty($val['contactos_invalidos'])?$val['contactos_invalidos']:0),
											'dataPer'=> round(!empty($val['contactos_invalidos'])?$val['contactos_invalidos']*100/$val['contactos_totales']:0),
											'color' =>  '#cb4b4a',
											'dataLabels' =>  array('enabled'=> false),
										),
										array(
											'name' => 'Bloqueados',
											'y' => round(!empty($val['contactos_bloqueados'])?$val['contactos_bloqueados']:0),
											'dataPer'=> round(!empty($val['contactos_bloqueados'])?$val['contactos_bloqueados']*100/$val['contactos_totales']:0),
											'color' =>  '#378BEB',
											'dataLabels' =>  array('enabled'=> false),
										),
										array(
											'name' => 'Desuscritos por el usuario',
											'y' => round(!empty($val['contactos_desuscriptosxusr'])?$val['contactos_desuscriptosxusr']:0),
											'dataPer'=> round(!empty($val['contactos_desuscriptosxusr'])?$val['contactos_desuscriptosxusr']*100/$val['contactos_totales']:0),
											'color' =>  '#E837EB',
											'dataLabels' =>  array('enabled'=> false),
										),
									),
									'innerSize' => '55%'
								),
							),
						)
					));
				?>
			</div>
			
			
			<div class="col-md-3" style="padding: 10px !important;">
				<ul class="list-unstyled">
					<li>Total suscriptos: <b><?php echo !empty($val['contactos_suscriptos'])?$val['contactos_suscriptos']:0; ?> </b> (<small><?php echo round(!empty($val['contactos_suscriptos'])?$val['contactos_suscriptos']*100/$val['contactos_totales']:0);?> %</small>)</li>
					<li>Total desuscriptos: <b><?php echo !empty($val['contactos_desuscriptos'])?$val['contactos_desuscriptos']:0; ?> </b> (<small><?php echo round(!empty($val['contactos_desuscriptos'])?$val['contactos_desuscriptos']*100/$val['contactos_totales']:0);?> %</small>)</li>
					<li>Total inválidos: <b><?php echo !empty($val['contactos_invalidos'])?$val['contactos_invalidos']:0; ?> </b> (<small><?php echo round(!empty($val['contactos_invalidos'])?$val['contactos_invalidos']*100/$val['contactos_totales']:0);?> %</small>)</li>
					<li>Total bloqueados: <b><?php echo !empty($val['contactos_bloqueados'])?$val['contactos_bloqueados']:0; ?> </b> (<small><?php echo round(!empty($val['contactos_bloqueados'])?$val['contactos_bloqueados']*100/$val['contactos_totales']:0);?> %</small>)</li>
					<li>Desuscripto por el usuario: <b><?php echo !empty($val['contactos_desuscriptosxusr'])?$val['contactos_desuscriptosxusr']:0; ?> </b> (<small><?php echo round(!empty($val['contactos_desuscriptosxusr'])?$val['contactos_desuscriptosxusr']*100/$val['contactos_totales']:0);?> %</small>)</li>
				</ul>
			
			</div>
			<?php } ?>
		 
			
		</div>
<?php }} ?>

</div> 
<div class="tab-pane" id="tab_2">
<!--<div class="pull-right" style="margin: 10px 0 !important; ">-->
			<?php echo Utils::SearchObject(); ?>
<!--</div>-->
<?php $arrayDataProvider = new CArrayDataProvider($contactosiframe, array(
    'id'=>'contactos',
    'keyField' => 'id',
    'keys'=>array('id','nombre', 'email'),
    'sort'=>array(
        'attributes'=>array(
            'nombre', 
			'email',
			),
		),
    'pagination'=>array(
        'pageSize'=>15,
		),
)); ?>
 
					  
		<?php $this->widget('zii.widgets.grid.CGridView', array(
			'id'=>'contacts-grid',
			'summaryText' => '', 
			'dataProvider'=>$arrayDataProvider,
			'ajaxUrl' => $this->createUrl('adminiframe'),
			'itemsCssClass' => 'table table-bordered table-striped table-list',
			'enableSorting'=>true,
			'pager' => array(
					'cssFile'=>false,
					'header'=> '',
					'firstPageLabel' => '<i class="fa fa-forward fa-flip-horizontal"></i>',
					'prevPageLabel'  => '<i class="fa fa-caret-right fa-flip-horizontal"></i>',
					'nextPageLabel'  => '<i class="fa fa-caret-right"></i>',
					'lastPageLabel'  => '<i class="fa fa-forward"></i>',
				),
			//'filter'=>$model,
			'columns'=>array(
				array(
					'name' => 'id',
					'headerHtmlOptions' => array('style' => 'display:none'),
					'htmlOptions' => array('style' => 'display:none'),
				),			
				array(
					'name' => 'nombre',
					'header' => 'Nombre',
				),				
				array(
					'name' => 'email',
					'header' => 'Email',
					'cssClassExpression'=>'"mailaddr-".$data["id"]',
				), 
				array(
					'name' => 'admin',
					'header' => 'Administrador',
					'value' => '$data["nombreAdmin"]',
				),
				array(
					'header' => 'Suscrito',
					'name' => 'suscripto',
					'type' => 'raw',
					'value' => '($data["suscripto"] <= 1 ? ($data["suscripto"] == 0 ?   ($data["prohibido"]?\'<div><input type="checkbox" class="suscripto-\'.$data["id"].\'" checked disabled></div>\': \'<div><input type="checkbox" class="suscripto-\'.$data["id"].\'" checked></div>\') : ($data["prohibido"]?\'<div><input type="checkbox" class="suscripto-\'.$data["id"].\'" disabled></div>\':\'<div><input type="checkbox" class="suscripto-\'.$data["id"].\'" ></div>\')) : ($data["suscripto"] == 2 ? \'Invalido\' : ($data["suscripto"] == 3 ? \'Bloqueado\' : ($data["suscripto"] == 4 ? \'Desuscripto por el Usuario\':\'Faltan Datos\'))));',
				),
				array(
					'header' => 'Compartido',
					'name' => 'compartido',
					'type' => 'raw',
					'value' => '($data["compartido"] == 1 || $data["compartido"] == true)? \'<div><input type="checkbox" class="compartido-\'.$data["id"].\'" checked ></div>\' : \'<div><input type="checkbox" class="compartido-\'.$data["id"].\'" ></div>\' ;',
				), 
			),
			'afterAjaxUpdate'=>'function getchecks(){
									$(":checkbox").on("click", function(){										 
										var myClass = $(this).attr("class");
										$("."+myClass).disabled = true;
										var res = myClass.split("-");
										if(res[0]=="suscripto"){
											if($(this).is(\':checked\'))
											{
													$("."+myClass).hide();
													var request = $.ajax({
														type: "POST", 
														url: "'.Yii::app()->request->baseUrl.'/iframe/contactos/suscribir/"+res[1]+"/'.$usernameiframe.'",
														success: function(msg){
															  swal("Solicitud realizada con exito","\""+msg+"\"","success"); 
															  
															  },
												    	error: function(err){   
															  swal("Error:","\""+err.responseText.split(":")[2]+"\"","error"); } 
													}).done(function(msg) {
													
														
													})
													$("."+myClass).css("display", "block"); 
											}
											else
												{
												var request = $.ajax({
													type: "POST", 
													url: "'.Yii::app()->request->baseUrl.'/iframe/contactos/desuscribir/"+res[1]+"/'.$usernameiframe.'",
													success: function(msg){
														  swal("Solicitud realizada con exito","\""+msg+"\"","success"); 
														  
														  },
													error: function(err){   
														  swal("Error:","\""+err.responseText.split(":")[2]+"\"","error"); } 
												}).done(function(msg) {
												 
												})
												$("."+myClass).hide();
												$("."+myClass).css("display", "block");
												}
										}
										else if(res[0]=="compartido")
											{
											if($(this).is(\':checked\'))
											{
												var request = $.ajax({
													type: "POST", 
													url: "'.Yii::app()->request->baseUrl.'/iframe/contactos/compartir/"+res[1]+"/'.$usernameiframe.'",
													success: function(msg){
														  swal("Solicitud realizada con exito","\""+msg+"\"","success"); 
														  
														  },
													error: function(err){   
														 swal("Error:","\""+err.responseText.split(":")[2]+"\"","error"); } 
												}).done(function(msg) {
												 
												})
											}
											else
												{
												var request = $.ajax({
													type: "POST", 
													url: "'.Yii::app()->request->baseUrl.'/iframe/contactos/ocultar/"+res[1]+"/'.$usernameiframe.'",
													success: function(msg){
														  swal("Solicitud realizada con exito","\""+msg+"\"","success"); 
														  
														  },
													error: function(err){   
														  swal("Error:","\""+err.responseText.split(":")[2]+"\"","error");} 
												}).done(function(msg) {
												 
												})
												} 
											} 
									} ); 
									}',
		)); ?>
		</div>
              
            </div>
            <!-- /.tab-content -->
          </div>
		  
		</div>
		</div>
 </section> 
<?php Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl.'/js/plugins/sweetalert/sweetalert.css'); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/plugins/sweetalert/sweetalert.min.js',CClientScript::POS_END); 
echo '<script>viewPage = "'.Yii::app()->controller->action->id.'";</script>';
?>

 <script>
 
 $( document ).ready(function() {
	 
	$( "#buscar" ).click(function(){
		$('#contacts-grid').yiiGridView('update', {
			data: { criterio : $('#criterio').val(), 'username':<?php echo '\''.$usernameiframe.'\''; ?> }
		});
		return false;
	});	
	$("#criterio").keypress(function(e){
       if(e.which == 13) {
          $("#buscar").trigger('click');
       }
    });
	
	$(".right-icon-search").click(function(){
        $(this).parent().find("input").val('');
        $(this).css('color','#ccc');
		$('#buscar').trigger('click');	
    });
	
	$(":checkbox").on("click", function(){		
		var myClass = $(this).attr("class");
		$("."+myClass).disabled = true;
		var res = myClass.split("-");
		var mail = $('.mailaddr-'+res[1]).text();
		if(res[0]=="suscripto"){
			if($(this).is(":checked"))
			{
					$("."+myClass).hide();
					var request = $.ajax({
						type: "POST", 
						url: "<?php echo(Yii::app()->request->baseUrl);?>/iframe/contactos/suscribir/"+res[1]+"/<?php echo $usernameiframe.'"';?>,
						success: function(msg){
							  swal("Solicitud realizada con exito.","\""+msg+"\"","success"); 							  
							 },
						error: function(err){   
							 swal("Error:","\""+err.responseText.split(":")[2]+"\"","error"); 		
							  } 
					}).done(function(msg) {
					})
					$("."+myClass).css("display", "block"); 
			}
			else
				{
				var request = $.ajax({
					type: "POST", 
					url: "<?php echo(Yii::app()->request->baseUrl);?>/iframe/contactos/desuscribir/"+res[1]+"/<?php echo $usernameiframe.'"'; ?>,
					success: function(msg){
						  swal("Solicitud realizada con exito.","\""+msg+"\"","success"); 						  
						  },
					error: function(err){   
						 swal("Error:","\""+err.responseText.split(":")[2]+"\"","error"); 		
							  } 
				}).done(function(msg) {
				})
				$("."+myClass).css("display", "block");
				} 
		}
		else if(res[0]=="compartido")
			{
			if($(this).is(':checked'))
			{
				var request = $.ajax({
						type: "POST", 
						url: "<?php echo(Yii::app()->request->baseUrl);?>/iframe/contactos/compartir/"+res[1]+"/<?php echo $usernameiframe.'"'; ?>,
						success: function(msg){
							  swal("Solicitud realizada con exito.","\""+msg+"\"","success"); 							  
							  },
						error: function(err){   
							  swal("Error:","\""+err+"\"","error"); 		
							  } 
					}).done(function(msg) {
					})
			}
			else
				{
				var request = $.ajax({
						type: "POST", 
						url: "<?php echo(Yii::app()->request->baseUrl);?>/iframe/contactos/ocultar/"+res[1]+"/<?php echo $usernameiframe.'"'; ?>,
						success: function(msg){
							 swal("Solicitud realizada con exito.","\""+msg+"\"","success"); 				  
							  },
						error: function(err){   
							  swal("Error:","\""+err+"\"","error"); 		
							  } 
					}).done(function(msg) {
					})
				}
			} 
	} ); 
});			 
</script>