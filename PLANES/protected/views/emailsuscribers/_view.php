<?php
/* @var $this MailSuscribersController */
/* @var $data MailSuscribers */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('mail')); ?>:</b>
	<?php echo CHtml::encode($data->mail); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nombre')); ?>:</b>
	<?php echo CHtml::encode($data->nombre); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('apellido')); ?>:</b>
	<?php echo CHtml::encode($data->apellido); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('administrador')); ?>:</b>
	<?php echo CHtml::encode($data->administrador); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('clasificado')); ?>:</b>
	<?php echo CHtml::encode($data->clasificado); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('origen')); ?>:</b>
	<?php echo CHtml::encode($data->origen); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('dk')); ?>:</b>
	<?php echo CHtml::encode($data->dk); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('compartido')); ?>:</b>
	<?php echo CHtml::encode($data->compartido); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('estado_envio')); ?>:</b>
	<?php echo CHtml::encode($data->estado_envio); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('estado_admin')); ?>:</b>
	<?php echo CHtml::encode($data->estado_admin); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('errores_acum')); ?>:</b>
	<?php echo CHtml::encode($data->errores_acum); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('lista')); ?>:</b>
	<?php echo CHtml::encode($data->lista); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fecha_alta')); ?>:</b>
	<?php echo CHtml::encode($data->fecha_alta); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fecha_desuscripcion')); ?>:</b>
	<?php echo CHtml::encode($data->fecha_desuscripcion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('observaciones')); ?>:</b>
	<?php echo CHtml::encode($data->observaciones); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('userstamp')); ?>:</b>
	<?php echo CHtml::encode($data->userstamp); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('datestamp')); ?>:</b>
	<?php echo CHtml::encode($data->datestamp); ?>
	<br />

	*/ ?>

</div>