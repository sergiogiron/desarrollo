<?php
$this->breadcrumbs=array(
	'Contacts'=>array('index'),
	'Create',
);
$this->menu=array(
	array('label'=>'List Contacts', 'url'=>array('index')),
	array('label'=>'Manage Contacts', 'url'=>array('admin')),
);
?>
<section class="content-header">
  <h1>
	Contactos
  </h1>
  <ol class="breadcrumb">
	<li><a href="<?php echo Yii::app()->request->baseUrl; ?>"><i class="fa fa-dashboard"></i> Home</a></li>
	<li><a href="<?php echo Yii::app()->request->baseUrl; ?>/contactos">Contactos</a></li>
  </ol>
</section>
<section class="content">
	<div class="box box-danger">
		<div class="box-header with-border">
			<?php $this->renderPartial('_form', array('model'=>$model)); ?>
		</div>
	</div>
</section>