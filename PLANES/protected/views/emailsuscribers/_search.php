<?php
/* @var $this MailSuscribersController */
/* @var $model MailSuscribers */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'mail'); ?>
		<?php echo $form->textField($model,'mail',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'nombre'); ?>
		<?php echo $form->textField($model,'nombre',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'apellido'); ?>
		<?php echo $form->textField($model,'apellido',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'administrador'); ?>
		<?php echo $form->textField($model,'administrador',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'clasificado'); ?>
		<?php echo $form->textField($model,'clasificado'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'origen'); ?>
		<?php echo $form->textField($model,'origen',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'dk'); ?>
		<?php echo $form->textField($model,'dk'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'compartido'); ?>
		<?php echo $form->textField($model,'compartido'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'suscripto'); ?>
		<?php echo $form->textField($model,'suscripto'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'estado_admin'); ?>
		<?php echo $form->textField($model,'estado_admin'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'errores_acum'); ?>
		<?php echo $form->textField($model,'errores_acum'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'lista'); ?>
		<?php echo $form->textField($model,'lista'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'fecha_alta'); ?>
		<?php echo $form->textField($model,'fecha_alta'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'fecha_desuscripcion'); ?>
		<?php echo $form->textField($model,'fecha_desuscripcion'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'observaciones'); ?>
		<?php echo $form->textArea($model,'observaciones',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'userstamp'); ?>
		<?php echo $form->textField($model,'userstamp',array('size'=>16,'maxlength'=>16)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'datestamp'); ?>
		<?php echo $form->textField($model,'datestamp'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->