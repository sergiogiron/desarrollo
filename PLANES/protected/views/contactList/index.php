<section class="content-header">
    <h1>
	Base de Contactos
	<small>Contactos</small>
  </h1>
    <ol class="breadcrumb">
        <li><a href="/ttsoffice_frontend"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="/ttsoffice_frontend/basecontactos/admin">Base de Contactos</a></li>
        <li class="active">Contactos</li>
    </ol>
</section>
<section class="content">
    <div class="box box-danger">
        <div class="box-header with-border">
            <a href="/ttsoffice_frontend/basecontactos/crear" class="btn btn-default b_customize"><i class="fa fa-plus i_customize"></i> Agregar contacto</a>

            <div id="contacts-grid" class="grid-view">
                <div class="summary"></div>
                <table class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th id="contacts-grid_c0"><a class="sort-link" href="/ttsoffice_frontend/contactos/admin?EmailSuscribers_sort=name">Nombre</a></th>
                            <th id="contacts-grid_c1"><a class="sort-link" href="/ttsoffice_frontend/contactos/admin?EmailSuscribers_sort=lastname">Apellido</a></th>
                            <th id="contacts-grid_c2"><a class="sort-link" href="/ttsoffice_frontend/contactos/admin?EmailSuscribers_sort=email">N° Documento</a></th>
                            <th id="contacts-grid_c3"><a class="sort-link" href="/ttsoffice_frontend/contactos/admin?EmailSuscribers_sort=admin">Pais Residencia</a></th>
                            <th class="button-column" id="contacts-grid_c6">&nbsp;</th>
                        </tr>
                        <tr class="filters">
                            <td>
                                <input name="name" type="text" maxlength="255">
                            </td>
                            <td>
                                <input name="lastname" type="text" maxlength="255">
                            </td>
                            <td>
                                <input name="documento" type="text" maxlength="255">
                            </td>
                            <td>
                                <input name="residencia" type="text" maxlength="100">
                            </td>
                            <td>&nbsp;</td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="odd">
                          <td>Ariel</td>
                          <td>Astorga</td>
                          <td>19.333.666</td>
                          <td>AR</td>
                          <td class="button-column">
                            <a title="Borrar contacto" class="delete" href="#"><i class="fa fa-trash-o icon_cbutton"></i></a>
                            <a title="Editar contacto" href="/ttsoffice_frontend/basecontactos/modificar"><i class="fa fa-edit icon_cbutton"></i></a>
                          </td>
                        </tr>
                        <tr class="even">
                          <td>Carlos</td>
                          <td>Altamirano</td>
                          <td>26.777.666</td>
                          <td>AR</td>
                          <td class="button-column">
                            <a title="Borrar contacto" class="delete" href="#"><i class="fa fa-trash-o icon_cbutton"></i></a>
                            <a title="Editar contacto" href="/ttsoffice_frontend/basecontactos/modificar"><i class="fa fa-edit icon_cbutton"></i></a>
                          </td>
                        </tr>
                        <tr class="odd">
                          <td>Marta</td>
                          <td>Deliz</td>
                          <td>12.663.111</td>
                          <td>AR</td>
                          <td class="button-column">
                            <a title="Borrar contacto" class="delete" href="#"><i class="fa fa-trash-o icon_cbutton"></i></a>
                            <a title="Editar contacto" href="/ttsoffice_frontend/basecontactos/modificar"><i class="fa fa-edit icon_cbutton"></i></a>
                          </td>
                        </tr>
                        <tr class="even">
                          <td>Silvia</td>
                          <td>Pedelez</td>
                          <td>29.333.666</td>
                          <td>AR</td>
                          <td class="button-column">
                            <a title="Borrar contacto" class="delete" href="#"><i class="fa fa-trash-o icon_cbutton"></i></a>
                            <a title="Editar contacto" href="/ttsoffice_frontend/basecontactos/modificar"><i class="fa fa-edit icon_cbutton"></i></a>
                          </td>
                        </tr>
                        <tr class="odd">
                          <td>Godofredo</td>
                          <td>Medero</td>
                          <td>33.123.689</td>
                          <td>AR</td>
                          <td class="button-column">
                            <a title="Borrar contacto" class="delete" href="#"><i class="fa fa-trash-o icon_cbutton"></i></a>
                            <a title="Editar contacto" href="/ttsoffice_frontend/basecontactos/modificar"><i class="fa fa-edit icon_cbutton"></i></a>
                          </td>
                        </tr>
                        <tr class="even">
                          <td>Mateo</td>
                          <td>Plomin</td>
                          <td>12.625.167</td>
                          <td>AR</td>
                          <td class="button-column">
                            <a title="Borrar contacto" class="delete" href="#"><i class="fa fa-trash-o icon_cbutton"></i></a>
                            <a title="Editar contacto" href="/ttsoffice_frontend/basecontactos/modificar"><i class="fa fa-edit icon_cbutton"></i></a>
                          </td>
                        </tr>
                        <tr class="odd">
                          <td>David</td>
                          <td>Lopez</td>
                          <td>16.143.776</td>
                          <td>AR</td>
                          <td class="button-column">
                            <a title="Borrar contacto" class="delete" href="#"><i class="fa fa-trash-o icon_cbutton"></i></a>
                            <a title="Editar contacto" href="/ttsoffice_frontend/basecontactos/modificar"><i class="fa fa-edit icon_cbutton"></i></a>
                          </td>
                        </tr>
                        <tr class="even">
                          <td>Carla</td>
                          <td>Sand</td>
                          <td>17.153.946</td>
                          <td>AR</td>
                          <td class="button-column">
                            <a title="Borrar contacto" class="delete" href="#"><i class="fa fa-trash-o icon_cbutton"></i></a>
                            <a title="Editar contacto" href="/ttsoffice_frontend/basecontactos/modificar"><i class="fa fa-edit icon_cbutton"></i></a>
                          </td>
                        </tr>
                    </tbody>
                </table>
                <div class="keys" style="display:none" title="/ttsoffice_frontend/emailsuscribers/admin"><span>2</span><span>10</span><span>12</span></div>
            </div>
        </div>
    </div>
</section>
