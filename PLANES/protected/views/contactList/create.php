<style media="screen">
.contact-editor form{
  display: none;
}
.contact-editor form.active{
  display: block;
}

</style>

<section class="content-header">
    <h1>
  Base de Contactos
  <small>Crear Contacto</small>
  </h1>
    <ol class="breadcrumb">
        <li><a href="/ttsoffice_frontend"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="/ttsoffice_frontend/basecontactos/admin">Base de Contactos</a></li>
        <li class="active"><a href="/ttsoffice_frontend/basecontactos/crear">Crear</a></li>
    </ol>
</section>

<section class="content contact-editor">
    <div class="box box-danger">
        <div class="box-header with-border">
          <div class="nav-tabs">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#tab_1"  data-toggle="tab" >Contacto</a></li>
              <li><a href="#tab_2" data-toggle="tab">Email</a></li>
              <li><a href="#tab_3" data-toggle="tab">Teléfono</a></li>
              <li><a href="#tab_4" data-toggle="tab">Domicilio</a></li>
              <li><a href="#tab_5" data-toggle="tab">Red Social</a></li>
              <li><a href="#tab_6" data-toggle="tab">Pasajero Frecuente</a></li>
              <li><a href="#tab_7" data-toggle="tab">Etiquetas</a></li>
            </ul>
          </div>
          <div class="tab-content">
            <div class="tab-pane active" id="tab_1">
              <div class="box-body">
                  <form id="contact-form" class="clearfix show" action="/ttsoffice_frontend/basecontactos/crear" method="post">
                      <p class="note">Los campos con <span class="required">*</span> son requeridos.</p>

                      <div class="form-group col-md-6 col-xs-12">
                          <label for="name" class="required">Nombre <span class="required">*</span></label>
                          <input size="60" maxlength="255" class="form-control" name="name" id="name" type="text" required>
                      </div>

                      <div class="form-group col-md-6 col-xs-12">
                          <label for="lastname" class="required">Apellido <span class="required">*</span></label>
                          <input size="60" maxlength="255" class="form-control" name="lastname" id="lastname" type="text" required>
                      </div>

                      <div class="form-group col-md-6 col-xs-12">
                          <label for="fechanacimiento">Fecha Nacimiento <span class="required">*</span></label>
                          <input size="60" maxlength="255" class="form-control datepicker" name="fechanacimiento" id="fechanacimiento" type="text" required>
                      </div>

                      <div class="form-group col-md-6 col-xs-12">
                          <label for="paisnacimiento">Pais Nacimiento</label>
                          <input size="60" maxlength="255" class="form-control" name="paisnacimiento" id="paisnacimiento" type="text">
                      </div>

                      <div class="form-group col-md-6 col-xs-12">
                          <label for="paisnacimiento">Pais Residencia</label>
                          <input size="60" maxlength="255" class="form-control" name="paisresidencia" id="paisresidencia" type="text">
                      </div>

                      <div class="form-group col-md-6 col-xs-12">
                          <label for="sexo" class="required">Sexo <span class="required">*</span></label>
                          <select class="form-control" name="sexo" id="sexo">
                              <option value="M">Masculino</option>
                              <option value="F">Femenino</option>
                          </select>
                      </div>

                      <div class="form-group col-md-6 col-xs-12">
                          <label for="tipodocumento" class="required">Tipo Documento <span class="required">*</span></label>
                          <select class="form-control" name="tipodocumento" id="tipodocumento">
                              <option value="">Seleccione</option>
                              <option value="CU">CU</option>
                              <option value="CUIL">CUIL</option>
                              <option value="DNI">DNI</option>
                              <option value="PASP">PASP</option>
                          </select>
                      </div>

                      <div class="form-group col-md-6 col-xs-12">
                          <label for="documento" class="required">Numero de Documento <span class="required">*</span></label>
                          <input size="60" maxlength="255" class="form-control" name="documento" id="documento" type="text">
                      </div>

                      <div class="form-group col-md-6 col-xs-12">
                          <label for="pasaporte">Pasaporte</label>
                          <input size="60" maxlength="255" class="form-control" name="pasaporte" id="pasaporte" type="text">
                      </div>

                      <div class="form-group col-md-6 col-xs-12">
                          <label for="pasaportepor">Pasaporte Emitido por</label>
                          <input size="60" maxlength="255" class="form-control" name="pasaportepor" id="pasaportepor" type="text">
                      </div>

                      <div class="form-group col-md-6 col-xs-12">
                          <label for="pasaportefin">Pasaporte Vencimiento</label>
                          <input size="60" maxlength="255" class="form-control datepicker" name="pasaportefin" id="pasaportefin" type="text">
                      </div>

                      <div class="form-group col-md-6 col-xs-12">
                          <label for="admin" class="required">Administrador <span class="required">*</span></label>
                          <select class="form-control" name="admin" id="admin" required>
                              <option value="">Seleccione</option>
                              <option value="">Walter</option>
                              <option value="19">Ariel</option>
                          </select>
                      </div>

                      <div class="form-group col-md-6 col-xs-12">
                          <label for="origen">Origen <span class="required">*</span></label>
                          <input size="60" maxlength="255" class="form-control" name="origen" id="origen" type="text" required>
                      </div>

                      <div class="form-group col-xs-12">
                          <label for="description">Comentarios</label>
                          <textarea rows="6" cols="50" class="form-control" name="comentarios" id="comentarios"></textarea>
                      </div>
                  </form>
              </div>
            </div>
            <div class="tab-pane" id="tab_2">
              <div class="box-body">
                <table class="table table-bordered table-striped">
                  <thead>
                      <tr>
                        <th>Tipo de Mail</th>
                        <th>E-Mail</th>
                        <th>Origen del mail</th>
                      </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>

                <div class="box-footer">
                    <input class="btn btn-primary" type="button" name="agregar" value="Agregar">
                </div>

                <form id="email-contact-form" class="clearfix" action="/ttsoffice_frontend/basecontactos/crear" method="post">
                    <p class="note">Los campos con <span class="required">*</span> son requeridos.</p>
                    <div class="form-group col-md-4 col-xs-12">
                        <label for="tipomail" class="required">Tipo de Mail <span class="required">*</span></label>
                        <select class="form-control" name="tipomail" id="tipomail" required>
                            <option value="P">Personal</option>
                            <option value="L">Laboral</option>
                            <option value="C">Contacto</option>
                        </select>
                    </div>
                    <div class="form-group col-md-4 col-xs-12">
                        <label for="email" class="required">E-Mail <span class="required">*</span></label>
                        <input size="60" maxlength="255" class="form-control" name="email" id="email" type="email" required pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" >
                    </div>
                    <div class="form-group col-md-4 col-xs-12">
                        <label for="origen">Origen del mail</label>
                        <input size="60" maxlength="255" class="form-control" name="origenmail" id="origenmail" type="text">
                    </div>
                    <div class="box-footer clearfix col-xs-12">
                        <input class="btn btn-primary" type="submit" name="save" value="Guardar">
                        <input class="btn btn-primary" type="submit" name="savemore" value="Guardar y agregar otro">
                        <input class="btn btn-default" type="button" name="cancel" value="Cancelar">
                    </div>
                </form>
              </div>
            </div>
            <div class="tab-pane" id="tab_3">
              <div class="box-body">
                <table class="table table-bordered table-striped">
                  <thead>
                      <tr>
                        <th>Tipo de Teléfono</th>
                        <th>Pais</th>
                        <th>DDN</th>
                        <th>Teléfono</th>
                        <th>Interno</th>
                        <th>Origen</th>
                      </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>

                <div class="box-footer">
                    <input class="btn btn-primary" type="button" name="agregar" value="Agregar">
                </div>

                <form id="phone-contact-form" class="clearfix" action="/ttsoffice_frontend/basecontactos/crear" method="post">
                    <p class="note">Los campos con <span class="required">*</span> son requeridos.</p>

                    <div class="form-group col-md-3 col-xs-12">
                        <label for="tipotel" class="required">Tipo de Teléfono <span class="required">*</span></label>
                        <select class="form-control" name="tipotel" id="tipotel">
                            <option value="C">Contacto</option>
                            <option value="P">Personal</option>
                            <option value="F">Fax</option>
                            <option value="L">Laboral</option>
                            <option value="CP">Celular Particular</option>
                            <option value="CL">Celular Laboral</option>
                        </select>
                    </div>

                    <div class="form-group col-md-2 col-xs-12">
                        <label for="paistel" class="required">País <span class="required">*</span></label>
                        <input size="60" maxlength="255" class="form-control" name="paistel" id="paistel" type="text" required>
                    </div>

                    <div class="form-group col-md-1 col-xs-12">
                        <label for="ddntel" class="required" >DDN <span class="required">*</span></label>
                        <input size="60" maxlength="255" class="form-control" name="ddntel" id="ddntel" type="text" required>
                    </div>

                    <div class="form-group col-md-2 col-xs-12">
                        <label for="telefono" class="required">Teléfono <span class="required">*</span></label>
                        <input size="60" maxlength="255" class="form-control" name="telefono" id="telefono" type="text" required>
                    </div>

                    <div class="form-group col-md-2 col-xs-12">
                        <label for="interno">Interno</label>
                        <input size="60" maxlength="255" class="form-control" name="interno" id="interno" type="text">
                    </div>

                    <div class="form-group col-md-2 col-xs-12">
                        <label for="origen">Origen</label>
                        <input size="60" maxlength="255" class="form-control" name="origen" id="origen" type="text">
                    </div>

                    <div class="box-footer col-xs-12">
                        <input class="btn btn-primary" type="submit" name="save" value="Guardar">
                        <input class="btn btn-primary" type="submit" name="savemore" value="Guardar y agregar otro">
                        <input class="btn btn-default" type="button" name="cancel" value="Cancelar">
                    </div>
                </form>
              </div>
            </div>
            <div class="tab-pane" id="tab_4">
              <div class="box-body">
                <table class="table table-bordered table-striped">
                  <thead>
                      <tr>
                        <th>Tipo de Domicilio</th>
                        <th>Calle</th>
                        <th>Altura</th>
                        <th>Torre</th>
                        <th>Piso</th>
                        <th>Depto</th>
                        <th>País</th>
                        <th>Provincia</th>
                        <th>Localidad</th>
                        <th>Codigo Postal</th>
                        <th>Ciudad</th>
                        <th>Municipio</th>
                        <th>Origen</th>
                      </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>

                <div class="box-footer">
                    <input class="btn btn-primary" type="button" name="agregar" value="Agregar">
                </div>

                <form id="address-contact-form" class="clearfix" action="/ttsoffice_frontend/basecontactos/crear" method="post">
                    <p class="note">Los campos con <span class="required">*</span> son requeridos.</p>

                    <div class="form-group col-md-2 col-xs-12">
                        <label for="tipotel" class="required">Tipo de Domicilio <span class="required">*</span></label>
                        <select class="form-control" name="tipotel" id="tipotel" required>
                            <option value="C">Contacto</option>
                            <option value="L">Laboral</option>
                            <option value="C">Correspondencia</option>
                        </select>
                    </div>

                    <div class="form-group col-md-4 col-xs-12">
                        <label for="calle" class="required">Calle <span class="required">*</span></label>
                        <input size="60" maxlength="255" class="form-control" name="calle" id="calle" type="text" required>
                    </div>

                    <div class="form-group col-md-2 col-xs-12">
                        <label for="altura">Altura</label>
                        <input size="60" maxlength="255" class="form-control" name="altura" id="altura" type="text">
                    </div>

                    <div class="form-group col-md-2 col-xs-12">
                        <label for="torre">Torre</label>
                        <input size="60" maxlength="255" class="form-control" name="torre" id="torre" type="text">
                    </div>

                    <div class="form-group col-md-2 col-xs-12">
                        <label for="piso">Piso</label>
                        <input size="60" maxlength="255" class="form-control" name="piso" id="piso" type="text">
                    </div>

                    <div class="form-group col-md-3 col-xs-12">
                        <label for="depto">Depto</label>
                        <input size="60" maxlength="255" class="form-control" name="depto" id="depto" type="text">
                    </div>

                    <div class="form-group col-md-3 col-xs-12">
                        <label for="pais" class="required">País <span class="required">*</span></label>
                        <input size="60" maxlength="255" class="form-control" name="pais" id="pais" type="text" required>
                    </div>

                    <div class="form-group col-md-3 col-xs-12">
                        <label for="provincia" class="required">Provincia <span class="required">*</span></label>
                        <input size="60" maxlength="255" class="form-control" name="provincia" id="provincia" type="text" required>
                    </div>

                    <div class="form-group col-md-3 col-xs-12">
                        <label for="localidad">Localidad</label>
                        <input size="60" maxlength="255" class="form-control" name="localidad" id="localidad" type="text">
                    </div>

                    <div class="form-group col-md-2 col-xs-12">
                        <label for="cpostal" class="required">Codigo Postal <span class="required">*</span></label>
                        <input size="60" maxlength="255" class="form-control" name="cpostal" id="cpostal" type="text">
                    </div>

                    <div class="form-group col-md-4 col-xs-12">
                        <label for="ciudad">Ciudad</label>
                        <input size="60" maxlength="255" class="form-control" name="ciudad" id="ciudad" type="text">
                    </div>

                    <div class="form-group col-md-4 col-xs-12">
                        <label for="municipio">Municipio</label>
                        <input size="60" maxlength="255" class="form-control" name="municipio" id="municipio" type="text">
                    </div>

                    <div class="form-group col-md-2 col-xs-12">
                        <label for="origen">Origen</label>
                        <input size="60" maxlength="255" class="form-control" name="origen" id="origen" type="text">
                    </div>

                    <div class="box-footer col-xs-12">
                        <input class="btn btn-primary" type="submit" name="save" value="Guardar">
                        <input class="btn btn-primary" type="submit" name="savemore" value="Guardar y agregar otro">
                        <input class="btn btn-default" type="button" name="cancel" value="Cancelar">
                    </div>

                </form>
              </div>
            </div>
            <div class="tab-pane" id="tab_5">
              <div class="box-body">

                <table class="table table-bordered table-striped">
                  <thead>
                      <tr>
                        <th>Tipo de Red Social</th>
                        <th>Nombre de Usuario</th>
                        <th>Origen</th>
                      </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>

                <div class="box-footer">
                    <input class="btn btn-primary" type="button" name="agregar" value="Agregar">
                </div>
                <form id="social-contact-form" class="clearfix" action="/ttsoffice_frontend/basecontactos/crear" method="post">
                    <p class="note">Los campos con <span class="required">*</span> son requeridos.</p>

                    <div class="form-group col-md-4 col-xs-12">
                        <label for="tiposocial" class="required">Tipo de Red Social <span class="required">*</span></label>
                        <select class="form-control" name="tiposocial" id="tiposocial" required>
                            <option value="F">Facebook</option>
                            <option value="T">Twitter</option>
                            <option value="I">Instagram</option>
                            <option value="S">Snaptchat</option>
                            <option value="l">Linkedin</option>
                        </select>
                    </div>

                    <div class="form-group col-md-4 col-xs-12">
                        <label for="socialuser" class="required">Nombre de Usuario <span class="required">*</span></label>
                        <input size="60" maxlength="255" class="form-control" name="socialuser" id="socialuser" type="text" required>
                    </div>

                    <div class="form-group col-md-4 col-xs-12">
                        <label for="origen">Origen</label>
                        <input size="60" maxlength="255" class="form-control" name="socialorigen" id="socialorigen" type="text">
                    </div>

                    <div class="box-footer col-xs-12">
                        <input class="btn btn-primary" type="submit" name="save" value="Guardar">
                        <input class="btn btn-primary" type="submit" name="savemore" value="Guardar y agregar otro">
                        <input class="btn btn-default" type="button" name="cancel" value="Cancelar">
                    </div>
                </form>
              </div>
            </div>
            <div class="tab-pane" id="tab_6">
              <div class="box-body">
                <table class="table table-bordered table-striped">
                  <thead>
                      <tr>
                        <th>Número de pasajero frecuente</th>
                        <th>ID Proveedor</th>
                        <th>Borrado</th>
                        <th>Origen</th>
                      </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>

                <div class="box-footer">
                    <input class="btn btn-primary" type="button" name="agregar" value="Agregar">
                </div>
                <form id="frecuent-contact-form" class="clearfix" action="/ttsoffice_frontend/basecontactos/crear" method="post">
                    <p class="note">Los campos con <span class="required">*</span> son requeridos.</p>

                    <div class="form-group col-md-3 col-xs-12">
                        <label for="idfrecuente" class="required">Número de pasajero frecuente <span class="required">*</span></label>
                        <input size="60" maxlength="255" class="form-control" name="idfrecuente" id="idfrecuente" type="text" required>
                    </div>

                    <div class="form-group col-md-3 col-xs-12">
                        <label for="idproveedor" class="required">ID Proveedor <span class="required">*</span></label>
                        <input size="60" maxlength="255" class="form-control" name="idproveedor" id="idproveedor" type="text" required>
                    </div>

                    <div class="form-group col-md-3 col-xs-12">
                        <label for="borrado" class="required">Borrado <span class="required">*</span></label>
                        <select class="form-control" name="borrado" id="borrado">
                            <option value="SI">SI</option>
                            <option value="NO">NO</option>
                        </select>
                    </div>

                    <div class="form-group col-md-3 col-xs-12">
                        <label for="origen">Origen</label>
                        <input size="60" maxlength="255" class="form-control" name="origen" id="origen" type="text">
                    </div>

                    <div class="box-footer col-xs-12">
                        <input class="btn btn-primary" type="submit" name="save" value="Guardar">
                        <input class="btn btn-primary" type="submit" name="savemore" value="Guardar y agregar otro">
                        <input class="btn btn-default" type="button" name="cancel" value="Cancelar">
                    </div>
                </form>
              </div>
            </div>
            <div class="tab-pane" id="tab_7">
              <div class="box-body">
                <table class="table table-bordered table-striped">
                  <thead>
                      <tr>
                        <th>Etiqueta</th>
                        <th>Descripción</th>
                        <th>Valores</th>
                        <th>Fecha</th>
                      </tr>
                  </thead>
                  <tbody>

                  </tbody>
                </table>

                <div class="box-footer">
                    <input class="btn btn-primary" type="button" name="agregar" value="Agregar">
                </div>

                <form id="label-contact-form" class="clearfix" action="/ttsoffice_frontend/basecontactos/modificar" method="post">
                  <div class="form-group col-md-4 col-xs-12">
                      <label for="etiqueta" class="required">Etiqueta <span class="required">*</span></label>
                      <select class="form-control" name="etiqueta" id="etiqueta" required>
                          <option value="miami">MIAMI</option>
                          <option value="datelastbuy">FECHA ULTIMA COMPRA</option>
                          <option value="segment">SEGMENTO</option>
                          <option value="familygroup">GRUPO FAMILIAR</option>
                      </select>
                  </div>
                  <div class="form-group insert col-md-4 col-xs-12">
                      <label for="description">Descripción</label>
                      <input size="60" maxlength="255" class="form-control" name="description" id="description" type="text" readonly="readonly">
                  </div>
                  <div class="form-group col-md-3 col-xs-12 hide">
                      <label for="date">Fecha</label>
                      <input size="60" maxlength="255" class="form-control" name="date" id="date" type="hidden" readonly="readonly">
                  </div>

                  <div class="box-footer col-xs-12">
                      <input class="btn btn-primary" type="submit" name="save" value="Guardar">
                      <input class="btn btn-primary" type="submit" name="savemore" value="Guardar y agregar otro">
                      <input class="btn btn-default" type="button" name="cancel" value="Cancelar">
                  </div>
                </form>
              </div>
            </div>
            <div class="box-footer col-xs-12">
                <input class="btn btn-primary" type="submit" name="yt0" value="Crear">
                <a href="/ttsoffice_frontend/basecontactos/admin" class="btn btn-default">Cancelar</a>
            </div>
          </div>
        </div>
      </div>
    </div>
</section>

<?php Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl.'/plugins/datepicker/datepicker3.css'); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/plugins/datepicker/bootstrap-datepicker.js',CClientScript::POS_END); ?>
<?php Yii::app()->clientScript->registerCoreScript('jquery.ui') ?>

<script type="text/javascript">
  function datepickerInit(){
    $('.datepicker').datepicker({
        autoclose: true,
        format: 'yyyy-mm-dd',
        firstDay: 1
    });
  }
  $(document).ready(function() {
      datepickerInit();
  });

  $('.contact-editor').on('click', 'input[name="agregar"]', function(ev){
    var box = $(ev.target).parent().parent();
    var form = $(box.find('form')[0]);
    form.removeAttr('novalidate');
    form.show();
  });

  $('form').on('click', 'input[name=save]', function(ev){
    addRecord(ev.target, ev, true);
  });

  $('form').on('click', 'input[name=savemore]', function(ev){
    addRecord(ev.target,ev, false);
  });

  $('form').on('click', 'input[name=cancel]', function(ev){
    $(this).parent().parent().hide();
  });

  function addRecord(elm, event, close = false){
    event.preventDefault();
    var target = elm;
    var form = $(target).parent().parent()[0];
    var formGroup = $(form).find('.form-group');
    var record = document.createElement('tr');
    var flag = false;
    var editRemove = '<td class="button-column">'+
                        '<a title="Borrar contacto" class="delete" href="#" onclick="removeItem(event);return false;"><i class="fa fa-trash-o icon_cbutton"></i></a>' +
                        '<a title="Editar contacto" href="#" onclick="modifyItem(event);return false;"><i class="fa fa-edit icon_cbutton"></i></a>' +
                      '</td>';
    $.each(formGroup, function(key, elm){
      var child = $(elm).children().eq(1)[0];
      var value;
      if (!$(child).is(':valid') && $(child).attr('required')) flag = true;
      value = (child.nodeName.toLowerCase() === "select") ?  child.options[child.selectedIndex].text :  child.value;
      $(record).append('<td>'+ value +'</td>');
    });
    $(record).append(editRemove);
    if (!flag) {
      event.preventDefault();
      $(form).parent().find('tbody').append(record);
      if (!$(form).is('#label-contact-form')) {
        form.reset();
      }
    } else {
      return false;
    }
    if (close) {
      $(form).attr('novalidate','');
      $(form).hide();
    }
  }

  function removeItem(){
    if (confirm('¿Desea borrar el registro?'))  $(event.target).closest('tr').remove();
  }

  function modifyItem(){
    var box = $(event.target).closest('.box-body');
    var tbody = $(event.target).closest('tr').find('td').not('.button-column');
    var formInputs = box.find('form').find('.form-group .form-control');
    $.each(tbody, function(key, value){
      var nodeType =  formInputs[key].nodeName.toLowerCase();
      var valor = $(value).text();
      var current = formInputs[key];
      if (nodeType === 'select'){
        $.each(formInputs.children(), function(key, value){
          if (valor === value.label) current.selectedIndex = key;
        });
      } else if (nodeType ==='input') {
        formInputs[key].value = valor;
      }
    });
    var form = $(box.find('form')[0]);
    form.removeAttr('novalidate');
    $(event.target).closest('tr').remove();
    form.show();
  }

  var selectTpl ='<div class="form-group col-md-3 col-xs-12">' +
                    '<label for="valores">Valores</label>' +
                    '<select class="form-control" name="etiqueta" id="etiqueta" required>' +
                    '</select>' +
                  '</div>';

  var datepickerTpl = '<div class="form-group col-md-3 col-xs-12">' +
                        '<label for="datevalue">Fecha</label>' +
                        '<input size="60" maxlength="255" class="form-control datepicker" name="datevalue" id="datevalue" type="text">' +
                      '</div>';

  var booleanTpl = '<div class="form-group col-md-3 col-xs-12 hide">' +
                        '<label for="boolvalue">Fecha</label>' +
                        '<input size="60" maxlength="255" class="form-control" name="boolvalue" id="boolvalue" type="hidden" value="true">' +
                      '</div>';

  $('#label-contact-form select[name=etiqueta]').on('change', function(ev){
    var selection = this.options[this.selectedIndex].value;
    var form = $(this).parent().parent();
    switch (selection) {
      case 'miami':
        form.find('input[name=description]').val('Descripción de los mas naranjas');
        form.find('.form-group.insert').next().remove();
        $(booleanTpl).insertAfter($('#label-contact-form').find('.form-group.insert'));
      break;
      case 'segment':
        form.find('input[name=description]').val('Peleas que nunca van a ir');
        form.find('.form-group.insert').next().remove();
        // Emulated DATA
        var arr = [
          {val : 1, text: 'Alto'},
          {val : 2, text: 'Medio'},
          {val : 3, text: 'Economico'}
        ];
        var sTpl = $(selectTpl);
        var sel = sTpl.find('select');
        $(arr).each(function() {
         sel.append($("<option>").attr('value',this.val).text(this.text));
        });
        // end Emulated Data
        $(sTpl).insertAfter($('#label-contact-form').find('.form-group.insert'));
      break;
      case 'datelastbuy':
        form.find('input[name=description]').val('Comercio que nunca vende');
        form.find('.form-group.insert').next().remove();
        $(datepickerTpl).insertAfter($('#label-contact-form').find('.form-group.insert'));
        datepickerInit();
      break;
      case 'familygroup':
        form.find('input[name=description]').val('Despensa que siempre vuela;');
        form.find('.form-group.insert').next().remove();
        // Emulated DATA
        var arr = [
          {val : 1, text: 'Soltero'},
          {val : 2, text: 'Casado'},
          {val : 3, text: 'Casado con hijos'}
        ];
        var sTpl = $(selectTpl);
        var sel = sTpl.find('select');
        $(arr).each(function() {
         sel.append($("<option>").attr('value',this.val).text(this.text));
        });
        // end Emulated Data
        $(sTpl).insertAfter($('#label-contact-form').find('.form-group.insert'));
      break;
      default:
    }
    form.find('input[name=date]').val('2016-03-29');
  });


  // Valores de relleno
  $('#label-contact-form').find('input[name=description]').val('Descripción de los mas naranjas');
  $('#label-contact-form').find('input[name=date]').val('2016-06-14');
  $(booleanTpl).insertAfter($('#label-contact-form').find('.form-group.insert'));
</script>
