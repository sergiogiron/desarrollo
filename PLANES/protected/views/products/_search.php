<?php
/* @var $this ProductsController */
/* @var $model Products */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="col-md-12" style="clear: both">
	<div class="row">

	<div class="col-md-4">
	<div class="row row_space">
		<?php echo $form->label($model,'title'); ?>
		<?php echo $form->textField($model,'title',array('size'=>45,'maxlength'=>45)); ?>
	</div>
	</div>

	<div class="col-md-4">
	<div class="row row_space">
		<?php echo $form->label($model,'enabled'); ?>
		<?php echo $form->checkBox($model,'enabled',array('class'=>'checkbox_form')); ?>
	</div>
	</div>

	</div>
	<div class="row buttons">
		<?php echo CHtml::submitButton('Buscar',array('class'=>'btn btn-default b_customize')); ?>
	</div>
<br />
<?php $this->endWidget(); ?>

</div>
</div><!-- search-form -->