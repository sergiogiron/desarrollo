<?php
/* @var $this MailFooterController */
/* @var $model MailFooter */

$this->breadcrumbs=array(
	'Mail Footers'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List MailFooter', 'url'=>array('index')),
	array('label'=>'Create MailFooter', 'url'=>array('create')),
	array('label'=>'Update MailFooter', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete MailFooter', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage MailFooter', 'url'=>array('admin')),
);
?>
<section class="content-header">
  <h1>
	Pies de Email
  </h1>
  <ol class="breadcrumb">
	<li><a href="<?php echo Yii::app()->request->baseUrl; ?>"><i class="fa fa-home"></i> Inicio</a></li>
	<li>Emailing</li>
  </ol>
</section>
<section class="content">
	<div class="box box-danger">
		<div class="box-header with-border">
			<?php $this->renderPartial('_form', array('model'=>$model)); ?>		</div>
	</div>
</section>

 
