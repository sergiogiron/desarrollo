<?php
/* @var $this MailFooterController */
/* @var $model MailFooter */

Yii::app()->clientScript->registerScript('search', "
	deleteBtn();
	search();
	searchAdd();
 
");
?>
<section class="content-header">
	<?php echo Yii::app()->user->Title(); ?>
	<?php echo Yii::app()->user->Breadcrumb(); ?>
	
</section>
	<section class="content">
		<div class="box box-danger">
		<div class="box-header with-border">
			<?php
			if(Yii::app()->user->checkAccess('create')){
				echo '<a href="'.Yii::app()->request->baseUrl.'/mailfooter/crear"class="btn btn-default b_customize"><i class="fa fa-plus i_customize"></i> Agregar</a>';
			}
			?>
			<?php echo Utils::SearchObject(); ?>
			<?php echo CHtml::link('Búsqueda avanzada','#',array('class'=>'search-button')); ?>
			<p class="registros">(<?=$model->countByAttributes(array('deleted'=> 0 ));?> registros encontrados)</p>
			<div class="search-form" style="display:none">
			<?php $this->renderPartial('_search',array(
				'model'=>$model,
			)); ?>
			</div><!-- search-form -->			
			<?php $this->widget('GridView', array(
				'id'=>'mail-footer-grid',
				'dataProvider'=>$model->search(),
				'summaryText' => '', 
				'itemsCssClass' => 'table table-bordered table-striped table-list',
				//'filter'=>$model,
				'pager' => array(
					'cssFile'=>false,
					'header'=> '',
					'firstPageLabel' => '<i class="fa fa-forward fa-flip-horizontal" data-toggle="tooltip" title="Primera página"></i>',
					'prevPageLabel'  => '<i class="fa fa-caret-right fa-flip-horizontal" data-toggle="tooltip" title="Anterior"></i>',
					'nextPageLabel'  => '<i class="fa fa-caret-right" data-toggle="tooltip" title="Siguiente"></i>',
					'lastPageLabel'  => '<i class="fa fa-forward" data-toggle="tooltip" title="Última página"></i>', 
				),
				'ajaxUpdate'=>true,
				'afterAjaxUpdate'=> 'function(){
					enableSwitch();
					deleteBtn();
					closeLoading(); 
					getTotalRows();
                }',
				'columns'=>array(
					array(
							'name' => 'enabled',
							'type' => 'raw',
							'value' => 'Utils::activeSwitch($data)',
							'htmlOptions'=>array('class'=>'activeSwitch'),

							 
						),
					'name',/*
					array(
							'name'=>'created_at',
							'filter'=>CHtml::activeTextField($model,'created_at'),
							 'value'=>'Yii::app()->dateFormatter->format("d/M/y",strtotime($data->created_at))'
						),	*/
					array(
						'class'=>'CButtonColumn',
						'template'=>'{view}{update}{erase}',	
						'buttons'=>array(
							'view' => array(
									'label'=>'<i class="fa fa-eye icon_cbutton"></i>',
									'url'=>'Yii::app()->createUrl(strtolower(Yii::app()->controller->id).\'/view/id/\'. $data->id)',
									'options'=>array('title'=>'Ver'),
									#'visible'=>'Yii::app()->user->checkAccess(\'view\')',
									'imageUrl'=>false,
							),						
							'update' => array(
								'label'=>'<i class="fa fa-edit icon_cbutton"></i>',
								'url'=>'Yii::app()->createUrl(\'mailfooter/modificar/\'. $data->id)',
								'visible'=>'Yii::app()->user->checkAccess(\'update\')',
								'options'=>array('title'=>'Editar'),
								'imageUrl'=>false,
							),							
							'erase' => array(
								'visible'=>'Yii::app()->user->checkAccess(\'delete\')',
									'label'=>'<i class="fa fa-trash-o icon_cbutton"></i>',
									'url'=>'Yii::app()->createUrl(strtolower(Yii::app()->controller->id).\'/delete/id/\'. $data->id)',
									'options' => array(
										'class' => 'delete_action',
										'title'=>'Borrar',
										'imageUrl'=>false,
									)	
								),

							)
						)
					),
				)); ?>
			</div>
		</div>
 </section>
  <?php
	// Activo/Inactivo
	Yii::app()->clientScript->registerCssFile(Yii::app()->params['cdnUrl'].'/plugins/lcswich/lc_switch-tts.css'); 
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/lcswich/lc_switch.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/lcswich/lc_switch-tts.js',CClientScript::POS_END);
 ?>
  <script>
$(document).ready(function(){
	$( "#buscar" ).click(function(){
		$('#mail-footer-grid').yiiGridView('update', {
			data: { criterio : $('#criterio').val() }
		});
		return false;
	});	
	$("#criterio").keypress(function(e){
       if(e.which == 13) {
          $("#buscar").trigger('click');
       }
    });	
});	
</script>