<?php
/* @var $this MailFooterController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Mail Footers',
);

$this->menu=array(
	array('label'=>'Create MailFooter', 'url'=>array('create')),
	array('label'=>'Manage MailFooter', 'url'=>array('admin')),
);
?>

<h1>Mail Footers</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
