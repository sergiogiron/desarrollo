<?php
/* @var $this MailFooterController */
/* @var $model MailFooter */
/* @var $form CActiveForm */
?>
<div class="box-body">
	<div>
		<?php $form=$this->beginWidget('CActiveForm', array(
			'id'=>'email-footer-form',
			// Please note: When you enable ajax validation, make sure the corresponding
			// controller action is handling ajax validation correctly.
			// There is a call to performAjaxValidation() commented in generated controller code.
			// See class documentation of CActiveForm for details on this.
			'enableAjaxValidation'=>false,
		)); ?>
			<span class="right_f">
					<?php echo $form->checkBox($model,'enabled',array('class'=>'lcs_check enabled')); ?>
			</span> 

			<div class="form-group col-md-8 unique">
				<?php echo $form->labelEx($model,'name'); ?>
				<?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>255,'class'=>'form-control required')); ?>
				<?php echo $form->error($model,'name'); ?>
			</div>

			<div class="form-group col-md-12">
				<?php echo $form->labelEx($model,'content'); ?>
				<?php echo $form->textArea($model,'content',array('rows'=>6, 'cols'=>50,'class'=>'form-control ckeditor required')); ?>
				<?php echo $form->error($model,'content'); ?>
			</div>
		  
			<div class="box-footer">
				<?php
					echo CHtml::button('Cerrar', array('onclick' => 'js:document.location.href="'.Yii::app()->request->baseUrl.'/'.strtolower(Yii::app()->controller->id).'"','class'=>'btn btn-secundary','data-toggle'=>'tooltip','title'=>'Volver sin guardar cambios'));
					if(Yii::app()->controller->action->id!='view'){
						echo CHtml::submitButton($model->isNewRecord ? 'Crear' : 'Guardar',array('class'=>'btn btn-primary','data-toggle'=>'tooltip','title'=>'Guardar cambios'));
					}?>
			</div>
	</div>
 
</div>
<?php $this->endWidget(); ?>
<?php
	echo '<script>viewPage = "'.Yii::app()->controller->action->id.'";</script>';
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/js/form-validators.js',CClientScript::POS_END);
	// Activo/Inactivo
	Yii::app()->clientScript->registerCssFile(Yii::app()->params['cdnUrl'].'/plugins/lcswich/lc_switch-tts.css'); 
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/lcswich/lc_switch.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/lcswich/lc_switch-tts.js',CClientScript::POS_END);

	Yii::app()->clientScript->registerScriptFile('https://cdn.ckeditor.com/4.5.7/standard/ckeditor.js',CClientScript::POS_END);	
	?>

<script>
$( document ).ready(function() { 
	CKEDITOR.replace( 'Emailfooter_content', {
		language: 'es',
		allowedContent: true,
		/*uiColor: '#9AB8F3',*/
		toolbar: [    
		{ name: 'clipboard', groups: [ 'clipboard', 'undo' ], items: [ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ] },	
		{ name: 'document', groups: [ 'mode', 'document', 'doctools' ], items: [ 'Source', '-', 'Save', 'NewPage', 'Preview', 'Print', '-', 'Templates' ] },
		{ name: 'links', items: [ 'Link', 'Unlink', 'Anchor' ] },
		{ name: 'insert', items: [ 'Image', 'Flash', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe' ] },	
		{ name: 'editing', groups: [ 'find', 'selection', 'spellchecker' ], items: [ 'Find', 'Replace', '-', 'SelectAll', '-', 'Scayt' ] },
		{ name: 'forms', items: [ 'Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField' ] },	
		'/',
		{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat' ] },
		{ name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl', 'Language' ] },
		{ name: 'styles', items: [ 'Styles', 'Format', 'Font', 'FontSize' ] },
		{ name: 'colors', items: [ 'TextColor', 'BGColor' ] },
		{ name: 'tools', items: [ 'Maximize', 'ShowBlocks' ] }    
		],  
	});
});	
</script>
