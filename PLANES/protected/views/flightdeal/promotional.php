<!doctype html>
<html>
	<head>
		<meta http-equiv="Content-type" content="text/html; charset=utf-8">
		<script type="text/javascript" >
		<?php if($reward==1){ echo "var access_point='dHRzaHNiYw==';"; }else { echo "var access_point='dHRzdmlhamVz';"; } ?>
		$(document).ready(function(){		
			promotionalShopping(access_point).done(function(result){

				$.each( result, function( key, data ) {	
					if(data.promoOrigen=='BUE'){
						var ciudadorigen='Buenos Aires';
					}else{
						var ciudadorigen=data.promoCiudadOrigen;
					}
					<?php if($reward==1){ echo "var cambio='<td>'+data.promoPrecio+'</td>';"; echo "var attr_re='data-points='+data.promoPrecio+' data-type=2';"; }else { echo "var cambio='<td>'+data.promoMoneda+'</td><td>'+data.promoPrecio+'</td>';"; echo "var attr_re='data-type=1 data-currency='+data.promoMoneda+' data-precio='+data.promoPrecio+'';"; } ?>
					$('#content').append( '<tr><td>'+data.promoOrigen+' - '+ciudadorigen+'</td><td>'+data.promoDestino+' - '+data.promoCiudadDestino+'</td><td>'+data.promoAerolinea+' - '+data.promoAerolineaName+'</td><td>'+data.promoSalida+'</td>'+cambio+'<td><a href="http://e-flights.net/promotionalShopping/showPromotion.php?c=dHRzdmlhamVz&promid='+data.promoID+'" target="_blank" class="return_link" '+attr_re+' data-origen="'+data.promoAerolinea+'">Seleccionar</a></td></tr>');
				});
				$('#myTable').DataTable( {
					"language": {
						"lengthMenu": "Mostrar _MENU_ resultados",
						"infoEmpty":      "Showing 0 to 0 of 0 entries",
						"search":         "Buscar:",
						"zeroRecords": "Nothing found - sorry",
						"info": "Mostrando pagina _PAGE_ de _PAGES_",
						"infoEmpty": "No se encontraron resultados.",
						"paginate": {
								"first":      "Primera",
								"last":       "Ultima",
								"next":       "Siguiente",
								"previous":   "Anterior"
							},						
						"infoFiltered": "(filtered from _MAX_ total records)"
					},	drawCallback: function() {
				        $( ".return_link" ).on( "click", function(event) {
								event.preventDefault();
								$('#Flightdeal_url').val($(this).attr('href'));					
								$('#link2').css('display','table-cell');				
								$('#link2').attr('href',$(this).attr('href'));					
								$('#content_result_img').html('');
								$('.ligth-box-modal-images').css('display','none');
								if($(this).attr('data-type')=='2'){
									var puntos=$(this).attr('data-points');
									puntos=puntos.replace(".", "");
									$('#Flightdeal_total_points').val(puntos);						
								}else{
									var precio=$(this).attr('data-precio');
									precio=precio.replace(".", "");
									$('#Flightdeal_price').val(precio);
									//$('#type_currency').val($(this).attr('data-currency'));
								}
								openLoading();
								$.ajax({
								  method: "POST",
								  url: homeurl+"/flightdeal/origencode/code/"+$(this).attr('data-origen'),
								  data:{ id_subchannel: select_val }
								}).done(function(msg){	
									$('#Flightdeal_id_airline > option[value="'+msg+'"]').attr('selected', 'selected');
									//$('#Flightdeal_id_airline').attr('disabled','disabled');
									closeLoading();
								});					
							});
    					}
				} );
			});	
		});	
		</script>
		<style>
		.divcenter_table {
			display: table;
			margin: auto;
			font-family: arial;
			width: 100%;
			font-size: 12px;
			text-align: left;
		}
		</style>
	</head>
	<body>
	<div class="divcenter_table">
		<table id="myTable" class="display" cellspacing="0" width="100%">
			<thead>
				<tr>
					<th>Origen</th>
					<th>Destino</th>
					<th>Aerolinea</th>
					<th>Salida</th>
					<?php if($reward==1){
						echo '<th>Puntos</th>';
					}else{
						echo '<th>Moneda</th>
						<th>Precio</th>';
					}
					?>				
					<th></th>
				</tr>
			</thead>
			<tbody id="content">
			</tbody>
	</body>
	</div>
</html>