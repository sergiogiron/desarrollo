<?php
/* @var $this FlightdealController */
/* @var $model Flightdeal */
/* @var $form CActiveForm */
?>
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'flightdeal-form',
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
	'enableAjaxValidation'=>true,
    'enableClientValidation' => true,
	 'clientOptions' => array(
        'validateOnSubmit' => true,
		'validateOnChange' => false, 
		'afterValidate' => 'js:checkErrors'
    )
)); ?>
<div class="box-body">
	<span class="right_f">
<?php echo $form->checkBox($model,'enabled',array('class'=>'lcs_check enabled')); ?>

	</span>

<div class="col-md-12 col-data">

	<div style="width:100%;height:82px;float:left;">
		<div class="form-group col-md-6 required unique">
			<?php
			if(!$model->isNewRecord){ $disable='disabled'; }else{ $disable='enabled'; }
			if($model->isNewRecord){ $selected=array(); }else{ $selected=array($model->relational[0]->id_channel=>$model->relational[0]->id_channel); }
			?>
			<label>Canal</label>
			<?php echo CHtml::dropDownList('id_channel',$selected , CHtml::listData(Channels::model()->findAll(), 'id', 'title'),array($disable=>$disable,'empty' => 'Seleccione','class'=>'form-control channel')); ?>
			<div class="errorMessage" id="id_channel_error"></div>
		</div>
		<div class="form-group col-md-6 required unique">
			<label>Subcanal</label>
			<?php
			if($model->isNewRecord){
				echo CHtml::dropDownList('id_subchannel', array(), array(),array('prompt' => 'Seleccione','multiple'=>'multiple','style'=>'margin-bottom:5px','class'=>'form-control subchannel select2'));
			}else{
				echo CHtml::dropDownList('id_subchannel', 
				CHtml::listData($model->relational, 'id_subchannel', 'id_subchannel'),
				CHtml::listData(Subchannels::model()->findAll('id_channel='.$model->relational[0]->id_channel), 'id', 'title'),
				array('multiple'=>'multiple','style'=>'margin-bottom:5px','class'=>'form-control subchannel select2','disabled'=>'disabled')
				);
			}
			?>	
			<div class="errorMessage" id="id_subchannel_error"><?=$error?></div>
		</div>
	</div>
	<div style="width:100%;height:82px;float:left;<?php if($model->isNewRecord){ echo "display:none;"; } ?>" id="content_promotional">
		<div class="form-group col-md-6 required">
			<?php echo $form->labelEx($model,'url');
				if($model->promotional==1){
					$disabled="readonly";
				}else{
					$disabled='enabled';
				}		
			?>
			<div class="input-group">
				<?php echo $form->textField($model,'url',array('size'=>60,'maxlength'=>256, $disabled=>$disabled,'placeholder'=>'http://','class'=>'form-control')); ?>
				<a href="<?php if($model->promotional==0){ echo "#"; }else{ echo "link"; } ?>" id="link" class="input-group-addon pointer" data-toggle="tooltip" title="Seleccionar url" <?php if($model->isNewRecord){ echo 'style="display:none;"'; }else{ if($model->promotional==0){ echo 'style="display:none;"'; } }?>><i class="fa fa-globe" ></i></a>
				<a href="<?php if($model->promotional==0){ echo "#"; }else{ echo $model->url; } ?>" id="link2" class="input-group-addon pointer" target="_blank" data-toggle="tooltip" title="Verificar url" <?php if($model->isNewRecord){ 'style="display:none;"'; }else{ if($model->promotional==0){ echo 'style="display:none;"'; } }?>><i class="fa fa-external-link-square"></i></a>
			</div>
			<?php echo $form->error($model,'url'); ?>
		</div>
		<div class="form-group col-md-6">
			<?php echo $form->labelEx($model,'promotional'); ?><br>
			<?php echo $form->checkBox($model,'promotional',array('class'=>'checkbox_form')); ?>
			<?php echo $form->error($model,'promotional'); ?>
		</div>	
	</div>
			<div class="form-group col-md-6 required">
	<?php echo $form->labelEx($model,'title'); ?>
			<?php echo $form->textField($model,'title',array('size'=>32,'maxlength'=>32,'class'=>'form-control')); ?>
							<?php echo $form->error($model,'title'); ?>
</div>
<div class="form-group col-md-6 required">
	<?php echo $form->labelEx($model,'expire_date_from'); 
		if($model->expire_date_from==''){
			$fecha_desde=date("d/m/Y");
		}else{
			$fecha_desde=$model->expire_date_from;
		}
	?>
	<div class="input-group">		
	<?php echo $form->textField($model,'expire_date_from',array('class'=>'form-control','readonly'=>true,'data-inputmask'=>'\'alias\': \'dd/mm/yyyy\'','data-mask'=>'data-mask')); ?>
	<div class="input-group-addon pointer"><i class="fa fa-calendar expire_date_from_calendar" data-toggle="tooltip" title="Abrir el calendario"></i></div>
	</div>	
	<?php echo $form->error($model,'expire_date_from'); ?>
</div>
<div class="form-group col-md-6 required">
	<?php echo $form->labelEx($model,'expire_date_to'); ?>
	<div class="input-group">		
	<?php echo $form->textField($model,'expire_date_to',array('class'=>'form-control','readonly'=>true,'data-inputmask'=>'\'alias\': \'dd/mm/yyyy\'','data-mask'=>'data-mask')); ?>
	<div class="input-group-addon pointer"><i class="fa fa-calendar expire_date_to_calendar" data-toggle="tooltip" title="Abrir el calendario"></i></div>	
	</div>
	<?php echo $form->error($model,'expire_date_to'); ?>
</div>
<div id="loading" style="display:none;width: 100%;">
	<img src="<?=Yii::app()->request->baseUrl?>/img/ajax_loader.gif" width="50" style="margin-bottom: 14px;margin-left: 10px;    clear: both;" />
</div>
<div id="content_currency" style="<?php if($model->promotional==0){ echo 'display:none;'; }?>float:left;clear:both;">
		<div class="form-group col-md-3">
			<?php 
			if($model->type_currency==''){
				$disabled="disabled";
			}else{
				$disabled='enabled';
			}			
			echo $form->labelEx($model,'type_currency'); ?>
			<?php echo $form->dropDownList($model, 'type_currency',CHtml::listData(Typecurrency::model()->findAll(), 'id', 'title'),array('empty' => 'Seleccione',$disabled=>$disabled,'class'=>'dis form-control'));?>
			<?php echo $form->error($model,'type_currency'); ?>
		</div>
		<div class="form-group col-md-3 ">
			<?php echo $form->labelEx($model,'price'); ?>
			<?php echo $form->textField($model,'price',array('class'=>'dis form-control',$disabled=>$disabled)); ?>
			<?php echo $form->error($model,'price'); ?>
		</div>
		<div class="form-group col-md-2">
			<?php echo $form->labelEx($model,'discount'); ?>
			<?php 
			if($model->discount==''){
				$disabled="disabled";
			}else{
				$disabled='enabled';
			}
			echo $form->textField($model,'discount',array('class'=>'dis form-control',$disabled=>$disabled)); ?>
			<?php echo $form->error($model,'discount'); ?>
		</div>
		<div class="form-group col-md-2 ">
			<?php echo $form->labelEx($model,'points'); ?>
			<?php echo $form->textField($model,'points',array('class'=>'dis form-control',$disabled=>$disabled)); ?>
			<?php echo $form->error($model,'points'); ?>
		</div>
		<div class="form-group col-md-2 ">
			<?php echo $form->labelEx($model,'total_points'); ?>
			<?php echo $form->textField($model,'total_points',array('class'=>'dis form-control',$disabled=>$disabled)); ?>
			<?php echo $form->error($model,'total_points'); ?>
		</div>	
</div>

<div class="form-group col-md-6 required">
	<?php
		if($model->promotional==1){
			$disabled="disabled";
		}else{
			$disabled='enabled';
		}		
	?>
	<?php echo $form->labelEx($model,'id_airline'); ?>
			<?php echo $form->dropDownList($model, 'id_airline',CHtml::listData(Airliners::model()->findAll('enabled=1'), 'id', 'title'),array($disabled=>$disabled,'empty' => 'Seleccione','class'=>'form-control'));?>

							<?php echo $form->error($model,'id_airline'); ?>
</div>
<div class="form-group col-md-3 ">
	<?php echo $form->labelEx($model,'direct'); ?><br>
			<?php echo $form->checkBox($model,'direct',array('class'=>'checkbox_form')); ?>
							<?php echo $form->error($model,'direct'); ?>
</div>


<div class="form-group col-md-12" id="detail_promotional_content" <?php if($model->promotional==0){ echo 'style="display:none;"'; }?>>
	<?php echo $form->labelEx($model,'detail'); ?><br>
			<?php echo $form->textArea($model, 'detail', array('rows' => 5, 'cols' => 100, 'class' => 'form-control ckeditor')); ?>
					<?php echo $form->error($model,'detail'); ?>
</div>
<div class="form-group col-md-3">
	<?php echo $form->labelEx($model,'order'); ?>
			<?php echo $form->dropDownList($model, 'order',CHtml::listData(array(array('id'=>1,'title'=>'Mega oferta'),array('id'=>2,'title'=>'Oferta'),array('id'=>3,'title'=>'Recomendada')), 'id', 'title'),array('empty' => 'Seleccione','class'=>'form-control'));?>
							<?php echo $form->error($model,'order'); ?>
</div>
<div class="col-md-12">
		<label class="label_image">Imagen*</label>
		<div class="error_img" id="id_image_error"><?=$img_error?></div>
		<div class="col-md-1">
			<div class="more_img action_img action_img" data-position="1" title="Agregar imagen" data-toggle="tooltip">+</div>
		</div>
		<div class="col-md-11 content-images">
			<ul class="slider1" id="slider_1">
				<?php
					foreach($model->imagenes as $imagenes){
						if($imagenes->position==1){
							echo "<li id='".$imagenes->filename."_slide' class='bslide_1 slide ui-state-default ui-sortable-handle'><i class='fa fa-edit edit_img_i' data-position=".$imagenes->position." data-id=".$imagenes->filename." onclick='edit_element($(this))'></i><i class='fa fa-times delete_img_i' data-id=".$imagenes->filename."   data-position=".$imagenes->position." onclick='delete_element($(this))'></i><img src='".Yii::app()->request->baseUrl."/uploads/tmp/".Yii::app()->user->id."/".$imagenes->filename."/".$imagenes->filename.".".$imagenes->ext."'></li>";
						}
					}
				?>
			</ul>
			<div id="data-d-content_1">
				<?php
					foreach($model->imagenes as $imagenes){
						if($imagenes->position==1){
							$data=array('name'=>$imagenes->filename,'position'=>$imagenes->position,'ext'=>$imagenes->ext,'title'=>$imagenes->title,'link'=>$imagenes->link,'order'=>$imagenes->order,'coords'=>json_decode($imagenes->coords));
							echo "<input type='hidden' id=".$imagenes->filename." name='image[]' class='element_data' value='".json_encode($data)."'>";
						}
					}
				?>
			</div>
		</div>
	</div>

	<div class="box-footer">
	<?php
	$title = (Yii::app()->controller->action->id!='view') ? 'Volver sin guardar cambios' : 'Volver';
	echo '<a href="'.Yii::app()->request->baseUrl.'/'.strtolower(Yii::app()->controller->id).'" class="btn btn-secundary" data-toggle="tooltip" title="'.$title.'" >Cerrar</a>';
	if(Yii::app()->controller->action->id!='view'){
		echo CHtml::submitButton('Guardar',array('class'=>'btn btn-primary','data-toggle'=>'tooltip','title'=>'Guardar cambios'));
	}
	?>
	</div>

<script>
id_menu=<?=Yii::app()->user->id_menu()?>;
menu='<?=strtolower(Yii::app()->controller->id)?>';
eval("var cantidad_images_"+1+" = "+$('#data-d-content_1 :input').length);
//eval("var cantidad_images_"+2+" = "+$('#data-d-content_2 :input').length);
eval("var limit_images_"+1+" = '1'");
//eval("var limit_images_"+2+" = '2'");
</script>
<?php $this->endWidget(); ?>

</div><!-- form -->
</div>
<link rel="stylesheet" type="text/css" href="http://cdn.datatables.net/1.10.11/css/jquery.dataTables.min.css">
<script type="text/javascript" language="javascript" src="http://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
<?php
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/ckeditor-standard/ckeditor.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerCssFile(Yii::app()->params['cdnUrl'].'/plugins/lcswich/lc_switch-tts.css');
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/lcswich/lc_switch.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/lcswich/lc_switch-tts.js',CClientScript::POS_END);

	Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/plugins/select2/select2.full.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl.'/js/plugins/select2/select2.css');

	//logica uploades de imagenes
	Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/uploader_image.js',CClientScript::POS_END);
	//logica uploades de imagenes
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/js/dropzone.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerCssFile(Yii::app()->params['cdnUrl'].'/css/jquery.Jcrop.css');
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/js/jquery.Jcrop.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/js/jquery.bxslider.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerCssFile(Yii::app()->params['cdnUrl'].'/plugins/jQueryUI/jquery-ui.min.css');
	Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/jquery-ui.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile('http://e-flights.net/promotionalShopping/apiJson.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerCssFile(Yii::app()->params['cdnUrl'].'/css/jquery.bxslider.css');



	Yii::app()->clientScript->registerCssFile(Yii::app()->params['cdnUrl'].'/plugins/datepicker/datepicker3.css');
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/datepicker/bootstrap-datepicker.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/input-mask/jquery.inputmask.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/input-mask/jquery.inputmask.date.extensions.js',CClientScript::POS_END);
	//Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/datepicker/datepicker-tts.js',CClientScript::POS_END);

echo '<script>viewPage = "'.Yii::app()->controller->action->id.'";</script>';
Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/js/form-validators.js',CClientScript::POS_END) ?>
<script>
//$('.errorMessage').html('');
function toObject(arr) {
  var rv = {};
  for (var i = 0; i < arr.length; ++i)
    rv[i] = arr[i];
  return rv;
} 
function checkErrors(form, data, hasError) {
	if(hasError!=true){
		var formData = $('#flightdeal-form').serialize();
		sub_var=toObject($('#id_subchannel').val());
		formData=formData+'&id_channel='+$('#id_channel').val()+'&id_subchannel='+JSON.stringify(sub_var);
		openLoading();
		$.ajax({
			type: 'POST',
			url: $('#flightdeal-form').attr('action'),
			data:formData,
			success:function(data){
				var error=0;
				if($("#data-d-content_1 :input").length==0){
				   $("#id_image_error").html("Debe subir al menos una imagen");
				   error=1;
				}else{
					$("#id_image_error").html("");
				}
				if($('#id_subchannel').select2('data').length==0){
					$('#id_subchannel_error').css("display","block");	
				   $("#id_subchannel_error").html("Debe seleccionar al menos un subcanal");
				   error=1;
				}else{
					 $("#id_subchannel_error").html("");
				}
				if($('#Flightdeal_promotional').is(':checked')){
					if(CKEDITOR.instances['Flightdeal_detail'].getData()==''){
						error=1;
						$('#Flightdeal_detail_em_').css("display","block");	
						$('#Flightdeal_detail_em_').html("Debe completar este campo.");
					}else{
						$('#Flightdeal_detail_em_').html("");
					}
				}
				if($('#Flightdeal_discount').attr('disabled')=='disabled'){
					if($('#Flightdeal_type_currency').val()==''){
						error=1;
						$('#Flightdeal_type_currency_em_').css("display","block");
						$('#Flightdeal_type_currency_em_').html("Debe completar este campo.");
					}else{
						$('#Flightdeal_type_currency_em_').html("");
					}
					if($('#Flightdeal_price').val()==''){
						error=1;
						$('#Flightdeal_price_em_').css("display","block");						
						$('#Flightdeal_price_em_').html("Debe completar este campo.");						
					}else{
						$('#Flightdeal_price_em_').html("");		
					}		
				}else{
					if($('#Flightdeal_discount').val()==''){
						error=1;
						$('#Flightdeal_discount_em_').css("display","block");
						$('#Flightdeal_discount_em_').html("Debe completar este campo.");
					}else{
						$('#Flightdeal_discount_em_').html("");
					}
					if($('#Flightdeal_points').val()==''){
						error=1;
						$('#Flightdeal_points_em_').css("display","block");	
						$('#Flightdeal_points_em_').html("Debe completar este campo.");						
					}else{
						$('#Flightdeal_points_em_').html("");
					}
					if($('#Flightdeal_total_points').val()==''){
						error=1;
						$('#Flightdeal_total_points_em_').css("display","block");	
						$('#Flightdeal_total_points_em_').html("Debe completar este campo.");
					}else{
						$('#Flightdeal_total_points_em_').html("");
					}				
				}
				if(error==0){
					$(".errorMessage").each( function( index, element ){
						if($( this ).html()!="" && $( this ).css('display')!="none" ){ error=1;  }
					});			
					if(error==0){
						location.href=homeurl+'/flightdeal/admin';
					}				
				}	
				closeLoading();
			}
		});
	}
}
$( document ).ready(function() {
	$('.expire_date_from_calendar').on('click', function(e) {
		event.preventDefault();
		$('#Flightdeal_expire_date_from').focus();
    });		
	$('.expire_date_to_calendar').on('click', function(e) {
		event.preventDefault();
		$('#Flightdeal_expire_date_to').focus();
    });	
	var StartDate = new Date();
	$('#Flightdeal_expire_date_from').datepicker({
			Date:'<?=$fecha_desde?>',
			language:"es",
			format: 'dd/mm/yyyy',
			startDate:'<?=date("d-m-Y")?>',
			autoclose:true
	});	
	$('#Flightdeal_expire_date_to').datepicker({
			Date:'<?=$fecha_desde?>',
			language:"es",
			format: 'dd/mm/yyyy',
			startDate:'<?=$fecha_desde?>',
			autoclose:true
	});		
	$('#Flightdeal_expire_date_from').on("changeDate", function() {
		$('#Flightdeal_expire_date_to').datepicker('setStartDate',$(this).datepicker('getFormattedDate'));
		$('#Flightdeal_expire_date_to').datepicker('setDate',$('#Flightdeal_expire_date_from').val());
	});
	/*$('#Flightdeal_expire_date_from').on("changeDate", function() {
		var date2 = $('#fecha_ida_i').datepicker('getDate', '+'+anticipation_to_max_days+'d');
		date2.setDate(date2.getDate()+anticipation_to_max_days);
		$('#fecha_vuelta_i').datepicker('setEndDate',date2);
		if(validate_fechaMayorQue($('#fecha_vuelta').val(),$(this).datepicker('getFormattedDate'))){
			$('#fecha_vuelta_i').datepicker('setStartDate',$(this).datepicker('getFormattedDate'));
			$('#fecha_vuelta_i').datepicker('setDate',$(this).datepicker('getFormattedDate'));
			$('#fecha_vuelta').val($(this).datepicker('getFormattedDate'));
		}else{
			$('#fecha_vuelta_i').datepicker('setStartDate',$(this).datepicker('getFormattedDate'));
			$('#fecha_vuelta_i').datepicker('setDate',$('#fecha_vuelta').val());
		}
		$('#fecha_ida').val($(this).datepicker('getFormattedDate'));
		$('.fecha_ida').html($(this).datepicker('getFormattedDate'));
	});	*/
	CKEDITOR.replace( 'Flightdeal_detail', {
	   language: 'es',
	  /*uiColor: '#9AB8F3  ',*/
	  toolbar: [
	  { name: 'clipboard', groups: [ 'clipboard', 'undo' ], items: [ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ] },
	   { name: 'document', groups: [ 'mode', 'document', 'doctools' ], items: [ 'Source', '-', 'Save', 'NewPage', 'Preview', 'Print', '-', 'Templates' ] },
	   { name: 'links', items: [ 'Link', 'Unlink', 'Anchor' ] },
	  { name: 'insert', items: [ 'Image', 'Flash', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe' ] },
	  { name: 'editing', groups: [ 'find', 'selection', 'spellchecker' ], items: [ 'Find', 'Replace', '-', 'SelectAll', '-', 'Scayt' ] },
	  { name: 'forms', items: [ 'Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField' ] },
	  '/',
	  { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat' ] },
	  { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl', 'Language' ] },
	  { name: 'styles', items: [ 'Styles', 'Format', 'Font', 'FontSize' ] },
	  { name: 'colors', items: [ 'TextColor', 'BGColor' ] },
	  { name: 'tools', items: [ 'Maximize', 'ShowBlocks' ] }
	   ],
	});

	<?php if(Yii::app()->controller->action->id=='view'){ echo "$('form').find('input, textarea, button, checkbox, select').attr('disabled','disabled');"; } ?>
	$('[data-mask]').inputmask('dd/mm/yyyy', {'placeholder': 'dd/mm/aaaa'});
	/*$('.fa-calendar').datepicker().on('changeDate', function(e) {
		$(this).parent().prev().val(e.format('dd/mm/yyyy'));
	    $(this).datepicker('hide');
    });*/
   channelsFilter();
	$('.select2').select2().on('select2:unselect', function(e) {
		$('.select2-selection').trigger('click');
	});
	$('#Flightdeal_promotional').on('click', function(e) {
		if($('#Flightdeal_promotional').is(':checked')){
			$('#detail_promotional_content').css('display','block');
			$('Flightdeal_url').val('');			
			$('#Flightdeal_url').attr('readonly','true');
			$( "#link" ).attr('href','link')
			$( "#link" ).trigger('click');
			$('#link2').css('display','table-cell');	
			$('#link').css('display','table-cell');				
		}else{
			$('#detail_promotional_content').css('display','none');	
			$('#link2').css('display','none');	
			$('#link').css('display','none');	
			$('#Flightdeal_url').removeAttr('readonly');
			$( "#link" ).attr('href','#');
			$('#Flightdeal_url').val('');						
		}
	});
	$( "#link" ).click(function( event ) {
		event.preventDefault();
		if($(this).attr('href')!='#'){
			openLoading();
			var data =$('#id_subchannel').select2('data');
			select_val=[];
			$.each( data, function( key, value ) {
			  select_val.push( value.id );
			});			
			$.ajax({
			  method: "POST",
			  url: homeurl+"/flightdeal/promotional",
			  data:{ id_subchannel: select_val }
			}).done(function(msg){	
				closeLoading();
				$('#modal-content-images').css('width','1022px');
				$('#content_result_img').css('padding','15px');
				$('.ligth-box-modal-images').css('display','block');
				$('#content_result_img').html(msg);
			});
		}
	});	
	$('#id_subchannel').on('change', function(e) {
		$('#loading').css('display','table');		
		$('#content_currency').css('display','none');
		var selected_element = $(e.currentTarget);
		var select_val = selected_element.val();
		if(select_val==null){
			select_val=[];
		}
		/*$("#Flightdeal_type_currency_em_").css('display','none');
		$("#Flightdeal_price_em_").css('display','none');
		$("#Flightdeal_discount_em_").css('display','none');
		$("#Flightdeal_points_em_").css('display','none');
		$("#Flightdeal_total_points_em_").css('display','none');	*/	
		$.ajax({
		  method: "POST",
		  url: homeurl+"/flightdeal/statusreward",
		  data:{ id_subchannel: select_val }
		}).done(function(msg){
			$('#loading').css('display','none');	
			var obj = jQuery.parseJSON(msg);
			if((obj.reward>0 && obj.not_reward==0) || (obj.not_reward>0 && obj.reward==0)){
				$('#content_currency').css('display','block');
				$('#id_subchannel_error').html('');
				if(obj.reward>0){
					$('#Flightdeal_type_currency').attr('disabled','disabled');
					$('#Flightdeal_price').attr('disabled','disabled');				
					$('#Flightdeal_type_currency').val('');
					$('#Flightdeal_price').val('');		
					$('#Flightdeal_points').removeAttr('disabled');
					$('#Flightdeal_discount').removeAttr('disabled');				
					$('#Flightdeal_total_points').removeAttr('disabled');				
					$('#Flightdeal_points').parent().addClass( "required" );
					$('#Flightdeal_discount').parent().addClass( "required" );
					$('#Flightdeal_total_points').parent().addClass( "required" );
					$('#Flightdeal_type_currency').parent().removeClass( "required" );
					$('#Flightdeal_price').parent().removeClass( "required" );
					if($('#Flightdeal_points').prev().html().indexOf('*')==-1) {
						$('#Flightdeal_points').prev().append( "*" );
					}
					if($('#Flightdeal_discount').prev().html().indexOf('*')==-1) {
						$('#Flightdeal_discount').prev().append( "*" );
					}						
					if($('#Flightdeal_total_points').prev().html().indexOf('*')==-1) {
						$('#Flightdeal_total_points').prev().append( "*" );
					}										
					var Flightdeal_type_currency=$('#Flightdeal_type_currency').prev().html();
					Flightdeal_type_currency=Flightdeal_type_currency.replace("*", "");	
					$('#Flightdeal_type_currency').prev().html(Flightdeal_type_currency);	

					var Flightdeal_price=$('#Flightdeal_price').prev().html();
					Flightdeal_price=Flightdeal_price.replace("*", "");	
					$('#Flightdeal_price').prev().html(Flightdeal_price);						
				}				
				if(obj.not_reward>0){
					$('#Flightdeal_discount').attr('disabled','disabled');
					$('#Flightdeal_total_points').attr('disabled','disabled');
					$('#Flightdeal_points').attr('disabled','disabled');		
					$('#Flightdeal_discount').val('');
					$('#Flightdeal_total_points').val('');
					$('#Flightdeal_points').val('');	
					$('#Flightdeal_type_currency').removeAttr('disabled');
					$('#Flightdeal_price').removeAttr('disabled');					
					$('#Flightdeal_type_currency').parent().addClass( "required" );
					$('#Flightdeal_price').parent().addClass( "required" );						
					$('#Flightdeal_points').parent().removeClass( "required" );
					$('#Flightdeal_discount').parent().removeClass( "required" );					
					$('#Flightdeal_total_points').parent().removeClass( "required" );	
					if($('#Flightdeal_type_currency').prev().html().indexOf('*')==-1) {
						$('#Flightdeal_type_currency').prev().append( "*" );
					}		
					if($('#Flightdeal_price').prev().html().indexOf('*')==-1) {
						$('#Flightdeal_price').prev().append( "*" );
					}							
					
					var Flightdeal_points=$('#Flightdeal_points').prev().html();
					Flightdeal_points=Flightdeal_points.replace("*", "");	
					$('#Flightdeal_points').prev().html(Flightdeal_points);	

					var Flightdeal_discount=$('#Flightdeal_discount').prev().html();
					Flightdeal_discount=Flightdeal_discount.replace("*", "");	
					$('#Flightdeal_discount').prev().html(Flightdeal_discount);	
					
					var Flightdeal_total_points=$('#Flightdeal_total_points').prev().html();
					Flightdeal_total_points=Flightdeal_total_points.replace("*", "");	
					$('#Flightdeal_total_points').prev().html(Flightdeal_total_points);						
				}
			}else{
				$('#content_currency').css('display','none');
				$('#id_subchannel_error').html('Los subcanales elegidos deben ser todos del mismo tipo');
			}
			$('#content_promotional').css('display','block');
		});		
	});
	if(viewPage=='update'){
		$('.unique input').prop('disabled',true);
		$(".subchannel").prop('disabled',true);
	}
	$('#id_subchannel').removeAttr('disabled');
	<?php 
		if(!$model->isNewRecord){
			//hardcore de un campo unívoco 
			echo '$("#id_subchannel").attr("disabled","disabled");'; 
		}
	?>				
});
</script>
