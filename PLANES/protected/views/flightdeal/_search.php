<?php
/* @var $this FlightdealController */
/* @var $model Flightdeal */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>
<div class="col-md-12" style="clear: both">
<div class="row">
			
			<div class="col-md-3">
				<div class="row row_space">
					<?php echo $form->label($model,'id'); ?>
					<?php echo $form->textField($model,'id',array('class'=>'form-control')); ?>
				</div>
			</div>
			
			<div class="col-md-3">
				<div class="row row_space">
					<?php echo $form->label($model,'title'); ?>
					<?php echo $form->textField($model,'title',array('size'=>32,'maxlength'=>32,'class'=>'form-control')); ?>
				</div>
			</div>
			
			<div class="col-md-3">
				<div class="row row_space">
					<?php echo $form->label($model,'url'); ?>
					<?php echo $form->textField($model,'url',array('size'=>60,'maxlength'=>256,'class'=>'form-control')); ?>
				</div>
			</div>
			
			<div class="col-md-3">
				<div class="row row_space">
					<?php echo $form->label($model,'type_currency'); ?>
					<?php echo $form->textField($model,'type_currency',array('class'=>'form-control')); ?>
				</div>
			</div>
			
			<div class="col-md-3">
				<div class="row row_space">
					<?php echo $form->label($model,'price'); ?>
					<?php echo $form->textField($model,'price',array('class'=>'form-control')); ?>
				</div>
			</div>
			
			<div class="col-md-3">
				<div class="row row_space">
					<?php echo $form->label($model,'points'); ?>
					<?php echo $form->textField($model,'points',array('class'=>'form-control')); ?>
				</div>
			</div>
			
			<div class="col-md-3">
				<div class="row row_space">
					<?php echo $form->label($model,'total_points'); ?>
					<?php echo $form->textField($model,'total_points',array('class'=>'form-control')); ?>
				</div>
			</div>
			
			<div class="col-md-3">
				<div class="row row_space">
					<?php echo $form->label($model,'discount'); ?>
					<?php echo $form->textField($model,'discount',array('class'=>'form-control')); ?>
				</div>
			</div>
			
			<div class="col-md-3">
				<div class="row row_space">
					<?php echo $form->label($model,'expire_date_from'); ?>
					<?php echo $form->textField($model,'expire_date_from',array('class'=>'form-control datepicker','data-inputmask'=>'\'alias\': \'dd/mm/yyyy\'','data-mask'=>'data-mask')); ?>
				</div>
			</div>
			
			<div class="col-md-3">
				<div class="row row_space">
					<?php echo $form->label($model,'expire_date_to'); ?>
					<?php echo $form->textField($model,'expire_date_to',array('class'=>'form-control datepicker','data-inputmask'=>'\'alias\': \'dd/mm/yyyy\'','data-mask'=>'data-mask')); ?>
				</div>
			</div>
			
			<div class="col-md-3">
				<div class="row row_space">
					<?php echo $form->label($model,'id_airline'); ?>
					<?php echo $form->textField($model,'id_airline',array('class'=>'form-control')); ?>
				</div>
			</div>
			
			<div class="col-md-3">
				<div class="row row_space">
					<?php echo $form->label($model,'direct'); ?>
					<?php echo $form->textField($model,'direct',array('class'=>'form-control')); ?>
				</div>
			</div>
			
			<div class="col-md-3">
				<div class="row row_space">
					<?php echo $form->label($model,'deal'); ?>
					<?php echo $form->textField($model,'deal',array('class'=>'form-control')); ?>
				</div>
			</div>
			
			<div class="col-md-3">
				<div class="row row_space">
					<?php echo $form->label($model,'enabled'); ?>
					<?php echo $form->checkBox($model,'enabled',array('class'=>'lcs_check enabled')); ?>
				</div>
			</div>
			
			<div class="col-md-3">
				<div class="row row_space">
					<?php echo $form->label($model,'created_at'); ?>
					<?php echo $form->textField($model,'created_at',array('class'=>'form-control datepicker','data-inputmask'=>'\'alias\': \'dd/mm/yyyy\'','data-mask'=>'data-mask')); ?>
				</div>
			</div>
			
			<div class="col-md-3">
				<div class="row row_space">
					<?php echo $form->label($model,'id_user'); ?>
					<?php echo $form->textField($model,'id_user',array('class'=>'form-control')); ?>
				</div>
			</div>
</div>
	<div class="row buttons">
		<?php echo CHtml::submitButton('Buscar',array('class'=>'btn btn-default b_customize')); ?>
	</div>
<br />
<?php $this->endWidget(); ?>
</div>
</div><!-- search-form -->