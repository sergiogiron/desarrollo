<?php
/* @var $this OffersproductController */
/* @var $model Offersproduct */
/* @var $form CActiveForm */
?>
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'offersproduct-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>
<div class="box-body">
<span class="right_f active-button">
	<?php echo $form->checkBox($model,'enabled',array('class'=>'lcs_check enabled')); ?>
</span>
<br />
<div class="col-md-12 col-data">
	<div class="row">
	<div class="row">
<div class="form-group col-md-4 required unique">
	
		<?php 
	if(!$model->isNewRecord){ $disable='disabled'; }else{ $disable='enabled'; }	
	echo $form->labelEx($model,'id_channel'); ?>
	<?php echo $form->dropDownList($model, 'id_channel',CHtml::listData(Channels::model()->findAll(), 'id', 'title'),array($disable=>$disable,'empty' => 'Seleccione','class'=>'form-control channel'));?>
	<?php echo $form->error($model,'id_channel'); ?>
	
	</div>

	<div class="form-group col-md-4 required unique">
		<?php 

		if(!$model->isNewRecord){ $disable='disabled'; }else{ $disable='enabled'; }	
		echo $form->labelEx($model,'id_subchannel'); ?>
					<?php 
					if($model->isNewRecord){
						echo $form->dropDownList($model, 'id_subchannel',array(),array('empty' => 'Seleccione','class'=>'form-control subchannel'));						
					}else{
						echo $form->dropDownList($model, 'id_subchannel',CHtml::listData(Subchannels::model()->findAll("id_channel=".$model->id_channel), 'id', 'title'),array($disable=>$disable,'empty' => 'Seleccione','class'=>'form-control'));						
					}
					?>
					<?php echo $form->error($model,'id_subchannel'); ?>
	</div>
	
	<div class="form-group col-md-4 required unique">
		<?php if(!$model->isNewRecord){ $disable='disabled'; }else{ $disable='enabled'; }	
		echo $form->labelEx($model,'id_product'); ?>
		<?php echo $form->dropDownList($model, 'id_product',CHtml::listData(Products::model()->findAll(), 'id', 'title'),array($disable=>$disable,'empty' => 'Seleccione','class'=>'form-control'));?>
		<?php echo $form->error($model,'id_product'); ?>
	</div>
	<?php
	if($model->id_email_server_sales==''){
		$disabled='disabled';
	}else{
		$disabled='enabled';
	}
	?>
		  <div class="nav-tabs">
		<ul class="nav nav-tabs">
		  <li class="active tab_1"><a href="#tab_1" data-toggle="tab" aria-expanded="true">Notificaciones y Alertas</a></li>
		  <li class="tab_2"><a href="#tab_2" data-toggle="tab"  aria-expanded="false">Tracking</a></li>
		  <li class="tab_3"><a href="#tab_3" data-toggle="tab"  aria-expanded="false">General</a></li>
		</ul>
		<div class="tab-content">
		  <div class="tab-pane active" id="tab_1">
				<br />
				<h3 class="box-title titletab">Notificaciones online:</h3>
				<div class="col-md-4 form-group">
					<?php echo $form->labelEx($model,'id_email_server_sales'); ?>
					<?php echo $form->dropDownList($model, 'id_email_server_sales',CHtml::listData(Emailserver::model()->findAll(), 'id', 'title'),array('empty' => 'Seleccione','class'=>'form-control'));?>
					<?php echo $form->error($model,'id_email_server_sales'); ?>
				</div>
				<div class="col-md-4 form-group required_promise">
					<?php echo $form->labelEx($model,'sale_user'); ?>
					<?php echo $form->textField($model,'sale_user',array('class'=>'form-control',$disabled=>$disabled)); ?>
					<?php echo $form->error($model,'sale_user'); ?>
				</div>

				<div class="col-md-4 form-group required_promise">
					<?php echo $form->labelEx($model,'sale_pass'); ?>
					<?php echo $form->textField($model,'sale_pass',array('class'=>'form-control',$disabled=>$disabled)); ?>
					<?php echo $form->error($model,'sale_pass'); ?>
				</div>

				<div class="col-md-4 form-group required_promise">
					<?php echo $form->labelEx($model,'sale_show_as'); ?>
					<?php echo $form->textField($model,'sale_show_as',array('class'=>'form-control',$disabled=>$disabled)); ?>
					<?php echo $form->error($model,'sale_show_as'); ?>
				</div>

				<div class="col-md-8 form-group required_promise">
					<?php echo $form->labelEx($model,'sale_to'); ?>
					<?php echo $form->textField($model,'sale_to',array('class'=>'form-control',$disabled=>$disabled)); ?>
					<?php echo $form->error($model,'sale_to'); ?>
				</div>
				<h3 class="box-title titletab">Alertas:</h3>
				<div class="col-md-4 form-group">
					<?php echo $form->labelEx($model,'id_email_server_alerts'); ?>
					<?php echo $form->dropDownList($model, 'id_email_server_alerts',CHtml::listData(Emailserver::model()->findAll(), 'id', 'title'),array('empty' => 'Seleccione','class'=>'form-control'));?>
					<?php echo $form->error($model,'id_email_server_alerts'); ?>
				</div>
				<div class="col-md-4 form-group required_promise2">
					<?php echo $form->labelEx($model,'alerts_user'); ?>
					<?php echo $form->textField($model,'alerts_user',array('class'=>'form-control',$disabled=>$disabled)); ?>
					<?php echo $form->error($model,'alerts_user'); ?>
				</div>

				<div class="col-md-4 form-group required_promise2">
					<?php echo $form->labelEx($model,'alerts_pass'); ?>
					<?php echo $form->textField($model,'alerts_pass',array('class'=>'form-control',$disabled=>$disabled)); ?>
					<?php echo $form->error($model,'alerts_pass'); ?>
				</div>

				<div class="col-md-4 form-group required_promise2">
					<?php echo $form->labelEx($model,'alerts_show_as'); ?>
					<?php echo $form->textField($model,'alerts_show_as',array('class'=>'form-control',$disabled=>$disabled)); ?>
					<?php echo $form->error($model,'alerts_show_as'); ?>
				</div>

				<div class="col-md-8 form-group required_promise2">
					<?php echo $form->labelEx($model,'alerts_to'); ?>
					<?php echo $form->textField($model,'alerts_to',array('class'=>'form-control',$disabled=>$disabled)); ?>
					<?php echo $form->error($model,'alerts_to'); ?>
				</div>

				<div class="col-md-4 form-group required_promise2">
					<?php echo $form->labelEx($model,'alerts_subject_patterns'); ?>
					<?php echo $form->textField($model,'alerts_subject_patterns',array('class'=>'form-control',$disabled=>$disabled)); ?>
					<?php echo $form->error($model,'alerts_subject_patterns'); ?>
				</div>

		  </div>
		  <div class="tab-pane" id="tab_2">
			<br />
			<div class="col-md-8 form-group ">
				<?php echo $form->labelEx($model,'tracking_description'); ?>
				<?php echo $form->textArea($model,'tracking_description',array('class'=>'form-control','rows'=>3)); ?>
				<?php echo $form->error($model,'tracking_description'); ?>
			</div>


			<div class="col-md-8 form-group">
				<?php echo $form->labelEx($model,'tracking_analitycs'); ?>
				<?php echo $form->textArea($model,'tracking_analitycs',array('class'=>'form-control','rows'=>10)); ?>
				<?php echo $form->error($model,'tracking_analitycs'); ?>
			</div>
		  </div>
			<div class="tab-pane" id="tab_3">	
<br />			
			<div class="col-md-12 form-group ">
				<?php echo $form->labelEx($model,'script'); ?>
				<?php echo $form->textArea($model,'script',array('class'=>'form-control','rows'=>10)); ?>
				<?php echo $form->error($model,'script'); ?>
			</div>			
		  	</div>
		  	</div>
		<!-- /.tab-content -->
	  </div>
	  <!-- nav-tabs-custom -->
	</div>
</div>
</div>
</div>
<div class="box-footer">
		<?php
		$title = (Yii::app()->controller->action->id!='view') ? 'Volver sin guardar cambios' : 'Volver';
		echo '<a href="'.Yii::app()->request->baseUrl.'/'.strtolower(Yii::app()->controller->id).'" class="btn btn-secundary" data-toggle="tooltip" title="'.$title.'" >Cerrar</a>';
		if(Yii::app()->controller->action->id!='view'){
			echo CHtml::submitButton('Guardar',array('class'=>'btn btn-primary','data-toggle'=>'tooltip','title'=>'Guardar cambios'));
		}
		?></div>
<?php $this->endWidget(); ?>

</div><!-- form -->
</div>

<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/plugins/select2/select2.full.js',CClientScript::POS_END); ?>
<?php Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl.'/js/plugins/select2/select2.css'); ?>
<?php 
	//logica uploades de imagenes
	Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/uploader_image.js',CClientScript::POS_END); 
	//logica uploades de imagenes	
	Yii::app()->clientScript->registerScriptFile('https://cdn.ckeditor.com/4.5.7/standard/ckeditor.js',CClientScript::POS_END);	
	Yii::app()->clientScript->registerCssFile(Yii::app()->params['cdnUrl'].'/plugins/lcswich/lc_switch-tts.css');
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/lcswich/lc_switch.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/lcswich/lc_switch-tts.js',CClientScript::POS_END);

	Yii::app()->clientScript->registerCssFile(Yii::app()->params['cdnUrl'].'/plugins/datepicker/datepicker3.css');
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/datepicker/bootstrap-datepicker.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/input-mask/jquery.inputmask.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/input-mask/jquery.inputmask.date.extensions.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/datepicker/datepicker-tts.js',CClientScript::POS_END);

	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/timepicker/bootstrap-timepicker.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/input-mask/jquery.inputmask.bundle.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerCssFile(Yii::app()->params['cdnUrl'].'/plugins/timepicker/bootstrap-timepicker.css');
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/timepicker/timepicker-tts.js',CClientScript::POS_END);

	//logica uploades de imagenes
	//logica uploades de imagenes
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/js/dropzone.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/js/jquery.bxslider.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerCssFile(Yii::app()->params['cdnUrl'].'/plugins/jQueryUI/jquery-ui.min.css');
	Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/jquery-ui.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerCssFile(Yii::app()->params['cdnUrl'].'/css/jquery.bxslider.css');

	echo '<script>viewPage = "'.Yii::app()->controller->action->id.'";</script>';
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/js/form-validators.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile('//cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js',CClientScript::POS_END);
 ?>



<script>
$( document ).ready(function() {
	$( "#Offersproduct_id_email_server_sales" ).change(function() {
	  if($(this).val()!=''){
		   $('.required_promise').addClass('required');
		   $('.required_promise').find( "label" ).append('<span class="required_ask">*</span>');  
		   $('.required_promise').find( "input").removeAttr('disabled');
	  }else{
		  $('.required_promise').removeClass('required');
		   $('.required_promise').find( "input").attr('disabled','disabled');
		   $('.required_promise').find( "input").val('');
		    $('#Offersproduct_id_email_server_sales').removeAttr('disabled');
		  $('.required_ask').remove();
	  }
	});		
	$( "#Offersproduct_id_email_server_alerts" ).change(function() {
	  if($(this).val()!=''){
		   $('.required_promise2').addClass('required');
		   $('.required_promise2').find( "label" ).append('<span class="required_ask2">*</span>');  
		   $('.required_promise2').find( "input").removeAttr('disabled');
	  }else{
		  $('.required_promise2').removeClass('required');
		   $('.required_promise2').find( "input").attr('disabled','disabled');
		   $('.required_promise2').find( "input").val('');
		    $('#Offersproduct_id_email_server_sales').removeAttr('disabled');
		  $('.required_ask2').remove();
	  }
	});	
	<?php if(Yii::app()->controller->action->id=='view'){ echo "$('form').find('input, textarea, button, checkbox, select').attr('disabled','disabled');"; } ?>
	channelsFilter();

	$('.select2').select2();

	if(viewPage=='update'){
		$('.unique input').prop('disabled',true);
		$(".subchannel").prop('disabled',true);
	}
});		
</script>