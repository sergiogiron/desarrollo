<?php
/* @var $this SearchController */
/* @var $model Search */
/* @var $form CActiveForm */
?>
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'search-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
));
 ?>
<span class="right_f active-button">
	<?php echo $form->checkBox($model,'enabled',array('class'=>'lcs_check enabled')); ?>
	<?php echo $form->error($model,'enabled'); ?>
</span>
<br />
<div class="col-md-4 form-group required  unique">
	<?php
	echo $form->labelEx($model,'id_channel'); ?>
	<?php
	echo $form->dropDownList($model, 'id_channel',CHtml::listData(Channels::model()->findAll(), 'id', 'title'),array($disable=>$disable,'empty' => 'Seleccione','class'=>'form-control'));?>
	<?php echo $form->error($model,'id_channel'); ?>
</div>

<div class="col-md-4 form-group required  unique">
	<?php echo $form->labelEx($model,'id_subchannel'); ?>
	<?php echo $form->dropDownList($model, 'id_subchannel',CHtml::listData(Subchannels::model()->findAll(), 'id', 'title'),array($disable=>$disable,'empty' => 'Seleccione','class'=>'form-control'));?>
	<?php echo $form->error($model,'id_subchannel'); ?>
</div>

<div class="col-md-4 form-group required  unique">
	<?php echo $form->labelEx($model,'id_product'); ?>
	<?php echo $form->dropDownList($model, 'id_product',CHtml::listData(Products::model()->findAll(), 'id', 'title'),array($disable=>$disable,'empty' => 'Seleccione','class'=>'form-control'));?>
	<?php echo $form->error($model,'id_product'); ?>
</div>

<div class="col-md-12">
	<div class="row">
	  <!-- Custom Tabs -->
	  <div class="nav-tabs">
		<ul class="nav nav-tabs">
		  <li class="active tab_1"><a href="#tab_1" data-toggle="tab" aria-expanded="true">Notificaciones y Alertas</a></li>
		  <li class="tab_2"><a href="#tab_2" data-toggle="tab"  aria-expanded="false">Tracking</a></li>
		  <li class="tab_3"><a href="#tab_3" data-toggle="tab"  aria-expanded="false">Landings</a></li>
		  <li class="tab_4"><a href="#tab_4" data-toggle="tab"  aria-expanded="false">General</a></li>
		</ul>
		<div class="tab-content">
		  <div class="tab-pane active" id="tab_1">
				<br />
				<h3 class="box-title titletab">Notificaciones online:</h3>
				<div class="col-md-4 form-group required">
					<?php echo $form->labelEx($model,'id_email_server_sales'); ?>
					<?php echo $form->dropDownList($model, 'id_email_server_sales',CHtml::listData(Emailserver::model()->findAll(), 'id', 'title'),array('empty' => 'Seleccione','class'=>'form-control'));?>
					<?php echo $form->error($model,'id_email_server_sales'); ?>
				</div>
				<div class="col-md-4 form-group required">
					<?php echo $form->labelEx($model,'sale_user'); ?>
					<?php echo $form->textField($model,'sale_user',array('class'=>'form-control')); ?>
					<?php echo $form->error($model,'sale_user'); ?>
				</div>

				<div class="col-md-4 form-group required">
					<?php echo $form->labelEx($model,'sale_pass'); ?>
					<?php echo $form->textField($model,'sale_pass',array('class'=>'form-control')); ?>
					<?php echo $form->error($model,'sale_pass'); ?>
				</div>

				<div class="col-md-4 form-group required">
					<?php echo $form->labelEx($model,'sale_show_as'); ?>
					<?php echo $form->textField($model,'sale_show_as',array('class'=>'form-control')); ?>
					<?php echo $form->error($model,'sale_show_as'); ?>
				</div>

				<div class="col-md-8 form-group required">
					<?php echo $form->labelEx($model,'sale_to'); ?>
					<?php echo $form->textField($model,'sale_to',array('class'=>'form-control')); ?>
					<?php echo $form->error($model,'sale_to'); ?>
				</div>
				<h3 class="box-title titletab">Alertas:</h3>
				<div class="col-md-4 form-group required">
					<?php echo $form->labelEx($model,'id_email_server_alerts'); ?>
					<?php echo $form->dropDownList($model, 'id_email_server_alerts',CHtml::listData(Emailserver::model()->findAll(), 'id', 'title'),array('empty' => 'Seleccione','class'=>'form-control'));?>
					<?php echo $form->error($model,'id_email_server_alerts'); ?>
				</div>
				<div class="col-md-4 form-group required">
					<?php echo $form->labelEx($model,'alerts_user'); ?>
					<?php echo $form->textField($model,'alerts_user',array('class'=>'form-control')); ?>
					<?php echo $form->error($model,'alerts_user'); ?>
				</div>

				<div class="col-md-4 form-group required">
					<?php echo $form->labelEx($model,'alerts_pass'); ?>
					<?php echo $form->textField($model,'alerts_pass',array('class'=>'form-control')); ?>
					<?php echo $form->error($model,'alerts_pass'); ?>
				</div>

				<div class="col-md-4 form-group required">
					<?php echo $form->labelEx($model,'alerts_show_as'); ?>
					<?php echo $form->textField($model,'alerts_show_as',array('class'=>'form-control')); ?>
					<?php echo $form->error($model,'alerts_show_as'); ?>
				</div>

				<div class="col-md-8 form-group required">
					<?php echo $form->labelEx($model,'alerts_to'); ?>
					<?php echo $form->textField($model,'alerts_to',array('class'=>'form-control')); ?>
					<?php echo $form->error($model,'alerts_to'); ?>
				</div>

				<div class="col-md-4 form-group required">
					<?php echo $form->labelEx($model,'alerts_subject_patterns'); ?>
					<?php echo $form->textField($model,'alerts_subject_patterns',array('class'=>'form-control')); ?>
					<?php echo $form->error($model,'alerts_subject_patterns'); ?>
				</div>
		  </div>
		  <!-- /.tab-pane -->
		  <div class="tab-pane" id="tab_2">
			<br />
			<div class="col-md-8 form-group ">
				<?php echo $form->labelEx($model,'tracking_description'); ?>
				<?php echo $form->textArea($model,'tracking_description',array('class'=>'form-control','rows'=>3)); ?>
				<?php echo $form->error($model,'tracking_description'); ?>
			</div>


			<div class="col-md-8 form-group">
				<?php echo $form->labelEx($model,'tracking_analitycs'); ?>
				<?php echo $form->textArea($model,'tracking_analitycs',array('class'=>'form-control','rows'=>10)); ?>
				<?php echo $form->error($model,'tracking_analitycs'); ?>
			</div>
		  </div>
		  <!-- /.tab-pane -->
		  <div class="tab-pane" id="tab_3">
				<br />
				<div class="col-md-4 form-group required">
					<?php echo $form->labelEx($model,'landings_order_price'); ?>
					<?php echo $form->dropDownList($model, 'landings_order_price',array('asc'=>'Ascendente','desc'=>'Descendente'),array('empty' => 'Seleccione','class'=>'form-control'));?>
					<?php echo $form->error($model,'landings_order_price'); ?>
				</div>

				<div class="col-md-4 form-group required">
					<?php echo $form->labelEx($model,'landings_order_departure'); ?>
					<?php echo $form->dropDownList($model, 'landings_order_departure',array('asc'=>'Ascendente','desc'=>'Descendente'),array('empty' => 'Seleccione','class'=>'form-control'));?>
					<?php echo $form->error($model,'landings_order_departure'); ?>
				</div>

				<div class="col-md-4 form-group required">
					<?php echo $form->labelEx($model,'landings_max_results_per_pages'); ?>
					<?php echo $form->textField($model,'landings_max_results_per_pages',array('class'=>'form-control numeros')); ?>
					<?php echo $form->error($model,'landings_max_results_per_pages'); ?>
				</div>

				<div class="form-group col-md-4 required">
					<?php echo $form->labelEx($model,'url_iframe'); ?>
					<div class="input-group">
						<?php echo $form->textField($model,'url_iframe',array('size'=>260,'maxlength'=>260,'placeholder'=>'http://www.ttsoffice.com','class'=>'form-control')); ?>
						<div class="input-group-addon pointer"><i class="fa fa-globe"></i></div>
					</div>
					<?php echo $form->error($model,'url_iframe'); ?>
				</div>
				<div class="col-md-4 form-group">
					<label>Filtros</label>
					<?php
					if(!$model->isNewRecord){
						$filtros=CHtml::listData($model->filtros, 'id_facets', 'id_facets');
					}else{
						$filtros=array();
					}
					echo CHtml::dropDownList( 'id_filters' ,$filtros, CHtml::listData(Facetsextendedfilters::model()->findAll('is_facets=0'), 'id', 'title'),array('multiple'=>'multiple','style'=>'width:100%','class'=>'form-control select2'));?>
				</div>
				<div class="col-md-4 form-group">
					<label>Facetas</label>
					<?php
					if(!$model->isNewRecord){
						$facetas=CHtml::listData($model->facetas, 'id_facets', 'id_facets');
					}else{
						$facetas=array();
					}
					echo CHtml::dropDownList( 'id_facets', $facetas ,CHtml::listData(Facetsextendedfilters::model()->findAll('is_facets=1'), 'id', 'title'),array('multiple'=>'multiple','style'=>'width:100%','class'=>'form-control select2'));?>
				</div>
		  </div>
		  <div class="tab-pane" id="tab_4">
		  <br />
		  	<div class="col-md-12 form-group ">
				<?php echo $form->labelEx($model,'script'); ?>
				<?php echo $form->textArea($model,'script',array('class'=>'form-control','rows'=>10)); ?>
				<?php echo $form->error($model,'script'); ?>
			</div>
		  </div>
		  <!-- /.tab-pane -->
		</div>
		<!-- /.tab-content -->
	  </div>
	  <!-- nav-tabs-custom -->
	</div>
</div>
	<div class="box-footer">
		<?php
			$title = (Yii::app()->controller->action->id!='view') ? 'Volver sin guardar cambios' : 'Volver';
			echo '<a href="'.Yii::app()->request->baseUrl.'/'.strtolower(Yii::app()->controller->id).'" class="btn btn-secundary cancel" data-toggle="tooltip" title="'.$title.'" >Cerrar</a>';
			if(Yii::app()->controller->action->id!='view'){
				echo CHtml::submitButton('Guardar',array('class'=>'btn btn-primary save','data-toggle'=>'tooltip','title'=>'Guardar cambios'));
			}
		?>
	</div>
<?php $this->endWidget(); ?>
</div><!-- form -->
<?php
	// Activo/Inactivo
	echo '<script>viewPage = "'.Yii::app()->controller->action->id.'";</script>';
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/js/form-validators.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerCssFile(Yii::app()->params['cdnUrl'].'/plugins/lcswich/lc_switch-tts.css');
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/lcswich/lc_switch.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/lcswich/lc_switch-tts.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/plugins/select2/select2.full.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl.'/js/plugins/select2/select2.css');
 ?>
 <script>
 $( document ).ready(function() {
	 $(".tab-pane").each(function() {
		if($(this).find('.errorMessage').length>0){
			$('.nav-tabs').find('li').removeClass('active');
			$("."+$(this).attr('id')).children().trigger( "click" );
			return false;
		}
	});
 });
 $( document ).ready(function() {
	$('.select2').select2().on('select2:unselect', function(e) {
		$('.select2-selection').trigger('click');
	});
 	$("#Search_id_channel").change(function(){
		$("#Search_id_subchannel").load(homeurl+"/filtersubchannel/"+$(this).val());
	});
});
</script>
