<?php
/* @var $this MailAssistantController */
/* @var $model MailAssistant */

$this->breadcrumbs=array(
	'Mail Assistants'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List MailAssistant', 'url'=>array('index')),
	array('label'=>'Create MailAssistant', 'url'=>array('create')),
	array('label'=>'Update MailAssistant', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete MailAssistant', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage MailAssistant', 'url'=>array('admin')),
);
?>

<h1>View MailAssistant #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'id_assistant',
	),
)); ?>
