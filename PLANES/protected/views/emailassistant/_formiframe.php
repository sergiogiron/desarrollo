<div class="col-md-3 search_bar">
					<div class="row">
						<div class="input-group input-group-sm">
							<input type="text" class="form-control" id="criterio"><i class="fa fa-close right-icon-search"></i>
							<span class="input-group-btn">
							  <button type="button" id="buscar" class="btn btn-default btn-flat"><i class="fa fa-search"></i></button>
							</span>
						</div>
					</div>
				</div>

<div class="box-body">
<div class="col-md-12 col-data">

	<div class="row">			
	<form id="asistenes_form" method="post">
	<div class="form-group">
		<?php
		$id_user=$idiframeuser;
		$id_filtrados = Yii::app()->db->createCommand("select id,name,lastname,email,phone from users where id NOT IN (select id_assistant from email_assistant where id_user=$id_user ) and id!=$id_user")->queryAll();
		?>
		<table id="datetable" class="table table-bordered table-hover dataTable table-list">
			<thead>
			<tr>
				<th>Nombre</th>
				<th>Apellido</th>
				<th>Email</th>
				<th></th>
			</tr>
			</thead>
			<tbody>
			<?php
			foreach($id_filtrados as $users){
				echo '<tr><td>'.$users['name'].'</td><td>'.$users['lastname'].'</td><td>'.$users['email'].'</td><td><input type="checkbox" class="asistentes" name="'.$users['id'].'" value="'.$users['id'].'"></td></tr>';
			}
			?>
			</tbody>
		</table>
		<div class="box-footer">
		  <div class="box-button-p">
			<input type="button" class="btn btn-primary agregar_asistentes" value="Guardar">
			<?php echo CHtml::button('Cerrar', array('onclick' => 'js:document.location.href="'.Yii::app()->request->baseUrl.'/iframe/asistentes/'.$usernameiframe.'"','class'=>'btn btn-secundary'));?>
		  </div>
		</div>
	</div>
  </div>
</form>
</div>
	</div>
</div>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/datatables/jquery.dataTables.min.js',CClientScript::POS_END); ?>
 
<?php Yii::app()->clientScript->registerCssFile(Yii::app()->params['cdnUrl'].'/plugins/sweetalert/sweetalert.css'); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/sweetalert/sweetalert.min.js',CClientScript::POS_END); ?>
<script>
$( document ).ready(function() {
	$( ".agregar_asistentes" ).click(function() {
		var asistentes=[];
		$(".asistentes:checked").each(function() {
			var asistente=new Object;
			asistente=$(this).val();
			asistentes.push(asistente);
		});
		if(asistentes.length>0){
			$( ".agregar_asistentes" ).attr('disable','disable');
			var request = $.ajax({
				url: homeurl+"/iframe/asistente/<?php echo $usernameiframe;?>/crear",
				method: "POST",
				data: {asistentes:asistentes},
			}).done(function(msg) {
				location.href=homeurl+'/iframe/asistentes/<?php echo($usernameiframe);?>';
			});
		}else{
			swal('Debe seleccionar al menos un asistente.');
		}
	});	
    $('#datetable').DataTable({
		"bLengthChange": false,
		"bInfo": false,
		"bAutoWidth": false,
		"pagingType": "full_numbers",
		"sDom": 'tp',
		"language": {
			"sSearch":"",
	        "paginate": {
	            first:    '<i class="fa fa-forward fa-flip-horizontal" data-toggle="tooltip" title="Primera página"></i>',
	            previous: '<i class="fa fa-caret-right fa-flip-horizontal" data-toggle="tooltip" title="Anterior"></i>',
	            next:     '<i class="fa fa-caret-right" data-toggle="tooltip" title="Siguiente"></i>',
	            last:     '<i class="fa fa-forward" data-toggle="tooltip" title="Última página"></i>'
	        },
		}
	});

oTable = $('#datetable').DataTable();  
$('#criterio').keyup(function(){
       oTable.search($(this).val()).draw() ;
})
 $(".right-icon-search").click(function(){
        $(this).parent().find("input").val('');
        $(this).css('color','#ccc');
		$('#buscar').trigger('click');	
var table = $('#datetable').DataTable();
table
 .search( '' )
 .columns().search( '' )
 .draw();	
    });

});	
</script>