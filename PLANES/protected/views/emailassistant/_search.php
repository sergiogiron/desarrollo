<?php
/* @var $this MailAssistantController */
/* @var $model MailAssistant */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

<div class="col-md-12" style="clear: both">
	<div class="row">
		<div class="col-md-3">
			<div class="row row_space">
		<label for="Emailassistant_id_assistant">Apellido</label>
		<?php echo $form->textField($model,'id_assistant',array('size'=>16,'maxlength'=>16,'class'=>'form-control')); ?>
			</div>
		</div>
		</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Buscar',array('class'=>'btn btn-default b_customize')); ?>
	</div>
<br />

<?php $this->endWidget(); ?>

</div><!-- search-form -->
</div><!-- search-form -->