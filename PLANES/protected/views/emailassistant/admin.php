<?php
/* @var $this MailAssistantController */
/* @var $model MailAssistant */

Yii::app()->clientScript->registerScript('search', "
	deleteBtn();
	search();
	searchAdd(); 
"); 
?>
<section class="content-header">
	<?php echo Yii::app()->user->Title(); ?>
	<?php echo Yii::app()->user->Breadcrumb(); ?>
</section>

	<section class="content">
		<div class="box box-danger">
		<div class="box-header with-border">
			<?php
			if(Yii::app()->user->checkAccess('create')){
				echo '<a href="'.Yii::app()->request->baseUrl.'/asistentes/crear"class="btn btn-default b_customize"><i class="fa fa-plus i_customize"></i> Agregar</a>';
			}
			?>
			<?php echo Utils::SearchObject(); ?>
			<?php echo CHtml::link('Búsqueda avanzada','#',array('class'=>'search-button')); ?>
			<div class="search-form" style="display:none">
			<?php $this->renderPartial('_search',array(
				'model'=>$model,
			)); ?>
			</div><!-- search-form -->			
		<?php $this->widget('zii.widgets.grid.CGridView', array(
			'id'=>'mail-assistant-grid',
			'dataProvider'=>$model->search(),
			//'filter'=>$model,
			'pager' => array(
				'cssFile'=>false,
				'header'=> '',
				'firstPageLabel' => '<i class="fa fa-forward fa-flip-horizontal" data-toggle="tooltip" title="Primera página"></i>',
				'prevPageLabel'  => '<i class="fa fa-caret-right fa-flip-horizontal" data-toggle="tooltip" title="Anterior"></i>',
				'nextPageLabel'  => '<i class="fa fa-caret-right" data-toggle="tooltip" title="Siguiente"></i>',
				'lastPageLabel'  => '<i class="fa fa-forward" data-toggle="tooltip" title="Última página"></i>', 
			),
			'summaryText' => '', 
			'itemsCssClass' => 'table table-bordered table-striped table-list',	
			'columns'=>array(
				array(
					'name'=>'usuarios.name',
					'filter'=>CHtml::activeTextField($model,'usuarios_name'),
				),	
				array(
					'name'=>'usuarios.lastname',
					'filter'=>CHtml::activeTextField($model,'usuarios_lastname'),
				),
				array(
					'name'=>'usuarios.email',
					'filter'=>CHtml::activeTextField($model,'usuarios_email'),
				),
				/*array(
					'name'=>'usuarios.phone',
					'filter'=>CHtml::activeTextField($model,'usuarios_personal_phone'),
				),	
				array(
					'name'=>'usuarios.created_at',
					'filter'=>CHtml::activeTextField($model,'created_at'),
					 'value'=>'Yii::app()->dateFormatter->format("d/M/y",strtotime($data->created_at))'
				),*/		
				array(
					'class'=>'CButtonColumn',
					'template'=>'{erase}',	
					'buttons'=>array(
						'erase' => array(
							'label'=>'<i class="fa fa-trash-o icon_cbutton"></i>',
							'url'=>'Yii::app()->createUrl(\'asistentes/borrar/\'. $data->id)', 
							'visible'=>'Yii::app()->user->checkAccess(\'delete\')',
							'imageUrl'=>false,
							'options' => array(
										'class' => 'delete_action',
										'title'=>'Borrar',
										'imageUrl'=>false,
									)	
						),						
					)
				)
			),
		)); ?>
		</div>
		</div>
 </section>
   <script>
$(document).ready(function(){
	$( "#buscar" ).click(function(){
		$('#mail-assistant-grid').yiiGridView('update', {
			data: { criterio : $('#criterio').val() }
		});
		return false;
	});	
	$("#criterio").keypress(function(e){
       if(e.which == 13) {
          $("#buscar").trigger('click');
       }
    });	
});	
</script>