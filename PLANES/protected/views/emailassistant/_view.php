<?php
/* @var $this MailAssistantController */
/* @var $data MailAssistant */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('asistente')); ?>:</b>
	<?php echo CHtml::encode($data->asistente); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('userstamp')); ?>:</b>
	<?php echo CHtml::encode($data->userstamp); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('datestamp')); ?>:</b>
	<?php echo CHtml::encode($data->datestamp); ?>
	<br />


</div>