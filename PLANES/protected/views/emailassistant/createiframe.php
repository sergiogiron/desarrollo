<?php
/* @var $this MailAssistantController */
/* @var $model MailAssistant */

?>
<section class="content-header">
  <h1>
	Asistentes
  </h1>
  <ol class="breadcrumb">
	<li><i class="fa fa-home"></i> Inicio</li>
	<li>Emailing</li>
  </ol>
</section>
<section class="content">
	<div class="box box-danger">
		<div class="box-header with-border">
			<?php $this->renderPartial('//emailassistant/_formiframe',array('idiframeuser'=>$idiframeuser,'usernameiframe'=>$usernameiframe)); ?>
		</div>
	</div>
</section>