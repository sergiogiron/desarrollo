<?php
/* @var $this MailAssistantController */
/* @var $model MailAssistant */
?>
<section class="content-header">
	<?php echo Yii::app()->user->Title(); ?>
	<?php echo Yii::app()->user->Breadcrumb(); ?>
</section>

<section class="content">
	<div class="box box-danger">
		<div class="box-header with-border">
			<?php $this->renderPartial('_form'); ?>
		</div>
	</div>
</section>