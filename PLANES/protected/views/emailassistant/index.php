<?php
/* @var $this MailAssistantController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Mail Assistants',
);

$this->menu=array(
	array('label'=>'Create MailAssistant', 'url'=>array('create')),
	array('label'=>'Manage MailAssistant', 'url'=>array('admin')),
);
?>

<h1>Mail Assistants</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
