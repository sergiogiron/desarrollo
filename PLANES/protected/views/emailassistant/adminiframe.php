<?php
/* @var $this MailAssistantController */
/* @var $model MailAssistant */

Yii::app()->clientScript->registerScript('delete_btn', "deleteBtn2();");
?>
<section class="content-header">
  <h1>
	Asistentes
  </h1>
  <ol class="breadcrumb">
	<li><i class="fa fa-home"></i> Inicio</li>	
	<li>Emailing</li>
  </ol>
</section>

	<section class="content">
		<div class="box box-danger">
		<div class="box-header with-border">
			<?php echo '<a href="'.Yii::app()->request->baseUrl.'/iframe/asistente/'.$usernameiframe.'/crear/"class="btn btn-default b_customize"><i class="fa fa-plus i_customize"></i> Agregar</a>';
			?>
			<?php echo Utils::SearchObject(); ?>	
			<?php echo CHtml::link('Búsqueda avanzada','#',array('class'=>'search-button')); ?>
			<div class="search-form" style="display:none">
			<?php $this->renderPartial('//emailassistant/_search',array(
				'model'=>$model,
			)); ?>
			</div><!-- search-form -->			
		<?php $this->widget('zii.widgets.grid.CGridView', array(
			'id'=>'mail-assistant-grid',
			'dataProvider'=>$model->searchbyUserid($idiframeuser),
			//'filter'=>$model,
			'pager' => array(
				'cssFile'=>false,
				'header'=> '',
				'firstPageLabel' => '<i class="fa fa-forward fa-flip-horizontal" data-toggle="tooltip" title="Primera página"></i>',
				'prevPageLabel'  => '<i class="fa fa-caret-right fa-flip-horizontal" data-toggle="tooltip" title="Anterior"></i>',
				'nextPageLabel'  => '<i class="fa fa-caret-right" data-toggle="tooltip" title="Siguiente"></i>',
				'lastPageLabel'  => '<i class="fa fa-forward" data-toggle="tooltip" title="Última página"></i>', 
			),
			'afterAjaxUpdate'=> 'function(){
					deleteBtn2();
				}',
			'summaryText' => '', 
			'itemsCssClass' => 'table table-bordered table-striped table-list',	
			'columns'=>array(
				array(
					'name'=>'usuarios.name',
					'filter'=>CHtml::activeTextField($model,'usuarios_name'),
				),	
				array(
					'name'=>'usuarios.lastname',
					'filter'=>CHtml::activeTextField($model,'usuarios_lastname'),
				),
				array(
					'name'=>'usuarios.email',
					'filter'=>CHtml::activeTextField($model,'usuarios_email'),
				),/*
				array(
					'name'=>'usuarios.phone',
					'filter'=>CHtml::activeTextField($model,'usuarios_personal_phone'),
				),	
				array(
					'name'=>'usuarios.created_at',
					'filter'=>CHtml::activeTextField($model,'created_at'),
					 'value'=>'Yii::app()->dateFormatter->format("d/M/y",strtotime($data->created_at))'
				),	*/	
				array(
					'class'=>'CButtonColumn',
					'template'=>'{erase}',	
					'buttons'=>array(
						'erase' => array(
							'label'=>'<i class="fa fa-trash-o icon_cbutton"></i>',
							'url'=>'Yii::app()->createUrl(\'iframe/asistente/'.$usernameiframe.'/borrar/\'. $data->id)',
							'options' => array(
								'class' => 'delete_action',
								'title'=>'Borrar',
								'imageUrl'=>false,
							)	
						),
					)
				)
			),
		)); ?>
		</div>
		</div>
 </section>
<?php Yii::app()->clientScript->registerCssFile(Yii::app()->params['cdnUrl'].'/plugins/sweetalert/sweetalert.css'); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/sweetalert/sweetalert.min.js',CClientScript::POS_END);
	echo '<script>viewPage = "'.Yii::app()->controller->action->id.'";</script>';
 ?>
   <script>
$(document).ready(function(){
	search();
});	
 
function deleteBtn2(){
	$( ".delete_action" ).click(function(event) {
		event.preventDefault();
		link=$(this).attr('href');
		swal({
			title: "Seguro desea eliminar este registro?", 
			text: "No se podrá recuperar el registro!",  
			type: "warning", 
			showCancelButton: true, 
			cancelButtonText: "Cancelar",
			confirmButtonColor: "#DD6B55", 
			confirmButtonText: "Sí, deseo borrarlo!", 
			closeOnConfirm: false
			}, 
			function(){
				$.ajax({
					url:link ,
					success:function() {
						swal("Operación correcta!", "Se ha eliminado el registro.", "success"); 
					},
					error:function(xhr) {
						 swal("Operación incorrecta!", xhr.responseText, "error"); 
					},
				}).done(function() {
				$('.grid-view').yiiGridView('update');
			});								
		});				  
	});
}
</script>