<?php
/* @var $this ApikeyController */
/* @var $model Apikey */
/* @var $form CActiveForm */
?>
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'apikey-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>
<div class="box-body">
	<span class="right_f">
<?php echo $form->checkBox($model,'enabled',array('class'=>'lcs_check enabled')); ?>
	
	</span>


	<p class="note">Los campos con <span class="required">*</span> son requeridos.</p>

	<div class="form-group">
		<?php echo $form->labelEx($model,'id_user_services'); ?>
						<?php echo $form->textField($model,'id_user_services',array('class'=>'form-control')); ?>
														<?php echo $form->error($model,'id_user_services'); ?>
	</div>
	<div class="form-group">
		<?php echo $form->labelEx($model,'api_key'); ?>
						<?php echo $form->textField($model,'api_key',array('size'=>60,'maxlength'=>128,'class'=>'form-control')); ?>
														<?php echo $form->error($model,'api_key'); ?>
	</div>
	<div class="form-group">
		<?php echo $form->labelEx($model,'expire_date_from'); ?>
						<?php echo $form->textField($model,'expire_date_from',array('class'=>'form-control datepicker','data-inputmask'=>'\'alias\': \'dd/mm/yyyy\'','data-mask'=>'data-mask')); ?>
														<?php echo $form->error($model,'expire_date_from'); ?>
	</div>
		
	<div class="form-group">		
		<div class="bootstrap-timepicker">
			<div class="form-group">
				<label for="Profile_title" class="required">Hora<span class="required">*</span></label>
				<div class="input-group">
					<input type="text" name="expire_date_from_hour" id="expire_date_from_hour" class="form-control timepicker" value="<?php if(isset($model->criterio["expire_date_from_hour"])){ echo $model->criterio["expire_date_from_hour"]; } ?>" > 
					<div class="input-group-addon">
						<i class="fa fa-clock-o"></i>
					</div>
				</div>
			</div>
		</div>	
	</div>
	<div class="form-group">
		<?php echo $form->labelEx($model,'expire_date_to'); ?>
						<?php echo $form->textField($model,'expire_date_to',array('class'=>'form-control datepicker','data-inputmask'=>'\'alias\': \'dd/mm/yyyy\'','data-mask'=>'data-mask')); ?>
														<?php echo $form->error($model,'expire_date_to'); ?>
	</div>
		
	<div class="form-group">		
		<div class="bootstrap-timepicker">
			<div class="form-group">
				<label for="Profile_title" class="required">Hora<span class="required">*</span></label>
				<div class="input-group">
					<input type="text" name="expire_date_to_hour" id="expire_date_to_hour" class="form-control timepicker" value="<?php if(isset($model->criterio["expire_date_to_hour"])){ echo $model->criterio["expire_date_to_hour"]; } ?>" > 
					<div class="input-group-addon">
						<i class="fa fa-clock-o"></i>
					</div>
				</div>
			</div>
		</div>	
	</div>
	<div class="form-group">
		<?php echo $form->labelEx($model,'valid_expire'); ?>
						<?php echo $form->textField($model,'valid_expire',array('class'=>'form-control')); ?>
														<?php echo $form->error($model,'valid_expire'); ?>
	</div>
	<div class="form-group">
		<?php echo $form->labelEx($model,'created_at'); ?>
						<?php echo $form->textField($model,'created_at',array('class'=>'form-control datepicker','data-inputmask'=>'\'alias\': \'dd/mm/yyyy\'','data-mask'=>'data-mask')); ?>
														<?php echo $form->error($model,'created_at'); ?>
	</div>
		
	<div class="form-group">		
		<div class="bootstrap-timepicker">
			<div class="form-group">
				<label for="Profile_title" class="required">Hora<span class="required">*</span></label>
				<div class="input-group">
					<input type="text" name="created_at_hour" id="created_at_hour" class="form-control timepicker" value="<?php if(isset($model->criterio["created_at_hour"])){ echo $model->criterio["created_at_hour"]; } ?>" > 
					<div class="input-group-addon">
						<i class="fa fa-clock-o"></i>
					</div>
				</div>
			</div>
		</div>	
	</div>
	<div class="form-group">
		<?php echo $form->labelEx($model,'id_user'); ?>
						<?php echo $form->textField($model,'id_user',array('class'=>'form-control')); ?>
														<?php echo $form->error($model,'id_user'); ?>
	</div>
	<?php if(Yii::app()->controller->action->id!='view'){ 	<div class="box-footer">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Crear' : 'Guardar',array('class'=>'btn btn-primary')); ?>
	</div>
	} ?><?php $this->endWidget(); ?>

</div><!-- form -->
<?php
	Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl.'/css/lc_switch.css'); 
	Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/lc_switch.js',CClientScript::POS_END);
 ?> 
<script>
$( document ).ready(function() {
	<?php if(Yii::app()->controller->action->id=='view'){ echo '$('form').find('input, textarea, button, checkbox, select').attr('disabled','disabled');'; } ?>	$('.enabled').lc_switch();
});		
</script>