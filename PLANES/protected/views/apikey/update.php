<?php
/* @var $this ApikeyController */
/* @var $model Apikey */

$this->breadcrumbs=array(
	'Apikeys'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Apikey', 'url'=>array('index')),
	array('label'=>'Manage Apikey', 'url'=>array('admin')),
);
?>
<section class="content-header">
  <h1>
	Apikey	<small>modificar</small>
  </h1>
  <ol class="breadcrumb">
	<li><a href="/TTS/office"><i class="fa fa-dashboard"></i> Home</a></li>
	<li><a href="/TTS/office/apikey">Apikey</a></li>
	<li class="active"><a href="">Modificar</a></li>
  </ol>
</section>
<section class="content">
	<div class="box box-danger">
		<div class="box-header with-border">
			<?php $this->renderPartial('_form', array('model'=>$model)); ?>		</div>
	</div>
</section>


