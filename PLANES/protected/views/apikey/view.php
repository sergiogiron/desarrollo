<?php
/* @var $this ApikeyController */
/* @var $model Apikey */

$this->breadcrumbs=array(
	'Apikeys'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Apikey', 'url'=>array('index')),
	array('label'=>'Create Apikey', 'url'=>array('create')),
	array('label'=>'Update Apikey', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Apikey', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Apikey', 'url'=>array('admin')),
);
?>

<section class="content-header">
	<h1>
		Apikeys	<small>ver</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="/TTS/office"><i class="fa fa-dashboard"></i> Home</a></li>
		<li><a href="/TTS/office/apikey">Apikey</a></li>
		<li class="active">Ver</li>
	</ol>
</section><id>
<section class="content">
	<div class="box box-danger">
		<div class="box-body">
<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'htmlOptions' => array('class' => 'table table-bordered table-striped'),
	'attributes'=>array(
		'id',
		'id_user_services',
		'api_key',
		'expire_date_from',
		'expire_date_to',
		'valid_expire',
		'enabled',
		'created_at',
		'id_user',
	),
)); ?>
		</div>
	</div>
</section>
