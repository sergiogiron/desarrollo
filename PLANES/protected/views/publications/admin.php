<?php
/* @var $this PublicationsController */


Yii::app()->clientScript->registerScript('search', "search();deleteBtn();"); 
?>
<section class="content-header">
	<?php echo Yii::app()->user->Title(); ?>
	<?php echo Yii::app()->user->Breadcrumb(); ?>
	<p class="registros">(<?=$model->count();?> registros encontrados)</p>
</section>
<section class="content">
	<div class="box box-danger">
		<div class="box-header with-border">	
		<div class="col-md-3 search_bar">
				<div class="row">
					<div class="input-group input-group-sm">
						<input type="text" class="form-control" id="criterio">
						<span class="input-group-btn">
						  <button type="button" id="buscar" class="btn btn-default btn-flat">
						  	<i class="fa fa-search"></i>
						  </button>
						</span>
					</div>
				</div>
			</div>	
			<?php echo CHtml::link('Búsqueda avanzada','#',array('class'=>'search-button')); ?>
			<div class="search-form" style="display:none">
			<?php $this->renderPartial('_search',array(
				'model'=>$model,
			)); ?>
			</div><!-- search-form -->

			<?php $this->widget('zii.widgets.grid.CGridView', array(
				'id'=>'publications-grid',
				'pager' => array(
                    'cssFile'=>false,
                    'header'=> '',
                    'firstPageLabel' => '<i class="fa fa-forward fa-flip-horizontal"></i>',
			        'prevPageLabel'  => '<i class="fa fa-caret-right fa-flip-horizontal"></i>',
			        'nextPageLabel'  => '<i class="fa fa-caret-right"></i>',
			        'lastPageLabel'  => '<i class="fa fa-forward"></i>',
			        //'afterAjaxUpdate'=> 'function(){enableSwitch();}',
                ),					
				'summaryText' => '', 
				'itemsCssClass' => 'table table-bordered table-striped table-list',				
				'dataProvider'=>$model->search(),
				'columns'=>array(
				array(
					'name' => 'id_channel',
					'value'=>'$data->channels==null ? " " : $data->channels->title',
				),
				array(
					'name' => 'id_subchannel',
					'value'=>'$data->subchannels==null ? " " : $data->subchannels->title',
				),
				array(
					'name' => 'id_routes_all',
					'value'=>'$data->route_general==null ? " " : $data->route_general->name',
				),				
				array(
                    'name' => 'publication_date',
                    'value' => 'Utils::datetime_spa($data->publication_date)',
                ),				
				'model',
				/*
				'created_at',
				*/
					array(
						'class'=>'CButtonColumn',
						'template'=>'{view}',
						'buttons'=>array(	
							'view' => array(
								'label'=>'<i class="fa fa-eye icon_cbutton"></i>',
								'url'=>'Yii::app()->createUrl(strtolower(Yii::app()->controller->id).\'/view/id/\'. $data->id)',							
								'visible'=>'Yii::app()->user->checkAccess(\'view\')',
								'options'=>array('title'=>'Ver '.strtolower(Yii::app()->controller->id)),
								'imageUrl'=>false,
							),
						),	
					),
				)
			)); ?>
		</div>
	</div>
 </section>