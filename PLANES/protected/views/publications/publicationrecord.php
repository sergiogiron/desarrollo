<div class="box-body">
	<form id="asistenes_form" method="post">
	<div class="form-group">
		<table id="datetable" class="table table-bordered table-hover dataTable">
			<thead>
			<tr>
				<th data-container="body" data-toggle="tooltip" data-placement="top" title="Ordenar por">Usuario</th>
				<th data-container="body" data-toggle="tooltip" data-placement="top" title="Ordenar por">Fecha de creación</th>
				<th data-container="body" data-toggle="tooltip" data-placement="top" title="Ordenar por">Fecha de publicación</th>
			</tr>
			</thead>
			<tbody>
			<?php
			foreach($model as $publication){
				echo '<tr>
				<td>'.$publication->user->name.' '.$publication->user->lastname.'</td>
				<td>'.Utils::datetime_spa($publication->created_at).'</td>
				<td>'.Utils::datetime_spa($publication->publication_date).'</td>
				</tr>';
			}
			?>
			</tbody>
		</table>
	</div>
  </div>
</form>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/jquery.dataTables.min.js',CClientScript::POS_END);	?>
<script>
$('[data-toggle="tooltip"]').tooltip();
$(".dataTable").ready(function(){
	setTimeout(function(){
	if($(".dataTables_paginate span").length<=1){
		$(".dataTables_paginate").hide();
	}else{
		$(".dataTables_paginate").show();
	}

},8);
});
</script>
