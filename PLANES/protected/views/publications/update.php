<?php
/* @var $this PublicationsController */
/* @var $model Publications */

$this->breadcrumbs=array(
	'Publications'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Publications', 'url'=>array('index')),
	array('label'=>'Manage Publications', 'url'=>array('admin')),
);
?>
<section class="content-header">
  <h1>
	Publications	<small>modificar</small>
  </h1>
  <ol class="breadcrumb">
	<li><a href="/ttsoffice_repo"><i class="fa fa-dashboard"></i> Home</a></li>
	<li><a href="/ttsoffice_repo/publications">Publications</a></li>
	<li class="active"><a href="">Modificar</a></li>
  </ol>
</section>
<section class="content">
	<div class="box box-danger">
		<div class="box-header with-border">
			<?php $this->renderPartial('_form', array('model'=>$model)); ?>		</div>
	</div>
</section>


