<?php
/* @var $this PublicationsController */
/* @var $model Publications */
/* @var $form CActiveForm */
?>

<div class="box-body">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'publications-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<div class="col-md-12 col-data">

	<div class="form-group col-md-6">
		<?php echo $form->labelEx($model,'id_user'); ?>
		<?php echo $form->textField($model,'id_user',array('class'=>'form-control')); ?>
		<?php echo $form->error($model,'id_user'); ?>
	</div>

	<div class="form-group col-md-6">
		<?php echo $form->labelEx($model,'content'); ?>
		<?php echo $form->textArea($model,'content',array('rows'=>6, 'cols'=>50,'class'=>'form-control')); ?>
		<?php echo $form->error($model,'content'); ?>
	</div>

	<div class="form-group col-md-6">
		<?php echo $form->labelEx($model,'id_channel'); ?>
		<?php echo $form->textField($model,'id_channel',array('class'=>'form-control')); ?>
		<?php echo $form->error($model,'id_channel'); ?>
	</div>

	<div class="form-group col-md-6">
		<?php echo $form->labelEx($model,'id_subchannel'); ?>
		<?php echo $form->textField($model,'id_subchannel',array('class'=>'form-control')); ?>
		<?php echo $form->error($model,'id_subchannel'); ?>
	</div>

	<div class="form-group col-md-6">
		<?php echo $form->labelEx($model,'publication_date'); ?>
		<?php echo $form->textField($model,'publication_date',array('class'=>'form-control')); ?>
		<?php echo $form->error($model,'publication_date'); ?>
	</div>

	<div class="form-group col-md-6">
		<?php echo $form->labelEx($model,'model'); ?>
		<?php echo $form->textField($model,'model',array('size'=>60,'maxlength'=>64,'class'=>'form-control')); ?>
		<?php echo $form->error($model,'model'); ?>
	</div>

	<div class="form-group col-md-6">
		<?php echo $form->labelEx($model,'created_at'); ?>
		<?php echo $form->textField($model,'created_at',array('class'=>'form-control')); ?>
		<?php echo $form->error($model,'created_at'); ?>
	</div>

	<div class="box-footer">
		<?php
			$title = (Yii::app()->controller->action->id!='view') ? 'Volver sin guardar cambios' : 'Volver';
			echo '<a href="'.Yii::app()->request->baseUrl.'/'.strtolower(Yii::app()->controller->id).'" class="btn btn-secundary" data-toggle="tooltip" title="'.$title.'" >Cerrar</a>';
			if(Yii::app()->controller->action->id!='view'){
				echo CHtml::submitButton('Guardar',array('class'=>'btn btn-primary','data-toggle'=>'tooltip','title'=>'Guardar cambios'));
			}
		?>
	</div>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
<script type="text/javascript">
	<?php
		if(Yii::app()->controller->action->id=='view'){ echo "$('form').find('input, textarea, checkbox, select').attr('disabled','disabled'); $('.fa-pencil-square-o').hide()"; }
	?>	
</script>
