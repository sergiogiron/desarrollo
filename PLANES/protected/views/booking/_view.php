<?php
/* @var $this BookingController */
/* @var $data Booking */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('checkoutId')); ?>:</b>
	<?php echo CHtml::encode($data->checkoutId); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_channel')); ?>:</b>
	<?php echo CHtml::encode($data->id_channel); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_subchannel')); ?>:</b>
	<?php echo CHtml::encode($data->id_subchannel); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
	<?php echo CHtml::encode($data->status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('passenger')); ?>:</b>
	<?php echo CHtml::encode($data->passenger); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('date')); ?>:</b>
	<?php echo CHtml::encode($data->date); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('book_code')); ?>:</b>
	<?php echo CHtml::encode($data->book_code); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('package')); ?>:</b>
	<?php echo CHtml::encode($data->package); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('expire_date')); ?>:</b>
	<?php echo CHtml::encode($data->expire_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('json')); ?>:</b>
	<?php echo CHtml::encode($data->json); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('email_client')); ?>:</b>
	<?php echo CHtml::encode($data->email_client); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('email_operation')); ?>:</b>
	<?php echo CHtml::encode($data->email_operation); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('consumer')); ?>:</b>
	<?php echo CHtml::encode($data->consumer); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_providers')); ?>:</b>
	<?php echo CHtml::encode($data->id_providers); ?>
	<br />

	*/ ?>

</div>