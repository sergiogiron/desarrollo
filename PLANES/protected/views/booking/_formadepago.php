<h1><i class="fa fa-credit-card"></i> Forma de pago</h1>
<div class="row">
 	<div class="col-xs-12">
	 	<div class="row">
	 	<?php if(strtolower($formapago->entity) == 'todopago'){ ?>
	 		<div class="col-xs-12">Forma de Pago: TodoPago</div>
	 	<?php } 
	 	elseif( strtolower($formapago->entity) == 'hsbc' || $formapago->credit_card->paymentMethod->dues ==1){ ?>	   	   <div class="col-xs-12">Forma de Pago: Nps </div>	
	 		<div class="col-xs-12">Banco: <?=$formapago->entity;?></div>
	 		<div class="col-xs-12">Tarjeta: <?=$formapago->credit_card->credit_card;?></div>
	 		<div class="col-xs-12">Cuotas: <?=$formapago->credit_card->paymentMethod->dues;?></div>
	 		<div class="col-xs-12">Intereses: <?=$formapago->credit_card->paymentMethod->interest;?>%</div>
	 		<div class="col-xs-12">CFT: <?=$formapago->credit_card->paymentMethod->cftn;?></div>	
	 	<?php }else{ ?>
			<div class="col-xs-12">Forma de Pago: A convenir </div>	
	 		<div class="col-xs-12">Banco: <?=$formapago->entity;?></div>
	 		<div class="col-xs-12">Tarjeta: <?=$formapago->credit_card->credit_card;?></div>
	 		<div class="col-xs-12">Cuotas: <?=$formapago->credit_card->paymentMethod->dues;?></div>
	 		<div class="col-xs-12">Intereses: <?=$formapago->credit_card->paymentMethod->interest;?>%</div>
	 		<div class="col-xs-12">CFT: <?=$formapago->credit_card->paymentMethod->cftn;?></div>	
	 	<?php } ?>
	 	</div>
	</div>
</div>
<h1><i class="fa fa-usd"></i> Datos precios</h1>
<div class="row">
 	<div class="col-xs-12">
	 	<div class="row">

	 		<div class="col-xs-12"><h4><b>Total a cobrar: $ <?=Utils::formatPrice($formapago->credit_card->paymentMethod->pricing->TotalFare);?></b></h4></div>
	 		<div class="col-xs-12">FEE: $ <?=$formapago->credit_card->paymentMethod->pricing->FEE;?></div>
	 		<div class="col-xs-12">Total Tarifa Bruta: $ <?=Utils::formatPrice($formapago->credit_card->paymentMethod->pricing->NetFare);?></div>
	 		<div class="col-xs-12">Impuestos: $ <?=Utils::formatPrice($formapago->credit_card->paymentMethod->pricing->Taxes);?></div>
	 		<div class="col-xs-12">IVA: $ <?=$formapago->credit_card->paymentMethod->pricing->IVA;?></div>
	 		<div class="col-xs-12">Descuento: <?=$formapago->credit_card->paymentMethod->pricing->Discount?> (<?=$formapago->credit_card->paymentMethod->pricing->PercentDiscount;?>%)</div>
	 		<div class="col-xs-12">Precio por pasajero: $ <?=Utils::formatPrice($formapago->credit_card->paymentMethod->pricing->PricePerPax);?></div>
	 		<div class="col-xs-12">Impuestos tasas y cargos: $ <?=Utils::formatPrice($formapago->credit_card->paymentMethod->pricing->TaxesAndOtherCharges);?></div>
	 		<div class="col-xs-12">Precio Adulto: $ <?=Utils::formatPrice($formapago->credit_card->paymentMethod->pricing->NetAdultsFare);?></div>
	 		<div class="col-xs-12">Precio Menor: $ <?=Utils::formatPrice($formapago->credit_card->paymentMethod->pricing->NetChildrenFare);?></div>
	 		<div class="col-xs-12">Precio Infante: $ <?=Utils::formatPrice($formapago->credit_card->paymentMethod->pricing->NetInfantsFare);?></div>
	 		
	 	</div>
	</div>
<?php if(count($trys_payments)>0){ ?> 
	<div class="row">
			<div class="col-xs-12">
				<h1><i class="fa fa-list-alt "></i> Historial de pago</h1>
			</div>
	 		<div class="col-xs-12" id="width_fix" style="width: 723px;">
					<div class="table-responsive no-padding">
						<table class="table table-striped" >
							<thead>
								<tr>
									<th>ID</th>
									<th style="display: none;">Tjt. nombre</th>
									<th style="display: none;">Tjt. numero</th>
									<th >Tarjeta</th>
									<th>Banco</th> 
									<th style="display: none;">Vencimiento</th>
									<th>Cuotas</th>
									<th>Precio final</th>
									<th style="display: none;">PrecioFinalMoneda</th>
									<th style="display: none;">Tipo De Puntos</th>
									<th style="display: none;">Puntos</th>
									<th style="display: none;">CodAutorizacion</th>
									<th>N° de Tarjeta</th>
									<th style="display: none;">Concepto</th>
									<th>Estado</th>
									<th style="display: none;">Merchant ID</th>
									<th style="display: none;">CVV</th>
									<th>Transaction ID</th>
									<th style="display: none;">ID Pasarela</th>
								</tr>
							</thead>
							<tbody>
								<?php foreach ($trys_payments as $key => $value) { ?>
									<tr>
										<td><?=$value['IdPago'];?></td>
										<td style="display: none;"><?=$value['tarjetanombre'];?></td>
										<td style="display: none;"><?=$value['tarjetanumero'];?></td>
										<td ><?=$value['tarjetatipo'];?></td>
										<td><?=$value['tarjetabanco'];?></td> 
										<td style="display: none;"><?=$value['tarjetavencimiento'];?></td>
										<td><?=$value['tarjetacuotas'];?></td>
										<td><?=$value['preciofinal'];?></td>
										<td style="display: none;"><?=$value['PrecioFinalMoneda'];?></td>
										<td style="display: none;"><?=$value['TipoDePuntos'];?></td>
										<td style="display: none;"><?=$value['Puntos'];?></td>
										<td style="display: none;"><?=$value['CodAutorizacion'];?></td>
										<td ><?=$value['TokenTarjeta'];?></td>
										<td style="display: none;"><?=$value['Concepto'];?></td>
										<td><?=$value['DescEstado'];?></td>
										<td style="display: none;"><?=$value['MerchandId'];?></td>
										<td style="display: none;"><?=$value['CVV'];?></td>
										<td><?=$value['TransactionId'];?></td>
										<td style="display: none;"><?=$value['IdPasarela'];?></td>
									</tr>
								<?php } ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
	<?php } ?>
</div>
<script type="text/javascript">
	$(window).resize(function() {
   		var test = $('#tab_4 .content').width();
   		$('#width_fix').css('width',test+'px');

	});
	//$(window).trigger('resize');
</script>