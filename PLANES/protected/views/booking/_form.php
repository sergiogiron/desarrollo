<?php
/* @var $this BookingController */
/* @var $model Booking */
/* @var $form CActiveForm */
?>

<div class="box-body">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'booking-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Los campos con <span class="required">*</span> son requeridos.</p>

	<div class="form-group">
		<?php echo $form->labelEx($model,'checkoutId'); ?>
		<?php echo $form->textField($model,'checkoutId',array('size'=>20,'maxlength'=>20,'class'=>'form-control')); ?>
		<?php echo $form->error($model,'checkoutId'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'id_channel'); ?>
		<?php echo $form->textField($model,'id_channel',array('class'=>'form-control')); ?>
		<?php echo $form->error($model,'id_channel'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'id_subchannel'); ?>
		<?php echo $form->textField($model,'id_subchannel',array('class'=>'form-control')); ?>
		<?php echo $form->error($model,'id_subchannel'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'status'); ?>
		<?php echo $form->textField($model,'status',array('size'=>20,'maxlength'=>20,'class'=>'form-control')); ?>
		<?php echo $form->error($model,'status'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'passenger'); ?>
		<?php echo $form->textField($model,'passenger',array('size'=>50,'maxlength'=>50,'class'=>'form-control')); ?>
		<?php echo $form->error($model,'passenger'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'date'); ?>
		<?php echo $form->textField($model,'date',array('class'=>'form-control')); ?>
		<?php echo $form->error($model,'date'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'book_code'); ?>
		<?php echo $form->textField($model,'book_code',array('size'=>20,'maxlength'=>20,'class'=>'form-control')); ?>
		<?php echo $form->error($model,'book_code'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'package'); ?>
		<?php echo $form->textField($model,'package',array('size'=>50,'maxlength'=>50,'class'=>'form-control')); ?>
		<?php echo $form->error($model,'package'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'expire_date'); ?>
		<?php echo $form->textField($model,'expire_date',array('class'=>'form-control')); ?>
		<?php echo $form->error($model,'expire_date'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'json'); ?>
		<?php echo $form->textArea($model,'json',array('rows'=>6, 'cols'=>50,'class'=>'form-control')); ?>
		<?php echo $form->error($model,'json'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'email_client'); ?>
		<?php echo $form->textArea($model,'email_client',array('rows'=>6, 'cols'=>50,'class'=>'form-control')); ?>
		<?php echo $form->error($model,'email_client'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'email_operation'); ?>
		<?php echo $form->textArea($model,'email_operation',array('rows'=>6, 'cols'=>50,'class'=>'form-control')); ?>
		<?php echo $form->error($model,'email_operation'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'consumer'); ?>
		<?php echo $form->textField($model,'consumer',array('size'=>55,'maxlength'=>55,'class'=>'form-control')); ?>
		<?php echo $form->error($model,'consumer'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'id_providers'); ?>
		<?php echo $form->textField($model,'id_providers',array('class'=>'form-control')); ?>
		<?php echo $form->error($model,'id_providers'); ?>
	</div>

	<div class="box-footer">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Crear' : 'Guardar',array('class'=>'btn btn-primary')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->