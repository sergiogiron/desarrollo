<div id="<?=$id;?>-grid" class="grid-view">
	<div class="summary"></div>
	<table class="table table-bordered table-striped table-list">
		<thead>
			<tr>
				<th>Fecha de Reserva</th>
				<th>Subchannel</th>
				<th>Nro Reserva</th>
				<!--<th>Checkout ID</th>-->
				
				<th>Cliente</th>
				<?php if($id == 'resultados'){ ?>
					<th >Estado</th> 
				<?php } ?>
				<th>Email</th>
				<th>Paquete</th>
				<th>Fecha Exp</th>
				<th class="button-column">Acciones</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($data as $key => $value) { ?>
			<tr >
				<td><?=$value['fecha'];?></td>
				<td><?=$value['subchannel'];?></td>
				<td><?=$value['checkout_id'];?></td>
				<!--<td><?=$value['nro_reserva'];?></td>-->
				
				<td><?=$value['cliente'];?></td>
				<?php if($id == 'resultados'){ ?>
					<td><?=$value['estado'];?></td>
				<?php } ?>
				<td><?=$value['email'];?></td>
				<td><?=$value['nombre_paquete'];?></td>
				<td><?=$value['expiracion'];?></td>
				<td class="button-column">
					<a title="Detalle" class="open_lightbox" data-toggle="tooltip" data-provider="<?=$value['proveedor'];?>"  data-booking="<?=$value['checkout_id'];?>" href="<?=Yii::app()->createUrl(strtolower(Yii::app()->controller->id).'/view/id/'. $value["checkout_id"]);?>"><i class="fa fa-eye icon_cbutton"></i></a>
					<?php if ($value["estado"] == 'Pendiente'){ ?>
 					<a title="Confirmar" data-type="confirmar" data-provider="<?=$value['proveedor'];?>"  data-booking="<?=$value['checkout_id'];?>" class="accion_reserva" data-toggle="tooltip" href="<?=Yii::app()->createUrl(strtolower(Yii::app()->controller->id).'/confirm/id/'.$value["checkout_id"].'/proveedor/'.$value["id_proveedor"].'/checkout_id/'.$value["checkout_id"])?>"><i class="fa fa-check-circle-o icon_cbutton"></i></a>
					<?php } ?>
					<?php if ( $value["proveedor"] == 'OLA' && ($value["estado"] == 'Pendiente' || $value["estado"] == 'Confirmado')){ ?>
					<a title="Cancelar" data-type="cancelar" data-provider="<?=$value['proveedor'];?>"  data-booking="<?=$value['nro_reserva'];?>" class="accion_reserva" data-toggle="tooltip" href="<?=Yii::app()->createUrl(strtolower(Yii::app()->controller->id).'/cancel/id/'. $value["nro_reserva"].'/proveedor/'.$value["id_proveedor"].'/checkout_id/'.$value["checkout_id"]);?>"><i class="fa fa-ban icon_cbutton"></i></a>
					<?php } ?>
				</td>
			</tr>
			<?php } ?>		
		</tbody>
	</table>
	<!-- <div class="keys" style="display:none" title="/ttsoffice_frontend/booking"><span>nro_reserva</span><span>checkout_id</span><span>cliente</span><span>email</span></div> -->
</div>



<?php /*$arrayDataProvider = new CArrayDataProvider($data, array(
    'id'=>$id,
    'keyField' => 'nro_reserva',
    'keys'=>array('nro_reserva','checkout_id', 'cliente','email'),
    'sort'=>array(
    	'defaultOrder'=>'checkout_id ASC',
        'attributes'=>array(
            'subchannel',
            'nro_reserva',
            'checkout_id', 
            'cliente',
            'email',
            'proveedor',
			),
		),
    'pagination'=>array(
        'pageSize'=>10
		),
)); 

//$arrayDataProvider->setSort('proveedor');
if($id == 'resultados'){
	$arrayShow = array();
}else{
	$arrayShow = array('style' => 'display:none');
}
?>

<?php $this->widget('zii.widgets.grid.CGridView', array(
		'id'=> $id.'-grid',
		'summaryText' => '', 
		'dataProvider'=>$arrayDataProvider,
		'ajaxUrl' => $this->createUrl('admin'),
		'itemsCssClass' => 'table table-bordered table-striped table-list',
		'enableSorting'=>true,
		'pager' => array(
				'cssFile'=>false,
				'header'=> '',
				'firstPageLabel' => '<i class="fa fa-forward fa-flip-horizontal"></i>',
				'prevPageLabel'  => '<i class="fa fa-caret-right fa-flip-horizontal"></i>',
				'nextPageLabel'  => '<i class="fa fa-caret-right"></i>',
				'lastPageLabel'  => '<i class="fa fa-forward"></i>',
			),
		//'filter'=>$model,
		'columns'=>array(		
			array(
				'name' => 'subchannel',
				'header' => 'Subchannel',
			),
			array(
				'name' => 'nro_reserva',
				'header' => 'Nro Reserva',
			),				
			array(
				'name' 	=> 'checkout_id',
				'header' => 'Checkout ID',
			),				
			array(
				'name' => 'cliente',
				'header' => 'Cliente',
			),
			array(
				'name' => 'estado',
				'header' => 'Estado',
				'visible' =>'$id == "resultados"',
				'htmlOptions' => $arrayShow,
				'headerHtmlOptions' => $arrayShow,
				'filterHtmlOptions' => $arrayShow,
			),				
			array(
				'name' => 'email',
				'header' => 'Email',
			),
			array(
				'name' => 'proveedor',
				'header' => 'Proveedor',
			),				
			array(
				'name' => 'nombre_paquete',
				'header' => 'Paquete',
			),				
			array(
				'name' => 'fecha',
				'header' => 'Fecha',
			),
			array(
				'name' => 'expiracion',
				'header' => 'Fecha Exp',
			),
			array(
				'class'=>'CButtonColumn',
				'header'=>'Acciones',
				'template'=>'{view}{update}{cancel}',	
				'buttons'=>array(					
					'view' => array(
						'label'=>'<i class="fa fa-eye icon_cbutton"></i>',
						'url'=>'Yii::app()->createUrl(strtolower(Yii::app()->controller->id).\'/view/id/\'. $data["checkout_id"])',
						'options'=>array('title'=>'Ver', 'class'=>'open_lightbox','data-toggle'=>'tooltip'),
						'imageUrl'=>false,
					),	
					'update' => array(
						'label'=>'<i class="fa fa-check-circle-o icon_cbutton"></i>',
										'url'=>'Yii::app()->createUrl(strtolower(Yii::app()->controller->id).\'/confirm/id/\'. $data["nro_reserva"].\'/proveedor/\'.$data["id_proveedor"].\'/checkout_id/\'.$data["checkout_id"])',
						'options'=>array('title'=>'Confirmar',  'class'=>'accion_reserva','data-toggle'=>'tooltip'),
						'imageUrl'=>false,
						'visible' => '$data["estado"] == \'Pendiente\''
					),					
					'cancel' => array(
						'label'=>'<i class="fa fa-ban icon_cbutton"></i>',
						'url'=>'Yii::app()->createUrl(strtolower(Yii::app()->controller->id).\'/cancel/id/\'. $data["nro_reserva"].\'/proveedor/\'.$data["id_proveedor"].\'/checkout_id/\'.$data["checkout_id"])',
						'options'=>array('title'=>'Cancelar', 'class'=>'accion_reserva','data-toggle'=>'tooltip'),
						'imageUrl'=>false,
						'visible' => '$data["estado"] == \'Pendiente\' || $data["estado"] == \'Confirmado\''
					),
				),
			),
		)
	)); */?>
