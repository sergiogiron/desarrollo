<?php
/* @var $this PruebaController */
/* @var $model Prueba */

$this->breadcrumbs=array(
	'Pruebas'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Prueba', 'url'=>array('index')),
	array('label'=>'Manage Prueba', 'url'=>array('admin')),
);
?>
<section class="content-header">
  <h1>
	Prueba	<small>modificar</small>
  </h1>
  <ol class="breadcrumb">
	<li><a href="/ttsoffice_repo"><i class="fa fa-dashboard"></i> Home</a></li>
	<li><a href="/ttsoffice_repo/prueba">Prueba</a></li>
	<li class="active"><a href="">Modificar</a></li>
  </ol>
</section>
<section class="content">
	<div class="box box-danger">
		<div class="box-header with-border">
			<?php $this->renderPartial('_form', array('model'=>$model)); ?>		</div>
	</div>
</section>


