<?php
/* @var $this PruebaController */
/* @var $model Prueba */
/* @var $form CActiveForm */
?>
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'prueba-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>
<div class="box-body">
	<span class="right_f">
<?php echo $form->checkBox($model,'enabled',array('class'=>'lcs_check enabled')); ?>
	
	</span>
	<div class="col-md-12 col-data">

		<div class="form-group col-md-6 required">
			<?php echo $form->labelEx($model,'name'); ?>
			<?php echo $form->textField($model,'name',array('size'=>62,'maxlength'=>62,'class'=>'form-control letras')); ?>
			<?php echo $form->error($model,'name'); ?>
		</div>
		<div class="form-group col-md-6 required">
			<?php echo $form->labelEx($model,'description'); ?>
			<?php echo $form->textArea($model,'description',array('class'=>'form-control')); ?>
			<?php echo $form->error($model,'description'); ?>
		</div>
		<div class="form-group col-md-6 required">
			<?php echo $form->labelEx($model,'title'); ?>
			<?php echo $form->textField($model,'title',array('size'=>65,'maxlength'=>65,'class'=>'form-control letras_numeros')); ?>
			<?php echo $form->error($model,'title'); ?>
		</div>
		<div class="form-group col-md-6 required">
			<?php echo $form->labelEx($model,'url'); ?>
			<div class="input-group">		
				<?php echo $form->textField($model,'url',array('size'=>260,'maxlength'=>260,'placeholder'=>'http://www.ttsoffice.com','class'=>'form-control')); ?>
				<div class="input-group-addon"><i class="fa fa-globe"></i></div>						
			</div>
			<?php echo $form->error($model,'url'); ?>						
		</div>
		<div class="form-group col-md-6 required">
			<?php echo $form->labelEx($model,'email'); ?>
			<div class="input-group">		<?php echo $form->EmailField($model,'email',array('size'=>80,'maxlength'=>80,'class'=>'form-control')); ?>
				<div class="input-group-addon"><i class="fa fa-envelope-o"></i></div>		
			</div>						
			<?php echo $form->error($model,'email'); ?>
		</div>
		<div class="form-group col-md-6 required">
			<?php echo $form->labelEx($model,'phone'); ?>
					<div class="input-group">		<?php echo $form->textField($model,'phone',array('size'=>35,'maxlength'=>35,'class'=>'form-control phone')); ?>
					<div class="input-group-addon"><i class="fa fa-phone"></i></div>				</div>						<?php echo $form->error($model,'phone'); ?>
		</div>
		<div class="form-group col-md-6 required">
			<?php echo $form->labelEx($model,'datepicker'); ?>
			<div class="input-group">				
				<?php echo $form->textField($model,'datepicker',array('class'=>'form-control datepicker','data-inputmask'=>'\'alias\': \'dd/mm/yyyy\'','data-mask'=>'data-mask')); ?>
				<div class="input-group-addon pointer" data-toggle="tooltip" title="Abrir el calendario"><i class="fa fa-calendar"></i></div>		
			</div>		
			<?php echo $form->error($model,'datepicker'); ?>
		</div>
		<div class="form-group col-md-6 required">
			<?php echo $form->labelEx($model,'timepicker'); ?>
			<div class="input-group bootstrap-timepicker">
				<?php echo $form->textField($model,'timepicker',array('class'=>'form-control timepicker','data-inputmask'=>'\'alias\': \'dd/mm/yyyy\'','data-mask'=>'data-mask')); ?>
				<div class="input-group-addon pointer" data-toggle="tooltip" title="Abrir el reloj"><i class="fa fa-clock-o"></i></div>	
			</div>
			<?php echo $form->error($model,'timepicker'); ?>
		</div>
			
		<div class="form-group col-md-6 required">
			<?php echo $form->labelEx($model,'text_ritch'); ?>
			<?php echo $form->textArea($model,'text_ritch',array('class'=>'form-control ckeditor','rows'=>6,'cols'=>6)); ?>
			<?php echo $form->error($model,'text_ritch'); ?>
		</div>
		<div class="form-group col-md-6 unique">
			<?php echo $form->labelEx($model,'testname'); ?>
			<?php echo $form->textField($model,'testname',array('size'=>260,'maxlength'=>260,'class'=>'form-control')); ?>
			<?php echo $form->error($model,'testname'); ?>						
		</div>
		<div class="form-group col-md-6 required">
			<?php echo $form->labelEx($model,'file'); ?>
			<div class="input-group">		
				<div size="260" maxlength="260" placeholder="Subi tu archivo" class="form-control file" attr="disabled" id="trigger">Subi tu archivo</div>
				<div class="input-group-addon pointer" data-toggle="tooltip" title="Subir un archivo"><i class="fa fa-upload"></i></div>
				<?php echo $form->fileField($model,'file',array('size'=>62,'maxlength'=>62,'class'=>'hidden','id'=>"file")); ?>						
			</div>
			<?php echo $form->error($model,'file'); ?>						
		</div>



	</div>
	<div class="box-footer">
		<?php
			echo CHtml::button('Cerrar', array('onclick' => 'js:document.location.href="'.Yii::app()->request->baseUrl.'/'.strtolower(Yii::app()->controller->id).'"','class'=>'btn btn-secundary','data-toggle'=>'tooltip','title'=>'Volver sin guardar cambios'));
			if(Yii::app()->controller->action->id!='view'){
				echo CHtml::submitButton($model->isNewRecord ? 'Crear' : 'Guardar',array('class'=>'btn btn-primary','data-toggle'=>'tooltip','title'=>'Guardar cambios'));
			}
		?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
<?php
	// Activo/Inactivo
	Yii::app()->clientScript->registerCssFile(Yii::app()->params['cdnUrl'].'/plugins/lcswich/lc_switch-tts.css'); 
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/lcswich/lc_switch.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/lcswich/lc_switch-tts.js',CClientScript::POS_END);
	
	// Datepicker con la mascara
	Yii::app()->clientScript->registerCssFile(Yii::app()->params['cdnUrl'].'/plugins/datepicker/datepicker3.css');
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/datepicker/bootstrap-datepicker.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/input-mask/jquery.inputmask.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/input-mask/jquery.inputmask.date.extensions.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/datepicker/datepicker-tts.js',CClientScript::POS_END);
	// Timepicker con la mascara
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/timepicker/bootstrap-timepicker.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/input-mask/jquery.inputmask.bundle.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerCssFile(Yii::app()->params['cdnUrl'].'/plugins/timepicker/bootstrap-timepicker.css');
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/timepicker/timepicker-tts.js',CClientScript::POS_END);
	// Editor de texto enriquesido
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/ckeditor-standard/ckeditor.js',CClientScript::POS_END);
	// Validadores de campos del Formulario
	echo '<script>viewPage = "'.Yii::app()->controller->action->id.'";</script>';
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/js/form-validators.js',CClientScript::POS_END);
 ?>

<script>
$( document ).ready(function() {
});		
</script>