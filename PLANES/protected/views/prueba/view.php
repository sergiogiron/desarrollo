<?php
/* @var $this PruebaController */
/* @var $model Prueba */

$this->breadcrumbs=array(
	'Pruebas'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List Prueba', 'url'=>array('index')),
	array('label'=>'Create Prueba', 'url'=>array('create')),
	array('label'=>'Update Prueba', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Prueba', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Prueba', 'url'=>array('admin')),
);
?>

<section class="content-header">
	<h1>
		Pruebas	<small>ver</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="/ttsoffice_repo"><i class="fa fa-dashboard"></i> Home</a></li>
		<li><a href="/ttsoffice_repo/prueba">Prueba</a></li>
		<li class="active">Ver</li>
	</ol>
</section><id>
<section class="content">
	<div class="box box-danger">
		<div class="box-header with-border">
			<?php $this->renderPartial('_form', array('model'=>$model)); ?>		</div>
	</div>
</section>


