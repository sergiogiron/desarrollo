<?php
/* @var $this UsersController */
/* @var $model Users */
/*
$this->breadcrumbs=array(
	'Users'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List Users', 'url'=>array('index')),
	array('label'=>'Create Users', 'url'=>array('create')),
	array('label'=>'Update Users', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Users', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Users', 'url'=>array('admin')),
);
*/
?>
<section class="content-header">
	<?php echo Yii::app()->user->Title(); ?>
	<?php echo Yii::app()->user->Breadcrumb(); ?>
</section>
<section class="content">
	<div class="box box-danger">
		<div class="box-header with-border">
			<?php $this->renderPartial('_form', array('model'=>$model, 'usuarios'=>$usuarios,'usuarios_selected'=>$usuarios_selected)); ?>
		</div>
	</div>
</section>
<!--
<h1>View Users #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'id_user',
		'username',
		'status',
		'name',
		'lastname',
		'doc_type',
		'doc_number',
		'passport_number',
		'passport_expire_date',
		'cuil',
		'birthdate',
		'nationality',
		'status_civil',
		'phone',
		'celular',
		'email',
		'address',
		'state',
		'postal_code',
		'studies',
		'several_studies',
		'start_date',
		'file',
		'img_type',
		'location',
		'sector',
		'job',
		'top_boss',
		'interno',
		'personal_phone',
		'mobile',
		'created_at',
	),
)); ?>
-->