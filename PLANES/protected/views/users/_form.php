<?php
/* @var $this UsersController */
/* @var $model Users */
/* @var $form CActiveForm */
?>

<link rel="stylesheet" href="<?=Yii::app()->request->baseUrl?>/css/users.css">

<div class="box-body">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'users-form',
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
	'enableAjaxValidation'=>false,
)); ?>

	<span class="right_f active-button">
		<?php echo $form->checkBox($model,'enabled',array('class'=>'lcs_check enabled')); ?>
	</span>
	<div class="col-md-6 col-data">
		<h2>Datos Personales</h2>

		<!-- <p class="note" style="float:left;">Los campos con <span class="required">*</span> son requeridos.</p> -->

		<div class="col-xxs-12 col-xs-6 form-group">
			<?php echo $form->labelEx($model, 'image', array('label' => CHtml::image(Yii::app()->request->baseUrl.'/img/usuario.png', 'Cambiar imagen',array('class'=>'fotocarnet')))); ?>
			<?php echo $form->fileField($model, 'image',array('class'=>'input-file')); ?>
			<?php echo $form->error($model, 'image'); ?>
		</div>
		<?php
			if(!$model->isNewRecord){ ?>
				<div class="col-xxs-12 col-xs-6 form-group user-name unique">
					<?php echo $form->labelEx($model,'username'); ?>
					<?php echo $form->textField($model,'username',array('size'=>20,'maxlength'=>20,'class'=>'form-control', 'disabled'=>'disabled')); ?>
					<?php echo $form->error($model,'username'); ?>
				</div>
			<?php }
		?>


		<div class="col-xxs-12 col-xs-6 form-group name required">
			<?php echo $form->labelEx($model,'name'); ?>
			<?php echo $form->textField($model,'name',array('size'=>62,'maxlength'=>62,'class'=>'form-control letras')); ?>
			<?php echo $form->error($model,'name'); ?>
		</div>
		<div class="col-xxs-12 col-xs-6 form-group required">
			<?php echo $form->labelEx($model,'lastname'); ?>
			<?php echo $form->textField($model,'lastname',array('size'=>62,'maxlength'=>62,'class'=>'form-control letras')); ?>
			<?php echo $form->error($model,'lastname'); ?>
		</div>

		<div class="col-xs-12 form-group required">
			<div class="col-xs-4">
				<?php echo $form->labelEx($model,'doc_type'); ?>
				<?php //echo $form->textField($model,'doc_type',array('size'=>20,'maxlength'=>20,'class'=>'form-control')); ?>
				<?php echo $form->dropDownList($model,'doc_type',array('CI'=>'CI','DNI'=>'DNI','LE'=>'LE'), array('options' => array('DNI'=>array('selected'=>true)), 'class'=>'form-control')); ?>
				<?php echo $form->error($model,'doc_type'); ?>
			</div>
			<div class="col-xs-8">
				<?php echo $form->labelEx($model,'doc_number'); ?>
				<?php echo $form->textField($model,'doc_number',array('class'=>'form-control')); ?>
				<?php echo $form->error($model,'doc_number'); ?>
			</div>
		</div>

		<div class="col-xxs-12 col-xs-6 form-group required">
			<?php echo $form->labelEx($model,'birthdate'); ?>
			<div class="input-group">
				<!-- <div class="input-group-addon pointer"><i class="fa fa-calendar"></i></div> -->
				<?php echo $form->textField($model,'birthdate',array('class'=>'form-control datepicker','data-inputmask'=>'\'alias\': \'dd/mm/yyyy\'','data-mask'=>'data-mask')); ?>
				<div class="input-group-addon pointer" data-toggle="tooltip" title="Abrir el calendario"><i class="fa fa-calendar"></i></div>
			</div>
			<?php echo $form->error($model,'birthdate'); ?>
		</div>

		<div class="col-xxs-12 col-xs-6 form-group required">
			<?php echo $form->labelEx($model,'nationality'); ?>
			<?php //echo $form->textField($model,'nationality',array('size'=>30,'maxlength'=>30,'class'=>'form-control')); ?>
			<?php echo $form->dropDownList($model,'nationality', array('Argentino'=>'Argentino','Colombiano'=>'Colombiano','Venezolano'=>'Venezolano'), array('options' => array('Argentino'=>array('selected'=>true)), 'class'=>'form-control')); ?>
			<?php echo $form->error($model,'nationality'); ?>
		</div>

		<div class="col-xxs-12 col-xs-6 form-group required">
			<?php echo $form->labelEx($model,'status_civil'); ?>
			<?php //echo $form->textField($model,'status_civil',array('size'=>30,'maxlength'=>30,'class'=>'form-control')); ?>
			<?php echo $form->dropDownList($model,'status_civil',array('Soltero'=>'Soltero','Concuvino'=>'Concuvino','Casado'=>'Casado','Divorciado'=>'Divorciado','Viudo'=>'Viudo'), array('data-placeholder'=>'Seleccionar...', 'prompt'=>'Seleccionar...', 'class'=>'form-control')); ?>
			<?php echo $form->error($model,'status_civil'); ?>
		</div>

		<div class="col-xxs-12 col-xs-6 form-group">
			<?php echo $form->labelEx($model,'cuil'); ?>
			<?php echo $form->textField($model,'cuil',array('class'=>'form-control')); ?>
			<?php echo $form->error($model,'cuil'); ?>
		</div>

		<div class="col-xs-12 form-group">
			<div class="col-xxs-5 col-xs-7">
				<?php echo $form->labelEx($model,'passport_number'); ?>
				<?php echo $form->textField($model,'passport_number',array('class'=>'form-control')); ?>
				<?php echo $form->error($model,'passport_number'); ?>
			</div>
			<div class="col-xxs-7 col-xs-5 form-group">
				<?php echo $form->labelEx($model,'passport_expire_date'); ?>
				<?php //echo $form->textField($model,'passport_expire_date',array('size'=>10,'maxlength'=>10,'class'=>'form-control datepicker')); ?>
				<div class="input-group">
					<!-- <div class="input-group-addon pointer"><i class="fa fa-calendar"></i></div> -->
					<?php echo $form->textField($model,'passport_expire_date',array('class'=>'form-control datepicker','data-inputmask'=>'\'alias\': \'dd/mm/yyyy\'','data-mask'=>'data-mask')); ?>
					<div class="input-group-addon pointer" data-toggle="tooltip" title="Abrir el calendario"><i class="fa fa-calendar"></i></div>
				</div>
				<?php echo $form->error($model,'passport_expire_date'); ?>
			</div>
		</div>

		<div class="col-xxs-12 col-xs-6 form-group">
			<?php echo $form->labelEx($model,'phone'); ?>
			<div class="input-group">
				<?php echo $form->textField($model,'phone',array('size'=>35,'maxlength'=>35,'class'=>'form-control phone')); ?>
				<div class="input-group-addon"><i class="fa fa-phone"></i></div>
			</div>
			<?php echo $form->error($model,'phone'); ?>
		</div>

		<div class="col-xxs-12 col-xs-6 form-group">
			<?php echo $form->labelEx($model,'celular'); ?>
			<div class="input-group">
				<?php echo $form->textField($model,'celular',array('size'=>35,'maxlength'=>35,'class'=>'form-control phone')); ?>
				<div class="input-group-addon"><i class="fa fa-phone"></i></div>
			</div>
			<?php echo $form->error($model,'celular'); ?>
		</div>

		<div class="col-xs-12 form-group required">
			<?php echo $form->labelEx($model,'email'); ?>
			<div class="input-group">
				<?php echo $form->EmailField($model,'email',array('size'=>80,'maxlength'=>80,'class'=>'form-control')); ?>
				<div class="input-group-addon"><i class="fa fa-envelope-o"></i></div>
			</div>
			<?php echo $form->error($model,'email'); ?>
		</div>

		<div class="col-xxs-12 col-xs-12 form-group">
			<?php echo $form->labelEx($model,'address'); ?>
			<?php echo $form->textField($model,'address',array('size'=>60,'maxlength'=>100,'class'=>'form-control')); ?>
			<?php echo $form->error($model,'address'); ?>
		</div>

		<div class="col-xxs-12 col-xs-6 form-group">
			<?php echo $form->labelEx($model,'state'); ?>
			<?php //echo $form->textField($model,'state',array('size'=>25,'maxlength'=>25,'class'=>'form-control')); ?>
			<?php echo $form->dropDownList($model,'state',array('Buenos Aires'=>'Buenos Aires','CABA'=>'Cdad.Aut.Bs.As. (CABA)','Chubut'=>'Chubut','Córdoba'=>'Córdoba'), array('options' => array('CABA'=>array('selected'=>true)), 'class'=>'form-control')); ?>
			<?php echo $form->error($model,'state'); ?>
		</div>

		<div class="col-xxs-12 col-xs-6 form-group">
			<?php echo $form->labelEx($model,'postal_code'); ?>
			<?php echo $form->textField($model,'postal_code',array('size'=>8,'maxlength'=>8,'class'=>'form-control')); ?>
			<?php echo $form->error($model,'postal_code'); ?>
		</div>

		<div class="col-xxs-12 col-xs-12 form-group">
			<?php echo $form->labelEx($model,'studies'); ?>
			<?php //echo $form->textField($model,'studies',array('size'=>25,'maxlength'=>25,'class'=>'form-control')); ?>
			<?php echo $form->dropDownList($model,'studies',array('Primario'=>'Primario','Secundario'=>'Secundario','Terciario'=>'Terciario','Universitario'=>'Universitario'), array('options' => array('Universitario'=>array('selected'=>true)), 'class'=>'form-control')); ?>
			<?php echo $form->error($model,'studies'); ?>
		</div>

		<div class="col-xxs-12 col-xs-12 form-group">
			<?php echo $form->labelEx($model,'several_studies'); ?>
			<?php //echo $form->textField($model,'several_studies',array('size'=>60,'maxlength'=>1024,'class'=>'form-control')); ?>
			<?php echo $form->textArea($model, 'several_studies', array('maxlength' => 1024,'class'=>'form-control', 'rows' => 10)); ?>
			<?php echo $form->error($model,'several_studies'); ?>
		</div>
	</div>
	<div class="col-md-6 col-data">
		<h2>Datos Laborales</h2>
		<!--
		<div class="col-xxs-12 col-xs-6 form-group">
			<?php echo $form->labelEx($model,'start_date'); ?>
			<?php echo $form->textField($model,'start_date',array('size'=>10,'maxlength'=>10,'class'=>'form-control')); ?>
			<?php echo $form->error($model,'start_date'); ?>
		</div>
		-->
		<div class="col-xxs-12 col-xs-6 form-group required">
			<?php echo $form->labelEx($model,'start_date'); ?>
			<div class="input-group">
				<?php echo $form->textField($model,'start_date',array('class'=>'form-control datepicker','data-inputmask'=>'\'alias\': \'dd/mm/yyyy\'','data-mask'=>'data-mask')); ?>
				<div class="input-group-addon pointer" data-toggle="tooltip" title="Abrir el calendario"><i class="fa fa-calendar"></i></div>
			</div>
			<?php echo $form->error($model,'start_date'); ?>
		</div>

		<div class="col-xxs-12 col-xs-6 form-group required">
			<?php echo $form->labelEx($model,'file'); ?>
			<?php echo $form->textField($model,'file',array('class'=>'form-control')); ?>
			<?php echo $form->error($model,'file'); ?>
		</div>
		<!--
		<div class="form-group">
			<?php echo $form->labelEx($model,'img_type'); ?>
			<?php echo $form->textField($model,'img_type',array('size'=>3,'maxlength'=>3,'class'=>'form-control')); ?>
			<?php echo $form->error($model,'img_type'); ?>
		</div>
		-->
		<div class="col-xxs-12 col-xs-6 form-group required">
			<?php echo $form->labelEx($model,'location'); ?>
			<?php //echo $form->textField($model,'location',array('size'=>30,'maxlength'=>30,'class'=>'form-control')); ?>
			<?php echo $form->dropDownList($model,'location',array('Córdoba'=>'Córdoba','Microcentro'=>'Microcentro','Parque Patricios'=>'Parque Patricios'), array('class'=>'form-control','data-placeholder'=>'Seleccionar...', 'prompt'=>'Seleccionar...')); ?>
			<?php echo $form->error($model,'location'); ?>
		</div>

		<div class="col-xxs-12 col-xs-6 form-group required">
			<?php echo $form->labelEx($model,'sector'); ?>
			<?php //echo $form->textField($model,'sector',array('size'=>50,'maxlength'=>50,'class'=>'form-control')); ?>
			<?php echo $form->dropDownList($model,'sector',array('Administración'=>'Administración','Desarrollo'=>'Desarrollo','Sistemas'=>'Sistemas','Ventas'=>'Ventas'), array('class'=>'form-control','data-placeholder'=>'Seleccionar...', 'prompt'=>'Seleccionar...')); ?>
			<?php echo $form->error($model,'sector'); ?>
		</div>

		<div class="col-xs-12 form-group required">
			<?php echo $form->labelEx($model,'job'); ?>
			<?php echo $form->textField($model,'job',array('size'=>60,'maxlength'=>100,'class'=>'form-control')); ?>
			<?php echo $form->error($model,'job'); ?>
		</div>

		<?php #var_dump($model->top_boss);die;
			$seleccionados=[];
			if(isset($usuarios_selected) && !empty($usuarios_selected) && count($usuarios_selected)>0 ) {
				$seleccionados[]=$usuarios_selected;
			}
			//var_dump($seleccionados);die;
		?>

		<div class="col-xs-12 form-group">
			<?php echo $form->labelEx($model,'top_boss'); ?>
			<?php #echo $form->textField($model,'top_boss',array('size'=>25,'maxlength'=>25,'class'=>'form-control')); ?>
			<?php echo CHtml::dropDownList('Users[top_boss]',$seleccionados, $usuarios, array('class'=>'form-control select2','data-placeholder'=>'Seleccionar...', 'prompt'=>'Seleccionar...', 'id'=>'top_boss','style'=>'width:100%')); ?>

			<?php echo $form->error($model,'top_boss'); ?>
		</div>

		<div class="col-xxs-12 col-xs-6 form-group">
			<?php echo $form->labelEx($model,'personal_phone'); ?>
			<div class="input-group">
				<?php echo $form->textField($model,'personal_phone',array('size'=>35,'maxlength'=>35,'class'=>'form-control phone')); ?>
				<div class="input-group-addon"><i class="fa fa-phone"></i></div>
			</div>
			<?php echo $form->error($model,'personal_phone'); ?>
		</div>

		<div class="col-xxs-12 col-xs-6 form-group">
			<?php echo $form->labelEx($model,'interno'); ?>
			<div class="input-group">
				<?php echo $form->textField($model,'interno',array('size'=>35,'maxlength'=>35,'class'=>'form-control phone')); ?>
				<div class="input-group-addon"><i class="fa fa-phone"></i></div>
			</div>
			<?php echo $form->error($model,'interno'); ?>
		</div>

		<div class="col-md-12">
			<label class="label_image">Imagen de banners</label>
			<div class="col-md-1">
				<div class="more_img action_img action_img" data-position="1" title="Agregar imagen" data-toggle="tooltip">+</div>
			</div>
			<div class="col-md-11 content-images">
				<ul class="slider1" id="slider_1">
					<?php
						foreach($model->imagenes as $imagenes){
							if($imagenes->position==1){
								echo "<li id='".$imagenes->filename."_slide'  title='Titulo:".$imagenes->title." Enlace:".$imagenes->link."' class='bslide_1 slide ui-state-default ui-sortable-handle'><i class='fa fa-edit edit_img_i' data-position=".$imagenes->position." data-id=".$imagenes->filename." onclick='edit_element($(this))'></i><i class='fa fa-times delete_img_i' data-id=".$imagenes->filename."   data-position=".$imagenes->position." onclick='delete_element($(this))'></i><img src='".Yii::app()->request->baseUrl."/uploads/tmp/".Yii::app()->user->id."/".$imagenes->filename."/".$imagenes->filename.".".$imagenes->ext."'></li>";
							}
						}
					?>
				</ul>
			</div>
		</div>

		<!--
		<div class="form-group">
			<?php echo $form->labelEx($model,'interno'); ?>
			<?php echo $form->textField($model,'interno',array('class'=>'form-control')); ?>
			<?php echo $form->error($model,'interno'); ?>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'mobile'); ?>
			<?php echo $form->textField($model,'mobile',array('class'=>'form-control')); ?>
			<?php echo $form->error($model,'mobile'); ?>
		</div>
		-->
	</div>


	<div class="box-footer">
		<?php
			$title = (Yii::app()->controller->action->id!='view') ? 'Volver sin guardar cambios' : 'Volver';
			echo '<a href="'.Yii::app()->request->baseUrl.'/'.strtolower(Yii::app()->controller->id).'" class="btn btn-secundary" data-toggle="tooltip" title="'.$title.'" >Cerrar</a>';
			if(Yii::app()->controller->action->id!='view'){
				echo CHtml::submitButton('Guardar',array('class'=>'btn btn-primary','data-toggle'=>'tooltip','title'=>'Guardar cambios'));
			}
		?>
	</div>

<?php $this->endWidget(); ?>
</div>
<script>
id_menu=<?=Yii::app()->user->id_menu()?>;
menu='<?=strtolower(Yii::app()->controller->id)?>';
eval("var cantidad_images_"+1+" = "+$('#data-d-content_1 :input').length);
eval("var cantidad_images_"+2+" = "+$('#data-d-content_2 :input').length);
eval("var limit_images_"+1+" = '2'");
eval("var limit_images_"+2+" = '2'");

$( document ).ready(function() {

$('.select2').select2().on('select2:unselect', function(e) {
		$('.select2-selection').trigger('click');
	});

});
</script>
<!-- form -->
<!-- Este codigo lo genera el CRUD de yii. Para el TTSofficeDay vamos a tener que ponerlo a mano. -->
<!-- Eliminen lo que no se usa. (Incluyendo estos comentarios) -->
<?php
	//logica uploades de imagenes
	Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/uploader_image.js',CClientScript::POS_END);
	//logica uploades de imagenes
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/js/dropzone.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerCssFile(Yii::app()->params['cdnUrl'].'/css/jquery.Jcrop.css');
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/js/jquery.Jcrop.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/js/jquery.bxslider.js',CClientScript::POS_END);

	Yii::app()->clientScript->registerCssFile(Yii::app()->params['cdnUrl'].'/css/jquery.bxslider.css');
	echo '<script>viewPage = "'.Yii::app()->controller->action->id.'";</script>';
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/js/form-validators.js',CClientScript::POS_END);
	// Activo/Inactivo
	Yii::app()->clientScript->registerCssFile(Yii::app()->params['cdnUrl'].'/plugins/lcswich/lc_switch-tts.css');
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/lcswich/lc_switch.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/lcswich/lc_switch-tts.js',CClientScript::POS_END);
	// Datepicker con la mascara
	Yii::app()->clientScript->registerCssFile(Yii::app()->params['cdnUrl'].'/plugins/jQueryUI/jquery-ui.min.css');
	Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/jquery-ui.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerCssFile(Yii::app()->params['cdnUrl'].'/plugins/datepicker/datepicker3.css');
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/datepicker/bootstrap-datepicker.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/input-mask/jquery.inputmask.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/input-mask/jquery.inputmask.date.extensions.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/datepicker/datepicker-tts.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/select2/select2.full.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerCssFile(Yii::app()->params['cdnUrl'].'/plugins/select2/select2.css');
 ?>
