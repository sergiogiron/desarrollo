<?php
/* @var $this UsersController */
/* @var $model Users */
/*
$this->breadcrumbs=array(
	'Users'=>array('index'),
	'Create',
);
$this->menu=array(
	array('label'=>'List Users', 'url'=>array('index')),
	array('label'=>'Manage Users', 'url'=>array('admin')),
);
*/
?>
<section class="content-header">
	<?php echo Yii::app()->user->Title(); ?>
	<?php echo Yii::app()->user->Breadcrumb(); ?>
</section>
<section class="content">
	<div class="box box-danger">
		<div class="box-header with-border">
			<?php $this->renderPartial('_form', array('model'=>$model,'usuarios'=>$usuarios,'usuarios_selected'=>$usuarios_selected)); ?>
		</div>
	</div>
</section>