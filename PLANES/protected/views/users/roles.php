<?php
/* @var $this UsersController */
/* @var $model Users */

$this->breadcrumbs=array(
	'Users'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Users', 'url'=>array('index')),
	array('label'=>'Manage Users', 'url'=>array('admin')),
);
?>
<section class="content-header">
	<h1>Asignar perfiles</h1>
	<ol class="breadcrumb">
		<li>
			<a href="<?=Yii::app()->request->baseUrl?>"><i class="fa fa-home"></i>Inicio</a>
		</li>
		<li>Personas</li>
		<li><a href="<?=Yii::app()->request->baseUrl?>/usuarios">Usuarios</a></li>
	</ol>
</section>
<section class="content">
	<div class="box box-danger">
		<div class="box-header with-border">
			<div class="box-body">
				<p class="note" style="float:left;">Los campos con <span class="required">*</span> son requeridos.</p>
				<form action="" method="post">
					<div class="form-group" style="clear: both;">
						<label for="Users_username" class="required">
						Username <span class="required">*</span></label>	
						<input size="20" maxlength="20" class="form-control" name="username" value="<?=$usuario->username?>" id="Users_username" type="text">
					</div>
					<div class="form-group">
						<label for="Profile_title" class="required">Perfiles <span class="required">*</span></label>
						<?php 
							$list=CHtml::listData(Profile::model()->findAll(),'id','title');
							$selected=array();
							foreach($roles as $r){
								$selected[$r->id_profile]=array('selected'=>true);
							}
							echo CHtml::dropDownList('id_profile',array(), $list, array('options' =>$selected ,'class'=>'form-control select2','data-placeholder'=>'Seleccione','prompt'=>'Seleccione','multiple'=>'multiple','style'=>'width:100%')); 
						?>
					</div>
					<div class="box-footer">
						<a href="<?=Yii::app()->request->baseUrl?>/usuarios" class="btn btn-secundary" data-toggle="tooltip" title="Volver" >Cerrar</a>
						<input class="btn btn-primary" type="submit" name="yt0" title="Guardar cambios" data-toggle="tooltip" value="Guardar">
					</div>
				</form>
			</div>
		</div>
	</div>
</section>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/plugins/select2/select2.full.js',CClientScript::POS_END); ?>
<?php Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl.'/js/plugins/select2/select2.css'); ?>
<script>
$(document).ready(function(){
	$('.select2').select2().on('select2:unselect', function(e) {
		$('.select2-selection').trigger('click');
	});
});	
</script>	