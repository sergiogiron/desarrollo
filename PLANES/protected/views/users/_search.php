<?php
/* @var $this UsersController */
/* @var $model Users */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>
<div class="col-md-12" style="clear: both">
	<div class="row">
		<div class="col-md-3">
			<div class="row row_space">
				<?php echo $form->label($model,'username'); ?>
				<?php echo $form->textField($model,'username',array('size'=>20,'maxlength'=>20,'class'=>'form-control')); ?>
			</div>
		</div>

		<div class="col-md-3">
			<div class="row row_space">
				<?php echo $form->label($model,'name'); ?>
				<?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>60,'class'=>'form-control')); ?>
			</div>
		</div>

		<div class="col-md-3">
			<div class="row row_space">
				<?php echo $form->label($model,'lastname'); ?>
				<?php echo $form->textField($model,'lastname',array('size'=>60,'maxlength'=>60,'class'=>'form-control')); ?>
			</div>
		</div>

		<div class="col-md-3">
			<div class="row row_space">
				<?php echo $form->label($model,'doc_type'); ?>
				<?php echo $form->textField($model,'doc_type',array('size'=>20,'maxlength'=>20,'class'=>'form-control')); ?>
			</div>
		</div>

		<div class="col-md-3">
			<div class="row row_space">
				<?php echo $form->label($model,'doc_number'); ?>
				<?php echo $form->textField($model,'doc_number',array('class'=>'form-control')); ?>
			</div>
		</div>

		<div class="col-md-3">
			<div class="row row_space">
				<?php echo $form->label($model,'passport_number'); ?>
				<?php echo $form->textField($model,'passport_number',array('class'=>'form-control')); ?>
			</div>
		</div>

		<div class="col-md-3">
			<div class="row row_space">
				<?php echo $form->label($model,'passport_expire_date'); ?>
				<?php echo $form->textField($model,'passport_expire_date',array('size'=>10,'maxlength'=>10,'class'=>'form-control')); ?>
			</div>
		</div>

		<div class="col-md-3">
			<div class="row row_space">
				<?php echo $form->label($model,'cuil'); ?>
				<?php echo $form->textField($model,'cuil',array('class'=>'form-control')); ?>
			</div>
		</div>

		<div class="col-md-3">
			<div class="row row_space">
				<?php echo $form->label($model,'birthdate'); ?>
				<?php echo $form->textField($model,'birthdate',array('class'=>'form-control')); ?>
			</div>
		</div>

		<div class="col-md-3">
			<div class="row row_space">
				<?php echo $form->label($model,'nationality'); ?>
				<?php echo $form->textField($model,'nationality',array('size'=>30,'maxlength'=>30,'class'=>'form-control')); ?>
			</div>
		</div>

		<div class="col-md-3">
			<div class="row row_space">
				<?php echo $form->label($model,'status_civil'); ?>
				<?php echo $form->textField($model,'status_civil',array('size'=>30,'maxlength'=>30,'class'=>'form-control')); ?>
			</div>
		</div>

		<div class="col-md-3">
			<div class="row row_space">
				<?php echo $form->label($model,'phone'); ?>
				<?php echo $form->textField($model,'phone',array('class'=>'form-control')); ?>
			</div>
		</div>

		<div class="col-md-3">
			<div class="row row_space">
				<?php echo $form->label($model,'celular'); ?>
				<?php echo $form->textField($model,'celular',array('class'=>'form-control')); ?>
			</div>
		</div>

		<div class="col-md-3">
			<div class="row row_space">
				<?php echo $form->label($model,'email'); ?>
				<?php echo $form->textField($model,'email',array('size'=>60,'maxlength'=>75,'class'=>'form-control')); ?>
			</div>
		</div>

		<div class="col-md-3">
			<div class="row row_space">
				<?php echo $form->label($model,'address'); ?>
				<?php echo $form->textField($model,'address',array('size'=>60,'maxlength'=>100,'class'=>'form-control')); ?>
			</div>
		</div>

		<div class="col-md-3">
			<div class="row row_space">
				<?php echo $form->label($model,'state'); ?>
				<?php echo $form->textField($model,'state',array('size'=>25,'maxlength'=>25,'class'=>'form-control')); ?>
			</div>
		</div>

		<div class="col-md-3">
			<div class="row row_space">
				<?php echo $form->label($model,'postal_code'); ?>
				<?php echo $form->textField($model,'postal_code',array('size'=>8,'maxlength'=>8,'class'=>'form-control')); ?>
			</div>
		</div>

		<div class="col-md-3">
			<div class="row row_space">
				<?php echo $form->label($model,'studies'); ?>
				<?php echo $form->textField($model,'studies',array('size'=>25,'maxlength'=>25,'class'=>'form-control')); ?>
			</div>
		</div>

		<div class="col-md-3">
			<div class="row row_space">
				<?php echo $form->label($model,'several_studies'); ?>
				<?php echo $form->textField($model,'several_studies',array('size'=>60,'maxlength'=>1024,'class'=>'form-control')); ?>
			</div>
		</div>
		
		<div class="col-md-3">
			<div class="row row_space">
				<?php echo $form->label($model,'start_date'); ?>
				<?php echo $form->textField($model,'start_date',array('size'=>10,'maxlength'=>10,'class'=>'form-control')); ?>
			</div>
		</div>

		<div class="col-md-3">
			<div class="row row_space">
				<?php echo $form->label($model,'location'); ?>
				<?php echo $form->textField($model,'location',array('size'=>30,'maxlength'=>30,'class'=>'form-control')); ?>
			</div>
		</div>

		<div class="col-md-3">
			<div class="row row_space">
				<?php echo $form->label($model,'sector'); ?>
				<?php echo $form->textField($model,'sector',array('size'=>50,'maxlength'=>50,'class'=>'form-control')); ?>
			</div>
		</div>

		<div class="col-md-3">
			<div class="row row_space">
				<?php echo $form->label($model,'job'); ?>
				<?php echo $form->textField($model,'job',array('size'=>60,'maxlength'=>100,'class'=>'form-control')); ?>
			</div>
		</div>

		<div class="col-md-3">
			<div class="row row_space">
				<?php echo $form->label($model,'top_boss'); ?>
				<?php echo $form->textField($model,'top_boss',array('size'=>25,'maxlength'=>25,'class'=>'form-control')); ?>
			</div>
		</div>

		<div class="col-md-3">
			<div class="row row_space">
				<?php echo $form->label($model,'interno'); ?>
				<?php echo $form->textField($model,'interno',array('class'=>'form-control')); ?>
			</div>
		</div>

		<div class="col-md-3">
			<div class="row row_space">
				<?php echo $form->label($model,'personal_phone'); ?>
				<?php echo $form->textField($model,'personal_phone',array('class'=>'form-control')); ?>
			</div>
		</div>
	
		<div class="col-md-3">
			<div class="row row_space">
				<?php echo $form->label($model,'mobile'); ?>
				<?php echo $form->textField($model,'mobile',array('class'=>'form-control')); ?>
			</div>
		</div>
	</div>
	<div class="row buttons">
		<?php echo CHtml::submitButton('Buscar',array('class'=>'btn btn-default b_customize')); ?>
	</div>
<br />
<?php $this->endWidget(); ?>
</div>
</div><!-- search-form -->