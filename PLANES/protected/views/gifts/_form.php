<?php
/* @var $this GiftsController */
/* @var $model Gifts */
/* @var $form CActiveForm */
?>
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'gifts-form',
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
	'enableAjaxValidation'=>true,
	'clientOptions' => array(
		'validateOnSubmit'=>true,
		'beforeValidate'=>'js:function(form,data,hasError){
			if($("#data-d-content_1 :input").length==0){
				$("#error_img_empty").html("Debe seleccionar al menos una imagen");
				return false;
			}else{
				$("#error_img_empty").html("");
				return true;
			}
		}'	       
    ),
)); ?>
<div class="box-body">
	<span class="right_f">
<?php echo $form->checkBox($model,'enabled',array('class'=>'lcs_check enabled')); ?>
	
	</span>
<div class="row">
<div class="col-md-12 col-data">
<div class="row">
<div class="form-group col-md-6 required unique">
	<?php 
	if(!$model->isNewRecord){ $display='disabled'; }else{ $display='enabled'; }
	echo $form->labelEx($model,'boyfriends'); ?>
			<?php echo $form->textField($model,'boyfriends',array('size'=>60,'maxlength'=>100,'class'=>'form-control',$display=>$display)); ?>
							<?php echo $form->error($model,'boyfriends'); ?>
</div>
<div class="form-group col-md-6 required">
	<?php echo $form->labelEx($model,'destinations'); ?>
			<?php echo $form->textField($model,'destinations',array('size'=>60,'maxlength'=>100,'class'=>'form-control')); ?>
							<?php echo $form->error($model,'destinations'); ?>
</div>
<div class="col-xxs-12 col-xs-6 form-group required">
	<?php echo $form->labelEx($model,'date'); ?>
	<div class="input-group">
		<!-- <div class="input-group-addon pointer"><i class="fa fa-calendar"></i></div> -->
		<?php echo $form->textField($model,'date',array('class'=>'form-control','data-inputmask'=>'\'alias\': \'dd/mm/yyyy\'','data-mask'=>'data-mask')); ?>
		<div class="input-group-addon pointer"><i class="fa fa-calendar expire_date_to_calendar" data-toggle="tooltip" title="" data-original-title="Abrir el calendario"></i></div>
	</div>
	<?php echo $form->error($model,'date'); ?>
</div>
<div class="form-group col-md-6 required">
	<?php echo $form->labelEx($model,'id_user'); ?>
	<?php echo $form->dropDownList($model, 'id_user',CHtml::listData(Users::model()->findAll(), 'id',  function($model) { return $model->name . ' ' . $model->lastname; }),array('empty' => 'Seleccione','class'=>'form-control'));?>
							<?php echo $form->error($model,'id_user'); ?>
</div>

<div class="form-group col-md-6 required">
	<?php echo $form->labelEx($model,'dk'); ?>
			<?php echo $form->textField($model,'dk',array('size'=>60,'maxlength'=>64,'class'=>'form-control numeros')); ?>
							<?php echo $form->error($model,'dk'); ?>
</div>
<div class="form-group col-md-6 required">
	<?php echo $form->labelEx($model,'id_type_currency'); ?>
	<?php echo $form->dropDownList($model, 'id_type_currency',CHtml::listData(Typecurrency::model()->findAll(), 'id',  'title'),array('empty' => 'Seleccione','class'=>'form-control'));?>
	<?php echo $form->error($model,'id_type_currency'); ?>
</div>
<div id="data-d-content_1">
	<?php
		foreach($model->imagenes as $imagenes){
			if($imagenes->position==1){
				$data=array('name'=>$imagenes->filename,'position'=>$imagenes->position,'ext'=>$imagenes->ext,'title'=>$imagenes->title,'link'=>$imagenes->link,'order'=>$imagenes->order,'coords'=>json_decode($imagenes->coords));
				echo "<input type='hidden' id=".$imagenes->filename." name='image[]' class='element_data' value='".json_encode($data)."'>";
			}
		}
	?>	
</div>	
</div>
<div class="col-md-12" style="margin-bottom:20px;">
<a href="#" class="btn btn-primary" id="add_gift_light" style="display: block;float: left;">Añadir regalo</a>
<input type='hidden' id='gifts_json' name='gifts_json' value='<?php if(count($gift_list_selected)>0){ echo json_encode($gift_list_selected); }else{ echo '[]'; }?>'/>
<br />
<br />
<div class="row" style="display:none;" id="content_form">
	<div class="col-md-6">
		<label for="Gifts_date" class="required">Descripción <span class="required">*</span></label>
		<input type="text" class="form-control" id="description"/>
	</div>
	<div class="col-md-2">
		<label for="Gifts_date" class="required">Monto <span class="required">*</span></label>
		<input type="text" class="form-control numeros" id="price" />
	</div>
	<input type="hidden" id="id" />
	<input type="hidden" id="order" />	
	<div class="col-md-4" style="padding-top: 21px;">
		<input type="button" class="btn btn-primary" id="add_gift" value="Agregar"/>
		<input type="button" value="Cancelar" id="cancelar_edit" onclick="cancelar_gift();" class="btn btn-primary" style="margin-top:0px;margin-left:10px;">
	</div>
	<br /><br /><br />
</div>
</div>
	<div class="col-md-12">
		<table id="example" class="table table-bordered table-striped  table-list">
			<thead>
				<tr>
					<th>Orden</th>
					<th>Descripción</th>
					<th>Monto</th>
					<th></th>
				</tr>
			</thead>
			<tbody id="result">
				<?php
					foreach($model->giftslist as $giftslist){
						echo '<tr id="'.$giftslist->id.'" data-price="'.$giftslist->price.'" data-description="'.$giftslist->description.'" role="row" class="odd"><td class="sorting_1"><span  data-toggle="tooltip" data-original-title="Arrastrar para ordenar">'.$giftslist->order.'</span></td><td>'.$giftslist->description.'</td><td>'.$giftslist->price.'</td><td><a href="#" data-id="'.$giftslist->id.'" data-type="" class="edit_gift" data-toggle="tooltip" data-original-title="Editar"><i class="fa fa-pencil-square-o icon_cbutton"></i></a><a href="#" delete-data-id="'.$giftslist->id.'" class="delete_gift" data-toggle="tooltip" data-original-title="Borrar"><i class="fa fa-trash-o icon_cbutton"></i></a></td></tr>';
					}
				?>
			</tbody>
		</table>
	</div>
</div>
</div>
<div class="col-md-12">
	<label class="label_image">Imagen*</label>
	<div class="error_color_lf" id="error_img_empty"></div>
	<div class="col-md-1">
		<div class="more_img action_img action_img" data-position="1" title="Agregar imagen" data-toggle="tooltip">+</div>
	</div>
	<div class="col-md-11 content-images">
		<ul class="slider1" id="slider_1">
			<?php
				foreach($model->imagenes as $imagenes){
					if($imagenes->position==1){
						echo "<li id='".$imagenes->filename."_slide'  title='Titulo:".$imagenes->title." Enlace:".$imagenes->link."' class='bslide_1 slide ui-state-default ui-sortable-handle'><i class='fa fa-edit edit_img_i' data-position=".$imagenes->position." data-id=".$imagenes->filename." onclick='edit_element($(this))'></i><i class='fa fa-times delete_img_i' data-id=".$imagenes->filename."   data-position=".$imagenes->position." onclick='delete_element($(this))'></i><img src='".Yii::app()->request->baseUrl."/uploads/tmp/".Yii::app()->user->id."/".$imagenes->filename."/".$imagenes->filename.".".$imagenes->ext."'></li>";
					}
				}
			?>	
		</ul>			
	</div>	
</div>
<?php
	Yii::app()->clientScript->registerCssFile('https://cdn.datatables.net/rowreorder/1.2.0/css/rowReorder.dataTables.min.css');
	Yii::app()->clientScript->registerScriptFile('https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js',CClientScript::POS_END);	
	Yii::app()->clientScript->registerScriptFile('https://cdn.datatables.net/rowreorder/1.2.0/js/dataTables.rowReorder.min.js',CClientScript::POS_END);	
	Yii::app()->clientScript->registerCssFile(Yii::app()->params['cdnUrl'].'/plugins/datepicker/datepicker3.css');
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/datepicker/bootstrap-datepicker.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/input-mask/jquery.inputmask.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/input-mask/jquery.inputmask.date.extensions.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/js/dropzone.js',CClientScript::POS_END); 
	Yii::app()->clientScript->registerCssFile(Yii::app()->params['cdnUrl'].'/plugins/lcswich/lc_switch-tts.css'); 
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/lcswich/lc_switch.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/lcswich/lc_switch-tts.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerCssFile(Yii::app()->params['cdnUrl'].'/css/dropzone.css'); 	
	Yii::app()->clientScript->registerCssFile(Yii::app()->params['cdnUrl'].'/css/dropzone_one.css');
	echo '<script>viewPage = "'.Yii::app()->controller->action->id.'";</script>';
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/js/form-validators.js',CClientScript::POS_END);
	//logica uploades de imagenes
	Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/uploader_image.js',CClientScript::POS_END); 
	//logica uploades de imagenes
	Yii::app()->clientScript->registerCssFile(Yii::app()->params['cdnUrl'].'/css/jquery.Jcrop.css');
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/js/jquery.Jcrop.js',CClientScript::POS_END); 
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/js/jquery.bxslider.js',CClientScript::POS_END); 
	Yii::app()->clientScript->registerCssFile(Yii::app()->params['cdnUrl'].'/plugins/jQueryUI/jquery-ui.min.css');
	Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/jquery-ui.js',CClientScript::POS_END);		
	Yii::app()->clientScript->registerCssFile(Yii::app()->params['cdnUrl'].'/css/jquery.bxslider.css');
 ?> 
<script>
id_menu=<?=Yii::app()->user->id_menu()?>;
menu='<?=strtolower(Yii::app()->controller->id)?>';
eval("var cantidad_images_"+1+" = "+$('#data-d-content_1 :input').length);
eval("var limit_images_"+1+" = '1'");
function cancelar_gift(){
	//$('#cancelar_edit').css("display","none");
	$( ".ligth-box-modal" ).css('display','none');	
	$('#add_gift').val('Agregar');
	$('#description').val('');
	$('#id').val('');	
	$('#price').val('');	
}
function refresh(){
	$('#example').dataTable().fnDestroy();
	$('#result').html('');
	var gifts_json=JSON.parse($('#gifts_json').val());
	order=0;
	gifts_json.sort(function (a, b) {
		return (a.order - b.order)
	})	
	$.each(gifts_json, function( key, value ){
		order++;
		$('#result').append('<tr id="'+value.id+'" data-price="'+value.price+'" data-description="'+value.description+'"><td><span  data-toggle="tooltip" data-original-title="Arrastrar para ordenar">'+order+'</span></td><td>'+value.description+'</td><td>'+formatNumber.new(value.price)+'</td><td><a href="#" data-id="'+value.id+'" data-type="" class="edit_gift" data-toggle="tooltip" data-original-title="Editar"><i class="fa fa-pencil-square-o icon_cbutton"></i></a><a href="#" delete-data-id="'+value.id+'" class="delete_gift" data-toggle="tooltip" data-original-title="Borrar"><i class="fa fa-trash-o icon_cbutton"></i></a></td></tr>');			
	});	
	 var table =$('#example').DataTable( {
		 rowReorder: true,
		"bPaginate": false,
		"bLengthChange": false,
		"bFilter": true,
		"bInfo": false,
		"bAutoWidth": false,
		"language": {
			"sSearch": "Buscar:",
		}
    } );
	delete_gift();
	edit_gift();
}
function delete_gift(){
	$( ".delete_gift" ).on( "click", function(event) {
		event.preventDefault();
		var id=$(this).attr('delete-data-id');
		var gifts_json=JSON.parse($('#gifts_json').val());
		var gifts_json_new=[];
		$.each( gifts_json, function( key, value ) {
			if(value.id!=id){
				gifts_json_new.push(value);
			}
		});
		$('#gifts_json').val(JSON.stringify(gifts_json_new));
		$(this).parent().parent().remove();
		refresh();
	});			
}
function add_gift(){
	$( "#add_gift" ).on( "click", function(event) {
		event.preventDefault();
		$('.close').css('visibility','hidden');
		if($( "#description" ).val()!='' && $( "#price" ).val()!=''){
			if($( "#id" ).val()!=''){				
				$('[delete-data-id='+$( "#id" ).val()+']').trigger('click');
				var order=$("#order").val();
			}
			if($( "#id" ).val()==''){
				if($('.edit_gift').length==0){
					var order=1;
				}else{
					var order=$("#result tr").length+1;
				}
			}			
			finish_object=new Object;
			finish_object.id=new Date().getTime();
			finish_object.order=order;
			finish_object.description=$('#description').val();
			finish_object.price=$('#price').val();	

			var gifts_json=JSON.parse($('#gifts_json').val());
			gifts_json.push(finish_object);		
			$('#gifts_json').val(JSON.stringify(gifts_json));							
			$('#description').val('');
			$('#price').val('');
			if($( "#id" ).val()!=''){
				$('#cancelar_edit').css("display","none");
				$('#add_gift').val('Agregar');
				$('#description').val('');
				$('#id').val('');	
				$('#price').val('');					
			}
			refresh();
			$( ".ligth-box-modal" ).css('display','none');
		}else{
			alert('Complete todos los campos');
		}
	});
}
function edit_gift(){
	$( ".edit_gift" ).on( "click", function(event) {
		event.preventDefault();
		$('.close').css('visibility','hidden');
		$('#cancelar_edit').css("display","table-cell");
		$('#add_gift').val('Editar');
		var id=$(this).attr('data-id');
		var gift_edit;
		var gift_content=JSON.parse($('#gifts_json').val());			
		$.each( gift_content, function( key, value ) {
			if(value.id==id){
				gift_edit=value;
			}
		});
		$('#description').val(gift_edit.description);
		$('#price').val(gift_edit.price);	
		$('#id').val(gift_edit.id);	
		$('#order').val(gift_edit.order);	
		$( ".ligth-box-modal" ).css('display','block');	
	});		
}
$( document ).ready(function() {
	$('#Gifts_date').datepicker({
			Date:'<?=date("d/m/Y")?>',
			language:"es",
			format: 'dd/mm/yyyy',
			startDate:'<?=date("d/m/Y")?>',
			autoclose:true
	});			
	$('.expire_date_to_calendar').on('click', function(e) {
		event.preventDefault();
		$('#Gifts_date').focus();
    });		
	$('.modal-title').html('Agregar regalo');
	$('.close').css('visibility','hidden');
	$('.modal-body').html($('#content_form').html());	
	$( "#add_gift_light" ).on( "click", function(event) {
		event.preventDefault();	
		$('.close').css('visibility','hidden');
		$( ".ligth-box-modal" ).css('display','block');	
	});			
	delete_gift();
	add_gift();
	edit_gift();
	 var table =$('#example').DataTable( {
		 rowReorder: true,
		"bPaginate": false,
		"bLengthChange": false,
		"bFilter": true,
		"bInfo": false,
		"bAutoWidth": false,
		"language": {
			"sSearch": "Buscar:",
		}
    } );
	table.on( 'row-reorder', function ( e, diff, edit ) {
		var order=1;
		gifts_json=[];
		$('#result tr').each(function(index, element ) {
			finish_object=new Object;
			finish_object.id=$(this).attr('id');
			finish_object.order=order;
			finish_object.description=$(this).attr('data-description');
			finish_object.price=$(this).attr('data-price');
			gifts_json.push(finish_object);	
			order++;
		});	
		$('#gifts_json').val(JSON.stringify(gifts_json));
    } );
	<?php if(Yii::app()->controller->action->id=='view'){ echo "$('.delete_gift').remove();$('.edit_gift').remove();$('#add_gift_light').remove();$('.edit_img_i').remove();$('.delete_img_i').remove();$('form').find('input, textarea, button, checkbox, select').attr('disabled','disabled');"; } ?>});		
</script>
<div class="box-footer">
		<?php
		$title = (Yii::app()->controller->action->id!='view') ? 'Volver sin guardar cambios' : 'Volver';
		echo '<a href="'.Yii::app()->request->baseUrl.'/'.strtolower(Yii::app()->controller->id).'" class="btn btn-secundary" data-toggle="tooltip" title="'.$title.'" >Cerrar</a>';
		if(Yii::app()->controller->action->id!='view'){
			echo CHtml::submitButton('Guardar',array('class'=>'btn btn-primary','data-toggle'=>'tooltip','title'=>'Guardar cambios'));
		}
		?></div>
<?php $this->endWidget(); ?>
</div><!-- form -->
