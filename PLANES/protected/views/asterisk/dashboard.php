<section class="content-header" style="margin-bottom: 10px;">
  <h1>
	Dashboard
  </h1>
  <form action="<?php echo Yii::app()->request->baseUrl; ?>/asterisk/dashboard" method="post">
  <div  style="float: right;margin-top: -25px;">	
		<label style="margin-right:10px;float:left;">Fecha desde : </label>
		<div style="float:left;width:150px;"> 
		<div class="input-group">
			<input class="form-control datepicker" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="data-mask" id="desde" name="desde"  type="text" value="<?=$desde?>" maxlength="20">	
			<div class="input-group-addon pointer" data-toggle="tooltip" title="" data-original-title="Abrir el calendario"><i class="fa fa-calendar"></i></div>
		</div>
	</div>	
	
 <label style="margin-right:10px;float:left;">Fecha hasta : </label>
 <div style="float:left;width:150px;"> 
	<div class="input-group">
		<input class="form-control datepicker" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="data-mask" id="hasta" name="hasta"  type="text" value="<?=$hasta?>" maxlength="20">	
		<div class="input-group-addon pointer" data-toggle="tooltip" title="" data-original-title="Abrir el calendario"><i class="fa fa-calendar"></i></div>
	</div>	
	</div>
  <input type="submit" id="filtrar" class="btn btn-primary" style="margin-left:10px;" value="Buscar">
  </div>
  </form>
</section>
          <div class="nav-tabs">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#tab_1" data-toggle="tab">Resumen</a></li>
              <li><a href="#tab_2" data-toggle="tab">Detalle Telefonos</a></li> 
              <li><a href="#tab_3" data-toggle="tab">Llamados por agente</a></li>                            
            </ul>
            <div class="tab-content">
              <div class="tab-pane active" id="tab_1">
<section class="content">
	<div class="row">
        <div class="col-md-4 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-purple"><i class="fa fa-phone"></i></span>

            <div class="info-box-content">
              <span class="info-box-text-dash">On line</span>
              <span class="info-box-number-dash">0</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-4 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-green"><i class="fa fa-check"></i></span>

            <div class="info-box-content">
              <span class="info-box-text-dash">Total llamadas atendidas</span>
              <span class="info-box-number-dash atendidasOffice"><?=Utils::formatPrice($data['atendidas'])?></span>
              <div class="containerLlamadas">
              	<i class="iconFont icon-vuelos mRightIcons" data-toggle="tooltip" data-placement="top" title="<?=Utils::formatPrice($data['aereos_atendidos'])?> llamadas por vuelos"></i>
              	<i class="iconFont icon-hoteles mRightIcons" data-toggle="tooltip" data-placement="top" title="<?=Utils::formatPrice($data['hoteles_atendidos'])?> llamadas por hoteles"></i>
              	<i class="iconFont icon-paquetes  mRightIcons" data-toggle="tooltip" data-placement="top" title="<?=Utils::formatPrice($data['paquetes_atendidos'])?> llamadas por paquetes"></i>
              	<i class="iconFont icon-cruceros mRightIcons" data-toggle="tooltip" data-placement="top" title="<?=Utils::formatPrice($data['cruceros_atendidos'])?> llamadas por cruceros"></i>
              	<i class="iconFont icon-otros " data-toggle="tooltip" data-placement="top" title="<?=Utils::formatPrice($data['otros_atendidos'])?> llamadas por otros"></i>
              </div>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->

        <!-- fix for small devices only -->
        <div class="clearfix visible-sm-block"></div>

        <div class="col-md-4 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-red"><i class="fa fa-thumbs-o-down"></i></span>

            <div class="info-box-content">
              <span class="info-box-text-dash">Total llamadas abandonadas</span>
              <span class="info-box-number-dash abandonadasOffice"><?=Utils::formatPrice($data['sin_atender'])?></span>

               <div class="container-Llamadas">
              	<i class="iconFont icon-vuelos  mRightIcons" data-toggle="tooltip" data-placement="top" title="<?=Utils::formatPrice($data['aereos_sin_atender'])?> llamadas por vuelos"></i>
              	<i class="iconFont icon-hoteles mRightIcons" data-toggle="tooltip" data-placement="top" title="<?=Utils::formatPrice($data['hoteles_sin_atender'])?> llamadas por hoteles"></i>
              	<i class="iconFont icon-paquetes  mRightIcons" data-toggle="tooltip" data-placement="top" title="<?=Utils::formatPrice($data['paquetes_sin_atender'])?> llamadas por paquetes"></i>
              	<i class="iconFont icon-cruceros mRightIcons" data-toggle="tooltip" data-placement="top" title="<?=Utils::formatPrice($data['cruceros_sin_atender'])?> llamadas por cruceros"></i>
              	<i class="iconFont icon-otros" data-toggle="tooltip" data-placement="top" title="<?=Utils::formatPrice($data['otros_sin_atender'])?> llamadas por otros"></i>
              </div>

            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-4 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-blue"><i class="fa fa-arrow-right"></i></span>

            <div class="info-box-content">
              <span class="info-box-text-dash">Total llamadas al desborde</span>
              <span class="info-box-number-dash desbordeOffice">0</span>

               <div class="container--Llamadas">
              	<i class="iconFont icon-vuelos  mRightIcons" data-toggle="tooltip" data-placement="top" title="0 llamadas por vuelos"></i>
              	<i class="iconFont icon-hoteles mRightIcons" data-toggle="tooltip" data-placement="top" title="0 llamadas por hoteles"></i>
              	<i class="iconFont icon-paquetes  mRightIcons" data-toggle="tooltip" data-placement="top" title="0 llamadas por paquetes"></i>
              	<i class="iconFont icon-cruceros mRightIcons" data-toggle="tooltip" data-placement="top" title="0 llamadas por cruceros"></i>
              	<i class="iconFont icon-otros" data-toggle="tooltip" data-placement="top" title="0 llamadas por otros"></i>
              </div>


            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <div class="col-md-4 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-yellow"><i class="fa fa-heart"></i></span>

            <div class="info-box-content">
              <span class="info-box-text-dash">Total llamadas</span>
              <span class="info-box-number-dash totalOffice"><?=Utils::formatPrice($data['total'])?></span>

               <div class="container---Llamadas">
              	<i class="iconFont icon-vuelos  mRightIcons" data-toggle="tooltip" data-placement="top" title="<?=Utils::formatPrice($data['aereos_total'])?> llamadas por vuelos"></i>
              	<i class="iconFont icon-hoteles mRightIcons" data-toggle="tooltip" data-placement="top" title="<?=Utils::formatPrice($data['hoteles_total'])?> llamadas por hoteles"></i>
              	<i class="iconFont icon-paquetes  mRightIcons" data-toggle="tooltip" data-placement="top" title="<?=Utils::formatPrice($data['paquetes_total'])?> llamadas por paquetes"></i>
              	<i class="iconFont icon-cruceros mRightIcons" data-toggle="tooltip" data-placement="top" title="<?=Utils::formatPrice($data['cruceros_total'])?> llamadas por cruceros"></i>
              	<i class="iconFont icon-otros" data-toggle="tooltip" data-placement="top" title="<?=Utils::formatPrice($data['otros_total'])?> llamadas por otros"></i>
              </div>

            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>		
        <!-- /.col -->
      </div>
</section>	
</div>
<div class="tab-pane" id="tab_2">
<section class="content">
	<div class="row">
		<div class="col-xs-12">
			<div class="form-group">
				<table id="datetable"  class="table table-bordered table-hover table-list">
					<thead>
					<tr>
						<th data-container="body" data-toggle="tooltip" data-placement="top" title="Llamada" class="centerItemTable"><i class="fa fa-phone llamadoOffice"></i></th>
						<!--<th class="padCenter centerItemTable centerItemTable2" data-container="body" data-toggle="tooltip" data-placement="top" title="Agente"><i class="iconFont icon-pasajeros pasajeroOffice"></i></th>-->			
						
						<th class="padCenter centerItemTable centerItemTable2" data-container="body" data-toggle="tooltip" data-placement="top" title="Llamadas de vuelos atendidas"><i class="iconFont icon-vuelos greenOffice " id="vuelosIc"></i></th>							
						
						<th class="padCenter centerItemTable centerItemTable2" data-container="body" data-toggle="tooltip" data-placement="top" title="Llamadas de vuelos no atendidas"><i class="iconFont icon-vuelos redOffice" id="vuelosIc"></i></th>							
						
						<th class="padCenter centerItemTable centerItemTable2" data-container="body" data-toggle="tooltip" data-placement="top" title="Llamadas de hoteles atendidas"><i class="iconFont icon-hoteles greenOffice" id="hotelIc"></i></th>							
						
						<th class="padCenter centerItemTable centerItemTable2" data-container="body" data-toggle="tooltip" data-placement="top" title="Llamadas de hoteles no atendidas"><i class="iconFont icon-hoteles redOffice" id="hotelIc"></i></th>							
						
						<th class="padCenter centerItemTable centerItemTable2" data-container="body" data-toggle="tooltip" data-placement="top" title="Llamadas de paquetes atendidas"><i class="iconFont icon-paquetes greenOffice"  id="paqIc"></i></th>							
						
						<th class="padCenter centerItemTable centerItemTable2" data-container="body" data-toggle="tooltip" data-placement="top" title="Llamadas de paquetes no atendidas"><i class="iconFont icon-paquetes redOffice"  id="paqIc"></i></th>							
						
						<th class="padCenter centerItemTable centerItemTable2" data-container="body" data-toggle="tooltip" data-placement="top" title="Llamadas de cruceros atendidas"><i class="iconFont icon-cruceros greenOffice" id="boat"></i></th>							
						
						<th class="padCenter centerItemTable centerItemTable2" data-container="body" data-toggle="tooltip" data-placement="top" title="Llamadas de cruceros no atendidas"><i class="iconFont icon-cruceros redOffice" id="boat"></i></th>							
						
						<th class="padCenter centerItemTable centerItemTable2" data-container="body" data-toggle="tooltip" data-placement="top" title="Otras llamadas no atendidas"><i class="iconFont icon-otros greenOffice"  id="otrosIc"></i></th>							
						
						<th class="padCenter centerItemTable centerItemTable2" data-container="body" data-toggle="tooltip" data-placement="top" title="Otras llamadas atendidas"><i class="iconFont icon-otros redOffice"  id="otrosIc"></i></th>							
						
						
						<th data-container="body" data-toggle="tooltip" data-placement="top" title="Total de llamadas" class="centerItemTable">Total</th>
					</tr>
					</thead>
					<tbody>
					<?php
					foreach($result as $r){
						echo '<tr>
						<td class="centerItem">'.$r['did'].'</td>';
						/*<td class="centerItem">'.$r['agentes'].'</td>	*/							
						echo '<td class="centerItem">'.$r['aereos_atendidos'].'</td>
						<td class="centerItem">'.$r['aereos_sin_atender'].'</td>
						<td class="centerItem">'.$r['hoteles_atendidos'].'</td>
						<td class="centerItem">'.$r['hoteles_sin_atender'].'</td>
						<td class="centerItem">'.$r['paquetes_atendidos'].'</td>
						<td class="centerItem">'.$r['paquetes_sin_atender'].'</td>
						<td class="centerItem">'.$r['cruceros_atendidos'].'</td>
						<td class="centerItem">'.$r['cruceros_sin_atender'].'</td>
						<td class="centerItem">'.$r['otros_atendidos'].'</td>
						<td class="centerItem">'.$r['otros_sin_atender'].'</td>							
						<td class="centerItem">'.$r['total'].'</td>
						</tr>';
					}
					?>
					</tbody>
				</table>			
			</div>
		</div>
	</div>
</section>
</div>
<div class="tab-pane" id="tab_3">
<section class="content">
	<div class="row">
		<div class="col-xs-12">
					<div class="form-group">
					<table id="datetable3"  class="table table-bordered table-hover table-list">
					<thead>
					<tr>							
						<th data-container="body" data-toggle="tooltip" data-placement="top" title="Ordenar por" class="centerItemTable"><i class="fa fa-phone llamadoOffice"></i></th>
						<th data-container="body" data-toggle="tooltip" data-placement="top" title="Primer inicio de sesión" class="centerItemTable"><i class="fa fa-sign-in llamadoOffice greenOffice"></i></th>
						<th data-container="body" data-toggle="tooltip" data-placement="top" title="Último cierre de sesión" class="centerItemTable"><i class="fa fa-sign-out llamadoOffice redOffice" id="logOut"></i></th>
						<th data-container="body" data-toggle="tooltip" data-placement="top" title="Tiempo logueado" class="centerItemTable"><i class="iconFont icon-pasajeros pasajeroOffice-2 greenOffice" id="pasajeroOffice"></i></th>
						<th data-container="body" data-toggle="tooltip" data-placement="top" title="Libre para recibir llamados" class="centerItemTable"><i class="fa fa-check llamadoOffice greenOffice" id="checkOffice"></i></th>
						<th data-container="body" data-toggle="tooltip" data-placement="top" title="Tiempo hablado" class="centerItemTable"><i class="fa fa-clock-o llamadoOffice timeOffice greenOffice" aria-hidden="true"></i></th>
						<th data-container="body" data-toggle="tooltip" data-placement="top" title="Tiempo pausado" class="centerItemTable"><i class="fa fa-clock-o llamadoOffice timeOffice orangeOffice"></i></th>
						<th data-container="body" data-toggle="tooltip" data-placement="top" title="Llamadas atendidas" class="centerItemTable"><i class="fa fa-phone llamadoOffice greenOffice"></i></th>
						<th data-container="body" data-toggle="tooltip" data-placement="top" title="Llamadas no atendidas" class="centerItemTable"><i class="fa fa-phone llamadoOffice redOffice"></i></th>
						<th data-container="body" data-toggle="tooltip" data-placement="top" title="Llamadas derivadas" class="centerItemTable"><i class="fa fa-phone llamadoOffice orangeOffice"></i></th>
					</tr>
					</thead>
					<tbody>
					<?php
					foreach($agentes as $ag){
						echo '<tr>
						<td class="centerItem"><a href="'.Yii::app()->request->baseUrl.'/asterisk/llamadasagente/fecha/2017-04-05/id_agente/'.$ag['id_agente'].'" data-name="'.$ag['agente'].'" class="llamados_x_agente">'.$ag['agente'].'</a></td>
						<td class="centerItem">'.Utils::datetime_spa($ag['primer_login']).'</td>
						<td class="centerItem">'.Utils::datetime_spa($ag['ultimo_logout']).'</td>
						<td class="centerItem">'.$ag['tiempo_login'].'</td>
						<td class="centerItem">'.$ag['tiempo_listo'].'</td>
						<td class="centerItem">'.$ag['tiempo_hablando'].'</td>
						<td class="centerItem">'.$ag['tiempo_pausado'].'</td>								
						<td class="centerItem">'.$ag['cantidad_llamadas_atendidas'].'</td>								
						<td class="centerItem">'.$ag['cantidad_llamadas_no_atendidas'].'</td>								
						<td class="centerItem">'.$ag['cantidad_llamadas_derivadas'].'</td>								
						</tr>';
					}
					?>
					</tbody>
				</table>		
					</div>
				</div>
	</div>
</section>
</div>
<?php
	Yii::app()->clientScript->registerCssFile(Yii::app()->params['cdnUrl'].'/plugins/jQueryUI/jquery-ui.min.css');
	Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/jquery-ui.js',CClientScript::POS_END);	
	Yii::app()->clientScript->registerCssFile(Yii::app()->params['cdnUrl'].'/plugins/datepicker/datepicker3.css');
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/datepicker/bootstrap-datepicker.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/input-mask/jquery.inputmask.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/input-mask/jquery.inputmask.date.extensions.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/datepicker/datepicker-tts.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/jquery.dataTables.min.js',CClientScript::POS_END);
?>
<script>
$( document ).ready(function() {
	$( ".llamados_x_agente" ).on( "click", function() {
		event.preventDefault();
		$('.ligth-box-modal').css('display','block');
		$('.modal-body').html('');
		$('.modal-title').html('');
		var data_name=$(this).attr('data-name');
		var request = $.ajax({
			url: $(this).attr('href'),
		}).done(function(msg) {
			$('.modal-body').html(msg);
			$('.modal-title').html(data_name);
			$('#llamados_x_agente_t').DataTable( {
				 "bPaginate":true,		 
				 "iDisplayLength": 10,
				 "order": [[ 1, "desc" ]],	
				 "pageLength": 6,
				"bLengthChange": false,
				"bFilter": true,
				"bInfo": false,
				"bAutoWidth": false,
				"language": {
					"sSearch": "Buscar:",
					"paginate": {
								first:    '<i class="fa fa-forward fa-flip-horizontal" data-toggle="tooltip" title="Primera página"></i>',
								previous: '<i class="fa fa-caret-right fa-flip-horizontal" data-toggle="tooltip" title="Anterior"></i>',
								next:     '<i class="fa fa-caret-right" data-toggle="tooltip" title="Siguiente"></i>',
								last:     '<i class="fa fa-forward" data-toggle="tooltip" title="Última página"></i>'
					},
				}
			} );			
		});	
	});	
	$('#datetable').DataTable( {
		 "bPaginate":true,		 
		 "iDisplayLength": 10,
		 "order": [[ 1, "desc" ]],	
		 "pageLength": 6,
		"bLengthChange": false,
		"bFilter": true,
		"bInfo": false,
		"bAutoWidth": false,
		"language": {
			"sSearch": "Buscar:",
			"paginate": {
						first:    '<i class="fa fa-forward fa-flip-horizontal" data-toggle="tooltip" title="Primera página"></i>',
						previous: '<i class="fa fa-caret-right fa-flip-horizontal" data-toggle="tooltip" title="Anterior"></i>',
						next:     '<i class="fa fa-caret-right" data-toggle="tooltip" title="Siguiente"></i>',
						last:     '<i class="fa fa-forward" data-toggle="tooltip" title="Última página"></i>'
			},
		}
    } );
	$('#datetable3').DataTable( {
		 "bPaginate":true,		 
		 "iDisplayLength": 10,
		 "order": [[ 1, "desc" ]],	
		 "pageLength": 6,
		"bLengthChange": false,
		"bFilter": true,
		"bInfo": false,
		"bAutoWidth": false,
		"language": {
			"sSearch": "Buscar:",
			"paginate": {
						first:    '<i class="fa fa-forward fa-flip-horizontal" data-toggle="tooltip" title="Primera página"></i>',
						previous: '<i class="fa fa-caret-right fa-flip-horizontal" data-toggle="tooltip" title="Anterior"></i>',
						next:     '<i class="fa fa-caret-right" data-toggle="tooltip" title="Siguiente"></i>',
						last:     '<i class="fa fa-forward" data-toggle="tooltip" title="Última página"></i>'
			},
		}
    } );	
	$("#filtrar").click(function() {
		if(($('#desde').val()!='' && $('#hasta').val()=='') || ($('#hasta').val()=='' && $('#hasta').val()!='')){
			alert('Si selecciona la fecha desde debe seleccionar la fecha hasta');
		}else{
			location.href=homeurl+'/asterisk/dashboard/sede/'+$('#sede').val()+'/desde/'+$('#desde').val()+'/hasta/'+$('#hasta').val();
		}		
	});
});
</script>	