<section class="content"  style="min-height: 210px!important;padding-bottom: 0px!important;">
	<div class="row">
        <div class="col-md-4 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-purple"><i class="fa fa-phone"></i></span>

            <div class="info-box-content">
              <span class="info-box-text-dash">On line</span>
              <span class="info-box-number-dash">0</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-4 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-green"><i class="fa fa-check"></i></span>

            <div class="info-box-content">
              <span class="info-box-text-dash">Total llamadas atendidas</span>
              <span class="info-box-number-dash atendidasOffice"><?=Utils::formatPrice($data['atendidas'])?></span>
              <div class="containerLlamadas">
              	<i class="iconFont icon-vuelos mRightIcons" data-toggle="tooltip" data-placement="top" title="<?=Utils::formatPrice($data['aereos_atendidos'])?> llamadas por vuelos"></i>
              	<i class="iconFont icon-hoteles mRightIcons" data-toggle="tooltip" data-placement="top" title="<?=Utils::formatPrice($data['hoteles_atendidos'])?> llamadas por hoteles"></i>
              	<i class="iconFont icon-paquetes  mRightIcons" data-toggle="tooltip" data-placement="top" title="<?=Utils::formatPrice($data['paquetes_atendidos'])?> llamadas por paquetes"></i>
              	<i class="iconFont icon-cruceros mRightIcons" data-toggle="tooltip" data-placement="top" title="<?=Utils::formatPrice($data['cruceros_atendidos'])?> llamadas por cruceros"></i>
              	<i class="iconFont icon-otros " data-toggle="tooltip" data-placement="top" title="<?=Utils::formatPrice($data['otros_atendidos'])?> llamadas por otros"></i>
              </div>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->

        <!-- fix for small devices only -->
        <div class="clearfix visible-sm-block"></div>

        <div class="col-md-4 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-red"><i class="fa fa-thumbs-o-down"></i></span>

            <div class="info-box-content">
              <span class="info-box-text-dash">Total llamadas abandonadas</span>
              <span class="info-box-number-dash abandonadasOffice"><?=Utils::formatPrice($data['sin_atender'])?></span>

               <div class="container-Llamadas">
              	<i class="iconFont icon-vuelos  mRightIcons" data-toggle="tooltip" data-placement="top" title="<?=Utils::formatPrice($data['aereos_sin_atender'])?> llamadas por vuelos"></i>
              	<i class="iconFont icon-hoteles mRightIcons" data-toggle="tooltip" data-placement="top" title="<?=Utils::formatPrice($data['hoteles_sin_atender'])?> llamadas por hoteles"></i>
              	<i class="iconFont icon-paquetes  mRightIcons" data-toggle="tooltip" data-placement="top" title="<?=Utils::formatPrice($data['paquetes_sin_atender'])?> llamadas por paquetes"></i>
              	<i class="iconFont icon-cruceros mRightIcons" data-toggle="tooltip" data-placement="top" title="<?=Utils::formatPrice($data['cruceros_sin_atender'])?> llamadas por cruceros"></i>
              	<i class="iconFont icon-otros" data-toggle="tooltip" data-placement="top" title="<?=Utils::formatPrice($data['otros_sin_atender'])?> llamadas por otros"></i>
              </div>

            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-4 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-blue"><i class="fa fa-arrow-right"></i></span>

            <div class="info-box-content">
              <span class="info-box-text-dash">Total llamadas al desborde</span>
              <span class="info-box-number-dash desbordeOffice">0</span>

               <div class="container--Llamadas">
              	<i class="iconFont icon-vuelos  mRightIcons" data-toggle="tooltip" data-placement="top" title="0 llamadas por vuelos"></i>
              	<i class="iconFont icon-hoteles mRightIcons" data-toggle="tooltip" data-placement="top" title="0 llamadas por hoteles"></i>
              	<i class="iconFont icon-paquetes  mRightIcons" data-toggle="tooltip" data-placement="top" title="0 llamadas por paquetes"></i>
              	<i class="iconFont icon-cruceros mRightIcons" data-toggle="tooltip" data-placement="top" title="0 llamadas por cruceros"></i>
              	<i class="iconFont icon-otros" data-toggle="tooltip" data-placement="top" title="0 llamadas por otros"></i>
              </div>


            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <div class="col-md-4 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-yellow"><i class="fa fa-heart"></i></span>

            <div class="info-box-content">
              <span class="info-box-text-dash">Total llamadas</span>
              <span class="info-box-number-dash totalOffice"><?=Utils::formatPrice($data['total'])?></span>

               <div class="container---Llamadas">
              	<i class="iconFont icon-vuelos  mRightIcons" data-toggle="tooltip" data-placement="top" title="<?=Utils::formatPrice($data['aereos_total'])?> llamadas por vuelos"></i>
              	<i class="iconFont icon-hoteles mRightIcons" data-toggle="tooltip" data-placement="top" title="<?=Utils::formatPrice($data['hoteles_total'])?> llamadas por hoteles"></i>
              	<i class="iconFont icon-paquetes  mRightIcons" data-toggle="tooltip" data-placement="top" title="<?=Utils::formatPrice($data['paquetes_total'])?> llamadas por paquetes"></i>
              	<i class="iconFont icon-cruceros mRightIcons" data-toggle="tooltip" data-placement="top" title="<?=Utils::formatPrice($data['cruceros_total'])?> llamadas por cruceros"></i>
              	<i class="iconFont icon-otros" data-toggle="tooltip" data-placement="top" title="<?=Utils::formatPrice($data['otros_total'])?> llamadas por otros"></i>
              </div>

            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>	
		<a href="<?=Yii::app()->request->baseUrl?>/asterisk/dashboard" target="_blank">Expandir</a>
		<!-- /.col -->
      </div>
</section>	
<?php
	Yii::app()->clientScript->registerCssFile(Yii::app()->params['cdnUrl'].'/plugins/jQueryUI/jquery-ui.min.css');
	Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/jquery-ui.js',CClientScript::POS_END);	
	Yii::app()->clientScript->registerCssFile(Yii::app()->params['cdnUrl'].'/plugins/datepicker/datepicker3.css');
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/datepicker/bootstrap-datepicker.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/input-mask/jquery.inputmask.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/input-mask/jquery.inputmask.date.extensions.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/datepicker/datepicker-tts.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/jquery.dataTables.min.js',CClientScript::POS_END);
?>
<script>
$(document).ready(function(){
	newHeight = $('html').outerHeight();
	 var height = {
		 "caja": {
			 "height" : newHeight
			 }
	 };

	var scrolltop = {
	 "scrolltop": {
	   "0" : 0
	   }
	};
	parent.postMessage(height, '*');
   });
</script>
<script>
$( document ).ready(function() {
	$('#datetable').DataTable( {
		"bPaginate":true,	
		"sDom": 'tp',
		"pagingType": "full_numbers",
		"order": [[ 1, "desc" ]],	 
		"iDisplayLength": 10,
		"pageLength": 6,
		"bLengthChange": false,
		"bFilter": true,
		"bInfo": false,
		"bAutoWidth": false,
		"language": {
			"sSearch": "Buscar:",
			"paginate": {
					first:    '<i class="fa fa-forward fa-flip-horizontal" data-toggle="tooltip" title="Primera página"></i>',
					previous: '<i class="fa fa-caret-right fa-flip-horizontal" data-toggle="tooltip" title="Anterior"></i>',
					next:     '<i class="fa fa-caret-right" data-toggle="tooltip" title="Siguiente"></i>',
					last:     '<i class="fa fa-forward" data-toggle="tooltip" title="Última página"></i>'
			},
		}
    } );
	$('#datetable2').DataTable( {
		"bPaginate":true,	
		"sDom": 'tp',
		"pagingType": "full_numbers",	 
		"iDisplayLength": 10,
		"order": [[ 1, "desc" ]],	
		"pageLength": 6,
		"bLengthChange": false,
		"bFilter": true,
		"bInfo": false,
		"bAutoWidth": false,
		"language": {
			"sSearch": "Buscar:",
			"paginate": {
						first:    '<i class="fa fa-forward fa-flip-horizontal" data-toggle="tooltip" title="Primera página"></i>',
						previous: '<i class="fa fa-caret-right fa-flip-horizontal" data-toggle="tooltip" title="Anterior"></i>',
						next:     '<i class="fa fa-caret-right" data-toggle="tooltip" title="Siguiente"></i>',
						last:     '<i class="fa fa-forward" data-toggle="tooltip" title="Última página"></i>'
			},
		}
    } );	
	$('#datetable3').DataTable( {
		 "bPaginate":true,		 
		 "iDisplayLength": 10,
		 "order": [[ 1, "desc" ]],	
		 "pageLength": 6,
		"bLengthChange": false,
		"bFilter": true,
		"bInfo": false,
		"bAutoWidth": false,
		"language": {
			"sSearch": "Buscar:",
			"paginate": {
						first:    '<i class="fa fa-forward fa-flip-horizontal" data-toggle="tooltip" title="Primera página"></i>',
						previous: '<i class="fa fa-caret-right fa-flip-horizontal" data-toggle="tooltip" title="Anterior"></i>',
						next:     '<i class="fa fa-caret-right" data-toggle="tooltip" title="Siguiente"></i>',
						last:     '<i class="fa fa-forward" data-toggle="tooltip" title="Última página"></i>'
			},
		}
    } );	
	$("#filtrar").click(function() {
		if(($('#desde').val()!='' && $('#hasta').val()=='') || ($('#hasta').val()=='' && $('#hasta').val()!='')){
			alert('Si selecciona la fecha desde debe seleccionar la fecha hasta');
		}else{
			location.href=homeurl+'/asterisk/dashboard/sede/'+$('#sede').val()+'/desde/'+$('#desde').val()+'/hasta/'+$('#hasta').val();
		}		
	});		
});
</script>	