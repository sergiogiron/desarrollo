<div class="form-group">
	<table id="llamados_x_agente_t"  class="table table-bordered table-hover table-list">
		<thead>
		<tr>							
			<th data-container="body" data-toggle="tooltip" data-placement="top" title="Llamada" class="centerItemTable"><i class="fa fa-phone llamadoOffice"></i></th>
			<th data-container="body" data-toggle="tooltip" data-placement="top" title="Comienzo de la llamada" class="centerItemTable"><i class="fa fa-phone llamadoOffice greenOffice"></i></th></th>
			<th data-container="body" data-toggle="tooltip" data-placement="top" title="Finalización de la llamada" class="centerItemTable"><i class="fa fa-phone llamadoOffice redOffice"></i></th>
		</tr>
		</thead>
		<tbody>
		<?php
		foreach($user as $u){
			echo '<tr>
			<td class="centerItem">'.$u['src'].'</td>
			<td class="centerItem">'.Utils::normalize_date($u['answered']).'</td>
			<td class="centerItem">'.Utils::normalize_date($u['hangup']).'</td>
			</tr>';
		}
		?>
		</tbody>
	</table>
</div>
<?php
	Yii::app()->clientScript->registerCssFile(Yii::app()->params['cdnUrl'].'/plugins/jQueryUI/jquery-ui.min.css');
	Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/jquery-ui.js',CClientScript::POS_END);	
	Yii::app()->clientScript->registerCssFile(Yii::app()->params['cdnUrl'].'/plugins/datepicker/datepicker3.css');
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/datepicker/bootstrap-datepicker.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/input-mask/jquery.inputmask.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/input-mask/jquery.inputmask.date.extensions.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/datepicker/datepicker-tts.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/jquery.dataTables.min.js',CClientScript::POS_END);
?>