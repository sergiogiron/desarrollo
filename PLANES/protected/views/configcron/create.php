<?php
/* @var $this ConfigcronController */
/* @var $model Configcron */
?>
<section class="content-header">
  <h1>
	Configuración del cron
  </h1>
  <ol class="breadcrumb">
	<li><a href="<?php echo Yii::app()->request->baseUrl; ?>"><i class="fa fa-home"></i> Inicio</a></li>
	<li>Configuraciones Web</li>
 </ol>
</section>
<section class="content">
	<div class="box box-danger">
		<div class="box-header with-border">
			<?php $this->renderPartial('_form', array('model'=>$model)); ?>
		</div>
	</div>
</section>