<?php
/* @var $this ConfigcronController */
/* @var $model Configcron */

$this->breadcrumbs=array(
	'Configcrons'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Configcron', 'url'=>array('index')),
	array('label'=>'Manage Configcron', 'url'=>array('admin')),
);
?>
<section class="content-header">
  <h1>
	Configuración del cron
	<small>modificar</small>
  </h1>
  <ol class="breadcrumb">
	<li><a href="<?php echo Yii::app()->request->baseUrl; ?>"><i class="fa fa-dashboard"></i> Home</a></li>
	<li><a href="<?php echo Yii::app()->request->baseUrl; ?>/configcron">Configuración del cron</a></li>
	<li class="active"><a href="">Modificar</a></li>
  </ol>
</section>
<section class="content">
	<div class="box box-danger">
		<div class="box-header with-border">
			<?php $this->renderPartial('_form', array('model'=>$model)); ?>
		</div>
	</div>
</section>