<?php
/* @var $this ConfigcronController */
/* @var $model Configcron */

$this->breadcrumbs=array(
	'Configcrons'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Cron', 'url'=>array('index')),
	array('label'=>'Crear Cron', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "search();deleteBtn();"); 
?>

<section class="content-header">
	<h1>
		Configuración del cron
	</h1>
	<ol class="breadcrumb">
		<li><a href="<?php echo Yii::app()->request->baseUrl; ?>"><i class="fa fa-home"></i> Home</a></li>
		<li><a href="<?php echo Yii::app()->request->baseUrl; ?>/configcron">Configuración del cron</a></li>
	</ol>
</section>
<section class="content">
	<div class="box box-danger">
		<div class="box-header with-border">	
			<?php
			if(Yii::app()->user->checkAccess('create')){
				echo '<a href="'.Yii::app()->request->baseUrl.'/configcron/crear"class="btn btn-default b_customize"><i class="fa fa-plus i_customize"></i> Agregar</a>';
			}
			?>
			<div class="col-md-3 search_bar">
				<div class="row">
					<div class="input-group input-group-sm">
						<input type="text" class="form-control" id="criterio">
						<span class="input-group-btn">
						  <button type="button" id="buscar" class="btn btn-default btn-flat">
						  	<i class="fa fa-search"></i>
						  </button>
						</span>
					</div>
				</div>
			</div>			
			<?php echo CHtml::link('Búsqueda avanzada','#',array('class'=>'search-button')); ?>
			<div class="search-form" style="display:none">
			<?php $this->renderPartial('_search',array(
				'model'=>$model,
			)); ?>
			</div><!-- search-form -->
			<?php $this->widget('zii.widgets.grid.CGridView', array(
				'id'=>'configcron-grid',

				'pager' => array(
                    'cssFile'=>false,
                    'header'=> '',
                    'firstPageLabel' => '<i class="fa fa-forward fa-flip-horizontal"></i>',
			        'prevPageLabel'  => '<i class="fa fa-caret-right fa-flip-horizontal"></i>',
			        'nextPageLabel'  => '<i class="fa fa-caret-right"></i>',
			        'lastPageLabel'  => '<i class="fa fa-forward"></i>',
			        //'afterAjaxUpdate'=> 'function(){enableSwitch();}',
                ),
				'afterAjaxUpdate'=> 'function(){enableSwitch();}',
				'summaryText' => '', 
				'itemsCssClass' => 'table table-bordered table-striped table-list',				
				'dataProvider'=>$model->search(),
				'columns'=>array(
					array(
							'name' => 'enabled',
							'type' => 'raw',
							'value' => 'Utils::activeSwitch($data)',
							'htmlOptions'=>array('class'=>'activeSwitch'),

						),
					'cron',
					'timevar',
					'lastrun',
					array(

						'class'=>'CButtonColumn',
						'template'=>'{update}{erase}',	
						'buttons'=>array(						
							'update' => array(
								'label'=>'<i class="fa fa-edit icon_cbutton"></i>',
								'visible'=>'Yii::app()->user->checkAccess(\'update\')',
								'url'=>'Yii::app()->createUrl(\'configcron/modificar/\'. $data->id)',
								'options'=>array('title'=>'Editar'),
								'imageUrl'=>false,
							),					
							'erase' => array(
								'label'=>'<i class="fa fa-trash-o icon_cbutton"></i>',
								'visible'=>'Yii::app()->user->checkAccess(\'delete\')',
								'url'=>'Yii::app()->createUrl(strtolower(Yii::app()->controller->id).\'/delete/id/\'. $data->id)',
								'options' => array(
									'class' => 'delete_action',
									'title'=>'Borrar',
									'imageUrl'=>false,
								)	
							),
							
						)
					)
				),
			)); ?>
		</div>
	</div>
 </section>
<script>
$(document).ready(function(){
	$( "#buscar" ).click(function(){
		$('#configcron-grid').yiiGridView('update', {
			data: { criterio : $('#criterio').val() }
		});
		return false;
	});	
	$("#criterio").keypress(function(e){
       if(e.which == 13) {
          $("#buscar").trigger('click');
       }
    });	
});	
</script>
<script>
<?php
	// Activo/Inactivo
	Yii::app()->clientScript->registerCssFile(Yii::app()->params['cdnUrl'].'/plugins/lcswich/lc_switch-tts.css'); 
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/lcswich/lc_switch.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/lcswich/lc_switch-tts.js',CClientScript::POS_END);
/*
	// Datepicker con la mascara
	Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl.'/../cdn/plugins/datepicker/datepicker3.css');
	Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/../cdn/plugins/datepicker/bootstrap-datepicker.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/../cdn/plugins/input-mask/jquery.inputmask.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/../cdn/plugins/input-mask/jquery.inputmask.date.extensions.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/../cdn/plugins/datepicker/datepicker-tts.js',CClientScript::POS_END);
	// Timepicker con la mascara
	Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/../cdn/plugins/timepicker/bootstrap-timepicker.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/../cdn/plugins/input-mask/jquery.inputmask.bundle.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl.'/../cdn/plugins/timepicker/bootstrap-timepicker.css');
	Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/../cdn/plugins/timepicker/timepicker-tts.js',CClientScript::POS_END);
	// Editor de texto enriquesido
	Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/../cdn/plugins/ckeditor-standard/ckeditor.js',CClientScript::POS_END);
	*/
 ?>
</script>