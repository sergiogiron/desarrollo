<?php
/* @var $this ConfigcronController */
/* @var $model Configcron */
/* @var $form CActiveForm */
?>

<div class="box-body">
	<div class="col-md-12 col-data">
		<?php $form=$this->beginWidget('CActiveForm', array(
			'id'=>'configcron-form',
			'htmlOptions' => array('enctype' => 'multipart/form-data'),
			'enableAjaxValidation'=>false,
		)); ?>

			<span class="right_f active-button">
				<?php echo $form->checkBox($model,'enabled',array('class'=>'lcs_check enabled')); ?>
			</span>
			<br>
			<div class="col-xs-6 form-group required">
				<?php echo $form->labelEx($model,'cron'); ?>
				<?php
			 if ($model->isNewRecord)
				echo $form->textField($model,'cron',array('size'=>62,'maxlength'=>62,'class'=>'form-control letras'));
			else
				echo $form->textField($model,'cron',array('size'=>60,'maxlength'=>60,'class'=>'form-control letras'));?>
				<?php echo $form->error($model,'cron'); ?>
			</div>

			<div class="col-xs-6 form-group required">
				<?php echo $form->labelEx($model,'timevar'); ?>
				<?php echo $form->textField($model,'timevar',array('size'=>62,'maxlength'=>62,'class'=>'form-control letras')); ?>
				<?php echo $form->error($model,'timevar'); ?>
			</div>
		
			<div class="col-xs-6 form-group required">
				<?php echo $form->labelEx($model,'lastrun'); ?>
				<?php echo $form->textField($model,'lastrun',array('size'=>62,'maxlength'=>62,'class'=>'form-control datepicker letras')); ?>
				<?php echo $form->error($model,'lastrun'); ?>
			</div>
		
		    <?php
			if(Yii::app()->controller->action->id!='view'){
			?>
			<div class="box-footer">
				<?php echo CHtml::button('Cerrar', array('onclick' => 'js:document.location.href="'.Yii::app()->request->baseUrl.'/configcron"','class'=>'btn btn-secundary')); ?>
				<?php echo CHtml::submitButton($model->isNewRecord ? 'Crear' : 'Guardar',array('class'=>'btn btn-primary')); ?>
			</div>
			<?php
			}
			?>
		<?php $this->endWidget(); ?>
	</div>
</div><!-- form -->
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/plugins/datepicker/bootstrap-datepicker.js',CClientScript::POS_END); ?>
<script>
$( document ).ready(function() {
	<?php
		if(Yii::app()->controller->action->id=='view'){ echo "$('form').find('input, textarea, button, checkbox, select').attr('disabled','disabled');"; }
	?>	
    $('.datepicker').datepicker({
      autoclose: true, 
	  format: 'yyyy-mm-dd',
	  firstDay: 1
    });
});	
</script>	
<?php
	// Activo/Inactivo
	Yii::app()->clientScript->registerCssFile(Yii::app()->params['cdnUrl'].'/plugins/lcswich/lc_switch-tts.css'); 
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/lcswich/lc_switch.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/lcswich/lc_switch-tts.js',CClientScript::POS_END);

	// Datepicker con la mascara
	Yii::app()->clientScript->registerCssFile(Yii::app()->params['cdnUrl'].'/plugins/datepicker/datepicker3.css');
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/datepicker/bootstrap-datepicker.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/input-mask/jquery.inputmask.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/input-mask/jquery.inputmask.date.extensions.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/datepicker/datepicker-tts.js',CClientScript::POS_END);
	// Timepicker con la mascara
	/*
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/timepicker/bootstrap-timepicker.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/input-mask/jquery.inputmask.bundle.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerCssFile(Yii::app()->params['cdnUrl'].'/plugins/timepicker/bootstrap-timepicker.css');
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/timepicker/timepicker-tts.js',CClientScript::POS_END);
	// Editor de texto enriquesido
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/ckeditor-standard/ckeditor.js',CClientScript::POS_END);
	*/
 ?>