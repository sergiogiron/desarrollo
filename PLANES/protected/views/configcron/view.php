<?php
/* @var $this ConfigcronController */
/* @var $model Configcron */

$this->breadcrumbs=array(
	'Configcrons'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Configcron', 'url'=>array('index')),
	array('label'=>'Create Configcron', 'url'=>array('create')),
	array('label'=>'Update Configcron', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Configcron', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Configcron', 'url'=>array('admin')),
);
?>

<h1>View Configcron #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'cron',
		'timevar',
		'lastrun',
		'created_at',
		'id_user',
	),
)); ?>
