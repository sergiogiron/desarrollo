<?php
/* @var $this ConfigcronController */
/* @var $model Configcron */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>
<div class="col-md-12" style="clear: both">
	<div class="row">
		<div class="col-md-3">
				<div class="row row_space">
			<?php echo $form->label($model,'cron'); ?>
			<?php echo $form->textField($model,'cron',array('class'=>'form-control')); ?>
		</div>
		</div>

		<div class="col-md-3">
				<div class="row row_space">
			<?php echo $form->label($model,'timevar'); ?>
			<?php echo $form->textField($model,'timevar',array('class'=>'form-control')); ?>
			</div>
		</div>

		<div class="col-md-3">
			<div class="row row_space">
		<?php echo $form->label($model,'lastrun'); ?>
		<?php echo $form->textField($model,'lastrun',array('class'=>'form-control')); ?>
	</div>
	</div>
</div>
	<div class="row buttons">
		<?php echo CHtml::submitButton('Buscar',array('class'=>'btn btn-default b_customize')); ?>
	</div>
<br />
<?php $this->endWidget(); ?>

<!-- search-form -->
</div><!-- search-form -->
</div><!-- search-form -->