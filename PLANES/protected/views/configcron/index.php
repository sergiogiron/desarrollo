<?php
/* @var $this ConfigcronController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Configcrons',
);

$this->menu=array(
	array('label'=>'Create Configcron', 'url'=>array('create')),
	array('label'=>'Manage Configcron', 'url'=>array('admin')),
);
?>

<h1>Configcrons</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
