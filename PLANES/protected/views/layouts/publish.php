<span style="display:none;" id="title-modal-p"><?=$title?></span>
<?=$breadcrumb?>
<form method="post" id="publication_form" style="clear: both;">
	<div class="form-group required">
	<label>Fecha de publicación <span class="required">*</span></label>
	<div class="input-group">
	  <input type="text" class="form-control datepicker" id="date" name="date" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask value="<?=date("d/m/Y")?>">
	  <div class="input-group-addon pointer"><i class="fa fa-calendar"></i></div>
	</div>
	<!-- /.input group -->
	</div>
	<div class="form-group required">		
	  <div class="bootstrap-timepicker">
		<div class="form-group">
		  <label for="Profile_title" class="required">Hora de publicación <span class="required">*</span></label>
		  <div class="input-group">
			<input type="text" name="hour" id="hour" class="form-control timepicker"> 
			<div class="input-group-addon">
			  <i class="fa fa-clock-o"></i>
			</div>
		  </div>
		  <!-- /.input group -->
		</div>
		<!-- /.form group -->
	  </div>	
	</div>
	<div class="form-group required">
		<label for="Profile_title" class="required">Canal <span class="required">*</span></label>
		<?php echo CHtml::dropDownList('channel', array(), CHtml::listData($channel, 'id', 'name'),array('empty' => 'Seleccione','class'=>'form-control channel')); ?>
	</div>
	<div class="form-group required">
		<label for="Profile_title" class="required">Subcanal <span class="required">*</span></label>
		<?php echo CHtml::dropDownList('subchannel', array(), array(),array('multiple'=>'multiple','class'=>'form-control subchannel select2','style'=>'width:100%')); ?>
	</div>
	<input class="btn btn-default publicar" type="submit" id="assign" value="Publicar">
</form>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/js/form.js',CClientScript::POS_END) ;?>
<script>
$( document ).ready(function() {
	channelsFilter();
});
</script>	
