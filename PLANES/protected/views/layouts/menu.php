 <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar" style="overflow-y: scroll;
    overflow-x: hidden;
    max-height: 595px;">
      <!-- Sidebar user panel -->

      <!-- search form -->
     <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="search_bar" id="search_bar" class="form-control" placeholder="Buscar...">
		  <i class="fa fa-close right-icon-search-p" style="color: rgb(204, 204, 204);"></i>
              <span class="input-group-btn">
                <button type="button" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
		<ul class="sidebar-menu">
			<li class="header">MENU</li>
		</ul>
        <?php
        $this->widget('zii.widgets.CMenu', array(
                'items'=>Yii::app()->user->getMenu(),
                'htmlOptions'=>array('class'=>'sidebar-menu'),
                'submenuHtmlOptions'=>array('class'=>'treeview-menu'),
                'encodeLabel'=>false,
                'activeCssClass' => 'active'
            )
        );
        ?>
      <!--<ul class="sidebar-menu">
        <li class="header">MENU</li>
		<li class="treeview">
          <a href="<?php echo Yii::app()->request->baseUrl; ?>/dashboard">
            <i class="fa fa-user"></i>
            <span>Dashboard</span>
          </a>
        </li>
		<li class="treeview">
          <a href="#">
            <i class="fa fa-user"></i>
            <span>Personas</span>
            <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo Yii::app()->request->baseUrl; ?>/usuarios"><i class="fa fa-circle-o"></i> Usuarios</a></li>
            <li><a href="<?php echo Yii::app()->request->baseUrl; ?>/widgets"><i class="fa fa-circle-o"></i> Widgets</a></li>
            <li><a href="<?php echo Yii::app()->request->baseUrl; ?>/roles"><i class="fa fa-circle-o"></i> Roles</a></li>
            <li><a href="<?php echo Yii::app()->request->baseUrl; ?>/menues"><i class="fa fa-circle-o"></i> Menues</a></li>
            <li><a href="<?php echo Yii::app()->request->baseUrl; ?>/perfiles"><i class="fa fa-circle-o"></i> Perfiles</a></li>
          </ul>
        </li>
		<li class="treeview">
          <a href="#">
            <i class="fa fa-globe"></i>
            <span>Intranet</span>
            <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">
            <li><a href="pages/charts/chartjs.html"><i class="fa fa-circle-o"></i> Administración</a></li>
            <li><a href="pages/charts/morris.html"><i class="fa fa-circle-o"></i> Productos</a></li>
            <li><a href="pages/charts/flot.html"><i class="fa fa-circle-o"></i> Comercial</a></li>
			<li class="treeview">
			  <a href="#">
				<i class="fa fa-circle-o"></i>
				<span>Recursos Humanos</span>
				<i class="fa fa-angle-left pull-right"></i>
			  </a>
			  <ul class="treeview-menu">
				<li>
				  <a href="<?php echo Yii::app()->request->baseUrl; ?>/usuarios">
					<i class="fa fa-circle-o"></i> <span>Personal</span>
				  </a>
				</li>
				</ul>
			</li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-gear"></i>
            <span>Sistemas</span>
            <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">

      <li class="treeview">
        <a href="#">
        <i class="fa fa-circle-o"></i>
        <span>Api's Internas</span>
        <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
         <li><a href="<?php echo Yii::app()->request->baseUrl; ?>/usuariosapi"><i class="fa fa-circle"></i> Usuarios Api</a></li>
            <li><a href="<?php echo Yii::app()->request->baseUrl; ?>/contactosapi"><i class="fa fa-circle"></i> Contactos</a></li>
            <li><a href="<?php echo Yii::app()->request->baseUrl; ?>/usuariosapi2"><i class="fa fa-circle"></i> Usuarios</a></li>
            <li><a href="<?php echo Yii::app()->request->baseUrl; ?>/catalogoapi"><i class="fa fa-circle"></i> Catálogo Api's</a></li>
            <li><a href="<?php echo Yii::app()->request->baseUrl; ?>/logsapi"><i class="fa fa-circle"></i> Logs</a></li>
            <li><a href="<?php echo Yii::app()->request->baseUrl; ?>/apikey"><i class="fa fa-circle"></i> Api's Key</a></li>
        </ul>
      </li>

      <li class="treeview">
        <a href="#">
        <i class="fa fa-circle-o"></i>
        <span>Api's Externas</span>
        <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
         <li><a href="<?php echo Yii::app()->request->baseUrl; ?>/usuariosapi"><i class="fa fa-circle"></i> Plataforma proveedora</a></li>
            <li><a href="<?php echo Yii::app()->request->baseUrl; ?>/contactosapi"><i class="fa fa-circle"></i> Regla de servicios</a></li>
            <li><a href="<?php echo Yii::app()->request->baseUrl; ?>/usuariosapi2"><i class="fa fa-circle"></i> Canales</a></li>
            <li><a href="<?php echo Yii::app()->request->baseUrl; ?>/catalogoapi"><i class="fa fa-circle"></i> Subcanales</a></li>
            <li><a href="<?php echo Yii::app()->request->baseUrl; ?>/logsapi"><i class="fa fa-circle"></i> Tipo de Servicios</a></li>
            <li><a href="<?php echo Yii::app()->request->baseUrl; ?>/apikey"><i class="fa fa-circle"></i> Proveedores</a></li>
        </ul>
      </li>
          </ul>
        </li>

		<li class="treeview">
          <a href="#">
            <i class="fa fa-envelope"></i>
            <span>Emailing</span>
            <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo Yii::app()->request->baseUrl; ?>/estadisticas"><i class="fa fa-circle-o"></i> Estadisticas</a></li>
            <li><a href="<?php echo Yii::app()->request->baseUrl; ?>/contactos"><i class="fa fa-circle-o"></i> Contactos</a></li>
            <li><a href="<?php echo Yii::app()->request->baseUrl; ?>/campaña"><i class="fa fa-circle-o"></i> Campañas</a></li>
            <li><a href="<?php echo Yii::app()->request->baseUrl; ?>/asistentes"><i class="fa fa-circle-o"></i> Asistentes</a></li>
            <li><a href="<?php echo Yii::app()->request->baseUrl; ?>/mailheader"><i class="fa fa-circle-o"></i> Header News</a></li>
            <li><a href="<?php echo Yii::app()->request->baseUrl; ?>/mailfooter"><i class="fa fa-circle-o"></i> Footer News</a></li>
          </ul>
        </li>

        <li class="treeview">
          <a href="#">
            <i class="fa fa-credit-card"></i>
            <span>Medios de Pago</span>
            <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo Yii::app()->request->baseUrl; ?>/entidades"><i class="fa fa-circle-o"></i> Entidades</a></li>
            <li><a href="<?php echo Yii::app()->request->baseUrl; ?>/mediospago"><i class="fa fa-circle-o"></i> Medios de pagos</a></li>
            <li><a href="<?php echo Yii::app()->request->baseUrl; ?>/planfinanciacion"><i class="fa fa-circle-o"></i> Planes de financiación</a></li>
            <li><a href="<?php echo Yii::app()->request->baseUrl; ?>/legales"><i class="fa fa-circle-o"></i> Legales</a></li>
            <li><a href="<?php echo Yii::app()->request->baseUrl; ?>/proveedores"><i class="fa fa-circle-o"></i> Proveedores</a></li>
            <li><a href="<?php echo Yii::app()->request->baseUrl; ?>/productos"><i class="fa fa-circle-o"></i> Productos</a></li>
            <li><a href="<?php echo Yii::app()->request->baseUrl; ?>/tarjetascredito"><i class="fa fa-circle-o"></i> Tarjetas de crédito</a></li>
            <li><a href="<?php echo Yii::app()->request->baseUrl; ?>/markupconcepto"><i class="fa fa-circle-o"></i> Markups conceptos</a></li>
            <li><a href="<?php echo Yii::app()->request->baseUrl; ?>/markupconfig"><i class="fa fa-circle-o"></i> Configurar markup</a></li>
          </ul>
        </li>


		<li class="treeview">
          <a href="#">
            <i class="fa fa-search"></i>
            <span>Buscadores</span>
            <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">
          <li><a href="<?php echo Yii::app()->request->baseUrl; ?>/buscadores"><i class="fa fa-circle-o"></i> Buscadores</a></li>
            <li><a href="#"><i class="fa fa-circle-o"></i> Configuración Ski</a></li>

          </ul>
        </li>
        <li>
          <a href="#">
            <i class="fa fa-file-picture-o"></i> <span>Tipos de banner</span>
          </a>
        </li>
      </ul>-->
    </section>
  </aside>
