<span style="display:none;" id="title-modal-p"><?=$title?></span>
<?=$breadcrumb?>
<form method="post" id="publication_form" style="clear: both;">
	<div class="error_c"></div>
	<div class="form-group  required">
	<label>Fecha de publicación <span class="required">*</span></label>
	<div class="input-group">
	  <input type="text" class="form-control datepicker" id="date" name="date" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask value="<?=date("d/m/Y")?>">
	  <div class="input-group-addon">
		<i class="fa fa-calendar"></i>
	  </div>
	</div>
	<!-- /.input group -->
	</div>
	<div class="form-group  required">		
	  <div class="bootstrap-timepicker">
		<div class="form-group">
		  <label for="Profile_title" class="required">Hora de publicación <span class="required">*</span></label>

		  <div class="input-group">
			<input type="text" name="hour" id="hour" class="form-control timepicker"> 

			<div class="input-group-addon">
			  <i class="fa fa-clock-o"></i>
			</div>
		  </div>
		  <!-- /.input group -->
		</div>
		<!-- /.form group -->
	  </div>	
	</div>
	<input class="btn btn-default publicar" type="submit" id="assign" value="Publicar">
</form>