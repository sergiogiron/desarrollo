<?
$path = "http://localhost/ttsviajes";

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />

	

	 <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?=$path?>/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?=$path?>/css/AdminLTE.min.css">
  <link rel="stylesheet" href="<?=$path?>/css/skins/_all-skins.min.css">

  <link rel="stylesheet" href="<?=$path?>/plugins/datatables/dataTables.bootstrap.css">

  <!-- blueprint CSS framework 
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen.css" media="screen, projection" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css" media="print" />
	[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection" />
	<![endif]-->

	<!--<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css" />-->
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css" />

	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>

<body class="hold-transition skin-red sidebar-mini">

<div class="wrapper">

	<? include('header.php');?>
	<? include('menu.php');?>

	<div class="content-wrapper">
		
	<section class="content-header">
      <h1>
        Dashboard
        <small>Control panel</small>
      </h1>
      
    

	<?php $this->widget('zii.widgets.CBreadcrumbs', array(
		'links'=>$this->breadcrumbs,
		'htmlOptions'=>array('class'=>'breadcrumb'),
	)); ?><!-- breadcrumbs -->
</section>
<section class="content">
	<?php echo $content; ?>
	</section>
</div>
	

</div><!-- page -->
<? include('footer.php');?>
</div>

<script src="plugins/jQuery/jQuery-2.2.0.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
<script src="<?=$path?>/js/bootstrap.min.js"></script>
<script src="<?=$path?>/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?=$path?>/plugins/datatables/dataTables.bootstrap.min.js"></script>


</body>
</html>