<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo CHtml::encode($this->pageTitle); ?></title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="<?=Yii::app()->params['cdnUrl']?>/plugins/bootstrap/bootstrap-iconpicker.min.css"/>
    <link rel="stylesheet" href="<?=Yii::app()->params['cdnUrl']?>/plugins/bootstrap/bootstrap.tts.css">
    <!-- <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/../cdn/plugins/bootstrap/bootstrap-iconpicker.min.css"/>
    <link rel="stylesheet" href="<?=Yii::app()->request->baseUrl?>/../cdn/plugins/bootstrap/bootstrap.tts.css"> -->

    <meta name="robots" content="noindex">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?=Yii::app()->params['cdnUrl']?>/fonts/font-awesome-4.2.0/css/font-awesome.min.css"/>
    <!-- <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/../cdn/fonts/font-awesome-4.2.0/css/font-awesome.min.css"/> -->

    <!-- Font TTS -->
    <link rel="stylesheet" href="<?=Yii::app()->params['cdnUrl']?>/css/ttsfonticons.css"/>

    <!-- OTHERS Font -->
  	<link rel="stylesheet" href="<?=Yii::app()->params['cdnUrl']?>/fonts/elusive-icons-2.0.0/css/elusive-icons.min.css"/>
    <link rel="stylesheet" href="<?=Yii::app()->params['cdnUrl']?>/fonts/map-icons-2.1.0/css/map-icons.min.css"/>
    <link rel="stylesheet" href="<?=Yii::app()->params['cdnUrl']?>/fonts/material-design-1.1.1/css/material-design-iconic-font.min.css"/>
    <link rel="stylesheet" href="<?=Yii::app()->params['cdnUrl']?>/fonts/octicons-2.1.2/css/octicons.min.css"/>
    <link rel="stylesheet" href="<?=Yii::app()->params['cdnUrl']?>/fonts/typicons-2.0.6/css/typicons.min.css"/>
    <link rel="stylesheet" href="<?=Yii::app()->params['cdnUrl']?>/fonts/weather-icons-1.2.0/css/weather-icons.min.css"/>
    <!-- <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/icon-fonts/elusive-icons-2.0.0/css/elusive-icons.min.css"/>
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/icon-fonts/map-icons-2.1.0/css/map-icons.min.css"/>
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/icon-fonts/material-design-1.1.1/css/material-design-iconic-font.min.css"/>
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/icon-fonts/octicons-2.1.2/css/octicons.min.css"/>
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/icon-fonts/typicons-2.0.6/css/typicons.min.css"/>
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/icon-fonts/weather-icons-1.2.0/css/weather-icons.min.css"/> -->

    <!-- Ionicons -->
    <link rel="stylesheet" href="<?=Yii::app()->params['cdnUrl']?>/fonts/ionicons-1.5.2/css/ionicons.min.css"/>
    <!-- <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/../cdn/fonts/ionicons-1.5.2/css/ionicons.min.css"/> -->

    <!-- Theme style -->
    <link rel="stylesheet" href="<?=Yii::app()->params['cdnUrl']?>/plugins/admin-lte/AdminLTE.css">
    <link rel="stylesheet" href="<?=Yii::app()->params['cdnUrl']?>/fonts/icomoon/style.css">
    <link rel="stylesheet" href="<?=Yii::app()->params['cdnUrl']?>/plugins/datatables/dataTables.bootstrap.css">
    <!-- <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/../cdn/plugins/admin-lte/AdminLTE.css">
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/../cdn/fonts/icomoon/style.css">
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/../cdn/plugins/datatables/dataTables.bootstrap.css"> -->

    <!-- AdminLTE Skins. Choose a skin from the css/skins. folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="<?=Yii::app()->params['cdnUrl']?>/css/skins/_all-skins.min.css">
    <!-- <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/../cdn/css/skins/_all-skins.min.css"> -->

    <?php Yii::app()->clientScript->registerCssFile(Yii::app()->params['cdnUrl'].'/plugins/sweetalert/sweetalert.css'); ?>
    <?php //Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl.'/../cdn/plugins/sweetalert/sweetalert.css'); ?>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- jQuery 2.2.0 -->
    <?php 
        if ($this->loadJQuery){
            //echo '<script src="'.Yii::app()->request->baseUrl.'/plugins/jQuery/jQuery-2.2.0.min.js"></script>';
            echo '<script src="'.Yii::app()->params['cdnUrl'].'/plugins/jQuery/jQuery-2.2.0.min.js"></script>';
            //echo '<script src="'.Yii::app()->request->baseUrl.'/../cdn/plugins/jQuery/jquery.min.js"></script>';
        }       
    ?>

    <!-- Estilos del CDN -->
    <link rel="stylesheet" href="<?=Yii::app()->params['cdnUrl']?>/css/form.css">
    <!-- <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/../cdn/css/form.css"> -->

    <!-- Estilos del Proyecto -->
    <link rel="stylesheet" type="text/css" href="<?=Yii::app()->request->baseUrl?>/css/form.css" />
    <link rel="stylesheet" href="<?=Yii::app()->request->baseUrl?>/css/form-tts.css"/>

    <script>homeurl='<?=Yii::app()->request->baseUrl?>';</script>
	<script>
	$( document ).ready(function() {
		$( ".treeview" ).each(function( index ) {
		  if($( this ).hasClass('active_menu')){ $(this).parent().parent().addClass('active'); }
		});		
		$('#search_bar').keyup(function(){
		   var valThis = $(this).val().toLowerCase();
			if(valThis == ""){
				$('.treeview').show();
				$('.treeview-menu').show();
			} else {
				$('.treeview-menu').hide();
				$('.treeview').each(function(){
					var text = $(this).text().toLowerCase();
					if(text.indexOf(valThis) >= 0){
						$(this).show();
						$(this).parent().show();							
					}else{						
						$(this).hide();
					} 
				});
		   };
		});
		$("#search_bar").keyup(function(){
			var val = $(this).val();   
			if(val.length > 0) {
				$(this).parent().find(".right-icon-search-p").css('color','#555');
			} else {
				$(this).parent().find(".right-icon-search-p").css('color','#ccc');
			}
		});
		$(".right-icon-search-p").click(function(){
			$(this).parent().find("input").val('');
			$(this).css('color','#ccc');
		});	
	});
	</script>
</head>
<div class="ligth-box-modal">
	<div class="modal-dialog"  id="ldng">
		<div class="modal-content">
			<div class="modal-header modal-publish-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="$('.ligth-box-modal').css('display','none');$('.modal-body').html('');$('.modal-title').html('');$('.modal-footer').html('');">
				<span aria-hidden="true" title="Cerrar" data-toggle="tooltip" class="close_window">×</span></button>
				<h4 class="modal-title"></h4><div id="breadcrumb"></div>
			</div>
			<div class="modal-body">
			</div>
			<div class="modal-footer">
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
</div>
<div class="ligth-box-modal-images">
	<div class="modal-dialog" id="modal-dialog-images" style="min-width: 950px;">
		<div class="modal-content" id="modal-content-images">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="$('.ligth-box-modal-images').css('display','none');">			
				<span aria-hidden="true" title="Cerrar" data-toggle="tooltip" class="close_window">×</span></button>
			</div>
			<div class="modal-footer img-b images_footer_i" id="content_result_img">  
			</div>	  
		</div>
	<!-- /.modal-content -->
	</div>
</div>
<body class="hold-transition skin-black-light sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

  
  <?php echo $this->renderPartial('//layouts/header');  ?>

  <!-- =============================================== -->

  <!-- Left side column. contains the sidebar -->
  
<?php echo $this->renderPartial('//layouts/menu');  ?>
  <!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
		<?php echo $content; ?>
  </div>
  <!-- /.content-wrapper -->

<?php echo $this->renderPartial('//layouts/footer');  ?>
 
 
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
        <!-- ./wrapper -->

        <!-- Bootstrap 3.3.6 -->
        <script src="<?=Yii::app()->params['cdnUrl']?>/plugins/bootstrap/bootstrap.min.js"></script>
        <!-- <script src="<?php echo Yii::app()->request->baseUrl; ?>/../cdn/plugins/bootstrap/bootstrap.min.js"></script> -->

        <!-- SlimScroll -->
        <script src="<?=Yii::app()->params['cdnUrl']?>/plugins/slimScroll/jquery.slimscroll.min.js"></script>
        <!-- <script src="<?php echo Yii::app()->request->baseUrl; ?>/../cdn/plugins/slimScroll/jquery.slimscroll.min.js"></script> -->

        <!-- FastClick -->
        <script src="<?=Yii::app()->params['cdnUrl']?>/plugins/fastclick/fastclick.js"></script>
        <!-- <script src="<?php echo Yii::app()->request->baseUrl; ?>/../cdn/plugins/fastclick/fastclick.js"></script> -->

        <!-- AdminLTE App -->
        <script src="<?=Yii::app()->params['cdnUrl']?>/plugins/admin-lte/app.js"></script>
        <!-- <script src="<?php echo Yii::app()->request->baseUrl; ?>/../cdn/plugins/admin-lte/app.js"></script> -->

        <!-- AdminLTE for demo purposes -->
        <script src="<?=Yii::app()->params['cdnUrl']?>/plugins/admin-lte/demo.js"></script>
        <!-- <script src="<?php echo Yii::app()->request->baseUrl; ?>/../cdn/plugins/admin-lte/demo.js"></script> -->

        <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/sweetalert/sweetalert.min.js',CClientScript::POS_END); ?>
        <?php //Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/../cdn/plugins/sweetalert/sweetalert.min.js',CClientScript::POS_END); ?>

        <script type="text/javascript" src="<?=Yii::app()->params['cdnUrl']?>/plugins/iconset/iconset-all.min.js"></script>
        <!-- <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/../cdn/plugins/iconset/iconset-all.min.js"></script> -->

        <script type="text/javascript" src="<?=Yii::app()->params['cdnUrl']?>/plugins/bootstrap/bootstrap-iconpicker.js"></script>
        <!-- <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/../cdn/plugins/bootstrap/bootstrap-iconpicker.js"></script> -->

        <!-- Scrips del CDN -->
        <script src="<?=Yii::app()->params['cdnUrl']?>/js/form.js"></script>
        <!-- <script src="<?=Yii::app()->request->baseUrl?>/../cdn/js/form.js"></script> -->

    </body>
</html>
