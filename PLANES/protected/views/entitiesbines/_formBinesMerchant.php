<div class="row">
	<div class="col-sm-12">
	<?php  
	foreach($products as $key => $producto){ ?>
		<a href="#" class="btn btn-default ">
			<?=$producto['title'];?>
		</a>
	<?php  } ?>
	</div>
</div>
<div class="box-body">
	<form id="formBinesMerchant" action="<?=Yii::app()->createUrl(strtolower(Yii::app()->controller->id).'/savebines');?>">
		<div class="col-md-12 col-data">
			<div class="form-group col-md-12">
				<label>Bines</label>
				<div class="callout callout-danger format-error" style="display:none">El formato esperado es numérico y con lóngitud 6</div>
				<div class="callout callout-success save-ok" style="display:none">Datos guardados correctamente</div>
				<input type="hidden" id="bines" name="bines" value="<?=$bines;?>">
			</div>
			<div class="form-group col-md-6">
				<label>Merchant</label>
				<input type="text" name="merchant" id="merchant" class="form-control" value="<?=$merchants;?>">
			</div>
			<div class="form-group col-md-6 text-right">
				<input type="submit" class="btn btn-primary " value="Guardar" id="submitForm">
			</div>
		</div>
		<?php foreach($array_data as $key => $data){?>
			<input type="hidden" name="<?=$key;?>" value="<?=$data;?>">
		<?php }?>
			<input type="hidden" value="<?=$enabled_products;?>" name="enabled_products">
	</form>
</div>

<script type="text/javascript">
	$('#bines').tagit({
		singleFieldDelimiter:'|',
		placeholderText :'999999',
		beforeTagAdded: function(event, ui) {
        // do something special
        	if(!isNaN(ui.tagLabel) && ui.tagLabel.length==6){
        		$('.format-error').hide(150);
        		return true;
        	}else{
        		$('.format-error').show(150);
        		return false;
        	}
    	}
	});

	$('#formBinesMerchant').submit(function(e){
		e.preventDefault();
		$('.save-ok').hide(150);
		$('#submitForm').val('Procesando...');
		var url = $(this).attr('action');
		$.ajax({
           type: "POST",
           url: url,
           data: $(this).serialize(), // serializes the form's elements.
           success: function(data){
	        	if(data=='OK'){
					$('#submitForm').val('Guardar');
					$('.save-ok').show(150);
					setTimeout(function() {
						$('.save-ok').hide(150);
				    }, 5000);
	        		
	        	}
           	}
        });
	})
</script>