<?php
/* @var $this CreditCardsController */
/* @var $model CreditCards */
/* @var $form CActiveForm */
?>
<style>
	.tarjeta_button img{
		max-height: 60px;
	}
	.tarjetas-container{
		margin-bottom: 15px;
	}
	.card{
	background: #0348a7;
	color:white;
	font-weight: bold;
	font-size: 14px;
}
.classic{
	background: #aca8a5;
	color:black;
}
.advance{
	background: #d00115; /* Old browsers */
	background: -moz-linear-gradient(45deg,  #d00115 0%, #000000 100%); /* FF3.6-15 */
	background: -webkit-linear-gradient(45deg,  #d00115 0%,#000000 100%); /* Chrome10-25,Safari5.1-6 */
	background: linear-gradient(45deg,  #d00115 0%,#000000 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#d00115', endColorstr='#000000',GradientType=1 ); /* IE6-9 fallback on horizontal gradient */
}
.platinum,.black,.black-{
	background: black;
}
.premier{
	background: #23123f;
}
.american-express{
	background: #589377;
	-webkit-box-shadow:inset 0px 0px 0px 5px #415049;
    -moz-box-shadow:inset 0px 0px 0px 5px #415049;
    box-shadow:inset 0px 0px 0px 5px #415049;
}
.galicia{
	background: #cb5b43; /* Old browsers */
	background: -moz-linear-gradient(left,  #cb5b43 0%, #f3930b 25%); /* FF3.6-15 */
	background: -webkit-linear-gradient(left,  #cb5b43 0%,#f3930b 25%); /* Chrome10-25,Safari5.1-6 */
	background: linear-gradient(to right,  #cb5b43 0%,#f3930b 25%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#cb5b43', endColorstr='#f3930b',GradientType=1 ); /* IE6-9 */
}
.icbc{
	background: #e0e0d8;
	color:#807f7a;
}
.citibank{
	background: #428cc4;
}
.patagonia{
	background: #c8c8ca;
	color: #00ad4e;
}
.macro{
	background: #008cd4;
}
.banco-provincia{
	background: #224c2a; /* Old browsers */
	background: -moz-linear-gradient(left,  #224c2a 77%, #021202 100%); /* FF3.6-15 */
	background: -webkit-linear-gradient(left,  #224c2a 77%,#021202 100%); /* Chrome10-25,Safari5.1-6 */
	background: linear-gradient(to right,  #224c2a 77%,#021202 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#224c2a', endColorstr='#021202',GradientType=1 ); /* IE6-9 */
}
.nacion{
	background: #1498d8; /* Old browsers */
	background: -moz-linear-gradient(left,  #1498d8 59%, #014171 100%); /* FF3.6-15 */
	background: -webkit-linear-gradient(left,  #1498d8 59%,#014171 100%); /* Chrome10-25,Safari5.1-6 */
	background: linear-gradient(to right,  #1498d8 59%,#014171 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#1498d8', endColorstr='#014171',GradientType=1 ); /* IE6-9 */
}
.mastercard{
	background: #cc0000; /* Old browsers */
	background: -moz-linear-gradient(left,  #cc0000 0%, #cc0000 49%, #ff9900 52%); /* FF3.6-15 */
	background: -webkit-linear-gradient(left,  #cc0000 0%,#cc0000 49%,#ff9900 52%); /* Chrome10-25,Safari5.1-6 */
	background: linear-gradient(to right,  #cc0000 0%,#cc0000 49%,#ff9900 52%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#cc0000', endColorstr='#ff9900',GradientType=1 ); /* IE6-9 */

}
.visa{
	background: #e59703; /* Old browsers */
	background: -moz-linear-gradient(top,  #e59703 0%, #e59703 33%, #ffffff 36%, #ffffff 66%, #1b1d74 68%); /* FF3.6-15 */
	background: -webkit-linear-gradient(top,  #e59703 0%,#e59703 33%,#ffffff 36%,#ffffff 66%,#1b1d74 68%); /* Chrome10-25,Safari5.1-6 */
	background: linear-gradient(to bottom,  #e59703 0%,#e59703 33%,#ffffff 36%,#ffffff 66%,#1b1d74 68%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#e59703', endColorstr='#1b1d74',GradientType=0 ); /* IE6-9 */
}
.superville{
	background: #d24e42; /* Old browsers */
	background: -moz-linear-gradient(top,  #d24e42 0%, #d24e42 24%, #ffffff 27%, #ffffff 29%, #221e1f 34%); /* FF3.6-15 */
	background: -webkit-linear-gradient(top,  #d24e42 0%,#d24e42 24%,#ffffff 27%,#ffffff 29%,#221e1f 34%); /* Chrome10-25,Safari5.1-6 */
	background: linear-gradient(to bottom,  #d24e42 0%,#d24e42 24%,#ffffff 27%,#ffffff 29%,#221e1f 34%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#d24e42', endColorstr='#221e1f',GradientType=0 ); /* IE6-9 */
}
.itau{
	color:#f9f500;
}
</style>
<div class="box-body">
	<div class="tarjetas-container">
		<?php foreach ($tarjetas as $key => $tarjeta) { ?>
			<a href="<?=Yii::app()->createUrl(strtolower(Yii::app()->controller->id).'/providersform/id_entidad/'. $id_entidad.'/id_cc/'. $tarjeta['id_credit_card']);?>" class="button tarjeta_button" id="cc<?=$tarjeta['id_credit_card'];?>">
			<!-- <img src="<?='http://office.ttsviajes.com/uploads/creditcards/'.$tarjeta['id_credit_card'].'/'.$tarjeta['credit_card_image_name'].'/'.$tarjeta['credit_card_image_name'].'.'.$tarjeta['credit_card_image_ext']?>" alt="<?=$tarjeta['card_name'];?>"> -->	

			<img src="<?=Yii::app()->request->baseUrl.'/uploads/creditcards/'.$tarjeta['id_credit_card'].'/'.$tarjeta['credit_card_image_name'].'/'.$tarjeta['credit_card_image_name'].'.'.$tarjeta['credit_card_image_ext']?>" alt="<?=$tarjeta['card_name'];?>">	
			</a>
		<?php } ?>
	</div>
	<div id="providers_form">
	</div>
</div>

<?php

	// Activo/Inactivo
	Yii::app()->clientScript->registerCssFile(Yii::app()->params['cdnUrl'].'/plugins/lcswich/lc_switch-tts.css');
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/lcswich/lc_switch.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/lcswich/lc_switch-tts.js',CClientScript::POS_END);
	// Datepicker con la mascara
	Yii::app()->clientScript->registerCssFile(Yii::app()->params['cdnUrl'].'/plugins/datepicker/datepicker3.css');
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/datepicker/bootstrap-datepicker.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/input-mask/jquery.inputmask.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/input-mask/jquery.inputmask.date.extensions.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/datepicker/datepicker-tts.js',CClientScript::POS_END);

	//logica uploades de imagenes
	Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/uploader_image.js',CClientScript::POS_END);
	//logica uploades de imagenes
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/js/dropzone.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerCssFile(Yii::app()->params['cdnUrl'].'/css/jquery.Jcrop.css');
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/js/jquery.Jcrop.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/js/jquery.bxslider.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerCssFile(Yii::app()->params['cdnUrl'].'/plugins/jQueryUI/jquery-ui.min.css');
	Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/jquery-ui.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerCssFile(Yii::app()->params['cdnUrl'].'/css/jquery.bxslider.css');
	
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/jQueryUI/jquery-ui.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerCssFile(Yii::app()->params['cdnUrl'].'/plugins/jQueryUI/jquery-ui.min.css'); 
	Yii::app()->clientScript->registerCssFile(Yii::app()->params['cdnUrl'].'/plugins/tag-it/css/jquery.tagit.css'); 
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/tag-it/js/tag-it.js',CClientScript::POS_END);

	// Timepicker con la mascara
	/*
	Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/../plugins/timepicker/bootstrap-timepicker.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/../plugins/input-mask/jquery.inputmask.bundle.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl.'/../plugins/timepicker/bootstrap-timepicker.css');
	Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/../plugins/timepicker/timepicker-tts.js',CClientScript::POS_END);
	// Editor de texto enriquesido
	Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/../plugins/ckeditor-standard/ckeditor.js',CClientScript::POS_END);
	*/
 ?>
   <script type="text/javascript">
	$('body').on('click',".tarjeta_button",function(event) {
		$('.tarjeta_button img').css('border','none');
		var thisElm = $(this);
		event.preventDefault();
		openLoading();
		var request = $.ajax({
			url: $(this).attr('href'),
		}).done(function(msg) {
			closeLoading();
			$(thisElm).find('img').css('border-bottom','3px solid black');
			/*icono.removeClass('fa fa-spinner fa-spin fa-3x fa-fw');
			icono.addClass(clase);
			boton.css('pointer-events','auto');
			$('.ligth-box-modal').css('display','block');
			$('.modal-content').css('display','block');
			$('.modal-title').html('Visualizando contenido');*/
			$('#providers_form').html(msg);
		});
	});
	$(document).ready(function(){
		$( ".tarjeta_button" ).first().trigger('click');
	})
</script>
