<section class="content-header">
	<?php echo Yii::app()->user->Title(); ?>
	<?php echo Yii::app()->user->Breadcrumb(); ?>
</section>
<section class="content">
	<div class="box box-danger">
		<div class="box-header with-border">
			<h1><?=$nombre_entidad;?></h1>
			<?php $this->renderPartial('_form', array('tarjetas'=>$tarjetas,'id_entidad'=>$id_entidad)); ?>
		</div>
	</div>
</section>