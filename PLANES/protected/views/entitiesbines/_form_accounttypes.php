<?php foreach($providers as $key => $account_types){?>
	<div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title"><?=$key;?></h3>
		</div>
		<div class="panel-body">
			<?php 
			$result = array();
			foreach ($account_types as $data) {
			  $id = $data['account_type'];
			  if (isset($result[$id])) {
			     $result[$id][] = $data;
			  } else {
			     $result[$id] = array($data);
			  }
			}
			foreach ($result as $ac => $account_type) {
				?>
				<a class="account_type" href="<?=Yii::app()->createUrl(strtolower(Yii::app()->controller->id).'/binesform/id_account_type/'. $account_type[0]['id_account_type'].'/providertype/'.$key.'/id_entity/'.$account_type[0]['id_entity'].'/id_credit_card/'.$account_type[0]['id_credit_card']);?>" >
					<div class="panel panel-default col-sm-2 card <?=$this->getClassCreditCard($ac)?>">
						<div class="panel-body">
						<?=$ac;?>
						</div>
					</div>
				</a>
			<?php } ?>
		</div>
	</div>
<?php

 } ?>
<script>
$('body').on('click',".account_type",function(event) {
	var thisElm = $(this);
	event.preventDefault();
	var href = thisElm.attr('href');
	openLoading();
	var request = $.ajax({
		url: href
	}).done(function(msg) {
		closeLoading();
		$('.ligth-box-modal').css('display','block');
		$('.modal-title').html('Visualizando contenido');
		$('.modal-body').html(msg);
	}).fail(function( jqXHR, textStatus, errorThrown ) {
		closeLoading();
	     if ( console && console.log ) {
	         console.log( "La solicitud a fallado: " +  textStatus);
	     }
	});

});	
</script>