<?php Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl.'/css/financiacion.css');?>
<section class="site financiacion responsive" style="max-height: 70vh;overflow-x: hidden;overflow-y: auto;">
<h1 class="destacado">Disfrut&aacute; de la mejor financiaci&oacute;n con estos beneficios</h1>
	<div class="row">
		<?php
			foreach($financiacion as $banco){
				if(isset($banco['expire_date_from']) && isset($banco['expire_date_to'])){
					$banco_expire_date_from=$banco['expire_date_from'];
					$banco_expire_date_to=$banco['expire_date_to'];
				}else{
					$banco_expire_date_from='';
					$banco_expire_date_to='';
				}
				if($banco_expire_date_to!='' && $banco_expire_date_from!=''){
					$result=Utils::getVigencia(array('active_from'=>$banco_expire_date_from,'active_to'=>$banco_expire_date_to));
				}else{
					$result=1;
				}
				if($result>=0){
				$cuotas = 0;
				if($banco['entity_highlight'] == 1){
					$bancoExclusivo = 'bancoExclusivo';
					$titleExclusivo = '<p class="exclusivo">Exclusivo</p>';
				}else{
					$bancoExclusivo = '';
					$titleExclusivo = '';
				}
				$popupHTML = $titleExclusivo;
				$popupHTML .= '<div class="texto">';

				foreach($banco['data'] as $bancod){
					if(Utils::getVigencia(array('active_from'=>$bancod['date_validity_start'],'active_to'=>$bancod['date_validity_end']))>=0){
						$cuotasBuscar = array(' - ','-');
						$cuotas = str_replace($cuotasBuscar,' a ',$bancod['amount_dues']);
						$cuotasBuscar = array(', ',',');
						$cuotas = str_replace($cuotasBuscar,', ',$cuotas);

						$servicios = '';
						if(isset($bancod['text_1'])){
							$servicios = ' <span>'.$bancod['text_1'].'</span>';
						}else{
							$servicios = '';
						}

						$interes = '';
						if(strpos($banco['title'],'HSBC')!==false){
							$interes = ' sin inter&eacute;s';
						}else{
							if($bancod['interests'] == 0){
							$interes = ' ';
						}else{
							$interes = ' con '.$bancod['interests'].'% de inter&eacute;s';
						}
						}
						

						$clientes = '';
						if(isset($bancod['text_2'])){
							$clientes = '<p class="cliente">'.$bancod['text_2'].'</p>';
						}else{
							$clientes = '';
						}

						$popupHTML .= '<article>';
						//$popupHTML .= '<p class="cftna"><span class="sigla">C.F.T.N.A.</span>'.Utils::formatPercent($bancod['cftna']).'<span class="porciento">%</span></p>';
						$popupHTML .= '<p class="cftna"><span class="sigla">C.F.T.N.A.</span>'.$bancod['cftna'].'</p>';
						$popupHTML .= '<h5>'.$cuotas.' cuotas'.$interes.$servicios.'</h5>';
						$popupHTML .= $clientes;
						$popupHTML .= '<div class="legales">';
						$contenido = str_replace('@vigencia_desde',$bancod['date_validity_start'],$bancod['legal']);
						$contenido = str_replace('@vigencia_hasta',$bancod['date_validity_end'],$contenido);
						$popupHTML .= $contenido.'</div>';
						$popupHTML .= '</article>';
					}
				}
				$popupHTML .= '</div>';
				?>
				<article class="col-xxs-12 col-xs-6 col-sm-4 col-md-3 col-lg-2 <?=$bancoExclusivo?>">
					<a class="caja" title="<?=$banco['title']?>" data-html='<?=$popupHTML?>' data-image="<?=$banco['imagenes'][0]['filename'].'.'.$banco['imagenes'][0]['ext']?>"  data-image-alt="<?=$banco['imagenes'][0]['title']?>">
						<img src="<?=$banco['imagenes'][0]['filename'].'.'.$banco['imagenes'][0]['ext']?>" alt="<?=$banco['imagenes'][0]['title']?>" />
						<?=$titleExclusivo?>
						<p class="cuotas"><strong>Hasta <?=$banco['amount_dues_max_f']?> cuotas</strong><br />
						<?php if(strpos($banco['title'],'HSBC')!==false):?>
							sin inter&eacute;s
						<?php endif;?>
						</p>
						<p class="vermas">Ver m&aacute;s</p>
					</a>
				</article>
				<?php
				}
			}
		?>
	</div>
	<!-- <h2 class="destacado">Ver otros medios de pago</h2> -->
</section>