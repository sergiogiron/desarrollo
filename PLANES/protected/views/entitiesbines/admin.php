<?php
	/* @var $this ExcludeRulesController */
	/* @var $model ExcludeRules */

	Yii::app()->clientScript->registerScript('search', "search(); deleteBtn();");
?>

<style>
/*
	.panel-body img{
		max-width: 100%;
		max-height: 60px;
	}
	.custom-panel{
		border: 1px solid red;
	}
	#search{
		height: 34px;
	}
	.panel-footer-hidden{
		display: none;
	}
	ul {
	   list-style-type: none; margin: 0; padding: 0;
	}
	.entidades .panel{
		transition: 0.5s all;
	}
.bancoExclusivo p.exclusivo{
    background-color:#D32027;
    color:#FFF;
    font-family:'OpenSansBold';
    font-size:14px;
    margin-top:-10px;
    position:absolute;
    text-align:center;
    width:80px;
}
.bancoExclusivo p.exclusivo:after{
	border-top:10px solid transparent;
	border-bottom:10px solid transparent;
	border-left:10px solid #D01921;
	content: "";
	left:80px;
	margin-top:0px;
	position: absolute;
}
*/
.sweet-alert{
    width:90% !important;
    left: 24% !important;
    margin-top: -290px !important;
}
.sweet-alert h2{
	margin: 0px;

}
</style>
<section class="content-header">
	<?php echo Yii::app()->user->Title(); ?>
	<?php echo Yii::app()->user->Breadcrumb(); ?>
	<?php //$this->widget('ext.publicationrecord.PublicationrecordWidget'); ?>
</section>
<section class="content admin">
	<div class="box box-danger">
		<div class="box-header with-border">	
			<?php 
			if(Yii::app()->user->checkAccess('create')){
				/*echo '<a href="'.Yii::app()->request->baseUrl.'/creditcards/create/origin/entitiesbines" class="btn btn-default b_customize"><i class="fa fa-plus i_customize"></i>Agregar Tarjeta Crédito</a>';
				echo '<a href="'.Yii::app()->request->baseUrl.'/entity/create/origin/entitiesbines" class="btn btn-default b_customize"><i class="fa fa-plus i_customize"></i>Agregar Entidad</a>';*/
			}
			
			?>
			<?php
				if(Yii::app()->user->checkAccess('publish')){
					echo '<a href="'.Yii::app()->request->baseUrl.'/entitiesbines/publishpreview" class="preview btn btn-default b_customize"><i class="fa fa-cloud-upload i_customize"></i>Publicar</a>';

					echo '<a href="'.Yii::app()->request->baseUrl.'/entitiesbines/publish" style="display:none;" class="publish btn btn-success a_customize"><i class="fa fa-cloud-upload i_customize" ></i>Publicar</a>';
				}
			?>
			<div class="col-md-4 search_bar">
				<div class="row">
					<div class="input-group input-group-sm">
						<input type="text" class="form-control" id="search"><i class="fa fa-close right-icon-search"></i>
						<span class="input-group-btn">
						  <button type="button" class="btn btn-default btn-flat"><i class="fa fa-search"></i></button>
						</span>
					</div>
				</div>
			</div>
		</div>
		<div class="content">
			<input type="hidden" name="order" id="order" />	
			<div class="row entidades">
				<?php 
					foreach ($entites as $key => $entity) {
						$classExclusivo = '';
						$pExclusivo = '';
						if(!empty($entity[0]['entity_highlight'])){
							$classExclusivo = 'bancoExclusivo';
							$pExclusivo = '<p class="exclusivo">Exclusivo</p>';
						}
					 ?>
						<div class="col-xxs-12 col-xs-6 col-md-4 col-lg-3 entity-container <?=$classExclusivo?>" id="<?=$entity[0]['id_entity'];?>">
							<div class="panel panel-default text-center" >	
								<div class="panel-body ">
									<input type="checkbox" class="enabledMini" name="estado" id="estado" value="<?=$entity[0]['id_entity'];?>" <?=($entity[0]['enabled']==1)?' checked="checked"':"";?> />
									
									<!-- <img src="<?='http://office.ttsviajes.com/uploads/entity/'.$entity[0]['id_entity'].'/'.$entity[0]['entity_image_name'].'/'.$entity[0]['entity_image_name'].'.'.$entity[0]['entity_image_ext']?>" alt=""> -->

									<img src="<?=Yii::app()->request->baseUrl.'/uploads/entity/'.$entity[0]['id_entity'].'/'.$entity[0]['entity_image_name'].'/'.$entity[0]['entity_image_name'].'.'.$entity[0]['entity_image_ext']?>" alt="">
									<?=$pExclusivo?>
								</div>
								<div class="panel-footer">
									<span class="description">
										<?php if($entity[0]['dues']){
											$max_cuotas = 0;
											foreach ($entity as $k => $due) {
												if(Utils::getMaxDue($due['dues'])>$max_cuotas){
													$max_cuotas = Utils::getMaxDue($due['dues']);
												}
											}
										 ?>
										<p>HASTA <span class="cuota"><?=$max_cuotas;?></span> CUOTAS</p>
										<?php } ?>
										<span><?=Utils::date_spa($entity[0]['expire_date_from']).' al '.Utils::date_spa($entity[0]['expire_date_to']);?></span>
									</span>
									<span class="options" style="display: none;">
										<a href="<?=Yii::app()->request->baseUrl;?>/entidades/modificar/<?=$entity[0]['id_entity'];?>"><i class="fa fa-edit icon_cbutton" data-toggle="tooltip" title="Editar"></i></a>
										<a href="<?=Yii::app()->request->baseUrl;?>/entidades/borrar/<?=$entity[0]['id_entity'];?>"><i class="fa fa-trash-o icon_cbutton delete_action" data-toggle="tooltip" title="Borrar"></i></a>
										<a href="<?=Yii::app()->request->baseUrl.'/entitiesbines/update/id/'.$entity[0]['id_entity'];?>"><i class="fa fa-credit-card icon_cbutton" data-toggle="tooltip" title="Bines"></i></a>
									</span>
								</div>
								<div class="panel-footer-hidden">
									<?php 
									$ccdispo = '';
									foreach ($entity as $k => $entity_data) {
										$ccdispo.=$entity_data['card_name']." ";
										if($entity_data['type'] == 'Airlines' || $entity_data['type'] == 'Cruiser'){
											$ccdispo.=$entity_data['provider_name']." ";
										}
									}
									$ccdispo .= $key." ";
									echo rtrim($ccdispo);
									?>
								</div>
							</div>
						</div>	
					<?php } ?>
			</div>
		</div>

	</div>
 </section>
<?php 
Yii::app()->clientScript->registerCssFile(Yii::app()->params['cdnUrl'].'/plugins/lcswich/lc_switch-tts.css');
Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/lcswich/lc_switch.js',CClientScript::POS_END);
Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/lcswich/lc_switch-tts.js',CClientScript::POS_END);
Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl.'/css/entitiesbines.css');
 ?>
<script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script type="text/javascript">
$('#search').keyup(function(){
	var filter = $(this).val();
  	$(".entity-container").each(function(){
        // If the list item does not contain the text phrase fade it out
        if ($(this).text().search(new RegExp(filter, "i")) < 0) {
            $(this).fadeOut();
        // Show the list item if the phrase matches and increase the count by 1
        } else {
            $(this).show();
        }
	});
});
/*$('.panel-footer').on('mouseover',function(){
	$(this).find('.description').hide();
	$(this).find('.options').show();
	}).mouseout(function() {
    	$(this).find('.description').show();
		$(this).find('.options').hide();
}); */
$('.options .fa-edit').on('click',function(){
	console.log($(this).data('url'))
});
$(document).ready(function(){
	//$('.enabled').lc_switch('ACTIVO', 'INACTIVO');
	
	$( ".entidades" ).sortable({
    	update:function(e,ui){
    		var order = $(this).sortable( "toArray");
    		$('#order').val(order);
    		console.log(order.toString());
    		$.ajax({
		       type: "POST",
		       url: '<?=Yii::app()->request->baseUrl;?>/entity/updateorder',
		       data: {order:order.toString()}, // serializes the form's elements.
		       success: function(data){
		        	if(data=='OK'){
		        		console.log(data)
		        		//clearInterval(glowing);
						/*$('.save-ok').show(150);
						setTimeout(function() {
							$('.save-ok').hide(150);
					    }, 5000);*/
		        		
		        	}
		       	}
		    });
    	}
    });

});

$('body').delegate('.enabledMini', 'lcs-statuschange', function() {
    var status = ($(this).is(':checked')) ? '1' : '0';
    var value = $(this).val();
	$.ajax({
       type: "POST",
       url: '<?=Yii::app()->request->baseUrl;?>/entity/disableentity',
       data: {id_entity:value,status:status}, // serializes the form's elements.
       success: function(data){
       		console.log(data);
        	/*if(data=='OK'){
				$('.save-ok').show(150);
				setTimeout(function() {
					$('.save-ok').hide(150);
			    }, 5000);
        		
        	}*/
       	}
    });
});

$( ".preview" ).click(function(event){
	event.preventDefault();
	/*$('.modal-content').css('display','none');
	$('.ligth-box-modal').css('display','block');
	$('#ldng').append('<img src="'+homeurl+'/img/ajax_loader.gif" class="loader_gif_img" />');	*/
	//openLoading();			
	var request = $.ajax({
		url: $(this).attr('href'),
	}).done(function(msg) {
		closeLoading();
		swal({
		  title: "Previsualizando como se va a ver en el sitio",
		  text: msg,
		  html: true,
	      showCancelButton: true,
		  cancelButtonText: "Cancelar", 
		  confirmButtonColor: "#00a65a",
		  confirmButtonText: "Confirmar",
		  closeOnConfirm: true
		},
		function(){
			$('.publish').trigger('click');
		});

	});			
});

</script>
