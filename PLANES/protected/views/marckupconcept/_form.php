<?php
/* @var $this MarckupConceptController */
/* @var $model MarckupConcept */
/* @var $form CActiveForm */
?>

<div class="box-body">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'marckup-concept-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>
	<span class="right_f active-button">
		<?php echo $form->checkBox($model,'enabled',array('class'=>'lcs_check enabled')); ?>
	</span>

	<div class="col-md-12 col-data">
		<div class="form-group col-md-6 required">
			<label>Concepto </label> <i class="fa fa-key" aria-hidden="true"></i>
			<?php echo $form->textField($model,'title',array('size'=>45,'maxlength'=>45, 'class'=>'form-control')); ?>
			<?php echo $form->error($model,'title'); ?>
		</div>
		</div>
		<div class="box-footer">
		<?php
			$title = (Yii::app()->controller->action->id!='view') ? 'Volver sin guardar cambios' : 'Volver';
			echo '<a href="'.Yii::app()->request->baseUrl.'/'.strtolower(Yii::app()->controller->id).'" class="btn btn-secundary cancel" data-toggle="tooltip" title="'.$title.'" >Cerrar</a>';
			if(Yii::app()->controller->action->id!='view'){
				echo CHtml::submitButton('Guardar',array('class'=>'btn btn-primary save','data-toggle'=>'tooltip','title'=>'Guardar cambios'));
			}
		?>
	</div>
  	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
<?php
	// Activo/Inactivo
	Yii::app()->clientScript->registerCssFile(Yii::app()->params['cdnUrl'].'/plugins/lcswich/lc_switch-tts.css');
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/lcswich/lc_switch.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/lcswich/lc_switch-tts.js',CClientScript::POS_END);
	// Datepicker con la mascara
	Yii::app()->clientScript->registerCssFile(Yii::app()->params['cdnUrl'].'/plugins/datepicker/datepicker3.css');
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/datepicker/bootstrap-datepicker.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/input-mask/jquery.inputmask.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/input-mask/jquery.inputmask.date.extensions.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/datepicker/datepicker-tts.js',CClientScript::POS_END);
	// Timepicker con la mascara
	/*
	Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/../plugins/timepicker/bootstrap-timepicker.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/../plugins/input-mask/jquery.inputmask.bundle.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl.'/../plugins/timepicker/bootstrap-timepicker.css');
	Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/../plugins/timepicker/timepicker-tts.js',CClientScript::POS_END);
	// Editor de texto enriquesido
	Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/../plugins/ckeditor-standard/ckeditor.js',CClientScript::POS_END);
	*/
 ?>
   <script type="text/javascript">
	<?php
		if(Yii::app()->controller->action->id=='view'){ echo "$('form').find('input, textarea, checkbox, select').attr('disabled','disabled'); $('.fa-pencil-square-o').hide()"; }
	?>
</script>
