<?php
/* @var $this MarckupConceptController */
/* @var $data MarckupConcept */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('concept')); ?>:</b>
	<?php echo CHtml::encode($data->concept); ?>
	<br />


</div>