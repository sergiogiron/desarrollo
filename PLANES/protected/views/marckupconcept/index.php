<?php
/* @var $this MarckupConceptController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Marckup Concepts',
);

$this->menu=array(
	array('label'=>'Create MarckupConcept', 'url'=>array('create')),
	array('label'=>'Manage MarckupConcept', 'url'=>array('admin')),
);
?>

<h1>Marckup Concepts</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
