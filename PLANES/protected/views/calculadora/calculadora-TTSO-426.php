<link rel="stylesheet" href="<?=Yii::app()->baseUrl?>/css/calculadora.css">

<section class="content-header">
	<?php echo Yii::app()->user->Title(); ?>
	<?php echo Yii::app()->user->Breadcrumb(); ?> 
</section>

<section class="content">
	<div class="box box-danger">
		<div class="box-header with-border">
			<div class="col-xxs-12 col-xs-6 col-sm-4 search_bar">
				<div class="row">
					<div class="input-group input-group-sm">
						<input type="text" class="form-control criterio" id="origin" onkeypress="return validateQty(event);" placeholder="Ingrese el precio sin el 6%" /><i class="fa fa-close right-icon-search"></i>
						<span class="input-group-btn">
						  <button type="submit" id="calcular" title="Calcular" class="btn btn-default btn-flat"><i class="fa fa-calculator"></i></button>
						</span>
					</div>
				</div>
			</div>
			<button class="btn btn-success a_customize" id="btnPrint"><i class="fa fa-print i_customize"></i>Imprimir</button>
<!--
			<div style="margin-top:30px;margin-bottom:10px;">	
				<p>Precio sin el 6%:</p>
				<input class="" id="origin" style="" value=""onkeypress='return (event.charCode >= 48 && event.charCode <= 57) || event.charCode == 8'>
				<input type="submit" class="btn" id="calcular" value="Calcular" >
				<button class="btn btn-primary" id="btnPrint"> Imprimir</button>
			</div>
-->
			<!--<br />
			<button class="btn btn-primary btn-block" id="btnPrint" style="margin top:30px;margin-bottom:30px;">Imprimir</button>-->
			<div class="print_content">
				<div class="row parametros">	
					<p class="col-xs-6">Precio Inicial:<br /><span id="initial_price"></span> <span class="leyenda">(sin el 6%)</span></p>
					<p class="col-xs-6">Precio Final:<br /><span id="final_price"></span></p>
				</div>

				<div class="row bancos">
					<?php
						$cant_legales=0;
						if(!empty($array_financing)){ 
							foreach($array_financing as $key => $entidad){
								if(!empty($entidad['imagen'])){
									$imagen = $entidad['imagen'];
								} ?>

								<div id="bank-grid" class="grid-view col-lg-6">
									<div class="table-responsive">
										<table class="table table-bordered table-striped table-list">
											<thead>
												<!--<tr><img src="<?=$imagen?>" alt="<?=$key?>" title="<?=$key?>" /></tr>-->
												<tr>
													<th class="tarjetas">Tarjetas <img src="<?=$imagen?>" alt="<?=$key?>" title="<?=$key?>" /></th>
													<th>Cuotas</th>
													<th>Valor Cuota</th>
													<th>Precio Final</th>
													<th>Financ.</th>
													<!--<th>Legales</th>-->
												</tr>
											</thead>
											<tbody>
												<?php if(isset($entidad['cuotas']) && is_array($entidad['cuotas'])){
													foreach ($entidad['cuotas'] as $cuota=>$intereses){
														foreach ($intereses['interes'] as $interes=>$tarjetas){ ?>

															<tr>
																<td class="tarjetas">
																	<?php foreach ($tarjetas as $indice=>$tarjeta){
																		if($indice>0) echo'<br />'; echo $tarjeta;
																	}?>
																</td>
																<td class="cuotas"><a href="#legal-<?php echo++$cant_legales;?>" title="Ver legales"><?=$cuota?><small class="legal"><?='('.$cant_legales.')'?></small></a></td>
																<td>
																	<div class="calculate_cuota" data-cuotas="<?=$cuota?>" data-financiacion="<?=$interes?>"></div>
																</td>
																<td>
																	<div class="calculate_final" data-cuotas="<?=$cuota?>" data-financiacion="<?=$interes?>"></div>
																</td>
																<td>
																	<div><?=$interes.' %'?></div>
																</td>
															</tr>
														<?php }				
													}
												} ?>
											</tbody>
										</table>
									</div>
								</div>
							<?php }
						}else{  
							echo'<p>Ocurri� un error procesando las financiaciones. Comuniquese con el administrador.</p>';
						}
					?> 
				</div>

				<div class="legales">
					<h2>Legales</h2>
					<?php	
						$i_legales=0;
						if(!empty($array_financing)){ 
							foreach($array_financing as $key => $entidad){
								if(isset($entidad['cuotas']) && is_array($entidad['cuotas'])){
									$banco = $key;
									foreach ($entidad['cuotas'] as $cuota=>$intereses){
										$i_legales++;
										$cuotaPlural = ($cuota > 1) ? 's' : '';
										echo '<h3 id="legal-'.$i_legales.'">'.$banco.' - '.$cuota.' cuota'.$cuotaPlural.'<small class="legal">('.$i_legales.')</small></h3>';
										echo '<p>'.$intereses['legal'].'</p>';
									}
								}
							} 
						}
					?> 
				</div>
			</div>	
		</div>
	</div>
</section>
<styleCSS style="display: none;"><?=include('css/calculadora.print.css');?></styleCSS>

<script>
	$( document ).ready(function() {

		$('#calcular').attr('disabled',true);
		$('#btnPrint').attr('disabled',true);
		$('#origin').keyup(function(){
	        if($(this).val().length !=0){
				$('#calcular').attr('disabled',false);
				$('#btnPrint').attr('disabled',false);
			}          
	        else{
				$('#calcular').attr('disabled',true);
				$('#btnPrint').attr('disabled',true);
			}
	    })
		
		$("#calcular").click(function(){
			var origin= $('#origin').val();
			var final_price = Math.round(origin/0.94);
			$('#initial_price').html('$'+formatNumber(origin));
			$('#final_price').html('$'+formatNumber(final_price));

			$('.calculate_final').each(function( index ) {
				var_cuota=$(this).data( "cuotas" );
				var_interes=$(this).data( "financiacion" );
				$(this).html('$'+formatNumber(Math.round((final_price+(final_price*var_interes/100)))));
			});

			$('.calculate_cuota').each(function( index ) {
				var_cuota=$(this).data( "cuotas" );
				var_interes=$(this).data( "financiacion" );
				$(this).html('$'+formatNumber(Math.round((final_price+(final_price*var_interes/100))/var_cuota)));
			});
		});

	});

	$("#btnPrint").on("click", function () {
		var printCSS = $('styleCSS').text();

		var divContents = $(".print_content").html();
		var printWindow = window.open('', 'Financiacion', 'height=400,width=800,status=0,toolbar=0');
		printWindow.document.write('<title>Financiacion al <?php echo date('d/m/Y H:i').' hs';?></title>');
		printWindow.document.write('<style>'+printCSS+'</style>');
		printWindow.document.write(divContents);
		printWindow.document.close();
		printWindow.print();
	});

		
		
	function formatNumber (num) {
		return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")
	}
	
	$(".fa-close").on("click", function () {
		$("#origin").val("");
		$('.calculate_final').each(function( index ) {
			$(this).html("");
		});

		$('.calculate_cuota').each(function( index ) {
			$(this).html("");
		});

		$('#calcular').attr('disabled',true);
		$('#btnPrint').attr('disabled',true);
		$('#initial_price').html("");
		$('#final_price').html("");
	});
		
	$("#origin").keypress(function(event) {
		if (event.which == 13) {
			event.preventDefault();
			$("#calcular").trigger( "click" );
		}
	});
		
	function validateQty(event) {
		var key = window.event ? event.keyCode : event.which;

	if (event.keyCode == 8 || event.keyCode == 46  || event.keyCode == 13 || event.keyCode == 37 || event.keyCode == 39) {
		return true;
	}
	else if ( key < 48 || key > 57 ) {
		return false;
	}
	else return true;
	};
</script>