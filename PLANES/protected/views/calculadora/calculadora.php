<?php ?>
<link rel="stylesheet" href="<?=Yii::app()->baseUrl?>/css/calculadora.css">

<section class="content-header">
	<?php echo Yii::app()->user->Title(); ?>
	<?php echo Yii::app()->user->Breadcrumb(); ?> 
</section>

<section class="content cotizador">
	<div class="box box-danger">
		<div id="volver" class="box-header with-border">
			<div class="col-xxs-12 col-xs-4 col-lg-3 search_bar">
				<div class="row">
					<div class="input-group input-group-sm">
						<input type="text" class="form-control criterio" id="origin" onkeypress="return validateQty(event);" placeholder="(sin el 6%)" /><i class="fa fa-close right-icon-search"></i>
						<span class="input-group-btn">
						  <button type="submit" id="calcular" title="Calcular" class="btn btn-default btn-flat"><i class="fa fa-calculator"></i></button>
						</span>
					</div>
				</div>
			</div>
			<div class="label-float">Cuotas:</div>
			<div class="col-xxs-3 col-xs-3 col-sm-2 cuotas">
				<select class="form-control select_cuotas">
				  <option value="">Todas</option>
				  <?php foreach($cuotas as $indice => $cuota){ 
						//$palabra="Cuota";
						//if($indice>1)$palabra="Cuotas";
						echo '<option value="'.$indice.'">'.$indice.'</option>';
				   } ?>
				  
				</select>
			</div>
			<button class="btn btn-success a_customize" id="btnPrint"><i class="fa fa-download i_customize"></i>Descargar PDF</button>

			<div class="row parametros parametros-active">	
				<p class="col-xs-7">Precio Inicial:<br /><span id="initial_price">&nbsp;</span> <span class="leyenda">(sin el 6%)</span></p>
				<p class="col-xs-5">Precio Final:<br /><span id="final_price">&nbsp;</span></p>
			</div>

			<div class="print_content">
				<div class="row parametros printView">	
					<p class="printPrice">Precio Final: <span id="final_price_print">&nbsp;</span></p>
				</div>
				<div class="bancos">
					<?php
						$cant_legales=0;
						if(!empty($array_financing)){ 
							foreach($array_financing as $key => $entidad){

								$keyID = urlencode(str_replace(' ','',strtolower($key)));
								if(!empty($entidad['imagen'])){
									$imagen = $entidad['imagen'];
								}

								if(!empty($banco['entity_highlight']) && $banco['entity_highlight'] == 1){
									$bancoExclusivo = 'bancoExclusivo';
									$titleExclusivo = '<p class="exclusivo">Exclusivo</p>';
								}else{
									$bancoExclusivo = '';
									$titleExclusivo = '';
								} ?>

								<article id="mini-<?=$keyID?>" class="miniView miniView1 <?=$bancoExclusivo?>">
									<a class="maximizar fa fa-plus"></a>
									<a class="caja" title="<?=$key?>">
										<img src="<?=$imagen?>" alt="<?=$key?>" title="<?=$key?>" />
										<?=$titleExclusivo?>
										<?php
											end($entidad['cuotas']);											
											$cuotasVar = key($entidad['cuotas']);
											$interesVar = key($entidad['cuotas'][$cuotasVar]['interes']);
										?>
										<p class="cuotas">Hasta <strong><?=$cuotasVar?> cuotas</strong> <br />de <span class="calculate_cuota" data-cuotas="<?=$cuotasVar?>" data-financiacion="<?=$interesVar?>"></span></p>
										<p class="precioFinal">P.Final: <span class="calculate_final" data-cuotas="<?=$cuotasVar?>" data-financiacion="<?=$interesVar?>"></span> / C.Fin.: <span><?=$interesVar.'%'?></span></p>
									</a>
								</article>

								<div id="table-<?=$keyID?>" class="grid-view tableView tableView1">
									<a class="minimizar fa fa-minus"></a>
									<div class="table-responsive">
										<table class="table table-bordered table-striped table-list">
											<thead>
												<!--<tr><img src="<?=$imagen?>" alt="<?=$key?>" title="<?=$key?>" /></tr>-->
												<tr>
													<!--<th class="tarjetas">Tarjetas <img src="<?=$imagen?>" alt="<?=$key?>" title="<?=$key?>" /></th>
													<th>Cuotas</th>-->
													<th><img src="<?=$imagen?>" alt="<?=$key?>" title="<?=$key?>" /></th>
													<th>Valor Cuota</th>
													<th>Precio Final</th>
													<th>Costo Financ.</th>
													<!--<th>Legales</th>-->
												</tr>
											</thead>
											<tbody>
												<?php if(isset($entidad['cuotas']) && is_array($entidad['cuotas'])){
													foreach ($entidad['cuotas'] as $cuota=>$intereses){
														//echo '<pre>'; var_dump($entidad);die;
														foreach ($intereses['interes'] as $interes=>$tarjetas){ ?>

															<tr>
																<?php 
																	$kk = $entidad['cuotas'][$cuota]['legal'] +1;
																?>
																<!--<td class="tarjetas">
																	<?php foreach ($tarjetas as $indice=>$tarjeta){
																		if($indice>0) echo'<br />'; echo $tarjeta;
																	}?>
																</td>-->
																<td class="cuotas"><a href="#legal-<?=$key?>-<?=$kk?>" title="Ver legales"><?=$cuota?><sup class="legal"><?='('.$kk.')'?></sup></a></td>
																<td>
																	<div class="calculate_cuota" data-cuotas="<?=$cuota?>" data-financiacion="<?=$interes?>"></div>
																</td>
																<td>
																	<div class="calculate_final" data-cuotas="<?=$cuota?>" data-financiacion="<?=$interes?>"></div>
																</td>
																<td>
																	<div><?=$interes.'%'?></div>
																</td>
															</tr>
														<?php }				
													}
												} ?>
											</tbody>
										</table>
									</div>
								</div>
							<?php }
						}else{  
							echo'<p>Ocurri� un error procesando las financiaciones. Comuniquese con el administrador.</p>';
						}
					?> 
				</div>
			</div>	
			
			<div class="print_content2">				
				<div class="bancos">
					<?php
						$cant_legales=0;
						if(!empty($array_financing)){ 
							foreach($array_financing as $key => $entidad){

								$keyID = urlencode(str_replace(' ','',strtolower($key)));
								if(!empty($entidad['imagen'])){
									$imagen = $entidad['imagen'];
								}

								if(!empty($banco['entity_highlight']) && $banco['entity_highlight'] == 1){
									$bancoExclusivo = 'bancoExclusivo';
									$titleExclusivo = '<p class="exclusivo">Exclusivo</p>';
								}else{
									$bancoExclusivo = '';
									$titleExclusivo = '';
								} ?>

								<article id="mini2-<?=$keyID?>" class="miniView miniView2<?=$bancoExclusivo?>">
									<a class="maximizar fa fa-plus"></a>
									<a class="caja" title="<?=$key?>">
										<img src="<?=$imagen?>" alt="<?=$key?>" title="<?=$key?>" />
										<?=$titleExclusivo?>
										<?php
											end($entidad['cuotas']);											
											$cuotasVar = key($entidad['cuotas']);
											$interesVar = key($entidad['cuotas'][$cuotasVar]['interes']);
										?>
										<p class="cuotas"><strong class="cuota_fixed"><?=$cuotasVar?> cuotas</strong> <br />de <span class="calculate_cuota" data-cuotas="<?=$cuotasVar?>" data-financiacion="<?=$interesVar?>"></span></p>
										<p class="precioFinal">P.Final: <span class="calculate_final" data-cuotas="<?=$cuotasVar?>" data-financiacion="<?=$interesVar?>"></span> / C.Fin.: <span class="costo-financiero"><?=$interesVar.'%'?></span></p>
									</a>
								</article>

								<div id="table2-<?=$keyID?>" class="grid-view tableView tableView2">
									<a class="minimizar fa fa-minus"></a>
									<div class="table-responsive">
										<table class="table table-bordered table-striped table-list">
											<thead>
												<!--<tr><img src="<?=$imagen?>" alt="<?=$key?>" title="<?=$key?>" /></tr>-->
												<tr>
													<!--<th class="tarjetas">Tarjetas <img src="<?=$imagen?>" alt="<?=$key?>" title="<?=$key?>" /></th>
													<th>Cuotas</th>-->
													<th><img src="<?=$imagen?>" alt="<?=$key?>" title="<?=$key?>" /></th>
													<th>Valor Cuota</th>
													<th>Precio Final</th>
													<th>Costo Financ.</th>
													<!--<th>Legales</th>-->
												</tr>
											</thead>
											<tbody>
												<?php if(isset($entidad['cuotas']) && is_array($entidad['cuotas'])){
													foreach ($entidad['cuotas'] as $cuota=>$intereses){
														
														foreach ($intereses['interes'] as $interes=>$tarjetas){ ?>
																<?php 
																	$kk = $entidad['cuotas'][$cuota]['legal'] +1;
																?>

															<tr class="<?php echo $keyID.'-'.$cuota?>" data-interes="<?=$interes?>">
																<!--<td class="tarjetas">
																	<?php foreach ($tarjetas as $indice=>$tarjeta){
																		if($indice>0) echo'<br />'; echo $tarjeta;
																	}?>
																</td>-->
																<td class="cuotas"><a href="#legal-<?=$key?>-<?=$kk?>" title="Ver legales"><?=$cuota?><sup class="legal"><?='('.$kk.')'?></sup></a></td>
																<td>
																	<div class="calculate_cuota" data-cuotas="<?=$cuota?>" data-financiacion="<?=$interes?>"></div>
																</td>
																<td>
																	<div class="calculate_final" data-cuotas="<?=$cuota?>" data-financiacion="<?=$interes?>"></div>
																</td>
																<td>
																	<div><?=$interes.'%'?></div>
																</td>
															</tr>
														<?php }				
													}
												} ?>
											</tbody>
										</table>
									</div>
								</div>
							<?php }
						}else{  
							echo'<p>Ocurri� un error procesando las financiaciones. Comuniquese con el administrador.</p>';
						}
					?> 
				</div>
			</div>	
			
			
			
				<div class="legales legalesView">
					<h2 class="legal3">Legales</h2>
					<?php	
						$i_legales = 1;
						if(!empty($legales)){ 
							foreach($legales as $entidad => $legales_array){
									$legalID = urlencode(str_replace(' ','',strtolower($entidad)));
									echo '<div class="legal2 legal-'.$legalID.'">';
									foreach($legales_array as $kk => $legal){
										$banco = $entidad;
										$i_legales = $kk+1;
										echo '<h3 id="legal-'.$entidad.'-'.$i_legales.'">'.$banco.'<sup class="legal">('.$i_legales.')</sup></h3>';
										echo '<p>'.$legal.'</p>';
									}
									echo '</div>';
								}
							}
						
					?> 
				</div>
			
			<a class="fa fa-arrow-up volver" href="#volver" title="Ir arriba"></a>
			<a class="fa fa-table tablesBtn" title="Ver tablas"></a>
			<a class="fa fa-th-list listBtn" title="Var cuotas M�ximas"></a>
			<a class="fa fa-refresh refresh" title="Recargar"></a>
		</div>
	</div>
</section>
<styleCSS style="display: none;"><?=include('css/calculadora.print.css');?></styleCSS>

<script>
	$( document ).ready(function() {

		$(".parametros-active").css('display','none');
		//$('.select_cuotas').attr('disabled',true);
		$('#calcular').attr('disabled',true);
		$('#btnPrint').attr('disabled',true);
		$('#origin').keyup(function(){
	        if($(this).val().length !=0){
				$('#calcular').attr('disabled',false);
				$('#btnPrint').attr('disabled',false);
				//$('.select_cuotas').attr('disabled',false);
			}          
	        else{
				$('#calcular').attr('disabled',true);
				$('#btnPrint').attr('disabled',true);
			}
	    })
		
		$("#calcular").click(function(){
			var origin= $('#origin').val();
			var final_price = Math.round(origin/0.94);
			$('#initial_price').html('$'+formatNumber(origin));
			$('#final_price').html('$'+formatNumber(final_price));
			$('#final_price_print').html('$'+formatNumber(final_price));

			$('.calculate_final').each(function( index ) {
				var_cuota=$(this).data( "cuotas" );
				var_interes=$(this).data( "financiacion" );
				$(this).html('$'+formatNumber(Math.round((final_price+(final_price*var_interes/100)))));
			});

			$('.calculate_cuota').each(function( index ) {
				var_cuota=$(this).data( "cuotas" );
				var_interes=$(this).data( "financiacion" );
				cuotasValue = (final_price+(final_price*var_interes/100))/var_cuota;
				$(this).html('$'+formatNumber(Math.round(cuotasValue)));
				//$(this).html('$'+cuotasValue.formatMoney(0, ',', '.'));
			});
			
			$(".parametros-active").css('display','block');
		});

	});

	
	
	$("#btnPrint").on("click", function () {
		var printCSS = $('styleCSS').text();

		var divContents = $(".print_content").html();
		var legalContents = $(".legalesView").html();
		
		var imprimir_de_la_mejor_manera_un_pdf=false;
		if(imprimir_de_la_mejor_manera_un_pdf){		/*
		var printWindow = window.open('', 'Financiacion', 'height=400,width=800,status=0,toolbar=0');
		printWindow.document.write('<title>Financiacion al <?php echo date('d/m/Y H:i').' hs';?></title>');
		printWindow.document.write('<style>'+printCSS+'</style>');
		printWindow.document.write(divContents);
		printWindow.document.close();
		printWindow.print();*/
		}else{
		var content = JSON.stringify(divContents);
		var style = JSON.stringify(printCSS);
		var legal = JSON.stringify(legalContents);
		var url= "<?=Yii::app()->createUrl(strtolower(Yii::app()->controller->id))?>"+"/imprimir";
		
		var Base64={_keyStr:"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",encode:function(e){var t="";var n,r,i,s,o,u,a;var f=0;e=Base64._utf8_encode(e);while(f<e.length){n=e.charCodeAt(f++);r=e.charCodeAt(f++);i=e.charCodeAt(f++);s=n>>2;o=(n&3)<<4|r>>4;u=(r&15)<<2|i>>6;a=i&63;if(isNaN(r)){u=a=64}else if(isNaN(i)){a=64}t=t+this._keyStr.charAt(s)+this._keyStr.charAt(o)+this._keyStr.charAt(u)+this._keyStr.charAt(a)}return t},decode:function(e){var t="";var n,r,i;var s,o,u,a;var f=0;e=e.replace(/[^A-Za-z0-9+/=]/g,"");while(f<e.length){s=this._keyStr.indexOf(e.charAt(f++));o=this._keyStr.indexOf(e.charAt(f++));u=this._keyStr.indexOf(e.charAt(f++));a=this._keyStr.indexOf(e.charAt(f++));n=s<<2|o>>4;r=(o&15)<<4|u>>2;i=(u&3)<<6|a;t=t+String.fromCharCode(n);if(u!=64){t=t+String.fromCharCode(r)}if(a!=64){t=t+String.fromCharCode(i)}}t=Base64._utf8_decode(t);return t},_utf8_encode:function(e){e=e.replace(/rn/g,"n");var t="";for(var n=0;n<e.length;n++){var r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r)}else if(r>127&&r<2048){t+=String.fromCharCode(r>>6|192);t+=String.fromCharCode(r&63|128)}else{t+=String.fromCharCode(r>>12|224);t+=String.fromCharCode(r>>6&63|128);t+=String.fromCharCode(r&63|128)}}return t},_utf8_decode:function(e){var t="";var n=0;var r=c1=c2=0;while(n<e.length){r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r);n++}else if(r>191&&r<224){c2=e.charCodeAt(n+1);t+=String.fromCharCode((r&31)<<6|c2&63);n+=2}else{c2=e.charCodeAt(n+1);c3=e.charCodeAt(n+2);t+=String.fromCharCode((r&15)<<12|(c2&63)<<6|c3&63);n+=3}}return t}}


		  var form='<form action="'+url+'" method="POST" id="calc_form">' + 
		'<input type="hidden" name="content" value="' + Base64.encode(content) + '">' +
		'<input type="hidden" name="style" value="' + Base64.encode(style) + '">' +
		'<input type="hidden" name="legal" value="' + Base64.encode(legal) + '">' +
		'</form>';
	
		$(document.body).append(form);
		$('#calc_form').submit();
	}});

		
	Number.prototype.formatMoney = function(c, d, t){
		var n = this, 
		    c = isNaN(c = Math.abs(c)) ? 2 : c, 
		    d = d == undefined ? "." : d, 
		    t = t == undefined ? "," : t, 
		    s = n < 0 ? "-" : "", 
		    i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))), 
		    j = (j = i.length) > 3 ? j % 3 : 0;
		return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
	};

	function formatNumber (num) {
		return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")
	}
	
	$(".fa-close").on("click", function () {
		$("#origin").val("");
		$('.calculate_final').each(function( index ) {
			$(this).html("");
		});

		$('.calculate_cuota').each(function( index ) {
			$(this).html("");
		});

		$('#calcular').attr('disabled',true);
		$('#btnPrint').attr('disabled',true);
		//$('.select_cuotas').attr('disabled',true);
		$('#initial_price').html("");
		$('#final_price').html("");
		$(".print_content").css('display','block');
		$(".print_content2").css('display','none');
		$(".parametros-active").css('display','none');
		$('.select_cuotas').eq(0).prop('selected', true);
		$(".select_cuotas").val("");
		$('.legal2').css('display','block');
		$("#calcular").trigger( "click" );
	});
		
	$("#origin").keypress(function(event) {
		if (event.which == 13) {
			event.preventDefault();
			$("#calcular").trigger( "click" );
		}
	});
		
	function validateQty(event) {
		var key = window.event ? event.keyCode : event.which;

		if (event.keyCode == 8 || event.keyCode == 46  || event.keyCode == 13 || event.keyCode == 37 || event.keyCode == 39) {
			return true;
		} else if ( key < 48 || key > 57 ) {
			return false;
		} else {
			return true;
		}
	};

	$(".tablesBtn").on("click", function () {
		var cuota_selected=$( ".select_cuotas option:selected" )[0].value;
		if(cuota_selected==0){
			$('.miniView1').css('display','none');
			$('.tableView1').css('display','block');
			$('.legalesView').css('display','block');
			$('.legal3').css('display','block');
			$('.legal2').css('display','block');
		}else{
			$('.miniView2').css('display','none');
			$('.tableView2').css('display','block');
			$("div[id*='table2-']").each(function(){
				var flag0=false;
			    $(this).find('tbody tr').each(function(){
					var flag=false;
					var clase= $(this).attr('class');
					if (typeof clase === 'undefined') {					 
					}else{
						var cuotaPos = clase.indexOf('-')
						var cuotaSelect = clase.substring(cuotaPos+1);
						if(cuotaSelect==cuota_selected) {flag=true;flag0=true;}
					}
					if(!flag){$(this).css('display','none');}else {$(this).css('display','table-row');}
				})
				if(!flag0){$(this).css('display','none');}else {$(this).css('display','block');}
			});
			
			$("div[id*='table2-']").each(function(){
				var entidadClick = $(this).prop('id');
				var entidadPos = entidadClick.indexOf('-')
				var entidadSelect = entidadClick.substring(entidadPos+1);
				if($(this).css('display') == 'block'){				
					legalesOcultar = false;
					$('.legal-'+entidadSelect).css('display','block');
				}else{
					$('.legal-'+entidadSelect).css('display','none');
				}
			});
			
			if(legalesOcultar != false){
				$('.legalesView').css('display','none');
				$('.legal3').css('display','none');
			}else{
				$('.legalesView').css('display','block');
				$('.legal3').css('display','block');
			}
			
		}
		
		//$('.legalesView').css('display','block');
		//$('.legal3').css('display','block');
		//$('.legal2').css('display','block');
	});

	$(".listBtn").on("click", function () {
		var cuota_selected=$( ".select_cuotas option:selected" )[0].value;
		if(cuota_selected==0 || cuota_selected=="" ){
			$(".print_content2").css('display','none');
			$(".print_content").css('display','block');
			$('.miniView1').css('display','block');
			$('.tableView1').css('display','none');
		}else{
			$(".print_content2").css('display','block');
			$(".print_content").css('display','none');
			$('.miniView2').css('display','block');
			$('.tableView2').css('display','none');
			
			$("div[id*='table2-']").each(function(){
				var flag0=false;
				var entidadClick = $(this).prop('id');
				var entidadPos = entidadClick.indexOf('-')
				var entidadActual = entidadClick.substring(entidadPos+1);
			    $(this).find('tbody tr').each(function(){
					var clase= $(this).attr('class');
					if (typeof clase === 'undefined') {					 
					}else{
						var cuotaPos = clase.indexOf('-');
						var cuotaSelect = clase.substring(cuotaPos+1);
						if(cuotaSelect==cuota_selected) {flag0=true; }
					}
				})
				if(!flag0){
						$('#mini2-'+entidadActual).css('display','none');
					}
					else 
					{
						$('#mini2-'+entidadActual).css('display','block');
					}
			});
		}
		$('.legalesView').css('display','none');
		$('.legal3').css('display','none');
		$('.legal2').css('display','none');
	});

	$("article[id*='mini-'] .maximizar").on("click", function () {
		entidadClick = $(this).parent().prop('id');
		entidadPos = entidadClick.indexOf('-')
		entidadSelect = entidadClick.substring(entidadPos+1);
		$('#'+entidadClick).css('display','none');
		$(".print_content").css('display','block');
		$(".print_content2").css('display','none');
		$('#table-'+entidadSelect).css('display','block');
		$('.legalesView').css('display','block');
		$('.legal3').css('display','block');
		$('.legal-'+entidadSelect).css('display','block');
	});

	$("div[id*='table-'] .minimizar").on("click", function () {
		entidadClick = $(this).parent().prop('id');
		entidadPos = entidadClick.indexOf('-')
		entidadSelect = entidadClick.substring(entidadPos+1);
		legalesOcultar = true;
		$('#'+entidadClick).css('display','none');
		$(".print_content").css('display','block');
		$(".print_content2").css('display','none');
		$('#mini-'+entidadSelect).css('display','block');
		$("div[id*='table-']").each(function(){
			if($(this).css('display') == 'block'){				
				legalesOcultar = false;
			}
		});
		if(legalesOcultar != false){
			$('.legalesView').css('display','none');
			$('.legal3').css('display','none');
		}
		$('.legal-'+entidadSelect).css('display','none');
	});

	
	/*2*/
	$("article[id*='mini2-'] .maximizar").on("click", function () {
		entidadClick = $(this).parent().prop('id');
		entidadPos = entidadClick.indexOf('-')
		entidadSelect = entidadClick.substring(entidadPos+1);
		$(".print_content2").css('display','block');
		$(".print_content").css('display','none');
		$('#'+entidadClick).css('display','none');
		$('#table2-'+entidadSelect).css('display','block');
		$('.legalesView').css('display','block');
		$('.legal3').css('display','block'); 
		$('.legal-'+entidadSelect).css('display','block'); 
		console.log(entidadSelect);
		var cuota_selected=$( ".select_cuotas option:selected" )[0].value;
		
		$('#table2-'+entidadSelect).find('tbody tr').each(function(){
			var flag=false;
			var clase= $(this).attr('class');
			if (typeof clase === 'undefined') {					 
			}else{
				var cuotaPos = clase.indexOf('-')
				var cuotaSelect = clase.substring(cuotaPos+1);
				if(cuotaSelect==cuota_selected) {flag=true;}
			}
			if(!flag){$(this).css('display','none');}else {$(this).css('display','table-row');}
		}) 
		
	});

	$("div[id*='table2-'] .minimizar").on("click", function () {
		legalesOcultar = true;
		entidadClick = $(this).parent().prop('id');
		entidadPos = entidadClick.indexOf('-')
		entidadSelect = entidadClick.substring(entidadPos+1);
		$(".print_content2").css('display','block');
		$(".print_content").css('display','none');
		$('#'+entidadClick).css('display','none');
		$('#mini2-'+entidadSelect).css('display','block');
		$("div[id*='table2-']").each(function(){
			if($(this).css('display') == 'block'){				
				legalesOcultar = false;
			}
		});
		if(legalesOcultar != false){
			$('.legalesView').css('display','none');
			$('.legal3').css('display','none');
		}
		$('.legal-'+entidadSelect).css('display','none');
		
	});
	
	$(".select_cuotas").on("change", function () {
		var final_price = Math.round(origin/0.94);
		//var cuota_selected=$(this).context.selectedIndex;
		var cuota_selected=$(this).context.value;
		var text_cuota_selected=$( ".select_cuotas option:selected" ).text();
		if(cuota_selected==0){
			$(".print_content2").css('display','none');
			$(".print_content").css('display','block');
		}else{
			if(cuota_selected==1){cuota='cuota';}else{cuota='cuotas';}
			$(".print_content").css('display','none');
			$(".print_content2").css('display','block');
			
			$("article[id*='mini2-'] .cuota_fixed").html(cuota_selected+' '+cuota);
			$("article[id*='mini2-'] .cuotas .calculate_cuota").data( "cuotas", cuota_selected );
			$.each($(".print_content2 .bancos .miniView .caja .cuotas .calculate_cuota"),function(i,l){
				
				var mini2_id= $(this).closest('article').attr('id');
				var mini2Pos = mini2_id.indexOf('-')
				var entidad = mini2_id.substring(mini2Pos+1);
				var caja= $("."+entidad+'-'+cuota_selected);
				if(caja.length > 0){
					$("article[id*='mini2-"+entidad+"']").css('display','block');
					$("article[id*='table2-"+entidad+"']").css('display','block');
					var interes = $("."+entidad+'-'+cuota_selected).data("interes");
					$(this).data( "cuotas", cuota_selected );
				 
					//$("article[id*='mini2-"+entidad+"'] .calculate_final").html('$'+formatNumber(Math.round((final_price+(final_price*interes/100)))/cuota_selected));
				 
					$("article[id*='mini2-"+entidad+"'] .calculate_final").data( "cuotas", cuota_selected );
					$("article[id*='mini2-"+entidad+"'] .calculate_final").data( "financiacion", interes );
					$("article[id*='mini2-"+entidad+"'] .calculate_cuota").data( "cuotas", cuota_selected );
					$("article[id*='mini2-"+entidad+"'] .calculate_cuota").data( "financiacion", interes );
					
					$("article[id*='mini2-"+entidad+"'] .costo-financiero").html(interes+'%');
				}else{
					$("article[id*='mini2-"+entidad+"']").css('display','none');
					$("article[id*='table2-"+entidad+"']").css('display','none');
				}
			});
			$("#calcular").trigger( "click" );
		}
		$(".listBtn").trigger( "click" );
	});
	
	$(".refresh").on("click", function () {
		location.reload(); 
	});
	
</script>