<section class="content-header">
	<h1>Configuración de imágenes</h1>
	<?php echo Yii::app()->user->Breadcrumbbyid($id_menu); ?>
	<p class="registros">(<?=$model->count();?> registros encontrados)</p>
</section>
<section class="content">
	<div class="box box-danger">
		<div class="box-header with-border">
			<?php $this->renderPartial('_form', array('model'=>$model,'id_menu'=>$id_menu,'menu'=>$menu)); ?>		</div>
	</div>
</section>


