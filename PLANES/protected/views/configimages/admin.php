<?php
/* @var $this ConfigimagesController */
/* @var $model Configimages */

Yii::app()->clientScript->registerScript('search', "
	deleteBtn();
	search();
	searchAdd();
");
?>
<section class="content-header">
	<h1>Configuración de imágenes</h1>
	<?php echo Yii::app()->user->Breadcrumbbyid($id_menu); ?>
	<p class="registros">(<?=$model->count();?> registros encontrados)</p>
</section>
<section class="content">
	<div class="box box-danger">
		<div class="box-header with-border">	
			<?php
				echo '<a href="'.Yii::app()->request->baseUrl.'/'.Yii::app()->controller->id.'/create/'.$id_menu.'"class="btn btn-default b_customize"><i class="fa fa-plus i_customize"></i>Agregar</a>';
			?>
			<?php echo Utils::SearchObject(); ?>	
			<?php echo CHtml::link('Búsqueda avanzada','#',array('class'=>'search-button')); ?>
			<div class="search-form" style="display:none">
			<?php $this->renderPartial('_search',array(
				'model'=>$model,
			)); ?>
			</div><!-- search-form -->

			<?php $this->widget('zii.widgets.grid.CGridView', array(
				'id'=>'configimages-grid',
				'pager' => array(
                    'cssFile'=>false,
                    'header'=> '',
                    'firstPageLabel' => '<i class="fa fa-forward fa-flip-horizontal" data-toggle="tooltip" title="Primera página"></i>',
			        'prevPageLabel'  => '<i class="fa fa-caret-right fa-flip-horizontal" data-toggle="tooltip" title="Anterior"></i>',
			        'nextPageLabel'  => '<i class="fa fa-caret-right" data-toggle="tooltip" title="Siguiente"></i>',
			        'lastPageLabel'  => '<i class="fa fa-forward" data-toggle="tooltip" title="Última página"></i>',
                ),	
                'afterAjaxUpdate'=> 'function(){
					deleteBtn();
					search();
					searchAdd();
                }',			
				'summaryText' => '', 
				'itemsCssClass' => 'table table-bordered table-striped table-list',					
				'dataProvider'=>$model->search($id_menu),
				'columns'=>array(		
					array(
						'name' => 'position',
						'type'=>'html',
						'value'=>function($data) {
							return '<span class="position_class " title="'.$data->position.'"><i class="fa fa-spinner fa-spin"></i></span>';
						},						
					),
					'width',
					'height',
					array(
						'class'=>'CButtonColumn',
						'template'=>'{view}{update}{erase}',	
						'buttons'=>array(		
							'view' => array(
								'label'=>'<i class="fa fa-eye icon_cbutton"></i>',
								'url'=>'Yii::app()->createUrl(strtolower(Yii::app()->controller->id).\'/view/\'. $data->id.\'/'.$id_menu.'\')',
								'options'=>array('data-toggle'=>'tooltip','title'=>'Ver'),
								'imageUrl'=>false,
							),						
							'update' => array(
								'label'=>'<i class="fa fa-edit icon_cbutton"></i>',
								'url'=>'Yii::app()->createUrl(strtolower(Yii::app()->controller->id).\'/update/\'. $data->id.\'/'.$id_menu.'\')',
								'options'=>array('data-toggle'=>'tooltip','title'=>'Editar'),
								'imageUrl'=>false,
							),					
							'erase' => array(
								'label'=>'<i class="fa fa-trash-o icon_cbutton"></i>',
								'url'=>'Yii::app()->createUrl(strtolower(Yii::app()->controller->id).\'/delete/\'. $data->id.\'/'.$id_menu.'\')',
								'options' => array(
									'class' => 'delete_action',
									'title'=>'Borrar',
									'imageUrl'=>false,
									'data-toggle'=>'tooltip',
								)	
							),
						),
					),
				)
			)); ?>
		</div>
	</div>
 </section>
 <script>
$( document ).ready(function() {
	$.ajax({
	  method: "POST",
	  url: homeurl+"/<?=$menu->url?>/create",
	}).done(function( msg ) {
		var options_select=[];
		var i=1;
		$(msg).find(".label_image").each(function() {
			label=new Object;
			label.id=i;
			label.name=$(this).html();
			options_select.push(label);
			i++;
		});	
		$(".position_class").each(function() {
			actual=$(this);
			$.each(options_select, function(index, value) {	
				if(actual.attr('title')==value.id){				
					actual.html(value.name);
				}
			});	
		});	
	});		
});	
</script>