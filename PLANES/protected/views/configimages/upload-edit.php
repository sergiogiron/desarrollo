<form method="post" id="upload_data">
<div class="col-md-12" id="data-bar">	
	<div class="row">
	<div class="input_datas">
		<input type="hidden" name="position" id="position" value="<?=$position?>"/>
		<span  <?php if($config_images[0]->title_required==1){ echo 'class="required"'; }?>>
			<label style="margin-right: 5px;">Título</label>
			<input type="text" name="title" class="input_lgh letras_numeros" id="title" data-required="<?=$config_images[0]->title_required?>" value="<?=$data['title']?>"/>
			<div class="error_color_ms" id="error_title">Debe completar el título</div>
		</span>
		<span  <?php if($config_images[0]->link_required==1){ echo 'class="required"'; }?>>
			<label style="margin-top: 6px;">Enlace</label>
			<div class="error_color_ms" id="error_link"></div>
			<div class="input-group input_group_c" >	
				<input type="text" class="form-control" name="link" id="link" data-required="<?=$config_images[0]->link_required?>" value="<?=$data['link']?>">		
				<div class="input-group-addon"><i class="fa fa-globe"></i></div>						
			</div>			
		</span>
		<span>
			<input type="button" style="margin-left: 10px;" id="fin_upload" class="btn btn-primary " data-toggle="tooltip" title="Finalizar carga de imágenes" value="Finalizar" />
			<span class="image_size_label" id="image_size"></span>
		</span>
	</div>
	</div>
</div>
<div class="col-md-12">
	<div class="row">
	<div class="col-md-2 f1">		
		<input type="hidden" name="file_selected" id="file_selected" value="<?=$data['name']?>" />
		<input type="hidden" name="ext" id="ext" value="<?=$data['ext']?>" />
		<?php
		$i=0;
		$width=0;		
		$height=0;		
		foreach($data['coords'] as $ci){
			if($i==0){
				$id_f="first_selected";
			}else{
				$id_f='';	
			}			
			echo '<div data-activo="'.$i.'" class="image_size" id="'.$id_f.'">'.$ci['width'].'x'.$ci['height'].'
			<input type="hidden" name="'.$i.'-x" class="'.$i.'-x" value="'.$ci['x'].'" />
			<input type="hidden" name="'.$i.'-x2" class="'.$i.'-x2" value="'.$ci['x2'].'" />
			<input type="hidden" name="'.$i.'-y" class="'.$i.'-y" value="'.$ci['y'].'" />
			<input type="hidden" name="'.$i.'-y2" class="'.$i.'-y2" value="'.$ci['y2'].'" />
			<input type="hidden" name="'.$i.'-w" class="'.$i.'-w" value="'.$ci['w'].'" />
			<input type="hidden" name="'.$i.'-h" class="'.$i.'-h" value="'.$ci['h'].'" />
			<input type="hidden" name="'.$i.'-width" class="'.$i.'-width" value="'.$ci['width'].'" />
			<input type="hidden" name="'.$i.'-height" class="'.$i.'-heigth" value="'.$ci['height'].'" />			
			</div>';
			$i++;
		}
		$imagen_size = getimagesize('http://'.$_SERVER['HTTP_HOST'].Yii::app()->request->baseUrl."/uploads/tmp/".Yii::app()->user->id."/".$data['name']."/".$data['name'].".".$data['ext']);
		if($imagen_size[0]>$imagen_size[1]){
			if($imagen_size[1]<500){
				$centrado="width";
			}else{
				$centrado="height";
			}					
		}else{					
			$centrado="height";
		}			
		?>
		
	</div>
	</form>
	<div class="col-md-10 f2">
		<div class="result_image" <?php if($data['ext']=='png'){ echo ' style=" background: url('.Yii::app()->request->baseUrl.'/img/fondo_png.jpg);" ';}?>>
			<img <?=$centrado?>="100%" id="image_jcrop" src="<?php echo Yii::app()->request->baseUrl; ?>/uploads/tmp/<?=Yii::app()->user->id?>/<?=$data['name']?>/<?=$data['name']?>.<?=$data['ext']?>" />
		</div>
	</div>
	</div>
</div>	
<?php
	Yii::app()->clientScript->registerCssFile(Yii::app()->params['cdnUrl'].'/css/jquery.Jcrop.css');
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/js/jquery.Jcrop.js',CClientScript::POS_END); 
	Yii::app()->clientScript->registerCssFile(Yii::app()->params['cdnUrl'].'/css/dropzone.css'); 	
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/js/jquery.bxslider.js',CClientScript::POS_END); 
	Yii::app()->clientScript->registerCssFile(Yii::app()->params['cdnUrl'].'/css/jquery.bxslider.css');
 ?>
<script>
activo=0;
id_menu=<?=$id_menu?>;
function showCoords(c){
	$('.'+activo+'-x').val(c.x);
	$('.'+activo+'-y').val(c.y);
	$('.'+activo+'-x2').val(c.x2);
	$('.'+activo+'-y2').val(c.y2);
	$('.'+activo+'-w').val(c.w);
	$('.'+activo+'-h').val(c.h);	
};
$( document ).ready(function() {		
	$("#fin_upload").click(function() {
		var error=0;
		if($('#title').attr('data-required')=='1'){			
			if($('#title').val()==''){
				$('#error_title').css('display','block');
				error=1;				
			}else{
				$('#error_title').css('display','none');
			}				
		}	
		if($('#link').attr('data-required')=='1'){
			if(validateURL($('#link').val())){
				if($('#link').val()==''){
					$('#error_link').html('Debe completar el enlace');
					$('#error_link').css('display','block');
					error=1;				
				}else{
					$('#error_link').css('display','none');
				}
			}else{
				error=1;
				$('#error_link').html('Error de formato');
				$('#error_link').css('display','block');
			}			
		}	
		if(error==0){
			var imagen=$('#file_selected').val();
			$.ajax({
				method: "POST",
				url: homeurl+"/configimages/upload/id_menu/"+id_menu+"/position/<?=$position?>",
				data:$("#upload_data").serialize(),
			}).done(function( msg ){	
				msg=JSON.parse(msg);
				$('#'+msg.name+'_slide').html('');
				$('#'+msg.name).remove();
				$('#'+msg.name+'_slide').attr('title', "Titulo:"+$('#title').val()+" Enlace:"+$('#link').val());
				$('#'+msg.name+'_slide').html("<i class='fa fa-edit edit_img_i' data-id='"+msg.name+"' data-position='<?=$position?>' onclick='edit_element($(this))'></i><i class='fa fa-times delete_img_i' data-position='<?=$position?>' data-id="+msg.name+" onclick='delete_element($(this))'></i><img src='"+msg.url+"' ></li>");
				$('#data-d-content_<?=$position?>').append("<input type='hidden' id="+msg.name+" name='image[]' class='element_data' value='"+JSON.stringify(msg.json)+"'>");
				$('#content_result_img').html('');
				$('.ligth-box-modal-images').css('display','none');			
				$('.data-bar').css('display','none');			
				if(typeof(v_slider_<?=$position?>) !== 'undefined'){
					v_slider_<?=$position?>.destroySlider();				
				}
				v_slider_<?=$position?> = $('#slider_<?=$position?>').bxSlider({
					slideWidth: 250,
					minSlides: 4,
					maxSlides: 4,
					infiniteLoop: false,
					slideMargin:9
				});					
				$('.bslide_<?=$position?>').each(function( index, element ) {
					var id=$(this).find('i').attr('data-id');
					var json=JSON.parse($('#'+id).val());				
					json.order=index;
					$('#'+id).val(JSON.stringify(json));
				});				
			});			
		}
	});		
	$( ".image_size" ).click(function() {
			JcropAPI = $('#image_jcrop').data('Jcrop');
			JcropAPI.destroy();		
			self=$(this);
			activo=parseInt($(this).attr('data-activo'));
			$( ".image_size" ).removeClass('image_selected_jscrop').css('visibility','hidden').promise().done(function(){
			  self.addClass('image_selected_jscrop').css('visibility','visible').promise().done(function(){ 
					var width2  = jQuery('#image_jcrop').prop('naturalWidth');
					var height2 = jQuery('#image_jcrop').prop('naturalHeight');	
					$('#image_jcrop').Jcrop({
						trueSize: [width2,height2],
						setSelect: [parseInt($('.'+activo+'-x').val()),parseInt($('.'+activo+'-y').val()),parseInt($('.'+activo+'-x2').val()),parseInt($('.'+activo+'-y2').val())],
						onChange: showCoords,
						allowSelect : false,
						allowResize: false,
						onSelect: showCoords,

					},function() {
						$( ".image_size" ).css('visibility','visible');
					});					
			   }) ;
			}) ;	
	});
	$('#image_size').html('Tamaño ('+$('#image_jcrop').prop('naturalWidth')+'x'+$('#image_jcrop').prop('naturalHeight')+')');
});		
</script>