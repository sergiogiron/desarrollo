<?php
/* @var $this ConfigimagesController */
/* @var $model Configimages */
/* @var $form CActiveForm */
?>
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'configimages-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>
<div class="box-body">
	<span class="right_f">
	
	</span>


<div class="col-md-12 col-data">

	<div class="col-xs-6 form-group required">
		<?php echo $form->labelEx($model,'position'); ?>
		<i class="fa fa-spinner fa-spin loader_select"></i>
		<?php echo $form->dropDownList($model,'position', array(), array('empty'=>'Seleccione','class'=>'form-control','style'=>'display:none')) ?> 						
		<?php echo $form->error($model,'position'); ?>
	</div>
	<div class="col-xs-6 form-group required">
		<?php echo $form->labelEx($model,'width'); ?>
						<?php echo $form->textField($model,'width',array('size'=>5,'maxlength'=>5,'class'=>'form-control')); ?>
														<?php echo $form->error($model,'width'); ?>
	</div>
	<div class="col-xs-6 form-group required">
		<?php echo $form->labelEx($model,'height'); ?>
						<?php echo $form->textField($model,'height',array('size'=>5,'maxlength'=>5,'class'=>'form-control')); ?>
														<?php echo $form->error($model,'height'); ?>
	</div>
	<div class="col-xs-6 form-group required">
		<?php echo $form->labelEx($model,'transition_interval'); ?>
			<?php echo $form->textField($model,'transition_interval',array('size'=>5,'maxlength'=>5,'class'=>'form-control')); ?>
			<?php echo $form->error($model,'transition_interval'); ?>
	</div>	
	<div class="col-xs-6 form-group required">
		<?php echo $form->labelEx($model,'title_required'); ?>
			<?php echo $form->checkbox($model,'title_required'); ?>
			<?php echo $form->error($model,'title_required'); ?>
	</div>	
	<div class="col-xs-6 form-group required">
		<?php echo $form->labelEx($model,'link_required'); ?>
			<?php echo $form->checkbox($model,'link_required'); ?>
			<?php echo $form->error($model,'link_required'); ?>
	</div>	
	<div class="box-footer">
		<?php
			$title = (Yii::app()->controller->action->id!='view') ? 'Volver sin guardar cambios' : 'Volver';
			echo '<a href="'.Yii::app()->request->baseUrl.'/'.strtolower(Yii::app()->controller->id).'/'.$id_menu.'" class="btn btn-secundary" data-toggle="tooltip" title="'.$title.'" >Cerrar</a>';
			if(Yii::app()->controller->action->id!='view'){
				echo CHtml::submitButton('Guardar',array('class'=>'btn btn-primary','data-toggle'=>'tooltip','title'=>'Guardar cambios'));
			}
		?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
</div><!-- form -->
<script>
$( document ).ready(function() {
	$.ajax({
	  method: "POST",
	  url: homeurl+"/<?=$menu->url?>/create",
	}).done(function( msg ) {
		var options_select='';
		var i=1;
		$(msg).find(".label_image").each(function() {
			options_select+="<option value="+i+">"+$(this).html()+"</option>";
			i++;
		});	
		$('#Configimages_position').append(options_select);
		$('.loader_select').css('display','none');
		$('#Configimages_position').css('display','block');
	});			
	<?php if(Yii::app()->controller->action->id=='view'){ echo "$('form').find('input, textarea, button, checkbox, select').attr('disabled','disabled');"; } ?>
});		
</script>