<?php
/* @var $this ConfigimagesController */
/* @var $model Configimages */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>
<div class="col-md-12" style="clear: both">
<div class="row">		
	
			<div class="col-md-3">
				<div class="row row_space">
					<?php echo $form->label($model,'position'); ?>
					<?php echo $form->textField($model,'position',array('size'=>60,'maxlength'=>64,'class'=>'form-control')); ?>
				</div>
			</div>
			
			<div class="col-md-3">
				<div class="row row_space">
					<?php echo $form->label($model,'width'); ?>
					<?php echo $form->textField($model,'width',array('size'=>5,'maxlength'=>5,'class'=>'form-control')); ?>
				</div>
			</div>
			
			<div class="col-md-3">
				<div class="row row_space">
					<?php echo $form->label($model,'height'); ?>
					<?php echo $form->textField($model,'height',array('size'=>5,'maxlength'=>5,'class'=>'form-control')); ?>
				</div>
			</div>
</div>
	<div class="row buttons">
		<?php echo CHtml::submitButton('Buscar',array('class'=>'btn btn-default b_customize')); ?>
	</div>
<br />
<?php $this->endWidget(); ?>
</div>
</div><!-- search-form -->