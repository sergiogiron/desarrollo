<?php
/* @var $this ConfigimagesController */
/* @var $data Configimages */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_model')); ?>:</b>
	<?php echo CHtml::encode($data->id_model); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('position')); ?>:</b>
	<?php echo CHtml::encode($data->position); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('width')); ?>:</b>
	<?php echo CHtml::encode($data->width); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('height')); ?>:</b>
	<?php echo CHtml::encode($data->height); ?>
	<br />


</div>