<form method="post" id="upload_data">
<div class="col-md-12" style="display:none" id="data-bar">	
	<div class="row">
	<div class="input_datas">
		<input type="hidden" name="position" id="position" value="<?=$position?>"/>	
		<span  <?php if($config_images[0]->title_required==1){ echo 'class="required"'; }?>>
			<label style="margin-right: 5px;">Título</label>
			<input type="text" name="title" class="input_lgh letras_numeros" id="title" data-required="<?=$config_images[0]->title_required?>"/>			
			<div class="error_color_ms" id="error_title">Debe completar el título</div>
		</span>
		<span   <?php if($config_images[0]->link_required==1){ echo 'class="required"'; }?>>
			<label style="margin-top: 6px;">Enlace</label>
			<div class="error_color_ms" id="error_link"></div>
			<div class="input-group input_group_c" >		
				<input type="text" class="form-control" name="link" id="link" data-required="<?=$config_images[0]->link_required?>" />		
				<div class="input-group-addon"><i class="fa fa-globe"></i></div>						
			</div>			
		</span>
		<span>
			<input type="button" style="margin-left: 10px;" id="fin_upload" class="btn btn-primary " data-toggle="tooltip" title="Finalizar carga de imágenes" value="Finalizar" />
			<span class="image_size_label" id="image_size"></span>
		</span>
	</div>
	</div>
</div>
<div class="col-md-12">
	<div class="row">
	<div class="col-md-2 f1" style="display:none;">		
		<input type="hidden" name="file_selected" id="file_selected" value="">
		<input type="hidden" name="ext" id="ext" />
		<?php
		$i=0;
		$width=0;		
		$height=0;	
		foreach($config_images as $ci){
			if($i==0){
				$id_f="first_selected";
			}else{
				$id_f='';	
			}
			echo '<div data-activo="'.$i.'" class="image_size" id="'.$id_f.'">'.$ci->width.'x'.$ci->height.'
			<input type="hidden" name="'.$i.'-x" class="'.$i.'-x" value="0" />
			<input type="hidden" name="'.$i.'-x2" class="'.$i.'-x2" value="'.$ci->width.'" />
			<input type="hidden" name="'.$i.'-y" class="'.$i.'-y" value="0" />
			<input type="hidden" name="'.$i.'-y2" class="'.$i.'-y2" value="'.$ci->height.'" />
			<input type="hidden" name="'.$i.'-w" class="'.$i.'-w" value="'.$ci->width.'" />
			<input type="hidden" name="'.$i.'-h" class="'.$i.'-h" value="'.$ci->height.'" />
			<input type="hidden" name="'.$i.'-width" class="'.$i.'-width" value="'.$ci->width.'" />
			<input type="hidden" name="'.$i.'-height" class="'.$i.'-height" value="'.$ci->height.'" />
			</div>';
			$i++;
		}
		?>
		
	</div>
	</form>
	<div class="col-md-12 f2">
		<form class="dropzone needsclick dz-clickable" id="dropzone">
		  <div class="dz-message needsclick">
			Arrastra los archivos aca o clickea para subir un archivo.<br>
			<span class="note needsclick">(No hay ningun archivo seleccionado.)</span>
		  </div>
		</form>
	</div>
	</div>
</div>	
<?php
	Yii::app()->clientScript->registerCssFile(Yii::app()->params['cdnUrl'].'/css/jquery.Jcrop.css');
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/js/jquery.Jcrop.js',CClientScript::POS_END); 
	Yii::app()->clientScript->registerCssFile(Yii::app()->params['cdnUrl'].'/css/dropzone.css'); 	
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/js/form-validators.js',CClientScript::POS_END); 
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/js/jquery.bxslider.js',CClientScript::POS_END); 
	Yii::app()->clientScript->registerCssFile(Yii::app()->params['cdnUrl'].'/css/jquery.bxslider.css');
 ?>
<script>
activo=0;
function showCoords(c){
	$('.'+activo+'-x').val(c.x);
	$('.'+activo+'-y').val(c.y);
	$('.'+activo+'-x2').val(c.x2);
	$('.'+activo+'-y2').val(c.y2);
	$('.'+activo+'-w').val(c.w);
	$('.'+activo+'-h').val(c.h);
};
$( document ).ready(function() {
	$("#fin_upload").click(function() {
		var error=0;
		if($('#title').attr('data-required')=='1'){		
			if($('#title').val()==''){
				$('#error_title').css('display','block');
				error=1;				
			}else{
				$('#error_title').css('display','none');
			}				
		}	
		if($('#link').attr('data-required')=='1'){
			if(validateURL($('#link').val())){
				if($('#link').val()==''){
					$('#error_link').html('Debe completar el enlace');
					$('#error_link').css('display','block');
					error=1;				
				}else{
					$('#error_link').css('display','none');
				}
			}else{
				error=1;
				$('#error_link').html('Error de formato');
				$('#error_link').css('display','block');
			}			
		}	
		if(error==0){
			var imagen=$('#file_selected').val();
			$.ajax({
				method: "POST",
				url: homeurl+"/configimages/upload/id_menu/<?=$id_menu?>/position/<?=$position?>",
				data:$("#upload_data").serialize(),
			}).done(function( msg ){
				msg=JSON.parse(msg);		
				$('#slider_<?=$position?>').append("<li class='slide bslide_<?=$position?>' id='"+msg.name+"_slide'><i class='fa fa-edit edit_img_i' data-id="+msg.name+" data-position='<?=$position?>' onclick='edit_element($(this))'></i><i class='fa fa-times delete_img_i' data-id="+msg.name+" data-position='<?=$position?>' onclick='delete_element($(this))'></i><img src='"+msg.url+"' ></li>");
				$('#data-d-content_<?=$position?>').append("<input type='hidden' id="+msg.name+" name='image[]' class='element_data' value='"+JSON.stringify(msg.json)+"'>");
				$('#content_result_img').html('');
				$('.ligth-box-modal-images').css('display','none');			
				$('.data-bar').css('display','none');			
				if(typeof(v_slider_<?=$position?>) !== 'undefined'){
					v_slider_<?=$position?>.destroySlider();				
				}
				v_slider_<?=$position?> = $('#slider_<?=$position?>').bxSlider({
					slideWidth: 250,
					minSlides: 4,
					maxSlides: 4,
					infiniteLoop: false,
					slideMargin:9
				});					
				$('.slide').each(function( index, element ) {
					var id=$(this).find('i').attr('data-id');
					var json=JSON.parse($('#'+id).val());				
					json.order=index;
					$('#'+id).val(JSON.stringify(json));
				});				
			});			
		}
	});		
	$( ".image_size" ).click(function() {
			JcropAPI = $('#image_jcrop').data('Jcrop');
			JcropAPI.destroy();		
			self=$(this);
			activo=parseInt($(this).attr('data-activo'));
			$( ".image_size" ).removeClass('image_selected_jscrop').css('visibility','hidden').promise().done(function(){
			  self.addClass('image_selected_jscrop').css('visibility','visible').promise().done(function(){ 
					var width2  = jQuery('#image_jcrop').prop('naturalWidth');
					var height2 = jQuery('#image_jcrop').prop('naturalHeight');	
					$('#image_jcrop').Jcrop({
						trueSize: [width2,height2],
						setSelect: [parseInt($('.'+activo+'-x').val()),parseInt($('.'+activo+'-y').val()),parseInt($('.'+activo+'-x2').val()),parseInt($('.'+activo+'-y2').val())],
						onChange: showCoords,
						aspectRatio:parseInt($('.'+activo+'-w').val())/parseInt($('.'+activo+'-h').val()),
						minSize:[parseInt($('.'+activo+'-w').val()),parseInt($('.'+activo+'-h').val())],
						allowSelect : false,
					},function() {
						$( ".image_size" ).css('visibility','visible');
					});					
			   }) ;
			}) ;	
	});	
	Dropzone.autoDiscover = false;
	$("#dropzone").dropzone({
		url: homeurl+"/configimages/uploadfile/id_menu/<?=$id_menu?>/position/<?=$position?>",
		maxFilesize: 100,
		uploadMultiple:false,
		maxFiles:1,
		previewsContainer:false,
		paramName: "file",
		maxThumbnailFilesize: 5,
		init: function() { 
		  this.on('success', function(file, responseText) {
			result=JSON.parse(responseText);
			if(result[0].error){
				$('.ligth-box-modal-images').css('display','none');
				sweetAlert("No se pudo cargar la imagen", "Debe elegir una imagen mas grande", "error");
			}else{
				$( ".f2" ).html('');
				$('#file_selected').val(result[0].name);
				$('#ext').val(result[0].ext);
				$('#data-bar').css( "display","block" );
				$( ".f2" ).append('<div class="result_image"></div>');
				$( ".f2" ).removeClass( "col-md-12" );
				$( ".f2" ).addClass( "col-md-10" );
				$( ".f1" ).css( "display","block" );
				var elem = document.createElement("img");				
				if(result[0].width>result[0].height){
					if(result[0].height<500){
						elem.setAttribute("width", "100%");
					}else{
						elem.setAttribute("height", "100%");
					}					
				}else{					
					elem.setAttribute("height", "100%");
				}				
				elem.setAttribute("id", "image_jcrop");
				elem.setAttribute("src", homeurl+'/'+result[0].url);			
				$('.result_image').html(elem).promise().done(function(){
					$('#image_jcrop').Jcrop({
					},function() {
						$('#first_selected').trigger('click');				
						$('#image_size').html('Tamaño ('+$('#image_jcrop').prop('naturalWidth')+'x'+$('#image_jcrop').prop('naturalHeight')+')');
					});					  
				}) ;				
			}			  
		  });
		}
	});
});		
</script>