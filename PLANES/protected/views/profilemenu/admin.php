<?php
/* @var $this ProfilemenuController */
/* @var $model Profilemenu */

Yii::app()->clientScript->registerScript('search', "
	deleteBtn();
	search();
	searchAdd();
");
?>
<section class="content-header">
	<?php echo Yii::app()->user->Title(); ?>
	<?php echo Yii::app()->user->Breadcrumb(); ?>
	<p class="registros">(<?=$model->count();?> registros encontrados)</p>
</section>
<section class="content">
	<div class="box box-danger">
		<div class="box-header with-border">	
			<?php
			if(Yii::app()->user->checkAccess('create')){
				echo '<a href="'.Yii::app()->request->baseUrl.'/menues/crear"class="btn btn-default b_customize"><i class="fa fa-plus i_customize"></i> Agregar</a>';
			}
			?>				
			<?php echo Utils::SearchObject(); ?>			
			<?php echo CHtml::link('Búsqueda avanzada','#',array('class'=>'search-button')); ?>
			<div class="search-form" style="display:none">
			<?php $this->renderPartial('_search',array(
				'model'=>$model,
			)); ?>
			</div><!-- search-form -->
			<?php $this->widget('zii.widgets.grid.CGridView', array(
				'id'=>'profilemenu-grid',
				'pager' => array(
                    'cssFile'=>false,
                    'header'=> '',
                    'firstPageLabel' => '<i class="fa fa-forward fa-flip-horizontal" data-toggle="tooltip" title="Primera página"></i>',
			        'prevPageLabel'  => '<i class="fa fa-caret-right fa-flip-horizontal" data-toggle="tooltip" title="Anterior"></i>',
			        'nextPageLabel'  => '<i class="fa fa-caret-right" data-toggle="tooltip" title="Siguiente"></i>',
			        'lastPageLabel'  => '<i class="fa fa-forward" data-toggle="tooltip" title="Última página"></i>',
                ),	
                'afterAjaxUpdate'=> 'function(){
					deleteBtn();
					search();
					searchAdd();
                	enableSwitch();
                }',			
				'summaryText' => '', 				
				'itemsCssClass' => 'table table-bordered table-striped table-list',					
				'dataProvider'=>$model->search(),
				'columns'=>array(
					array(
						'name' => 'enabled',
						'type' => 'raw',
						'value' => 'Utils::activeSwitch($data)',
						'htmlOptions'=>array('class'=>'activeSwitch'),
					),						
					'title',
					array(
						'class'=>'CButtonColumn',
						'template'=>'{view}{update}{erase}',	
						'buttons'=>array(		
							'view' => array(
								'label'=>'<i class="fa fa-eye icon_cbutton"></i>',
								'url'=>'Yii::app()->createUrl(strtolower(Yii::app()->controller->id).\'/view/id/\'. $data->id)',
								'visible'=>'Yii::app()->user->checkAccess(\'view\')',
								'options'=>array('title'=>'Ver','data-toggle'=>'tooltip'),
								'imageUrl'=>false,
							),							
							'update' => array(
								'label'=>'<i class="fa fa-edit icon_cbutton"></i>',
								'url'=>'Yii::app()->createUrl(\'menues/modificar/\'. $data->id)',
								'visible'=>'Yii::app()->user->checkAccess(\'update\')',
								'options'=>array('title'=>'Editar','data-toggle'=>'tooltip'),
								'imageUrl'=>false,
							),					
							'erase' => array(
								'label'=>'<i class="fa fa-trash-o icon_cbutton"></i>',
								'url'=>'Yii::app()->createUrl(strtolower(Yii::app()->controller->id).\'/delete/id/\'. $data->id)',
								'visible'=>'Yii::app()->user->checkAccess(\'delete\')',
								'options' => array(
									'class' => 'delete_action',
									'data-toggle'=>'tooltip',
									'title'=>'Borrar',
									'imageUrl'=>false,
								)	
							),						
						)
					)
				),
			)); ?>
		</div>
	</div>
 </section>
 <?php
	Yii::app()->clientScript->registerCssFile(Yii::app()->params['cdnUrl'].'/plugins/lcswich/lc_switch-tts.css'); 
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/lcswich/lc_switch.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/lcswich/lc_switch-tts.js',CClientScript::POS_END);
 ?>