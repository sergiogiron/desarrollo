<?php
/* @var $this ProfilemenuController */
/* @var $model Profilemenu */
/* @var $form CActiveForm */
?>

<div class="box-body">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'profilemenu-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<div class="form-group"  style="clear: both;">
		<?php echo $form->labelEx($model,'title'); ?>
		<?php echo $form->textField($model,'title',array('maxlength'=>64,'class'=>'form-control')); ?>
		<?php echo $form->error($model,'title'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'description'); ?>
		<?php echo $form->textField($model,'description',array('maxlength'=>64,'class'=>'form-control')); ?>
		<?php echo $form->error($model,'description'); ?>
	</div>
	
	<div class="form-group">
		<label for="Profilemenu_description" class="required">Tipo <span class="required">*</span></label>
		<select name="menu_type" id="menu_type" class="form-control selectobject">
			<option value="">Seleccione</option>
			<option value="1" <?php if(!$model->isNewRecord){ if($model->id_menu==''){ echo 'selected'; } }?>>Menu</option>
			<option value="2" <?php if(!$model->isNewRecord){ if($model->id_menu!=''){ echo 'selected'; } }?> >Submenu</option>
		</select>
	</div>
	<?php 
	if($model->id_menu==''){
		$muestra=true;
	}else{
		$muestra=false;
	}
	?>
	<div class="form-group" style="<?php if(!$model->isNewRecord){ if($muestra){ echo 'display:block;'; }else{ echo 'display:none;'; } }else{ echo 'display:none;'; } ?>" id="div_ico">
		<?php echo $form->labelEx($model,'icono'); ?>
		<?php echo '<br /><button class="btn btn-default" id="convert_example_1"></button>';
		
		echo $form->hiddenField($model,'icono',array('size'=>60,'maxlength'=>100,'class'=>'form-control')); ?>
		<?php echo $form->error($model,'icono'); ?>
	</div>	
	<div class="form-group" style="<?php if(!$model->isNewRecord){ if(!$muestra){ echo 'display:block;'; }else{ echo 'display:none;'; } }else{ echo 'display:none;'; } ?>" id="div_menu">
		<?php echo $form->labelEx($model,'id_menu'); ?>
		<?php echo $form->dropDownList($model,'id_menu',CHtml::listData(Profilemenu::model()->findAll(), 'id', 'title'),array('empty' => 'Seleccione','class'=>'form-control selectobject')); ?>
		<?php echo $form->error($model,'id_menu'); ?>
	</div>
	<div class="form-group" style="<?php //if(!$model->isNewRecord){ if($muestra){ echo 'display:none;'; }else{ echo 'display:block;'; } }else{ echo 'display:none;'; } ?>" id="div_url">
		<?php
		$rutas_yii=array();
		foreach(Yii::app()->components['urlManager']->rules as $key => $value){
			if(strpos($value,'/')==false){
					$rutas_yii[]=array('id'=>$key,'name'=>$key);
			}
		}
		?>
		<?php echo $form->labelEx($model,'url'); ?>
		<?php echo $form->dropDownList($model,'url',CHtml::listData($rutas_yii, 'id', 'name'),array('empty' => 'Seleccione','class'=>'form-control selectobject')); ?>
		<?php echo $form->error($model,'url'); ?>
	</div>	
	<div class="box-footer">
		<?php
		$title = (Yii::app()->controller->action->id!='view') ? 'Volver sin guardar cambios' : 'Volver';
		echo '<a href="'.Yii::app()->request->baseUrl.'/'.strtolower(Yii::app()->controller->id).'" class="btn btn-secundary" data-toggle="tooltip" title="'.$title.'" >Cerrar</a>';
		if(Yii::app()->controller->action->id!='view'){
			echo CHtml::submitButton('Guardar',array('class'=>'btn btn-primary','data-toggle'=>'tooltip','title'=>'Guardar cambios'));
		}
		?>
	</div>
<?php $this->endWidget(); ?>
<?php
	Yii::app()->clientScript->registerCssFile(Yii::app()->params['cdnUrl'].'/plugins/lcswich/lc_switch-tts.css'); 
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/lcswich/lc_switch.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/lcswich/lc_switch-tts.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/plugins/select2/select2.full.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl.'/js/plugins/select2/select2.css');
?>
</div><!-- form -->
<script>
$( document ).ready(function() {
	<?php if(Yii::app()->controller->action->id=='view'){ echo "$('form').find('input, textarea, button, checkbox, select').attr('disabled','disabled');"; } ?>});		
</script>
<script>
$('#convert_example_1').on('change', function(e) {
	$('#Profilemenu_icono').val(e.icon);
});	
$(document).ready(function(){
	$('#convert_example_1').iconpicker({
		arrowClass: 'btn-danger',
		arrowPrevIconClass: 'glyphicon glyphicon-chevron-left',
		arrowNextIconClass: 'glyphicon glyphicon-chevron-right',
		cols: 10,
		footer: true,
		header: true,
		icon: 'fa-bomb',
		iconset: 'fontawesome',                    
		labelHeader: '{0} of {1} pages',
		labelFooter: '{0} - {1} of {2} icons',
		placement: 'bottom',
		rows: 5,
		search: true,
		searchText: 'Buscar',
		selectedClass: 'btn-success',
		unselectedClass: ''
	});
	$("#menu_type").change(function(){
		if($(this).val()==1){
			$('#Profilemenu_id_menu').val('');
			$('#Profilemenu_url').val('');
			$('#div_menu').css('display','none');
			$('#div_url').css('display','none');	
			$('#div_ico').css('display','block');	
		}else if($(this).val()==2){
			$('#Profilemenu_id_menu').val('');
			$('#Profilemenu_url').val('');
			$('#div_menu').css('display','block');
			$('#div_url').css('display','block');
			$('#div_ico').css('display','none');			
		}else{
			$('#Profilemenu_id_menu').val('');
			$('#Profilemenu_url').val('');
			$('#div_menu').css('display','none');
			$('#div_url').css('display','none');			
			$('#div_ico').css('display','none');			
		}
	});	
});		
</script>