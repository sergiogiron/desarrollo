<?php
/* @var $this ProfilemenuController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Profilemenus',
);

$this->menu=array(
	array('label'=>'Create Profilemenu', 'url'=>array('create')),
	array('label'=>'Manage Profilemenu', 'url'=>array('admin')),
);
?>

<h1>Profilemenus</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
