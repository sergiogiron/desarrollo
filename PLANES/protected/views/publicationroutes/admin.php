<?php
/* @var $this PublicationroutesController */
/* @var $model Publicationroutes */

$this->breadcrumbs=array(
	'Publicationroutes'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Publicationroutes', 'url'=>array('index')),
	array('label'=>'Create Publicationroutes', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "search();deleteBtn();"); 
?>
<section class="content-header">
	<h1>
		Endpoint de publicaciones
	</h1>
	<ol class="breadcrumb">
		<li><a href="/ttsoffice_repo"><i class="fa fa-home"></i> Home</a></li>
		<li><a href="/ttsoffice_repo/publicationroutes">Endpoint de publicaciones</a></li>
	</ol>
</section>
<section class="content">
	<div class="box box-danger">
		<div class="box-header with-border">	
			<?php			if(Yii::app()->user->checkAccess('create')){
				echo '<a href="'.Yii::app()->request->baseUrl.'/'.Yii::app()->controller->id.'/create"class="btn btn-default b_customize"><i class="fa fa-plus i_customize"></i> Agregar</a>';
			}
			?>			<div class="col-md-3 search_bar">
				<div class="row">
					<div class="input-group input-group-sm">
						<input type="text" class="form-control" id="criterio">
						<span class="input-group-btn">
						  <button type="button" id="buscar" class="btn btn-default btn-flat">
						  	<i class="fa fa-search"></i>
						  </button>
						</span>
					</div>
				</div>
			</div>	
			<?php echo CHtml::link('Búsqueda avanzada','#',array('class'=>'search-button')); ?>
			<div class="search-form" style="display:none">
			<?php $this->renderPartial('_search',array(
				'model'=>$model,
			)); ?>
			</div><!-- search-form -->
			<?php $this->widget('zii.widgets.grid.CGridView', array(
				'id'=>'publicationroutes-grid',
				'pager' => array(
                    'cssFile'=>false,
                    'header'=> '',
                    'firstPageLabel' => '<i class="fa fa-forward fa-flip-horizontal"></i>',
			        'prevPageLabel'  => '<i class="fa fa-caret-right fa-flip-horizontal"></i>',
			        'nextPageLabel'  => '<i class="fa fa-caret-right"></i>',
			        'lastPageLabel'  => '<i class="fa fa-forward"></i>',
			        //'afterAjaxUpdate'=> 'function(){enableSwitch();}',
                ),					
				'summaryText' => '', 
				'itemsCssClass' => 'table table-bordered table-striped table-list',				
				'dataProvider'=>$model->search(),
				'columns'=>array(
					array(
						'name' => 'id_channel',
						'value'=>'$data->channels==null ? " " : $data->channels->title',
					),
					array(
						'name' => 'id_subchannel',
						'value'=>'$data->subchannels==null ? " " : $data->subchannels->title',
					),		
					'url',
					array(
						'class'=>'CButtonColumn',
						'template'=>'{view}{update}{erase}',	
						'buttons'=>array(	
							'view' => array(
								'label'=>'<i class="fa fa-eye icon_cbutton"></i>',
								'url'=>'Yii::app()->createUrl(strtolower(Yii::app()->controller->id).\'/view/id/\'. $data->id)',
								'visible'=>'Yii::app()->user->checkAccess(\'view\')',
								'options'=>array(
									'title'=>'Ver',
									'data-toggle'=>'tooltip',
								),
								'imageUrl'=>false,
							),					
							'update' => array(
								'label'=>'<i class="fa fa-edit icon_cbutton"></i>',
								'visible'=>'Yii::app()->user->checkAccess(\'update\')',
								'url'=>'Yii::app()->createUrl(strtolower(Yii::app()->controller->id).\'/update/id/\'. $data->id)',
								'options'=>array(
									'title'=>'Editar',
									'data-toggle'=>'tooltip',
								),
								'imageUrl'=>false,
							),					
							'erase' => array(
								'visible'=>'Yii::app()->user->checkAccess(\'delete\')',
									'label'=>'<i class="fa fa-trash-o icon_cbutton"></i>',
									'url'=>'Yii::app()->createUrl(strtolower(Yii::app()->controller->id).\'/delete/id/\'. $data->id)',
									'options' => array(
										'class' => 'delete_action',
										'title'=>'Borrar',
										'imageUrl'=>false,
										'data-toggle'=>'tooltip',
									)		
								),	
						),
					),
				)
			)); ?>			
		</div>
	</div>
 </section>
 <script>
<?php
	// Activo/Inactivo
	Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl.'/../cdn/plugins/lcswich/lc_switch-tts.css'); 
	Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/../cdn/plugins/lcswich/lc_switch-tts.js',CClientScript::POS_END);
	/*
	// Datepicker con la mascara
	Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl.'/../cdn/plugins/datepicker/datepicker3.css');
	Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/../cdn/plugins/datepicker/bootstrap-datepicker.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/../cdn/plugins/input-mask/jquery.inputmask.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/../cdn/plugins/input-mask/jquery.inputmask.date.extensions.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/../cdn/plugins/datepicker/datepicker-tts.js',CClientScript::POS_END);
	// Timepicker con la mascara
	Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/../cdn/plugins/timepicker/bootstrap-timepicker.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/../cdn/plugins/input-mask/jquery.inputmask.bundle.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl.'/../cdn/plugins/timepicker/bootstrap-timepicker.css');
	Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/../cdn/plugins/timepicker/timepicker-tts.js',CClientScript::POS_END);
	// Editor de texto enriquesido
	Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/../cdn/plugins/ckeditor-standard/ckeditor.js',CClientScript::POS_END);
	*/
 ?>
</script>
