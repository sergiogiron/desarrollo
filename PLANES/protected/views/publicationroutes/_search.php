<?php
/* @var $this PublicationroutesController */
/* @var $model Publicationroutes */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>
<div class="col-md-12" style="clear: both">
<div class="row">
			
			<div class="col-md-3">
				<div class="row row_space">
				<?php echo $form->labelEx($model,'id_channel'); ?>
				<?php echo $form->dropDownList($model, 'id_channel',CHtml::listData(Channels::model()->findAll(), 'id', 'title'),array('empty' => 'Seleccione','class'=>'form-control'));?>
				<?php echo $form->error($model,'id_channel'); ?>
				</div>
			</div>

			<div class="col-md-3">
				<div class="row row_space">
				<?php echo $form->labelEx($model,'id_subchannel'); ?>
				<?php echo $form->dropDownList($model, 'id_subchannel',CHtml::listData(Subchannels::model()->findAll(), 'id', 'name'),array('empty' => 'Seleccione','class'=>'form-control'));?>
				<?php echo $form->error($model,'id_subchannel'); ?>
				</div>
			</div>
			
			<div class="col-md-3">
				<div class="row row_space">
					<?php echo $form->label($model,'url'); ?>
					<?php echo $form->textField($model,'url',array('size'=>60,'maxlength'=>256,'class'=>'form-control')); ?>
				</div>
			</div>
</div>
	<div class="row buttons">
		<?php echo CHtml::submitButton('Buscar',array('class'=>'btn btn-default b_customize')); ?>
	</div>
<br />
<?php $this->endWidget(); ?>
</div>
</div><!-- search-form -->