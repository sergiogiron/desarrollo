<?php
/* @var $this PublicationroutesController */
/* @var $model Publicationroutes */
/* @var $form CActiveForm */
?>

<div class="box-body">
	<div class="col-md-12 col-data">

	<?php $form=$this->beginWidget('CActiveForm', array(
		'id'=>'publicationroutes-form',
		// Please note: When you enable ajax validation, make sure the corresponding
		// controller action is handling ajax validation correctly.
		// There is a call to performAjaxValidation() commented in generated controller code.
		// See class documentation of CActiveForm for details on this.
		'enableAjaxValidation'=>false,
	)); ?>



		<div class="col-xs-6 form-group required unique">
			<?php echo $form->labelEx($model,'id_channel'); ?>
			<?php echo $form->dropDownList($model, 'id_channel',CHtml::listData(Channels::model()->findAll(), 'id', 'title'),array('empty' => 'Seleccione','class'=>'channel form-control'));?>
			<?php echo $form->error($model,'id_channel'); ?>
		</div>

		<div class="col-xs-6 form-group required unique">
			<?php echo $form->labelEx($model,'id_subchannel'); ?>
			<?php echo $form->dropDownList($model, 'id_subchannel',CHtml::listData(Subchannels::model()->findAll(), 'id', 'title'),array('empty' => 'Seleccione...','class'=>'form-control subchannel selectobject'));?>
			<?php echo $form->error($model,'id_subchannel'); ?>
		</div>

		<div class="col-xs-6 form-group required">
			<?php echo $form->labelEx($model,'url'); ?>
			<?php echo $form->textField($model,'url',array('size'=>60,'maxlength'=>256,'class'=>'form-control')); ?>
			<?php echo $form->error($model,'url'); ?>
		</div>

		<div class="box-footer">
		<?php
			$title = (Yii::app()->controller->action->id!='view') ? 'Volver sin guardar cambios' : 'Volver';
			echo '<a href="'.Yii::app()->request->baseUrl.'/'.strtolower(Yii::app()->controller->id).'" class="btn btn-secundary cancel" data-toggle="tooltip" title="'.$title.'" >Cerrar</a>';
			if(Yii::app()->controller->action->id!='view'){
				echo CHtml::submitButton('Guardar',array('class'=>'btn btn-primary save','data-toggle'=>'tooltip','title'=>'Guardar cambios'));
			}
		?>
	</div>

	<?php $this->endWidget(); ?>
	</div>
</div><!-- form -->
<?php echo '<script>viewPage = "'.Yii::app()->controller->action->id.'";</script>'; ?>
<script>
$(document).ready(function() {
	channelsFilter();
	<?php
		if(Yii::app()->controller->action->id=='view'){ echo "$('form').find('input, textarea, button, checkbox, select').attr('disabled','disabled');"; }
	?>

	/*$("#Publicationroutes_id_channel").change(function(){ 
		$("#Publicationroutes_id_subchannel").load(homeurl+"/filtersubchannel/"+$(this).val()); 
	});*/
	if(viewPage=='update'){
		$('.unique input').prop('disabled',true);
		$('.unique select').prop('disabled',true);
	}
});	
</script>	