<?php
/* @var $this ApiuserController */
/* @var $model Apiuser */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>
<div class="col-md-12" style="clear: both">
<div class="row">
			
			<div class="col-md-3">
				<div class="row row_space">
					<?php echo $form->label($model,'id'); ?>
					<?php echo $form->textField($model,'id',array('class'=>'form-control')); ?>
				</div>
			</div>
			
			<div class="col-md-3">
				<div class="row row_space">
					<?php echo $form->label($model,'id_user_services'); ?>
					<?php echo $form->textField($model,'id_user_services',array('class'=>'form-control')); ?>
				</div>
			</div>
			
			<div class="col-md-3">
				<div class="row row_space">
					<?php echo $form->label($model,'id_master_api'); ?>
					<?php echo $form->textField($model,'id_master_api',array('class'=>'form-control')); ?>
				</div>
			</div>
			
			<div class="col-md-3">
				<div class="row row_space">
					<?php echo $form->label($model,'enabled'); ?>
					<?php echo $form->checkBox($model,'enabled',array('class'=>'lcs_check enabled')); ?>
				</div>
			</div>
			
			<div class="col-md-3">
				<div class="row row_space">
					<?php echo $form->label($model,'created_at'); ?>
					<?php echo $form->textField($model,'created_at',array('class'=>'form-control datepicker','data-inputmask'=>'\'alias\': \'dd/mm/yyyy\'','data-mask'=>'data-mask')); ?>
				</div>
			</div>
			
			<div class="col-md-3">
				<div class="row row_space">
					<?php echo $form->label($model,'id_user'); ?>
					<?php echo $form->textField($model,'id_user',array('class'=>'form-control')); ?>
				</div>
			</div>
</div>
	<div class="row buttons">
		<?php echo CHtml::submitButton('Buscar',array('class'=>'btn btn-default b_customize')); ?>
	</div>
<br />
<?php $this->endWidget(); ?>
</div>
</div><!-- search-form -->