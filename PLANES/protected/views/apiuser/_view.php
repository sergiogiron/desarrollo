<section class="content-header">
  <h1>
	Apiuser	<small>Ver</small>
  </h1>
  <ol class="breadcrumb">
	<li><a href="/TTS/office"><i class="fa fa fa-home"></i> Home</a></li>
	<li><a href="/TTS/office/apiuser">Apiuser</a></li>
	<li class="active"><a href="/TTS/office/apiuser/create">Crear</a></li>
  </ol>
</section>
<section class="content">
	<div class="box box-danger">
		<div class="box-header with-border">
			<?php $this->renderPartial('_form', array('model'=>$model)); ?>		</div>
	</div>
</section>
