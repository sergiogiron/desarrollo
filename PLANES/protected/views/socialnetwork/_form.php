<?php
/* @var $this SocialnetworkController */
/* @var $model Socialnetwork */
/* @var $form CActiveForm */
?>
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'socialnetwork-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>
<div class="box-body">
	<span class="right_f">
<?php echo $form->checkBox($model,'enabled',array('class'=>'lcs_check enabled')); ?>
	
	</span>

<div class="col-md-12 col-data">


	<div class="form-group col-md-6 required unique">

			<?php 
			if(!$model->isNewRecord){ $disable='disabled'; }else{ $disable='enabled'; }	
			echo $form->labelEx($model,'id_channel'); ?>
			<?php echo $form->dropDownList($model, 'id_channel',CHtml::listData(Channels::model()->findAll(), 'id', 'title'),array($disable=>$disable,'empty' => 'Seleccione','class'=>'form-control channel'));?>
			<?php echo $form->error($model,'id_channel'); ?>

			</div>

			<div class="form-group col-md-6 required unique">
			<?php 

			if(!$model->isNewRecord){ $disable='disabled'; }else{ $disable='enabled'; }	
			echo $form->labelEx($model,'id_subchannel'); ?>
			<?php 
			if($model->isNewRecord){
			echo $form->dropDownList($model, 'id_subchannel',array(),array('empty' => 'Seleccione','class'=>'form-control subchannel'));						
			}else{
			echo $form->dropDownList($model, 'id_subchannel',CHtml::listData(Subchannels::model()->findAll("id_channel=".$model->id_channel), 'id', 'title'),array($disable=>$disable,'empty' => 'Seleccione','class'=>'form-control'));						
			}
			?>
			<?php echo $form->error($model,'id_subchannel'); ?>
			</div>


			<div class="form-group col-md-3 required unique">
	<?php echo $form->labelEx($model,'name'); ?>
			<?php echo $form->textField($model,'name',array('size'=>32,'maxlength'=>32,'class'=>'form-control')); ?>
							<?php echo $form->error($model,'name'); ?>
</div>
<div class="form-group col-md-6 required">
	<?php echo $form->labelEx($model,'url'); ?>
			<?php echo $form->textField($model,'url',array('size'=>60,'maxlength'=>255,'class'=>'form-control')); ?>
							<?php echo $form->error($model,'url'); ?>
</div>
<div class="form-group col-md-3 required">
	<?php echo $form->labelEx($model,'order'); ?>
			<?php echo $form->textField($model,'order',array('class'=>'form-control')); ?>
							<?php echo $form->error($model,'order'); ?>
</div>
<div class="box-footer">
		<?php
		$title = (Yii::app()->controller->action->id!='view') ? 'Volver sin guardar cambios' : 'Volver';
		echo '<a href="'.Yii::app()->request->baseUrl.'/'.strtolower(Yii::app()->controller->id).'" class="btn btn-secundary" data-toggle="tooltip" title="'.$title.'" >Cerrar</a>';
		if(Yii::app()->controller->action->id!='view'){
			echo CHtml::submitButton('Guardar',array('class'=>'btn btn-primary','data-toggle'=>'tooltip','title'=>'Guardar cambios'));
		}
		?></div>
<?php $this->endWidget(); ?>

</div><!-- form -->
</div>
<?php
	Yii::app()->clientScript->registerCssFile(Yii::app()->params['cdnUrl'].'/plugins/lcswich/lc_switch-tts.css'); 
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/lcswich/lc_switch.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/lcswich/lc_switch-tts.js',CClientScript::POS_END);


	Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/plugins/select2/select2.full.js',CClientScript::POS_END); 
	Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl.'/js/plugins/select2/select2.css'); 



echo '<script>viewPage = "'.Yii::app()->controller->action->id.'";</script>';
Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/js/form-validators.js',CClientScript::POS_END) ?> 
<script>
$( document ).ready(function() {
	<?php if(Yii::app()->controller->action->id=='view'){ echo "$('form').find('input, textarea, button, checkbox, select').attr('disabled','disabled');"; } ?>

 channelsFilter();

	$('.select2').select2();

	
   

	if(viewPage=='update'){
		$('.unique input').prop('disabled',true);
		$(".subchannel").prop('disabled',true);
	}


});		
</script>