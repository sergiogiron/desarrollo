<?php
/* @var $this HighlightProductsController */
/* @var $model HighlightProducts */
/* @var $form CActiveForm */
?>
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'highlight-products-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>
<div class="box-body">
	<span class="right_f">
<?php echo $form->checkBox($model,'enabled',array('class'=>'lcs_check enabled')); ?>

	</span>

<div class="col-md-12 col-data">
	<div class="form-group col-md-3 required unique">
		<?php echo $form->labelEx($model,'id_channel'); ?>
		<?php echo $form->dropDownList($model, 'id_channel',CHtml::listData(Channels::model()->findAll(), 'id', 'title'),array('empty' => 'Seleccione...','class'=>'form-control selectobject channel'));?>
		<?php echo $form->error($model,'id_channel'); ?>
	</div>
	<div class="form-group col-md-6 required unique">
		<?php echo $form->labelEx($model,'id_subchannel'); ?>
		<?php echo $form->dropDownList($model, 'id_subchannel',CHtml::listData(Subchannels::model()->findAll(), 'id', 'title'),array('empty' => 'Seleccione...','class'=>'form-control selectobject subchannel'));?>
		<?php echo $form->error($model,'id_subchannel'); ?>
	</div>
	<div class="form-group col-md-3 required unique">
		<?php echo $form->labelEx($model,'id_provider'); ?>
		<?php echo $form->dropDownList($model, 'id_provider',CHtml::listData(Providers::model()->findAll(), 'id', 'title'),array('empty' => 'Seleccione...','class'=>'form-control selectobject'));?>
		<?php echo $form->error($model,'id_provider'); ?>
	</div>
	<div class="form-group col-md-6 required">
		<?php echo $form->labelEx($model,'title',array('title'=>'Texto que se va a mostrar online.','data-toggle'=>'tooltip')); ?>
		<?php echo $form->textField($model,'title',array('size'=>60,'maxlength'=>64,'class'=>'form-control')); ?>
		<?php echo $form->error($model,'title'); ?>
	</div>

	<div class="form-group col-md-6" style="border:1px solid #e0e0e0;padding:10px 0px;border-radius:5px;margin-top: 16px;">
		<div class="col-md-4 col-md-offset-2 ">
			<?php echo $form->labelEx($model,'highlight_dynamic'); ?>
			<?php echo $form->CheckBox($model,'highlight_dynamic',array('class'=>'lcs_check')); ?>
			<?php echo $form->error($model,'highlight_dynamic'); ?>
		</div>
		<div class="col-md-4 ">
			<?php echo $form->labelEx($model,'highlight_static'); ?>
			<?php echo $form->CheckBox($model,'highlight_static',array('class'=>'lcs_check')); ?>
			<?php echo $form->error($model,'highlight_static'); ?>
		</div>
	</div>
	<div class="form-group col-md-12 paq_dinamico" style="display: none;">
		<div class="col-md-2 required">
			<?php echo $form->labelEx($model,'Number_of_results'); ?>
			<?php for ($i=1; $i < 10; $i++) {  $array10[$i] = $i; }?>
			<?php echo $form->dropDownList($model, 'Number_of_results', $array10,array('empty' => 'Seleccione...','class'=>'form-control '));?>
			<?php echo $form->error($model,'Number_of_results'); ?>
		</div>
		<div class="col-md-2 required">
		<?php echo $form->labelEx($model,'order'); ?>
			<?php for ($i=1; $i < 10; $i++) { $array10[$i] = $i; }?>
			<?php echo $form->dropDownList($model, 'order', $array10,array('empty' => 'Seleccione...','class'=>'form-control'));?>
			<?php echo $form->error($model,'order'); ?>
		</div>
		<div class="col-md-2 required">
			<?php echo $form->labelEx($model,'id_highlights_criteria'); ?>
			<?php echo $form->dropDownList($model, 'id_highlights_criteria',CHtml::listData(HighlightsCriteria::model()->findAll(), 'id', 'title'),array('empty' => 'Seleccione...','class'=>'form-control'));?>
			<?php echo $form->error($model,'id_highlights_criteria'); ?>
		</div>
		<div class="col-md-2 required ">
			<?php echo $form->labelEx($model,'highlight_order_criteria'); ?>
			<?php echo $form->dropDownList($model, 'highlight_order_criteria', array('ASC' => 'ASC', 'DESC' => 'DESC'),array('empty' => 'Seleccione...','class'=>'form-control'));?>
			<?php echo $form->error($model,'highlight_order_criteria'); ?>
		</div>
	</div>
	<div class="form-group col-md-12 paq_estatico" style="display: none;">
		<div class="col-md-12 ">
			<?php echo $form->labelEx($model,'ids_paquetes'); ?>
			<?php echo $form->hiddenField($model,'ids_paquetes',array('size'=>60,'maxlength'=>1024,'class'=>'form-control')); ?>
			<?php echo $form->error($model,'ids_paquetes'); ?>
		</div>
	</div>
	<div class="form-group col-md-12 ">
		<div class="col-md-3  required">
			<?php echo $form->labelEx($model,'from_date'); ?>
			<div class="input-group">
				<?php echo $form->textField($model,'from_date',array('class'=>'form-control datepicker','data-inputmask'=>'\'alias\': \'dd/mm/yyyy\'','data-mask'=>'data-mask')); ?>
				<div class="input-group-addon pointer"><i class="fa fa-calendar"></i></div>
			</div>
			<?php echo $form->error($model,'from_date'); ?>
		</div>
		<div class="col-md-3 required ">
			<?php echo $form->labelEx($model,'to_date'); ?>
			<div class="input-group">
			<?php echo $form->textField($model,'to_date',array('class'=>'form-control datepicker','data-inputmask'=>'\'alias\': \'dd/mm/yyyy\'','data-mask'=>'data-mask')); ?>
				<div class="input-group-addon pointer"><i class="fa fa-calendar"></i></div>
			</div>
			<?php echo $form->error($model,'to_date'); ?>
		</div>
		<div class="form-group col-md-6" style="border:1px solid #e0e0e0;padding:10px 0px;border-radius:5px;margin-top: 16px;">
			<div class="col-md-4 col-md-offset-2 ">
				<?php echo $form->labelEx($model,'offer'); ?>
				<?php echo $form->CheckBox($model,'offer',array('class'=>'lcs_check')); ?>
				<?php echo $form->error($model,'offer'); ?>

			</div>
			<div class="col-md-4 ">
				<?php echo $form->labelEx($model,'highlight'); ?>
				<?php echo $form->CheckBox($model,'highlight',array('class'=>'lcs_check')); ?>
				<?php echo $form->error($model,'highlight'); ?>
			</div>
		</div>
	</div>
	<div class="box-footer">
		<?php
		$title = (Yii::app()->controller->action->id!='view') ? 'Volver sin guardar cambios' : 'Volver';
		echo '<a href="'.Yii::app()->request->baseUrl.'/'.strtolower(Yii::app()->controller->id).'" class="btn btn-secundary cancel" data-toggle="tooltip" title="'.$title.'" >Cerrar</a>';
		if(Yii::app()->controller->action->id!='view'){
			echo CHtml::submitButton('Guardar',array('class'=>'btn btn-primary save','data-toggle'=>'tooltip','title'=>'Guardar cambios'));
		}
		?>
	</div>
<?php $this->endWidget(); ?>
</div><!-- form -->
</div>


<?php
	Yii::app()->clientScript->registerCssFile(Yii::app()->params['cdnUrl'].'/plugins/lcswich/lc_switch-tts.css');
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/lcswich/lc_switch.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/lcswich/lc_switch-tts.js',CClientScript::POS_END);

	Yii::app()->clientScript->registerCssFile(Yii::app()->params['cdnUrl'].'/plugins/datepicker/datepicker3.css');
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/datepicker/bootstrap-datepicker.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/input-mask/jquery.inputmask.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/input-mask/jquery.inputmask.date.extensions.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/datepicker/datepicker-tts.js',CClientScript::POS_END);

	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/timepicker/bootstrap-timepicker.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/input-mask/jquery.inputmask.bundle.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerCssFile(Yii::app()->params['cdnUrl'].'/plugins/timepicker/bootstrap-timepicker.css');
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/timepicker/timepicker-tts.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/plugins/select2/select2.full.js',CClientScript::POS_END);

	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/jQueryUI/jquery-ui.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerCssFile(Yii::app()->params['cdnUrl'].'/plugins/jQueryUI/jquery-ui.min.css'); 
	Yii::app()->clientScript->registerCssFile(Yii::app()->params['cdnUrl'].'/plugins/tag-it/css/jquery.tagit.css'); 
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/tag-it/js/tag-it.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl.'/js/plugins/select2/select2.css'); 

echo '<script>viewPage = "'.Yii::app()->controller->action->id.'";</script>';
Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/js/form-validators.js',CClientScript::POS_END) ?>
<script>
/*
if (!$.fn.bootstrapDP && $.fn.datepicker && $.fn.datepicker.noConflict) {
   var datepicker = $.fn.datepicker.noConflict();
   $.fn.bootstrapDP = datepicker;
}*/

$( document ).ready(function() {
	$("#HighlightProducts_ids_paquetes").tagit({
		singleFieldDelimiter:'|',
	});
	<?php if(Yii::app()->controller->action->id=='view'){ echo "$('form').find('input, textarea, button, checkbox, select').attr('disabled','disabled');"; } ?>

	$('[data-mask]').inputmask('dd/mm/yyyy', {'placeholder': 'dd/mm/aaaa'});
	/*$('.fa-calendar').bootstrapDP().on('changeDate', function(e) {
		//$(this).bootstrapDP({dateFormat:'dd/mm/yyyy'});
		$(this).parent().prev().val(e.format('dd/mm/yyyy'));
	  //  $(this).bootstrapDP('hide');
    });*/

    $('.datepicker').datepicker({
    	dateFormat:'dd/mm/yyyy'
    });

	$('.fa-clock-o').on('click', function() {
		$(this).parent().prev().trigger('click');
    });

	$('#HighlightProducts_highlight').on('click',function(){
		if($(this).prop('checked')){
			$('#HighlightProducts_offer').prop('checked',false);
		}else{
			//console.log('no');
		}
	});

	$('#HighlightProducts_offer').on('click',function(){
		if($(this).prop('checked')){
			$('#HighlightProducts_highlight').prop('checked',false);
		}else{
			//console.log('no');
		}
	});

	$('#HighlightProducts_highlight_dynamic').on('click',function(){
		if($(this).prop('checked')){
			$('#HighlightProducts_highlight_static').prop('checked',false);
			$('.paq_dinamico').show(100);
			$('.paq_estatico').hide(100);
			$('.paq_estatico').find('input').val('');
			$("#HighlightProducts_ids_paquetes").tagit("removeAll");
		}else{
			$('.paq_dinamico').hide(100);
		}
	});

	$('#HighlightProducts_highlight_static').on('click',function(){
		if($(this).prop('checked')){
			$('#HighlightProducts_highlight_dynamic').prop('checked',false);
			$('.paq_dinamico').hide(100);
			$('.paq_estatico').show(100);
			$('.paq_dinamico').find('select').val('');
		}else{
			$('.paq_estatico').hide(100);
			//console.log('no');
		}
	});

	if(viewPage=='create'){
		$('#HighlightProducts_highlight_dynamic').trigger('click');
	}
	if(viewPage=='update'){
		if($('#HighlightProducts_highlight_dynamic').prop('checked')===true){
			$('.paq_dinamico').show(100);	
		}
		if($('#HighlightProducts_highlight_static').prop('checked')===true){
			$('.paq_estatico').show(100);
		}
	}

   channelsFilter();

	$('.datepicker').datepicker({
      autoclose: true,
	  //format: 'yyyy-mm-dd',
	  format: 'dd/mm/yyyy',
	  firstDay: 1
    });
});
</script>
