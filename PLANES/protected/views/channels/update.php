<?php
/* @var $this ChannelsController */
/* @var $model Channels */

$this->breadcrumbs=array(
	'Channels'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Channels', 'url'=>array('index')),
	array('label'=>'Manage Channels', 'url'=>array('admin')),
);
?>

<section class="content-header">
  <h1>
	Canales
	<small>Modificar</small>
  </h1>
  <ol class="breadcrumb">
	<li><a href="<?php echo Yii::app()->request->baseUrl; ?>"><i class="fa fa-dashboard"></i> Home</a></li>
	<li><a href="<?php echo Yii::app()->request->baseUrl; ?>/canales">Canales</a></li>
	<li class="active">Modificar</li>
  </ol>
</section>
<section class="content">
	<div class="box box-danger">
		<div class="box-header with-border">
			<?php $this->renderPartial('_form', array('model'=>$model)); ?>
		</div>
	</div>
</section>