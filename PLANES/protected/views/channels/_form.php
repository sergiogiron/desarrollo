<?php
/* @var $this ChannelsController */
/* @var $model Channels */
/* @var $form CActiveForm */
?>

<div class="box-body">
	<div class="col-md-12 col-data">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'channels-form',
	
	'enableAjaxValidation'=>false,
)); ?>



	<div class="col-xs-6 form-group required">
		<?php echo $form->labelEx($model,'title'); ?>
		<?php echo $form->textField($model,'title',array('size'=>45,'maxlength'=>45,'class'=>'form-control letras')); ?>
		<?php echo $form->error($model,'title'); ?>
	</div>

	<div class="box-footer">
		<?php
			$title = (Yii::app()->controller->action->id!='view') ? 'Volver sin guardar cambios' : 'Volver';
			echo '<a href="'.Yii::app()->request->baseUrl.'/'.strtolower(Yii::app()->controller->id).'" class="btn btn-secundary cancel" data-toggle="tooltip" title="'.$title.'" >Cerrar</a>';
			if(Yii::app()->controller->action->id!='view'){
				echo CHtml::submitButton('Guardar',array('class'=>'btn btn-primary save','data-toggle'=>'tooltip','title'=>'Guardar cambios'));
			}
		?>
	</div>

<?php $this->endWidget(); ?>

</div>
</div>
<?php 
echo '<script>viewPage = "'.Yii::app()->controller->action->id.'";</script>';
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/js/form-validators.js',CClientScript::POS_END) ?>
<script>

$( document ).ready(function() {

	<?php if(Yii::app()->controller->action->id=='view'){ echo "$('form').find('input, textarea, button, checkbox, select').attr('disabled','disabled');"; } ?>

});
</script>
