<section class="content-header">
    <h1>
	Base de Contactos
	<small>Etiquetas</small>
  </h1>
    <ol class="breadcrumb">
        <li><a href="/ttsoffice_frontend"><i class="fa fa-dashboard"></i> Home</a></li>
        <li>Base de Contactos</li>
        <li class="active"><a href="/ttsoffice_frontend/etiquetas/admin">Etiquetas</a></li>
    </ol>
</section>
<section class="content">
    <div class="box box-danger">
        <div class="box-header with-border">
            <a href="/ttsoffice_frontend/etiquetas/crear" class="btn btn-default b_customize"><i class="fa fa-plus i_customize"></i> Agregar Etiqueta</a>

            <div id="contacts-grid" class="grid-view">
                <div class="summary"></div>
                <table class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th id="contacts-grid_c0"><a class="sort-link" href="#">Nombre</a></th>
                            <th id="contacts-grid_c1"><a class="sort-link" href="#">Descripción</a></th>
                            <th id="contacts-grid_c2"><a class="sort-link" href="#">Tipo</a></th>
                            <th id="contacts-grid_c3"><a class="sort-link" href="#">Fecha de alta</a></th>
                            <th id="contacts-grid_c3"><a class="sort-link" href="#">Alta por usuario</a></th>
                            <th class="button-column" id="contacts-grid_c6">&nbsp;</th>
                        </tr>
                        <tr class="filters">
                            <td>
                                <input name="name" type="text" maxlength="255">
                            </td>
                            <td>
                                <input name="description" type="text" maxlength="255">
                            </td>
                            <td>
                                <input name="type" type="text" maxlength="255">
                            </td>
                            <td>
                                <input name="date" type="text" maxlength="255">
                            </td>
                            <td>
                                <input name="createdby" type="text" maxlength="255">
                            </td>
                            <td>&nbsp;</td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="odd">
                          <td>Deserb</td>
                          <td>Lorem ipsum dolor sit amet</td>
                          <td>Bool</td>
                          <td>2016-02-15</td>
                          <td>slopez</td>
                          <td class="button-column">
                            <a title="Borrar contacto" class="delete" href="#"><i class="fa fa-trash-o icon_cbutton"></i></a>
                            <a title="Editar contacto" href="/ttsoffice_frontend/etiquetas/modificar"><i class="fa fa-edit icon_cbutton"></i></a>
                          </td>
                        </tr>
                        <tr class="even">
                          <td>Rolby</td>
                          <td>Lorem ipsum dolor sit amet</td>
                          <td>Lista</td>
                          <td>2016-02-15</td>
                          <td>kmelvillo</td>
                          <td class="button-column">
                            <a title="Borrar contacto" class="delete" href="#"><i class="fa fa-trash-o icon_cbutton"></i></a>
                            <a title="Editar contacto" href="/ttsoffice_frontend/etiquetas/modificar"><i class="fa fa-edit icon_cbutton"></i></a>
                          </td>
                        </tr>
                        <tr class="odd">
                          <td>Comensly</td>
                          <td>Lorem ipsum dolor sit amet</td>
                          <td>Bool</td>
                          <td>2016-02-15</td>
                          <td>tloprestti</td>
                          <td class="button-column">
                            <a title="Borrar contacto" class="delete" href="#"><i class="fa fa-trash-o icon_cbutton"></i></a>
                            <a title="Editar contacto" href="/ttsoffice_frontend/etiquetas/modificar"><i class="fa fa-edit icon_cbutton"></i></a>
                          </td>
                        </tr>
                        <tr class="even">
                          <td>Licouret</td>
                          <td>Lorem ipsum dolor sit amet</td>
                          <td>Fecha</td>
                          <td>2016-02-15</td>
                          <td>ngudman</td>
                          <td class="button-column">
                            <a title="Borrar contacto" class="delete" href="#"><i class="fa fa-trash-o icon_cbutton"></i></a>
                            <a title="Editar contacto" href="/ttsoffice_frontend/etiquetas/modificar"><i class="fa fa-edit icon_cbutton"></i></a>
                          </td>
                        </tr>
                        <tr class="odd">
                          <td>Monstlalon</td>
                          <td>Lorem ipsum dolor sit amet</td>
                          <td>Lista</td>
                          <td>2016-02-15</td>
                          <td>ltorres</td>
                          <td class="button-column">
                            <a title="Borrar contacto" class="delete" href="#"><i class="fa fa-trash-o icon_cbutton"></i></a>
                            <a title="Editar contacto" href="/ttsoffice_frontend/etiquetas/modificar"><i class="fa fa-edit icon_cbutton"></i></a>
                          </td>
                        </tr>
                        <tr class="even">
                          <td>Ciotec</td>
                          <td>Lorem ipsum dolor sit amet</td>
                          <td>Fecha</td>
                          <td>2016-02-15</td>
                          <td>lacosta</td>
                          <td class="button-column">
                            <a title="Borrar contacto" class="delete" href="#"><i class="fa fa-trash-o icon_cbutton"></i></a>
                            <a title="Editar contacto" href="/ttsoffice_frontend/etiquetas/modificar"><i class="fa fa-edit icon_cbutton"></i></a>
                          </td>
                        </tr>
                        <tr class="odd">
                          <td>Gorgorot</td>
                          <td>Lorem ipsum dolor sit amet</td>
                          <td>Bool</td>
                          <td>2016-02-15</td>
                          <td>zmadelon</td>
                          <td class="button-column">
                            <a title="Borrar contacto" class="delete" href="#"><i class="fa fa-trash-o icon_cbutton"></i></a>
                            <a title="Editar contacto" href="/ttsoffice_frontend/etiquetas/modificar"><i class="fa fa-edit icon_cbutton"></i></a>
                          </td>
                        </tr>
                        <tr class="even">
                          <td>Polimer</td>
                          <td>Lorem ipsum dolor sit amet</td>
                          <td>Lista</td>
                          <td>2016-02-15</td>
                          <td>plineker</td>
                          <td class="button-column">
                            <a title="Borrar contacto" class="delete" href="#"><i class="fa fa-trash-o icon_cbutton"></i></a>
                            <a title="Editar contacto" href="/ttsoffice_frontend/etiquetas/modificar"><i class="fa fa-edit icon_cbutton"></i></a>
                          </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>
