<style media="screen">
  .form-group.list, .form-group.list-table{
    display: none;
  }
  .form-group.list label {
    display: block;
  }
  .form-group.list input {
    display: inline;
    width: 75%;
  }

  .form-group.list button{
    display: inline;
    padding: 6px 10px;
    vertical-align: initial;
  }

</style>

<section class="content-header">
    <h1>
	Base de Contactos
	<small>Crear Etiqueta</small>
  </h1>
    <ol class="breadcrumb">
        <li><a href="/ttsoffice_frontend"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="/ttsoffice_frontend/basecontactos/admin">Base de Contactos</a></li>
        <li class="active"><a href="/ttsoffice_frontend/etiquetas/crear">Crear Etiqueta</a></li>
    </ol>
</section>
<section class="content">
    <div class="box box-danger">
        <div class="box-header with-border">
          <form id="tags-form" action="/ttsoffice_frontend/etiquetas/crear" method="post">
              <p class="note">Los campos con <span class="required">*</span> son requeridos.</p>

              <div class="form-group col-md-6 col-xs-12">
                  <label for="name" class="required">Nombre <span class="required">*</span></label>
                  <input size="60" maxlength="255" class="form-control" name="name" id="name" type="text" required="">
              </div>

              <div class="form-group col-md-6 col-xs-12">
                  <label for="descripcion" class="required">Descripcion <span class="required">*</span></label>
                  <input size="60" maxlength="255" class="form-control" name="descripcion" id="descripcion" type="text" required="">
              </div>

              <div class="form-group col-md-6 col-xs-12">
                  <label for="tipo">Tipo</label>
                  <select class="form-control" name="tipo" id="tipo">
                      <option value="boolean">Bool</option>
                      <option value="array">Lista</option>
                      <option value="date">Fecha</option>
                  </select>
              </div>

              <div class="form-group list col-md-6 col-xs-12">
                  <label for="list" class="required">Agregar items a lista <span class="required">*</span></label>
                  <input size="60" maxlength="255" class="form-control" name="list" id="list" type="text">
                  <button id="add-item" type="button" class="btn btn-primary btn-flat">Agregar</button>
              </div>

              <div class="form-group list-table col-md-6 col-xs-12">
                <table class="table table-bordered table-striped">
                  <thead>
                      <tr>
                        <th>Lista</th>
                      </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
              <div class="box-footer col-xs-12">
                  <input class="btn btn-primary" type="submit" name="yt0" value="Crear">
                  <a href="/ttsoffice_frontend/etiquetas/admin" class="btn btn-default">Cancelar</a>
              </div>
          </form>
        </div>
    </div>
</section>

<script>

$('#tags-form select[name=tipo]').on('change', function(ev){
  var selection = this.options[this.selectedIndex].value;
  if (selection === 'array') {
    $('.form-group.list,.form-group.list-table').show();
  } else{
    $('.form-group.list,.form-group.list-table').hide();
  }
});

$('#tags-form .form-group.list button').on('click', function(ev){
  var valor = $(this).prev().val();
  if (valor){
    $(this).prev().val('');
    var record = document.createElement('tr');
    var editRemove = '<td class="button-column">'+
                        '<a title="Borrar contacto" class="delete" href="#" onclick="removeItem(event);return false;"><i class="fa fa-trash-o icon_cbutton"></i></a>' +
                        '<a title="Editar contacto" href="#" onclick="modifyItem(event);return false;"><i class="fa fa-edit icon_cbutton"></i></a>' +
                      '</td>';
    var item = '<td>'+ valor +'</td>';
    var table = $('.list-table tbody');
    $(record).append(item);
    $(record).append(editRemove);
    table.append(record);
  }
});

function removeItem(){
  if (confirm('¿Desea borrar el registro?'))  $(event.target).closest('tr').remove();
}

function modifyItem(){
  var record = $(event.target).closest('tr');
  var value = record.first('td').text();
  $('#tags-form .form-group.list input').val(value);
  record.remove();

}


</script>
