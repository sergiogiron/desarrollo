<?php
/* @var $this OfficesController */
/* @var $model Offices */
/* @var $form CActiveForm */
?>

<div class="box-body">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'offices-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

<span class="right_f">
	<?php echo $form->checkBox($model,'enabled',array('class'=>'lcs_check enabled')); ?>
</span>

	

	<div class="col-md-12" style="clear: both">
	<div class="row">
	
	
	
	<div class="col-md-6 form-group required unique">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name',array('size'=>32,'maxlength'=>32,'class'=>'form-control letras')); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>
	

	
	<div class="col-md-6 form-group">
		<?php echo $form->labelEx($model,'schedule'); ?>
		<?php echo $form->textField($model,'schedule',array('class'=>'form-control')); ?>
		<?php echo $form->error($model,'schedule'); ?>
	</div>

	</div>
	<div class="row">

	<div class="col-md-6 form-group required">
		<?php echo $form->labelEx($model,'mail'); ?>
		<div class="input-group">
		<?php echo $form->textField($model,'mail',array('size'=>60,'maxlength'=>128,'class'=>'form-control')); ?>
		<div class="input-group-addon pointer"><i class="fa fa-envelope-o"></i></div>
		</div>
		<?php echo $form->error($model,'mail'); ?>
	</div>
	
	<div class="col-md-6 form-group required">
		<?php echo $form->labelEx($model,'phone'); ?>
		<div class="input-group">
		<?php echo $form->textField($model,'phone',array('size'=>32,'maxlength'=>32,'class'=>'form-control ')); ?>
		<div class="input-group-addon pointer"><i class="fa fa-phone"></i></div>
		</div>
		<?php echo $form->error($model,'phone'); ?>
	</div>
	

	
	

	
	
	<div class="col-md-6 form-group required">
		<?php echo $form->labelEx($model,'address'); ?>
		<?php echo $form->textField($model,'address',array('size'=>60,'maxlength'=>256,'class'=>'form-control')); ?>
		<?php echo $form->error($model,'address'); ?>
	</div>
	

	<div class="col-md-6 form-group required">
		<?php echo $form->labelEx($model,'info'); ?>
		<?php echo $form->textField($model,'info',array('size'=>60,'maxlength'=>1024,'class'=>'form-control')); ?>
		<?php echo $form->error($model,'info'); ?>
	</div>
	
	

	<div class="col-md-6 form-group">

	<?php echo $form->labelEx($model,'responsable'); ?>
		<?php echo $form->dropDownList($model, 'responsable',CHtml::listData(Users::model()->findAll(), 'id',function($model){return "{$model->name} {$model->lastname}";}),array('empty' => 'Seleccione...','class'=>'form-control'));?>
		<?php echo $form->error($model,'responsable'); ?>

	</div>
	<div class="col-md-6 form-group required">
		<?php echo $form->labelEx($model,'order'); ?>
		<?php 
		$order=count($order);
		if($model->isNewRecord){$order=$order+1;}
		
		$order_array=array();
		for($i=1;$i<=$order;$i++){
			$order_array[]=array('id'=>$i,'title'=>$i);
		}
		$orden_aux=$model->isNewRecord?$order:$model->order;
		echo $form->dropDownList($model, 'order',CHtml::listData($order_array, 'id', 'title'),array('empty' => 'Seleccione...','class'=>'form-control','options' => array($orden_aux=>array('selected'=>true))));?>		
		<?php echo $form->error($model,'order'); ?>
		<div class="errorMessage"><?php if(isset($errors['order'])){ echo $errors['order']; } ?></div>		
	</div>	
	
	<div class="col-md-6 form-group">
		<?php echo $form->labelEx($model,'public'); ?>
		<?php echo $form->checkBox($model,'public',array('class'=>'checkbox_form')); ?>
		<?php echo $form->error($model,'public'); ?>
	</div>
	

	

</div>

<div class="col-md-12">
		<label class="label_image">Banner Lateral</label>
		<div class="col-md-1">
			<div class="more_img action_img action_img" data-position="1" title="Agregar imagen" data-toggle="tooltip">+</div>
		</div>
		<div class="col-md-11 content-images">
			<ul class="slider1" id="slider_1">
				<?php
					foreach($model->imagenes as $imagenes){
						if($imagenes->position==1){
							echo "<li id='".$imagenes->filename."_slide'  title='Titulo:".$imagenes->title." Enlace:".$imagenes->link."' class='bslide_1 slide ui-state-default ui-sortable-handle'><i class='fa fa-edit edit_img_i' data-position=".$imagenes->position." data-id=".$imagenes->filename." onclick='edit_element($(this))'></i><i class='fa fa-times delete_img_i' data-id=".$imagenes->filename."   data-position=".$imagenes->position." onclick='delete_element($(this))'></i><img src='".Yii::app()->request->baseUrl."/uploads/tmp/".Yii::app()->user->id."/".$imagenes->filename."/".$imagenes->filename.".".$imagenes->ext."'></li>";
						}
					}
				?>	
			</ul>	
			<div id="data-d-content_1">
				<?php
					foreach($model->imagenes as $imagenes){
						if($imagenes->position==1){
							$data=array('name'=>$imagenes->filename,'position'=>$imagenes->position,'ext'=>$imagenes->ext,'title'=>$imagenes->title,'link'=>$imagenes->link,'order'=>$imagenes->order,'coords'=>json_decode($imagenes->coords));
							echo "<input type='hidden' id=".$imagenes->filename." name='image[]' class='element_data' value='".json_encode($data)."'>";
						}
					}
				?>	
			</div>			
		</div>	
	</div>
	
	
	<div class="box-footer">
		<?php
			$title = (Yii::app()->controller->action->id!='view') ? 'Volver sin guardar cambios' : 'Volver';
			echo '<a href="'.Yii::app()->request->baseUrl.'/'.strtolower(Yii::app()->controller->id).'" class="btn btn-secundary cancel" data-toggle="tooltip" title="'.$title.'" >Cerrar</a>';
			if(Yii::app()->controller->action->id!='view'){
				echo CHtml::submitButton('Guardar',array('class'=>'btn btn-primary save','data-toggle'=>'tooltip','title'=>'Guardar cambios'));
			}
		?>
	</div>

	<script>
id_menu=<?=Yii::app()->user->id_menu()?>;
menu='<?=strtolower(Yii::app()->controller->id)?>';
eval("var cantidad_images_"+1+" = "+$('#data-d-content_1 :input').length);
//eval("var cantidad_images_"+2+" = "+$('#data-d-content_2 :input').length);
eval("var limit_images_"+1+" = '5'");
//eval("var limit_images_"+2+" = '5'");
</script>

<?php $this->endWidget(); ?>

</div><!-- form -->
</div>

<?php
// Validadores de campos del Formulario
	echo '<script>viewPage = "'.Yii::app()->controller->action->id.'";</script>';
Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/js/form-validators.js',CClientScript::POS_END);

	// Activo/Inactivo
	Yii::app()->clientScript->registerCssFile(Yii::app()->params['cdnUrl'].'/plugins/lcswich/lc_switch-tts.css'); 
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/lcswich/lc_switch.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/lcswich/lc_switch-tts.js',CClientScript::POS_END);


	//logica uploades de imagenes
	Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/uploader_image.js',CClientScript::POS_END); 
	//logica uploades de imagenes
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/js/dropzone.js',CClientScript::POS_END); 
	Yii::app()->clientScript->registerCssFile(Yii::app()->params['cdnUrl'].'/css/jquery.Jcrop.css');
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/js/jquery.Jcrop.js',CClientScript::POS_END); 
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/js/jquery.bxslider.js',CClientScript::POS_END); 
	Yii::app()->clientScript->registerCssFile(Yii::app()->params['cdnUrl'].'/plugins/jQueryUI/jquery-ui.min.css');
	Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/jquery-ui.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerCssFile(Yii::app()->params['cdnUrl'].'/css/jquery.bxslider.css');



	// Datepicker con la mascara


	Yii::app()->clientScript->registerCssFile(Yii::app()->params['cdnUrl'].'/plugins/datepicker/datepicker3.css');
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/datepicker/bootstrap-datepicker.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/input-mask/jquery.inputmask.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/input-mask/jquery.inputmask.date.extensions.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/datepicker/datepicker-tts.js',CClientScript::POS_END);

	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/ckeditor-standard/ckeditor.js',CClientScript::POS_END);
	// Timepicker con la mascara
	/*
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/timepicker/bootstrap-timepicker.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/input-mask/jquery.inputmask.bundle.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerCssFile(Yii::app()->params['cdnUrl'].'/plugins/timepicker/bootstrap-timepicker.css');
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/timepicker/timepicker-tts.js',CClientScript::POS_END);
	// Editor de texto enriquesido
	
	*/
 ?>