<?php
/* @var $this OfficesController */
/* @var $model Offices */

$this->breadcrumbs=array(
	'Offices'=>array('index'),
	$model->name,
);


?>

<section class="content-header">
	<?php echo Yii::app()->user->Title(); ?>
	<?php echo Yii::app()->user->Breadcrumb(); ?>
</section>
<section class="content">
	<div class="box box-danger">
		<div class="box-body">
<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'htmlOptions' => array('class' => 'table table-bordered table-striped'),
	'attributes'=>array(
		'id',
		'name',
		'schedule',
		'phone',
		'fax',
		'address',
		'mail',
		'info',
		'order',
		'responsable',
		'public',
		'enabled',
		'created_at',
		'id_user',
	),
)); ?>
		</div>
	</div>
</section>
