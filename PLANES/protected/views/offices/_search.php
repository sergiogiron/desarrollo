<?php
/* @var $this OfficesController */
/* @var $model Offices */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>
<div class="col-md-12" style="clear: both">
<div class="row">
			
			
			
			<div class="col-md-4">
				<div class="row row_space">
					<?php echo $form->label($model,'name'); ?>
					<?php echo $form->textField($model,'name',array('size'=>32,'maxlength'=>32,'class'=>'form-control')); ?>
				</div>
			</div>
			
			
			
			<div class="col-md-4">
				<div class="row row_space">
					<?php echo $form->label($model,'phone'); ?>
					<?php echo $form->textField($model,'phone',array('size'=>32,'maxlength'=>32,'class'=>'form-control')); ?>
				</div>
			</div>
			
			
			
			<div class="col-md-4">
				<div class="row row_space">
					<?php echo $form->label($model,'mail'); ?>
					<?php echo $form->textField($model,'mail',array('size'=>60,'maxlength'=>128,'class'=>'form-control')); ?>
				</div>
			</div>
			
			
			<div class="col-md-3">
				<div class="row row_space">
					<?php echo $form->label($model,'public'); ?>
					<?php echo $form->checkBox($model,'public',array('class'=>'checkbox_form')); ?>
				</div>
			</div>
			
			<div class="col-md-3">
				<div class="row row_space">
					<?php echo $form->label($model,'enabled'); ?>
					<?php echo $form->checkBox($model,'enabled',array('class'=>'checkbox_form')); ?>
				</div>
			</div>
			
</div>
	<div class="row buttons">
		<?php echo CHtml::submitButton('Buscar',array('class'=>'btn btn-default b_customize')); ?>
	</div>
<br />
<?php $this->endWidget(); ?>
</div>
</div><!-- search-form -->