<?php
/* @var $this OfficesController */
/* @var $model Offices */

Yii::app()->clientScript->registerScript('search', "search(); deleteBtn();"); 
?>
<section class="content-header">
	<?php echo Yii::app()->user->Title(); ?>
	<?php echo Yii::app()->user->Breadcrumb(); ?>
	<?php $this->widget('ext.publicationrecord.PublicationrecordWidget'); ?>
</section>
<section class="content">
	<div class="box box-danger">
		<div class="box-header with-border">	
			<?php			
			if(Yii::app()->user->checkAccess('create')){
				echo '<a href="'.Yii::app()->request->baseUrl.'/'.Yii::app()->controller->id.'/create"class="btn btn-default b_customize"><i class="fa fa-plus i_customize"></i> Agregar </a>';
			}
			?>

			<?php
				if(Yii::app()->user->checkAccess('publish')){		
					echo '<a href="'.Yii::app()->request->baseUrl.'/offices/publish" class="publish btn btn-success a_customize"><i class="fa fa-cloud-upload i_customize"></i>Publicar</a>';
				}
			?>

			<?php
			if(Yii::app()->user->checkAccess('configimages')){
					echo '<a href="'.Yii::app()->request->baseUrl.'/configimages/'.Yii::app()->user->getRoute().'" class="btn btn-success a_customize"><i class="fa fa-cog i_customize"></i>Imagenes</a>';
			}
			?>

			
			<?php echo Utils::SearchObject(); ?>	
			<?php echo CHtml::link('Búsqueda avanzada','#',array('class'=>'search-button')); ?>
			<p class="registros">(<span><?=$model->count();?></span> registros encontrados)</p>

			<div class="search-form" style="display:none">
			<?php $this->renderPartial('_search',array(
				'model'=>$model,
			)); ?>
			</div><!-- search-form -->
			
			<?php $this->widget('GridView', array(
				'id'=>'offices-grid',
				'pager' => array(
                    'cssFile'=>false,
                    'header'=> '',
                    'firstPageLabel' => '<i class="fa fa-forward fa-flip-horizontal"></i>',
			        'prevPageLabel'  => '<i class="fa fa-caret-right fa-flip-horizontal"></i>',
			        'nextPageLabel'  => '<i class="fa fa-caret-right"></i>',
			        'lastPageLabel'  => '<i class="fa fa-forward"></i>',
			        //'afterAjaxUpdate'=> 'function(){enableSwitch();}',
                ),	
				'ajaxUpdate'=>true, //AJAX enabled
                'loadingCssClass'=>'',
				'afterAjaxUpdate'=> 'function(){enableSwitch();deleteBtn(); closeLoading(); getTotalRows();}',					
				'summaryText' => '', 
				'itemsCssClass' => 'table table-bordered table-striped table-list',					
				'dataProvider'=>$model->search(),
				'columns'=>array(
					//'id',
					array(
						'name' => 'enabled',
						 'type' => 'raw',
						'value' =>'Utils::activeSwitch($data)',
						'htmlOptions'=>array('class'=>'activeSwitch'),
					),	

		'name',
		//'schedule',
		'phone',
		//'fax',
		//'address',
		'mail',
		//'info',
		
		array(
					'name' => 'responsable',
					'value'=>'$data->usuario==null ? " " : $data->usuario->name.\' \'.$data->usuario->lastname',
				),
		
		array(
			'name' => 'public',
			 'type' => 'raw',
			'value' => '$data->public == 1 ? \'<i class="fa fa-check icon_center" aria-hidden="true"></i>\' : \'<i class="fa fa-close icon_center" aria-hidden="true"></i>\' ;',
		),

		'order',
		
		/*
		'created_at',
		'id_user',
		*/
					array(
						'class'=>'CButtonColumn',
						'template'=>'{view}{update}{erase}',	
						'buttons'=>array(		
							'view' => array(
								'label'=>'<i class="fa fa-eye icon_cbutton"></i>',
								'url'=>'Yii::app()->createUrl(strtolower(Yii::app()->controller->id).\'/view/id/\'. $data->id)',
								'visible'=>'Yii::app()->user->checkAccess(\'view\')',
								'options'=>array('title'=>'Ver'),
								'imageUrl'=>false,
							),						
							'update' => array(
								'label'=>'<i class="fa fa-edit icon_cbutton"></i>',
								'visible'=>'Yii::app()->user->checkAccess(\'update\')',
								'url' => 'Yii::app()->createUrl(strtolower(Yii::app()->controller->id).\'/update/id/\'. $data->id)',
								'options'=>array('title'=>'Editar'),
								'imageUrl'=>false,
							),			
							'erase' =>array(
								'visible'=>'Yii::app()->user->checkAccess(\'delete\')',
								'url'=>'Yii::app()->createUrl(strtolower(Yii::app()->controller->id).\'/delete/id/\'. $data->id)',
								'label'=>'<i class="fa fa-trash-o icon_cbutton"></i>',
								'options' => array(
									'class' => 'delete_action',
									'title'=>'Borrar',
									'imageUrl'=>false,
								)	
							),
						),
					),
				)
			)); ?>
		</div>
	</div>
 </section>

 <?php
	// Activo/Inactivo
	Yii::app()->clientScript->registerCssFile(Yii::app()->params['cdnUrl'].'/plugins/lcswich/lc_switch-tts.css'); 
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/lcswich/lc_switch.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/lcswich/lc_switch-tts.js',CClientScript::POS_END);
 ?>