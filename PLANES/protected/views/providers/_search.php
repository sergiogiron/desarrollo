<?php
/* @var $this ProvidersController */
/* @var $model Providers */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="col-md-12 col-data">
	
	
		
	<div class="col-md-4">
	<div class="row row_space">
		<?php echo $form->label($model,'title'); ?>
		<?php echo $form->textField($model,'title',array('size'=>40,'maxlength'=>65)); ?>
	</div>
	</div>

	<div class="col-md-3">
	<div class="row row_space">
		<?php echo $form->label($model,'enabled'); ?>
		<?php echo $form->CheckBox($model,'enabled',array('class'=>'checkbox_form')); ?>
	</div>
	</div>

	
	<div class="row buttons">
		<?php echo CHtml::submitButton('Buscar',array('class'=>'btn btn-default b_customize')); ?>
	</div>
<br />
<?php $this->endWidget(); ?>
</div>
</div><!-- search-form -->