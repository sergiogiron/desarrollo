<?php
/* @var $this BusinessController */
/* @var $model Business */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>
<div class="col-md-12" style="clear: both">
<div class="row">
			
			<div class="col-md-3">
				<div class="row row_space">
					<?php echo $form->label($model,'id'); ?>
					<?php echo $form->textField($model,'id',array('class'=>'form-control')); ?>
				</div>
			</div>
			
			<div class="col-md-3">
				<div class="row row_space">
					<?php echo $form->label($model,'id_user'); ?>
					<?php echo $form->textField($model,'id_user',array('class'=>'form-control')); ?>
				</div>
			</div>
			
			<div class="col-md-3">
				<div class="row row_space">
					<?php echo $form->label($model,'filename'); ?>
					<?php echo $form->textField($model,'filename',array('size'=>60,'maxlength'=>64,'class'=>'form-control')); ?>
				</div>
			</div>
			
			<div class="col-md-3">
				<div class="row row_space">
					<?php echo $form->label($model,'ext'); ?>
					<?php echo $form->textField($model,'ext',array('size'=>12,'maxlength'=>12,'class'=>'form-control')); ?>
				</div>
			</div>
			
			<div class="col-md-3">
				<div class="row row_space">
					<?php echo $form->label($model,'obdt'); ?>
					<?php echo $form->textField($model,'obdt',array('class'=>'form-control')); ?>
				</div>
			</div>
			
			<div class="col-md-3">
				<div class="row row_space">
					<?php echo $form->label($model,'title'); ?>
					<?php echo $form->textField($model,'title',array('size'=>50,'maxlength'=>50,'class'=>'form-control')); ?>
				</div>
			</div>
			
			<div class="col-md-3">
				<div class="row row_space">
					<?php echo $form->label($model,'address'); ?>
					<?php echo $form->textField($model,'address',array('size'=>50,'maxlength'=>50,'class'=>'form-control')); ?>
				</div>
			</div>
			
			<div class="col-md-3">
				<div class="row row_space">
					<?php echo $form->label($model,'phone'); ?>
					<?php echo $form->textField($model,'phone',array('size'=>35,'maxlength'=>35,'class'=>'form-control phone')); ?>
				</div>
			</div>
			
			<div class="col-md-3">
				<div class="row row_space">
					<?php echo $form->label($model,'email'); ?>
					<?php echo $form->EmailField($model,'email',array('size'=>80,'maxlength'=>80,'class'=>'form-control')); ?>
				</div>
			</div>
			
			<div class="col-md-3">
				<div class="row row_space">
					<?php echo $form->label($model,'created_at'); ?>
					<?php echo $form->textField($model,'created_at',array('class'=>'form-control datepicker','data-inputmask'=>'\'alias\': \'dd/mm/yyyy\'','data-mask'=>'data-mask')); ?>
				</div>
			</div>
			
			<div class="col-md-3">
				<div class="row row_space">
					<?php echo $form->label($model,'name'); ?>
					<?php echo $form->textField($model,'name',array('size'=>62,'maxlength'=>62,'class'=>'form-control letras')); ?>
				</div>
			</div>
			
			<div class="col-md-3">
				<div class="row row_space">
					<?php echo $form->label($model,'order'); ?>
					<?php echo $form->textField($model,'order',array('class'=>'form-control')); ?>
				</div>
			</div>
</div>
	<div class="row buttons">
		<?php echo CHtml::submitButton('Buscar',array('class'=>'btn btn-default b_customize')); ?>
	</div>
<br />
<?php $this->endWidget(); ?>
</div>
</div><!-- search-form -->