<?php
/* @var $this BusinessController */
/* @var $model Business */

Yii::app()->clientScript->registerScript('search', "
	search();
	searchAdd();
	deleteBtn();
");
?>
<section class="content-header">
	<?php
	echo Yii::app()->user->Title();
	echo Yii::app()->user->Breadcrumb();
	?>	<p class="registros">(<?=$model->count();?> registros encontrados)</p>
</section>
<section class="content">
	<div class="box box-danger">
		<div class="box-header with-border">
			<?php			if(Yii::app()->user->checkAccess('create')){
				echo '<a href="'.Yii::app()->request->baseUrl.'/'.Yii::app()->controller->id.'/create" class="btn btn-default b_customize agregar"><i class="fa fa-plus i_customize"></i> Agregar</a>';
			}
			?>
			<?php
				if(Yii::app()->user->checkAccess('publish')){
					echo '<a href="'.Yii::app()->request->baseUrl.'/business/publish" class="publish btn btn-success a_customize"><i class="fa fa-cloud-upload i_customize"></i>Publicar</a>';
				}
			?>
			<?php echo Utils::SearchObject(); ?>			<?php echo CHtml::link('Búsqueda avanzada','#',array('class'=>'search-button')); ?>
			<div class="search-form" style="display:none">
			<?php $this->renderPartial('_search',array(
				'model'=>$model,
			)); ?>
			</div><!-- search-form -->

			<?php $this->widget('zii.widgets.grid.CGridView', array(
				'id'=>'business-grid',
				'pager' => array(
					'cssFile'=>false,
					'header'=> '',
					'firstPageLabel' => '<i class="fa fa-forward fa-flip-horizontal" data-toggle="tooltip" title="Primera página"></i>',
					'prevPageLabel'  => '<i class="fa fa-caret-right fa-flip-horizontal" data-toggle="tooltip" title="Anterior"></i>',
					'nextPageLabel'  => '<i class="fa fa-caret-right" data-toggle="tooltip" title="Siguiente"></i>',
					'lastPageLabel'  => '<i class="fa fa-forward" data-toggle="tooltip" title="Última página"></i>',
				),
				'afterAjaxUpdate'=> 'function(){
					enableSwitch();
					deleteBtn();
					search();
					searchAdd();
				}',
				'summaryText' => '',
				'itemsCssClass' => 'table table-bordered table-striped table-list',
				'dataProvider'=>$model->search(),
				'columns'=>array(
			array(
					'name' => 'enabled',
					'type' => 'raw',
					'value' => 'Utils::activeSwitch($data)',
					'htmlOptions'=>array('class'=>'activeSwitch'),
				),
					array(
					'header'=>'Logo',
					'value'=>'(!empty($data->filename))?CHtml::image(Yii::app()->request->baseUrl."/uploads/business/".$data->id."/".$data->filename."/".$data->filename.".".$data->ext,"",array("style"=>"width:50px;margin: auto;display: table;")):"Sin logo"',
					'type'  => 'raw',
					),
					'title',
					array(
						'header'=>'OBT',
						'name'=>'obdt',
						 'type'=>'raw',
						'value' => '($data->obdt == 1 || $data->obdt== true)? \'<i class="fa fa-check"><i/>\' : \'<i class="fa fa-close"><i/>\'',
					),
					array(
						'class'=>'CButtonColumn',
						'template'=>'{view}{update}{erase}',
						'buttons'=>array(
							'view' => array(
								'label'=>'<i class="fa fa-eye icon_cbutton"></i>',
								'url'=>'Yii::app()->createUrl(strtolower(Yii::app()->controller->id).\'/view/id/\'. $data->id)',
								'visible'=>'Yii::app()->user->checkAccess(\'view\')',
								'options'=>array('title'=>'Ver','data-toggle'=>'tooltip'),
								'imageUrl'=>false,
							),
							'update' => array(
								'label'=>'<i class="fa fa-edit icon_cbutton"></i>',
								'visible'=>'Yii::app()->user->checkAccess(\'update\')',
								'url'=>'Yii::app()->createUrl(strtolower(Yii::app()->controller->id).\'/update/id/\'. $data->id)',
								'options'=>array('title'=>'Editar','data-toggle'=>'tooltip'),
								'imageUrl'=>false,
							),
							'erase' => array(
								'label'=>'<i class="fa fa-trash-o icon_cbutton"></i>',
								'url'=>'Yii::app()->createUrl(strtolower(Yii::app()->controller->id).\'/delete/id/\'. $data->id)',
								'visible'=>'Yii::app()->user->checkAccess(\'delete\')',
								'options' => array(
									'class' => 'delete_action',
									'title'=>'Borrar',
									'imageUrl'=>false,
									'data-toggle'=>'tooltip'
								)
							),
						),
					),
				)
			)); ?>
		</div>
	</div>
 </section>
<?php Yii::app()->clientScript->registerCssFile(Yii::app()->params['cdnUrl'].'/plugins/lcswich/lc_switch-tts.css');
Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/lcswich/lc_switch.js',CClientScript::POS_END);
Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/lcswich/lc_switch-tts.js',CClientScript::POS_END);
 ?>
