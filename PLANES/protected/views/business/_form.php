<?php
/* @var $this BusinessController */
/* @var $model Business */
/* @var $form CActiveForm */
?>
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'business-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>
<div class="box-body">
<span class="right_f active-button">
	<?php echo $form->checkBox($model,'enabled',array('class'=>'lcs_check enabled')); ?>
	<?php echo $form->error($model,'enabled'); ?>
</span>
<br />
<div class="col-md-12 col-data">
<div class="row">
<div class="form-group col-md-6 required">
	<?php echo $form->labelEx($model,'title'); ?>
			<?php echo $form->textField($model,'title',array('size'=>50,'maxlength'=>50,'class'=>'form-control')); ?>
							<?php echo $form->error($model,'title'); ?>
</div>
<?php echo $form->hiddenField($model,'ext',array('size'=>12,'maxlength'=>12,'class'=>'form-control')); ?>
<?php echo $form->hiddenField($model,'filename'); ?>

<div class="form-group col-md-6">
	<?php echo $form->labelEx($model,'name'); ?>
			<?php echo $form->textField($model,'name',array('size'=>62,'maxlength'=>62,'class'=>'form-control letras')); ?>
							<?php echo $form->error($model,'name'); ?>
</div>
<div class="form-group col-md-6">
	<?php echo $form->labelEx($model,'email'); ?>
		<div class="input-group">	<?php echo $form->EmailField($model,'email',array('size'=>80,'maxlength'=>80,'class'=>'form-control')); ?>
			<div class="input-group-addon pointer"><i class="fa fa-envelope-o"></i></div>	</div>			<?php echo $form->error($model,'email'); ?>
</div>
<div class="form-group col-md-6">
	<?php echo $form->labelEx($model,'phone'); ?>
		<div class="input-group">	<?php echo $form->textField($model,'phone',array('size'=>35,'maxlength'=>35,'class'=>'form-control phone')); ?>
		<div class="input-group-addon pointer"><i class="fa fa-phone"></i></div>		</div>			<?php echo $form->error($model,'phone'); ?>
</div>
<div class="form-group col-md-6">
	<?php echo $form->labelEx($model,'address'); ?>
			<?php echo $form->textField($model,'address',array('size'=>50,'maxlength'=>50,'class'=>'form-control')); ?>
							<?php echo $form->error($model,'address'); ?>
</div>
<div class="col-md-6 form-group">
	<?php echo $form->labelEx($model,'order'); ?>
	<?php

	if(Yii::app()->controller->action->id == 'create'){
		$order=count($order) + 1;
	} else{
		$order=count($order);
	}
	$order_array=array();
	for($i=1;$i<=$order;$i++){
		$order_array[]=array('id'=>$i,'title'=>$i);
	}
	echo $form->dropDownList($model, 'order',CHtml::listData($order_array, 'id', 'title'),array('empty' => 'Seleccione...','class'=>'form-control '));?>
	<div class="errorMessage"><?php if(isset($errors['order'])){ echo $errors['order']; } ?></div>
</div>
<div class="form-group col-md-6">
	<?php echo $form->labelEx($model,'obdt'); ?>
	<?php echo $form->checkBox($model,'obdt',array('class'=>'checkbox_form')); ?>
	<?php echo $form->error($model,'obdt'); ?>
</div>
<div class="box-footer">
		<?php
		$title = (Yii::app()->controller->action->id!='view') ? 'Volver sin guardar cambios' : 'Volver';
		echo '<a href="'.Yii::app()->request->baseUrl.'/'.strtolower(Yii::app()->controller->id).'" class="btn btn-secundary" data-toggle="tooltip" title="'.$title.'" >Cerrar</a>';
		if(Yii::app()->controller->action->id!='view'){
			echo CHtml::submitButton('Guardar',array('class'=>'btn btn-primary','data-toggle'=>'tooltip','title'=>'Guardar cambios'));
		}
		?></div>
<?php $this->endWidget(); ?>
<div class="col-md-4 f2_custom">
	<?php echo $form->labelEx($model,'filename'); ?>
	<form class="dropzone needsclick dz-clickable" id="dropzone">
		<?php
			if($model->filename!=''){
				echo '<div class="file_uploaded_class" style="display:block;"><i class="fa fa-times delete_img_i" onclick="$(\'#Business_filename\').val(\'\');$(\'#Business_ext\').val(\'\');$(\'.file_uploaded_class\').html(\'\');$(\'.file_uploaded_class\').css(\'display\',\'none\');"></i><img src="'.Yii::app()->request->baseUrl.'/uploads/tmp/'.Yii::app()->user->id.'/'.$model->filename.'/'.$model->filename.'.'.$model->ext.'" class="center_image" height="100%"/></div>';
			}else{
				echo '<div class="file_uploaded_class"></div>';
			}
		?>
	  <div class="dz-message needsclick">
		Arrastra los archivos aca o clickea para subir un archivo.<br>
		<span class="note needsclick">(No hay ningun archivo seleccionado.)</span>
	  </div>
	</form>
	<?php echo $form->error($model,'filename'); ?>	
</div>
</div><!-- form -->
</div>
</div>
<?php
	Yii::app()->clientScript->registerCssFile(Yii::app()->params['cdnUrl'].'/css/dropzone.css');
	Yii::app()->clientScript->registerCssFile(Yii::app()->params['cdnUrl'].'/css/dropzone_one.css');
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/js/dropzone.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerCssFile(Yii::app()->params['cdnUrl'].'/plugins/lcswich/lc_switch-tts.css');
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/lcswich/lc_switch.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/lcswich/lc_switch-tts.js',CClientScript::POS_END);

echo '<script>viewPage = "'.Yii::app()->controller->action->id.'";</script>';
Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/js/form-validators.js',CClientScript::POS_END) ?>
<script>
$( document ).ready(function() {
	Dropzone.autoDiscover = false;
	$("#dropzone").dropzone({
		url: homeurl+"/configimages/uploadone/1",
		maxFilesize: 100,
		uploadMultiple:false,
		maxFiles:100,
		previewsContainer:false,
		paramName: "file",
		maxThumbnailFilesize: 5,
		init: function() {
		 this.on('success', function(file, responseText) {
			result=JSON.parse(responseText);
				$('#Business_filename').val(result[0].name);
				$('#Business_ext').val(result[0].ext);
				$('.file_uploaded_class').css('display','block');
				$('.file_uploaded_class').html('<i class="fa fa-times delete_img_i" onclick="$(\'#Business_filename\').val(\'\');$(\'#Business_ext\').val(\'\');$(\'.file_uploaded_class\').html(\'\');$(\'.file_uploaded_class\').css(\'display\',\'none\');"></i><img src="'+homeurl+'/'+result[0].url+'" class="center_image" height="100%"/>');
		  });
		}
	});
	<?php if(Yii::app()->controller->action->id=='view'){ echo "$('form').find('input, textarea, button, checkbox, select').attr('disabled','disabled');"; } ?>
});
</script>
<?php
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/js/dropzone.js',CClientScript::POS_END);
?>
