<?php
/* @var $this LegalController */
/* @var $model Legal */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

<div class="col-md-12" style="clear: both">
	<div class="row">

	<div class="col-md-4">
	<div class="row row_space">
		<?php echo $form->label($model,'title'); ?>
		<?php echo $form->textField($model,'title',array('size'=>45,'maxlength'=>45)); ?>
	</div>
	</div>

	
	<div class="col-md-4">
	<div class="row row_space">
		<?php echo $form->labelEx($model,'id_entity'); ?>
		<?php echo $form->dropDownList($model, 'id_entity',CHtml::listData(Entity::model()->findAll(), 'id', 'name'),array('empty' => 'Seleccione','class'=>'form-control'));?>
		<?php echo $form->error($model,'id_entity'); ?>
	</div>
	</div>

	

	</div>
	<div class="row buttons">
		<?php echo CHtml::submitButton('Buscar',array('class'=>'btn btn-default b_customize')); ?>
	</div>
<br />
<?php $this->endWidget(); ?>

</div>
</div><!-- search-form -->