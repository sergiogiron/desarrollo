<?php
/* @var $this LegalController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Legals',
);

$this->menu=array(
	array('label'=>'Create Legal', 'url'=>array('create')),
	array('label'=>'Manage Legal', 'url'=>array('admin')),
);
?>

<h1>Legals</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
