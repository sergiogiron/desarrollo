<?php
/* @var $this LegalController */
/* @var $model Legal */
/* @var $form CActiveForm */
?>

<div class="box-body">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'legal-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>
	<span class="right_f active-button">
		<?php echo $form->checkBox($model,'enabled',array('class'=>'lcs_check enabled')); ?>
	</span>
	<br>
	<div class="col-md-12 col-data">

	<div class="form-group col-md-6 required">
		<label>Título </label> <i class="fa fa-key" aria-hidden="true"></i>
		<?php echo $form->textField($model,'title',array('size'=>60,'maxlength'=>255,'class'=>'form-control')); ?>
		<?php echo $form->error($model,'title'); ?>
	</div>

	<div class="form-group col-md-6 required">
		<?php echo $form->labelEx($model,'id_entity'); ?>
		<?php echo $form->dropDownList($model, 'id_entity',CHtml::listData(Entity::model()->findAll(), 'id', 'title'),array('empty' => 'Seleccione','class'=>'form-control'));?>
		<?php echo $form->error($model,'id_entity'); ?>
	</div>




	<div class="form-group col-md-12 required">
		<?php echo $form->labelEx($model,'description'); ?>
		<?php echo $form->textArea($model,'description',array('rows'=>6, 'cols'=>50,'class'=>'form-control ckeditor')); ?>
		<?php echo $form->error($model,'description'); ?>
	</div>
	<div class="box-footer">
		<?php
			$title = (Yii::app()->controller->action->id!='view') ? 'Volver sin guardar cambios' : 'Volver';
			echo '<a href="'.Yii::app()->request->baseUrl.'/'.strtolower(Yii::app()->controller->id).'" class="btn btn-secundary cancel" data-toggle="tooltip" title="'.$title.'" >Cerrar</a>';
			if(Yii::app()->controller->action->id!='view'){
				echo CHtml::submitButton('Guardar',array('class'=>'btn btn-primary save','data-toggle'=>'tooltip','title'=>'Guardar cambios'));
			}
		?>
	</div>

</div>
  </div>
  </div>

<?php $this->endWidget(); ?>

</div><!-- form -->

<?php Yii::app()->clientScript->registerScriptFile('https://cdn.ckeditor.com/4.5.7/standard/ckeditor.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerCssFile(Yii::app()->params['cdnUrl'].'/plugins/lcswich/lc_switch-tts.css');
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/lcswich/lc_switch.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/lcswich/lc_switch-tts.js',CClientScript::POS_END);
?>
  <script type="text/javascript">
	<?php
		if(Yii::app()->controller->action->id=='view'){ echo "$('form').find('input, textarea, checkbox, select').attr('disabled','disabled'); $('.fa-pencil-square-o').hide()"; }
	?>
</script>
