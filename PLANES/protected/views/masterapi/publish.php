<?php echo $this->renderPartial('//layouts/publish_sc',array('title'=>$title,'breadcrumb'=>$breadcrumb));  ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/plugins/select2/select2.full.js'); ?>
<?php Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl.'/js/plugins/select2/select2.css'); ?>
<?php Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl.'/plugins/datepicker/datepicker3.css'); ?>
<?php Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl.'/plugins/timepicker/bootstrap-timepicker.css'); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/plugins/datepicker/bootstrap-datepicker.js'); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/plugins/timepicker/bootstrap-timepicker.js'); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/plugins/input-mask/jquery.inputmask.js'); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/plugins/input-mask/jquery.inputmask.date.extensions.js'); ?>
<script>
$( document ).ready(function() {
	$("[data-mask]").inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/aaaa"});

	$( ".publicar" ).click(function(event){
		event.preventDefault();
		if($('#hour').val()!='' && $('#date').val()!='' && $('#routesall').val()!=''){
			$.ajax({
			  method: "POST",
			  url: homeurl+'/masterapi/publish',
			  data: $('#publication_form').serialize(),
			}).complete(function(result) {
				$('.close_window').trigger('click');					
				//if(result.responseText==''){
				//}else{
					//$('.error_c').html(result.responseText);
				//}
			});
		}else{
			alert('Debe completar todos los campos');
		}
	});	
    $('.datepicker').datepicker({
      autoclose: true, 
	  format: 'dd-mm-yyyy',
	  firstDay: 1
    });
    $(".timepicker").timepicker({
      showInputs: false,
	  showMeridian:false
    });
});	
</script>	
