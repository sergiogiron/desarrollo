<?php
/* @var $this MasterapiController */
/* @var $model Masterapi */
/* @var $form CActiveForm */
?>
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'masterapi-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>
<div class="box-body">
	<span class="right_f">
<?php echo $form->checkBox($model,'enabled',array('class'=>'lcs_check enabled')); ?>
	
	</span>
<h2>Configuración de Master Api</h2>
<div class="col-md-12 col-data">
<div class="form-group col-md-6 required">
	<?php echo $form->labelEx($model,'name'); ?>
			<?php echo $form->textField($model,'name',array('size'=>32,'maxlength'=>32,'class'=>'form-control')); ?>
							<?php echo $form->error($model,'name'); ?>
</div>

<div class="form-group col-md-3 required">
	<?php echo $form->labelEx($model,'rules_time'); ?>
			<?php echo $form->textField($model,'rules_time',array('class'=>'form-control')); ?>
							<?php echo $form->error($model,'rules_time'); ?>
</div>

<div class="form-group col-md-3 required">
	<?php echo $form->labelEx($model,'expire_apy_key'); ?><br>
			<?php echo $form->checkBox($model,'expire_apy_key',array('class'=>'checkbox_form')); ?>
							<?php echo $form->error($model,'expire_apy_key'); ?>
</div>

<div class="box-footer">
		<?php
		$title = (Yii::app()->controller->action->id!='view') ? 'Volver sin guardar cambios' : 'Volver';
		echo '<a href="'.Yii::app()->request->baseUrl.'/'.strtolower(Yii::app()->controller->id).'" class="btn btn-secundary cancel" data-toggle="tooltip" title="'.$title.'" >Cerrar</a>';
		if(Yii::app()->controller->action->id!='view'){
			echo CHtml::submitButton('Guardar',array('class'=>'btn btn-primary save','data-toggle'=>'tooltip','title'=>'Guardar cambios'));
		}
		?></div>
<?php $this->endWidget(); ?>

</div><!-- form -->
</div>
<?php
	Yii::app()->clientScript->registerCssFile(Yii::app()->params['cdnUrl'].'/plugins/lcswich/lc_switch-tts.css'); 
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/lcswich/lc_switch.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/lcswich/lc_switch-tts.js',CClientScript::POS_END);
 ?> 
<script>
$( document ).ready(function() {
	<?php if(Yii::app()->controller->action->id=='view'){ echo "$('form').find('input, textarea, button, checkbox, select').attr('disabled','disabled');"; } ?>});		
</script>