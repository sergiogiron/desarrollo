<?php 

if(isset($formapago->credit_card->paymentMethod->pricing) && $formapago->credit_card->paymentMethod->pricing != ''){ 
?>
<h1><i class="fa fa-credit-card"></i> Forma de pago</h1>
<div class="row">
 	<div class="col-xs-12">
	 	<div class="row">
	 		<div class="col-xs-6">Banco: <?=$formapago->entity;?></div>
	 		<div class="col-xs-6">Forma de pago: <?=$formapago->credit_card->credit_card;?></div>
	 		<div class="col-xs-3">Cuotas: <?=$formapago->credit_card->paymentMethod->dues;?></div>
	 		<div class="col-xs-3">Intereses: <?=$formapago->credit_card->paymentMethod->interest;?>%</div>
	 		<div class="col-xs-3">CFT: <?=$formapago->credit_card->paymentMethod->cftn;?></div>
	 		
	 	</div>
	</div>
</div>
<h1><i class="fa fa-usd"></i> Datos precios</h1>
<div class="row">
 	<div class="col-xs-12">
	 	<div class="row">
	 		<div class="col-xs-6">Total a cobrar: $ <?=$formapago->credit_card->paymentMethod->pricing->TotalFare;?></div>
	 		<div class="col-xs-6">FEE: $ <?=$formapago->credit_card->paymentMethod->pricing->FEE;?></div>
	 		<div class="col-xs-6">Total Neto: $ <?=$formapago->credit_card->paymentMethod->pricing->NetFare;?></div>
	 		<div class="col-xs-6">Impuestos: $ <?=$formapago->credit_card->paymentMethod->pricing->Taxes;?></div>
	 		<div class="col-xs-6">IVA: $ <?=$formapago->credit_card->paymentMethod->pricing->IVA;?></div>
	 		<div class="col-xs-6">Descuento: <?=$formapago->credit_card->paymentMethod->pricing->Discount?> (<?=$formapago->credit_card->paymentMethod->pricing->PercentDiscount;?>%)</div>
	 		<div class="col-xs-6">Precio por pasajero: $ <?=$formapago->credit_card->paymentMethod->pricing->PricePerPax;?></div>
	 		<div class="col-xs-6">Impuestos tasas y cargos: $ <?=$formapago->credit_card->paymentMethod->pricing->TaxesAndOtherCharges;?></div>
	 		<div class="col-xs-6">Precio Adulto: $ <?=$formapago->credit_card->paymentMethod->pricing->NetAdultsFare;?></div>
	 		<div class="col-xs-6">Precio Menor: $ <?=$formapago->credit_card->paymentMethod->pricing->NetChildrenFare;?></div>
	 		<div class="col-xs-6">Precio Infante: $ <?=$formapago->credit_card->paymentMethod->pricing->NetInfantsFare;?></div>
	 		
	 	</div>
	</div>
</div>
<?php } ?>
<h1><i class="fa fa-university"></i> Rewards Points</h1>
<div class="row">
 	<div class="col-xs-12">
	 		<div class="col-xs-6">Puntos: <?=$formapago->credit_card->paymentMethod->rewards->points;?></div>
	 	<div class="row">
	 	</div>
	</div>
</div>
		