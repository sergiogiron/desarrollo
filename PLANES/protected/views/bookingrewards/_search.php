<?php
/* @var $this BookingController */
/* @var $model Booking */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>
<div class="col-md-12" style="clear: both">
<div class="row">
			
			<div class="col-md-3">
				<div class="row row_space">
					<?php echo $form->label($model,'id'); ?>
					<?php echo $form->textField($model,'id',array('class'=>'form-control')); ?>
				</div>
			</div>
			
			<div class="col-md-3">
				<div class="row row_space">
					<?php echo $form->label($model,'checkoutId'); ?>
					<?php echo $form->textField($model,'checkoutId',array('size'=>20,'maxlength'=>20,'class'=>'form-control')); ?>
				</div>
			</div>
			<!-- 
			<div class="col-md-3">
				<div class="row row_space">
					<?php echo $form->label($model,'id_channel'); ?>
					<?php echo $form->textField($model,'id_channel',array('class'=>'form-control')); ?>
				</div>
			</div>
			
			<div class="col-md-3">
				<div class="row row_space">
					<?php echo $form->label($model,'id_subchannel'); ?>
					<?php echo $form->textField($model,'id_subchannel',array('class'=>'form-control')); ?>
				</div>
			</div>
			
			<div class="col-md-3">
				<div class="row row_space">
					<?php echo $form->label($model,'status'); ?>
					<?php echo $form->textField($model,'status',array('size'=>20,'maxlength'=>20,'class'=>'form-control')); ?>
				</div>
			</div>
			-->
			<div class="col-md-3">
				<div class="row row_space">
					<?php echo $form->label($model,'passenger'); ?>
					<?php echo $form->textField($model,'passenger',array('size'=>50,'maxlength'=>50,'class'=>'form-control')); ?>
				</div>
			</div>
			
			<div class="col-md-3">
				<div class="row row_space">
					<?php echo $form->label($model,'date'); ?>
					<?php echo $form->textField($model,'date',array('class'=>'form-control')); ?>
				</div>
			</div>
			
			<div class="col-md-3">
				<div class="row row_space">
					<?php echo $form->label($model,'book_code'); ?>
					<?php echo $form->textField($model,'book_code',array('size'=>20,'maxlength'=>20,'class'=>'form-control')); ?>
				</div>
			</div>
			
			<div class="col-md-3">
				<div class="row row_space">
					<?php echo $form->label($model,'package'); ?>
					<?php echo $form->textField($model,'package',array('size'=>50,'maxlength'=>50,'class'=>'form-control')); ?>
				</div>
			</div>
			
			<div class="col-md-3">
				<div class="row row_space">
					<?php echo $form->label($model,'expire_date'); ?>
					<?php echo $form->textField($model,'expire_date',array('class'=>'form-control')); ?>
				</div>
			</div>
			<!--
			<div class="col-md-3">
				<div class="row row_space">
					<?php echo $form->label($model,'json'); ?>
					<?php echo $form->textArea($model,'json',array('rows'=>6, 'cols'=>50,'class'=>'form-control')); ?>
				</div>
			</div>
			
			<div class="col-md-3">
				<div class="row row_space">
					<?php echo $form->label($model,'email_client'); ?>
					<?php echo $form->textArea($model,'email_client',array('rows'=>6, 'cols'=>50,'class'=>'form-control')); ?>
				</div>
			</div>
			
			<div class="col-md-3">
				<div class="row row_space">
					<?php echo $form->label($model,'email_operation'); ?>
					<?php echo $form->textArea($model,'email_operation',array('rows'=>6, 'cols'=>50,'class'=>'form-control')); ?>
				</div>
			</div>
			
			<div class="col-md-3">
				<div class="row row_space">
					<?php echo $form->label($model,'consumer'); ?>
					<?php echo $form->textField($model,'consumer',array('size'=>55,'maxlength'=>55,'class'=>'form-control')); ?>
				</div>
			</div>
			
			<div class="col-md-3">
				<div class="row row_space">
					<?php echo $form->label($model,'id_providers'); ?>
					<?php echo $form->textField($model,'id_providers',array('class'=>'form-control')); ?>
				</div>
			</div>
			-->
</div>
	<div class="row buttons">
		<?php echo CHtml::submitButton('Buscar',array('class'=>'btn btn-default b_customize')); ?>
	</div>
<br />
<?php $this->endWidget(); ?>
</div>
</div><!-- search-form -->