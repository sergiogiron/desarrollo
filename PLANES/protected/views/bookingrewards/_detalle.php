<article class="detalle detailPack">
	<h1><i class="fa fa-info-circle"></i> Detalle del paquete</h1>
	<div class="table">
		<div class="masck-slide" style="background:url('<?=$result['imagen'];?>'); height:150px;background-size:cover;">
			<!-- <img src="<?=$result['imagen'];?>" /> -->
		</div>
	</div>
	<h2>Paquete a <?=$result['nombre'];?></h2>
	<p class="check descCorta"><i class="fa fa-calendar"></i><?=$result['cantidad_noches']?> noches - Salida: <span></i><?=Utils::date_spa($result['desde'])?></span></p>
	<!-- Dinamico: Los check se muestran o no segun este contemplado en el paquete -->
	<?php if($result['aereo']==1){ ?>
		<p class="check aereos"><i class="fa fa-plane"></i> <?=$result['detalle_aereo'];?></p>
	<?php } ?>
	<?php if($result['hotel']==1){ ?>
		<p class="check hotel">
			<i class="fa fa-hotel"></i>
			<?php 
				$string_hot = '';
				foreach ($result['hoteles'] as $key => $hotel) {
					$string_hot .= $hotel['noches']." Noches en ".$hotel['codigo']. " + ";
				}
				echo rtrim($string_hot, " + ");
			?>
		</p>
	<?php } ?>
	<?php if($result['traslados']==1){ ?>
		<p class="check traslados"><i class="fa fa-bus"></i> <?=$result['transfer'][0]['nombre']?></p>
	<?php } ?>
	<?php if($result['asistencia_viajero']==1){ ?>
		<p class="check asistencia"><i class="fa fa-medkit"></i> Asistencia al viajero</p>
	<?php } ?>
	<h2>Transportes</h2>
	<?php 
		$ida 	= $result['vuelos'][0];
		$vuelta = $result['vuelos'][1];
	?>
	<table class="transportes">
        <thead>
            <tr>
                <td></td>
                <td>Origen</td>
                <td>Destino</td>
                <td>Salida</td>
                <td class="center">Escalas</td>
                <td>LLegada</td>
                <td>Aerolinea</td>
                <td></td>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td class="aereos"><i class="fa fa-plane icon_cbutton"></i></td>
                <td><?=$ida['ciudad_salida'];?></td>
                <td><?=$ida['ciudad_llegada'];?></td>
                <td>	
					<?php
						setlocale(LC_TIME, 'es_AR.UTF-8');
						$fecha_salida =	new DateTime($ida['fecha_salida']);
						echo strftime("%A %d de %B, %Hhs", $fecha_salida->getTimestamp());
					?>
            	</td>
                <td class="center"><?=$ida['escalas'];?></td>
                <td>
	                <?php 
	                	$fecha_llegada =	new DateTime($ida['fecha_llegada']);
						echo strftime("%A %d de %B, %Hhs", $fecha_llegada->getTimestamp());
					?>
				</td>
                <td><img src="<?=$ida['aerolinea_imagen'];?>"></td>
                <td class="detalle"><button  data-detail="ida" class="detalle_vuelo btn">Ver Detalle</button></td>
            </tr>
            <?php foreach ($ida['escalas_detalles'] as $key => $escalas) { ?>
            	<tr class="ida_detalle hidden">
	        		<td>Nro. <?=$escalas['nro_vuelo']?></td>
	                <td><?=$escalas['aeropuerto_salida'];?></td>
	                <td><?=$escalas['aeropuerto_llegada'];?></td>
	                <td>
					 	<?php 
	                		$fecha_salida =	new DateTime($escalas['fecha_salida']);
							echo strftime("%d/%m/%Y %R", $fecha_salida->getTimestamp());
						?>
	                </td>
	                <td class="center">Vuelo clase: <?=$escalas['clase_vuelo'];?></td>
	                <td> 
		                <?php 
		                	$fecha_llegada =	new DateTime($escalas['fecha_llegada']);
							echo strftime("%d/%m/%Y %R", $fecha_llegada->getTimestamp());
						?>
					</td>
	                <td><img src="<?=$escalas['aerolinea_imagen'];?>"></td>
	                <td class="detalle"></td>
            	</tr>
           <?php  } ?>
            <tr>
                <td class="aereos"><i class="fa fa-plane icon_cbutton fa-rotate-180"></i></td>
                <td><?=$vuelta['ciudad_salida'];?></td>
                <td><?=$vuelta['ciudad_llegada'];?></td>
                <td>
				 	<?php 
                		$fecha_salida =	new DateTime($vuelta['fecha_salida']);
						echo strftime("%A %d de %B, %H hs", $fecha_salida->getTimestamp());
					?>
                </td>
                <td class="center"><?=$vuelta['escalas'];?></td>
                <td> 
	                <?php 
	                	$fecha_llegada =	new DateTime($ida['fecha_llegada']);
						echo strftime("%A %d de %B, %H hs", $fecha_llegada->getTimestamp());
					?>
				</td>
                <td><img src="<?=$vuelta['aerolinea_imagen'];?>"></td>
                <td class="detalle"><button data-detail="vuelta" class="detalle_vuelo btn">Ver Detalle</button></td>
            </tr>
             <?php foreach ($vuelta['escalas_detalles'] as $key => $escalas) { ?>
            	<tr class="vuelta_detalle hidden">
	        		<td>Nro. <?=$escalas['nro_vuelo']?></td>
	                <td><?=$escalas['aeropuerto_salida'];?></td>
	                <td><?=$escalas['aeropuerto_llegada'];?></td>
	                <td>
					 	<?php 
	                		$fecha_salida =	new DateTime($escalas['fecha_salida']);
							echo strftime("%d/%m/%Y %R", $fecha_salida->getTimestamp());
						?>
	                </td>
	                <td class="center">Vuelo clase: <?=$escalas['clase_vuelo'];?></td>
	                <td> 
		                <?php 
		                	$fecha_llegada =	new DateTime($escalas['fecha_llegada']);
							echo strftime("%d/%m/%Y %R", $fecha_llegada->getTimestamp());
						?>
					</td>
	                <td><img src="<?=$escalas['aerolinea_imagen'];?>"></td>
	                <td class="detalle"></td>
            	</tr>
           <?php  } ?>
        </tbody>
    </table>

	<h2>Hotel</h2>

	<?php foreach ($result['hoteles'] as $key => $hotel) { ?>
		<h3 class="hotelName"> <span id="hotelName"><?=$hotel['nombre']?></span></h3>
		<?php for($z=0; $z<$hotel['hotel_clase']; $z++){ ?>
			<i class="fa fa-star "></i>
		<?php } ?>

		<div class="hotelHeader">
			<div class="col-xxs-12 col-xs-6 hotel-slide">
				<div id="carousel-example-generic<?=$key;?>" class="carousel  slide" data-ride="carousel">
					<ol class="carousel-indicators">
						<?php  for ($i=0; $i < count($hotel['imagenes']); $i++) { ?>
							<li data-target="#carousel-example-generic<?=$key;?>" data-slide-to="<?=$i;?>" class="<?=$i==0?'active':'';?>"></li>
						<?php } ?>
					</ol>	
					<div class="carousel-inner" role="listbox">
						<?php foreach ($hotel['imagenes'] as $key => $value) { ?>
							<div class="item <?=$key==0?'active':'';?>">
								<img src="<?=$value['url'];?>" class="responsive_carr" />
								<div class="carousel-caption"></div>
							</div>
						<?php  } ?>
					</div>
				</div>
			</div>
			<div class="col-xxs-12 col-xs-6 ">
				<p><strong>Noches:</strong> <?=$hotel['noches']?></p>
				<p><strong>Habitación:</strong> <?=$hotel['cat_habitacion']?></p>
				<p><strong>Tipo:</strong> <?=$hotel['tipo_habitacion']?></p>
				<p><strong>Entrada:</strong> <?=Utils::date_spa($hotel['checkin'])?></p>
				<p><strong>Salida:</strong> <?=Utils::date_spa($hotel['checkout'])?></p>
			</div>

			<div class="col-sm-12 col-md-12 descripcion hidden "  id="desc_hot_<?=$key;?>">
				<?=base64_decode($hotel['informacion']);?>
			</div>
			<hr />
			<!-- Reemplazar imagen por iconFont para que se pueda ajustar con css -->
			<p class="moreInfo" data-idhotel="<?=$key;?>" style="text-align:center;"><span>Ver m&aacute;s</span><i class="fa fa-caret-down"></i></p>
		</div>
	<?php } ?>
</article>
<script type="text/javascript">
	
	$('.carousel').carousel();
	
	

	$('.moreInfo').click(function(){
		var id_hotel = $(this).data('idhotel');
		$(this).find('i').toggleClass('fa-caret-up');
		$('#desc_hot_'+id_hotel).toggleClass('hidden');
		var text = 	$(this).contents('span').text();
		//console.log($(this).contents('span').text());
    	$(this).contents('span').text(
        	text == "Ver más" ? "Ver menos" : "Ver más");
	});

	$('.cajaPrecio').on('click','#next',function(){
		$(this).addClass('cargando');
		//$(this).removeAttr('href');
	});

	$('.detalle_vuelo').on('click',function(){
		var tipo = $(this).data('detail');
		$('.'+tipo+'_detalle').toggleClass('hidden');
	})
</script>