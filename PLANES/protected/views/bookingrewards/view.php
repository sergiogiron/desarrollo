<div class="nav-tabs">
	<ul class="nav nav-tabs">
		<li class="active"><a href="#tab_1" data-toggle="tab">Detalle</a></li>
		<li><a href="#tab_2" data-toggle="tab">Pasajeros</a></li> 
		<li><a href="#tab_3" data-toggle="tab">Datos de contacto</a></li> 
		<li><a href="#tab_4" data-toggle="tab">Forma de pago</a></li> 
		<li><a href="#tab_5" data-toggle="tab">Total a pagar a operador</a></li> 
		<li><a href="#tab_6" data-toggle="tab">Historial</a></li> 
	</ul>
	<div class="tab-content">
		<div class="tab-pane active" id="tab_1">
			<?php 
				$this->renderPartial('_detalle',array(
					'result'=>$paquete
				)); 
			?>
		</div>		
		<div class="tab-pane" id="tab_2">
			<?php 
				$this->renderPartial('_pasajeros',array(
					'pasajeros'=>$pasajeros
				)); 
			?>
		</div>
		<div class="tab-pane" id="tab_3">
			<?php 
				$this->renderPartial('_contacto',array(
					'contacto'=>$contacto
				));
			?>
		</div>
		<div class="tab-pane" id="tab_4">
			<?php 
				$this->renderPartial('_formadepago',array(
					'formapago'=>$formapago
				)); 
			?>
		</div>
		<div class="tab-pane" id="tab_5">
			<?php 
				$this->renderPartial('_totaltopay',array(
					'total_to_pay'=>$total_to_pay,
					'proveedor'=>$proveedor
				)); 
			?>
		</div>
		<div class="tab-pane" id="tab_6">
			<?php 
				$this->renderPartial('_historial',array(
					'historial'=>$historial
				)); 
			?>
		</div>
	</div>
</div>
<?php 
	$url_confirm = Yii::app()->createUrl(strtolower(Yii::app()->controller->id).'/confirm/id/'.$checkout_id.'/proveedor/'.$id_provider.'/checkout_id/'.$id);
	$url_cancel = Yii::app()->createUrl(strtolower(Yii::app()->controller->id).'/cancel/id/'.$checkout_id.'/proveedor/'.$id_provider.'/checkout_id/'.$id);
	$url_comment = Yii::app()->createUrl(strtolower(Yii::app()->controller->id).'/comment/id/'.$id);
	$url_close = Yii::app()->createUrl(strtolower(Yii::app()->controller->id).'/close/id/'.$id);
	$url_liberate = Yii::app()->createUrl(strtolower(Yii::app()->controller->id).'/LiberateBlockedTask/id/'.$id);

?>
<div class="row col-md-8" style="position: fixed;bottom: 27px;">
	<?php if(($estado == 'Pendiente' || $estado == 'Confirmado') && $editable){ ?>
		<div class="col-md-4">
			<a href="<?=$url_cancel;?>" 	class="accion_reserva btn btn-danger">Cancelar reserva</a>
		</div>
	<?php } ?>
		<div class="col-md-4">
			<a href="<?=$url_comment;?>" id="openModal" class="btn btn-primary">Agregar Comentario</a>
		</div>
	<?php if(Yii::app()->user->checkAccess('freeblockedtask')){ ?>
		<div class="col-md-4">
			<a href="<?=$url_liberate;?>" class="accion_liberate btn btn-warning">Liberar Bloqueo</a>
		</div>
	<?php } ?>
	<?php if($estado == 'Pendiente' && $editable){ ?>
		<div class="col-md-4">
			<a href="<?=$url_confirm;?>" 	class="accion_reserva btn btn-success">Confirmar reserva</a>
		</div>
	<?php } ?>
</div>
<script type="text/javascript">
	$('.close_window').attr('url','<?=$url_close;?>');

	$('body').one('click','.close_window',function(event){
		event.preventDefault();
		var request = $.ajax({
			url: $(this).attr('url'),
		}).done(function(msg) {
			return true;
		});
	});
	$('body').one('click','.accion_liberate',function(event){
		event.preventDefault();
		var request = $.ajax({
			url: $(this).attr('href'),
		}).done(function(msg) {
			$('.close_window').trigger('click');
		});
	});
</script>
