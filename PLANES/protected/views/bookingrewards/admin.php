<?php
/* @var $this BookingController */
/* @var $model Booking */

$this->breadcrumbs=array(
	'Bookings'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Booking', 'url'=>array('index')),
	array('label'=>'Create Booking', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
search();
deleteBtn();

");
?>
<style type="text/css">
	.modal-body{
		height: 80vh;
		overflow: auto;
	}
	div.nav-tabs {
		margin-top:0px !important;
	}
</style>
<section class="content-header">
	<h1>
		Control de reservas
	</h1>
	<ol class="breadcrumb">
		<li><a href="/ttsoffice_frontend"><i class="fa fa-home"></i> Home</a></li>
		<li><a href="/ttsoffice_frontend/booking">Control de reservas</a></li>
	</ol>
</section>
<section class="row" style="    padding: 5px;margin-left: 8px;">
	<div class="col-md-4 search_bar">
		<div class="row">
			<div class="input-group input-group-sm">
				<input type="text" class="form-control" id="search_txt"><i class="fa fa-close right-icon-search"></i>
				<span class="input-group-btn">
				  <button type="button" id="search" class="btn btn-default btn-flat"><i class="fa fa-search"></i></button>
				</span>
			</div>
		</div>
	</div>
</section>
<section class="content">
	<div class="box box-danger">
		<div class="box-header with-border">	
			<div class="nav-tabs">
				<ul class="nav nav-tabs">
					<li class="active"><a href="#tab_1" data-toggle="tab">Pendientes</a></li>
					<li><a href="#tab_2" data-toggle="tab">Confirmadas</a></li> 
					<li><a href="#tab_3" data-toggle="tab">Canceladas</a></li> 
					<li><a href="#tab_4" data-toggle="tab">Caidas</a></li> 
				</ul>
				<div class="tab-content">
					<div class="tab-pane active" id="tab_1">
						<?php 
							$this->renderPartial('//booking/_panel',array(
								'id'=>'pendientes',
								'data'=>$pendientes
							)); 
						?>
					</div>
					<div class="tab-pane " id="tab_2">
						<?php 
							$this->renderPartial('//booking/_panel',array(
								'id'=>'confirmadas',
								'data'=>$confirmadas
							)); 
						?>
					</div>
					<div class="tab-pane " id="tab_3">
						<?php 
							$this->renderPartial('//booking/_panel',array(
								'id'=>'canceladas',
								'data'=>$canceladas
							)); 
						?>
					</div>
					<div class="tab-pane " id="tab_4">
						<?php 
							$this->renderPartial('//booking/_panel',array(
								'id'=>'caidas',
								'data'=>$caidas
							)); 
						?>
					</div>
				</div>
			</div>
		</div>
	</div>	
</section>


<script type="text/javascript">
 	$('body').on('click', ".accion_reserva" , function(event) {
		event.preventDefault();
		console.log($(this).attr('href'));
		var boton = $(this);
		var icono = $(this).find('i');
		var clase = icono.attr('class');
		icono.removeClass(clase);
		boton.css('pointer-events','none');
		icono.addClass('fa fa-spinner fa-spin fa-fw');
		var url = $(this).attr('href');
		swal({   
			title:"Esta seguro?",   
			text: "Va a cambiar el estado de la reserva!",   
			type: "warning",   
			showCancelButton: true,   
			confirmButtonColor: "#DD6B55",   
			confirmButtonText: "Si, cambiar!",   
			cancelButtonText: "Cancelar",
			showLoaderOnConfirm: true,   
			closeOnConfirm: false }, 
		function(isConfirm){   
			if (isConfirm) {
				var request = $.ajax({
					url: url,
				}).done(function(msg) {
					icono.removeClass('fa fa-spinner fa-spin fa-3x fa-fw');
					icono.addClass(clase);
					boton.css('pointer-events','auto');
					$.fn.yiiGridView.update('pendientes-grid');
					$.fn.yiiGridView.update('confirmadas-grid');
					$.fn.yiiGridView.update('canceladas-grid');
					$.fn.yiiGridView.update('caidas-grid');
					console.log(msg);
					if(msg == 'OK'){
						swal("Completado!", "Se cambio el estado correctamente", "success"); 
					}else{
						swal("Error!", "Ocurrio un error", "error"); 
					}
					//$('.modal-body').html(msg);
				});	
			}else{
				icono.removeClass('fa fa-spinner fa-spin fa-3x fa-fw');
				icono.addClass(clase);
				boton.css('pointer-events','auto');
			}
		});
	});

 	 $('body').on('click',".open_lightbox",function(event) {
		event.preventDefault();
		var boton = $(this);
		var icono = $(this).find('i');
		var clase = icono.attr('class');
		icono.removeClass(clase);
		boton.css('pointer-events','none');
		icono.addClass('fa fa-spinner fa-spin fa-fw');
		var request = $.ajax({
			url: $(this).attr('href'),
		}).done(function(msg) {
			icono.removeClass('fa fa-spinner fa-spin fa-3x fa-fw');
			icono.addClass(clase);
			boton.css('pointer-events','auto');
			$('.ligth-box-modal').css('display','block');
			$('.modal-content').css('display','block');
			$('.modal-title').html('Visualizando contenido');
			$('.modal-body').html(msg);
			$.fn.dataTableExt.oPagination.iFullNumbersShowPages = 3;
			
		});	
	});

	$('#search_txt').keypress(function(e){
	   if(e.which == 13) {
		  $('#search').trigger('click');
	   }
	});	
	$('#search').click(function(){
		openLoading();
		var search = $('#search_txt').val();
		var request = $.ajax({
			url: "<?=Yii::app()->createUrl(strtolower(Yii::app()->controller->id))?>"+"/search",
			type:'POST',
			data:{
				buscar:search
			}
		}).done(function(msg) {
			closeLoading();
		/*	icono.removeClass('fa fa-spinner fa-spin fa-3x fa-fw');
			icono.addClass(clase);
			boton.css('pointer-events','auto');*/
			$('.ligth-box-modal').css('display','block');
			$('.modal-title').html('Visualizando contenido');
			$('.modal-body').html(msg);
		}).fail(function( jqXHR, textStatus, errorThrown ) {
			closeLoading();
		     if ( console && console.log ) {
		         console.log( "La solicitud a fallado: " +  textStatus);
		     }
		});
		/*$('#booking-grid-caidas').yiiGridView('update', {
			data: { criterio_caidas : $('#criterio_caidas').val() }
		});
		return false;*/
	});	

	$(document).ready(function(){
		<?php if($id_blocked){ ?>
			var url = '<?=Yii::app()->createUrl(strtolower(Yii::app()->controller->id).'/view/id/'.$id_blocked);?>';
			var request = $.ajax({
				url:url,
			}).done(function(msg) {
				$('.ligth-box-modal').css('display','block');
				$('.modal-content').css('display','block');
				$('.modal-title').html('Visualizando contenido');
				$('.modal-body').html(msg);

				$.fn.dataTableExt.oPagination.iFullNumbersShowPages = 3;
			
			});
			<?php } ?>
	});
 </script>
 <?php
Yii::app()->clientScript->registerCssFile(Yii::app()->params['cdnUrl'].'/plugins/datatables/dataTables.bootstrap.css');
Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/datatables/jquery.dataTables.min.js',CClientScript::POS_END);
Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/datatables/dataTables.bootstrap.min.js',CClientScript::POS_END);
?>
