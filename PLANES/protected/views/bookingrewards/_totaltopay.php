<h1><i class="fa fa-money"></i> Total a pagar al Operador</h1>
<h3><b><?=$proveedor->title;?></b></h3>
<div class="row">
 	<div class="col-xs-12 col-sm-9">
	 	<div class="row">
	 		<div class="col-xs-6">Neto: $<?=$total_to_pay->Net;?></div>
	 		<div class="col-xs-6">IVA: $<?=$total_to_pay->Vat;?></div>
	 		<div class="col-xs-6">Impuesto: $<?=$total_to_pay->Tax;?></div>
	 		<div class="col-xs-6">Moneda operador: <?=$total_to_pay->Currency;?></div>
	 		<div class="col-xs-6">Ratio conversi&oacute;n operador: <?=$total_to_pay->RatioProvider;?>%</div>
	 	</div>
	</div>
</div>
		