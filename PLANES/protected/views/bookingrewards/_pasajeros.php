<h1><i class="fa fa-hotel"></i> Pasajeros</h1>
<div class="row">
	<?php foreach ($pasajeros as $key => $pasajero) { ?>
		 	<div class="col-xs-12 col-sm-6">
		 		<h4><?=$pasajero->TipoPasajero;?> - Habitación <?=$pasajero->Room;?></h4>
			 	<div class="row">
			 		<div class="col-xs-6">Nombre: <?=$pasajero->FirstName;?></div>
			 		<div class="col-xs-6">Apellido: <?=$pasajero->LastName;?></div>
			 		<div class="col-xs-6">Tipo Documento: <?=$pasajero->DocumentType;?></div>
			 		<div class="col-xs-6">Nro. Documento: <?=$pasajero->DocumentNumber;?></div>
			 		<div class="col-xs-6">Genero: <?=($pasajero->Gender=='F')?'Femenino':'Masculino'?></div>
			 		<div class="col-xs-6">Fecha Nac.: <?php $fecha_nac = new DateTime($pasajero->Birthdate); echo strftime('%d/%m/%Y' , $fecha_nac->getTimestamp()) ?></div>
			 		<div class="col-xs-6">Nacionalidad: <?=$pasajero->Nationality;?></div>
			 		<div class="col-xs-6">Residencia: <?=$pasajero->Residence;?></div>
				</div>
			</div>
	<?php } ?>
</div>
		