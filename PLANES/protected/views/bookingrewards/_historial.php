<style>
	.long{
		display: none;
	}
	.comment{
		max-width: 300px;
	}
</style>
<table class="datatable table table-striped" style="width: 100%;">
	<thead>
		<tr>
			<th>Usuario</th>
			<th>Fecha</th>
			<th>Acción</th>
			<th>Comentario</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($historial as $key => $value) { ?>
			<tr class="tr<?=$key;?>">
				<td><?=$value['user'];?></td>
				<td><?=$value['date'];?></td>
				<td><?=$value['type'];?></td>
				<td class="comment">
					<span class="short<?=$key;?>">
						<?=($value['comment']!='')?(strlen($value['comment'])>20)?substr($value['comment'],0,20).'...':$value['comment']:'-';?>
					</span>
					<?php if(strlen($value['comment'])>20){ ?>
						<span class="long long<?=$key;?>">
							<?=$value['comment'];?>
						</span>
						<i class="fa fa-arrow-down show-hide" data-key="<?=$key;?>"></i>
					<?php } ?>
				</td>

			</tr>
		<?php } ?>
	</tbody>
</table>

<script type="text/javascript">
	var datatable = $('.datatable').DataTable({
        //"bLengthChange": false,
      //  "bInfo": false,
       // "bAutoWidth": false,
        "pagingType": "full_numbers",
        "sDom": 'tp',
        "iDisplayLength": 10,
        "language": {
          //  "sSearch":"",
            "paginate": {
                first:    '<i class="fa fa-forward fa-flip-horizontal" data-toggle="tooltip" title="Primera página"></i>',
                previous: '<i class="fa fa-caret-right fa-flip-horizontal" data-toggle="tooltip" title="Anterior"></i>',
                next:     '<i class="fa fa-caret-right" data-toggle="tooltip" title="Siguiente"></i>',
                last:     '<i class="fa fa-forward" data-toggle="tooltip" title="Última página"></i>'
            },
        },
    });
	$('#openModal').click(function(e){
		e.preventDefault();
		var url = $(this).attr('href');
		console.log(url);
		swal({
		  title: "Agregar Comentario",
		  type: "input",
		  showCancelButton: true,
		  closeOnConfirm: false,
		  animation: "slide-from-top",
		  showLoaderOnConfirm: true,
		  inputPlaceholder: "Comentario"
		},
		function(inputValue){
		  if (inputValue === false) return false;
		  
		  if (inputValue === "") {
		    swal.showInputError("El comentario no puede estar vacio!");
		    return false
		  }
		  
		  var request = $.ajax({
					url: url,
					method: "POST",
					data:{comment:inputValue},
					dataType:'JSON'
				}).done(function(msg) {
					console.log(msg);
					swal("Completado!", "Comentario agregado correctamente", "success");
					datatable.row.add( [
				        msg.user,
				        msg.date,
				        msg.type,
				        msg.comment
				    ] ).draw();
					//$('.modal-body').html(msg);
				});
		});
	});

	$(".datatable").on("click", ".comment .fa-arrow-down", function(){
		$('.short'+$(this).data('key')).hide(200);
		$('.long'+$(this).data('key')).show(200);
		$(this).removeClass('fa-arrow-down');
		$(this).addClass('fa-arrow-up');
		$('.tr'+$(this).data('key')).css('background','#e0e0e0');
	});
	
	$(".datatable").on("click", ".comment .fa-arrow-up", function(){
		$('.short'+$(this).data('key')).show(200);
		$('.long'+$(this).data('key')).hide(200);
		$(this).removeClass('fa-arrow-up');
		$(this).addClass('fa-arrow-down');
		$('.tr'+$(this).data('key')).css('background','white');
	});
</script>