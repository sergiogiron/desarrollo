<?php $arrayDataProvider = new CArrayDataProvider($data, array(
    'id'=>$id,
    'keyField' => 'nro_reserva',
    'keys'=>array('nro_reserva','checkout_id', 'cliente','email'),
    'sort'=>array(
    	'defaultOrder'=>'checkout_id ASC',
        'attributes'=>array(
            'nro_reserva',
            'checkout_id', 
            'cliente',
            'email',
            'proveedor',
			),
		),
    'pagination'=>array(
        'pageSize'=>10
		),
)); 

//$arrayDataProvider->setSort('proveedor');
if($id == 'resultados'){
	$arrayShow = array();
}else{
	$arrayShow = array('style' => 'display:none');
}
?>

<?php $this->widget('zii.widgets.grid.CGridView', array(
		'id'=> $id.'-grid',
		'summaryText' => '', 
		'dataProvider'=>$arrayDataProvider,
		'ajaxUrl' => $this->createUrl('admin'),
		'itemsCssClass' => 'table table-bordered table-striped table-list',
		'enableSorting'=>true,
		'pager' => array(
				'cssFile'=>false,
				'header'=> '',
				'firstPageLabel' => '<i class="fa fa-forward fa-flip-horizontal"></i>',
				'prevPageLabel'  => '<i class="fa fa-caret-right fa-flip-horizontal"></i>',
				'nextPageLabel'  => '<i class="fa fa-caret-right"></i>',
				'lastPageLabel'  => '<i class="fa fa-forward"></i>',
			),
		//'filter'=>$model,
		'columns'=>array(		
			array(
				'name' => 'nro_reserva',
				'header' => 'Nro Reserva',
			),				
			array(
				'name' 	=> 'checkout_id',
				'header' => 'Checkout ID',
			),				
			array(
				'name' => 'cliente',
				'header' => 'Cliente',
			),
			array(
				'name' => 'estado',
				'header' => 'Estado',
				'visible' =>'$id == "resultados"',
				'htmlOptions' => $arrayShow,
				'headerHtmlOptions' => $arrayShow,
				'filterHtmlOptions' => $arrayShow,
			),				
			array(
				'name' => 'email',
				'header' => 'Email',
			),
			array(
				'name' => 'proveedor',
				'header' => 'Proveedor',
			),				
			array(
				'name' => 'nombre_paquete',
				'header' => 'Paquete',
			),				
			array(
				'name' => 'fecha',
				'header' => 'Fecha',
			),
			array(
				'class'=>'CButtonColumn',
				'header'=>'Acciones',
				'template'=>'{view}{update}{cancel}',	
				'buttons'=>array(					
					'view' => array(
						'label'=>'<i class="fa fa-eye icon_cbutton"></i>',
						'url'=>'Yii::app()->createUrl(strtolower(Yii::app()->controller->id).\'/view/id/\'. $data["checkout_id"])',
						'options'=>array('title'=>'Ver', 'class'=>'open_lightbox','data-toggle'=>'tooltip'),
						'imageUrl'=>false,
					),	
					'update' => array(
						'label'=>'<i class="fa fa-check-circle-o icon_cbutton"></i>',
						'url'=>'Yii::app()->createUrl(strtolower(Yii::app()->controller->id).\'/confirm/id/\'. $data["nro_reserva"].\'/proveedor/\'.$data["id_proveedor"])',
						'options'=>array('title'=>'Confirmar',  'class'=>'accion_reserva','data-toggle'=>'tooltip'),
						'imageUrl'=>false,
						'visible' => '$data["estado"] == \'Pendiente\''
					),					
					'cancel' => array(
						'label'=>'<i class="fa fa-ban icon_cbutton"></i>',
						'url'=>'Yii::app()->createUrl(strtolower(Yii::app()->controller->id).\'/cancel/id/\'. $data["nro_reserva"].\'/proveedor/\'.$data["id_proveedor"])',
						'options'=>array('title'=>'Cancelar', 'class'=>'accion_reserva','data-toggle'=>'tooltip'),
						'imageUrl'=>false,
						'visible' => '$data["estado"] == \'Pendiente\''
					),
				),
			),
		)
	)); ?>
