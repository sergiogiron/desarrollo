<?php
/* @var $this BookingController */
/* @var $model Booking */

$this->breadcrumbs=array(
	'Bookings'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Booking', 'url'=>array('index')),
	array('label'=>'Create Booking', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
search();
deleteBtn();

");
?>
<style type="text/css">
	.modal-body{
		height: 80vh;
		overflow: auto;
	}
</style>
<section class="content-header">
	<h1>
		Control de reservas
	</h1>
	<ol class="breadcrumb">
		<li><a href="/ttsoffice_frontend"><i class="fa fa-home"></i> Home</a></li>
		<li><a href="/ttsoffice_frontend/booking">Control de reservas</a></li>
	</ol>
</section>
<section class="content">
	<div class="box box-danger">
		<div class="box-header with-border">	
			<div class="nav-tabs">
				<ul class="nav nav-tabs">
					<li class="active"><a href="#tab_1" data-toggle="tab">Pendientes</a></li>
					<li><a href="#tab_2" data-toggle="tab">Confirmadas</a></li> 
					<li><a href="#tab_3" data-toggle="tab">Canceladas</a></li> 
					<li><a href="#tab_4" data-toggle="tab">Caidas</a></li> 
				</ul>
				<div class="tab-content">
					<div class="tab-pane active" id="tab_1">
						<div class="col-md-3 search_bar">
							<div class="row">
								<div class="input-group input-group-sm">
									<input type="text" class="form-control booking-search" id="criterio_pendientes">
									<span class="input-group-btn">
									  <button type="button" id="buscar_pendientes" class="btn btn-default btn-flat">
									  	<i class="fa fa-search"></i>
									  </button>
									</span>
								</div>
							</div>
						</div>	
						<?php echo CHtml::link('Búsqueda avanzada','#',array('class'=>'search-button pendientes')); ?>
						<div class="search-form-pendientes" style="display:none">
						<?php $this->renderPartial('_search',array(
							'model'=>$model_pendientes,
						)); ?>
						</div><!-- search-form -->
						<style type="text/css">
							table > tbody > tr > td:first-child{
								text-align: center;
							}
						</style>
						<?php $this->widget('zii.widgets.grid.CGridView', array(
							'id'=>'booking-grid-pendientes',
							'pager' => array(
			                    'cssFile'=>false,
			                    'header'=> '',
			                    'firstPageLabel' => '<i class="fa fa-forward fa-flip-horizontal"></i>',
						        'prevPageLabel'  => '<i class="fa fa-caret-right fa-flip-horizontal"></i>',
						        'nextPageLabel'  => '<i class="fa fa-caret-right"></i>',
						        'lastPageLabel'  => '<i class="fa fa-forward"></i>',
						        //'afterAjaxUpdate'=> 'function(){enableSwitch();}',
			                ),				
							'summaryText' => '', 
							'itemsCssClass' => 'table table-bordered table-striped table-list',				
							'dataProvider'=>$model_pendientes->search_pendientes(),
							'columns'=>array(
								array(
									'header' => 'Canal',
									'name' => 'id_channel',
									'type' => 'raw',
									'value' => '($data["id_channel"] == 10 ? \'<i class="fa fa-mobile icon_cbutton"></i>\' : \'<i class="fa fa-laptop icon_cbutton"></i>\');',
								),
								'checkoutId',
								'passenger',
								array(
									'name'=>'date',
									'filter'=>CHtml::activeTextField($model_pendientes,'date'),
									'value'=>'Yii::app()->dateFormatter->format("dd/MM/yy",strtotime($data->date))'
								),	
								'book_code',
								'package',
								array(
									'name'=>'expire_date',
									'filter'=>CHtml::activeTextField($model_pendientes,'expire_date'),
									'value'=>'Yii::app()->dateFormatter->format("dd/MM/yy",strtotime($data->expire_date))'
								),	
								array(
									'class'=>'CButtonColumn',
									'header'=>'Emails',
									'template'=>'{cliente}{operacion}',	
									'buttons'=>array(					
										'cliente' => array(
											'label'=>'<i class="fa fa-envelope icon_cbutton"></i>',
											'visible'=>'Yii::app()->user->checkAccess(\'update\')',
											'url'=>'Yii::app()->createUrl(strtolower(Yii::app()->controller->id).\'/mailcliente/id/\'. $data->id)',
											'options'=>array('title'=>'Ver email cliente','class'=>'open_lightbox','data-toggle'=>'tooltip'),
											'imageUrl'=>false,
										),					
										'operacion' => array(
											'label'=>'<i class="fa fa-envelope-o icon_cbutton"></i>',
											'url'=>'Yii::app()->createUrl(strtolower(Yii::app()->controller->id).\'/mailoperacion/id/\'. $data->id)',
											
											'options'=>array('title'=>'Ver email operacion','class'=>'open_lightbox','data-toggle'=>'tooltip'),
											'imageUrl'=>false,
										),
									),
								),
								array(
									'class'=>'CButtonColumn',
									'header'=>'Acciones',
									'template'=>'{view}{update}{cancel}',	
									'buttons'=>array(					
										'view' => array(
											'label'=>'<i class="fa fa-eye icon_cbutton"></i>',
											'url'=>'Yii::app()->createUrl(strtolower(Yii::app()->controller->id).\'/view/id/\'. $data->id)',
											'options'=>array('title'=>'Ver', 'class'=>'open_lightbox','data-toggle'=>'tooltip'),
											'imageUrl'=>false,
										),	
										'update' => array(
											'label'=>'<i class="fa fa-check-circle-o icon_cbutton"></i>',
											'url'=>'Yii::app()->createUrl(strtolower(Yii::app()->controller->id).\'/confirm/id/\'. $data->id)',
											'options'=>array('title'=>'Confirmar',  'class'=>'accion_reserva','data-toggle'=>'tooltip'),
											'imageUrl'=>false,
										),					
										'cancel' => array(
											'label'=>'<i class="fa fa-ban icon_cbutton"></i>',
											'url'=>'Yii::app()->createUrl(strtolower(Yii::app()->controller->id).\'/cancel/id/\'. $data->id)',
											'options'=>array('title'=>'Cancelar', 'class'=>'accion_reserva','data-toggle'=>'tooltip'),
											'imageUrl'=>false,
										),
									),
								),
							)
						)); ?>
					</div>
					<div class="tab-pane" id="tab_2">
						
						<div class="col-md-3 search_bar">
							<div class="row">
								<div class="input-group input-group-sm">
									<input type="text" class="form-control booking-search" id="criterio_confirmadas">
									<span class="input-group-btn">
									  <button type="button" id="buscar_confirmadas" class="btn btn-default btn-flat">
									  	<i class="fa fa-search"></i>
									  </button>
									</span>
								</div>
							</div>
						</div>		
						<?php echo CHtml::link('Búsqueda avanzada','#',array('class'=>'search-button confirmadas')); ?>
						<div class="search-form-confirmadas" style="display:none">
						<?php $this->renderPartial('_search',array(
							'model'=>$model_confirmadas,
						)); ?>
						</div><!-- search-form -->

						<?php $this->widget('zii.widgets.grid.CGridView', array(
							'id'=>'booking-grid-confirmadas',
							'pager' => array(
			                    'cssFile'=>false,
			                    'header'=> '',
			                    'firstPageLabel' => '<i class="fa fa-forward fa-flip-horizontal"></i>',
						        'prevPageLabel'  => '<i class="fa fa-caret-right fa-flip-horizontal"></i>',
						        'nextPageLabel'  => '<i class="fa fa-caret-right"></i>',
						        'lastPageLabel'  => '<i class="fa fa-forward"></i>',
						        //'afterAjaxUpdate'=> 'function(){enableSwitch();}',
			                ),				
							'summaryText' => '', 
							'itemsCssClass' => 'table table-bordered table-striped table-list',				
							'dataProvider'=>$model_confirmadas->search_confirmadas(),
							'columns'=>array(
								array(
									'header' => 'Canal',
									'name' => 'id_channel',
									'type' => 'raw',
									'value' => '($data["id_channel"] == 10 ? \'<i class="fa fa-mobile icon_cbutton"></i>\' : \'<i class="fa fa-laptop icon_cbutton"></i>\');',
								),
								'checkoutId',
								'passenger',
								array(
									'name'=>'date',
									'filter'=>CHtml::activeTextField($model_confirmadas,'date'),
									'value'=>'Yii::app()->dateFormatter->format("dd/MM/yy",strtotime($data->date))'
								),	
								'book_code',
								'package',
								array(
									'name'=>'expire_date',
									'filter'=>CHtml::activeTextField($model_confirmadas,'expire_date'),
									'value'=>'Yii::app()->dateFormatter->format("dd/MM/yy",strtotime($data->expire_date))'
								),
								array(
									'class'=>'CButtonColumn',
									'header'=>'Emails',
									'template'=>'{cliente}{operacion}',	
									'buttons'=>array(					
										'cliente' => array(
											'label'=>'<i class="fa fa-envelope icon_cbutton"></i>',
											'visible'=>'Yii::app()->user->checkAccess(\'update\')',
											'url'=>'Yii::app()->createUrl(strtolower(Yii::app()->controller->id).\'/mailcliente/id/\'. $data->id)',
											'options'=>array('title'=>'Ver email cliente','class'=>'open_lightbox','data-toggle'=>'tooltip'),
											'imageUrl'=>false,
										),					
										'operacion' => array(
											'label'=>'<i class="fa fa-envelope-o icon_cbutton"></i>',
											'url'=>'Yii::app()->createUrl(strtolower(Yii::app()->controller->id).\'/mailoperacion/id/\'. $data->id)',
											
											'options'=>array('title'=>'Ver email operacion','class'=>'open_lightbox','data-toggle'=>'tooltip'),
											'imageUrl'=>false,
										),
									),
								),
								array(
									'class'=>'CButtonColumn',
									'header'=>'Acciones',
									'template'=>'{view}',	
									'buttons'=>array(					
										'view' => array(
											'label'=>'<i class="fa fa-eye icon_cbutton"></i>',
											'url'=>'Yii::app()->createUrl(strtolower(Yii::app()->controller->id).\'/view/id/\'. $data->id)',
											'options'=>array('title'=>'Ver', 'class'=>'open_lightbox','data-toggle'=>'tooltip'),
											'imageUrl'=>false,
										)
									),
								),
							)
						)); ?>
					</div>
					<div class="tab-pane" id="tab_3">
						
						<div class="col-md-3 search_bar">
							<div class="row">
								<div class="input-group input-group-sm">
									<input type="text" class="form-control booking-search" id="criterio_canceladas">
									<span class="input-group-btn">
									  <button type="button" id="buscar_canceladas" class="btn btn-default btn-flat">
									  	<i class="fa fa-search"></i>
									  </button>
									</span>
								</div>
							</div>
						</div>	
						<?php echo CHtml::link('Búsqueda avanzada','#',array('class'=>'search-button canceladas')); ?>
						<div class="search-form-canceladas" style="display:none">
						<?php $this->renderPartial('_search',array(
							'model'=>$model_canceladas,
						)); ?>
						</div><!-- search-form -->

						<?php $this->widget('zii.widgets.grid.CGridView', array(
							'id'=>'booking-grid-canceladas',
							'pager' => array(
			                    'cssFile'=>false,
			                    'header'=> '',
			                    'firstPageLabel' => '<i class="fa fa-forward fa-flip-horizontal"></i>',
						        'prevPageLabel'  => '<i class="fa fa-caret-right fa-flip-horizontal"></i>',
						        'nextPageLabel'  => '<i class="fa fa-caret-right"></i>',
						        'lastPageLabel'  => '<i class="fa fa-forward"></i>',
						        //'afterAjaxUpdate'=> 'function(){enableSwitch();}',
			                ),				
							'summaryText' => '', 
							'itemsCssClass' => 'table table-bordered table-striped table-list',				
							'dataProvider'=>$model_canceladas->search_canceladas(),
							'columns'=>array(
								array(
									'header' => 'Canal',
									'name' => 'id_channel',
									'type' => 'raw',
									'value' => '($data["id_channel"] == 10 ? \'<i class="fa fa-mobile icon_cbutton"></i>\' : \'<i class="fa fa-laptop icon_cbutton"></i>\');',
								),
								'checkoutId',
								'passenger',
								array(
									'name'=>'date',
									'filter'=>CHtml::activeTextField($model_canceladas,'date'),
									'value'=>'Yii::app()->dateFormatter->format("dd/MM/yy",strtotime($data->date))'
								),	
								'book_code',
								'package',
								array(
									'name'=>'expire_date',
									'filter'=>CHtml::activeTextField($model_canceladas,'expire_date'),
									'value'=>'Yii::app()->dateFormatter->format("dd/MM/yy",strtotime($data->expire_date))'
								),	
								array(
									'class'=>'CButtonColumn',
									'header'=>'Emails',
									'template'=>'{cliente}{operacion}',	
									'buttons'=>array(					
										'cliente' => array(
											'label'=>'<i class="fa fa-envelope icon_cbutton"></i>',
											'visible'=>'Yii::app()->user->checkAccess(\'update\')',
											'url'=>'Yii::app()->createUrl(strtolower(Yii::app()->controller->id).\'/mailcliente/id/\'. $data->id)',
											'options'=>array('title'=>'Ver email cliente','class'=>'open_lightbox','data-toggle'=>'tooltip'),
											'imageUrl'=>false,
										),					
										'operacion' => array(
											'label'=>'<i class="fa fa-envelope-o icon_cbutton"></i>',
											'url'=>'Yii::app()->createUrl(strtolower(Yii::app()->controller->id).\'/mailoperacion/id/\'. $data->id)',
											
											'options'=>array('title'=>'Ver email operacion','class'=>'open_lightbox','data-toggle'=>'tooltip'),
											'imageUrl'=>false,
										),
									),
								),
								array(
									'class'=>'CButtonColumn',
									'header'=>'Acciones',
									'template'=>'{view}',	
									'buttons'=>array(					
										'view' => array(
											'label'=>'<i class="fa fa-eye icon_cbutton"></i>',
											'url'=>'Yii::app()->createUrl(strtolower(Yii::app()->controller->id).\'/view/id/\'. $data->id)',
											'options'=>array('title'=>'Ver', 'class'=>'open_lightbox','data-toggle'=>'tooltip'),
											'imageUrl'=>false,
										)
									),
								),
							)
						)); ?>
					</div>
					<div class="tab-pane" id="tab_4">
						
						<div class="col-md-3 search_bar">
							<div class="row">
								<div class="input-group input-group-sm">
									<input type="text" class="form-control booking-search" id="criterio_caidas">
									<span class="input-group-btn">
									  <button type="button" id="buscar_caidas" class="btn btn-default btn-flat">
									  	<i class="fa fa-search"></i>
									  </button>
									</span>
								</div>
							</div>
						</div>	
						<?php echo CHtml::link('Búsqueda avanzada','#',array('class'=>'search-button caidas')); ?>
						<div class="search-form-caidas" style="display:none">
						<?php $this->renderPartial('_search',array(
							'model'=>$model_caidas,
						)); ?>
						</div><!-- search-form -->

						<?php $this->widget('zii.widgets.grid.CGridView', array(
							'id'=>'booking-grid-caidas',
							'pager' => array(
			                    'cssFile'=>false,
			                    'header'=> '',
			                    'firstPageLabel' => '<i class="fa fa-forward fa-flip-horizontal"></i>',
						        'prevPageLabel'  => '<i class="fa fa-caret-right fa-flip-horizontal"></i>',
						        'nextPageLabel'  => '<i class="fa fa-caret-right"></i>',
						        'lastPageLabel'  => '<i class="fa fa-forward"></i>',
						        //'afterAjaxUpdate'=> 'function(){enableSwitch();}',
			                ),				
							'summaryText' => '', 
							'itemsCssClass' => 'table table-bordered table-striped table-list',				
							'dataProvider'=>$model_caidas->search_caidas(),
							'columns'=>array(
								array(
									'header' => 'Canal',
									'name' => 'id_channel',
									'type' => 'raw',
									'value' => '($data["id_channel"] == 10 ? \'<i class="fa fa-mobile icon_cbutton"></i>\' : \'<i class="fa fa-laptop icon_cbutton"></i>\');',
								),
								'checkoutId',
								'passenger',
								array(
									'name'=>'date',
									'filter'=>CHtml::activeTextField($model_caidas,'date'),
									'value'=>'Yii::app()->dateFormatter->format("dd/MM/yy",strtotime($data->date))'
								),	
								'book_code',
								'package',
								array(
									'name'=>'expire_date',
									'filter'=>CHtml::activeTextField($model_caidas,'expire_date'),
									'value'=>'Yii::app()->dateFormatter->format("dd/MM/yy",strtotime($data->expire_date))'
								),	
								array(
									'class'=>'CButtonColumn',
									'header'=>'Emails',
									'template'=>'{cliente}{operacion}',	
									'buttons'=>array(					
										'cliente' => array(
											'label'=>'<i class="fa fa-envelope icon_cbutton"></i>',
											'visible'=>'Yii::app()->user->checkAccess(\'update\')',
											'url'=>'Yii::app()->createUrl(strtolower(Yii::app()->controller->id).\'/mailcliente/id/\'. $data->id)',
											'options'=>array('title'=>'Ver email cliente','class'=>'open_lightbox','data-toggle'=>'tooltip'),
											'imageUrl'=>false,
										),					
										'operacion' => array(
											'label'=>'<i class="fa fa-envelope-o icon_cbutton"></i>',
											'url'=>'Yii::app()->createUrl(strtolower(Yii::app()->controller->id).\'/mailoperacion/id/\'. $data->id)',
											
											'options'=>array('title'=>'Ver email operacion','class'=>'open_lightbox','data-toggle'=>'tooltip'),
											'imageUrl'=>false,
										),
									),
								),
								array(
									'class'=>'CButtonColumn',
									'header'=>'Acciones',
									'template'=>'{view}',	
									'buttons'=>array(					
										'view' => array(
											'label'=>'<i class="fa fa-eye icon_cbutton"></i>',
											'url'=>'Yii::app()->createUrl(strtolower(Yii::app()->controller->id).\'/view/id/\'. $data->id)',
											'options'=>array('title'=>'Ver', 'class'=>'open_lightbox','data-toggle'=>'tooltip'),
											'imageUrl'=>false,
										)
									),
								),
							)
						)); ?>
					</div>

				</div>
			</div>
		</div>
	</div>
</section>
<script type="text/javascript">
 	$('body').on('click', ".accion_reserva" , function(event) {
		event.preventDefault();

		var boton = $(this);
		var icono = $(this).find('i');
		var clase = icono.attr('class');
		icono.removeClass(clase);
		boton.css('pointer-events','none');
		icono.addClass('fa fa-spinner fa-spin fa-fw');

		swal({   
			title:"Esta seguro?",   
			text: "Va a cambiar el estado de la reserva!",   
			type: "warning",   
			showCancelButton: true,   
			confirmButtonColor: "#DD6B55",   
			confirmButtonText: "Si, cambiar!",   
			cancelButtonText: "Cancelar",   
			closeOnConfirm: false }, 
		function(isConfirm){   
			if (isConfirm) {
				var request = $.ajax({
					url: $(this).attr('href'),
				}).done(function(msg) {
					console.log(msg);
					icono.removeClass('fa fa-spinner fa-spin fa-3x fa-fw');
					icono.addClass(clase);
					boton.css('pointer-events','auto');
					$.fn.yiiGridView.update('booking-grid-pendientes');
					$.fn.yiiGridView.update('booking-grid-confirmadas');
					$.fn.yiiGridView.update('booking-grid-canceladas');
					$.fn.yiiGridView.update('booking-grid-caidas');
					swal("Completado!", "Se cambio el estado correctamente", "success"); 
					//$('.modal-body').html(msg);
				});	
			}else{
				icono.removeClass('fa fa-spinner fa-spin fa-3x fa-fw');
				icono.addClass(clase);
				boton.css('pointer-events','auto');
			}
		});
	});

 	 $('body').on('click',".open_lightbox",function(event) {
		event.preventDefault();
		var boton = $(this);
		var icono = $(this).find('i');
		var clase = icono.attr('class');
		icono.removeClass(clase);
		boton.css('pointer-events','none');
		icono.addClass('fa fa-spinner fa-spin fa-fw');
		var request = $.ajax({
			url: $(this).attr('href'),
		}).done(function(msg) {
			icono.removeClass('fa fa-spinner fa-spin fa-3x fa-fw');
			icono.addClass(clase);
			boton.css('pointer-events','auto');
			$('.ligth-box-modal').css('display','block');
			$('.modal-title').html('Visualizando contenido');
			$('.modal-body').html(msg);
		});	
	});

	 $('#criterio_pendientes').keypress(function(e){
	   if(e.which == 13) {
		  $('#buscar_pendientes').trigger('click');
	   }
	});	
	$('#buscar_pendientes').click(function(){
		$('#booking-grid-pendientes').yiiGridView('update', {
			data: { criterio_pendientes : $('#criterio_pendientes').val() }
		});
		return false;
	});	

	$('#criterio_confirmadas').keypress(function(e){
	   if(e.which == 13) {
		  $('#buscar_confirmadas').trigger('click');
	   }
	});	
	$('#buscar_confirmadas').click(function(){
		$('#booking-grid-confirmadas').yiiGridView('update', {
			data: { criterio_confirmadas : $('#criterio_confirmadas').val() }
		});
		return false;
	});	

	$('#criterio_canceladas').keypress(function(e){
	   if(e.which == 13) {
		  $('#buscar_canceladas').trigger('click');
	   }
	});	
	$('#buscar_canceladas').click(function(){
		$('#booking-grid-canceladas').yiiGridView('update', {
			data: { criterio_canceladas : $('#criterio_canceladas').val() }
		});
		return false;
	});	

	$('#criterio_caidas').keypress(function(e){
	   if(e.which == 13) {
		  $('#buscar_caidas').trigger('click');
	   }
	});	
	$('#buscar_caidas').click(function(){
		$('#booking-grid-caidas').yiiGridView('update', {
			data: { criterio_caidas : $('#criterio_caidas').val() }
		});
		return false;
	});	
 </script>