<?php
/* @var $this BookingController */
/* @var $model Booking */

$this->breadcrumbs=array(
	'Bookings'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Booking', 'url'=>array('index')),
	array('label'=>'Manage Booking', 'url'=>array('admin')),
);
?>
<section class="content-header">
  <h1>
	Booking	<small>modificar</small>
  </h1>
  <ol class="breadcrumb">
	<li><a href="/ttsoffice_frontend"><i class="fa fa-dashboard"></i> Home</a></li>
	<li><a href="/ttsoffice_frontend/booking">Booking</a></li>
	<li class="active"><a href="">Modificar</a></li>
  </ol>
</section>
<section class="content">
	<div class="box box-danger">
		<div class="box-header with-border">
			<?php $this->renderPartial('_form', array('model'=>$model)); ?>		</div>
	</div>
</section>


