<?php
/* @var $this FinancialCostController */
/* @var $model FinancialCost */
/* @var $form CActiveForm */
?>

<div class="box-body">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'financial-cost-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>
	<span class="right_f">
	<?php echo $form->checkBox($model,'enabled',array('class'=>'lcs_check enabled')); ?>
	</span>
	<div class="col-md-12 col-data">
	
	<div class="col-xs-4 form-group required unique" >
		<?php echo $form->labelEx($model,'id_provider'); ?>
		<?php echo $form->dropDownList($model, 'id_provider',CHtml::listData(Providers::model()->findAll(), 'id', 'title'),array('empty' => 'Seleccione','class'=>'form-control selectObj'));?>
		<?php echo $form->error($model,'id_provider'); ?>
	</div>

	<div class="col-xs-4 form-group required unique">
		<?php echo $form->labelEx($model,'id_product'); ?>
		<?php echo $form->dropDownList($model, 'id_product',CHtml::listData(Products::model()->findAll(), 'id', 'title'),array('empty' => 'Seleccione','class'=>'form-control selectObj'));?>
		<?php echo $form->error($model,'id_product'); ?>
	</div>

	<div class="col-xs-4 form-group required">
		<?php echo $form->labelEx($model,'percentage_financial_cost'); ?>
		<?php echo $form->textField($model,'percentage_financial_cost',array('size'=>6,'maxlength'=>6,'class'=>'form-control decimal')); ?>
		<?php echo $form->error($model,'percentage_financial_cost'); ?>
	</div>
	</div>
	<div class="box-footer">
		<?php
			$title = (Yii::app()->controller->action->id!='view') ? 'Volver sin guardar cambios' : 'Volver';
			echo '<a href="'.Yii::app()->request->baseUrl.'/'.strtolower(Yii::app()->controller->id).'" class="btn btn-secundary cancel" data-toggle="tooltip" title="'.$title.'" >Cerrar</a>';
			if(Yii::app()->controller->action->id!='view'){
				echo CHtml::submitButton('Guardar',array('class'=>'btn btn-primary save','data-toggle'=>'tooltip','title'=>'Guardar cambios'));
			}
		?>
	</div>
	</div>
<?php $this->endWidget(); ?>

</div><!-- form -->
<?php
	// Activo/Inactivo
	Yii::app()->clientScript->registerCssFile(Yii::app()->params['cdnUrl'].'/plugins/lcswich/lc_switch-tts.css');
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/lcswich/lc_switch.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/lcswich/lc_switch-tts.js',CClientScript::POS_END);
	// Datepicker con la mascara
	Yii::app()->clientScript->registerCssFile(Yii::app()->params['cdnUrl'].'/plugins/datepicker/datepicker3.css');
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/datepicker/bootstrap-datepicker.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/input-mask/jquery.inputmask.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/input-mask/jquery.inputmask.numeric.extensions.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/datepicker/datepicker-tts.js',CClientScript::POS_END);
	echo '<script>viewPage = "'.Yii::app()->controller->action->id.'";</script>';
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/js/form-validators.js',CClientScript::POS_END);
 ?>
  <script type="text/javascript">
	$( document ).ready(function() {

		<?php if(Yii::app()->controller->action->id=='view'){ echo "$('form').find('input, textarea, button, checkbox, select').attr('disabled','disabled');"; } ?>
	  $(".decimal").inputmask({ 
	  		'alias': 'decimal', 
	  		//'groupSeparator': ',', 
	  		'autoGroup': true, 
	  		'digits': 2, 
	  		'digitsOptional': false, 
	  		'placeholder': '0.00', 
	  		rightAlign : true,
	  		clearMaskOnLostFocus: !1 
	  	})

		if(viewPage=='update'){
			$('.unique input').prop('disabled',true);
		}
	});
</script>