<?php
/* @var $this FinancialCostController */
/* @var $model FinancialCost */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>
<div class="col-md-12" style="clear: both">
<div class="row">
			
			<div class="col-md-3">
				<div class="row row_space">
					<?php echo $form->label($model,'id'); ?>
					<?php echo $form->textField($model,'id',array('class'=>'form-control')); ?>
				</div>
			</div>
			
			<div class="col-md-3">
				<div class="row row_space">
					<?php echo $form->labelEx($model,'id_provider'); ?>
					<?php echo $form->dropDownList($model, 'id_provider',CHtml::listData(Providers::model()->findAll(), 'id', 'title'),array('empty' => 'Seleccione','class'=>'form-control'));?>
					<?php echo $form->error($model,'id_provider'); ?>
				</div>
			</div>
			
			<div class="col-md-3">
				<div class="row row_space">
					<?php echo $form->labelEx($model,'id_product'); ?>
		<?php echo $form->dropDownList($model, 'id_product',CHtml::listData(Products::model()->findAll(), 'id', 'title'),array('empty' => 'Seleccione','class'=>'form-control'));?>
		<?php echo $form->error($model,'id_product'); ?>
				</div>
			</div>
			
			<div class="col-md-3">
				<div class="row row_space">
					<?php echo $form->label($model,'percentage_financial_cost'); ?>
					<?php echo $form->textField($model,'percentage_financial_cost',array('size'=>10,'maxlength'=>10,'class'=>'form-control')); ?>
				</div>
			</div>
</div>
	<div class="row buttons">
		<?php echo CHtml::submitButton('Buscar',array('class'=>'btn btn-default b_customize')); ?>
	</div>
<br />
<?php $this->endWidget(); ?>
</div>
</div><!-- search-form -->