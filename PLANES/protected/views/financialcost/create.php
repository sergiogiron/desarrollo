<?php
/* @var $this FinancialCostController */
/* @var $model FinancialCost */

$this->breadcrumbs=array(
	'Financial Costs'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List FinancialCost', 'url'=>array('index')),
	array('label'=>'Manage FinancialCost', 'url'=>array('admin')),
);
?>
<section class="content-header">
	<?php echo Yii::app()->user->Title(); ?>
	<?php echo Yii::app()->user->Breadcrumb(); ?>
	<p class="registros">(<?=$model->count();?> registros encontrados)</p>
</section>
<section class="content">
	<div class="box box-danger">
		<div class="box-header with-border">
			<?php $this->renderPartial('_form', array('model'=>$model)); ?>		</div>
	</div>
</section>


