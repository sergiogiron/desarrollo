<?php
/* @var $this ProfileroleController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Profileroles',
);

$this->menu=array(
	array('label'=>'Create Profilerole', 'url'=>array('create')),
	array('label'=>'Manage Profilerole', 'url'=>array('admin')),
);
?>

<h1>Profileroles</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
