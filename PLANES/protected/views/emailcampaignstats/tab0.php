<div class="tab-pane" id="tab_0">

	<div class="col-md-12" style="margin-top:10px;">
	  <div class="box box-solid">
		<div class="box-header with-border">
			<!--<i class="fa fa-info-circle"></i>
			<h1 class="box-title">Informaci&oacute;n General</h1>-->
		</div>
		<div class="box-body">
		
			<div class="col-md-6">		
			
			  <h2><b>Asunto:</b> <?php echo !empty($model->emailcampaign->subject)?$model->emailcampaign->subject:'';?></h2>
			  <br />
			  
			  <h5><b>Campa&ntilde;a:</b> <?php echo !empty($model->name)?$model->name:''; ?> </h5> 
			  <span><b><u><a href="<?php echo Yii::app()->params['news_subdomain_url'].'/?id='.$model->emailcampaign->id;?>" target="_blank" style="color:grey;"> Ver campaña</a></u></b></span> 
 <br /> <br />
			  <h5><b>Contactos Involucrados:</b> <?php echo !empty($model->subscribers)?$model->subscribers:''; ?></h4>
			 
				  <h5><b>Estado:</b> <?php echo ($model->status == "completed")?"Completada":"En proceso"; ?></h5>			
					  
				  <h5><b>Inicio:</b> <?php echo !empty($model->start_date)?Yii::app()->dateFormatter->format("dd/MM/y",strtotime($model->start_date)):'';?></h5>						 
				  <h5><b>Completado:</b> <?php echo !empty($model->date_end)?Yii::app()->dateFormatter->format("dd/MM/y",strtotime($model->date_end)):''; ?>	</h5>
			</div> 
			
			<div class="col-md-6">
			<!--<h5><b>Remitente:</b> <?php echo !empty($model->from_name)?$model->from_name:''; echo !empty($model->from_mail)? ' ('.$model->from_mail.')':''; ?></h5>  -->
			<!--  <h5><b>Respuestas:</b> <?php #echo $model->from_name; ?> -</h5>-->
			</div> 
			<?php if(Yii::app()->user->checkAccess('viewcompletestats')){ ?>
			<h4 style="margin-top: 20px !important;"><b>Parametros de la Campaña:</b></h4>
				<div class="col-md-2">
				<h5>Incluyó</h5>
				<?php if($model->track_links){?><span class="label label-success">Seguimiento de aperturas	</span><br /><?php } ?>
				<?php if($model->track_reads){?><span class="label label-success"> Seguimiento de clicks</span><br /><?php } ?>
				<?php if($model->g_analytics){?><span class="label label-success">  Google Analytics	</span><br /><?php } ?>
				<?php if($model->clicktale){?><span class="label label-success">   ClickTale	</span>		<?php } ?>
				</div>
				<div class="col-md-2">
				<h5>No Incluyó</h5>
					<?php if(!$model->track_links){?><span class="label label-danger">Seguimiento de aperturas	</span><br /><?php } ?>
					<?php if(!$model->track_reads){?><span class="label label-danger"> Seguimiento de clicks</span><br /><?php } ?>
					<?php if(!$model->g_analytics){?><span class="label label-danger">  Google Analytics	</span><br /><?php } ?>
					<?php if(!$model->clicktale){?><span class="label label-danger">   ClickTale	</span>		<?php } ?> 
					
				</div>
			<?php } ?>
		<!--
			<div class="col-md-12">
				<h5><b>Listas de destinatarios:</b></h4>
					<?php 
					if(!empty($model->lists))
					{
						$var=json_decode($model->lists,true); 
						foreach($var as $key => $val) 
						{
							echo !empty($val['name'])?$val['name']:''.'<br />';
						}
					}
					else
						{
							echo 'No hay datos<br />';
						} ?> 
			</div>-->
		</div>
	  </div>
	</div>
	<?php if(!Yii::app()->user->checkAccess('viewcompletestats')){ ?>
	<?php if(!empty($open_details)){ ?>
		<div class="col-md-10 box box-solid" >
		<div class="box-header with-border">
			<h2 class="text-center"><b>Lista de Accesos</b></h2>
		</div>
		<div class="box-body">
			
			 
			<table id="datetable" class="table table-bordered table-hover dataTable table-list" style="padding-left:20px;">
				<thead>
					<tr>
					  <th style="width: 65%;">Email</th>
					  <th style="width: 35%;text-align: center;">Primer Lectura</th> 
						<!--<th style="width: 25%">Lecturas</th> -->
					</tr>
				</thead>
				<tbody>
					<?php $open_details=json_decode($open_details,1);
						foreach($open_details as $key => $val){
							 
							echo '<tr>';
							echo'<td style="width: 65%;">'.$val['email'].'</td>';
							echo'<td style="width: 35%;text-align: center">'.date('d/m/Y',$val['date']).'</td>';		
							#if (!empty($val['cant'])) echo '<td style="width: 25%">'.$val['cant'].'</td>';else echo'<td style="width: 25%">No hay datos</td>';		
							echo'</tr>';
							
						}
					?>	
				</tbody>
			</table>
		</div>
		</div>
	<?php } ?>
 <?php } ?>
</div>