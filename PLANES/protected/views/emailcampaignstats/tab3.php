
<div class="tab-pane" id="tab_3">
<?php 

$rand = array('0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f');
$arreglo_colores=[];
for($i=0;$i<15;$i++){
$color = '#'.$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)];
array_push($arreglo_colores,$color);
}
$arreglo_colores = array('#e96295', '#3bb7e7', '#8bda61', '#9882bc', '#e93295', '#c52a33', '#f7a35c', '#f2cfa9', '#24e3f6', '#10a63d', '#c52a33', '#74c55c');
?>

	<div class="row col-md-10" style="margin-top: 10px;">
	
		<div class="col-md-5">
			<div class="box box-solid" style="box-shadow:none;">
			<?php if(!empty($device_dev_type)){ 
				?>
					<div class="box-header ">
					  <h3 class="box-title  " style="text-align:center;">Tipo de dispositivos</h3>
					</div>
				<?php
				$this->widget('ext.highcharts.HighchartsWidget', array(
					'scripts' => array(
						#'modules/exporting',
						'themes/grid-light',
					),
					'id' => 'device_dev_type',
					'options' => array(
						
						'title' => array(
							'text' => '',
						),
						'chart' => array(
									'height' => 220, 
									'width' => 220,
									'reflow' => false,
								  ),
						'plotOptions'=> array( 'pie'=>array('shadow'=>false,'center'=>array(['45%','50%']))),
						'tooltip'=>array(
							'headerFormat'=> '',
							'pointFormat'=> '{point.name}<br /><strong>{point.y}</strong> ({point.dataPer}%)',
							'backgroundColor'=> 'rgba(0, 0, 0, 0.85)',
							'borderColor'=>  '#000',
							'borderRadius'=>  5,
							'style'=>array( 'color'=> '#e0e0e0','fontSize'=> '13px','padding'=> '6px 8px'),
						),
						
						'credits' => array('enabled' => false),
						'series' => array(				
							array(
								'type' => 'pie',
								'name' => 'Dispositivos',
								'data' =>[
								array('name'=> "Telefonos",'y'=> !empty($device_dev_type['p']['value'])?$device_dev_type['p']['value']:0, 'color'=> '#c67bbc', 'dataLabels'=> array( 'enabled'=> false), 'dataPer'=>round(!empty($device_dev_type['p']['total'])?$device_dev_type['p']['value']*100/$device_dev_type['p']['total']:'0') ),
								array( 'name'=>"Tablets",'y'=>  !empty($device_dev_type['t']['value'])?$device_dev_type['t']['value']:0, 'color'=> '#f2cfa9', 'dataLabels'=> array( 'enabled'=> false), 'dataPer'=>round(!empty($device_dev_type['t']['total'])?$device_dev_type['t']['value']*100/$device_dev_type['t']['total']:'0') ),
								array( 'name'=>"Computadoras", 'y'=> !empty($device_dev_type['c']['value'])?$device_dev_type['c']['value']:0, 'color'=> '#e96295', 'dataLabels'=> array('enabled'=> false), 'dataPer'=>round(!empty($device_dev_type['c']['total'])?$device_dev_type['c']['value']*100/$device_dev_type['c']['total']:'0') ),
								],
								'size' => '100%',
								'innerSize' => '55%'
							),
						),
					)
				));				 
				?>			
				<div class="data" style="padding-top: 16px;">
					<div class="device" style="margin-top: 6px;"><i class="icon-desktop" style="color: #e96295"></i> Computadoras <span class="per"><b><?php echo round(!empty($device_dev_type['c']['total'])?$device_dev_type['c']['value']*100/$device_dev_type['c']['total']:'0'); ?>%</b></span></div>
					<div class="device" style="margin-top: 6px;"><i class="icon-phone" style="color: #c67bbc"></i> Telefonos <span class="per"><b><?php echo round(!empty($device_dev_type['p']['total'])?$device_dev_type['p']['value']*100/$device_dev_type['p']['total']:'0'); ?>%</b></span></div>
					<div class="device" style="margin-top: 6px;"><i class="icon-tablet" style="color: #f2cfa9"></i> Tablets <span class="per"><b><?php echo round(!empty($device_dev_type['t']['total'])?$device_dev_type['t']['value']*100/$device_dev_type['t']['total']:'0'); ?>%</b></span></div>
				</div>  
			<?php } 
				else { ?>
					<div class="box-header">
					  <h3 class="box-title">Tipo de dispositivos</h3>
					</div>
					<div class="box-body">
					   <img src="<?php echo Yii::app()->request->baseUrl; ?>/img/emptypie.png" class="img-empty" title="No se puede cargar el gráfico" height="190px;" style="margin-top:-15px;" data-toggle="tooltip" >
					   <!--No se puede cargar el gráfico-->
					</div>
			<?php }  ?>
			</div>
		</div>
 
		<div class="col-md-5">
			<div class="box box-solid" style="box-shadow:none">
				<?php if(!empty($device_client)){?>
				<div class="box-header">
				  <h3 class="box-title">Clientes de correo</h3>
				</div>
				<?php
					
					$arreglo_devclients=[];
					$i=0;
					foreach($device_client as $key => $val){ 
					 
						array_push($arreglo_devclients, array('name'=> $key,'y'=> $val['value'], 'color'=> $arreglo_colores[$i], 'dataLabels'=> array( 'enabled'=> false),'dataPer'=>round(!empty($val['total'])?$val['value']*100/$val['total']:'0')  ));	
						$i++;
					}
				 
					$this->widget('ext.highcharts.HighchartsWidget', array(
						'scripts' => array(
							'themes/grid-light',
						),
						'id' => 'device_client',
						'options' => array(
							'title' => array(
								'text' => '',
							),
							'chart' => array(
									'height' => 220, 
									'width' => 220,
									'reflow' => false,
								  ),
							'plotOptions'=> array( 'pie'=>array('shadow'=>false,'center'=>array(['45%','50%']))),
							'tooltip'=>array(
								'headerFormat'=> '',
								'pointFormat'=> '{point.name}<br /><strong>{point.y}</strong> ({point.dataPer}%)',
								'backgroundColor'=> 'rgba(0, 0, 0, 0.85)',
								'borderColor'=>  '#000',
								'borderRadius'=>  5,
								'style'=>array( 'color'=> '#e0e0e0','fontSize'=> '13px','padding'=> '6px 8px'),
							),

							'credits' => array('enabled' => false),
							'series' => array(		
								array(
									'type' => 'pie',
									'name' => 'Tipo de dispositivos',
									'data' => $arreglo_devclients,
									'size' => '100%',
									'innerSize' => '55%'
								),
							),
						)
					));
				?>
				
				<div class="data ec" style="padding-top: 16px;">
					<?php $i=0;
					 
					foreach($device_client as $key => $val){ 
						if (!empty($val['total'])){$perc=round($val['value']*100/$val['total'],1);}else{$perc=0;}
					?>
					
						<div class="device" style="margin-top: 6px;"><i class="icon-mail" style="color: <?php echo $arreglo_colores[$i]; ?>"></i><?php echo ' '.$key; ?><span class="per"><b><?php echo ' '.$perc; ?>%</b></span></div>					
					<?php $i++;	} ?>
				</div>	 
			<?php }	else { ?>
				<div class="box-header">
				  <h3 class="box-title">Clientes de correo</h3>
				</div>
				<div class="box-body">
					  <img src="<?php echo Yii::app()->request->baseUrl; ?>/img/emptypie.png" class="img-empty" title="No se puede cargar el gráfico" height="190px;" style="margin-top:-15px;" data-toggle="tooltip" >
					   <!--No se puede cargar el gráfico-->
				</div>
				<?php }  ?>
			</div>
		</div>						
	</div>
 

	<div class="row col-md-10" style="margin-top: 50px; ">

		<div class="col-md-5">
				<div class="box box-solid" style="box-shadow:none"> 
				<?php if(!empty($device_browser)){	 
					?>
					<div class="box-header">
					  <h3 class="box-title">Navegador</h3>
					</div>
					<?php				
					$arreglo_devbrowser=[];
					$i=0;
					foreach($device_browser as $key => $val){ 
						array_push($arreglo_devbrowser, array('name'=> $key,'y'=> $val['value'], 'color'=> $arreglo_colores[$i], 'dataLabels'=> array( 'enabled'=> false),'dataPer'=>round(!empty($val['total'])?$val['value']*100/$val['total']:'0') ));	
						$i++;
					} 				
					$this->widget('ext.highcharts.HighchartsWidget', array(
						'scripts' => array(
							#'modules/exporting',
							'themes/grid-light',
						),
						'id' => 'device_browser',
						'options' => array(
							'title' => array(
								'text' => '',
							),
							'chart' => array(
									'height' => 220, 
									'width' => 220,
									'reflow' => false,
								  ),
							'plotOptions'=> array( 'pie'=>array('shadow'=>false,'center'=>array(['45%','50%']))),
							'tooltip'=>array(
								'headerFormat'=> '',
								'pointFormat'=> '{point.name}<br /><strong>{point.y}</strong> ({point.dataPer}%)',
								'backgroundColor'=> 'rgba(0, 0, 0, 0.85)',
								'borderColor'=>  '#000',
								'borderRadius'=>  5,
								'style'=>array( 'color'=> '#e0e0e0','fontSize'=> '13px','padding'=> '6px 8px'),
							),
							
							'credits' => array('enabled' => false),
							'series' => array(				
								array(
									'type' => 'pie',
									'name' => 'Navegador',
														'data' =>$arreglo_devbrowser,
									#'center' => array(20, 20),
									'size' => '100%',
									'innerSize' => '55%',
								),
							),
						)
					));
				?>  
						<div class="data ec" style="padding-top: 16px;">
							<?php $i=0;
							foreach($device_browser as $key => $val){ 
								if (!empty($val['total'])){$perc=round($val['value']*100/$val['total'],1);}else{$perc=0;}
							?>
								<div class="device" style="margin-top: 6px;"><i class="icon-mail" style="color: <?php echo $arreglo_colores[$i]; ?>"></i><?php echo ' '.$key; ?><span class="per"><b><?php echo ' '.$perc; ?>%</b></span></div>					
							<?php $i++;	} ?> 
						</div>
					<?php }
							else { ?>
					<div class="box-header">
					  <h3 class="box-title">Navegador</h3>
					</div>
					<div class="box-body">
					   <img src="<?php echo Yii::app()->request->baseUrl; ?>/img/emptypie.png" class="img-empty" title="No se puede cargar el gráfico" height="190px;" style="margin-top:-15px;" data-toggle="tooltip" >
					   <!--No se puede cargar el gráfico-->
					</div>
					
					<?php } ?>
				</div>		
		</div>
			
		<div class="col-md-5">
			<div class="box box-solid" style="box-shadow:none"> 
				<?php if(!empty($device_os)){
				?>
					<div class="box-header">
					  <h3 class="box-title">Sistema operativo</h3>
					</div>
				<?php
					$arreglo_devos=[];
					$i=0;
					foreach($device_os as $key => $val){ 
						array_push($arreglo_devos, array('name'=> $key,'y'=> $val['value'], 'color'=> $arreglo_colores[$i], 'dataLabels'=> array( 'enabled'=> false),'dataPer'=>round(!empty($val['total'])?$val['value']*100/$val['total']:'0') ));	
						$i++;
					} 							
					$this->widget('ext.highcharts.HighchartsWidget', array(
						'scripts' => array(
							'themes/grid-light',
						),
						'id' => 'device_os',
						'options' => array(
							'title' => array(
								'text' => '',
							),
							'chart' => array(
									'height' => 220, 
									'width' => 220,
									'reflow' => false,
								  ),
							'plotOptions'=> array( 'pie'=>array('shadow'=>false,'center'=>array(['45%','50%']))),
							'tooltip'=>array(
								'headerFormat'=> '',
								'pointFormat'=> '{point.name}<br /><strong>{point.y}</strong> ({point.dataPer}%)',
								'backgroundColor'=> 'rgba(0, 0, 0, 0.85)',
								'borderColor'=>  '#000',
								'borderRadius'=>  5,
								'style'=>array( 'color'=> '#e0e0e0','fontSize'=> '13px','padding'=> '6px 8px'),
							),
							
							'credits' => array('enabled' => false),
							'series' => array(				
								array(
									'type' => 'pie',
									'name' => 'Sistema operativo',
									'data' =>$arreglo_devos,
									'size' => '100%',
									'innerSize' => '55%',
								),
							),
						)
					));
			 ?>  
			 
				<div class="data ec" style="padding-top: 16px;">
							<?php $i=0;
							foreach($device_os as $key => $val){ 
								if (!empty($val['total'])){$perc=round($val['value']*100/$val['total'],1);}else{$perc=0;}
							?>
							
								<div class="device" style="margin-top: 6px;"><i class="icon-mail" style="color: <?php echo $arreglo_colores[$i]; ?>"></i><?php echo ' '.$key; ?><span class="per"><b><?php echo ' '.$perc; ?>%</b></span></div>					
							<?php $i++;	}?>
				</div>
				<?php }
							else { ?>
					<div class="box-header">
					  <h3 class="box-title">Sistema operativo</h3>
					</div>
					<div class="box-body">
					   <img src="<?php echo Yii::app()->request->baseUrl; ?>/img/emptypie.png" class="img-empty" title="No se puede cargar el gráfico" height="190px;" style="margin-top:-15px;" data-toggle="tooltip" >
					   <!--No se puede cargar el gráfico-->
					</div>
				
					<?php } ?>
			</div>
		</div>
		
	</div>	
	
</div>
<script>
$(window).resize(function() {
    clearTimeout(this.id);
    this.id = setTimeout(doneResizing, 500);
});
function doneResizing(){
  // $("#device_dev_type").setSize(300, 300, doAnimation = true); 
  // $("#device_os").setSize(300, 300, doAnimation = true); 
  // $("#device_browser").setSize(300, 300, doAnimation = true); 
  // $("#device_client").setSize(300, 300, doAnimation = true); 
}
</script>