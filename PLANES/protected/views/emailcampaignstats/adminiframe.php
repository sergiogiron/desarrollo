<?php
/* @var $this EmailcampaignstatsiframeController */

Yii::app()->clientScript->registerScript('search', "
	search();
	searchAdd();
	
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#mail-campaign-stats-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<style>
.grid-view tr td {cursor: pointer}
</style>
<section class="content-header">
  <h1>
	Estadísticas de Campaña
  </h1>
  <ol class="breadcrumb">
	<li><i class="fa fa-home"></i> Inicio</a></li>
	<li>Emailing</li>
  </ol>
</section>

	<section class="content">
		<div class="box box-danger">
		<div class="box-header with-border">
		
		<!--
			<div class="col-md-3 search_bar">
				<div class="row">
					<div class="input-group input-group-sm">
						<input type="text" class="form-control" id="criterio"><i class="fa fa-close right-icon-search"></i>
						<span class="input-group-btn">
						  <button type="button" id="buscar" class="btn btn-default btn-flat"><i class="fa fa-search"></i></button>
						</span>
					</div>
				</div>
			</div>
		-->
                 <?php $this->widget('zii.widgets.grid.CGridView', array(
					'id'=>'mail-campaign-stats-grid',
					'dataProvider'=>$model->searchCampaignsbyid($user_id),
					//'filter'=>$model,
						'selectableRows'=>1,
					'afterAjaxUpdate'=> 'function(){
						$(".button-column").hide();
						closeLoading();
					}',
					'ajaxUpdate' => 'mail-campaign-stats-grid',
					'ajaxUrl' => Yii::app()->createUrl('Emailcampaignstatsiframe/adminiframe/',array('username'=>$username)),
					'selectableRows'=>1,
					'selectionChanged'=>'function(id){ location.href = "'.$this->createUrl('Emailcampaignstatsiframe/viewiframe').'/username/'.$username.'/id/"+$.fn.yiiGridView.getSelection(id);}',
					//'url'=>'Yii::app()->createUrl(\'iframe/estadisticas-email/'.$username.'/ver/\'. $data->id)',
					'pager' => array(
						'cssFile'=>false,
						'header'=> '',
						'firstPageLabel' => '<i class="fa fa-forward fa-flip-horizontal" data-toggle="tooltip" title="Primera página"></i>',
						'prevPageLabel'  => '<i class="fa fa-caret-right fa-flip-horizontal" data-toggle="tooltip" title="Anterior"></i>',
						'nextPageLabel'  => '<i class="fa fa-caret-right" data-toggle="tooltip" title="Siguiente"></i>',
						'lastPageLabel'  => '<i class="fa fa-forward" data-toggle="tooltip" title="Última página"></i>', 
					),
					'summaryText' => '', 
					'itemsCssClass' => 'table table-bordered table-striped table-list',	
					 
					'columns'=>array(
						'name',						 
						array(
							'name'=>'start_date',
							'filter'=>CHtml::activeTextField($model,'start_date'),
							'value'=>'Yii::app()->dateFormatter->format("dd/MM/y",strtotime($data->start_date))',
							'header'=>'Fecha de Envío',
							'htmlOptions'=>array('style' => 'text-align: center;')
						),
						array(
							'name'=>'updated_info',
							'filter'=>CHtml::activeTextField($model,'updated_info'),
							 'value'=>'Yii::app()->dateFormatter->format("dd/MM/y",strtotime($data->updated_info))',
							 'htmlOptions'=>array('style' => 'text-align: center;')
						),
						array(
							'name'=>'sent_mails',							
							'htmlOptions'=>array('style' => 'text-align: right;'),
							),
						array(
							'name'=>'Porcentaje de Entregados',							
							'value'=>'$data->getSentPercentage()."%"',
							'header'=>'Entregados',							
							'htmlOptions'=>array('style' => 'text-align: right;')
						),
						array(
							'name'=>'Porcentaje de Rechazados',							
							'value'=>'$data->getRejectedPercentage()."%"',
							'header'=>'Rechazados',							
							'htmlOptions'=>array('style' => 'text-align: right;')
						),
						array(
							'name'=>'Fecha de Finalización', 
							'filter'=>CHtml::activeTextField($model,'date_end'),
							 'value'=>'Yii::app()->dateFormatter->format("dd/MM/y",strtotime($data->date_end))',
							 'header'=>'Fecha de Finalización',
							 'htmlOptions'=>array('style' => 'text-align: center;')
						),
						array(
							'class'=>'CButtonColumn',
							'template'=>'{view}',	
							'buttons'=>array(								
								'update' => array(
									'label'=>'<i class="fa fa-refresh icon_cbutton"></i>',
									'url'=>'Yii::app()->createUrl(\'estadísticas-email/actualizar/\'. $data->id)',
									'options'=>array('title'=>'Actualizar Estadística de Campaña'),
									'imageUrl'=>false,
								),	
								'view' => array(
									'label'=>'<i class="fa fa-eye icon_cbutton"></i>',
									'url'=>'Yii::app()->createUrl(\'iframe/estadisticas-email/'.$username.'/ver/\'. $data->id)',
									'options'=>array('title'=>'Ver Estadística',
													'data-toggle'=>'tooltip',
													),
									'imageUrl'=>false,
								),	
							),
							'htmlOptions'=>array('width'=>'0px','style' => 'display: none;'),			
						),
					),
				)); ?>
              
		
		</div>
		</div>
 </section>
 <script>
$(".table tbody tr").attr('title','Ver Estadística');
($(".table tbody tr").find("td:first").attr("class")!="empty") ? $(".table tbody").find("tr:first").attr('title','Ver Estadística'):$(".table tbody").find("tr:first").attr('title','No hay datos');
$(".table tbody tr").attr('data-toggle','tooltip');
$(".button-column").hide();

$(".table tbody tr").on("click", function(){
		var test = $(this).find("td:first").attr("class");
		if(test!="empty"){
			window.location.href = $(this).find("a").attr("href");
		}
}); 
</script>