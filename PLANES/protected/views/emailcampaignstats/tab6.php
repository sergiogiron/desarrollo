
 <script type="text/javascript" src="//cdn.datatables.net/plug-ins/1.10.12/sorting/numeric-comma.js"></script>
<div class="tab-pane" id="tab_6">

	<div class="row">
	<?php if(Yii::app()->user->checkAccess('viewcompletestats')){ ?>
			<div class="col-md-11" style="margin-top:20px;">
		
	 
			<div class="col-md-3 search_bar">
				<div class="row">
					<div class="input-group input-group-sm" style="margin-left:20px;margin-bottom:10px;">
						<input type="text" class="form-control" id="criterio"><i class="fa fa-close right-icon-search"></i>
						<span class="input-group-btn">
						  <button type="button" id="buscar" class="btn btn-default btn-flat"><i class="fa fa-search"></i></button>
						</span>
					</div>
				</div>
			</div>
				

			<table id="datetable2" class="table table-bordered table-hover dataTable table-list" style="padding-left:20px;">
				<thead>
					<tr>
					  <th style="width: 20%;text-align: center;">Vendedor</th>
					<!--  <th style="width: 20%;text-align: center;">Vendedor</th>-->
					  <th style="width: 15%;text-align: center;">Suscriptores</th>
					  <th style="width: 15%;text-align: center;">Aperturas Unicas</th>
					  <th style="width: 15%;text-align: center;">Aperturas Totales</th>
					  <th style="width: 15%;text-align: center;">Clicks Unicos</th>
					  <th style="width: 15%;text-align: center;">Clicks Totales</th> 
					  <th style="width: 15%;text-align: center;">Rechazados</th> 
					  <th style="width: 15%;text-align: center;">Invalidos</th> 
						<!--<th style="width: 25%">Lecturas</th> -->
					</tr>
				</thead>
				<tbody>
					<?php 
						foreach($campaign_detail as $val){
							 
							echo '<tr>';
							echo'<td style="width: 20%;">'.$val['NombreVendedor'].'</td>';
							echo'<!--<td style="width: 20%;">'.$val['Vendedor'].'</td>-->';
							echo'<td style="width: 15%;">'.$val['Suscriptores'].'</td>';
							echo'<td style="width: 15%;">'.$val['AperturasUnicas'].'</td>';
							echo'<td style="width: 15%;">'.$val['AperturasTotales'].'</td>';
							echo'<td style="width: 15%;">'.$val['ClicksUnicos'].'</td>'; 		
							echo'<td style="width: 15%;">'.$val['ClicksTotales'].'</td>'; 		
							echo'<td style="width: 15%;">'.$val['Rechazados'].'</td>'; 		
							echo'<td style="width: 15%;">'.$val['Invalidos'].'</td>'; 		
						    echo'</tr>';
							
						}
					?>	
				</tbody>
			</table>
</div>	
			
		<?php } ?>
		
			
			<div class="col-md-12" style="margin-top:20px; margin-left:20px;">
		 
		<?php echo CHtml::link('<i class="fa fa-file-excel-o" aria-hidden="true"></i>
 Exportar Listado',array('Emailcampaignstats/Export','id'=>$id),array('class'=>'btn btn-primary')); ?>
	
		</div>	
		
		
	</div>				

	
	 
	
	
</div>	
 <style>
.dataTables_paginate .ui-state-disabled {
     display:none;
}
</style>
<script>


$( document ).ready(function() {

$.fn.dataTableExt.oPagination.iFullNumbersShowPages = 3; 

    var oTable = $('#datetable2').DataTable({
		"retrieve": true,
		"bLengthChange": false,
		"bInfo": false,
		"bAutoWidth": false,
		"pagingType": "full_numbers",
		"sDom": 'tp',
		"iDisplayLength": 10,
		"language": {
			"sSearch":"",
	        "paginate": {
	            first:    '<i class="fa fa-forward fa-flip-horizontal" data-toggle="tooltip" title="Primera página"></i>',
	            previous: '<i class="fa fa-caret-right fa-flip-horizontal" data-toggle="tooltip" title="Anterior"></i>',
	            next:     '<i class="fa fa-caret-right" data-toggle="tooltip" title="Siguiente"></i>',
	            last:     '<i class="fa fa-forward" data-toggle="tooltip" title="Última página"></i>'
	        }, 
			"zeroRecords": "No hay registros que cumplan con el valor buscado.", 
			 "decimal": ",",
			 "thousands": "."
		},
		"aoColumnDefs": [ {
                    "aTargets": [1,2,3,4,5,6,7],
                    "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                        var $currencyCell = $(nTd);
                        var commaValue = $currencyCell.text().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
                        $currencyCell.text(commaValue);
                    }
                }]
	});
	

 

	//oTable = $('#datetable').DataTable();  

	$('#criterio').keyup(function(){
		   oTable.search($(this).val()).draw() ;
	})
	 $(".right-icon-search").click(function(){
			$(this).parent().find("input").val('');
			$(this).css('color','#ccc');
			$('#buscar').trigger('click');	
	var table = $('#datetable').DataTable();
	oTable
	 .search( '' )
	 .columns().search( '' )
	 .draw();	
		});


});	 
</script>
