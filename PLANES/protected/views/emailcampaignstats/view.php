<?php 
	Yii::app()->getClientScript()->registerCssFile(Yii::app()->baseUrl.'/css/graph.css');

/* @var $this EmailcampaignstatsController */

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#mail-campaign-stats-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
"); 
?>

<section class="content-header">
	<?php echo Yii::app()->user->Title();  ?>
	<?php echo Yii::app()->user->Breadcrumb(); ?>
</section>

<section class="content">
	<div class="box box-danger">
		<div class="box-header with-border">
		  <div class="nav-tabs2">
			<ul class="nav nav-tabs">
			   <li><a href="#tab_0" data-toggle="tab"><i class="fa fa-info-circle"></i> Información general</a></li>
			   <li class="active"><a href="#tab_1" data-toggle="tab"><i class="fa fa-pie-chart"></i> Principal</a></li>
			   <li><a href="#tab_2" data-toggle="tab"><i class="fa fa-area-chart"></i> Detalle de aperturas</a></li> 
			  <?php if(Yii::app()->user->checkAccess('viewcompletestats')){ ?>
				<li><a href="#tab_3" data-toggle="tab"><i class="fa fa-desktop"></i>  Dispositivos</a></li>
			  <?php } ?>
			   <li><a href="#tab_4" data-toggle="tab"><i class="fa fa-crosshairs" aria-hidden="true"></i> Accesos a las ofertas</a></li> 
			   	<?php if(!Yii::app()->user->checkAccess('viewcompletestats')){  ?>
					<li><a href="#tab_5" data-toggle="tab"><i class="fa fa-minus-circle"></i> Detalle de rechazados</a></li> 	
			   <?php } ?>
			   <?php if(Yii::app()->user->checkAccess('viewcompletestats')){  ?>
					<li><a href="#tab_6" data-toggle="tab"><i class="fa fa-search-plus"></i>  Detalle de campaña</a></li> 	
			   <?php } ?>
			  
			   
			</ul>
			<div class="tab-content">
				<?php $this->renderPartial('tab0',array('model'=>$model,
														'open_details'=>$open_details
														));?>
				<?php $this->renderPartial('tab1',array('model'=>$model,'esIframe'=>false));?>
				<?php $this->renderPartial('tab2',array('model'=>$model,
														'reads_data_timelapse'=>$reads_data_timelapse,
														'reads_data_weekly'=>$reads_data_weekly,
														'reads_data_hourly'=>$reads_data_hourly,
														'open_details'=>$open_details
														));?>
				<?php if(Yii::app()->user->checkAccess('viewcompletestats')){ ?>
				<?php $this->renderPartial('tab3',array('model'=>$model,
														'device_dev_type'=>$device_dev_type,
														'device_client'=>$device_client,
														'device_browser'=>$device_browser,
														'device_os'=>$device_os														
														));?>
				<?php } ?>
				<?php $this->renderPartial('tab4',array('model'=>$model,'esIframe'=>false));?>
				<?php if(!Yii::app()->user->checkAccess('viewcompletestats')){  
			 
				      $this->renderPartial('tab5',array('model'=>$model));	
				}?>
				<?php if(Yii::app()->user->checkAccess('viewcompletestats')){ 
					 $this->renderPartial('tab6',array('campaign_detail'=>$campaign_detail, 'id'=>$model->emailcampaign->id));	
			    } ?>
			  
			 </div>
		  </div>
		</div>

		<div class="box-footer">
			<?php
				echo CHtml::button('Cerrar', array('onclick' => 'js:document.location.href="'.Yii::app()->request->baseUrl.'/'.strtolower(Yii::app()->controller->id).'"','class'=>'btn btn-secundary','data-toggle'=>'tooltip','title'=>'Volver'));
				?>
		</div>

	</div>
 </section>