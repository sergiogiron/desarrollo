<div class="tab-pane" id="tab_5">

	<div class="row"> 
	<?php if(Yii::app()->user->checkAccess('viewcompletestats')){ ?>
		<div class="col-md-10" style="margin-top:20px; padding:10px;"> 	
				<div class="col-md-4 clicks_ctr">
					<div class="box box-solid" style="box-shadow:none">
						<?php if(!empty($model->bounced_soft)||!empty($model->bounced_hard)||!empty($model->bounced_invalid)||!empty($model->bounced_spam))
						{?>
						<div class="box-header">
								  <h3 class="box-title">Tipo de rechazo</h3>
						</div>
						<?php
							$this->widget('ext.highcharts.HighchartsWidget', array(
							'scripts' => array(
								'themes/grid-light',
							),
							'options' => array(
								'title' => array(
									'text' => '',
								),
								'chart' => array(
									'plotBackgroundColor' => null,
									'plotBorderWidth' => null,
									'plotShadow' => false,
									'height' => 250, 
									'width' => 250,
									'spacingBottom' => 15,
									'spacingTop' => 10,
									'spacingLeft' => 10,
									'spacingRight' => 10,
									
								  ),
								'plotOptions'=> array( 'pie'=>array('shadow'=>false,'center'=>array(['50%','50%']))),
								'tooltip'=>array(
									'headerFormat'=> '',
									'pointFormat'=> '{point.name}: <strong>{point.y}</strong>',
									'backgroundColor'=> 'rgba(0, 0, 0, 0.85)',
									'borderColor'=>  '#000',
									'borderRadius'=>  5,
									'style'=>array( 'color'=> '#e0e0e0','fontSize'=> '13px','padding'=> '6px 8px'),
								),
								
								'credits' => array('enabled' => false),
								'series' => array(				
									array(
										'type' => 'pie',
										'name' => 'Rechazados',
										'data' => array(
											array(
												'name' => 'Rechazo temporal',
												'y' => round(!empty($model->bounced_soft)?$model->bounced_soft:0),
												'color'=> '#f8dda1',
												'dataPer'=> round($model->bounced_soft*100/($model->bounced_soft+$model->bounced_hard+$model->bounced_invalid+$model->bounced_spam)),
												'dataLabels'=>  array('enabled'=>false),
											),
											array(
												'name' => 'Rechazo permanente',
												'y' => round(!empty($model->bounced_hard)?$model->bounced_hard:0),
												'dataPer'=> round($model->bounced_hard*100/($model->bounced_soft+$model->bounced_hard+$model->bounced_invalid+$model->bounced_spam)),
												'color'=>'#de873a',
												'dataLabels'=>  array('enabled'=> false),
											),
											array(
												'name' => 'Casilla inexistente',
												'y' => round(!empty($model->bounced_invalid)?$model->bounced_invalid:0),
												'dataPer'=> round($model->bounced_invalid*100/($model->bounced_soft+$model->bounced_hard+$model->bounced_invalid+$model->bounced_spam)),
												'color'=>'#6e5e52',
												'dataLabels'=>  array('enabled'=> false),
											),
											array(
												'name' => 'Interpretado como spam',
												'y' => round(!empty($model->bounced_spam)?$model->bounced_spam:0),
												'dataPer'=> round($model->bounced_spam*100/($model->bounced_soft+$model->bounced_hard+$model->bounced_invalid+$model->bounced_spam)),
												'color'=>'#a12f35',
												'dataLabels'=>  array('enabled'=> false),
											),
										),
										'size' => '100%',
										'innerSize' => '55%'
									),
								),
							)
						));?>
				<div class="data">
					<span style="width: 185px;"><strong class="icon-spam ic" style="color: #f8dda1;"></strong> Rechazo temporal:</span>
					<strong style="color: #555"><?php echo round(!empty($model->bounced_soft)?$model->bounced_soft:0);?></strong>
					<strong style="color: #f8dda1">(<?php echo round($model->bounced_soft*100/($model->bounced_soft+$model->bounced_hard+$model->bounced_invalid+$model->bounced_spam));?>%)</strong>
				</div>
				<div class="data">
					<span style="width: 185px;"><strong class="icon-spam ic" style="color: #de873a"></strong> Rechazo permanente:</span>
					<strong style="color: #555"><?php echo round(!empty($model->bounced_hard)?$model->bounced_hard:0); ?></strong>
					<strong style="color: #de873a">(<?php echo round($model->bounced_hard*100/($model->bounced_soft+$model->bounced_hard+$model->bounced_invalid+$model->bounced_spam));?>%)</strong>
				</div>
				<div class="data">
					<span style="width: 185px;"><strong class="icon-spam ic" style="color: #6e5e52"></strong> Casilla inexistente:</span>
					<strong style="color: #555"><?php echo round(!empty($model->bounced_invalid)?$model->bounced_invalid:0); ?></strong>
					<strong style="color: #6e5e52">(<?php echo round($model->bounced_invalid*100/($model->bounced_soft+$model->bounced_hard+$model->bounced_invalid+$model->bounced_spam));?>%)</strong>
				</div>
				<div class="data">
					<span style="width: 200px;"><strong class="icon-spam ic" style="color: #a12f35"></strong> Interpretado como spam:</span>
					<strong style="color: #555"><?php echo round(!empty($model->bounced_spam)?$model->bounced_spam:0); ?></strong>
					<strong style="color: #a12f35">(<?php echo round($model->bounced_spam*100/($model->bounced_soft+$model->bounced_hard+$model->bounced_invalid+$model->bounced_spam));?>%)</strong>
				</div>
					
				<?php  }
						else { ?>
								<div class="box-header">
								  <h3 class="box-title">Tipo de rechazo</h3>
								</div>
								<div class="box-body">
								   <img src="<?php echo Yii::app()->request->baseUrl; ?>/img/emptypie.png" class="img-empty" title="No se puede cargar el gráfico" height="220px;" data-toggle="tooltip" >
								   <!--No se puede cargar el gráfico-->
								</div>
						<?php } ?>
					
					</div>
				</div>
				
				<?php if(!empty($model->bounced_topdomains)){ ?>	
					<div class="col-md-6 top-domains">
						<div class="box box-solid">
							<div class="box-header">
								<h3 class="box-title">Top 10 dominios</h3>
							</div>			
							<table class="table table-striped">
									<tbody><tr>
									  <th style="width: 50%">Dominio</th>								 
									  <th style="width: 10%">Total</th>
									  <th style="width: 10%">Temporal</th>
									  <th style="width: 10%">Permanente</th>
									  <th style="width: 10%">Inexistente</th>
									  <th style="width: 10%">Spam</th>
									</tr>
									<?php  
									$domains=json_decode($model->bounced_topdomains,1); 								
										foreach($domains as $key => $val){ ?>
											<tr>
											  <td style="width: 50%"><?php echo $key;?></td>
											  <td style="width: 10%"><?php echo !empty($val['total'])?$val['total']:0;?></td>
											  <td style="width: 10%"><?php echo !empty($val['soft']['value'])?$val['soft']['value']:0;?></span></td>
											  <td style="width: 10%"><?php echo !empty($val['hard']['value'])?$val['hard']['value']:0;?></span></td>
											  <td style="width: 10%"><?php echo !empty($val['invalid']['value'])?$val['invalid']['value']:0;?></td>
											  <td style="width: 10%"><?php echo !empty($val['spam']['value'])?$val['spam']['value']:0;?></td>
											</tr>				
										<?php } ?>
									
									 
								  </tbody>
							</table>			
						</div>				
					</div>
				<?php } ?>		
		</div>		
		<?php } ?>
		<?php if(!empty($model->bounced_detail)){ ?>	
			<div class="col-md-12">
				<div class="box box-solid">
					<?php if(Yii::app()->user->checkAccess('viewcompletestats')){ ?>
						<div class="box-header">
							<h3 class="box-title">Listado de envíos rechazados</h3>
						</div>		
					<?php } ?>
					<!--<table class="table table-striped">-->
					<table id="datetable2" class="table table-bordered table-hover dataTable table-list" style="padding-left:20px;">
							<tbody><tr>
							  <th style="width: 50%">Email</th>								 
							  <th style="width: 10%">Fecha</th>
							  <th style="width: 10%">Motivo</th>
							</tr>
							<?php  
							$rejected=json_decode($model->bounced_detail,1); 
								foreach($rejected as $key => $val){ 							
									foreach ($val as $k => $v){									
								?>
									<tr>
									  <td style="width: 50%"><?php echo !empty($v[0])?$v[0]:'';?></td>
									  <td style="width: 10%"><?php echo !empty($v[1])? date('d/m/Y',(int)$v[1]):'';?></td>
									  <td style="width: 10%"><?php echo !empty($key)? strtoupper($key):'';?></span></td>
									</tr>				
								<?php }} ?>
						  </tbody>
					</table>			
				</div>
			</div>
		<?php } ?>		
	</div>				

	
	<?php if(empty($model->bounced_detail) && empty($model->bounced_topdomains) && (empty($model->bounced_soft)||empty($model->bounced_hard)||empty($model->bounced_invalid)||empty($model->bounced_spam))){?>
		<div class="row"> 
			<div class="col-md-10" style="margin-top:20px; padding:10px;"> 	
				<div class="box-header text-center">
						<h3 class="box-title">No hay datos.</h3>
				</div>			
			</div>
		</div>
	<?php } ?>	
	
	
	
</div>	