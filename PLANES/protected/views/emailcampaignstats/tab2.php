 
<div class="tab-pane" id="tab_2">						 
	<div class="GReads stats_pane col-md-10">
		<div id="reads_detail">
			<div class="col-md-12">
				<div class="box box-solid" style="box-shadow:none"> 
				<?php if(!empty($reads_data_timelapse))
					{?>
						<div class="box-header text-center">
						  <h3 class="box-title">Evolución de las aperturas</h3>
						</div>
						<?php
						$date0=explode('-',$model->first_day_reads);
						$date = new DateTime(NULL, new DateTimeZone('UTC'));
						$date->setDate($date0[0],$date0[1],$date0[2]);
						$ts = $date->getTimestamp();
						$this->widget('ext.highcharts.HighchartsWidget', array( 
							'scripts' => array(
								'themes/grid-light',			
							),
							 'id'=>'times',
							'options' => array(
								'title' => array(
									'text' => '',
								),
								'lang'=> array(
								  'decimalPoint'=> ',',
								  'thousandsSep'=> '.'
								),
								'chart' => array(
									'plotBackgroundColor' => '#ffffff',
									'plotBorderWidth' => null,
									'plotShadow' => false,
									'height' => 400, 
									'width'=>920,
								  ),
								'tooltip'=>array(
									'headerFormat'=> '',
									'backgroundColor'=> 'rgba(0, 0, 0, 0.85)',
									'borderColor'=>  '#000',
									'borderRadius'=>  5,
									'style'=>array( 'color'=> '#e0e0e0','fontSize'=> '13px','padding'=> '6px 8px'),
									'pointFormat'=> '{point.x:%e/%b/%y}: <strong>{point.y:,.0f}</strong>',
								),
								'plotOptions'=> array('line'=>array('center'=>array('45%','50%'))),	
								'xAxis'=> array('type'=>'datetime','title'=> array('enabled'=>'false')),			
								'yAxis'=> array('title'=> array( 'text'=> 'Lecturas' ),'min'=>0 ),
								'credits' => array('enabled' => false),
								'series' => array(				
									array( 
										'type' => 'line', 
										'name' => 'Evolución de las aperturas',
										'backgroundColor' => '#f4f4f4', 
										'allowPointSelect'=> true, 
										//'pointStart'=> $ts,
										'pointStart'=> 'js:Date.UTC('.$date0[0].','.($date0[1]-1).','.$date0[2].')',
										'pointInterval'=> 'js:24 * 3600 * 1000',
										'data' => !empty($reads_data_timelapse)?$reads_data_timelapse:0,
										'size' => '100%',
										
									),
								),
							)
						));
					}
					else { ?>
						<div class="box-header">
						  <h3 class="box-title">Evolución de las aperturas</h3>
						</div>
						<div class="box-body">
						  No se puede cargar el gráfico
						</div>						 
<?php } ?>
				</div> 
			</div> 
									
									
			<div id="readsWeekHour" class="row" style="margin-top:50px;">
				<div class="col-md-4 pull-left"style="min-width:430px;max-height:550px;">
					<div class="box box-solid" style="box-shadow:none"> 
				<?php if(!empty($reads_data_hourly))
				{?>
						<div class="box-header text-center">
						  <h3 class="box-title">Aperturas por hora del día</h3>
						</div>
				<?php
				$this->widget('ext.highcharts.HighchartsWidget', array( 
						'scripts' => array(
							'themes/grid-light',	
							'highcharts-more',			
						),
						  
						'options' => array(
							'title' => array(
								'text' => '',
							),
							'lang'=> array(
								  'decimalPoint'=> ',',
								  'thousandsSep'=> '.'
								),
							'width' => 420,
							'chart' => array(
								'plotBackgroundColor' => '#ffffff',
								'plotBorderWidth' => null,
								'plotShadow' => false,
								'width' => 420,
								'height' => 400,
								'reflow' => true,
				 
							  ),
							'tooltip'=>array(
								'headerFormat'=> '',
								'backgroundColor'=> 'rgba(0, 0, 0, 0.85)',
								'borderColor'=>  '#000',
								'borderRadius'=>  5,
								'style'=>array( 'color'=> '#e0e0e0','fontSize'=> '13px','padding'=> '6px 8px'),
								'pointFormat'=> '{point.x}hs: {point.y:,.0f}',			
							),
							
							'xAxis'=> array('type'=>'category','tickInterval'=> 3),			
							'yAxis'=> array('title'=> array( 'enabled'=> 'false' )),
							'credits' => array('enabled' => false),
							'series' => array(				
								array( 
									'type' => 'spline', 
									'name' => 'Aperturas por hora del dia',
									'backgroundColor' => '#f4f4f4', 
									'allowPointSelect'=> true, 
									'lineWidth'=> 3,					
									'data' => !empty($reads_data_hourly)?$reads_data_hourly:0,
									'size' => '50%',
									
								),
							),
						)
					));
				}
				else { ?>
						<div class="box-header">
						  <h3 class="box-title">Aperturas por hora del dia</h3>
						</div>
						<div class="box-body">
						  No se puede cargar el gráfico
						</div> 
				<?php } ?>
					</div>
				</div>
					 
				<div class="col-md-4" style="min-width:430px;">
					<div class="box box-solid" style="box-shadow:none"> 
						<?php if(!empty($reads_data_weekly))
						{?>
					<div class="box-header text-center">
					  <h3 class="box-title">Aperturas por dia de la semana</h3>
					</div>
					<?php
							$this->widget('ext.highcharts.HighchartsWidget', array( 
								'scripts' => array(
									'themes/grid-light',	
									'highcharts-more',			
								),
								  
								'options' => array(
									'title' => array(
										'text' => '',
									),
									'lang'=> array(
									  'decimalPoint'=> ',',
									  'thousandsSep'=> '.'
									),
									'chart' => array(
										'plotBackgroundColor' => '#ffffff',
										'plotBorderWidth' => null,
										'plotShadow' => false,
										'width' => 420,
										'height' => 400,
										'reflow' => true,
										 
									  ),
									'tooltip'=>array(
										'headerFormat'=> '',
										'backgroundColor'=> 'rgba(0, 0, 0, 0.85)',
										'borderColor'=>  '#000',
										'borderRadius'=>  5,
										'style'=>array( 'color'=> '#e0e0e0','fontSize'=> '13px','padding'=> '6px 8px'),
										'pointFormat'=> '{point.fullName}: <strong>{point.y:,.0f}</strong>',			
									),
									
									'xAxis'=> array('type'=>'category','tickInterval'=> 3),			
									'yAxis'=> array('min'=> 0,'title'=>''),
									'credits' => array('enabled' => false),
									'plotOptions'=> array('bar'=> array('shadow'=> 'true', 'dataLabels'=> array('enabled'=>true))),
									'series' => array(				
										array( 
											'type' => 'column', 
											'name' => 'Aperturas',
											'backgroundColor' => '#f4f4f4', 				
											'data' => array(
											array('name'=>'Do', 'color'=> '#c52a33', 'y'=> !empty($reads_data_weekly[0])?$reads_data_weekly[0]:0, 'fullName'=> 'Domingo'),
											array('name'=>'Lu', 'color'=> '#00b5ea', 'y'=> !empty($reads_data_weekly[1])?$reads_data_weekly[1]:0, 'fullName'=> 'Lunes'),
											array('name'=>'Ma', 'color'=> '#2b908f', 'y'=> !empty($reads_data_weekly[2])?$reads_data_weekly[2]:0, 'fullName'=> 'Martes'),
											array('name'=>'Mi', 'color'=> '#f7a35c', 'y'=> !empty($reads_data_weekly[3])?$reads_data_weekly[3]:0, 'fullName'=> 'Miercoles'),
											array('name'=>'Ju', 'color'=> '#434348', 'y'=> !empty($reads_data_weekly[4])?$reads_data_weekly[4]:0, 'fullName'=> 'Jueves'),
											array('name'=>'Vi', 'color'=> '#7cb5ec', 'y'=> !empty($reads_data_weekly[5])?$reads_data_weekly[5]:0, 'fullName'=> 'Viernes'),
											array('name'=>'Sa', 'color'=> '#f89135', 'y'=> !empty($reads_data_weekly[6])?$reads_data_weekly[6]:0, 'fullName'=> 'Sabado'),
											),
										),
									),
								)
							));
						}
						else { ?>
						<div class="box-header">
						  <h3 class="box-title">Aperturas por dia de la semana</h3>
						</div>
						<div class="box-body">
						  No se puede cargar el gráfico
						</div>
						 
						 
						<?php } ?>
					</div>
				</div>
				<div class="col-md-1"></div>
			</div>
		</div>
		
		<?php if(!empty($open_details) && 0){ ?>
	<div class="col-md-10" style="margin-top: 50px; ">
		<table id="datetable" class="table table-bordered table-hover dataTable table-list" style="padding-left:20px;">
			<thead>
				<tr>
				  <th style="width: 50%;">Email</th>
				  <th style="width: 25%">Primer Lectura</th> 
					<th style="width: 25%">Lecturas</th> 
				</tr>
			</thead>
			<tbody>
				<?php $open_details=json_decode($open_details,1);
					foreach($open_details as $key => $val){
						 
						echo '<tr>';
						echo'<td style="width: 50%;">'.$val['email'].'</td>';
						echo'<td style="width: 25%">'.date('d/m/Y',$val['date']).'</td>';		
						if (!empty($val['cant'])) echo '<td style="width: 25%">'.$val['cant'].'</td>';else echo'<td style="width: 25%">No hay datos</td>';		
						echo'</tr>';
						
					}
				?>	
			</tbody>
		</table>
	</div>
	<?php 	} ?>
	
	
	</div>
<div class="col-md-2"></div>
</div>
<?php 
	// DataTables
	#Yii::app()->clientScript->registerScriptFile('//cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js',CClientScript::POS_END); 
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/datatables/jquery.dataTables.min.js',CClientScript::POS_END);
?>
<style>
.dataTables_paginate .ui-state-disabled {
     display:none;
}
</style>
<script>
$( document ).ready(function() {
$.fn.dataTableExt.oPagination.iFullNumbersShowPages = 3; 
$('#datetable').DataTable({
		"bLengthChange": false,
		"bInfo": false,
		"bAutoWidth": false,
		"pagingType": "full_numbers",
		"sDom": 'tp',
		"iDisplayLength": 10,
		"language": {
			"sSearch":"",
	        "paginate": {
	            first:    '<i class="fa fa-forward fa-flip-horizontal" data-toggle="tooltip" title="Primera página"></i>',
	            previous: '<i class="fa fa-caret-right fa-flip-horizontal" data-toggle="tooltip" title="Anterior"></i>',
	            next:     '<i class="fa fa-caret-right" data-toggle="tooltip" title="Siguiente"></i>',
	            last:     '<i class="fa fa-forward" data-toggle="tooltip" title="Última página"></i>'
	        },
		},
	});
	
	oTable = $('#datetable').DataTable();  

	$('#criterio').keyup(function(){
		   oTable.search($(this).val()).draw() ;
	})
	
	$(".right-icon-search").click(function(){
			$(this).parent().find("input").val('');
			$(this).css('color','#ccc');
			$('#buscar').trigger('click');	
	var table = $('#datetable').DataTable();
	table
	 .search( '' )
	 .columns().search( '' )
	 .draw();	
	});


});	

 
</script>