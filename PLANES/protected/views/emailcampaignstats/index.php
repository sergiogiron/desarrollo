<?php
/* @var $this EmailcampaignstatsController */
/* @var $model Emailcampaignstats */

$this->breadcrumbs=array(
	'Email Campaign Stats',
);
?>
 
<!--//<script type="text/javascript" src="http://www.chartjs.org/assets/Chart.js"></script>-->
<?php Yii::app()->clientScript->registerScriptFile('https://code.highcharts.com/highcharts.js',CClientScript::POS_END); ?>

<section class="content-header">
  <h1>
	Estad&iacute;sticas
	<small>Inicio</small>
  </h1>
  <ol class="breadcrumb">
	<li><a href="<?php echo Yii::app()->request->baseUrl; ?>"><i class="fa fa-dashboard"></i> Home</a></li>
	<li><a href="<?php echo Yii::app()->request->baseUrl; ?>/estadisticas/admin">Estad&iacute;sticas</a></li>
	<li class="active">Inicio</li>
  </ol>
</section>

<section class="content">
	<div class="box box-danger">
		<div class="box-header with-border">
		
		
		 <!-- Custom Tabs -->
		  <div class="nav-tabs">
			<ul class="nav nav-tabs">
			   <li><a href="#tab_0" data-toggle="tab"><i class="fa fa-info-circle"></i> Sumario</a></li>
			   <li class="active"><a href="#tab_1" data-toggle="tab"><i class="fa fa-pie-chart"></i> Principal</a></li>
			   <li><a href="#tab_2" data-toggle="tab"><i class="fa fa-folder-open-o"></i> Detalle de aperturas</a></li> 
			   <li><a href="#tab_3" data-toggle="tab"><i class="fa fa-desktop"></i>  Dispositivos</a></li>
			   <li><a href="#tab_4" data-toggle="tab"><i class="fa fa-hand-pointer-o"></i> Detalle de clicks</a></li> 
			   <li><a href="#tab_5" data-toggle="tab"><i class="fa fa-minus-circle"></i> Detalle de rechazados</a></li> 
			   
			</ul>
			<div class="tab-content">
				<div class="tab-pane" id="tab_0">
					<div class="col-md-10" style="margin-top:10px;">
					  <div class="box box-solid">
						<div class="box-header with-border">
						 <i class="fa fa-info-circle"></i>
						  <h1 class="box-title">Información general</h1>
						</div>
						<!-- /.box-header -->
						<div class="box-body"> 
						
						  <h5><b>Campaña</b> Últimas 48 horas hasta 24 cuotas sin interés en vuelos - Rosario</h4> 
						  <h5><b>Asunto</b> Últimas 48 horas hasta 24 cuotas sin interés en vuelos</h4>
						  <h5><b>Destinatarios</b> 2.470</h4>
						   <div class="col-md-6">
						  <h5><b>Estado</b> 24/06/2016 18:42</h5>
						  
						  <h5><b>Inicio</b> 25/06/2016 08:00</h5>
						 
						  <h5><b>Completado</b> 25/06/2016 08:29	</h5>
						  </div> 
						  <div class="col-md-6">
							  <h5><b>Remitente</b> TTS Viajes (rosario@ttsviajes.com)</h5>  
							  <h5><b>Respuestas</b> -</h5>
							</div>
						  
						  
						  <div class="col-md-2">
						  <span class="label label-success">Seguimiento de aperturas	</span>
						  <span class="label label-success"> Seguimiento de clicks</span>
						  </div>
						  <div class="col-md-2">
						   <span class="label label-danger">  Google Analytics	</span>
							 <span class="label label-danger">   ClickTale	</span>
						
							</div>
						<div class="col-md-12">
						<h5><b>Listas de destinatarios:</b></h4>
						
							0_TTS_ROSARIO
							</div>
						</div>
						<!-- /.box-body -->
					  </div>
					  <!-- /.box -->
					</div>				  
				</div>
				
				<div class="tab-pane active" id="tab_1">
				<div class="row">
					<div class="pies" style="">
						<div class="chart delivered" style="margin-left: 30px; display: inline-block;    width: 250px;    vertical-align: top;    position: relative;">
								<div class="title" style="margin:15px 0px 0px 80px;">Entregados</div>
								<div class="pie"></div> 
								<div class="center_per" style="font-size:24px;font-family: Oswald,'Open sans',Tahoma;font-weight: 700;text-align: center;color: #84bb20;    display: block;    position: absolute;    z-index: 1;    width: 101px;    height: 101px;    border-radius: 50px;    top: 130px;    left: 69px;    line-height: 101px;">100<span class="per">%</span></div>
								 
								<div class="data">
									<span><strong class="icon-users ic" style="color: #48B600;"></strong>Suscriptores:</span>
									<strong>2.470</strong>
								</div>
								<div class="data">
									<span><strong class="icon-delivered ic" style="color: #0096CA;"></strong>Enviados:</span>
									<strong style="color: #555">2.470</strong> <b style="color: #6997AD">(100%)</b>
								</div>
								<div class="data">
									<span><strong class="icon-mail ic" style="color: #edc240"></strong>Entregados:</span>
									<strong style="color: #555">2.470</strong> <b style="color: #6997AD">(100%)</b>
								</div>
								<div class="data">
									<span><strong class="icon-spam ic" style="color: #cb4b4a"></strong>Rechazados:</span>
									<strong style="color: #555">0</strong>
									<strong style="color: #cb4b4a">0%</strong>
								</div>
						</div>
					 
						<div class="chart openrate" style="margin: 0 115px; display: inline-block;    width: 250px;    vertical-align: top;    position: relative;">
								<div class="title" style="margin:15px 0px 0px 80px;">Aperturas</div>
								<div class="pie"></div> 
								<div class="center_per" style="font-size:24px;font-family: Oswald,'Open sans',Tahoma;font-weight: 700;text-align: center;color: #84bb20;    display: block;    position: absolute;    z-index: 1;    width: 101px;    height: 101px;    border-radius: 50px;    top: 130px;    left: 69px;    line-height: 101px;">6.48<span class="per">%</span></div>
				 
								<div class="data">
									<span><strong class="icon-mail2 ic" style="color: #3e9ca8"></strong>Aperturas:</span>
									<strong style="color: #555">160</strong>
								</div>

								<div class="data">
									<span><strong class="icon-mail2 ic" style="color: #ee7e66"></strong>No leidos:</span>
									<strong style="color: #555">2.310</strong>
									<strong style="color: #ee7e66">93.52%</strong>
								</div>


								<div class="data">
									<span>Unicas/Totales:</span>
									<strong style="color: #555">160 / 253</strong>
								</div>
								<div class="data">
									<span>Radio:</span>
									<strong style="color: #555">1,58</strong>
								</div>
						</div><!--

							-->
						<div class="chart click" style="display: inline-block;    width: 250px;    vertical-align: top;    position: relative;">
								<div class="title" style="margin:15px 0px 0px 80px;">Clicks (CTRO)</div>
								<div class="pie"></div> 
								<div class="center_per" style="font-size:24px;font-family: Oswald,'Open sans',Tahoma;font-weight: 700;text-align: center;color: #84bb20;    display: block;    position: absolute;    z-index: 1;    width: 101px;    height: 101px;    border-radius: 50px;    top: 130px;    left: 69px;    line-height: 101px;">23.13<span class="per">%</span></div> 

								<div class="data">
									<span><strong class="icon-click ic" style="color: #81ad38"></strong>Clicks:</span>
									<strong>37</strong>
								</div>
								<div class="data">
									<span><strong class="icon-click ic" style="color: #666"></strong>Clicks totales:</span>
									<strong style="color: #666">43</strong>
								</div>
						</div>
					</div>
					
					
					
					<div class="pies" style="margin-top: 30px; margin-left: 15px;padding-top:20px;border-top: 1px solid #ddd;">
						<div class="chart" style="display: inline-block;    width: 250px;    vertical-align: top;    position: relative;">
							<div class="data">
								<span><strong class="icon-referred ic" style="color: #6ab550;"></strong>Referidos:</span>
								<strong style="color: #555">0</strong>
							</div>
							<div class="data">
								<span style="width: 170px!important;"><strong class="icon-mail2 ic" style="color: #6ab550;"></strong>Lecturas-Referidos:</span>
								<strong style="color: #555">0 (0%)</strong>
							</div>
							<div class="data">
								<span style="width: 170px!important;"><strong class="icon-mail2 ic" style="color: #6a9dcf;"></strong>Total de lecturas:</span>
								<strong style="color: #555">160</strong>
							</div>
						</div> 
						
						<div class="chart" style="margin: 0 115px; display: inline-block;    width: 250px;    vertical-align: top;    position: relative;">
							<div class="data">
								<span style="width: 175px"><strong class="icon-uadd ic" style="color: #6ab550;"></strong>Nuevos suscriptores:</span>
								<strong style="color: #555">0</strong>
							</div>
										<div class="data">
								<span><strong class="icon-unsus ic" style="color: #cb4b4a;"></strong>Desuscriptos:</span>
								<strong style="color: #555">0</strong>
							</div>
						</div>
						<div class="chart"></div>
					</div> 
				</div>
				</div>
				 
					<!-- /.tab-pane -->
					 
				<div class="tab-pane" id="tab_2">
						<div style="page-break-after:always"></div>
							<div class="GReads stats_pane">
								 
								<div id="reads_detail">
									<div id="reads_graph" style="margin-top:25px; width: 950px;height: 280px"></div> 
									<div id="readsWeekHour"style="margin-top:50px;">
										<div id="reads_week" style="margin-right: 30px;    width: 390px;    display: inline-block;    height: 280px;"></div><!--
									 --><div id="reads_hour"style="margin-left:30px; width: 500px;    display: inline-block;    height: 280px;"></div> 
									</div>
								</div>
							</div>
				</div>
 
					
					  
					<!-- /.tab-pane -->
					<div class="tab-pane" id="tab_3">
					 
					<div class="horizontal" style="margin-top: 30px; margin-left:25px;">
		
						<div class="half" style="    display: inline-block;    width: 470px;">

							<div class="title">Tipo de dispositivos</div>
							
							<div class="devType pie"style="display: inline-block;height: 230px;vertical-align: top;font-size: 14px;width: 240px;overflow: hidden;"></div>
							<div class="data" style="padding-top: 16px;     display: inline-block;vertical-align: top;font-size: 14px;width: 220px;position: relative;">
								<div class="device" style="color: #e96295;"><i class="icon-desktop" style="color: #e96295"></i>Computadoras <span class="per">55.26%</span></div>
								<div class="device" style="color: #c67bbc;"><i class="icon-phone" style="color: #c67bbc"></i>Telefonos <span class="per">38.82%</span></div>
								<div class="device" style="color: #f2cfa9;"><i class="icon-tablet" style="color: #f2cfa9"></i>Tablets <span class="per">5.92%</span></div>
							</div>
						</div>
												
						<div class="half" style="display: inline-block;   width: 472px;">

							<div class="title">Clientes de correo</div>

							<div class="client pie"style="display: inline-block;height: 230px;vertical-align: top;font-size: 14px;width: 240px;overflow: hidden;"></div>
							<div class="data ec" style="padding-top: 16px;     display: inline-block;vertical-align: top;font-size: 14px;width: 220px;position: relative;">
								<div class="device"><i class="icon-mail" style="color: #f6d67e"></i>Thunderbird <span class="per">0.81%</span></div>
								<div class="device"><i class="icon-mail" style="color: #c3d7c4"></i>Gmail/Android Email <span class="per">7.32%</span></div>
								<div class="device"><i class="icon-mail" style="color: #94939D"></i>Web Browser <span class="per">91.87%</span></div>
							</div>
						</div>						
						
					</div>

					<div class="horizontal" style="margin-top: 50px;    margin-left:25px;">
						<div class="half" style="    display: inline-block;    width: 472px;">
							<div class="title">Navegador</div>
							<div class="browser pie"style="display: inline-block;height: 230px;vertical-align: top;font-size: 14px;width: 240px;overflow: hidden;"></div>
							<div class="data ec" style="padding-top: 16px;     display: inline-block;vertical-align: top;font-size: 14px;width: 220px;position: relative;">
								<div class="device"><i class="icon-chrome" style="color: #9882bc"></i>Chrome <span class="per">23.84%</span></div>
								<div class="device"><i class="icon-firefox" style="color: #b3ed61"></i>Firefox <span class="per">2.65%</span></div>
								<div class="device"><i class="icon-safari" style="color: #e96295"></i>Safari <span class="per">19.87%</span></div>
								<div class="device"><i class="icon-ie" style="color: #7e6680"></i>Internet explorer <span class="per">28.48%</span></div>
								<div class="device"><i class="icon-browser" style="color: #e96295"></i>Otros <span class="per">25.17%</span></div>
							</div>
						</div>		
						<div class="half" style="    display: inline-block;    width: 470px;">
							<div class="title">Sistema operativo</div>
							<div class="so pie" style="display: inline-block;height: 230px;vertical-align: top;font-size: 14px;width: 240px;overflow: hidden;"></div>
							<div class="data ec" style="padding-top: 16px;     display: inline-block;vertical-align: top;font-size: 14px;width: 220px;position: relative;">
								<div class="device"><i class="icon-windows" style="color: #3bb7e7"></i>Windows <span class="per">52.98%</span></div>
								<div class="device"><i class="icon-apple" style="color: #8bda61"></i>Mac OSX <span class="per">1.99%</span></div>
								<div class="device"><i class="icon-apple" style="color: #e96295"></i>Apple iOS <span class="per">23.18%</span></div>
								<div class="device"><i class="icon-android" style="color: #9882bc"></i>Android <span class="per">21.19%</span></div>
								<div class="device"><i class="icon-desktop" style="color: #f3dd42"></i>Otros <span class="per">0.66%</span></div>
							</div>
						</div>
					</div>

					
					
					
					
					
					
					
					</div>
					<!-- /.tab-pane -->
					<div class="tab-pane" id="tab_4">
					
					
					
					
					
					<div id="clicks_detail">
						<div class="pies" style="margin-top: 20px;">
							<div class="chart clicks_ctr" style="display: inline-block;width: 250px;vertical-align: top;position: relative;">
								<div class="title">Click-to Rate (CTR)</div>
								<div class="pie"></div> 
								<div class="center_per" style="font-size: 24px;font-family: Oswald,'Open sans',Tahoma;font-weight: 700;text-align: center;color: #84bb20;
    display: block;position: absolute;z-index: 1;width: 101px;height: 101px;border-radius: 50px;top: 93px;left: 69px;line-height: 101px;">1.5<span class="per">%</span></div>
								<span class="openhelp icon-help" title="Necesita ayuda para entender las estad&iacute;sticas?"></span>

								<div class="data">
									<span><strong class="icon-window ic" style="color: #81ad38"></strong>Clicks:</span>
									<strong>37</strong>
								</div>
								<div class="data">
									<span><strong class="icon-window ic" style="color: #666"></strong>Clicks totales:</span>
									<strong style="color: #666">43</strong>
								</div>
							</div>

							<div class="chart clicks_ctor" style="margin: 0 100px; display: inline-block;width: 250px;vertical-align: top;position: relative;">
								<div class="title">Click-to Open Rate (CTOR)</div>
								<div class="pie"></div> 
								<div class="center_per" style="font-size: 24px;font-family: Oswald,'Open sans',Tahoma;font-weight: 700;text-align: center;color: #84bb20;
    display: block;position: absolute;z-index: 1;width: 101px;height: 101px;border-radius: 50px;top: 93px;left: 69px;line-height: 101px;">23.13<span class="per">%</span></div>
								<span class="openhelp icon-help" title="Necesita ayuda para entender las estad&iacute;sticas?"></span>
							</div>

							<div class="chart links" style="display: inline-block;width: 250px;vertical-align: top;position: relative;">
								<div class="title">Detalles de links</div>
								<div class="pie"></div> 
							</div>
						</div>
						<br />
						<div class="listOptions">
							<div class="list">
								<div class="title"><span class="icon-click"></span>Lista de links</div>
								<div class="links_list">
									<div class="table4">
										<table cellspacing="0">
											<thead>
												<tr>
													<td class="sortable">Link<span class="icon-caretdown"></span></td>
													<td class="sortable">Únicos</td>
													<td class="sortable">Clicks</td>
													<td class="sortable"></td>
												</tr>
											</thead>
											<tbody>
																				<tr>
													<td title="http://www.ttsviajes.com/e-news/tts_placer/2016-06-25/index.html?utm_source=email&amp;utm_medium=suc&amp;utm_campaign=news2016-06-25">http://www.ttsviajes.com/e-news/tts_placer/2016-06-25/index.... 
														<a href="http://www.ttsviajes.com/e-news/tts_placer/2016-06-25/index.html?utm_source=email&utm_medium=suc&utm_campaign=news2016-06-25" target="_blank" class="openLink" title="Abrir link">
														<span class="icon-popup"></span>
														</a>
													</td>
													<td style="font-weight: 700;">6</td>
													<td>8</td>
													<td>
																						</td>
												</tr>
														<tr>
															<td title="https://www.youtube.com/user/TTSViajes">https://www.youtube.com/user/TTSViajes 
																<a href="https://www.youtube.com/user/TTSViajes" target="_blank" class="openLink" title="Abrir link">
																<span class="icon-popup"></span>
																</a>
															</td>
															<td style="font-weight: 700;">0</td>
															<td>0</td>
															<td>
															</td>
														</tr>														 
											</tbody>
										</table>
									</div>
								</div>
								<div class="clicks_details_table">			
								</div>
							</div>
						</div>
					</div>
					
					
					
					
					
					
					
					</div>
					<!-- /.tab-pane -->
					<div class="tab-pane" id="tab_5">
					
					</div>
					<!-- /.tab-pane -->
					
			 </div>
			<!-- /.tab-content -->
		  </div>
		  <!-- nav-tabs-custom -->
		  
		  
		
		</div>
	</div>
 </section>
 
 
 <script>  
$( document ).ready(function() {

// ******************************************************************************************************
	// main  
	// grafico entregados 
	$('.pies .delivered .pie').highcharts({
		chart: { type: 'pie', height: 300, width: 250},
		title: "Delivered",
		plotOptions: { pie: { shadow: false,center: ['45%', '50%']}},
		tooltip: {
			headerFormat: '',
			
			pointFormat: '{point.name}<br /><strong>{point.y}</strong> ({point.dataPer}%)',
			backgroundColor: 'rgba(0, 0, 0, 0.85)',
			borderColor: '#000',
			borderRadius: 5,
			style: { color: '#e0e0e0',fontSize: '13px',padding: '6px 8px'}
			
		},

		series: [{
			name: 'Delivery',
			data: [{
				name: "Entregados",
				y: 2470,
				color: '#edc240',
				dataPer: 100,
				dataLabels: { enabled: false}
			},{	name: "No enviados",
				y: 0,
				dataPer: 0,
				color: '#cb744a',
				dataLabels: { enabled: false}
			},{	name: "Rechazados",
				y: 0,
				dataPer: 0,
				color: '#cb4b4a',
				dataLabels: { enabled: false}
			}],
			size: '100%',
			innerSize: '55%'
		}],
	});
	
	// ******************************************************************************************************
	// main aperturas
	$('.pies .openrate .pie').highcharts({
		chart: { type: 'pie',  height: 300, width: 250},
		title: "Aperturas",
		plotOptions: { pie: { shadow: false,center: ['45%', '50%'],startAngle: 0}},
		tooltip: {
			headerFormat: '',
			
			pointFormat: '{point.name}<br /><strong>{point.y}</strong> ({point.dataPer}%)',
			backgroundColor: 'rgba(0, 0, 0, 0.85)',
			borderColor: '#000',
			borderRadius: 5,
			style: { color: '#e0e0e0',fontSize: '13px',padding: '6px 8px'}
			
		},
		series: [{
			name: 'Aperturas',
			data: [{
				name: "Aperturas",
				y: 160,
				dataPer: 6.48,
				color: '#3e9ca8',
				dataLabels: { enabled: false}
			},{	name: "No leidos",
				y: 2310,
				dataPer: 93.52,
				color: '#ee7e66',
				dataLabels: { enabled: false}
			}],
			size: '100%',
			innerSize: '55%'
		}],
	});


	// ******************************************************************************************************
	// main clicks 

	$('.pies .click .pie').highcharts({
		chart: { type: 'pie',  height: 300, width: 250},
		title: "Click rate",
		plotOptions: { pie: { shadow: false,center: ['45%', '50%']}},
		tooltip: {
			headerFormat: '',
			
			pointFormat: '{point.name}: <strong>{point.y}</strong>',
			backgroundColor: 'rgba(0, 0, 0, 0.85)',
			borderColor: '#000',
			borderRadius: 5,
			style: { color: '#e0e0e0',fontSize: '13px',padding: '6px 8px'}
			
		},
		series: [{
			name: 'Clicks',
			data: [{
				name: "Clicks de lectores unicos",
				y: 37,
				color: '#81ad38',
				dataLabels: { enabled: false}
			},{	name: "No hicieron click",
				y: 123,
				color: '#9988b2',
				dataLabels: { enabled: false}
			}],
			size: '100%',
			innerSize: '55%'
		}],
	});

	
	
	
	
	// Tab2******************************************************************************************************
	// grafico de lecturas 
	$('#reads_graph').highcharts({
		chart: { type: 'line', backgroundColor: '#f4f4f4', width: 950},
		title: { text: 'Evolución de las lecturas/aperturas' },
		xAxis: { type: 'datetime', title: { enabled: false}},
		plotOptions: { line: { center: ['45%', '50%']}},
		yAxis: { title: { text: 'Lecturas' },
				min: 0 },

		legend: { enabled: false },
		tooltip: {
			headerFormat: '<b>Lecturas</b><br>',
			
			pointFormat: '{point.x:%e/%b/%y}: <strong>{point.y}</strong>',
			backgroundColor: 'rgba(0, 0, 0, 0.85)',
			borderColor: '#000',
			borderRadius: 5,
			style: { color: '#e0e0e0',fontSize: '13px',padding: '6px 8px'}
			
		},
		series: [{
			allowPointSelect: true,
			name: 'Lecturas',
			pointInterval: 24 * 3600 * 1000,
			pointStart: Date.UTC(2016, 5, 25),
			data: [90,22,37,5,2,2,1,0,0,1,0,],
		}]
	});

	
	
	
	// grafico lecturas por semana por hora del dia
	$('#reads_hour').highcharts({
		chart: { type: 'spline', backgroundColor: '#f4f4f4', width: 500, height: 280},
		title: { text: 'Lecturas por hora del dia'},
		legend: { enabled: false },
		xAxis: { type: 'category', tickInterval: 3, labels: { style: {
			fontSize: '14px'
		}}},
		yAxis: {
			title: { enabled: false},
		},
		tooltip: {
			headerFormat: '',
			
			pointFormat: '{point.x}hs: {point.y}',
			backgroundColor: 'rgba(0, 0, 0, 0.85)',
			borderColor: '#000',
			borderRadius: 5,
			style: { color: '#e0e0e0',fontSize: '13px',padding: '6px 8px'}
			
		},
		series: [{
			name: 'Lecturas por hora del dia',
			allowPointSelect: true,
			lineWidth: 3,
			data: [
							{ name: '0hs', color: '#3e9ca8', y: 3 },
							{ name: '1hs', color: '#3e9ca8', y: 2 },
							{ name: '2hs', color: '#3e9ca8', y: 0 },
							{ name: '3hs', color: '#3e9ca8', y: 0 },
							{ name: '4hs', color: '#3e9ca8', y: 0 },
							{ name: '5hs', color: '#3e9ca8', y: 1 },
							{ name: '6hs', color: '#3e9ca8', y: 1 },
							{ name: '7hs', color: '#3e9ca8', y: 2 },
							{ name: '8hs', color: '#3e9ca8', y: 30 },
							{ name: '9hs', color: '#3e9ca8', y: 23 },
							{ name: '10hs', color: '#3e9ca8', y: 25 },
							{ name: '11hs', color: '#3e9ca8', y: 11 },
							{ name: '12hs', color: '#3e9ca8', y: 8 },
							{ name: '13hs', color: '#3e9ca8', y: 6 },
							{ name: '14hs', color: '#3e9ca8', y: 3 },
							{ name: '15hs', color: '#3e9ca8', y: 8 },
							{ name: '16hs', color: '#3e9ca8', y: 10 },
							{ name: '17hs', color: '#3e9ca8', y: 3 },
							{ name: '18hs', color: '#3e9ca8', y: 4 },
							{ name: '19hs', color: '#3e9ca8', y: 6 },
							{ name: '20hs', color: '#3e9ca8', y: 4 },
							{ name: '21hs', color: '#3e9ca8', y: 3 },
							{ name: '22hs', color: '#3e9ca8', y: 3 },
							{ name: '23hs', color: '#3e9ca8', y: 4 },
			],
		}]
	});

	// ******************************************************************************************************
	// grafico lecturas por semana
	$('#reads_week').highcharts({
		chart: { type: 'column', backgroundColor: '#f4f4f4', width: 390, height: 280 },
		title: { text: 'Lecturas por dia de la semana'},
		xAxis: { type: 'category', labels: { style: {
			fontSize: '14px'
		}}},
		tooltip: {
			headerFormat: '',
			
			pointFormat: '{point.fullName}: <strong>{point.y}</strong>',
			backgroundColor: 'rgba(0, 0, 0, 0.85)',
			borderColor: '#000',
			borderRadius: 5,
			style: { color: '#e0e0e0',fontSize: '13px',padding: '6px 8px'}
			
		},
		yAxis: { min: 0,	title: { text: ''}},
		legend: { enabled: false },
		plotOptions: { bar: { shadow: true, dataLabels: { enabled: true } }},
		series: [{
			name: 'Lecturas',
			data: [ { name: 'Do', color: '#c52a33', y: 20, fullName: 'Domingo'},
					{ name: 'Lu', color: '#00b5ea', y: 39, fullName: 'Lunes'},
					{ name: 'Ma', color: '#00b5ea', y: 2, fullName: 'Martes'},
					{ name: 'Mi', color: '#00b5ea', y: 2, fullName: 'Miercoles'},
					{ name: 'Ju', color: '#00b5ea', y: 3, fullName: 'Jueves'},
					{ name: 'Vi', color: '#00b5ea', y: 0, fullName: 'Viernes'},
					{ name: 'Sa', color: '#f89135', y: 94, fullName: 'Sabado'},
			],
		}]
	});


	
	
  

	
	
	
	
	// ******************************************************************************************************
	// devices chart
	$('.devType').highcharts({
		chart: { type: 'pie', width: 240, height: 240},
		title: "Dispositivos",
		plotOptions: { pie: { shadow: false,center: ['45%', '50%'],startAngle: 0}},
		tooltip: {
			headerFormat: '',
			
			pointFormat: '{point.name}<br /><strong>{point.y}</strong> ({point.dataPer}%)',
			backgroundColor: 'rgba(0, 0, 0, 0.85)',
			borderColor: '#000',
			borderRadius: 5,
			style: { color: '#e0e0e0',fontSize: '13px',padding: '6px 8px'}
			
		},
		series: [{
			name: 'Dispositivos',
			data: [
				{ name: "Telefonos",    y: 38.82, color: '#c67bbc', dataLabels: { enabled: false} },
				{ name: "Tablets",           y: 5.92, color: '#f2cfa9', dataLabels: { enabled: false} },
				{ name: "Computadoras", y: 55.26, color: '#e96295', dataLabels: { enabled: false} },
			],
			size: '100%',
			innerSize: '55%'
		}],
	});

	// ******************************************************************************************************
	// SO chart
	$('.client').highcharts({
		chart: { type: 'pie', width: 240, height: 240},
		title: "Clientes de correo",
		plotOptions: { pie: { shadow: false,center: ['45%', '50%'],startAngle: 0}},
		tooltip: {
			headerFormat: '',
			
			pointFormat: '{point.name}<br /><strong>{point.y}</strong> ({point.dataPer}%)',
			backgroundColor: 'rgba(0, 0, 0, 0.85)',
			borderColor: '#000',
			borderRadius: 5,
			style: { color: '#e0e0e0',fontSize: '13px',padding: '6px 8px'}
			
		},
		series: [{
			name: 'Clientes de correo',
			data: [
				{ name: "Apple Mail",          y: 0, color: '#f1799e', dataLabels: { enabled: false} },
				{ name: "Outlook",             y: 0, color: '#9be2d0', dataLabels: { enabled: false} },
				{ name: "Outlook Express",     y: 0, color: '#f179a3', dataLabels: { enabled: false} },
				{ name: "Outlook 2010",        y: 0, color: '#7ec5f6', dataLabels: { enabled: false} },
				{ name: "Outlook 2013",        y: 0, color: '#b17ef6', dataLabels: { enabled: false} },
				{ name: "Windows Live Mail",   y: 0, color: '#c8f67e', dataLabels: { enabled: false} },
				{ name: "Thunderbird",         y: 0.81, color: '#f6d67e', dataLabels: { enabled: false} },
				{ name: "The Bat!",            y: 0, color: '#f6c57e', dataLabels: { enabled: false} },
				{ name: "Gmail/Android Email", y: 7.32, color: '#c3d7c4', dataLabels: { enabled: false} },
				{ name: "Web Browser", y: 91.87, color: '#94939D', dataLabels: { enabled: false} },
			],
			size: '100%',
			innerSize: '55%'
		}],
	});

	//
	// ******************************************************************************************************
	// browsers chart
	$('.browser').highcharts({
		chart: { type: 'pie', width: 240, height: 240},
		title: "Navegador",
		plotOptions: { pie: { shadow: false,center: ['45%', '50%'],startAngle: 0}},
		tooltip: {
			headerFormat: '',
			
			pointFormat: '{point.name}<br /><strong>{point.y}</strong> ({point.dataPer}%)',
			backgroundColor: 'rgba(0, 0, 0, 0.85)',
			borderColor: '#000',
			borderRadius: 5,
			style: { color: '#e0e0e0',fontSize: '13px',padding: '6px 8px'}
			
		},
		series: [{
			name: 'Clientes de correo',
			data: [
				{ name: "Chrome",            y: 23.84, color: '#9882bc', dataLabels: { enabled: false} },
				{ name: "Firefox",           y: 2.65, color: '#b3ed61', dataLabels: { enabled: false} },
				{ name: "Safari",            y: 19.87, color: '#f3dd42', dataLabels: { enabled: false} },
				{ name: "Opera",             y: 0, color: '#3bb7e7', dataLabels: { enabled: false} },
				{ name: "Internet explorer", y: 28.48, color: '#7e6680', dataLabels: { enabled: false} },
				{ name: "Otros",     y: 25.17, color: '#e96295', dataLabels: { enabled: false} },
			],
			size: '100%',
			innerSize: '55%'
		}],
	});

	// ******************************************************************************************************
	// devices chart
	$('.so').highcharts({
		chart: { type: 'pie', width: 240, height: 240},
		title: "Sistema operativo",
		plotOptions: { pie: { shadow: false,center: ['45%', '50%'],startAngle: 0}},
		tooltip: {
			headerFormat: '',
			
			pointFormat: '{point.name}<br /><strong>{point.y}</strong> ({point.dataPer}%)',
			backgroundColor: 'rgba(0, 0, 0, 0.85)',
			borderColor: '#000',
			borderRadius: 5,
			style: { color: '#e0e0e0',fontSize: '13px',padding: '6px 8px'}
			
		},
		series: [{
			name: 'Sistema operativo',
			data: [{ name: "Windows",             y: 52.98,  color: '#3bb7e7', dataLabels: { enabled: false} },
				   { name: "Apple Macintosh OSX", y: 1.99,  color: '#8bda61', dataLabels: { enabled: false} },
				   { name: "Apple iOS",           y: 23.18,  color: '#e96295', dataLabels: { enabled: false} },
				   { name: "Android",             y: 21.19,  color: '#9882bc', dataLabels: { enabled: false} },
				   { name: "Blackberry",          y: 0,  color: '#7e6680', dataLabels: { enabled: false} },
				   { name: "Linux",               y: 0,  color: '#59b64e', dataLabels: { enabled: false} },
				   { name: "Otros",       y: 0.66,   color: '#f3dd42', dataLabels: { enabled: false} }],
			size: '100%',
			innerSize: '55%'
		}],
	});

	
	
	
	
	
	// grafico links ***************************************************************************************
	$('.pies .links .pie').highcharts({
		chart: { type: 'pie', width: 250 , height: 250},
		title: "Links",
		tooltip: {
			headerFormat: '',
			
			pointFormat: '{point.name}<br /><strong>{point.y}</strong> clicks',
			backgroundColor: 'rgba(0, 0, 0, 0.85)',
			borderColor: '#000',
			borderRadius: 5,
			style: { color: '#e0e0e0',fontSize: '13px',padding: '6px 8px'}
			
		},
		plotOptions: { pie: { shadow: false,center: ['50%', '50%'] }},
		series: [{
			name: 'Links',
			data: [
						{ name: "http://www.ttsviajes.com/e-news/tts_placer/2016-06-25/index....", y: 6, dataLabels: { enabled: false}},
						{ name: "https://www.youtube.com/user/TTSViajes", y: 0, dataLabels: { enabled: false}},
						{ name: "https://twitter.com/ttsviajes", y: 0, dataLabels: { enabled: false}},
						{ name: "https://www.instagram.com/ttsviajes/", y: 0, dataLabels: { enabled: false}},
						{ name: "https://www.facebook.com/TTSVIAJES", y: 0, dataLabels: { enabled: false}},
						{ name: "http://turismo.apteknet.com/ttsviajes/busqueda/BUE/RIO/01-12...", y: 0, dataLabels: { enabled: false}},
						{ name: "http://turismo.apteknet.com/ttsviajes/busqueda/BUE/SCL/24-08...", y: 2, dataLabels: { enabled: false}},
						{ name: "http://turismo.apteknet.com/ttsviajes/busqueda/EZE/MIA/06-10...", y: 1, dataLabels: { enabled: false}},
						{ name: "http://turismo.apteknet.com/ttsviajes/busqueda/EZE/BCN/12-10...", y: 3, dataLabels: { enabled: false}},
						{ name: "http://eviajes.online/ttsviajes/busquedaM/EZE-MEX-MEX-HAV-HA...", y: 3, dataLabels: { enabled: false}},
						{ name: "http://turismo.apteknet.com/ttsviajes/busqueda/EZE/BKK/11-09...", y: 2, dataLabels: { enabled: false}},
						{ name: "http://eviajes.online/ttsviajes/busquedaM/EZE-YYZ-YYZ-CDG-CD...", y: 0, dataLabels: { enabled: false}},
						{ name: "http://eviajes.online/ttsviajes/busquedaM/EZE-MIA-MIA-BCN-BC...", y: 8, dataLabels: { enabled: false}},
						{ name: "http://www.ttsviajes.com/?utm_source=email&amp;utm_medium=su...", y: 10, dataLabels: { enabled: false}},
						{ name: "http://www.ttsviajes.com/?utm_source=email&amp;utm_medium=su...", y: 2, dataLabels: { enabled: false}},
						],
			size: '100%',
			innerSize: '55%'
		}]
	});

	$('.pies .clicks_ctor .pie').highcharts({
		chart: { type: 'pie', width: 250, height: 250},
		title: "Click-to Open rate",
		plotOptions: { pie: { shadow: false,center: ['45%', '50%']}},
		tooltip: {
			headerFormat: '',
			
			pointFormat: '{point.name}: <strong>{point.y}</strong>',
			backgroundColor: 'rgba(0, 0, 0, 0.85)',
			borderColor: '#000',
			borderRadius: 5,
			style: { color: '#e0e0e0',fontSize: '13px',padding: '6px 8px'}
			
		},
		series: [{
			name: 'Clicks',
			data: [{
				name: "Clicks de lectores unicos",
				y: 37,
				color: '#81ad38',
				dataLabels: { enabled: false}
			},{	name: "No hicieron click",
				y: 123,
				color: '#9988b2',
				dataLabels: { enabled: false}
			}],
			size: '100%',
			innerSize: '55%'
		}],
	});

	$('.pies .clicks_ctr .pie').highcharts({
		chart: { type: 'pie', width: 250, height: 250},
		title: "Click-to rate",
		plotOptions: { pie: { shadow: false,center: ['45%', '50%']}},
		tooltip: {
			headerFormat: '',
			
			pointFormat: '{point.name}: <strong>{point.y}</strong>',
			backgroundColor: 'rgba(0, 0, 0, 0.85)',
			borderColor: '#000',
			borderRadius: 5,
			style: { color: '#e0e0e0',fontSize: '13px',padding: '6px 8px'}
			
		},
		series: [{
			name: 'Clicks',
			data: [{
				name: "Clicks de lectores unicos",
				y: 37,
				color: '#e5d800',
				dataLabels: { enabled: false}
			},{	name: "No hicieron click",
				y: 2433,
				color: '#ff3b61',
				dataLabels: { enabled: false}
			}],
			size: '100%',
			innerSize: '55%'
		}],
	}); 
	

	
	

	$("text:contains('Highcharts.com')").remove();
  });
</script>		

