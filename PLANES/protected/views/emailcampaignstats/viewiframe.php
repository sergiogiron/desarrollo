<?php 
	Yii::app()->getClientScript()->registerCssFile(Yii::app()->baseUrl.'/css/graph.css');

/* @var $this EmailcampaignstatsController */

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#mail-campaign-stats-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
"); 
?>

<section class="content-header">
  <h1>
	Estad&iacute;sticas.
	<small><?php echo $model->name; ?></small>
  </h1>
  <ol class="breadcrumb">
	<li><i class="fa fa-home"></i> Inicio</li>
	<li>Emailing</li>
  </ol>
</section>

<section class="content">
	<div class="box box-danger">
		<div class="box-header with-border">
		  <div class="nav-tabs2">
			<ul class="nav nav-tabs">
			   <li><a href="#tab_0" data-toggle="tab"><i class="fa fa-info-circle"></i> Información general</a></li>
			   <li class="active"><a href="#tab_1" data-toggle="tab"><i class="fa fa-pie-chart"></i> Principal</a></li>
			   <li><a href="#tab_2" data-toggle="tab"><i class="fa fa-area-chart"></i> Detalle de aperturas</a></li> 
			   <?php if(Yii::app()->user->checkAccess('viewcompletestats')){ ?>
			   <li><a href="#tab_3" data-toggle="tab"><i class="fa fa-desktop"></i>  Dispositivos</a></li>
			   <?php } ?>
			   <li><a href="#tab_4" data-toggle="tab"><i class="fa fa-crosshairs" aria-hidden="true"></i> Accesos a las ofertas</a></li> 
			   <li><a href="#tab_5" data-toggle="tab"><i class="fa fa-minus-circle"></i> Detalle de rechazados</a></li> 			   
			</ul>
			<div class="tab-content">
				<?php $this->renderPartial('//emailcampaignstats/tab0',array('model'=>$model,
														'open_details'=>$open_details
														));?>
				<?php $this->renderPartial('//emailcampaignstats/tab1',array('model'=>$model,'esIframe'=>true));?>
				<?php $this->renderPartial('//emailcampaignstats/tab2',array('model'=>$model,
														'reads_data_timelapse'=>$reads_data_timelapse,
														'reads_data_weekly'=>$reads_data_weekly,
														'reads_data_hourly'=>$reads_data_hourly,
														'open_details'=>$open_details
														));?>
				<?php if(Yii::app()->user->checkAccess('viewcompletestats')){ ?>
				<?php $this->renderPartial('//emailcampaignstats/tab3',array('model'=>$model,
														'device_dev_type'=>$device_dev_type,
														'device_client'=>$device_client,
														'device_browser'=>$device_browser,
														'device_os'=>$device_os
														));?>
				<?php } ?>
				<?php $this->renderPartial('//emailcampaignstats/tab4',array('model'=>$model,'esIframe'=>true));?>
				<?php $this->renderPartial('//emailcampaignstats/tab5',array('model'=>$model));?>
			 </div>
		  </div>
		</div>

		<div class="box-footer">
			<?php
				echo CHtml::button('Cerrar', array('onclick' => 'js:document.location.href="'.Yii::app()->request->baseUrl.'/iframe/estadisticas-email/'.$username.'"','class'=>'btn btn-secundary','data-toggle'=>'tooltip','title'=>'Volver'));
				?>
		</div>

	</div>
 </section>