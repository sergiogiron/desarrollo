<?php 
/*
Ejemplo
Highcharts Widget

Combination Chart

El ejemplo de demo de Highcharts: "Column, Line and Pie" con las opciones adicionales de exportar y el theme Grid Light activado.
*/

$this->widget('ext.highcharts.HighchartsWidget', array(
    'scripts' => array(
        'modules/exporting',
        'themes/grid-light',
    ),
    'options' => array(
        'title' => array(
            'text' => 'Combination chart',
        ),
        'xAxis' => array(
            'categories' => array('Apples', 'Oranges', 'Pears', 'Bananas', 'Plums'),
        ),
        'labels' => array(
            'items' => array(
                array(
                    'html' => 'Total fruit consumption',
                    'style' => array(
                        'left' => '50px',
                        'top' => '18px',
                        'color' => 'js:(Highcharts.theme && Highcharts.theme.textColor) || \'black\'',
                    ),
                ),
            ),
        ),
        'series' => array(
            array(
                'type' => 'column',
                'name' => 'Jane',
                'data' => array(3, 2, 1, 3, 4),
            ),
            array(
                'type' => 'column',
                'name' => 'John',
                'data' => array(2, 3, 5, 7, 6),
            ),
            array(
                'type' => 'column',
                'name' => 'Joe',
                'data' => array(4, 3, 3, 9, 0),
            ),
            array(
                'type' => 'spline',
                'name' => 'Average',
                'data' => array(3, 2.67, 3, 6.33, 3.33),
                'marker' => array(
                    'lineWidth' => 2,
                    'lineColor' => 'js:Highcharts.getOptions().colors[3]',
                    'fillColor' => 'white',
                ),
            ),
            array(
                'type' => 'pie',
                'name' => 'Total consumption',
                'data' => array(
                    array(
                        'name' => 'Jane',
                        'y' => 13,
                        'color' => 'js:Highcharts.getOptions().colors[0]', // Jane's color
                    ),
                    array(
                        'name' => 'John',
                        'y' => 23,
                        'color' => 'js:Highcharts.getOptions().colors[1]', // John's color
                    ),
                    array(
                        'name' => 'Joe',
                        'y' => 19,
                        'color' => 'js:Highcharts.getOptions().colors[2]', // Joe's color
                    ),
                ),
                'center' => array(100, 80),
                'size' => 100,
                'showInLegend' => false,
                'dataLabels' => array(
                    'enabled' => false,
                ),
            ),
        ),
    )
));



/***************************
Uso
---

 Para usar el widget, se debe insertar el sig codigo en la vista:

php
**/
$this->Widget('ext.highcharts.HighchartsWidget', array(
     'options'=>array(
        'title' => array('text' => 'Titulo del grafico'),
        'xAxis' => array(
           'categories' => array('Col_1', 'Col_2', 'Col_3')
        ),
        'yAxis' => array(
           'title' => array('text' => 'Valores eje Y')
        ),
        'series' => array(
           array('name' => 'Usuario_1', 'data' => array(1, 0, 4)),
           array('name' => 'Usuario_2', 'data' => array(5, 7, 3))
        )
     )
  ));
/****
Configurando {@link $options}, se pueden especificar las opciones que necesitan ser pasadas al objeto de js. 
Referencias en la documentacion {link http://http://api.highcharts.com/highcharts doc}
Aparte, se puede usar un JSON valido en lugar de una arreglo asociativo para especificar las opciones:
**/
  $this->Widget('ext.highcharts.HighchartsWidget', array(
     'options'=>'{
        "title": { "text": "Titulo del grafico" },
        "xAxis": {
           "categories": ["Col_1", "Col_2", "Col_3"]
        },
        "yAxis": {
           "title": { "text": "Valores eje Y" }
        },
        "series": [
           { "name": "Usuario_1", "data": [1, 0, 4] },
           { "name": "Usuario_2", "data": [5, 7,3] }
        ]
     }'
  ));
/****

Nota: Se debe proveer un JSON valido (Vease comillas dobles: "") {@link http://jsonlint.com/ JSONLint}.
 

Tips
----
Si se necesita usar JavaScript en las opciones de configuracion, se debe usar el prefijo `js:` . Por ejemplo:
>>php
*****/

  'tooltip' => array(
       'formatter' => 'js:function(){ return this.series.name; }'
  ),
  
/****
* Highcharts por default muestra un label con publicidad en la esquina derecha de abajo del grafico. Para sacarla se debe usar la siguiente opcion (nivel superior):

>>php
****/

  'credits' => array('enabled' => false),
  
/****
Desde la version 3.0.2, todos los adapters, modules, themes, deben estar activados en el nivel superior de las opciones del 'script'.


>>php
*****/


  'scripts' => array(
       'highcharts-more',   // activa graficos de tipo suplementario (gauge, arearange, columnrange, etc.)
       'modules/exporting', // agrega la opción de Exportar al grafico
       'themes/grid'        // usa el theme global 'grid' para los graficos
  ),
  
/****
 para un listado de los scripts posibles, ver el contenido de `protected/extensions/highcharts/assets/`.
 
 
Para acceder al objeto de JavaScript desde otro script, debe usar:

>>javascript
************/
  var chart = $('#chart-id').highcharts();
  
/****
  donde `chart-id` es seteado por la opcion de nivel superior `id` de la configuracion. Asegurarse de
  registrar el script despues de la declaracion del widget asi initializa.

