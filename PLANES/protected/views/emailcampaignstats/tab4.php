<div class="tab-pane" id="tab_4">	
<?php 

$rand = array('0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f');
$arreglo_colores=[];
for($i=0;$i<15;$i++){
$color = '#'.$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)];
array_push($arreglo_colores,$color);
}
$arreglo_colores = array('#e96295', '#3bb7e7', '#8bda61', '#9882bc', '#e93295', '#c52a33', '#f7a35c', '#f2cfa9', '#24e3f6', '#10a63d', '#c52a33', '#74c55c','#ed7db5','#e5ab15','#09d9de','#cf01a0',);
?>	
	<div class="row"> 
		<?php if(Yii::app()->user->checkAccess('viewcompletestats')){ ?>
		<div class="col-md-10" style="margin-top:20px; padding:10px;"> 	
				<div class="col-md-3 clicks_ctr">
					<div class="box box-solid" style="box-shadow:none">
						<?php if(!empty($model->sent_mails)&&!empty($model->clicks_unique)){	?>
								<div class="box-header">
								  <h3 class="box-title">Tasa de clics</h3>
								</div>						
						<?php
						$this->widget('ext.highcharts.HighchartsWidget', array(
							'scripts' => array(
								'themes/grid-light',
							),
							'options' => array(
								'title' => array(
									'text' => '',
								),
								'chart' => array(
									'plotBackgroundColor' => null,
									'plotBorderWidth' => null,
									'plotShadow' => false,
									'height' => 250, 
									'width' => 250,
									'spacingBottom' => 15,
									'spacingTop' => 10,
									'spacingLeft' => 10,
									'spacingRight' => 10,
									
								  ),
								'plotOptions'=> array( 'pie'=>array('shadow'=>false,'center'=>array(['50%','50%']))),
								'tooltip'=>array(
									'headerFormat'=> '',
									'pointFormat'=> '{point.name}: <strong>{point.y}</strong>',
									'backgroundColor'=> 'rgba(0, 0, 0, 0.85)',
									'borderColor'=>  '#000',
									'borderRadius'=>  5,
									'style'=>array( 'color'=> '#e0e0e0','fontSize'=> '13px','padding'=> '6px 8px'),
								),
								
								'credits' => array('enabled' => false),
								'series' => array(				
									array(
										'type' => 'pie',
										'name' => 'Clicks',
										'data' => array(
											array(
												'name' => 'Clicks de lectores unicos',
												'y' => round(!empty($model->clicks_unique)?$model->clicks_unique:0),
												'color'=> '#e5d800',
												'dataPer'=> round(!empty($model->clicks_unique)&& !empty($model->sent_mails)?$model->clicks_unique*100/($model->sent_mails):0),
												'dataLabels'=>  array('enabled'=>false),
											),
											array(
												'name' => 'No hicieron click',
												'y' => round(!empty($model->sent_mails)&&!empty($model->clicks_unique)?$model->sent_mails-$model->clicks_unique:0),
												'dataPer'=> round(!empty($model->sent_mails)&& !empty($model->clicks_unique)?$model->sent_mails-$model->clicks_unique*100/($model->sent_mails):0),
												'color'=>'#ff3b61',
												'dataLabels'=>  array('enabled'=> false),
											),
										),
										'size' => '100%',
										'innerSize' => '55%'
									),
								),
							)
						));
						?>
						<div class="center_per" style="margin-top:-30px; margin-left:30px;"><?php echo round(!empty($model->clicks_unique)&& !empty($model->sent_mails)?$model->clicks_unique*100/($model->sent_mails):0);?><span class="per">%</span></div>
						
						<?php
					}
					else { ?>
								<div class="box-header">
								  <h3 class="box-title">Tasa de clics</h3>
								</div>
								<div class="box-body">
								    <img src="<?php echo Yii::app()->request->baseUrl; ?>/img/emptypie.png" class="img-empty" title="No se puede cargar el gráfico" style="margin-top: -5px;"height="220px;" data-toggle="tooltip" >
								   <!--No se puede cargar el gráfico-->
								</div>
					<?php } ?>
					</div>
				</div>		 
			<div class="col-md-1"></div>
				<div class="col-md-3 clicks_ctor">
					<div class="box box-solid" style="box-shadow:none">				
					<?php if(!empty($model->reads_val)&&!empty($model->clicks_unique)){	?>							
							<div class="box-header" style="	margin-bottom:-15px;">
							  <h3 class="box-title">Cuantos accedieron a la oferta</h3>
							</div>
							<?php					
						$this->widget('ext.highcharts.HighchartsWidget', array(
							'scripts' => array(
								'themes/grid-light',
							),
							'options' => array(
								'title' => array(
									'text' => '',
								),
								'chart' => array(
									'plotBackgroundColor' => null,
									'plotBorderWidth' => null,
									'plotShadow' => false,									
									'height' => 250, 
									'width' => 250,
									'spacingBottom' => 15,
									'spacingTop' => 10,
									'spacingLeft' => 10,
									'spacingRight' => 10,
								  ),
								'plotOptions'=> array( 'pie'=>array('shadow'=>false,'center'=>array(['50%','50%']))),
								'tooltip'=>array(
									'headerFormat'=> '',
									'pointFormat'=> '{point.name}: <strong>{point.y}</strong>',
									'backgroundColor'=> 'rgba(0, 0, 0, 0.85)',
									'borderColor'=>  '#000',
									'borderRadius'=>  5,
									'style'=>array( 'color'=> '#e0e0e0','fontSize'=> '13px','padding'=> '6px 8px'),
								),
								
								'credits' => array('enabled' => false),
								'series' => array(				
									array(
										'type' => 'pie',
										'name' => 'Clicks',
										'data' => array(
											array(
												'name' => 'Clicks de lectores unicos',
												'y' => round(!empty($model->clicks_unique)?$model->clicks_unique:0),
												'color'=> '#81ad38',
												'dataPer'=> round(!empty($model->clicks_unique)&& !empty($model->reads_val)?$model->clicks_unique*100/($model->reads_val):0),
												'dataLabels'=>  array('enabled'=>false),
											),
											array(
												'name' => 'No hicieron click',
												'y' => round(!empty($model->reads_val)&&!empty($model->clicks_unique)?$model->reads_val-$model->clicks_unique:0),
												'dataPer'=> round(!empty($model->reads_val)&& !empty($model->clicks_unique)?$model->reads_val-$model->clicks_unique*100/($model->reads_val):0),
												'color'=>'#9988b2',
												'dataLabels'=>  array('enabled'=> false),
											),
										),
										'innerSize' => '55%', 
									),
								),
							)
						));
					?>
				 
						<div class="center_per"style="margin-top:-30px; margin-left:30px;"><?php echo round(!empty($model->clicks_unique)&& !empty($model->reads_val)?$model->clicks_unique*100/($model->reads_val):0);?><span class="per">%</span></div>
						
					 
					<!--
								<div class="data">									
									<span><strong class="icon-click ic" style="color: #81ad38"></strong> Usuarios que accedieron a la oferta: </span>
									<strong><?php echo (!empty($model->clicks_unique)?$model->clicks_unique:'0'); ?></strong>
								</div>
								<div class="data">
									<span><strong class="icon-click ic" style="color: #666"></strong> Cantidad de veces que se accedió a la oferta: </span>
									 <strong style="color: #666"><?php echo round(!empty($model->clicks_total)?$model->clicks_total:'0'); ?></strong>
								</div>-->		
					<?php }
					else { ?>
							<div class="box-header"style="	margin-bottom:-10px;">
							  <h3 class="box-title">Cuantos accedieron a la oferta</h3>
							</div>
							<div class="box-body">
							    <img src="<?php echo Yii::app()->request->baseUrl; ?>/img/emptypie.png" class="img-empty" title="No se puede cargar el gráfico" style="margin-top: -20px;"height="230px;" data-toggle="tooltip" >
								   <!--No se puede cargar el gráfico-->
							</div>
							<!--
								<br />
									<div class="data">
										
										<span><strong class="icon-click ic" style="color: #81ad38"></strong> Usuarios que accedieron a la oferta: </span>
										<strong>No hay datos</strong>
									</div>
									<div class="data">
											<span><strong class="icon-click ic" style="color: #666"></strong> Cantidad de veces que se accedió a la oferta: </span>
										 
										<strong style="color: #666">No hay datos</strong>
									</div>-->
						
					<?php } ?>
					</div> 
				</div>		
				<div class="col-md-1"></div>	
				<div class="col-md-3 clicks_links">
					<div class="box box-solid" style="box-shadow:none">						
					<?php if(!empty($model->clicks_data)){
					 ?>	<div class="box-header">
							  <h3 class="box-title">Detalles de links</h3>
							</div>
					<?php	$links=json_decode($model->clicks_data,1); 
					 
						$arreglo_links=[];
						$i=0;
						
						foreach($links as $key => $val){
							if(!empty($val['link']) && !empty($val['clicks'])){
								array_push($arreglo_links, array('name'=> $val['link'],'y'=> $val['clicks'], 'color'=> $arreglo_colores[$i], 'dataLabels'=> array( 'enabled'=> false) ));	
								$i++;
							}
						}
						 
						$this->widget('ext.highcharts.HighchartsWidget', array(
							'scripts' => array(
								'themes/grid-light',
							),
							'options' => array(
								'title' => array(
									'text' => '',
								),
								'chart' => array(
									'plotBackgroundColor' => null,
									'plotBorderWidth' => null,
									'plotShadow' => false,
									'height' => 250, 
									'width' => 250,									
									'spacingBottom' => 15,
									'spacingTop' => 10,
									'spacingLeft' => 10,
									'spacingRight' => 10,

								  ),
								'plotOptions'=> array( 'pie'=>array('shadow'=>false,'center'=>array(['50%','50%']))),
								'tooltip'=>array(
									'headerFormat'=> '',
									'pointFormat'=> '{point.name}: <strong>{point.y}</strong>',
									'backgroundColor'=> 'rgba(0, 0, 0, 0.85)',
									'borderColor'=>  '#000',
									'borderRadius'=>  5,
									'style'=>array( 'color'=> '#e0e0e0','fontSize'=> '13px','padding'=> '6px 8px'),
								),
								
								'credits' => array('enabled' => false),
								'series' => array(				
									array(
										'type' => 'pie',
										'name' => 'Links',
										'data' => $arreglo_links,
										'size' => '100%',
										'innerSize' => '55%'
									),
								),
							)
						));
					?>
								
					<?php }
					else { ?> 
							<div class="box-header">
							  <h3 class="box-title">Detalles de links</h3>
							</div>
							<div class="box-body">
							  <img src="<?php echo Yii::app()->request->baseUrl; ?>/img/emptypie.png" class="img-empty" title="No se puede cargar el gráfico" style="margin-top: -10px;"height="230px;" data-toggle="tooltip" >
								   <!--No se puede cargar el gráfico--> 
							</div>
					<?php } ?>
					</div> 
				</div>		
	 
			</div>
		
		<?php if(!empty($model->clicks_unique)&& !empty($model->clicks_total)){?>
			<div class="row col-md-12" style="margin-top:10px; Padding-left:50px;">
				<div class="data">
					<span><strong class="icon-click ic" style="color: #81ad38"></strong> Accesos: </span>
					<strong><?php echo (!empty($model->clicks_unique)?$model->clicks_unique:'0'); ?></strong>
				</div>
				<div class="data">
					<span><strong class="icon-click ic" style="color: #666"></strong> Accesos totales: </span>
					<strong style="color: #666"><?php echo round(!empty($model->clicks_total)?$model->clicks_total:'0'); ?></strong>
				</div>				
			</div>
		<?php } ?>	 
		
		<?php } ?>			 
		<?php if(!empty($model->clicks_data)){?>
			<div class="row col-md-8" style="margin-top:10px; padding:20px;">
				<div class="box box-solid">
					<div class="box-header">
						<h3 class="box-title">Lista de enlaces del Correo</h3>
					</div>			
					<table class="table table-striped">
						<tbody>
							<tr>
							  <th style="width: 10px">#</th>
							  <th>Link</th>
							  <th style="width: 40px">Contactos</th>
							  <th style="width: 40px">Accesos</th>
							  <th style="width: 160px"></th>
							</tr>
							<?php 
							$links=json_decode($model->clicks_data,1); 
							/* Ordenar Links: */
							$sortArray = array(); 
							foreach($links as $link){ 
								foreach($link as $key=>$value){ 
									if(!isset($sortArray[$key])){ 
										$sortArray[$key] = array(); 
									} 
									$sortArray[$key][] = $value; 
								} 
							} 
							$orderby = "clicks_totales"; 
							array_multisort($sortArray[$orderby],SORT_DESC,$links);
							$i=0;
									foreach($links as $key => $val){ 
										if(!empty($val['link'])){
									?>
											<tr>
											  <td><?php echo $i+1;?>.</td>
											  <td><?php echo '<a href="'.$val['link'].'" target="_blank">'.$val['url'].'</a>';?></td>
											  <td style="width: 30px"><span class="badge bg-red"><?php echo $val['clicks'];?></span></td>
											  <td style="width: 30px"><span class="badge bg-red text-center"><?php echo $val['clicks_totales'];?></span></td>
											  <?php if(!Yii::app()->user->checkAccess('viewcompletestats')){ ?>
											  <?php if($val['clicks_totales']>0){?>
												<td style="width: 160px"><button type="button" id="<?php echo $val['id'];?>" class="get-link-detail btn-xs" title="Ver detalles"><small>Quien accedió</small></button></td>
											  <?php } ?>
											  <?php } ?>
											</tr>				
											<?php $i++;
											}
										} ?>
					  </tbody>
					</table>			
				</div>
			</div>
		<?php }  ?>
		
		<div class="row col-md-4 link-details" style="margin-top:10px; padding:20px; display:none;">
			<div class="box box-solid">
				<div class="box-header">
					<h3 class="box-title">Lista de Accesos</h3>
				</div>			
				<div class="link-table">	 
				 </div>
					 		
			</div>
		</div>
		
	</div>
	
	<?php if(empty($model->clicks_data) && empty($model->clicks_unique) && empty($model->clicks_total)){?>
		<div class="row"> 
			<div class="col-md-10" style="margin-top:20px; padding:10px;"> 	
				<div class="box-header text-center">
						<h3 class="box-title">No hay datos.</h3>
				</div>			
			</div>
		</div>
	<?php } ?>	
	
</div>
<script>
$( ".get-link-detail" ).click(function(event) {"id=" + $(this).attr("href", "id"), 
		event.preventDefault();
		var id = $(this).attr("href", "id");
		var link_id = (id.context.id);
		var request = $.ajax({
			method:"post",
			<?php if(empty($esIframe) && !$esIframe){?>
			url: homeurl+'/estadisticas-email/get-link-details/'+<?php echo $model->id;?>+'/'+link_id,
			<?php }else{?>
			url: homeurl+'/iframe/estadisticas-email/get-link-details/'+<?php echo $model->id;?>+'/'+link_id,
			<?php } ?>
			data: {id_link: link_id}
		}).done(function(msg) {
			$('.link-details').css('display','block');
			$("div.link-table").html(msg);
		});
	});	
</script>