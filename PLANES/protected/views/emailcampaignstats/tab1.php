
<div class="tab-pane active" id="tab_1">
				<div class="row">
					<div class="pies col-md-12"> 
					<?php if(!empty($model->sent_mails)&&!empty($model->delivered)){?>
						<div class="col-md-4 delivered">	
							<div class="box-header text-center">
								  <h3 class="box-title text-center" style="text-align:center;">Entrega de Correos</h3>
							</div>						
									<?php		
	$this->widget('ext.highcharts.HighchartsWidget', array(
		'scripts' => array(
			#'modules/exporting',
			'themes/grid-light',
		),
		'options' => array(
		 
			'title' => array(
				'text' => '',
			),
			'chart' => array(
				'plotBackgroundColor' => 'transparent',
				'plotBorderWidth' => null,
				'plotShadow' => false,
				'height' => 300, 
			  ),
			'plotOptions'=> array( 'pie'=>array('shadow'=>false,'center'=>array(['50%','50%']))),
			'tooltip'=>array(
				'headerFormat'=> '',
				'pointFormat'=> '{point.name}<br /><strong>{point.y}</strong> ({point.dataPer}%)',
				'backgroundColor'=> 'rgba(0, 0, 0, 0.85)',
				'borderColor'=>  '#000',
				'borderRadius'=>  5,
				'style'=>array( 'color'=> '#e0e0e0','fontSize'=> '13px','padding'=> '6px 8px'),
			),
			
			'credits' => array('enabled' => false),
					
			'series' => array(			
				array(
					'type' => 'pie',
					'name' => 'Delivery',
					'data' => array(
						array(
							'name' => 'Ofertas Entregadas',
							'y' => round(!empty($model->delivered)?$model->delivered:0),
							'color'=> '#edc240',
							#'color' => 'js:Highcharts.getOptions().colors[0]', // 1's color
							'dataPer'=> round(!empty($model->sent_mails+$model->not_send)?$model->delivered*100/($model->sent_mails+$model->not_send):0),
							'dataLabels'=>  array('enabled'=>false),
						),
						array(
							'name' => 'No enviados',
							'y' => round(!empty($model->not_send)?$model->not_send:0),
							'dataPer'=> round(!empty($model->sent_mails+$model->not_send)?$model->not_send*100/($model->sent_mails+$model->not_send):0),
							'color'=>'#cb744a',
							'dataLabels'=>  array('enabled'=> false),
						),
						array(
							'name' => 'Rechazados',
							'y' => round($model->failed+$model->bounced_soft+$model->bounced_hard+$model->bounced_spam+$model->bounced_invalid),
							'dataPer' =>  round(!empty($model->sent_mails+$model->not_send)?($model->failed+$model->bounced_soft+$model->bounced_hard+$model->bounced_spam+$model->bounced_invalid)*100/($model->sent_mails+$model->not_send):0),
							'color' =>  '#cb4b4a',
							'dataLabels' =>  array('enabled'=> false),
						),
					),
					#'center' => array(20, 20),
					#'size' => 300, 
					'innerSize' => '55%'
				),
			),
		)
	));
?>
								<div class="center_per"><?php echo  round(!empty($model->sent_mails+$model->not_send)?$model->delivered*100/($model->sent_mails+$model->not_send):0);?><span class="per">%</span></div>
								
							<!--	<div class="data">
									<span><strong class="icon-users ic" style="color: #48B600;"></strong> Suscriptores: </span>
									<strong><?php echo $model->subscribers; ?></strong>
								</div>-->
								
								<div class="data">
									<span><strong class="icon-delivered ic" style="color: #0096CA;"></strong> Enviados: </span>
									<strong style="color: #555"><?php echo number_format($model->sent_mails,0,",","."); ?></strong> <!--<b style="color: #6997AD">(<?php echo round(!empty($model->sent_mails+$model->not_send)?($model->sent_mails)*100/($model->sent_mails+$model->not_send):'0');?>%)</b>-->
								</div>
								<div class="data">
									<span><strong class="icon-mail ic" style="color: #edc240"></strong> Entregados: </span>
									<strong style="color: #555"><?php echo number_format($model->delivered,0,",","."); ?></strong> <b style="color: #6997AD">(<?php echo round(!empty($model->sent_mails+$model->not_send)?($model->delivered)*100/($model->sent_mails+$model->not_send):'0');?>%)</b>
								</div>
								<div class="data">
									<span><strong class="icon-spam ic" style="color: #cb4b4a"></strong> Rechazados: </span>
									<strong style="color: #555"><?php echo number_format($model->failed+$model->bounced_soft+$model->bounced_hard+$model->bounced_spam+$model->bounced_invalid,0,",","."); ?></strong>
									<strong style="color: #cb4b4a">(<?php echo round(!empty($model->sent_mails+$model->not_send)?($model->failed+$model->bounced_soft+$model->bounced_hard+$model->bounced_spam+$model->bounced_invalid)*100/($model->sent_mails+$model->not_send):'0'); ?>%)</strong>
								</div> 
								<div class="data">
									<span><strong class="icon-unsus ic" style="color: #cb4b4a;"></strong> Desuscriptos: </span>
									<strong style="color: #555"><?php echo !empty($model->unsus)? number_format($model->unsus,0,",","."):'0'; ?></strong>
								</div>
						</div> 
					<?php }
						else { ?>
							<div class="col-md-3">
							  <div class="box box-solid" style="box-shadow:none">
								<div class="box-header">
								  <h3 class="box-title">Entrega de Correos</h3>
								</div>
								<div class="box-body">
								    <img src="<?php echo Yii::app()->request->baseUrl; ?>/img/emptypie.png" class="img-empty" title="No se puede cargar el gráfico" style="margin-top: 39px;" height="<?php echo $esIframe?'220px;':'180px;';?>" data-toggle="tooltip" >
								  <!--No se puede cargar el gráfico-->
									<br /><br /> 
									</div>
										<div class="data">
										<span><strong class="icon-delivered ic" style="color: #0096CA;"></strong> Enviados: </span>
										<strong style="color: #555">No hay datos.</strong> 
									</div>
									<div class="data">
										<span><strong class="icon-mail ic" style="color: #edc240"></strong> Entregados: </span>
										<strong style="color: #555">No hay datos.</strong>
									</div>
									<div class="data">
										<span><strong class="icon-spam ic" style="color: #cb4b4a"></strong> Rechazados: </span>
										<strong style="color: #555">No hay datos.</strong>
										
									</div> 
									<div class="data">
										<span><strong class="icon-unsus ic" style="color: #cb4b4a;"></strong> Desuscriptos: </span>
										<strong style="color: #555">No hay datos.</strong>
									</div>
								 
							  </div>
							</div>
					<?php } ?>
						
						<?php if(!empty($model->sent_mails)&&!empty($model->sent_mails)){?>
						<div class="col-md-4 openrate">
							<div class="box-header text-center">
								  <h3 class="box-title text-center" style="text-align:center;">Aperturas del Correo</h3>
							</div>
							
								<?php				
	$this->widget('ext.highcharts.HighchartsWidget', array(
		'scripts' => array(
	#		'modules/exporting',
			'themes/grid-light',
		),
		'options' => array(
			'title' => array(
				'text' => '',
			),
			'chart' => array(
				'plotBackgroundColor' => 'transparent',
				'plotBorderWidth' => null,
				'plotShadow' => false,
				'height' => 300, 
			  ),
			'plotOptions'=> array( 'pie'=>array('shadow'=>false,'center'=>array(['50%','50%']))),
			'tooltip'=>array(
				'headerFormat'=> '',
				'pointFormat'=> '{point.name}<br /><strong>{point.y}</strong> ({point.dataPer}%)',
				'backgroundColor'=> 'rgba(0, 0, 0, 0.85)',
				'borderColor'=>  '#000',
				'borderRadius'=>  5,
				'style'=>array( 'color'=> '#e0e0e0','fontSize'=> '13px','padding'=> '6px 8px'),
			),
			
			'credits' => array('enabled' => false),
			'series' => array(				
				array(
					'type' => 'pie',
					'name' => 'Delivery',
										'data' => array(
						array(
							'name' => 'Abiertos',
							'y' => round(!empty($model->reads_val)?$model->reads_val:0),
							'color'=> '#3e9ca8',
							#'color' => 'js:Highcharts.getOptions().colors[0]', // 1's color
							'dataPer'=> round(!empty($model->sent_mails)&& !empty($model->reads_val)?$model->reads_val*100/($model->sent_mails):0),
							'dataLabels'=>  array('enabled'=>false),
						),
						array(
							'name' => 'No abiertos',
							'y' => round(!empty($model->not_reads)?$model->not_reads:0),
							'dataPer'=> round(!empty($model->sent_mails)&& !empty($model->not_reads)?$model->not_reads*100/($model->sent_mails):0),
							'color'=>'#ee7e66',
							'dataLabels'=>  array('enabled'=> false),
						),
					),
					#'center' => array(20, 20),
					#'size' => 300, 
					'innerSize' => '55%'
				),
			),
		)
	));
?>								<div class="center_per"><?php echo round(!empty($model->sent_mails)&& !empty($model->reads_val)?$model->reads_val*100/($model->sent_mails):0);?><span class="per">%</span></div>
								<div class="data">
									<span><strong class="icon-mail2 ic" style="color: #3e9ca8"></strong> Abiertos: </span>
									<strong style="color: #555"><?php echo !empty($model->reads_val)?number_format($model->reads_val,0,",","."):'0'; ?></strong>
									<strong style="color: #6997AD">(<?php echo round(!empty($model->delivered)?$model->reads_val*100/$model->delivered:'0'); ?>%)</strong>
								</div>

								<div class="data">
									<span><strong class="icon-mail2 ic" style="color: #ee7e66"></strong> No abiertos: </span>
									<strong style="color: #555"><?php echo !empty($model->not_reads)?number_format($model->not_reads,0,",","."):'0'; ?></strong>
									<strong style="color: #ee7e66">(<?php echo round(!empty($model->delivered)?$model->not_reads*100/$model->delivered:'0'); ?>%)</strong>
								</div>
							<?php if(Yii::app()->user->checkAccess('viewcompletestats')){ ?>
								<div class="data">
									<span>Unicas/Totales:</span>
									<strong style="color: #555"><?php echo !empty($model->reads_val)?number_format($model->reads_val,0,",","."):'0'; ?> / <?php echo !empty($model->reads_total)?number_format($model->reads_total,0,",","."):'0'; ?></strong>
									
								</div>
								<div class="data">
									<span>Ratio:</span>
									<strong style="color: #555"><?php echo !empty($model->reads_total)?number_format($model->reads_val/$model->reads_total,2,",","."):'0'; ?></strong>
								</div>
							<?php } ?>
						</div> 
						<?php }
						else { ?>
							<div class="col-md-3">
							  <div class="box box-solid" style="box-shadow:none">
								<div class="box-header">
								  <h3 class="box-title">Aperturas del Correo</h3>
								</div>
								<div class="box-body">
								   <img src="<?php echo Yii::app()->request->baseUrl; ?>/img/emptypie.png" class="img-empty" title="No se puede cargar el gráfico" style="margin-top: 39px;"height="<?php echo $esIframe?'220px;':'180px;';?>"  data-toggle="tooltip" >
								  <!--No se puede cargar el gráfico-->
								</div>
								  <br />
								  <div class="data">
										<span><strong class="icon-mail2 ic" style="color: #3e9ca8"></strong> Abiertos: </span>
										<strong style="color: #6997AD">No hay datos</strong>
									</div>

									<div class="data">
										<span><strong class="icon-mail2 ic" style="color: #ee7e66"></strong> No abiertos: </span>
										<strong style="color: #ee7e66">No hay datos</strong>
									</div>
							
								 
							  </div>
							</div>
					<?php } ?>
						
						<?php if(!empty($model->reads_val)&&!empty($model->clicks_unique)){?>
						<div class="col-md-4 click">
							<div class="box-header text-center">
								  <h3 class="box-title text-center" style="text-align:center;">Accesos a la Oferta</h3>
							</div>
								
								<?php				
	$this->widget('ext.highcharts.HighchartsWidget', array(
		'scripts' => array(
		#	'modules/exporting',
			'themes/grid-light',
		),
		'options' => array(
			 'title' => array(
				'text' => '',
			),
			'chart' => array(
				'plotBackgroundColor' => 'transparent',
				'plotBorderWidth' => null,
				'plotShadow' => false,
				'height' => 300, 
			  ),
			'plotOptions'=> array( 'pie'=>array('shadow'=>false,'center'=>array(['50%','50%']))),
			'tooltip'=>array(
				'headerFormat'=> '',
				'pointFormat'=> '{point.name}<br /><strong>{point.y}</strong> ({point.dataPer}%)',
				'backgroundColor'=> 'rgba(0, 0, 0, 0.85)',
				'borderColor'=>  '#000',
				'borderRadius'=>  5,
				'style'=>array( 'color'=> '#e0e0e0','fontSize'=> '13px','padding'=> '6px 8px'),
			),
			
			'credits' => array('enabled' => false),
			'series' => array(			
				array(
					'type' => 'pie',
					'name' => 'Clicks (CTRO)',
					/*'plotOptions'=> array( 'pie'=>array('shadow'=>false,'center'=>array(['45%','50%']))),*/
					'data' => array(
						array(
							'name' => 'Clicks de lectores unicos',
							'y' => round(!empty($model->clicks_unique)?$model->clicks_unique:0),
							'color'=> '#81ad38',
							'dataPer'=> round(!empty($model->clicks_unique)&& !empty($model->reads_val)?$model->clicks_unique*100/($model->reads_val):0),
							'dataLabels'=>  array('enabled'=>false),
						),
						array(
							'name' => 'No hicieron click',
							'y' => round(!empty($model->reads_val)&&!empty($model->clicks_unique)?$model->reads_val-$model->clicks_unique:0),
							'dataPer'=> round(!empty($model->reads_val)&& !empty($model->clicks_unique)?($model->reads_val-$model->clicks_unique)*100/($model->reads_val):0),
							'color'=>'#9988b2',
							'dataLabels'=>  array('enabled'=> false),
						),
					),
					#'center' => array(20, 20),
					#'size' => 300, 
					'size' => '100%',
					'innerSize' => '55%'
				),
			),
			
		)
	));
?>
								<div class="center_per"><?php echo  round(!empty($model->clicks_unique)&& !empty($model->reads_val)?$model->clicks_unique*100/$model->reads_val:0);?><span class="per">%</span></div>
								
								<div class="data">
									<!--<span style="font-size:10px;"><strong class="icon-click ic" style="color: #81ad38"></strong> Usuarios que accedieron a la oferta: </span>-->
									<span><strong class="icon-click ic" style="color: #81ad38"></strong> Usuarios que accedieron a la oferta: </span>
									<strong><?php echo !empty($model->clicks_unique)?number_format($model->clicks_unique,0,",","."):'0'; ?></strong>
								</div>
								<div class="data">
									<!--<span style="font-size:10px;"><strong class="icon-click ic" style="color: #666"></strong> Cantidad de veces que se accedió a la oferta: </span>-->
							 
										<span><strong class="icon-click ic" style="color: #666"></strong> Cantidad de veces que se accedió a la oferta: </span>
									 
									<strong style="color: #666"><?php echo round(!empty($model->clicks_total)?number_format($model->clicks_total,0,",","."):'0'); ?></strong>
								</div>
						</div> 
						<?php }
						else { ?>
							<div class="col-md-3">
							  <div class="box box-solid" style="box-shadow:none">
								<div class="box-header text-center">
								  <h3 class="box-title text-center" style="text-align:center;">Accesos a la Oferta</h3>
								</div>
								<div class="box-body">
								  <img src="<?php echo Yii::app()->request->baseUrl; ?>/img/emptypie.png" class="img-empty" title="No se puede cargar el gráfico" data-toggle="tooltip" style="margin-top: 39px;"height="<?php echo $esIframe?'220px;':'180px;';?>">
								   <!--No se puede cargar el gráfico-->
								</div>
								<br />
									<div class="data">
										
										<span><strong class="icon-click ic" style="color: #81ad38"></strong> Usuarios que accedieron a la oferta: </span>
										<strong>No hay datos</strong>
									</div>
									<div class="data">
											<span><strong class="icon-click ic" style="color: #666"></strong> Cantidad de veces que se accedió a la oferta: </span>
										 
										<strong style="color: #666">No hay datos</strong>
									</div>
							  </div>
							</div>
					<?php } ?>
						
					</div>
					<?php if(Yii::app()->user->checkAccess('viewcompletestats')){ ?>
					<br />
				 
					<div class="col-md-10" style="margin-top:50px; margin-left:10px;"> 
						<div class="box box-solid" style="box-shadow:none">  
							<div class="col-md-4">
								<div class="data">
									<span><strong class="icon-referred ic" style="color: #6ab550;"></strong> Referidos: </span>
									<strong style="color: #555"><?php echo round(!empty($model->refered)?$model->refered:'0'); ?></strong>
								</div>
								<div class="data">
									<span><strong class="icon-mail2 ic" style="color: #6ab550;"></strong> Lecturas-Referidos: </span>
									<strong style="color: #555"><?php echo round(!empty($model->refered_reads)?$model->refered_reads:'0'); ?> (<?php echo round(!empty($model->reads_val)?$model->refered_reads*100/$model->reads_val:'0'); ?>%)</strong>
								</div>
								<div class="data">
									<span><strong class="icon-mail2 ic" style="color: #6a9dcf;"></strong> Total de lecturas: </span>
									<strong style="color: #555"><?php echo !empty($model->reads_total)?number_format($model->reads_total,0,",","."):'0'; ?></strong>
								</div>
							</div> 
							
							<div class="col-md-4">
							<!--	<div class="data">
									<span><strong class="icon-uadd ic" style="color: #6ab550;"></strong> Nuevos suscriptores: </span>
									<strong style="color: #555"><?php echo round(!empty($model->new_suscribers)?$model->new_suscribers:'0'); ?></strong>
								</div>-->
								
							</div>
						 <div class="col-md-4"></div>  
						</div>
					</div>
					
					<?php } ?>
					
					
					
					
					
					
					
					
					
					</div>
				</div>
					<!-- /.tab-pane -->