<?php
/* @var $this PaymentmethodController */
/* @var $model Paymentmethod */
/* @var $form CActiveForm */
?>

<div class="box-body">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'paymentmethod-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>
<span class="right_f">
	<?php echo $form->checkBox($model,'enabled',array('class'=>'lcs_check enabled')); ?>
</span>

<div class="col-md-12 col-data">

	<div class="form-group col-md-3 required unique">
	
		<?php echo $form->labelEx($model,'rules'); ?>
		<?php echo $form->textField($model,'rules',array('size'=>45,'maxlength'=>45,'class'=>'form-control')); ?>
		<?php echo $form->error($model,'rules'); ?>
	
	</div>

	<div class="form-group col-md-3 required">
	
		<?php echo $form->labelEx($model,'id_channel'); ?>
		<?php echo $form->dropDownList($model, 'id_channel',CHtml::listData(Channels::model()->findAll(), 'id', 'title'),array('empty' => 'Seleccione...','class'=>'form-control channel selectobject'));?>
		<?php echo $form->error($model,'id_channel'); ?>
	
	</div>

	<div class="form-group col-md-3 required">
		<?php echo $form->labelEx($model,'id_subchannel'); ?>
		<?php echo $form->dropDownList($model, 'id_subchannel',CHtml::listData(Subchannels::model()->findAll(), 'id', 'title'),array('empty' => 'Seleccione...','class'=>'form-control subchannel selectobject'));?>
		<?php echo $form->error($model,'id_subchannel'); ?>
	</div>
	
	<div class="form-group col-md-3 required">
		<?php echo $form->labelEx($model,'id_product'); ?>
		<?php echo $form->dropDownList($model, 'id_product',CHtml::listData(Products::model()->findAll(), 'id', 'title'),array('empty' => 'Seleccione...','class'=>'form-control selectobject'));?>
		<?php echo $form->error($model,'id_product'); ?>
	</div>
	<div class="form-group col-md-12">
		<div class="col-md-4 required">
			<?php echo $form->labelEx($model,'id_bank'); ?>
			<?php echo $form->dropDownList($model, 'id_bank',CHtml::listData(Entity::model()->findAll(), 'id', 'title'),array('empty' => 'Seleccione...','class'=>'form-control selectobject bank'));?>
			<?php echo $form->error($model,'id_bank'); ?>
		</div>

		<div class="col-md-4 required">
			<?php echo $form->labelEx($model,'id_credit_card'); ?>
			<?php echo $form->dropDownList($model, 'id_credit_card',CHtml::listData(Creditcards::model()->findAll(), 'id', 'name'),array('empty' => 'Seleccione...','class'=>'form-control selectobject'));?>
			<?php echo $form->error($model,'id_credit_card'); ?>
		</div>

		<div class="col-md-4 required">
			<?php echo $form->labelEx($model,'id_provider'); ?>
			<?php echo $form->dropDownList($model, 'id_provider',CHtml::listData(Providers::model()->findAll(), 'id', 'title'),array('empty' => 'Seleccione...','class'=>'form-control selectobject'));?>
			<?php echo $form->error($model,'id_provider'); ?>
		</div>
	</div>

	<div class="form-group col-md-3 required">
		<?php echo $form->labelEx($model,'date_from'); ?>
		<div class="input-group">
			<?php echo $form->textField($model,'date_from',array('class'=>'form-control datepicker','data-inputmask'=>'\'alias\': \'dd/mm/yyyy\'','data-mask'=>'data-mask')); ?>
			<div class="input-group-addon pointer"><i class="fa fa-calendar"></i></div>
		</div>
		<?php echo $form->error($model,'date_from'); ?>
	</div>
	<div class="form-group col-md-3 required">
		<?php echo $form->labelEx($model,'date_to'); ?>
		<div class="input-group">
		<?php echo $form->textField($model,'date_to',array('class'=>'form-control datepicker','data-inputmask'=>'\'alias\': \'dd/mm/yyyy\'','data-mask'=>'data-mask')); ?>
			<div class="input-group-addon pointer"><i class="fa fa-calendar"></i></div>
		</div>
		<?php echo $form->error($model,'date_to'); ?>
	</div>
	<div class="box-footer">
		<?php
			$title = (Yii::app()->controller->action->id!='view') ? 'Volver sin guardar cambios' : 'Volver';
			echo '<a href="'.Yii::app()->request->baseUrl.'/'.strtolower(Yii::app()->controller->id).'" class="btn btn-secundary cancel" data-toggle="tooltip" title="'.$title.'" >Cerrar</a>';
			if(Yii::app()->controller->action->id!='view'){
				echo CHtml::submitButton('Guardar',array('class'=>'btn btn-primary save','data-toggle'=>'tooltip','title'=>'Guardar cambios'));
			}
		?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
</div>
<?php
	Yii::app()->clientScript->registerCssFile(Yii::app()->params['cdnUrl'].'/plugins/lcswich/lc_switch-tts.css');
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/lcswich/lc_switch.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/lcswich/lc_switch-tts.js',CClientScript::POS_END);

	Yii::app()->clientScript->registerCssFile(Yii::app()->params['cdnUrl'].'/plugins/datepicker/datepicker3.css');
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/datepicker/bootstrap-datepicker.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/input-mask/jquery.inputmask.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/input-mask/jquery.inputmask.date.extensions.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/datepicker/datepicker-tts.js',CClientScript::POS_END);

	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/timepicker/bootstrap-timepicker.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/input-mask/jquery.inputmask.bundle.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerCssFile(Yii::app()->params['cdnUrl'].'/plugins/timepicker/bootstrap-timepicker.css');
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/timepicker/timepicker-tts.js',CClientScript::POS_END);

	Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/plugins/select2/select2.full.js',CClientScript::POS_END);

	Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl.'/js/plugins/select2/select2.css'); 

	echo '<script>viewPage = "'.Yii::app()->controller->action->id.'";</script>';
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/js/form-validators.js',CClientScript::POS_END) ?>
<script>

$( document ).ready(function() {

	<?php if(Yii::app()->controller->action->id=='view'){ echo "$('form').find('input, textarea, button, checkbox, select').attr('disabled','disabled');"; } ?>
	$('[data-mask]').inputmask('dd/mm/yyyy', {'placeholder': 'dd/mm/aaaa'});
	$('.fa-calendar').datepicker().on('changeDate', function(e) {
		$(this).parent().prev().val(e.format('dd/mm/yyyy'));
	    $(this).datepicker('hide');
    });

	$('.select2').select2();

	$('.datepicker').datepicker({
      autoclose: true,
	  //format: 'yyyy-mm-dd',
	  format: 'dd/mm/yyyy',
	  firstDay: 1
    });
    
	channelsFilter();
	banksFilter();
	
	if(viewPage=='update'){
		$('.unique input').prop('disabled',true);
		$(".subchannel").prop('disabled',false);
		$(".bank").prop('disabled',false);
	}
});


function banksFilter(){
	$(".bank").prop('disabled',true);
	$(".channel").change(function(){
		$(".bank").prop('disabled',true); 
		$(".bank option:eq(0)").prop('selected', true);	
	});
    $(".subchannel").change(function(){
    	var id_subchannel = $(this).val();
    	var id_channel = $(".channel").val();
    	var id_bank = $(".bank").val();
    	$(".bank").prop('disabled',true);
    	$(".bank").addClass('cargando');
    	$(".bank").next().find("span.select2-selection").addClass('cargando');
    	
    	if(id_subchannel!=''){
    		$(".bank").load(homeurl+"/filterentities/"+id_subchannel,function(){
    			$(".bank").prop('disabled',false);
    			$(".bank").removeClass('cargando');
    			$(".bank").next().find("span.select2-selection").removeClass('cargando');
    			if($(".bank").find("option").length>10){
					if(!$('.bank').prop('multiple')){
						//$(".bank").prepend('<option value="">Seleccione...</option>');
						$(".bank option:eq(0)").prop('selected', true);	
					}																		
					$(".bank").addClass(' select2');
					$(".bank").css('width','100%');
					$(".select2").select2().on('select2:open', function(e) {
						$(".select2-dropdown").css('background-color',$(this).css('background-color'));
					});
				}else{
					if ($(".bank").data('select2')){
						$(".bank").select2("destroy");
					}				
					if($('.bank').prop('multiple')){	
						$(".bank").addClass(' select2');
						$(".bank").css('width','100%');
						$(".select2").select2().on('select2:open', function(e) {
							$(".select2-dropdown").css('background-color',$(this).css('background-color'));
						});					
					}else{
						$(".bank").prepend('<option value="">Seleccione...</option>');
						$(".bank option:eq(0)").prop('selected', true);							
					}
				}   			
    		});
    	}else{
    		$(".bank").removeClass('cargando');
			if ($(".bank").data('select2')){
				$(".bank").select2("destroy");
			}					
    		$(".bank").html('<option value="">Seleccione...</option>');
    		$(".bank").prop('disabled',true);
    	}
	});
	
}


</script>

