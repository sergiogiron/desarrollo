<?php
/* @var $this PaymentmethodController */
/* @var $data Paymentmethod */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_product')); ?>:</b>
	<?php echo CHtml::encode($data->id_product); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_bank')); ?>:</b>
	<?php echo CHtml::encode($data->id_bank); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_credit_card')); ?>:</b>
	<?php echo CHtml::encode($data->id_credit_card); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_provider')); ?>:</b>
	<?php echo CHtml::encode($data->id_provider); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('rules')); ?>:</b>
	<?php echo CHtml::encode($data->rules); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('date_from')); ?>:</b>
	<?php echo CHtml::encode($data->date_from); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('date_to')); ?>:</b>
	<?php echo CHtml::encode($data->date_to); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
	<?php echo CHtml::encode($data->status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_channel')); ?>:</b>
	<?php echo CHtml::encode($data->id_channel); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_subchannel')); ?>:</b>
	<?php echo CHtml::encode($data->id_subchannel); ?>
	<br />

	*/ ?>

</div>