<?php
/* @var $this PaymentmethodController */
/* @var $model Paymentmethod */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>
<div class="col-md-12" style="clear: both">
<div class="row">
			
			
			<div class="col-md-3">
		<div class="row row_space">
		<?php echo $form->labelEx($model,'id_product'); ?>
		<?php echo $form->dropDownList($model, 'id_product',CHtml::listData(Products::model()->findAll(), 'id', 'title'),array('empty' => 'Todos','class'=>'form-control'));?>
		</div>
	</div>
			
			<div class="col-md-3">
		<div class="row row_space">
		<?php echo $form->labelEx($model,'id_bank'); ?>
		<?php echo $form->dropDownList($model, 'id_bank',CHtml::listData(Entity::model()->findAll(), 'id', 'title'),array('empty' => 'Todos','class'=>'form-control'));?>
		</div>
	</div>
			
			<div class="col-md-3">
		<div class="row row_space">
		<?php echo $form->labelEx($model,'id_credit_card'); ?>
		<?php echo $form->dropDownList($model, 'id_credit_card',CHtml::listData(Creditcards::model()->findAll(), 'id', 'name'),array('empty' => 'Todos','class'=>'form-control'));?>
		</div>
	</div>

	<div class="col-md-3">
		<div class="row row_space">
		<?php echo $form->labelEx($model,'id_provider'); ?>
		<?php echo $form->dropDownList($model, 'id_provider',CHtml::listData(Providers::model()->findAll(), 'id', 'title'),array('empty' => 'Todos','class'=>'form-control'));?>
		</div>
	</div>
			
		
			
			<div class="col-md-3">
				<div class="row row_space">
					<?php echo $form->label($model,'rules'); ?>
					<?php echo $form->textField($model,'rules',array('size'=>45,'maxlength'=>45,'class'=>'form-control')); ?>
				</div>
			</div>

			<div class="col-md-3">
		<div class="row row_space">
		<?php echo $form->labelEx($model,'id_channel'); ?>
		<?php echo $form->dropDownList($model, 'id_channel',CHtml::listData(Channels::model()->findAll(), 'id', 'title'),array('empty' => 'Todos','class'=>'form-control'));?>
		</div>
		</div>

			<div class="col-md-3">
		<div class="row row_space">
		<?php echo $form->labelEx($model,'id_subchannel'); ?>
		<?php echo $form->dropDownList($model, 'id_subchannel',CHtml::listData(Subchannels::model()->findAll(), 'id', 'name'),array('empty' => 'Todos','class'=>'form-control'));?>
		</div>
		</div>
			
			</div>
			<div class="row">
			<div class="col-md-3">
				<div class="row row_space">
					<?php echo $form->label($model,'date_from'); ?><br>
					<?php echo $form->textField($model,'date_from',array('class'=>'form-control datepicker')); ?>
				</div>
			</div>
			
			<div class="col-md-3">
				<div class="row row_space">
					<?php echo $form->label($model,'date_to'); ?><br>
					<?php echo $form->textField($model,'date_to',array('class'=>'form-control datepicker')); ?>
				</div>
			</div>
			
			
		
			

			<div class="col-md-3">
				<div class="row row_space">
					<?php echo $form->label($model,'enabled'); ?>
					<?php echo $form->checkBox($model,'enabled',array('class'=>'checkbox_form')); ?>
				</div>
			</div>

</div>
	<div class="row buttons">
		<?php echo CHtml::submitButton('Buscar',array('class'=>'btn btn-default b_customize')); ?>
	</div>
<br />
<?php $this->endWidget(); ?>

</div>
</div><!-- search-form -->

<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/plugins/datepicker/bootstrap-datepicker.js',CClientScript::POS_END); ?>
<script>
$( document ).ready(function() {	
    $('.datepicker').datepicker({
      autoclose: true, 
	  format: 'yyyy-mm-dd',
	  firstDay: 1
    });
});	
</script>	
