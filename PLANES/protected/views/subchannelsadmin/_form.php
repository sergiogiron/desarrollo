<?php
/* @var $this SubchannelsController */
/* @var $model Subchannels */
/* @var $form CActiveForm */
?>
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'subchannels-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>
<span class="right_f active-button">
	<?php echo $form->checkBox($model,'enabled',array('class'=>'lcs_check enabled')); ?>
	<?php echo $form->error($model,'enabled'); ?>
</span>

<br />
<div class="col-md-4 form-group required  unique">
	<?php
	if(!$model->isNewRecord){ $disable='disabled'; }else{ $disable='enabled'; }
	echo $form->labelEx($model,'id_channel'); ?>
	<?php echo $form->dropDownList($model, 'id_channel',CHtml::listData(Channels::model()->findAll(), 'id', 'title'),array($disable=>$disable,'empty' => 'Seleccione','class'=>'form-control channel'));?>
	<?php echo $form->error($model,'id_channel'); ?>
</div>
<div class="col-md-4 form-group required  unique">
	<?php echo $form->labelEx($model,'title'); ?>
	<?php echo $form->textField($model,'title',array($disable=>$disable,'size'=>50,'maxlength'=>50,'class'=>'form-control letras_se')); ?>
	<?php echo $form->error($model,'title'); ?>
</div>
<div class="col-md-12">
	<div class="row">
	  <!-- Custom Tabs -->
	  <div class="nav-tabs">
		<ul class="nav nav-tabs">
		  <li class="active tab_1"><a href="#tab_1" data-toggle="tab" aria-expanded="true">General</a></li>
		  <li class="tab_2"><a href="#tab_2" data-toggle="tab" aria-expanded="false">Tracking</a></li>
		</ul>
		<div class="tab-content">
			<div class="tab-pane active" id="tab_1">
				<br />
				<div class="form-group col-md-4">
					<label for="Profile_title" class="required">Entidades validas</label>
					<?php
					if(!$model->isNewRecord){
						$filtros=CHtml::listData($model->entidades, 'id_entity', 'id_entity');
					}else{
						$filtros=array();
					}
					echo CHtml::dropDownList('id_entity', $filtros, CHtml::listData(Entity::model()->findAll(), 'id', 'title'),array('multiple'=>'multiple','class'=>'form-control select2','data-placeholder'=>'Todos los bancos','style'=>'width:100%')); ?>
				</div>
				<div class="form-group col-md-4">
					<label for="Profile_title" class="required">Tarjetas validas</label>
					<?php
					if(!$model->isNewRecord){
						$filtros=CHtml::listData($model->creditcards, 'id_creditcard', 'id_creditcard');
					}else{
						$filtros=array();
					}
					echo CHtml::dropDownList('id_creditcard', $filtros, CHtml::listData(Creditcards::model()->findAll(), 'id', 'name'),array('multiple'=>'multiple','class'=>'form-control select2','data-placeholder'=>'Todas las tarjetas','style'=>'width:100%')); ?>
				</div>
				<div class="form-group col-md-4 required">
				<?php echo $form->labelEx($model,'url'); ?>
				<div class="input-group">
				<?php echo $form->textField($model,'url',array('size'=>260,'maxlength'=>260,'placeholder'=>'http://www.ttsoffice.com','class'=>'form-control')); ?>
				<div class="input-group-addon pointer"><i class="fa fa-globe"></i></div>
				</div>
				<?php echo $form->error($model,'url'); ?>
				</div>
				<div class="form-group col-md-4 required">
				<?php echo $form->labelEx($model,'title_seo'); ?>
				<?php echo $form->textField($model,'title_seo',array('size'=>50,'maxlength'=>50,'class'=>'form-control')); ?>
				<?php echo $form->error($model,'title_seo'); ?>
				</div>
				<div class="col-md-4 form-group">
					<?php echo $form->labelEx($model,'id_subchannel'); ?>
					<?php
					if($model->isNewRecord){
						echo $form->dropDownList($model, 'id_subchannel',array(),array('empty' => 'Seleccione','class'=>'form-control subchannel'));
					}else{
						echo $form->dropDownList($model, 'id_subchannel',CHtml::listData(Subchannels::model()->findAll("id_channel=".$model->id_channel), 'id', 'title'),array('empty' => 'Seleccione','class'=>'form-control'));
					}
					?>
					<?php echo $form->error($model,'id_subchannel'); ?>
				</div>
				<div class="col-md-4 form-group required">
					<?php echo $form->labelEx($model,'timeout'); ?>
					<?php echo $form->textField($model,'timeout',array('class'=>'form-control numeros')); ?>
					<?php echo $form->error($model,'timeout'); ?>
				</div>
				<div class="col-md-4 form-group col-data">
					<?php echo $form->labelEx($model,'reward'); ?>
					<?php echo $form->checkBox($model,'reward',array('class'=>'checkbox_form')); ?>
					<?php echo $form->error($model,'reward'); ?>
				</div>
				<div class="col-md-4 form-group col-data" style="clear:both;">
					<?php echo $form->labelEx($model,'promotional_site'); ?>
					<?php echo $form->checkBox($model,'promotional_site',array('class'=>'checkbox_form')); ?>
					<?php echo $form->error($model,'promotional_site'); ?>
				</div>
				<div id="promotional_options" style="<?php if($model->promotional_site==1){ echo 'display:block;'; }else{ echo 'display:none;';  }?>">
					<div class="form-group col-md-4" style="clear:both;">
						<?php echo $form->labelEx($model,'promotional_url_before'); ?>
						<div class="input-group">
							<?php echo $form->textField($model,'promotional_url_before',array('size'=>260,'maxlength'=>260,'placeholder'=>'http://www.ttsoffice.com','class'=>'form-control')); ?>
							<div class="input-group-addon pointer"><i class="fa fa-globe"></i></div>
						</div>
						<?php echo $form->error($model,'promotional_url_before'); ?>
					</div>
					<div class="col-md-4 form-group">
						<?php echo $form->labelEx($model,'promotional_before_start_date'); ?>
						<div class="input-group">
							<?php echo $form->textField($model,'promotional_before_start_date',array('class'=>'form-control datepicker','data-inputmask'=>'\'alias\': \'dd/mm/yyyy\'','data-mask'=>'data-mask')); ?>
							<div class="input-group-addon pointer" data-toggle="tooltip" title="Abrir el calendario"><i class="fa fa-calendar"></i></div>
						</div>
						<?php echo $form->error($model,'promotional_before_start_date'); ?>
					</div>
					<div class="col-md-4 form-group">
						<?php echo $form->labelEx($model,'promotional_before_end_date'); ?>
						<div class="input-group">
							<?php echo $form->textField($model,'promotional_before_end_date',array('class'=>'form-control datepicker','data-inputmask'=>'\'alias\': \'dd/mm/yyyy\'','data-mask'=>'data-mask')); ?>
							<div class="input-group-addon pointer" data-toggle="tooltip" title="Abrir el calendario"><i class="fa fa-calendar"></i></div>
						</div>
						<?php echo $form->error($model,'promotional_before_end_date'); ?>
					</div>
					<div class="form-group col-md-4" style="clear:both;">
						<?php echo $form->labelEx($model,'promotional_url_after'); ?>
						<div class="input-group">
							<?php echo $form->textField($model,'promotional_url_after',array('size'=>260,'maxlength'=>260,'placeholder'=>'http://www.ttsoffice.com','class'=>'form-control')); ?>
							<div class="input-group-addon pointer"><i class="fa fa-globe"></i></div>
						</div>
						<?php echo $form->error($model,'promotional_url_after'); ?>
					</div>
					<div class="col-md-4 form-group">
						<?php echo $form->labelEx($model,'promotional_after_start_date'); ?>
						<div class="input-group">
							<?php echo $form->textField($model,'promotional_after_start_date',array('class'=>'form-control datepicker','data-inputmask'=>'\'alias\': \'dd/mm/yyyy\'','data-mask'=>'data-mask')); ?>
							<div class="input-group-addon pointer" data-toggle="tooltip" title="Abrir el calendario"><i class="fa fa-calendar"></i></div>
						</div>
						<?php echo $form->error($model,'promotional_after_start_date'); ?>
					</div>
					<div class="col-md-4 form-group">
						<?php echo $form->labelEx($model,'promotional_after_end_date'); ?>
						<div class="input-group">
							<?php echo $form->textField($model,'promotional_after_end_date',array('class'=>'form-control datepicker','data-inputmask'=>'\'alias\': \'dd/mm/yyyy\'','data-mask'=>'data-mask')); ?>
							<div class="input-group-addon pointer" data-toggle="tooltip" title="Abrir el calendario"><i class="fa fa-calendar"></i></div>
						</div>
						<?php echo $form->error($model,'promotional_after_end_date'); ?>
					</div>
				</div>
				<div class="col-md-12 form-group ">
					<?php echo $form->labelEx($model,'script'); ?>
					<?php echo $form->textArea($model,'script',array('class'=>'form-control','rows'=>10)); ?>
					<?php echo $form->error($model,'script'); ?>
				</div>
			</div>
			  <div class="tab-pane" id="tab_2">
				<br />
				<div class="col-md-8 form-group required">
					<?php echo $form->labelEx($model,'tracking_description'); ?>
					<?php echo $form->textArea($model,'tracking_description',array('class'=>'form-control','rows'=>3)); ?>
					<?php echo $form->error($model,'tracking_description'); ?>
				</div>

				<div class="col-md-8 form-group required">
					<?php echo $form->labelEx($model,'tracking_analitycs'); ?>
					<?php echo $form->textArea($model,'tracking_analitycs',array('class'=>'form-control','rows'=>10)); ?>
					<?php echo $form->error($model,'tracking_analitycs'); ?>
				</div>
			  </div>
		</div>
	  </div>
	</div>
</div>
<div class="box-footer">
		<?php
		$title = (Yii::app()->controller->action->id!='view') ? 'Volver sin guardar cambios' : 'Volver';
		echo '<a href="'.Yii::app()->request->baseUrl.'/'.strtolower(Yii::app()->controller->id).'" class="btn btn-secundary" data-toggle="tooltip" title="'.$title.'" >Cerrar</a>';
		if(Yii::app()->controller->action->id!='view'){
			echo CHtml::submitButton('Guardar',array('class'=>'btn btn-primary','data-toggle'=>'tooltip','title'=>'Guardar cambios'));
		}
		?>
</div>
<?php $this->endWidget(); ?>
<?php
	Yii::app()->clientScript->registerCssFile(Yii::app()->params['cdnUrl'].'/plugins/lcswich/lc_switch-tts.css');
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/lcswich/lc_switch.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/lcswich/lc_switch-tts.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/plugins/select2/select2.full.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl.'/js/plugins/select2/select2.css');
	Yii::app()->clientScript->registerCssFile(Yii::app()->params['cdnUrl'].'/plugins/jQueryUI/jquery-ui.min.css');
	Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/jquery-ui.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerCssFile(Yii::app()->params['cdnUrl'].'/plugins/datepicker/datepicker3.css');
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/datepicker/bootstrap-datepicker.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/input-mask/jquery.inputmask.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/input-mask/jquery.inputmask.date.extensions.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/datepicker/datepicker-tts.js',CClientScript::POS_END);
echo '<script>viewPage = "'.Yii::app()->controller->action->id.'";</script>';
Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/js/form-validators.js',CClientScript::POS_END) ?>
<script>
 $( document ).ready(function() {
	$('.select2').select2().on('select2:unselect', function(e) {
		$('.select2-selection').trigger('click');
	});
	 $(".tab-pane").each(function() {
		if($(this).find('.errorMessage').length>0){
			$('.nav-tabs').find('li').removeClass('active');
			$("."+$(this).attr('id')).children().trigger( "click" );
			return false;
		}
	});
 });
$( document ).ready(function() {
	channelsFilter();
 	$("#Subchannels_promotional_site").click(function(){
		if($(this).is(":checked")){
			$('#promotional_options').css('display','block');
		}else{
			$('#promotional_options').css('display','none');
		}
	});
	<?php if(Yii::app()->controller->action->id=='view'){ echo "$('form').find('input, textarea, button, checkbox, select').attr('disabled','disabled');"; } ?>
});
</script>
