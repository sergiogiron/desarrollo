<?php
/* @var $this SubchannelsController */
/* @var $model Subchannels */

Yii::app()->clientScript->registerScript('search', "
	search();
	searchAdd();
	deleteBtn();
");
?>
<section class="content-header">
	<?php echo Yii::app()->user->Title(); ?>
	<?php echo Yii::app()->user->Breadcrumb(); ?>
	<?php $this->widget('ext.publicationrecord.PublicationrecordWidget', array('controller_custom' =>'subchannels')); ?>
</section>
<section class="content">
	<div class="box box-danger">
		<div class="box-header with-border">	
			<?php			if(Yii::app()->user->checkAccess('create')){
				echo '<a href="'.Yii::app()->request->baseUrl.'/'.Yii::app()->controller->id.'/create" class="btn btn-default b_customize agregar"><i class="fa fa-plus i_customize"></i> Agregar</a>';
			}
			?>	

			<?php
				if(Yii::app()->user->checkAccess('publish')){		
					echo '<a href="'.Yii::app()->request->baseUrl.'/'.Yii::app()->controller->id.'/publish" validate="validate" class="publish btn btn-success a_customize"><i class="fa fa-cloud-upload i_customize"></i>Publicar</a>';
				}
			?>

					<?php echo Utils::SearchObject(); ?>			
					<?php echo CHtml::link('Búsqueda avanzada','#',array('class'=>'search-button')); ?>
					<p class="registros">(<span><?=$model->count();?></span> registros encontrados)</p>
			<div class="search-form" style="display:none">
			<?php $this->renderPartial('_search',array(
				'model'=>$model,
			)); ?>
			</div>

			<?php 
			$this->widget('GridView', array(
				'id'=>'subchannels-grid',
				'pager' => array(
					'cssFile'=>false,
					'header'=> '',
					'firstPageLabel' => '<i class="fa fa-forward fa-flip-horizontal" data-toggle="tooltip" title="Primera página"></i>',
					'prevPageLabel'  => '<i class="fa fa-caret-right fa-flip-horizontal" data-toggle="tooltip" title="Anterior"></i>',
					'nextPageLabel'  => '<i class="fa fa-caret-right" data-toggle="tooltip" title="Siguiente"></i>',
					'lastPageLabel'  => '<i class="fa fa-forward" data-toggle="tooltip" title="Última página"></i>', 
				),
				'afterAjaxUpdate'=> 'function(){enableSwitch();deleteBtn(); closeLoading(); getTotalRows();}',				
				'summaryText' => '', 
				'itemsCssClass' => 'table table-bordered table-striped table-list',	
				'dataProvider'=>$model->search(),
				'columns'=>array(
				
					array(
						'name' => 'enabled',
						'type' => 'raw',
						'value' => 'Utils::activeSwitch($data)',
						'htmlOptions'=>array('class'=>'activeSwitch'),
					),		
					array(
					'header'=>'Logo',
					'value'=>'(!empty($data->filename))?CHtml::image(Yii::app()->request->baseUrl."/uploads/subchannels/".$data->id."/".$data->filename."/".$data->filename.".".$data->ext,"",array("style"=>"width:50px;margin: auto;display: table;")):"Sin logo"',
					'type'  => 'raw',				   
					),						
				array(
					'name' => 'id_channel',
					'value' => '$data->canal->title',
				),
					'title',
					'url',
					array(
						'name'=>'reward',
						 'type'=>'raw',
						'value' => '($data->reward == 1 || $data->reward== true)? \'<i class="fa fa-check"><i/>\' : \'<i class="fa fa-close"><i/>\'',
					),
					array(
						'name'=>'promotional_site',
						 'type'=>'raw',
						'value' => '($data->promotional_site == 1 || $data->promotional_site== true)? \'<i class="fa fa-check"><i/>\' : \'<i class="fa fa-close"><i/>\'',
					),						
					array(
						'name' => 'id_subchannel',
						'value' => '(is_null($data->id_subchannel)) ? "" : Utils::is__null($data)',
					),
					array(
						'class'=>'CButtonColumn',
						'template'=>'{view}{update}{erase}',	
						'buttons'=>array(		
							'view' => array(
								'label'=>'<i class="fa fa-eye icon_cbutton"></i>',
								'url'=>'Yii::app()->createUrl(strtolower(Yii::app()->controller->id).\'/view/id/\'. $data->id)',
								'visible'=>'Yii::app()->user->checkAccess(\'view\')',
								'options'=>array('title'=>'Ver','data-toggle'=>'tooltip'),
								'imageUrl'=>false,
							),						
							'update' => array(
								'label'=>'<i class="fa fa-edit icon_cbutton"></i>',
								'visible'=>'Yii::app()->user->checkAccess(\'update\')',
								'url'=>'Yii::app()->createUrl(strtolower(Yii::app()->controller->id).\'/update/id/\'. $data->id)',
								'options'=>array('title'=>'Editar','data-toggle'=>'tooltip'),
								'imageUrl'=>false,
							),					
							'erase' => array(
								'label'=>'<i class="fa fa-trash-o icon_cbutton"></i>',
								'url'=>'Yii::app()->createUrl(strtolower(Yii::app()->controller->id).\'/delete/id/\'. $data->id)',
								'visible'=>'Yii::app()->user->checkAccess(\'delete\')',
								'options' => array(
									'class' => 'delete_action',
									'title'=>'Borrar',
									'imageUrl'=>false,
									'data-toggle'=>'tooltip'
								)	
							),
						),
					),
				)
			));
			?>
		</div>
	</div>
 </section>
 <script>
function validate_publish(element){	
	$.ajax({
	  method: "POST",
	  url: homeurl+"/subchannels/validatepublish",
	}).done(function(msg) {
		if(msg=='true'){
			element.removeAttr('validate');
			$(".publish").trigger('click');				
		}else{
			swal("Operación incorrecta!", "Debe completar los campos obligatorios de subcanales para poder publicar.", "error"); 
		}
	});	
}
</script>
<?php Yii::app()->clientScript->registerCssFile(Yii::app()->params['cdnUrl'].'/plugins/lcswich/lc_switch-tts.css'); 
Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/lcswich/lc_switch.js',CClientScript::POS_END);
Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/lcswich/lc_switch-tts.js',CClientScript::POS_END);
 ?>