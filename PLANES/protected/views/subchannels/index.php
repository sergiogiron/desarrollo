<?php
/* @var $this SubchannelsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Subchannels',
);

$this->menu=array(
	array('label'=>'Create Subchannels', 'url'=>array('create')),
	array('label'=>'Manage Subchannels', 'url'=>array('admin')),
);
?>

<h1>Subchannels</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
