<section class="content" style="">
	<div class="row">
	<?php
	foreach(Yii::app()->user->getWidgets() as $widget){
		echo '<div class="col-md-'.$widget['size'].'"><iframe width="100%" src="'.$widget['url'].'" id="buscador" style="border: 0px;scroll:none;"></iframe></div>';
	}
	?>
	</div>
</section>
<script type="text/javascript">
    $(window).load(function(){
             $("#loader").css('display','none');
     });

    $(document).ready(function(){
        // Create browser compatible event handler.
        var eventMethod = window.addEventListener ? "addEventListener" : "attachEvent";
        var eventer = window[eventMethod];
        var messageEvent = eventMethod == "attachEvent" ? "onmessage" : "message";
        // Listen for a message from the iframe.
        eventer(messageEvent, function(e) {
            var data = e.data;
            if (data.hasOwnProperty('scroll')) {
                window.scroll(data.scroll.height, data.scroll.width);
            }

            if (data.hasOwnProperty('caja')) {
                $('iframe#buscador').height(data.caja.height);
            }

            if (data.hasOwnProperty('cargando')) {
                $('#cargando').css('display',data.cargando.display);
                $('#cargando span').text(data.cargando.text);
            }

            if (data.hasOwnProperty('flex')) {
                $("#loader").css('display','flex');
            }

        }, false);
    });
</script>