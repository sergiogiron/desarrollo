<?php
/* @var $this MailCampaignController */
/* @var $model MailCampaign */

 
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#mail-campaign-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");

Yii::app()->clientScript->registerScript('search2', "
$('.search-button2').click(function(){
	$('.search-form2').toggle();
	return false;
});
$('.search-form2 form').submit(function(){
	$('#mail-campaign-grid2').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
Yii::app()->clientScript->registerScript('deletebtn', "
	deleteBtn2();
");
?>
<section class="content-header">
	<?php echo Yii::app()->user->Title(); ?>
	<?php echo Yii::app()->user->Breadcrumb(); ?>
</section>

	<section class="content">
		<div class="box box-danger">
		<div class="box-header with-border">
		<?php
		if(Yii::app()->user->checkAccess('create')){
			echo '<a href="'.Yii::app()->request->baseUrl.'/campaña/crear"class="btn btn-default b_customize"><i class="fa fa-plus i_customize"></i> Agregar</a>';
		}
		?>		

		 <!-- Custom Tabs -->
          <div class="nav-tabs">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#tab_1" data-toggle="tab">En cola</a></li>
              <li><a href="#tab_2" data-toggle="tab">Enviadas</a></li> 
               
            </ul>
            <div class="tab-content">
              <div class="tab-pane active" id="tab_1">
				<div class="col-md-3 search_bar">
					<div class="row">
						<div class="input-group input-group-sm">
							<input type="text" class="form-control" id="criterio"><i class="fa fa-close right-icon-search"></i>
							<span class="input-group-btn">
							  <button type="button" id="buscar" class="btn btn-default btn-flat"><i class="fa fa-search"></i></button>
							</span>
						</div>
					</div>
				</div>
				 	
				 		
				
                 <?php $this->widget('zii.widgets.grid.CGridView', array(
					'id'=>'mail-campaign-grid',
					'dataProvider'=>$model->search(),
					'pager' => array(
						'cssFile'=>false,
						'header'=> '',
						'firstPageLabel' => '<i class="fa fa-forward fa-flip-horizontal" data-toggle="tooltip" title="Primera página"></i>',
						'prevPageLabel'  => '<i class="fa fa-caret-right fa-flip-horizontal" data-toggle="tooltip" title="Anterior"></i>',
						'nextPageLabel'  => '<i class="fa fa-caret-right" data-toggle="tooltip" title="Siguiente"></i>',
						'lastPageLabel'  => '<i class="fa fa-forward" data-toggle="tooltip" title="Última página"></i>', 
					),
					'afterAjaxUpdate'=> 'function(){
						enableSwitch();
						limpiar_search();
						deleteBtn();
					}',
					'summaryText' => '', 
					'itemsCssClass' => 'table table-bordered table-striped table-list',	
					 
					'columns'=>array(
						array(
							'name' => 'enabled',
							'type' => 'raw',
							'value' => 'Utils::activeSwitch($data)',
							'htmlOptions'=>array('class'=>'activeSwitch'),
						),
						'name',
						'subject',
						array(
							'name'=>'created_at',
							'filter'=>CHtml::activeTextField($model,'created_at'),
							'value'=>'Utils::datetime_date_spa($data->created_at)',
						),	
						array(
							'name'=>'date_to',
							'filter'=>CHtml::activeTextField($model,'date_to'),
							  //'value'=>'Yii::app()->dateFormatter->format("d/M/y",strtotime($data->date_to))',
							 //'value'=>'date("d/m/Y",strtotime($data->date_to))'
							'value'=>'!empty($data->date_to)? $data->date_to:"No hay Datos" ;'
						),
						array(
							'name'=>'send_date',
							'filter'=>CHtml::activeTextField($model,'send_date'),
							 // 'value'=>'!empty($data->send_date)? Yii::app()->dateFormatter->format("d/M/y",strtotime($data->send_date)):"Sin fecha de envío" ;',
							 //'value'=>'!empty($data->send_date)? date("d/m/Y",strtotime($data->send_date)):"Sin fecha de envío" ;'
							 'value'=>'!empty($data->send_date)? $data->send_date:"Sin fecha de envío" ;'
						),
						
						array(
							'class'=>'CButtonColumn',
							'template'=>'{preview}{view}{update}{send}{erase}',	
							'buttons'=>array(	
								'send' => array(
									'label'=>'<i class="fa fa-paper-plane icon_cbutton"></i>',
									'url'=>'Yii::app()->createUrl(\'campaña/send/\'. $data->id)',
									'options'=>array('title'=>'Enviar','class'=>'enviar_email'),
									'imageUrl'=>false,
									'visible'=>'$data->enabled',
								),
								'preview' => array(
									'label'=>'<i class="fa fa-envelope-o icon_cbutton"></i>',
									'url'=>'Yii::app()->createUrl(\'campaña/preview/\'. $data->id)',
									'options'=>array('title'=>'Previsualizar','class'=>'ver_email'),
									'imageUrl'=>false,
								),		
								'view' => array(
									'label'=>'<i class="fa fa-eye icon_cbutton"></i>',
									'url'=>'Yii::app()->createUrl(\'campaña/ver/\'. $data->id)',
									'options'=>array('title'=>'Ver'),
									'imageUrl'=>false,
								),						
								'update' => array(
									'label'=>'<i class="fa fa-edit icon_cbutton"></i>',
									'url'=>'Yii::app()->createUrl(\'campaña/modificar/\'. $data->id)',
									'options'=>array('title'=>'Editar'),
									'imageUrl'=>false,
								),		/*			
								'delete' => array(
									'label'=>'<i class="fa fa-trash-o icon_cbutton"></i>',
									'url'=>'Yii::app()->createUrl(\'campaña/borrar/\'. $data->id)',
									'options'=>array('title'=>'Borrar'),
									'imageUrl'=>false,
								),	*/
								'erase' => array(
									'label'=>'<i class="fa fa-trash-o icon_cbutton"></i>',
									'url'=>'Yii::app()->createUrl(\'campaña/borrar/\'. $data->id)',
									'imageUrl'=>false,
									'options' => array(
										'class' => 'delete_action',
										'title'=>'Borrar',
										'imageUrl'=>false,
									)	
								),						
							),
							#'htmlOptions'=>array('width'=>'100px'),
						)
					),
				)); ?>
              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="tab_2">
				<div class="col-md-3 search_bar">
					<div class="row">
						<div class="input-group input-group-sm">
							<input type="text" class="form-control" id="criterio2"><i class="fa fa-close right-icon-search right-icon-search2"></i>
							<span class="input-group-btn">
							  <button type="button" id="buscar2" class="btn btn-default btn-flat"><i class="fa fa-search"></i></button>
							</span>
						</div>
					</div>
				</div>
				
				 	  	
				
				<?php $this->widget('zii.widgets.grid.CGridView', array(
					'id'=>'mail-campaign-grid2',
					'dataProvider'=>$model2->search2(),
					'pager' => array(
						'cssFile'=>false,
						'header'=> '',
						'firstPageLabel' => '<i class="fa fa-forward fa-flip-horizontal" data-toggle="tooltip" title="Primera página"></i>',
						'prevPageLabel'  => '<i class="fa fa-caret-right fa-flip-horizontal" data-toggle="tooltip" title="Anterior"></i>',
						'nextPageLabel'  => '<i class="fa fa-caret-right" data-toggle="tooltip" title="Siguiente"></i>',
						'lastPageLabel'  => '<i class="fa fa-forward" data-toggle="tooltip" title="Última página"></i>', 
					),
					'afterAjaxUpdate'=> 'function(){
						enableSwitch();
						limpiar_search2();
						deleteBtn();
					}',
					'summaryText' => '', 
					'itemsCssClass' => 'table table-bordered table-striped table-list',			
					'columns'=>array(
						array(
							'name' => 'enabled',
							'type' => 'raw',
							'value' => 'Utils::activeSwitch($data)',
							'htmlOptions'=>array('class'=>'activeSwitch'),

							 
						),
						'name',
						'subject',
						array(
							'name'=>'created_at',
							'filter'=>CHtml::activeTextField($model,'created_at'),
							'value'=>'Utils::datetime_date_spa($data->created_at)',
						),	
						
						array(
							'name'=>'date_to',
							'filter'=>CHtml::activeTextField($model2,'date_to'),
							 // 'value'=>'Yii::app()->dateFormatter->format("d/M/y",strtotime($data->date_to))'
							 'value'=>'!empty($data->date_to)? $data->date_to:"Sin fecha de envío" ;'
						),	 
						array(
							'name'=>'send_date',
							'filter'=>CHtml::activeTextField($model2,'send_date'),
							// 'value'=>'!empty($data->send_date)? Yii::app()->dateFormatter->format("d/M/y",strtotime($data->send_date)):"Sin fecha de envío" ;'
								'value'=>'!empty($data->send_date)? $data->send_date:"Sin fecha de envío" ;'
						),						
						array(
							'class'=>'CButtonColumn',
							'template'=>' {view} ',	
							'buttons'=>array(	 
								'view' => array(
									'label'=>'<i class="fa fa-envelope-o icon_cbutton"></i>',
									'url'=>'Yii::app()->createUrl(\'campaña/preview/\'. $data->id)',
									'options'=>array('title'=>'Ver','class'=>'ver_email'),
									'imageUrl'=>false,
								),				 
							'htmlOptions'=>array('width'=>'100px'),								
							)
						)
					),
				)); ?>
				 
              </div>
              
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- nav-tabs-custom -->
		  
		  
		
		</div>
		</div>
 </section>
 <?php
	// Activo/Inactivo
	Yii::app()->clientScript->registerCssFile(Yii::app()->params['cdnUrl'].'/plugins/lcswich/lc_switch-tts.css'); 
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/lcswich/lc_switch.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/lcswich/lc_switch-tts.js',CClientScript::POS_END);
 ?>
 <script>
function deleteBtn2(){
	$( ".delete_action" ).click(function(event) {
		event.preventDefault();
		link=$(this).attr('href');
		swal({
			title: "Seguro desea eliminar este registro?", 
			text: "No se podrá recuperar el registro!",  
			type: "warning", 
			showCancelButton: true, 
			confirmButtonColor: "#DD6B55", 
			confirmButtonText: "Sí, deseo borrarlo!", 
			closeOnConfirm: false
			}, 
			function(){
				$.ajax({
					url:link ,
					success:function() {
						swal("Operación correcta!", "Se ha eliminado el registro.", "success"); 
					},
					error:function(xhr) {
						 swal("Operación incorrecta!", xhr.responseText, "error"); 
					},
				}).done(function() {
				$('#mail-campaign-grid').yiiGridView('update');
			});								
		});		  
	});
}

function limpiar_search(){
 $(".right-icon-search").click(function(){
        $(this).parent().find("input").val('');
        $(this).css('color','#ccc');
		$('#buscar').trigger('click');	
    });
}
function limpiar_search2(){
 $(".right-icon-search2").click(function(){
        $(this).parent().find("input").val('');
        $(this).css('color','#ccc');
		$('#buscar2').trigger('click');	
    });
}
$( document ).ready(function() {
	deleteBtn2();
	$( "#buscar" ).click(function(){
		$('#mail-campaign-grid').yiiGridView('update', {
			data: { criterio : $('#criterio').val() }
		});
		return false;
	});	
	$("#criterio").keypress(function(e){
       if(e.which == 13) {
          $("#buscar").trigger('click');
       }
    });
	$( "#buscar2" ).click(function(){
		$('#mail-campaign-grid2').yiiGridView('update', {
			data: { criterio2 : $('#criterio2').val() }
		});
		return false;
	});	
	$("#criterio2").keypress(function(e){
       if(e.which == 13) {
          $("#buscar2").trigger('click');
       }
    });	
	$('body').on('click','.ver_email', function(event) {
		event.preventDefault();
		var request = $.ajax({
			url: $(this).attr('href'),
		}).done(function(msg) {
			$('.ligth-box-modal').css('display','block');
			$('.modal-title').html('Preview del email');
			$('.modal-body').html(msg);
		});	
	});
	 
	 $('body').on('click','.enviar_email', function(event) {
		event.preventDefault();
		var request = $.ajax({
			url: $(this).attr('href'),
		}).done(function(msg) {
			swal("La campaña se ha puesto en cola de envío");
			$.fn.yiiGridView.update('mail-campaign-grid');
			$.fn.yiiGridView.update('mail-campaign-grid2');
			/*$('.modal-body').html(msg);*/
		});	
	});
	  
});	
</script>