<?php
/* @var $this MailCampaignController */
/* @var $model MailCampaign */

 
?>
<section class="content-header">
	<?php echo Yii::app()->user->Title(); ?>
	<?php echo Yii::app()->user->Breadcrumb(); ?>
</section>

<section class="content">
	<div class="box box-danger">
		<div class="box-header with-border">
			<?php $this->renderPartial('_form', array(
			'model'=>$model,
			'agentes'=>$agentes, 
			'agentes_selected'=>$agentes_selected, 
			'gerencias'=>$gerencias, 
			'gerencias_selected'=>$gerencias_selected, 
			'sectores'=>$sectores, 
			'sectores_selected'=>$sectores_selected,
			'tags'=>$tags,
			'fullfilter'=>$fullfilter,
			)); ?>
		</div>
	</div>
</section>