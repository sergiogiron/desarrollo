<?php
#echo'<pre>';var_dump($fullfilter);die;
/* @var $this MailCampaignController */
/* @var $model MailCampaign */
/* @var $form CActiveForm */
?>
<script type='text/javascript'>
<?php
$js_array = json_encode($fullfilter);
echo "var javascript_array = ". $js_array . ";\n";
?>
</script>

<div class="box-body">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'mail-campaign-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>
	<span class="right_f">
		<?php echo $form->checkBox($model,'enabled',array('class'=>'lcs_check enabled')); ?>
	</span>
	<div class="preview_email"style="margin-right: 50px;"><i class="fa fa-envelope-o icon_cbutton "></i> Visualizar el email completo.</div>

<div class="col-md-12 col-data">



	<div class="form-group unique" style="clear: both;">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>255,'class'=>'form-control required')); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'subject'); ?>
		<?php echo $form->textField($model,'subject',array('size'=>60,'maxlength'=>255,'class'=>'form-control required')); ?>
		<?php echo $form->error($model,'subject'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'body'); ?>
		<?php echo $form->textArea($model,'body',array('rows'=>6, 'cols'=>50,'class'=>'form-control ckeditor required')); ?>
		<?php echo $form->error($model,'body'); ?>
	</div>

	<div class="form-group col-md-6">
		<?php echo $form->labelEx($model,'id_email_header'); ?>
		<?php echo $form->dropDownList($model, 'id_email_header',CHtml::listData(Emailheader::model()->findAll('deleted=0'), 'id', 'name'),array('empty' => 'Seleccione','class'=>'form-control'));?>
		<?php echo $form->error($model,'id_email_header'); ?>
	</div>

	<div class="form-group col-md-6">
		<?php echo $form->labelEx($model,'id_email_footer'); ?>
		<?php echo $form->dropDownList($model, 'id_email_footer',CHtml::listData(Emailfooter::model()->findAll('deleted=0'), 'id', 'name'),array('empty' => 'Seleccione','class'=>'form-control'));?>
		<?php echo $form->error($model,'id_email_footer'); ?>
	</div>

			<div class="col-xxs-12 col-xs-4 form-group">
			<?php echo $form->labelEx($model,'send_date'); ?>
			<div class="input-group">
				<!-- <div class="input-group-addon pointer"><i class="fa fa-calendar"></i></div> -->
				<?php echo $form->textField($model,'send_date',array('class'=>'form-control datepicker','data-inputmask'=>'\'alias\': \'dd/mm/yyyy\'','data-mask'=>'data-mask')); ?>
				<div class="input-group-addon pointer"><i class="fa fa-calendar"></i></div>
			</div>
			<?php echo $form->error($model,'send_date'); ?>
		</div>
	
	 
<!--
	<div class="form-group col-md-4">
		<?php #echo $form->labelEx($model,'send_hour'); ?>
		<?php
		$horas=array();
		for($i=0;$i<24;$i++){
			if($i<10){
				$time='0'.$i.':00';
			}else{
				$time=$i.':00';
			}
			$horas[]=array('option'=>$time,'value'=>$time);
		}
		?>
		<?php #echo $form->dropDownList($model,'send_hour', CHtml::listData($horas, 'option', 'value'),array('empty' => 'Seleccione','class'=>'form-control')); ?>
		
		  
		<?php #echo $form->error($model,'send_hour'); ?>
	</div>-->
	

<div class="form-group col-md-4">
			<?php echo $form->labelEx($model,'send_hour'); ?>
			<div class="input-group bootstrap-timepicker">
				<?php echo $form->textField($model,'send_hour',array('class'=>'form-control timepicker')); ?>
				<div class="input-group-addon pointer" data-toggle="tooltip" title="Abrir el reloj"><i class="fa fa-clock-o"></i></div>	
			</div>
			<?php echo $form->error($model,'send_hour'); ?>
</div>


		<div class="col-xxs-12 col-xs-4 form-group required">
			<?php echo $form->labelEx($model,'date_to', array('required' => true)); ?>
			<div class="input-group">
				<!-- <div class="input-group-addon pointer"><i class="fa fa-calendar"></i></div> -->
				<?php echo $form->textField($model,'date_to',array('class'=>'form-control datepicker','data-inputmask'=>'\'alias\': \'dd/mm/yyyy\'','data-mask'=>'data-mask')); ?>
				<div class="input-group-addon pointer"><i class="fa fa-calendar"></i></div>
			</div>
			<?php echo $form->error($model,'date_to'); ?>
		</div>

	
	<!--<div class="form-group col-md-3">
		<?php echo $form->labelEx($model,'enabled'); ?>
		<?php echo $form->dropDownList($model,'enabled',array(1=>'Activa',0=>'Inactiva'),array('empty' => 'Seleccione','class'=>'form-control  required')); ?>
		<?php echo $form->error($model,'enabled'); ?>
	</div>-->

	
	<?php
	$sectores_seleccionados=[];
	if(isset($sectores_selected) && !empty($sectores_selected) && count($sectores_selected)>0 ) {
	   foreach($sectores_selected as $key => $val){	
			$sectores_seleccionados[$key]=$val;	 
		}
	} 
	?>
	<div class="form-group col-md-6" id="sectores">
			<label for="Emailcampaign_sectores">Sectores</label>
			<?php 
				echo CHtml::dropDownList('Emailcampaign[sectores]',$sectores_seleccionados, $sectores, array('class'=>'form-control select2','data-placeholder'=>'Seleccione','id'=>'sectores_select','multiple'=>'multiple','style'=>'width:45%')); 
			?>
	</div> 
	
	
	<?php
	$gerencias_seleccionados=[];
	if(isset($gerencias_selected) && !empty($gerencias_selected) && count($gerencias_selected)>0 ) {
	   foreach($gerencias_selected as $key => $val){	
			$gerencias_seleccionados[$key]=$val;	 
		}
	} 
	?>
	<div class="form-group col-md-6" id="gerencias">
			<label for="Emailcampaign_gerencias">Gerencias</label>
			<?php 
				echo CHtml::dropDownList('Emailcampaign[gerencias]',$gerencias_seleccionados, $gerencias, array('class'=>'form-control select2','data-placeholder'=>'Seleccione','id'=>'gerencias_select','multiple'=>'multiple','style'=>'width:45%')); 
			?>
	</div> 
	
	<br />
	<?php
		$seleccionados=[];
		if(isset($agentes_selected) && !empty($agentes_selected) && count($agentes_selected)>0 ) {
		   foreach($agentes_selected as $key => $val){	
				$seleccionados[$key]=$val;	 
			}
		} 
	?>
	<div class="form-group col-md-12" id="agentes">
			<label for="Emailcampaign_agentes">Agentes de Venta</label>
			<?php 
				#$seleccionados= "";#array("RowID"=> 1,"Admin"=>"AALVAREZ",);
				echo CHtml::dropDownList('Emailcampaign[agentes]',$seleccionados, $agentes, array('class'=>'form-control select2','data-placeholder'=>'Seleccione','id'=>'agentes_select','multiple'=>'multiple','style'=>'width:100%')); 
			?>
	</div> 
	
	</div>

  <div class="box-footer">
	<?php echo CHtml::button('Cerrar', array('onclick' => 'js:document.location.href="'.Yii::app()->request->baseUrl.'/campañas"','class'=>'btn btn-secundary cerrarbtn','data-toggle'=>'tooltip','title'=>'Volver sin guardar cambios')); ?>
	<?php if(Yii::app()->controller->action->id!='viewform'){
					echo CHtml::submitButton($model->isNewRecord ? 'Crear' : 'Guardar',array('class'=>'btn btn-primary','data-toggle'=>'tooltip','title'=>'Guardar cambios')); 
				}  ?> 
  </div>

<?php $this->endWidget(); ?>
</div>
<?php 

	Yii::app()->clientScript->registerCssFile(Yii::app()->params['cdnUrl'].'/plugins/lcswich/lc_switch-tts.css'); 
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/lcswich/lc_switch.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/lcswich/lc_switch-tts.js',CClientScript::POS_END);
	
	Yii::app()->clientScript->registerCssFile(Yii::app()->params['cdnUrl'].'/plugins/datepicker/datepicker3.css');
	Yii::app()->clientScript->registerCssFile(Yii::app()->params['cdnUrl'].'/plugins/timepicker/bootstrap-timepicker.css');
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/datepicker/bootstrap-datepicker.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/timepicker/bootstrap-timepicker.js',CClientScript::POS_END);
 	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/ckeditor-standard/ckeditor.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/select2/select2.full.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerCssFile(Yii::app()->params['cdnUrl'].'/plugins/select2/select2.css');
		

	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/input-mask/jquery.inputmask.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/input-mask/jquery.inputmask.date.extensions.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/../cdn/plugins/datepicker/datepicker-tts.js',CClientScript::POS_END);


	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/input-mask/jquery.inputmask.bundle.js',CClientScript::POS_END);

	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/timepicker/timepicker-tts.js',CClientScript::POS_END);
	 ?>
	
<script>
 function process(date){
   var parts = date.split("-");
   return new Date(parts[0], parts[1] - 1, parts[2]);
}

$( document ).ready(function() {

<?php
	if(Yii::app()->controller->action->id=='viewform')
	{
		echo "$('form').find('input, textarea, button, checkbox, select').attr('disabled','disabled');"; 
		echo "$('.cerrarbtn').removeAttr('disabled');"; 
}
?>

$('.select2').select2({
    allowClear: true
}).on('select2:unselecting', function() {
    $(this).data('unselecting', true);
}).on('select2:opening', function(e) {
    if ($(this).data('unselecting')) {
        $(this).removeData('unselecting');
        e.preventDefault();
    }
});

	$( document ).ready(function() {
		var js_selected_sec =[];
		$('#sectores_select').on('change',function(){
			
			$("#sectores_select").each(function()
			{
				// console.log($(this).val());
				// console.log($(this).text());
				var js_selected_ger=[];
				if($(this).val() !== null){
					$(this).val().forEach(function(element, index, array)
					{
						 Object.keys(javascript_array[element]).forEach(function(element2, index2, array2)
						{
							js_selected_ger.push(element2);
							
						});
						
					});
					$("#gerencias_select").val(js_selected_ger).trigger("change");
				}
				else{
					$("#gerencias_select").val('').trigger("change");					
				}
				//js_selected_sec.push($(this).val());
			})
		});
		$('#sectores_select').on('select2:unselecting',function(e){
			
			var deleted_sectores=[];
			if(alert("Esta seguro que quiere deseleccionar al sector: "+e.params.args.data.text+", sus gerencias y respectivos agentes?")){
				  e.preventDefault();
			}
			else{
				 
				deleted_sectores.push(e.params.args.data.id);
				remove_gerencias(e.params.args.data.id);
				}
		});
		
		$('#gerencias_select').on('select2:unselecting',function(e){
			var deleted_gerencias=[];
			if(alert("Esta seguro que quiere deseleccionar a la gerencia: "+e.params.args.data.text+" y a todos sus agentes?")){
				  e.preventDefault();
			}
			else{
				deleted_gerencias.push(e.params.args.data.id);
				remove_agents(e.params.args.data.id);
				}
			
		});
		$('#gerencias_select').on('select2:unselect',function(e){
			var count_ger=$('#gerencias_select').val(); 
			if(count_ger == null){
				$("#sectores_select").val("").trigger("change");	
			}			
			//remove_parent_sector(e.params.data.id);
		}) 
				
		function remove_agents(gerencia_id) {
			 var dr=$("#agentes_select").val();
			 if(dr!==null)
			 {
			 dr.forEach(function(element, index2, array2)
				{
					if(gerencia_is_parent(gerencia_id,element)){
						deselect($("#agentes_select"),element);
					}
				})
			 }else
				 {
					$("#gerencias_select").val("").trigger("change");	
				 }
		}
		
		function remove_gerencias(sector_id) {
			if(sector_id!==null){
			  var dr=$("#gerencias_select").val();
			  if(dr!==null)
				{			 
				 var deleted_gerencias=[];
				 dr.forEach(function(element, index2, array2)
					{
						if(sector_is_parent(sector_id,element)){
							
							deselect($("#gerencias_select"),element);
							deleted_gerencias.push(element);
							remove_agents(element);
						}
					})
				}else{
					$("#sectores_select").val("").trigger("change");	
				}
			}
			else{$("#gerencias_select").val("").trigger("change");	}
		}
		
		
		function gerencia_is_parent(id,idagent)
		{
			var flag = 0;
			Object.keys(javascript_array).forEach(function(element2, index2, array2)
			{
				if (typeof javascript_array[element2][id] != 'undefined'){   
					javascript_array[element2][id].forEach(function(element, index, array){
						if(element==idagent){ flag=1;}
					})
				}
			})
			return flag;
		}
		
		function sector_is_parent(id,idgerencia)
		{
			var flag = 0;
			//if(!empty(id)){
			if (typeof id != 'undefined'){ 
				
				Object.keys(javascript_array).forEach(function(element2, index2, array2)
				{
					if (typeof javascript_array[id][idgerencia] != 'undefined'){   
						  flag=1; 
					}
				})
				return flag;
			}else{
				Object.keys(javascript_array).forEach(function(element2, index2, array2)
				{
					if (typeof javascript_array[element2][idgerencia] != 'undefined'){   
						  flag=element2; 
					}
				})
				return flag;
			}
		}
		
		function remove_parent_sector(gerencia) {
			Object.keys(javascript_array).forEach(function(element, index, array)
			{
				if (typeof javascript_array[element][gerencia] != 'undefined'){
					deselect($("#sectores_select"),element);
				}
			})
		}
		
		function deselect($selector, idToDeselect) {
			if($selector.val() !== null) {
				new_data = $.grep($selector.val(), function (id) {
					return id != idToDeselect;
				});
				$selector.val(new_data).trigger('change');
			}
		}
		
		$('#gerencias_select').on('change',function(e){
			 
			var js_selected_asoc=[];
			agentes_before=[];
			$("#agentes_select").select2('data').forEach(function(element, index, array)
					{
						agentes_before.push(element.id);
					});
			js_selected_asoc=agentes_before;
			$("#gerencias_select").each(function()					
			{
				if($(this).val() !== null){
					$(this).val().forEach(function(element, index, array)
					{
						 Object.keys(javascript_array).forEach(function(element2, index2, array2)
						{
							if (typeof javascript_array[element2][element] != 'undefined'){
								javascript_array[element2][element].forEach(function(element3, index3, array3){
									js_selected_asoc.push(element3);
								})
							}
						})
					}); 
					//manage_sectors();
				}
				else{
					//continue					
				}
			})
			//continue
			$("#agentes_select").val(js_selected_asoc).trigger("change");
			 
			
		});
		
		$('#agentes_select').on('select2:unselect',function(e){
			var count_ag=$('#agentes_select').val(); 
			if(count_ag == null){
				$("#sectores_select").val("").trigger("change");	
				$("#gerencias_select").val("").trigger("change");	
			}
		})  
		
		
	});

	function manage_sectors(){
	var gerencia_sector=[];
	 $("#gerencias_select").each(function()					
		{
			if($(this).val() !== null){
				$(this).val().forEach(function(element, index, array)
				{
					Object.keys(javascript_array).forEach(function(element2, index2, array2)
					{
						if (typeof javascript_array[element2][element] != 'undefined'){
								if (typeof gerencia_sector[element2] == 'undefined'){
									gerencia_sector[element2]=[];
									gerencia_sector[element2].push(element)
								}else{gerencia_sector[element2].push(element);}
						}						
					}) 
				});  
			}
			else{
				//continue					
			}
			
		})
		console.log("pasa");
		console.log(gerencia_sector);	
	for( var elem in gerencia_sector){
		console.log("elem  : "+elem);
		console.log(gerencia_sector[elem]);	
		console.log(gerencia_sector[elem].length);
		if (typeof javascript_array[elem] != 'undefined'){
			console.log(gerencia_sector[elem].length==javascript_array[elem].length );
		}
		 
		/*if(typeof gerencia != 'undefined' && gerencia!= null){
			Object.keys(javascript_array).forEach(function(element2, index2, array2)
			{
				if (typeof javascript_array[element2][gerencia] != 'undefined'){   
					  var sector_lenght=javascript_array[element2].length;
					  console.log(javascript_array[element2]);//
					  if(javascript_array[element2][gerencia]==gerencia){
						  //sector_lenght
						  alert(sector_lenght);
					  }
				}
			})
		}*/
	}
	}
	
	
	function get_parent(root, struct) {
		var parent = null;
		var check = function (root, struct) {
			_.each(root, function (value, key) {
				if (value == struct) {
					parent = key;
				} else if (root == struct) {
					parent = '_root';
				} else if (typeof value === 'object') {
					check(value, struct);
				}
			});
		}
		check(root, struct);
		return parent;
	}
	function set_gerencias(sectores){
		if(sectores !== null){
			console.log(javascript_array);
			sectores.forEach(function(element, index, array)
				{
					console.log("a[" + index + "] = " + element);
					// $('#gerencias_select').val('').change();
					 $("#gerencias_select").val(element).trigger("change");
					 console.log(javascript_array[element]);
				});
		}
	};


	$('.select2').select2().on('select2:unselect', function(e) {
		//$('.select2-selection').trigger('click');
	});
	
    $('.datepicker').datepicker({
      autoclose: true, 
	  //format: 'yyyy-mm-dd',
	  format: 'dd/mm/yyyy',
	  firstDay: 1
    });
	
	$( ".preview_email" ).click(function(event) {
		event.preventDefault();
		var request = $.ajax({
			method:"post",
			url: homeurl+'/campaña/view',
			data: {body:CKEDITOR.instances['Emailcampaign_body'].getData(), id_header: $('#Emailcampaign_id_email_header').val(), id_footer: $('#Emailcampaign_id_email_footer').val() }
		}).done(function(msg) {
			$('.ligth-box-modal').css('display','block');
			$('.modal-title').html('Visualización del email');
			$('.modal-body').html(msg);
		});
	});	
	
CKEDITOR.replace( 'Emailcampaign_body', {
	language: 'es',
    /*uiColor: '#9AB8F3',*/
    toolbar: [    
    { name: 'clipboard', groups: [ 'clipboard', 'undo' ], items: [ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ] },	
	{ name: 'document', groups: [ 'mode', 'document', 'doctools' ], items: [ 'Source', '-', 'Save', 'NewPage', 'Preview', 'Print', '-', 'Templates' ] },
	{ name: 'links', items: [ 'Link', 'Unlink', 'Anchor' ] },
    { name: 'insert', items: [ 'Image', 'Flash', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe' ] },	
    { name: 'editing', groups: [ 'find', 'selection', 'spellchecker' ], items: [ 'Find', 'Replace', '-', 'SelectAll', '-', 'Scayt' ] },
    { name: 'forms', items: [ 'Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField' ] },	
    '/',
    { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat' ] },
    { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl', 'Language' ] },
    { name: 'styles', items: [ 'Styles', 'Format', 'Font', 'FontSize' ] },
    { name: 'colors', items: [ 'TextColor', 'BGColor' ] },
    { name: 'tools', items: [ 'Maximize', 'ShowBlocks' ] }    
	],  
}); 

});	


$("#mail-campaign-form" ).submit(function(event) {
	//event.preventDefault();
	// var fechaCampana = new Date(process($('#EmailCampaign_send_date').val()));
	var pasa= false;
	var fechaCampana = process($('#Emailcampaign_send_date').val()); 
	var fechaFinCampana = process($('#Emailcampaign_date_to').val()); 
	var horaCampana = $('#Emailcampaign_send_hour').val();
	var timestamp = new Date();  
	timestamp.setHours(0, 0, 0, 0);
	fechaCampana.setHours(0, 0, 0, 0); 
	fechaFinCampana.setHours(0, 0, 0, 0); 
	var horaAhora = new Date().getHours();
	horaCampana= horaCampana.split(":"); 
	/*
	if($('#Emailcampaign_name').val()==''){
			swal("Error","El nombre es obligatorio.","error");
			pasa= false;
		}
		else {
			if($('#Emailcampaign_subject').val()=='')
			{
			swal("Error","El asunto  es obligatorio.","error");
			pasa= false;
			}
			else
			{
				if(CKEDITOR.instances['Emailcampaign_body'].getData() == "")
					{
					swal("Error","El contenido de la campaña es obligatorio.","error");
					pasa= false;
					}
				else
					{
					var active = document.getElementById("Emailcampaign_enabled");
					var activetext = active.options[active.selectedIndex].text;					
					if(activetext == "Seleccione")
					{
					swal("Error","Debe marcar la campaña como Activa o Inactiva.","error");
					pasa= false;
					}
					else
						{
							 
						if($('#Emailcampaign_date_to').val()=='')
							{
								swal("Error","La fecha de Fin de campaña es obligatoria.","error");
								pasa= false;
							}
							else
								if(fechaFinCampana.valueOf() < timestamp.valueOf())
								{
									swal("Error","La fecha de Finalización de la campaña debe ser en el futuro.","error");
									pasa= false;
								}
								else
									{
										if($('#Emailcampaign_send_date').val()!='' && $('#Emailcampaign_send_hour').val()=='' || $('#Emailcampaign_send_date').val()=='' && $('#Emailcampaign_send_hour').val()!='')
										{
											swal("Error","Si selecciona la fecha de envio debe seleccionar tambien la hora.","error");
											pasa= false;
										}
										else
										{
											if(fechaFinCampana.valueOf() < fechaCampana.valueOf())
											{
												swal("Error","La fecha de Finalización de la campaña debe ser posterior al día de envío.","error");
												pasa= false;
											}
											else
											{												
												if (fechaCampana.valueOf() > timestamp.valueOf()) 
												{
													pasa= true;
												}
												else
												{
													if (fechaCampana.valueOf() < timestamp.valueOf()) 	
													{
														swal("Error","La fecha de envio no puede ser tiempo pasado.","error");
														pasa= false;
													}
													else
													{
														if ($('#Emailcampaign_send_hour').val()!='' && horaAhora > horaCampana[0] )
														{
															swal("Error","La hora de envio no puede ser tiempo pasado.","error");
															pasa= false;
														}
														else 
															{
																pasa= true;
															}
													}
												}
											}
										}
									}
							 
						}
					}
			}
		}
	
	if(pasa){$("#mail-campaign-form" )[0].submit();}*/
});
 
</script>