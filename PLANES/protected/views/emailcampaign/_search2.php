<?php
/* @var $this MailCampaignController */
/* @var $model MailCampaign */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

<div class="col-md-12" style="clear: both">
	<div class="row">
		<div class="col-md-3">
			<div class="row row_space">
		<?php echo $form->label($model2,'name'); ?>
		<?php echo $form->textField($model2,'name',array('size'=>60,'maxlength'=>255,'class'=>'form-control')); ?>
		</div>
		</div>
		<div class="col-md-3">
			<div class="row row_space">
		<?php echo $form->label($model2,'subject'); ?>
		<?php echo $form->textField($model2,'subject',array('size'=>60,'maxlength'=>255,'class'=>'form-control')); ?>
		</div>
		</div>
	</div>
	<div class="row buttons">
		<?php echo CHtml::submitButton('Buscar',array('class'=>'btn btn-default b_customize')); ?>
	</div>
<?php $this->endWidget(); ?>

</div><!-- search-form -->
</div>