<?php
/* @var $this MailCampaignController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Mail Campaigns',
);

$this->menu=array(
	array('label'=>'Create MailCampaign', 'url'=>array('create')),
	array('label'=>'Manage MailCampaign', 'url'=>array('admin')),
);
?>

<h1>Mail Campaigns</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
