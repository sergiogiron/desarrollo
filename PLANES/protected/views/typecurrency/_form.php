<?php
/* @var $this TypecurrencyController */
/* @var $model Typecurrency */
/* @var $form CActiveForm */
?>

<div class="box-body">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'typecurrency-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

<div class="col-md-12" style="clear: both">
<div class="row">
	<div class="col-md-3">
	<div class="form-group">
		<?php echo $form->labelEx($model,'title'); ?>
		<?php echo $form->textField($model,'title',array('size'=>32,'maxlength'=>32,'class'=>'form-control')); ?>
		<?php echo $form->error($model,'title'); ?>
	</div>
	</div>

	<div class="col-md-3">
	<div class="form-group">
		<?php echo $form->labelEx($model,'description'); ?>
		<?php echo $form->textField($model,'description',array('size'=>32,'maxlength'=>32,'class'=>'form-control')); ?>
		<?php echo $form->error($model,'description'); ?>
	</div>
	</div>

	<div class="col-md-3">
	<div class="form-group">
		<?php echo $form->labelEx($model,'enabled'); ?>
		<?php echo $form->checkBox($model,'enabled',array('class'=>'checkbox_form')); ?>
		<?php echo $form->error($model,'enabled'); ?>
	</div>
	</div>

	

</div>
	<div class="box-footer">
		<?php
		$title = (Yii::app()->controller->action->id!='view') ? 'Volver sin guardar cambios' : 'Volver';
		echo '<a href="'.Yii::app()->request->baseUrl.'/'.strtolower(Yii::app()->controller->id).'" class="btn btn-secundary" data-toggle="tooltip" title="'.$title.'" >Cerrar</a>';
		if(Yii::app()->controller->action->id!='view'){
			echo CHtml::submitButton('Guardar',array('class'=>'btn btn-primary','data-toggle'=>'tooltip','title'=>'Guardar cambios'));
		}
		?>
	</div>

<?php $this->endWidget(); ?>

</div>
</div><!-- form -->
	
<?php
	Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl.'/plugins/datepicker/datepicker3.css');
	Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/plugins/datepicker/bootstrap-datepicker.js',CClientScript::POS_END);
?>
<script>
$( document ).ready(function() {
	<?php if(Yii::app()->controller->action->id=='view'){ echo "$('form').find('input, textarea, button, checkbox, select').attr('disabled','disabled');"; } ?>});		
</script>
<script>
$( document ).ready(function() {
	$('.datepicker').datepicker({
	  autoclose: true, 
	  format: 'yyyy-mm-dd',
	  firstDay: 1
	});
});	
</script>
			