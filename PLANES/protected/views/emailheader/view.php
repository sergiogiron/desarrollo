<?php
/* @var $this MailHeaderController */
/* @var $model MailHeader */

 
?>
<section class="content-header">
  <h1>
	Encabezado de Email 
  </h1>
  <ol class="breadcrumb">
	<li><a href="<?php echo Yii::app()->request->baseUrl; ?>"><i class="fa fa-home"></i> Inicio</a></li>
	<li>Emailing</li>
  </ol>
</section>
<section class="content">
	<div class="box box-danger">
		<div class="box-header with-border">
			<?php $this->renderPartial('_form', array('model'=>$model)); ?>		</div>
	</div>
</section>

 