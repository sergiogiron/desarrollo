<?php
 
?>
<section class="content-header">
	<?php echo Yii::app()->user->Title(); ?>
	<?php echo Yii::app()->user->Breadcrumb(); ?>
</section>

<section class="content">
	<div class="col-md-8">
		<div class="box box-danger">
			<div class="box-header with-border">
				<?php $this->renderPartial('_form', array('model'=>$model)); ?>
			</div>
		</div>
	</div>
	
	<div class="col-md-4">
		<div class="box box-danger">
			<div class="box-header with-border">
				<h3>	Variables </h3>
				<div class="box-body no-padding">
					<table class="table table-hover">
						<tr>
						  <th>Variable</th>
						  <th>Descripción</th>                 
						</tr>
						<?php 
							$results = Yii::app()->db->createCommand()->
							  select('name, content')->
							  from('email_variables')-> 
							  where('enabled = 1')->  							  
							  queryAll();		
							  $i = 0;
							foreach ($results as $r){ ?>
							<tr>
							  <td>
							  <?php if($r["name"]=='%pf_imagen_remitente%')
							  {
								echo '<input id="name-'.$i.'" value="<img src=\''.Yii::app()->params['news_subdomain_url'].'/images/profilepictures/'.$r["name"].'\' alt=\'Imagen\' />" class="col-md-6 form-control" style="width:55%; cursor: unset!important; background-color: #eee;">';
							  }
							  else
								{
								echo '<input id="name-'.$i.'" value="'.$r["name"].'" class="col-md-6 form-control" style="width:55%; cursor: unset!important; background-color: #eee;" />';
								} ?>
							   <button id="copyButton<?php echo '-'.$i;?>" class="btn col-md-6 form-control" style="width:44%; padding: 6px 4px!important;">Copiar</button> 
							  </td>
							  <td><?php  print_r($r["content"]); ?></td>
							</tr> 
							<?php $i++; } ?>
				    </table>
					<span id="msg"></span><br>
				</div>
			</div>
		</div>
	</div>
</section>

<script>
$(document).ready(function() {
<?php $j=$i-1;?>
var var_cant =<?php echo $j.';';?>
<?php for($j;$j>=0;$j--){ ?>
document.getElementById("copyButton<?php echo '-'.$j;?>").addEventListener("click", function() {
    copyToClipboardMsg(document.getElementById("name<?php echo '-'.$j;?>"), "msg");
});
<?php } ?>

function copyToClipboardMsg(elem, msgElem) {
	  var succeed = copyToClipboard(elem);
    var msg;
    if (!succeed) {
        msg = "Funcionalidad no soportada por el navegador o bloqueada.  Presione Ctrl+c para copiar la variable."
    } else {
        msg = "Variable copiada al portapapeles."
    }
    if (typeof msgElem === "string") {
        msgElem = document.getElementById(msgElem);
    }
    msgElem.innerHTML = msg;
    setTimeout(function() {
        msgElem.innerHTML = "";
    }, 2000);
}

function copyToClipboard(elem) {
	  // create hidden text element, if it doesn't already exist
    var targetId = "_hiddenCopyText_";
    var isInput = elem.tagName === "INPUT" || elem.tagName === "TEXTAREA";
    var origSelectionStart, origSelectionEnd;
    if (isInput) {
        // can just use the original source element for the selection and copy
        target = elem;
        origSelectionStart = elem.selectionStart;
        origSelectionEnd = elem.selectionEnd;
    } else {
        // must use a temporary form element for the selection and copy
        target = document.getElementById(targetId);
        if (!target) {
            var target = document.createElement("textarea");
            target.style.position = "absolute";
            target.style.left = "-9999px";
            target.style.top = "0";
            target.id = targetId;
            document.body.appendChild(target);
        }
        target.textContent = elem.textContent;
    }
    // select the content
    var currentFocus = document.activeElement;
    target.focus();
    target.setSelectionRange(0, target.value.length);
    
    // copy the selection
    var succeed;
    try {
    	  succeed = document.execCommand("copy");
    } catch(e) {
        succeed = false;
    }
    // restore original focus
    if (currentFocus && typeof currentFocus.focus === "function") {
        currentFocus.focus();
    }
    
    if (isInput) {
        // restore prior selection
        elem.setSelectionRange(origSelectionStart, origSelectionEnd);
    } else {
        // clear temporary content
        target.textContent = "";
    }
    return succeed;
}

});
</script>