<?php
/* @var $this MailHeaderController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Mail Headers',
);

$this->menu=array(
	array('label'=>'Create MailHeader', 'url'=>array('create')),
	array('label'=>'Manage MailHeader', 'url'=>array('admin')),
);
?>

<h1>Mail Headers</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
