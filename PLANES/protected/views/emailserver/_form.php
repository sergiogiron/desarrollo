<?php
/* @var $this EmailserverController */
/* @var $model Emailserver */
/* @var $form CActiveForm */
?>
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'emailserver-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>
<div class="box-body">
	<span class="right_f">
<?php echo $form->checkBox($model,'enabled',array('class'=>'lcs_check enabled')); ?>
	
	</span>

<div class="col-md-12 col-data">
<div class="form-group col-md-6  required unique">
	<?php 
	if(!$model->isNewRecord){ $disable='disabled'; }else{ $disable='enabled'; }		
	echo $form->labelEx($model,'title'); ?>
			<?php echo $form->textField($model,'title',array($disable=>$disable,'size'=>45,'maxlength'=>45,'class'=>'form-control')); ?>
							<?php echo $form->error($model,'title'); ?>
</div>
<div class="form-group col-md-6 required">
	<?php echo $form->labelEx($model,'host_server_mail'); ?>
			<?php echo $form->textField($model,'host_server_mail',array('size'=>45,'maxlength'=>45,'class'=>'form-control')); ?>
							<?php echo $form->error($model,'host_server_mail'); ?>
</div>
<div class="form-group col-md-6 required">
	<?php echo $form->labelEx($model,'Port'); ?>
			<?php echo $form->textField($model,'Port',array('size'=>5,'maxlength'=>5,'class'=>'form-control')); ?>
							<?php echo $form->error($model,'Port'); ?>
</div>
<div class="form-group col-md-6 required">
	<?php echo $form->labelEx($model,'ssl'); ?>
			<?php echo $form->checkBox($model,'ssl'); ?>
							<?php echo $form->error($model,'ssl'); ?>
</div>
<div class="box-footer">
		<?php
		$title = (Yii::app()->controller->action->id!='view') ? 'Volver sin guardar cambios' : 'Volver';
		echo '<a href="'.Yii::app()->request->baseUrl.'/'.strtolower(Yii::app()->controller->id).'" class="btn btn-secundary" data-toggle="tooltip" title="'.$title.'" >Cerrar</a>';
		if(Yii::app()->controller->action->id!='view'){
			echo CHtml::submitButton('Guardar',array('class'=>'btn btn-primary','data-toggle'=>'tooltip','title'=>'Guardar cambios'));
		}
		?></div>
<?php $this->endWidget(); ?>

</div><!-- form -->
</div>
<?php
	Yii::app()->clientScript->registerCssFile(Yii::app()->params['cdnUrl'].'/plugins/lcswich/lc_switch-tts.css'); 
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/lcswich/lc_switch.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/lcswich/lc_switch-tts.js',CClientScript::POS_END);

echo '<script>viewPage = "'.Yii::app()->controller->action->id.'";</script>';
Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/js/form-validators.js',CClientScript::POS_END) ?> 
<script>
$( document ).ready(function() {
	<?php if(Yii::app()->controller->action->id=='view'){ echo "$('form').find('input, textarea, button, checkbox, select').attr('disabled','disabled');"; } ?>});		
</script>