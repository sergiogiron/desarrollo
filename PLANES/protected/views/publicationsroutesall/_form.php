<?php
/* @var $this PublicationsroutesallController */
/* @var $model Publicationsroutesall */
/* @var $form CActiveForm */
?>

<div class="box-body">
	<div class="col-md-12 col-data">
	<?php $form=$this->beginWidget('CActiveForm', array(
		'id'=>'publicationsroutesall-form',
		// Please note: When you enable ajax validation, make sure the corresponding
		// controller action is handling ajax validation correctly.
		// There is a call to performAjaxValidation() commented in generated controller code.
		// See class documentation of CActiveForm for details on this.
		'enableAjaxValidation'=>false,
	)); ?>


		<div class="col-xs-6 form-group required">
			<?php echo $form->labelEx($model,'name'); ?>
			<?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>128,'class'=>'form-control')); ?>
			<?php echo $form->error($model,'name'); ?>
		</div>

		<div class="col-xs-6 form-group required">
			<?php echo $form->labelEx($model,'url'); ?>
			<?php echo $form->textField($model,'url',array('size'=>60,'maxlength'=>256,'class'=>'form-control')); ?>
			<?php echo $form->error($model,'url'); ?>
		</div>

		<div class="box-footer">
		<?php
			$title = (Yii::app()->controller->action->id!='view') ? 'Volver sin guardar cambios' : 'Volver';
			echo '<a href="'.Yii::app()->request->baseUrl.'/'.strtolower(Yii::app()->controller->id).'" class="btn btn-secundary" data-toggle="tooltip" title="'.$title.'" >Cerrar</a>';
			if(Yii::app()->controller->action->id!='view'){
				echo CHtml::submitButton('Guardar',array('class'=>'btn btn-primary','data-toggle'=>'tooltip','title'=>'Guardar cambios'));
			}
		?>
	</div>

	<?php $this->endWidget(); ?>
	</div>
</div><!-- form -->
<script type="text/javascript">
	<?php
		if(Yii::app()->controller->action->id=='view'){ echo "$('form').find('input, textarea, button, checkbox, select').attr('disabled','disabled');"; }
	?>

</script>