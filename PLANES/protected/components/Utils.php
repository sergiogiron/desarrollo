<?php
class Utils{
	public static function Status($status){
		switch ($status) {
			case 1:
				echo "Creada";
				break;
			case 2:
				echo "En proceso";
				break;
			case 3:
				echo "Finalizada";
				break;
			case 4:
				echo "Error";
				break;
		}
	}

	public static function limpia_espacios($cadena){
		$cadena = str_replace(' ', '', $cadena);
		return $cadena;
	}
	public static function SearchObject(){
		return '<div class="col-md-4 search_bar">
			<div class="row">
				<div class="input-group input-group-sm">
					<input type="text" class="form-control" id="criterio"><i class="fa fa-close right-icon-search"></i>
					<span class="input-group-btn">
					  <button type="button" id="buscar" class="btn btn-default btn-flat"><i class="fa fa-search"></i></button>
					</span>
				</div>
			</div>
		</div>';
	}

	public static function vaciarcacheimages($carpeta){
		foreach(glob($carpeta . "/*") as $archivos_carpeta){
			if (is_dir($archivos_carpeta)){
				self::vaciarcacheimages($archivos_carpeta);
			}else{
				unlink($archivos_carpeta);
			}
		}
		if(file_exists($carpeta)){
			//rmdir($carpeta);
		}
	}

	public static function vaciarcacheprodimages($carpeta){
		foreach(glob($carpeta . "/*") as $archivos_carpeta){
			if (is_dir($archivos_carpeta)){
				self::vaciarcacheimages($archivos_carpeta);
			}else{
				unlink($archivos_carpeta);
			}
		}
		if(file_exists($carpeta)){
			//rmdir($carpeta);
		}
	}

	public static function mover_temp ($id_menu,$id_user, $file_name) {
		copy('uploads/'.$id_menu.'/'.$id_user.'/'.$file_name.'/'.$file_name.'.jpg','uploads/tmp/'.Yii::app()->user->id.'/'.$file_name.'/'.$file_name.'.jpg');
	}

	public static function mover_directorio ($id_user, $file_name) {
		if(!is_dir('uploads/'.Yii::app()->controller->id)){
			mkdir('uploads/'.Yii::app()->controller->id, 0777);
		}
		if(!is_dir('uploads/'.Yii::app()->controller->id.'/'.$id_user)){
			mkdir('uploads/'.Yii::app()->controller->id.'/'.$id_user, 0777);
		}
		if(!is_dir('uploads/'.Yii::app()->controller->id.'/'.$id_user.'/'.$file_name)){
			mkdir('uploads/'.Yii::app()->controller->id.'/'.$id_user.'/'.$file_name, 0777);
		}
		$carpeta='uploads/tmp/'.Yii::app()->user->id.'/'.$file_name.'/';
		if(is_dir($carpeta)){
			if($dir = opendir($carpeta)){
				while(($archivo = readdir($dir)) !== false){
					if($archivo != '.' && $archivo != '..' && $archivo != '.htaccess'){
						copy('uploads/tmp/'.Yii::app()->user->id.'/'.$file_name.'/'.$archivo,'uploads/'.Yii::app()->controller->id.'/'.$id_user.'/'.$file_name.'/'.$archivo);
					}
				}
				closedir($dir);
			}
		}
	}

	public static function mover_directorio_prod ($id_user, $file_name) {
		if(!is_dir('uploads/tmp')){
			mkdir('uploads/tmp/', 0777);
		}

		if(!is_dir('uploads/tmp/'.Yii::app()->user->id)){
			mkdir('uploads/tmp/'.Yii::app()->user->id, 0777);
		}

		if(!is_dir('uploads/tmp/'.Yii::app()->user->id.'/'.$file_name)){
			mkdir('uploads/tmp/'.Yii::app()->user->id.'/'.$file_name, 0777);
		}
		$carpeta='uploads/'.Yii::app()->controller->id.'/'.$id_user.'/'.$file_name.'/';
		if(is_dir($carpeta)){
			if($dir = opendir($carpeta)){
				while(($archivo = readdir($dir)) !== false){
					if($archivo != '.' && $archivo != '..' && $archivo != '.htaccess'){
						copy('uploads/'.Yii::app()->controller->id.'/'.$id_user.'/'.$file_name.'/'.$archivo,'uploads/tmp/'.Yii::app()->user->id.'/'.$file_name.'/'.$archivo);
					}
				}
				closedir($dir);
			}
		}
	}

	public static function icon_action($deal){
        if ($deal == 1){
            $class = '<i class="fa fa-gift icon_center" aria-hidden="true"></i>';
        }elseif ($deal == 2){
            $class = '<i class="fa fa-thumbs-o-up icon_center" aria-hidden="true"></i>';
        }else{
            $class = '<i class="fa fa-close icon_center" aria-hidden="true"></i>';
        }
        return $class;
    }

	public static function Active($active){
		switch ($active) {
			case 0:
				echo "Inactiva";
				break;
			case 1:
				echo "Activa";
				break;
		}
	}

	public static function is__null($data){
		if($data->id_subchannel==null){
			return '';
		}else{
			$hijos_subchannel=Subchannels::model()->findBypk($data->id_subchannel);
			return $hijos_subchannel->title;
		}
	}

	public static function datetime_spa($date){
		if (!empty($date)) {
			$date = DateTime::createFromFormat('Y-m-d H:i:s', $date);
			$dateformat = $date->format('d/m/Y H:i:s');
		} else {
			$dateformat = 0;
		}
		return $dateformat;

	}

	public static function date_spa($date){
		if (!empty($date)) {
			$date=date_create_from_format("Y-m-d",$date);
			$dateformat = date_format($date,"d/m/Y");
		} else {
			$dateformat = 0;
		}
		return $dateformat;
	}

	public static function date_spa_r($date){
		if (!empty($date)) {
			$date=date_create_from_format("d/m/Y",$date);
			$dateformat = date_format($date,"Y-m-d");
		} else {
			$dateformat = 0;
		}
		return $dateformat;		
	}
	public static function cero_return_null($data){
		if($data==0){
			return null;
		}else{
			return $data;
		}
	}
	
	public static function datetime_date_spa($date){
		if (!empty($date)) {
			$date=DateTime::createFromFormat('Y-m-d H:i:s', $date);
			$dateformat = $date->format('d/m/Y');
		} else {
			$dateformat = 0;
		}
		return $dateformat;
	}


	public static function datetime_spa_format($date){
		//para fechas del tipo yyyy-MM-ddTHH:mm:ssZ
		if (!empty($date)) {
			$date=new DateTime($date);
			$dateformat = $date->format('d/m/Y H:i:s');
		} else {
			$dateformat = 0;
		}
		return $dateformat;
	}


	public static function activeSwitch($data)
	{
		if($data->enabled == 1){
			$checked = ' checked="checked"';
		}else{
			$checked = '';
		}

		$model = ucfirst(Yii::app()->controller->id);

		return '<input class="lcs_check enabledMini" name="'.$model.'[enabled]" id="'.$model.'_enabled" value="'.$data->enabled.'"'.$checked.' type="checkbox" data-model="'.Yii::app()->controller->id.'" data-id="'.$data->id.'">';
	}

	public static function generateActiveField($modelClass,$column)
	{
		$size=0;
		if(isset($column->size)){
			$size=$column->size;
		}
		if($column->type==='boolean'){
			return "\$form->checkBox(\$model,'{$column->name}')";
		}elseif(stripos($column->dbType,'datetime')!==false || stripos($column->dbType,'date')!==false){
			$inputField='textField';
				return "\$form->{$inputField}(\$model,'{$column->name}',array('class'=>'form-control datepicker','data-inputmask'=>'\'alias\': \'dd/mm/yyyy\'','data-mask'=>'data-mask'))";
		}elseif(stripos($column->dbType,'longtext')!==false){
			$inputField='textArea';
				return "\$form->{$inputField}(\$model,'{$column->name}',array('class'=>'form-control ckeditor','rows'=>6,'cols'=>6))";
		}elseif($column->name=='enabled'){
			$inputField='checkBox';
			return "\$form->{$inputField}(\$model,'{$column->name}',array('class'=>'lcs_check enabled'))";
		}elseif($size==80){
			$inputField='EmailField';
			$maxLength=80;
			return "\$form->{$inputField}(\$model,'{$column->name}',array('size'=>$size,'maxlength'=>$maxLength,'class'=>'form-control'))";
		}elseif($size==62){
			$inputField='textField';
			$maxLength=62;
			return "\$form->{$inputField}(\$model,'{$column->name}',array('size'=>$size,'maxlength'=>$maxLength,'class'=>'form-control letras'))";
		}elseif($size==260){
			$inputField='textField';
			$maxLength=260;
			return "\$form->{$inputField}(\$model,'{$column->name}',array('size'=>$size,'maxlength'=>$maxLength,'placeholder'=>'http://www.ttsoffice.com','class'=>'form-control'))";
		}elseif($size==520){
			$inputField='textArea';
			$maxLength=520;
			return "\$form->{$inputField}(\$model,'{$column->name}',array('class'=>'form-control'))";
		}elseif($size==65){
			$inputField='textField';
			$maxLength=65;
			return "\$form->{$inputField}(\$model,'{$column->name}',array('size'=>$size,'maxlength'=>$maxLength,'class'=>'form-control letras_numeros'))";
		}elseif($size==35){
			$inputField='textField';
			$maxLength=35;
			return "\$form->{$inputField}(\$model,'{$column->name}',array('size'=>$size,'maxlength'=>$maxLength,'class'=>'form-control phone'))";
		}else{
			if(preg_match('/^(password|pass|passwd|passcode)$/i',$column->name))
				$inputField='passwordField';
			else
				$inputField='textField';

			if($column->type!=='string' || $column->size===null)
				return "\$form->{$inputField}(\$model,'{$column->name}',array('class'=>'form-control'))";
			else
			{
				if(($size=$maxLength=$column->size)>60)
					$size=60;
				return "\$form->{$inputField}(\$model,'{$column->name}',array('size'=>$size,'maxlength'=>$maxLength,'class'=>'form-control'))";
			}
		}
	}

	public static function getPaymentFull(){
			$bancos = self::getPaymentEnt();
			$tarjetas = self::getPaymentTdc();

			$payment = array();
			$tdcAll = array();
			$bancoTdcAll = array();

			//añado los todas las tarjetas a UN solo banco (esto es en caso de navieras y aerolineas que acpeta todas las tarjetas)
			for ($i=0; $i <sizeof($bancos) ; $i++) {
				if(empty($bancos[$i]["tarjeta_banco"])){
					$bancoTdcAll[$i] = $bancos[$i];
					unset($bancos[$i]);
				}
			}
			if(isset($bancoTdcAll)){
				$bancos = array_values($bancos);
				$bancoTdcAll = array_values($bancoTdcAll);
				$nombreTdc = array();
				$new_banco_tarjeta = array();
				foreach ($tarjetas as $key => $value) {
					$nombreTdc[$key] = $value['title'];
				}
				$nombreTdc = array_values(array_unique($nombreTdc));

				for ($i=0; $i <sizeof($bancoTdcAll) ; $i++) {
					array_push($new_banco_tarjeta,self::addBancoToTarjeta($bancoTdcAll[$i],$nombreTdc));
					for ($j=0; $j <sizeof($new_banco_tarjeta[$i]) ; $j++) {
						array_push($bancos,$new_banco_tarjeta[$i][$j]);
					}
				}
			}

			//finnish
			//añado las tarjetas aptas para todos los bancos
			//start

			if(isset($tdcAll)){
				$nombreBanco = array();
				$new_banco = array();
				$tdcAll = array_values($tdcAll);
				foreach ($bancos as $key => $value) {
					$nombreBanco[$key] = $value['title'];
				}
				$nombreBanco = array_values(array_unique($nombreBanco));
				for ($i=0; $i <sizeof($tdcAll) ; $i++) {
					array_push($new_banco,self::addTarjetaToBanco($tdcAll[$i],$nombreBanco));
					for ($j=0; $j <sizeof($new_banco[$i]) ; $j++) {

						array_push($bancos,$new_banco[$i][$j]);
					}
				}
			}
			//finnish


			//si la tdc tiene mejor financiacion que el banco, uso la de la tdc
			#start
			for ($i=0; $i <sizeof($tarjetas) ; $i++) {
				for ($j=0; $j <sizeof($bancos) ; $j++) {
					if($tarjetas[$i]['tarjeta_banco'] == $bancos[$j]['title'] and $tarjetas[$i]['cuota'] == $bancos[$j]['cuota']){
						$bancos[$j] = self::compareTarjetaToBanco($tarjetas[$i],$bancos[$j]);
					}
				}
			}
			#finnish

			foreach ($bancos as $key => $value) {
				$payment[$key]['entity'] = $value['title'];
				$payment[$key]['credit_cards'] = $value['tarjeta_banco'];
				$payment[$key]['subchannel_son'] = $value['subchannel_hijo'];
				$payment[$key]['subchannel_father'] = $value['subchannel_padre'];
				$payment[$key]['product'] = $value['producto'];
				$payment[$key]['provider'] = $value['proveedor'];
				$payment[$key]['dues'] = $value['cuota'];
				$payment[$key]['interest'] = $value['interes'];
				$payment[$key]['cftn'] = $value['costo_financiero'];
				$payment[$key]['posnet_cost'] = $value['porcentaje_posnet'];
				$payment[$key]['date_validity_start'] = $value['vigencia_desde'];
				$payment[$key]['date_validity_end'] = $value['vigencia_hasta'];
				$payment[$key]['rewards'] = $value['rewards'];
			}

			#Kyllä, kirjoitin lopuksi kaksi s, koska suomalainen on mahtava
			return $payment;
	}

	public static function getPaymentEnt(){
		$cruceros = self::getPaymentEntByCruises();

		$query = "SELECT distinct cc.id as id, pe.title as title, cc.name as tarjeta_banco, pef.amount_dues as cuota,
							pef.interests as interes, pef.cftna as costo_financiero,
							pef.posnet_cost as porcentaje_posnet,pef.date_validity_end as vigencia_hasta,
							pef.date_validity_start as vigencia_desde, s.title as subchannel_hijo, sp.title as subchannel_padre,p.title as producto, null as proveedor
							, pef.rewards as rewards
							from payments_entity pe
							join payments_entity_account_type peat on peat.id_entity = pe.id
							join payments_entity_financing_relational pefr on pefr.id_payments_entity_account_type = peat.id
							join payments_entity_financing pef on pef.id = pefr.id_payments_entity_financing
							left join credit_cards cc on cc.id = peat.id_credit_cards
							left join subchannels_entity se on se.id_entity = pef.id_entity
							left join subchannels s on s.id = se.id_subchannel
							left join subchannels sp on sp.id = s.id_subchannel
							left join payments_entity_financing_services pefs on pefs.id_payments_entity_financing = pef.id
							left join products p on p.id = pefs.id_product
							where
							CURDATE() >= pe.expire_date_from and CURDATE() <= pe.expire_date_to and
							CURDATE() >= pef.date_validity_start and CURDATE() <= pef.date_validity_end and
							pe.enabled = 1 and
							cc.id is not null
							order by cc.id, pe.title, pef.amount_dues";
		$results = Yii::app()->db->createCommand($query)->query();
		$results = CJSON::encode($results);
		$results = json_decode($results,true);

		foreach ($results as $key => $result) {
			$array = self::splitCuotasByComma($result);
			if(strpos($result['cuota'],',')!=false){
				unset($results[$key]);
				for ($i=0; $i <sizeof($array) ; $i++) {
					array_push($results,$array[$i]);
				}
			}
			else if(strpos($result['cuota'],'-')!=false){
				$array = self::splitCuotasByHyphen($result);
				unset($results[$key]);
				for ($i=0; $i <sizeof($array) ; $i++) {
					array_push($results,$array[$i]);
				}

			}


		}
		$results = array_values($results);

		//añado las entidades de cruceros a el resto de las entidades
		foreach ($cruceros as $key => $value) {
			array_push($results,$value);
		}

		return $results;

	}
	
	public static function normalize_date($date){
		return substr($date,6,2).'/'.substr($date,4,2).'/'.substr($date,0,4).' '.substr($date,8,2).':'.substr($date,10,2);	
	}
	
	public static function getPaymentEntByCruises(){
		$query = "SELECT distinct cc.id as id, pe.title as title, cc.name as tarjeta_banco, pecs.amount_dues as cuota, pecs.interests as interes, pecs.cftna as costo_financiero,
							pecs.posnet_cost as porcentaje_posnet,pecs.date_validity_end as vigencia_hasta, pecs.date_validity_start as vigencia_desde,
							s.title as subchannel_hijo, sp.title as subchannel_padre, 'Cruceros' as producto, cs.code as proveedor, pecs.rewards as rewards
							from payments_entity pe
							join payments_entity_cruises_supplier pecs on pe.id = pecs.id_entity
							join payments_entity_cruises_supplier_relational pecsr on pecsr.id_payments_entity_cruises_supplier = pecs.id
							join cruises_supplier cs on cs.id = pecsr.id_cruises_supplier
							left join payments_entity_cruises_supplier_account_type pecsat on pecsat.id_payments_entity_cruises_supplier = pecsr.id
							left join payments_entity_account_type peat on pecsat.id_payments_entity_account_type = peat.id
							left join credit_cards cc on cc.id = peat.id_credit_cards
							left join subchannels_entity se on se.id_entity = pe.id
							left join subchannels s on s.id = se.id_subchannel
							left join subchannels sp on sp.id = s.id_subchannel
							where
							CURDATE() >= pe.expire_date_from and CURDATE() <= pe.expire_date_to and
							CURDATE() >= pecs.date_validity_start and CURDATE() <= pecs.date_validity_end and
							pe.enabled = 1
							order by  pe.title, pecs.amount_dues";
		$results = Yii::app()->db->createCommand($query)->query();
		$results = CJSON::encode($results);
		$results = json_decode($results,true);

		foreach ($results as $key => $result) {
			if(strpos($result['cuota'],',')!=false){
				$array = self::splitCuotasByComma($result);
				unset($results[$key]);
				for ($i=0; $i <sizeof($array) ; $i++) {
					array_push($results,$array[$i]);
				}
			}
			else if(strpos($result['cuota'],'-')!=false){
				$array = self::splitCuotasByHyphen($result);
				unset($results[$key]);
				for ($i=0; $i <sizeof($array) ; $i++) {
					array_push($results,$array[$i]);
				}

			}


		}
		$results = array_values($results);
		return $results;
	}

	public static function getPaymentTdc(){
			$cruceros = self::getPaymentTdcByCruises();
			$tdcall = self::getPaymentTdcNull();

			$query = "SELECT distinct cc.id as id, cc.name as title, pe.title as tarjeta_banco,
								ccf.amount_dues as cuota, ccf.interests as interes, ccf.cftna as costo_financiero,
								ccf.posnet_cost as porcentaje_posnet, ccf.date_validity_end as vigencia_hasta,
								ccf.date_validity_start as vigencia_desde, s.title as subchannel_hijo, sp.title as subchannel_padre,
								pr.title as producto,null as proveedor, ccf.rewards as rewards
								from credit_cards cc
								join credit_cards_financing ccf on cc.id = ccf.id_credit_cards
								left join credit_cards_financing_services ccfs on ccfs.id_credit_cards_financing = ccf.id
								left join products pr on pr.id = ccfs.id_product
								left join subchannels_credit_cards scc on scc.id_creditcard = cc.id
								left join subchannels s on s.id = scc.id_subchannel
								left join subchannels sp on sp.id = s.id_subchannel
								left join credit_cards_entity_relational ccer on ccer.id_credit_cards_financing = ccf.id
								left join payments_entity pe on pe.id = ccer.id_entity
								where
								CURDATE() >= ccf.date_validity_start and CURDATE() <= ccf.date_validity_end and
								cc.enabled = 1
								and
								s.title is not null
								order by ccf.amount_dues";
			$results = Yii::app()->db->createCommand($query)->query();
			$results = CJSON::encode($results);
			$results = json_decode($results,true);

			foreach ($results as $key => $result) {
				if(strpos($result['cuota'],',')!=false){
					$array = self::splitCuotasByComma($result);
					unset($results[$key]);
					for ($i=0; $i <sizeof($array) ; $i++) {
						array_push($results,$array[$i]);
					}
				}
				else if(strpos($result['cuota'],'-')!=false){
					$array = self::splitCuotasByHyphen($result);
					unset($results[$key]);
					for ($i=0; $i <sizeof($array) ; $i++) {
						array_push($results,$array[$i]);
					}

				}


			}
			$results = array_values($results);
		foreach ($cruceros as $key => $value) {
			array_push($results,$value);
		}
		foreach ($tdcall as $key => $value) {
			array_push($results,$value);
		}
		return $results;
	}

	public static function getPaymentTdcByCruises(){
			$query = "SELECT distinct cc.id as id, cc.name as title, case when bank = '' then null end as tarjeta_banco,
								cccs.amount_dues as cuota, cccs.interests as interes, cccs.cftna as costo_financiero,
								cccs.posnet_cost as porcentaje_posnet, cccs.date_validity_end as vigencia_hasta,
								cccs.date_validity_start as vigencia_desde, s.title as subchannel_hijo, sp.title as subchannel_padre,
								'Cruceros' as producto ,cs.code as proveedor, cccs.rewards as rewards
								from credit_cards cc
								join credit_cards_cruises_supplier cccs on cccs.id_credit_cards = cc.id
								join credit_cards_cruises_supplier_relational cccsr on cccsr.id_credit_cards_cruises_supplier = cccs.id
								join cruises_supplier cs on cs.id = cccsr.id_cruises_supplier
								left join payments_entity pe on pe.title = cccs.bank
								left join subchannels_entity se on se.id_entity = pe.id
								left join subchannels s on s.id = se.id_subchannel
								left join subchannels sp on sp.id = s.id_subchannel
								where
								CURDATE() >= cccs.date_validity_start and CURDATE() <= cccs.date_validity_end and
								cc.enabled = 1
								order by cccs.amount_dues";
			$results = Yii::app()->db->createCommand($query)->query();
			$results = CJSON::encode($results);
			$results = json_decode($results,true);

			foreach ($results as $key => $result) {
				if(strpos($result['cuota'],',')!=false){
					$array = self::splitCuotasByComma($result);
					unset($results[$key]);
					for ($i=0; $i <sizeof($array) ; $i++) {
						array_push($results,$array[$i]);
					}
				}
				else if(strpos($result['cuota'],'-')!=false){
					$array = self::splitCuotasByHyphen($result);
					unset($results[$key]);
					for ($i=0; $i <sizeof($array) ; $i++) {
						array_push($results,$array[$i]);
					}

				}


			}
			$results = array_values($results);
		return $results;
	}

	public static function getPaymentTdcNull(){
			$query = "SELECT distinct cc.id as id, cc.name as title, pe.title as tarjeta_banco,
								ccf.amount_dues as cuota, ccf.interests as interes, ccf.cftna as costo_financiero,
								ccf.posnet_cost as porcentaje_posnet, ccf.date_validity_end as vigencia_hasta,
								ccf.date_validity_start as vigencia_desde, s.title as subchannel_hijo, sp.title as subchannel_padre,
								pr.title as producto,null as proveedor, ccf.rewards as rewards
								from credit_cards cc
								join credit_cards_financing ccf on cc.id = ccf.id_credit_cards
								left join credit_cards_financing_services ccfs on ccfs.id_credit_cards_financing = ccf.id
								left join products pr on pr.id = ccfs.id_product
								left join subchannels_credit_cards scc on scc.id_creditcard = cc.id
								left join subchannels s on s.id = scc.id_subchannel
								left join subchannels sp on sp.id = s.id_subchannel
								left join credit_cards_entity_relational ccer on ccer.id_credit_cards_financing = ccf.id
								left join payments_entity pe on pe.id = ccer.id_entity
								where
								CURDATE() >= ccf.date_validity_start and CURDATE() <= ccf.date_validity_end and
								cc.enabled = 1
								and
								s.title is null
								order by ccf.amount_dues";
			$results = Yii::app()->db->createCommand($query)->query();
			$results = CJSON::encode($results);
			$results = json_decode($results,true);

			foreach ($results as $key => $result) {
				if(strpos($result['cuota'],',')!=false){
					$array = self::splitCuotasByComma($result);
					unset($results[$key]);
					for ($i=0; $i <sizeof($array) ; $i++) {
						array_push($results,$array[$i]);
					}
				}
				else if(strpos($result['cuota'],'-')!=false){
					$array = self::splitCuotasByHyphen($result);
					unset($results[$key]);
					for ($i=0; $i <sizeof($array) ; $i++) {
						array_push($results,$array[$i]);
					}

				}


			}
			$results = array_values($results);
		return $results;
	}

	public static	 function splitCuotasByComma($array){

		$exp = explode(',',$array['cuota']);
		$result = array();
		foreach ($exp as $e => $explode) {
			$result[$e]['id'] = $array['id'];
			$result[$e]['title'] = $array['title'];
			$result[$e]['tarjeta_banco'] = $array['tarjeta_banco'];
			$result[$e]['subchannel_hijo'] = $array['subchannel_hijo'];
			$result[$e]['subchannel_padre'] = $array['subchannel_padre'];
			$result[$e]['proveedor'] = $array['proveedor'];
			$result[$e]['producto'] = $array['producto'];
			$result[$e]['cuota'] = $explode;
			$result[$e]['interes'] = $array['interes'];
			$result[$e]['costo_financiero'] = $array['costo_financiero'];
			$result[$e]['porcentaje_posnet'] = $array['porcentaje_posnet'];
			$result[$e]['rewards'] = $array['rewards'];
			$result[$e]['vigencia_hasta'] = $array['vigencia_hasta'];
			$result[$e]['vigencia_desde'] = $array['vigencia_desde'];
		}
		return $result;

	}

	public static	 function splitCuotasByHyphen($array){
		$exp = explode('-',$array['cuota']);
		$cont = 0;
		for ($i=$exp[0]; $i <=$exp[1] ; $i++) {
			$cont++;
			$result[$cont]['id'] = $array['id'];
			$result[$cont]['title'] = $array['title'];
			$result[$cont]['tarjeta_banco'] = $array['tarjeta_banco'];
			$result[$cont]['subchannel_hijo'] = $array['subchannel_hijo'];
			$result[$cont]['subchannel_padre'] = $array['subchannel_padre'];
			$result[$cont]['proveedor'] = $array['proveedor'];
			$result[$cont]['producto'] = $array['producto'];
			$result[$cont]['cuota'] = $i;
			$result[$cont]['interes'] = $array['interes'];
			$result[$cont]['costo_financiero'] = $array['costo_financiero'];
			$result[$cont]['porcentaje_posnet'] = $array['porcentaje_posnet'];
			$result[$cont]['rewards'] = $array['rewards'];
			$result[$cont]['vigencia_hasta'] = $array['vigencia_hasta'];
			$result[$cont]['vigencia_desde'] = $array['vigencia_desde'];
		}
		$result = array_values($result);
		return $result;
	}


		public static function compareTarjetaToBanco($tarjeta,$banco){

			if($tarjeta['interes'] < $banco['interes']){
					$banco['interes'] = $tarjeta['interes'];
			}
			if($tarjeta['costo_financiero'] < $banco['costo_financiero']){
					$banco['porcentaje_posnet'] = $tarjeta['porcentaje_posnet'];
			}
			return $banco;
		}

		public static function addTarjetaToBanco($tarjeta, $nombreBanco){
			$new_bancos = array();
			// $tmp = array();
			for ($i=0; $i <sizeof($nombreBanco) ; $i++) {
				$new_bancos[$i]['id'] = $tarjeta['id'];
				$new_bancos[$i]['title'] = $nombreBanco[$i];
				$new_bancos[$i]['tarjeta_banco'] = $tarjeta['title'];
				$new_bancos[$i]['subchannel_hijo'] = $tarjeta['subchannel_hijo'];
				$new_bancos[$i]['subchannel_padre'] = $tarjeta['subchannel_padre'];
				$new_bancos[$i]['proveedor'] = $tarjeta['proveedor'];
				$new_bancos[$i]['producto'] = $tarjeta['producto'];
				$new_bancos[$i]['cuota'] = $tarjeta['cuota'];
				$new_bancos[$i]['interes'] = $tarjeta['interes'];
				$new_bancos[$i]['rewards'] = $tarjeta['rewards'];
				$new_bancos[$i]['costo_financiero'] = $tarjeta['costo_financiero'];
				$new_bancos[$i]['porcentaje_posnet'] = $tarjeta['porcentaje_posnet'];
				$new_bancos[$i]['vigencia_hasta'] = $tarjeta['vigencia_hasta'];
				$new_bancos[$i]['vigencia_desde'] = $tarjeta['vigencia_desde'];
			}

			return $new_bancos;
		}

		public static function addBancoToTarjeta($banco, $nombreTdc){
			$new_bancos = array();
			// $tmp = array();
			for ($i=0; $i <sizeof($nombreTdc) ; $i++) {
				$new_bancos[$i]['id'] = $banco['id'];
				$new_bancos[$i]['title'] = $banco['title'];
				$new_bancos[$i]['tarjeta_banco'] = $nombreTdc[$i] ;
				$new_bancos[$i]['subchannel_hijo'] = $banco['subchannel_hijo'];
				$new_bancos[$i]['subchannel_padre'] = $banco['subchannel_padre'];
				$new_bancos[$i]['proveedor'] = $banco['proveedor'];
				$new_bancos[$i]['producto'] = $banco['producto'];
				$new_bancos[$i]['cuota'] = $banco['cuota'];
				$new_bancos[$i]['interes'] = $banco['interes'];
				$new_bancos[$i]['costo_financiero'] = $banco['costo_financiero'];
				$new_bancos[$i]['porcentaje_posnet'] = $banco['porcentaje_posnet'];
				$new_bancos[$i]['rewards'] = $banco['rewards'];
				$new_bancos[$i]['vigencia_hasta'] = $banco['vigencia_hasta'];
				$new_bancos[$i]['vigencia_desde'] = $banco['vigencia_desde'];
			}

			return $new_bancos;
		}

		public static function getMaxCuotas(){
			$url = Yii::app()->params['appServer'].'/services/api/getmaxcuotas';

			$curl = curl_init();

			curl_setopt_array($curl, array(
			  CURLOPT_URL => $url,
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_ENCODING => "",
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 30,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  CURLOPT_CUSTOMREQUEST => "GET",
			  CURLOPT_HTTPHEADER => array(
			    "cache-control: no-cache",
			  ),
			));

			$response = curl_exec($curl);

			$max_cuotas = json_decode($response,true);
			return $max_cuotas;

		}

	public static function esBlock($id,$origin=[],$unblock=false){
		$user = Users::model()->findByPk(Yii::app()->user->id);
		$body=[];
		$curl = curl_init();
		curl_setopt_array($curl, array(
		  CURLOPT_PORT => "9200",
		  CURLOPT_URL => Yii::app()->params['elasticUrl'].'/blocked/'.$origin[0].'/'.$id,
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => 'GET',
		 ));
		$response = curl_exec($curl);
		$err = curl_error($curl);
		curl_close($curl);
		if ($err) {
		  return "cURL Error #:" . $err;
		  die();
		} else {
		  $respuesta = json_decode($response,true);
		}
		if(!$unblock){
			

			if(isset($respuesta['found']) && $respuesta['found']==1){
				if($respuesta['_source']['user']==Yii::app()->user->id){
					return true;
				}else{
					//comparar tiempo
					return false;
				}
			}else{
				$body['id'] = $id;
				$body['user'] = Yii::app()->user->id;
				$body['date'] = time();
				//$body['origin'] = $origin;
				$curl = curl_init();
				curl_setopt_array($curl, array(
				  CURLOPT_PORT => "9200",
				  CURLOPT_URL => Yii::app()->params['elasticUrl'].'/blocked/'.$origin[0].'/'.$id,
				  CURLOPT_RETURNTRANSFER => true,
				  CURLOPT_ENCODING => "",
				  CURLOPT_MAXREDIRS => 10,
				  CURLOPT_TIMEOUT => 30,
				  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				  CURLOPT_CUSTOMREQUEST => 'PUT',
				  CURLOPT_POSTFIELDS => json_encode($body)
				));

				$response = curl_exec($curl);
				$err = curl_error($curl);

				curl_close($curl);

				if($err){
					return 'curl error #:'.$err;
				}else{
					$body = array(
							'booking_code'=>$id,
							'type'=>'Bloqueado',
							'comment'=>'',
							'date'=>date('d/m/Y H:i:s'),
							'user'=>$user->name." ".$user->lastname);
					$result = self::esInteract($id,'PUT',$origin,$body);
					return true;
				}
			}
		}else{
			if($respuesta['_source']['user']==Yii::app()->user->id || Yii::app()->user->checkAccess('freeblockedtask')){
				$curl = curl_init();
				curl_setopt_array($curl, array(
				  CURLOPT_PORT => "9200",
				  CURLOPT_URL => Yii::app()->params['elasticUrl'].'/blocked/'.$origin[0].'/'.$id,
				  CURLOPT_RETURNTRANSFER => true,
				  CURLOPT_ENCODING => "",
				  CURLOPT_MAXREDIRS => 10,
				  CURLOPT_TIMEOUT => 30,
				  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				  CURLOPT_CUSTOMREQUEST => 'DELETE',
				 ));
				$response = curl_exec($curl);
				$err = curl_error($curl);
				curl_close($curl);
				if ($err) {
				  return "cURL Error #:" . $err;
				  die();
				} else {
				  	$respuesta = json_decode($response,true);
				  	if(isset($respuesta['found']) && $respuesta['found']==1){
						$body = array(
								'booking_code'=>$id,
								'type'=>'Desbloqueado',
								'comment'=>'',
								'date'=>date('d/m/Y H:i:s'),
								'user'=>$user->name." ".$user->lastname);
						$result = self::esInteract($id,'PUT',$origin,$body);
						return true;
					}
				}
			}
		}
	}

	public static function esInteract($id,$type,$origin=[],$bodyBody = null){
		//origin tiene que ser un array con 3 elementos
		// - $origin[0] = indiceEs
		// - $origin[1] = docType
		// - $origin[2] = nombre identificador
		$curl = curl_init();
		curl_setopt_array($curl, array(
		  CURLOPT_PORT => "9200",
		  CURLOPT_URL => Yii::app()->params['elasticUrl'].'/'.$origin[0].'/'.$origin[1].'/'.$id,
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => 'GET',
		 ));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
		  return "cURL Error #:" . $err;
		  die();
		} else {
		  $respuesta = json_decode($response,true);
		}


		if($type=='GET'){
			return $respuesta['_source']['Historial'];
		}else{
			$body = array();
			$cuerpoBody = array();
			if(isset($respuesta['found']) && $respuesta['found']!=''){
				$cuerpoBody = $respuesta['_source']['Historial'];
				array_push($cuerpoBody, $bodyBody);

			}else{
				$cuerpoBody[0] = $bodyBody;
			}

			$body[$origin[2]] 	= $id;
			$body['Historial'] 	= $cuerpoBody;

		

			$curl = curl_init();
			curl_setopt_array($curl, array(
			  CURLOPT_PORT => "9200",
			  CURLOPT_URL => Yii::app()->params['elasticUrl'].'/'.$origin[0].'/'.$origin[1].'/'.$id,
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_ENCODING => "",
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 30,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  CURLOPT_CUSTOMREQUEST => 'PUT',
			  CURLOPT_POSTFIELDS => json_encode($body)
			));
			$response = curl_exec($curl);
			$err = curl_error($curl);

			curl_close($curl);

			if ($err) {
			  return "cURL Error #:" . $err;
			  die();
			} else {
			  $respuesta = json_decode($response,true);
			  return $respuesta;
			}
		}
	}

	public static function esUserHaveBlockedTask($origin=[]){
		$curl = curl_init();
		$body= '{
				  "query": {
				    "bool": {
				      "must": [
				        {
				          "match": {
				            "user": "'.Yii::app()->user->id.'"
				          }
				        }
				      ]
				    }
				  }
				}';
		curl_setopt_array($curl, array(
		  CURLOPT_PORT => "9200",
		  CURLOPT_URL => Yii::app()->params['elasticUrl'].'/blocked/'.$origin[0].'/_search',
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => 'POST',
		  CURLOPT_POSTFIELDS => $body
		 ));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
		  return "cURL Error #:" . $err;
		  die();
		} else {
		  $respuesta = json_decode($response,true);
		}
		if(isset($respuesta['hits']) && isset($respuesta['hits']['total']) && $respuesta['hits']['total'] >= 1){
			if(isset($respuesta['hits']['hits'][0]['_id'])){
				return $respuesta['hits']['hits'][0]['_id'];
			}
		}else{
			return false;
		}

	}
	

		/************/
		 
		public static function getFileData($filepath)
		{
			$json = file_get_contents($filepath); 

			if ($json) {
				$decode = json_decode($json, true); //decode json content as array
				$data = self::getJsonData($decode);		//iterates over json and returns 'data' key
				return $data;
			} else {
				return false;
			}
		}
		public static function getJsonData($array)
		{
			if (is_array($array)) {
				foreach ($array as $key => $value) {
						if ($key == 'data') {
							return $value;
						}
				}
				return false;
			} else {
				return false;
			}
		}
		public static function datediff($strStart,$strEnd){
			$dteStart = new DateTime($strStart); 
			$dteEnd   = new DateTime($strEnd); 	
			$dteDiff  = $dteStart->diff($dteEnd); 
			return $dteDiff->format("%H:%I:%S"); 			
		}
		
		public static function parteHora( $hora ){
			$horaSplit = explode(":", $hora);
				if( count($horaSplit) < 3 ){
					$horaSplit[2] = 0;
				}
			return $horaSplit;
		}	
		
		public static function SumaHoras( $time1, $time2 ){
			list($hour1, $min1, $sec1) = self::parteHora($time1);
			list($hour2, $min2, $sec2) = self::parteHora($time2);
			return date('H:i:s', mktime( $hour1 + $hour2, $min1 + $min2, $sec1 + $sec2));
		}		
		public static function getVigencia($arrayItem)
		{
			date_default_timezone_set('America/Argentina/Buenos_Aires');
			$currentDate = date_create(date('Y-m-d'));

			if (isset($arrayItem['activo_desde']) && isset($arrayItem['activo_hasta'])) {
				$desde = date_create($arrayItem['activo_desde']);
				$hasta = date_create($arrayItem['activo_hasta']);
			} elseif (isset($arrayItem['active_from']) && isset($arrayItem['active_to'])) {
				//fix for flight_deal.json structure
				$d = explode("/", $arrayItem['active_from']);
				$h = explode("/", $arrayItem['active_to']);
				$desde = date_create($d[2] . "-" . $d[1] . "-" . $d[0]);
				$hasta = date_create($h[2] . "-" . $h[1] . "-" . $h[0]);
			}

			$activo = date_diff($desde, $currentDate);
			$interval = date_diff($currentDate, $hasta);
			$valido = date_diff($desde,$hasta);

			if ($activo->format("%r%a") >= 0) {
				if ($interval->format("%r%a") >= 0) {
					if ($valido->format("%r%a") >= 0){
						return $interval->format("%r%a");
					} else {
						return -1;
					}
				} else {
					return -1;
				}
			} else {
				return -1;
			}
		}
		
		public static function formatPrice($value){
			return number_format($value, 0, ',', '.');
		}		
		
		public static function getFinanciacion($bucle=array('financing','airliners','cruisessupplier'),$getProductServices=false){
		 
		  $finish=array();
		  $filename = Yii::app()->params['cdnUrl'].'/json/entity_cotizador.json';
		  $data = Utils::getFileData($filename);
		  
		  $filename = Yii::app()->params['cdnUrl'].'/json/credit_cards.json';
		  $data2 = Utils::getFileData($filename);
		  $array_finish=array();
		  for($h=0;$h<count($data);$h++){
			$array_generic=array();
			if((count($data[$h]['financing'])>0 and $data[$h]['enabled']=1) or (count($data[$h]['airliners'])>0 and $data[$h]['enabled']=1) or (count($data[$h]['cruisessupplier'])>0 and $data[$h]['enabled']=1)){
			 // $bucle=array('financing','airliners','cruisessupplier');
			  foreach($bucle as $b){
				 
				for($i=0;$i<count($data[$h][$b]);$i++){
					  if(Utils::getVigencia(array('active_from'=>$data[$h][$b][$i]['date_validity_start'],'active_to'=>$data[$h][$b][$i]['date_validity_end']))>=0){
						 if(count($data[$h]['financing'])>0 && (count($data[$h][$b][$i]['services'])==0 || $getProductServices)){	
							if(strpos($data[$h][$b][$i]['amount_dues'], '-')){
							  $pieces = explode("-", $data[$h][$b][$i]['amount_dues']);
							  rsort($pieces);
							  /////////////for de las pieces creando cuotsas
							  $data[$h][$b][$i]['amount_dues_max']=$pieces[0];
							  sort($pieces);	
							  $pieces2=[];
							  for($var_i=0, $var_j=$pieces[0] ; $var_j<$pieces[1]+1;$var_i++,$var_j++){
								   if( $var_j >= $pieces[1]+1 )
									  break;
								  $pieces2[$var_i]=$var_j;
							  }
							  $data[$h][$b][$i]['amount_dues']=$pieces2;
							}elseif(strpos($data[$h][$b][$i]['amount_dues'], ',')){
							  $pieces = explode(",", $data[$h][$b][$i]['amount_dues']);
							  rsort($pieces);
							  $data[$h][$b][$i]['amount_dues_max']=$pieces[0];
							  $data[$h][$b][$i]['amount_dues']=$pieces;
							}else{
							  $data[$h][$b][$i]['amount_dues_max']=$data[$h][$b][$i]['amount_dues'];
							  $data[$h][$b][$i]['amount_dues']=$data[$h][$b][$i]['amount_dues'];
							}
							$c_f=array();
							$count_financing='';
							if(count($data[$h][$b][$i]['account_type'])==0){
							  $count_financing='todas las tarjetas ';
							  $c_f[$b]=$count_financing;
							}else{
							  foreach($data[$h][$b][$i]['account_type'] as $fin){
								$count_financing.=$fin['title'].',';
							  }
							  $c_f[$b]=$count_financing;
							}
							$data[$h][$b][$i]['text_2']='Para clientes con '.substr($c_f[$b],0,-1);
							if($b=='financing'){
							  $c_f=array();
							  $count_financing='';
							  if(count($data[$h][$b][$i]['services'])==0){
								$count_financing='todos los productos ';
								$c_f[$b]=$count_financing;
							  }elseif(count($data[$h][$b][$i]['services'])>1){
								$count_financing='productos seleccionados';
								$c_f[$b]=$count_financing;
							  }else{
								foreach($data[$h][$b][$i]['services'] as $fin){
								  $count_financing.=$fin['title'].',';
								}
								$c_f[$b]=substr($count_financing,0,-1);
							  }
							  $data[$h][$b][$i]['text_1']='en '.$c_f[$b];
							}elseif($b=='airliners'){
							  $c_f=array();
							  $count_financing='';
							  if(count($data[$h][$b][$i]['airliners'])==0){
								$count_financing='todas las aerol&iacute;neas ';
								$c_f[$b]=$count_financing;
							  }else{
								foreach($data[$h][$b][$i]['airliners'] as $fin){
								  $count_financing.=$fin['title'].',';
								}
								$c_f[$b]=substr($count_financing,0,-1);
							  }
							  $data[$h][$b][$i]['text_1']='con '.$c_f[$b];
							}else{
							  $c_f=array();
							  $count_financing='';
							  if(count($data[$h][$b][$i]['cruisessupplier'])==0){
								$count_financing='todas las navieras ';
								$c_f[$b]=$count_financing;
							  }else{
								foreach($data[$h][$b][$i]['cruisessupplier'] as $fin){
								  $count_financing.=$fin['title'].',';
								}
								$c_f[$b]=substr($count_financing,0,-1);
							  }
							  $data[$h][$b][$i]['text_1']='con '.$c_f[$b];
							}
							$date_new=date_create_from_format('d/m/Y', $data[$h][$b][$i]['date_validity_end']);
							$data[$h][$b][$i]['date_validity_end_max']= strtotime(date_format($date_new, 'Y-m-d'));
							$array_generic[]=$data[$h][$b][$i];
					}
				  }
				}
			  }
			  if(count($array_generic)>0){
				$amount_dues_max=array();
				$date_validity_end=array();
				foreach ($array_generic as $key => $row) {
				  $amount_dues_max[$key]  = $row['amount_dues_max'];
				  $date_new=date_create_from_format('d/m/Y', $row['date_validity_end']);
				  $date_validity_end[$key] = strtotime(date_format($date_new, 'Y-m-d'));
				}
				array_multisort($amount_dues_max, SORT_DESC, $date_validity_end, SORT_DESC, $array_generic);
				$array_finish[]=array('entity_highlight'=>$data[$h]['entity_highlight'],'creditcards_highlight'=>0,'title'=>$data[$h]['title'],'banners'=>$data[$h]['banners'],'amount_dues_max_f'=>$array_generic[0]['amount_dues_max'],'date_validity_end_max'=>$array_generic[0]['date_validity_end_max'],'expire_date_from'=>$data[$h]['expire_date_from'],'expire_date_to'=>$data[$h]['expire_date_to'],'banners'=>$data[$h]['banners'],'imagenes'=>$data[$h]['imagenes'],'entity_highlight'=>$data[$h]['entity_highlight'],'data'=>$array_generic);
			  }
			}
		  }
		  for($h=0;$h<count($data2);$h++){
			$array_generic=array();
			if((count($data2[$h]['financing'])>0 and $data2[$h]['enabled']=1) or (count($data2[$h]['airliners'])>0 and $data2[$h]['enabled']=1) or (count($data2[$h]['cruisessupplier'])>0 and $data2[$h]['enabled']=1)){
			 // $bucle=array('financing','airliners','cruisessupplier');
			  foreach($bucle as $b){
				for($i=0;$i<count($data2[$h][$b]);$i++){
				  if(Utils::getVigencia(array('active_from'=>$data2[$h][$b][$i]['date_validity_start'],'active_to'=>$data2[$h][$b][$i]['date_validity_end']))>=0){
					if(strpos($data2[$h][$b][$i]['amount_dues'], '-')){
					  $pieces = explode("-", $data2[$h][$b][$i]['amount_dues']);
					  rsort($pieces);
					  $data2[$h][$b][$i]['amount_dues_max']=$pieces[0];
					  sort($pieces);	
					  $pieces2=[];
					  for($var_i=0, $var_j=$pieces[0] ; $var_j<$pieces[1]+1;$var_i++,$var_j++){
						   if( $var_j >= $pieces[1]+1 )
							  break;
						  $pieces2[$var_i]=$var_j;
					  }
					  $data2[$h][$b][$i]['amount_dues']=$pieces2;
					}elseif(strpos($data2[$h][$b][$i]['amount_dues'], ',')){
					  $pieces = explode(",", $data2[$h][$b][$i]['amount_dues']);
					  rsort($pieces);
					  $data2[$h][$b][$i]['amount_dues_max']=$pieces[0];
					   $data2[$h][$b][$i]['amount_dues']=$pieces;
					}else{
					  $data2[$h][$b][$i]['amount_dues_max']=$data2[$h][$b][$i]['amount_dues'];
					  $data2[$h][$b][$i]['amount_dues']=$data2[$h][$b][$i]['amount_dues'];
					}
					$data[$h][$b][$i]['text_2']='';
					if($b=='financing'){
					  $c_f=array();
					  $count_financing='';
					  if(count($data2[$h][$b][$i]['services'])==0){
						$count_financing='todos los productos ';
						$c_f[$b]=$count_financing;
					  }elseif(count($data2[$h][$b][$i]['services'])>1){
						$count_financing='productos seleccionados';
						$c_f[$b]=$count_financing;
					  }else{
						foreach($data2[$h][$b][$i]['services'] as $fin){
						  $count_financing.=$fin['title'].',';
						}
						$c_f[$b]=substr($count_financing,0,-1);
					  }
					  $data2[$h][$b][$i]['text_1']='en '.$c_f[$b];
					}elseif($b=='airliners'){
					  if($data2[$h][$b][$i]['bank']==''){
						$data2[$h][$b][$i]['text_2']='Todos los bancos ';
					  }else{
						$data2[$h][$b][$i]['text_2']=$data2[$h][$b][$i]['bank'];
					  }
					  $c_f=array();
					  $count_financing='';
					  if(count($data2[$h][$b][$i]['airliners'])==0){
						$count_financing='todas las aerolineas ';
						$c_f[$b]=$count_financing;
					  }else{
						foreach($data2[$h][$b][$i]['airliners'] as $fin){
						  $count_financing.=$fin['title'].',';
						}
						$c_f[$b]=substr($count_financing,0,-1);
					  }
					  $data2[$h][$b][$i]['text_1']='con '.$c_f[$b];
					}else{
					  if($data2[$h][$b][$i]['bank']==''){
						$data2[$h][$b][$i]['text_2']='Todos los bancos ';
					  }else{
						$data2[$h][$b][$i]['text_2']=$data2[$h][$b][$i]['bank'];
					  }
					  $c_f=array();
					  $count_financing='';
					  if(count($data2[$h][$b][$i]['cruisessupplier'])==0){
						$count_financing='todas las navieras ';
						$c_f[$b]=$count_financing;
					  }else{
						foreach($data2[$h][$b][$i]['cruisessupplier'] as $fin){
						  $count_financing.=$fin['title'].',';
						}
						$c_f[$b]=substr($count_financing,0,-1);
					  }
					  $data2[$h][$b][$i]['text_1']='con '.$c_f[$b];
					}
					$date_new=date_create_from_format('d/m/Y', $data2[$h][$b][$i]['date_validity_end']);
					$data2[$h][$b][$i]['date_validity_end_max']= strtotime(date_format($date_new, 'Y-m-d'));
					$array_generic[]=$data2[$h][$b][$i];
				  }
				}
			  }
			  if(count($array_generic)>0){
				$amount_dues_max=array();
				$date_validity_end=array();
				foreach ($array_generic as $key => $row) {
				  $amount_dues_max[$key]  = $row['amount_dues_max'];
				  $date_new=date_create_from_format('d/m/Y', $row['date_validity_end']);
				  $date_validity_end[$key] = strtotime(date_format($date_new, 'Y-m-d'));
				}
				array_multisort($amount_dues_max, SORT_DESC, $date_validity_end, SORT_DESC, $array_generic);
				$array_finish[]=array('creditcards_highlight'=>$data2[$h]['creditcards_highlight'],'entity_highlight'=>0,'title'=>$data2[$h]['title'],'brand'=>$data2[$h]['brand'],'amount_dues_max_f'=>$array_generic[0]['amount_dues_max'],'date_validity_end_max'=>$array_generic[0]['date_validity_end_max'],'imagenes'=>$data2[$h]['imagenes'],'data'=>$array_generic);
			  }
			}
		  }
		  $amount_dues_max=array();
		  $date_validity_end=array();
		  $entity_highlight=array();
		  $creditcards_highlight=array();
		  foreach ($array_finish as $key => $row) {
			$amount_dues_max[$key]  = $row['amount_dues_max_f'];
			$date_validity_end[$key] =  $row['date_validity_end_max'];
			$entity_highlight[$key] =  $row['entity_highlight'];
			$creditcards_highlight[$key] =  $row['creditcards_highlight'];
		  }
		  array_multisort($entity_highlight, SORT_DESC,$creditcards_highlight, SORT_DESC,$amount_dues_max, SORT_DESC, $date_validity_end, SORT_DESC, $array_finish);
		  return $array_finish;
		}
	
	/***************************************/

		public static function getMaxDue($dues)
		{
			$dues = str_replace(['-',','], '|', $dues);
			$dues = explode('|', $dues);
			return array_pop ($dues);
		}

	
	
	
}
