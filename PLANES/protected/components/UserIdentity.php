<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
	private $_id;

	/**
	 * Authenticates a user.
	 * @return boolean whether authentication succeeds.
	 */
	public function authenticate($client=null)
	{
		$user=Users::model()->find("username='$this->username' and enabled=1");
		if($client=='sabre'){
			$this->_id=$user->id;
			$this->username=$user->name.' '.$user->lastname;
			$this->errorCode=self::ERROR_NONE;				
		}else{
			if($user===null){	
				$this->errorCode=self::ERROR_USERNAME_INVALID;
			}else if(md5($this->password)==$user->password){
				$this->_id=$user->id;
				$this->username=$user->name.' '.$user->lastname;
				$this->errorCode=self::ERROR_NONE;			
			}else{
				$this->errorCode=self::ERROR_PASSWORD_INVALID;
			}			
		}
		return $this->errorCode==self::ERROR_NONE;
	}
	
	public function getId()
	{
		return $this->_id;
	}
}