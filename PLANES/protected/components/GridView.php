<?php
Yii::import('zii.widgets.grid.CGridView');
class GridView extends CGridView
{
    public function run()       
    {
        $this->registerClientScript();  
        echo CHtml::openTag($this->tagName,$this->htmlOptions)."\n";

        $total=$this->dataProvider->getTotalItemCount();//added line
        echo '<input type=hidden value="'.$total.'" class="totalRows"/> ';//added line

        $this->renderContent();
        $this->renderKeys();

        echo CHtml::closeTag($this->tagName);
    }
        
}
?>