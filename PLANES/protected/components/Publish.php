<?php
class Publish{
	public static function Run($entornos,$table,$channel,$subchannels,$date,$hour,$content,$images){
		$date_complete = date_create_from_format('d/m/Y', $date);
		$publication_date=date_format($date_complete,'Y-m-d').' '.$hour.':00';
		//publica en el log
		if(count($subchannels)>0){
			foreach($subchannels as $subcanal){
				$publications=new Publications;
				$publications->id_user=Yii::app()->user->id;
				$publications->created_at=date("Y-m-d H:i:s");
				$publications->content=json_encode($content);
				$publications->model=$table;
				$publications->id_channel=$channel;
				$publications->id_subchannel=$subcanal;
				$publications->publication_date	=$publication_date;
				$publications->save();
			}
		}else{
			$publications=new Publications;
			$publications->id_user=Yii::app()->user->id;
			$publications->created_at=date("Y-m-d H:i:s");
			$publications->content=json_encode($content);
			$publications->model=$table;
			//$publications->id_channel=$channel;
			//$publications->id_subchannel=$subcanal;
			$publications->publication_date	=$publication_date;
			$publications->save();
		}
		//publica en entornos
		foreach($entornos as $entorno){
			if($entorno=='site'){
				return self::Site($table,$channel,$subchannels,$content,$publication_date,$images);
			}elseif($entorno=='cdn'){
				return self::Cdn($table,$content,$publication_date,$images);
			}elseif($entorno=='app'){
				return self::App($table,$channel,$subchannels,$content,$publication_date);
			}elseif($entorno=='appjson'){
				return self::appJson($table,$channel,$subchannels,$content,$publication_date);
			}
		}
	}
	public static function Site($table,$channel,$subchannels,$content,$publication_date,$images){
		$subchannel_folder='';
		foreach($subchannels as $subcanal){
			$hijos_subchannel=Subchannels::model()->findBypk($subcanal);
			$channel_folder=$hijos_subchannel->canal->title;
			$subchannel_folder=$hijos_subchannel->title;
			if($hijos_subchannel->id_subchannel!=NULL){
				$children_subchannel=$hijos_subchannel->id_subchannel;
				$children_subchannel_folder=$hijos_subchannel->subcanal->title;
			}else{
				$children_subchannel_folder=$subchannel_folder;
				$children_subchannel=$subcanal;
			}
			$final=array("name"=>$table,"images"=>$images,"publication_date"=>$publication_date,"data"=>$content,"channel"=>$channel,"children_subchannel_folder"=>$children_subchannel_folder,"children_subchannel"=>$children_subchannel,"channel_folder"=>$channel_folder,"subchannel"=>$subcanal,"subchannel_folder"=>$subchannel_folder);
			$opciones = array(
			  'http'=>array(
				'method'=>'POST',
				'content'=>CJSON::encode($final),
				'ignore_errors' => true,
				'header'=>'Content-Type: application/json'
			  )
			);
			$contexto = stream_context_create($opciones);
			$endpoint=Publicationroutes::model()->findByAttributes(array('id_channel'=>$channel,'id_subchannel'=>$subcanal));
			/*  si no encuentra informacion sigue la ejecución	*/
			echo $fichero = @file_get_contents($endpoint->url,false, $contexto);
		}
	}

	public static function Cdn($table,$content,$publication_date,$images){
		$final=array("name"=>$table,"images"=>$images,"publication_date"=>$publication_date,"data"=>$content);
		$opciones = array(
		  'http'=>array(
			'method'=>'POST',
			'content'=>CJSON::encode($final),
			'ignore_errors' => true,
			'header'=>'Content-Type: application/json'
		  )
		);
		$contexto = stream_context_create($opciones);
		$endpoint=Publicationsroutesall::model()->findByPk(2);
		echo $fichero = @file_get_contents($endpoint->url,false, $contexto);
	}

	public static function convertDate($date)
	{
		$arr = explode('/', $date);
		if($arr){
			return $arr[2].'-'.$arr[1].'-'.$arr[0];
		}else{
			return $date;
		}
	}
	public static function App($table,$channel,$subchannels,$content,$publication_date){
		if(count($content)>0){
			$subchannel_folder='';
			if(count($subchannels)>0){
				foreach($subchannels as $subcanal){
					$hijos_subchannel=Subchannels::model()->findBypk($subcanal);
					$channel_folder=$hijos_subchannel->canal->title;
					$subchannel_folder=$hijos_subchannel->title;
					if($hijos_subchannel->id_subchannel!=NULL){
						$children_subchannel=$hijos_subchannel->id_subchannel;
						$children_subchannel_folder=$hijos_subchannel->subcanal->title;
					}else{
						$children_subchannel_folder=$subchannel_folder;
						$children_subchannel=$subcanal;
					}
					$model_name = get_class($content[0]);
					$model_n=new $model_name;
					$content_new = array();
					$arr_tmp = array();
					$columnas = $model_n->getMetadata()->columns;
					foreach ($content as $key =>$value) {
						$arr = $value->getAttributes();
						foreach($arr as $key2 => $value2){
							if($value2!==NULL){
								if($columnas[$key2]->dbType == 'date'){
									$value2 = self::convertDate($value2);
								}
								$arr_tmp[$key2] = $value2;
							}
						}
						array_push($content_new, $arr_tmp);
					}

					$final=array("name"=>$table,"publication_date"=>$publication_date,"data"=>$content_new,"channel"=>$channel,"channel_folder"=>$channel_folder,"subchannel"=>$subcanal,"subchannel_folder"=>$subchannel_folder);
						$opciones = array(
							'http'=>array(
							'method'=>'POST',
							'content'=>CJSON::encode($final),
							'ignore_errors' => true,
							'header'=>'Content-Type: application/json'
					  )
					);

					$contexto = stream_context_create($opciones);
					$endpoint=Publicationsroutesall::model()->findBypk(1);
					echo $fichero = @file_get_contents($endpoint->url,false, $contexto);
				}
			}else{
				$model_name = get_class($content[0]);
				$model_n=new $model_name;
				$content_new = array();
				$arr_tmp = array();
				$columnas = $model_n->getMetadata()->columns;

				foreach ($content as $key =>$value) {
					$arr = $value->getAttributes();
					foreach($arr as $key2 => $value2){
						if($value2!==NULL){
							if($columnas[$key2]->dbType == 'date'){
								$value2 = self::convertDate($value2);
							}
						}
						$arr_tmp[$key2] = $value2;
					}
					array_push($content_new, $arr_tmp);
				}

				$final=array("name"=>$table,"publication_date"=>$publication_date,"data"=>$content_new);
					$opciones = array(
						'http'=>array(
						'method'=>'POST',
						'content'=>CJSON::encode($final),
						'ignore_errors' => true,
						'header'=>'Content-Type: application/json'
				  )
				);

				$contexto = stream_context_create($opciones);
				$endpoint=Publicationsroutesall::model()->findBypk(1);
				echo $fichero = @file_get_contents($endpoint->url,false, $contexto);
			}
		}

	}
	public static function appJson($table,$channel,$subchannels,$content,$publication_date){
		$subchannel_folder='';
		if(count($subchannels)>0){
			foreach($subchannels as $subcanal){
				$hijos_subchannel=Subchannels::model()->findBypk($subcanal);
				$channel_folder=$hijos_subchannel->canal->title;
				$subchannel_folder=$hijos_subchannel->title;
				if($hijos_subchannel->id_subchannel!=NULL){
					$children_subchannel=$hijos_subchannel->id_subchannel;
					$children_subchannel_folder=$hijos_subchannel->subcanal->title;
				}else{
					$children_subchannel_folder=$subchannel_folder;
					$children_subchannel=$subcanal;
				}
				$final=array("name"=>$table,"images"=>$images,"publication_date"=>$publication_date,"data"=>$content,"channel"=>$channel,"children_subchannel_folder"=>$children_subchannel_folder,"children_subchannel"=>$children_subchannel,"channel_folder"=>$channel_folder,"subchannel"=>$subcanal,"subchannel_folder"=>$subchannel_folder);
				$opciones = array(
				  'http'=>array(
					'method'=>'POST',
					'content'=>CJSON::encode($final),
					'ignore_errors' => true,
					'header'=>'Content-Type: application/json'
				  )
				);
				$contexto = stream_context_create($opciones);
				$endpoint=Publicationroutes::model()->findByAttributes(array('id_channel'=>$channel,'id_subchannel'=>$subcanal));
				/*  si no encuentra informacion sigue la ejecución	*/
				echo $fichero = @file_get_contents($endpoint->url,false, $contexto);
			}
		}else{
			$final=array("name"=>$table,"publication_date"=>$publication_date,"data"=>$content);
				$opciones = array(
					'http'=>array(
					'method'=>'POST',
					'content'=>CJSON::encode($final),
					'ignore_errors' => true,
					'header'=>'Content-Type: application/json'
				)
			);

			$contexto = stream_context_create($opciones);
			$endpoint=Publicationsroutesall::model()->findBypk(1);
			echo $fichero = @file_get_contents($endpoint->url,false, $contexto);
		}

	}
}
//$publication_site=Publish:Run(array('site','app','cdn'),'search',$_POST['channel'],$_POST['subchannel'],$_POST['date'],$_POST['hour'],$content,"images"=>array("img"));
?>
