<?php

/**
 * This is the model class for table "user_services".
 *
 * The followings are the available columns in table 'user_services':
 * @property  public $criterio;
 * @property integer $id
 * @property string $user
 * @property string $pass
 * @property string $mail
 * @property string $app
 * @property string $date_from
 * @property string $date_to
 * @property integer $expire_date
 * @property integer $enabled
 * @property string $created_at
 * @property integer $id_user
 *
 * The followings are the available model relations:
 * @property ApiKey[] $apiKeys
 * @property ApiUser[] $apiUsers
 */
class Userservices extends CActiveRecord
{
	public $criterio;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'user_services';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			/*array('date_from, date_to', 'date', 'format'=>'dd/mm/yyyy hh:mm:ss', 'message'=>'{attribute}: no es una fecha'),*/
					
			array('user, pass, mail, app, created_at, id_user', 'required'),
			array('expire_date, enabled, id_user', 'numerical', 'integerOnly'=>true),
			array('user', 'length', 'max'=>30),
			array('pass', 'length', 'max'=>128),
			array('mail', 'length', 'max'=>254),
			array('app', 'length', 'max'=>45),
			array('date_from, date_to', 'safe'),
					// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, user, pass, mail, app, date_from, date_to, expire_date, enabled, created_at, id_user', 'safe', 'on'=>'search'),
		);
	}

	public function behaviors(){
		return array(
			// Classname => path to Class
			'ActiveRecordLogableBehavior'=>
				'application.behaviors.ActiveRecordLogableBehavior',
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'apiKeys' => array(self::HAS_MANY, 'ApiKey', 'id_user_services'),
			'apiUsers' => array(self::HAS_MANY, 'ApiUser', 'id_user_services'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'user' => 'Usuario',
			'pass' => 'Password',
			'mail' => 'Email',
			'app' => 'App',
			'date_from' => 'Vigencia desde',
			'date_to' => 'Vigencia hasta',
			'expire_date' => 'Valida expiracion',
			'enabled' => 'Activo',
			'created_at' => 'Created At',
			'id_user' => 'Id User',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		
		if($this->criterio!=''){
			$criteria->addSearchCondition('id',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('user',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('pass',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('mail',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('app',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('date_from',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('date_to',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('expire_date',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('enabled',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('created_at',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('id_user',$this->criterio,true,'OR', 'LIKE');
	
		}else{	
			$criteria->compare('id',$this->id);
			$criteria->compare('user',$this->user,true);
			$criteria->compare('pass',$this->pass,true);
			$criteria->compare('mail',$this->mail,true);
			$criteria->compare('app',$this->app,true);
			$criteria->compare('date_from',$this->date_from,true);
			$criteria->compare('date_to',$this->date_to,true);
			$criteria->compare('expire_date',$this->expire_date);
			$criteria->compare('enabled',$this->enabled);
			$criteria->compare('created_at',$this->created_at,true);
			$criteria->compare('id_user',$this->id_user);
		}
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	/*
	protected function beforeValidate(){
						
						
						if($this->date_from!='' && $_POST['date_from_hour']!=''){
			$this->date_from = date_create_from_format("d/m/Y H:i:s",$this->date_from.' '.$_POST['date_from_hour'].':00');
			$this->date_from =date_format( $this->date_from,"d/m/Y H:i:s");
		}
				
						if($this->date_to!='' && $_POST['date_to_hour']!=''){
			$this->date_to = date_create_from_format("d/m/Y H:i:s",$this->date_to.' '.$_POST['date_to_hour'].':00');
			$this->date_to =date_format( $this->date_to,"d/m/Y H:i:s");
		}
				
						
						
						
						
				return parent::beforeValidate();
    }	
	
	public static function normalize_dates($model){
		$model->criterio=array();
					
					
					
					
					
						$date_from= explode(" ",$model->date_from);
		$model->date_from=$date_from[0];
		$model->criterio=array_merge($model->criterio,array('date_from_hour'=>$date_from[1]));
			
						$date_to= explode(" ",$model->date_to);
		$model->date_to=$date_to[0];
		$model->criterio=array_merge($model->criterio,array('date_to_hour'=>$date_to[1]));
			
					
					
					
					
				return $model;		
	}
	
	protected function afterFind() {
						
						
				$this->date_from =Utils::datetime_spa($this->date_from);
					
						
				$this->date_to =Utils::datetime_spa($this->date_to);
				
						
		return parent::afterFind();
	}	
	
	protected function beforeSave ()
    {	
		$this->created_at=date("Y-m-d H:i:s");																																					$date_date_from = DateTime::createFromFormat('d/m/Y H:i:s', $this->date_from);
		$this->date_from=$date_date_from->format('Y-m-d H:i:s');
										$date_date_to = DateTime::createFromFormat('d/m/Y H:i:s', $this->date_to);
		$this->date_to=$date_date_to->format('Y-m-d H:i:s');
																														return parent::beforeSave ();
    }	
	*/
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Userservices the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
