<?php

/**
 * This is the model class for table "subchannels".
 *
 * The followings are the available columns in table 'subchannels':
 * @property  public $criterio;
 * @property integer $id
 * @property integer $id_channel
 * @property integer $id_subchannel
 * @property string $title
 * @property string $url
 * @property string $title_seo
 * @property string $tracking_description
 * @property string $tracking_analitycs
 * @property string $legals
 * @property string $phone_description
 * @property string $office_hours
 * @property string $phone
 * @property integer $reward
 * @property string $filename
 * @property string $ext
 * @property integer $enabled
 * @property string $created_at
 * @property integer $id_user
 *
 * The followings are the available model relations:
 * @property ExcludeRules[] $excludeRules
 * @property HighlightProducts[] $highlightProducts
 * @property Search[] $searches
 */
class Subchannels extends CActiveRecord
{
	public $criterio;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'subchannels';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
				array('url', 'url', 'message'=>'{attribute}: no es una url válida'),
				array('id_channel,title', 'uniqueuni', 'attributeName'=>'id_channel,title'),
				array('enabled', 'statusvalidate', 'attributeName'=>'enabled'),
				array('promotional_url_before', 'validatedatebefore', 'attributeName'=>'enabled'),
				array('promotional_url_after', 'validatedateafter', 'attributeName'=>'enabled'),
				array('timeout', 'timeout', 'attributeName'=>'timeout'),
				array('id_channel,title, title_seo, url,timeout,tracking_description,tracking_analitycs', 'required','message'=>'Debe completar este campo.'),
				array('id_channel, id_subchannel, reward, promotional_site, enabled, id_user,timeout', 'numerical', 'integerOnly'=>true),
				array('title, title_seo, office_hours', 'length', 'max'=>50),
				array('title_seo', 'length', 'max'=>140),
				array('promotional_before_start_date,promotional_before_end_date,promotional_after_start_date,promotional_after_end_date', 'length', 'max'=>10),
				array('url,promotional_url_after,promotional_url_before', 'length', 'max'=>140),
				array('tracking_description', 'length', 'max'=>1024),
				array('phone_description', 'length', 'max'=>32),
				array('phone', 'length', 'max'=>25),
				array('filename', 'length', 'max'=>64),
				array('ext', 'length', 'max'=>12),
				array('legals,script', 'safe'),
					// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, id_channel,tracking_analitycs, id_subchannel, title, url, title_seo, tracking_description, tracking_analitycs, legals, phone_description, office_hours, phone, reward, filename, ext, enabled, created_at, id_user', 'safe', 'on'=>'search'),
		);
	}

	public function behaviors(){
		return array(
			// Classname => path to Class
			'ActiveRecordLogableBehavior'=>
				'application.behaviors.ActiveRecordLogableBehavior',
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'imagenes' => array(self::HAS_MANY, 'Subchannelsimages', 'id_subchannel', 'order'=>'imagenes.order ASC'),
			'entidades' => array(self::HAS_MANY, 'Subchannelsentity', 'id_subchannel'),
			'excludeRules' => array(self::HAS_MANY, 'ExcludeRules', 'id_subchannel'),
			'highlightProducts' => array(self::HAS_MANY, 'HighlightProducts', 'id_subchannel'),
			'searches' => array(self::HAS_MANY, 'Search', 'id_subchannel'),
			'canal' => array(self::BELONGS_TO, 'Channels', 'id_channel'),
			'subcanal' => array(self::BELONGS_TO, 'Subchannels', 'id_subchannel'),
			'subcanals' => array(self::BELONGS_TO, 'Subchannels', 'id'),
			'creditcards' => array(self::HAS_MANY, 'Subchannelscreditcards', 'id_subchannel'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_channel' => 'Canal',
			'id_subchannel' => 'Subcanal padre',
			'title' => 'Subcanal',
			'timeout' => 'Tiempo de espera',
			'url' => 'URL',
			'title_seo' => 'Título seo',
			'tracking_description' => 'Descripción SEO',
			'tracking_analitycs' => 'Header',
			'script' => 'Script',
			'legals' => 'Legales',
			'phone_description' => 'Texto Teléfono',
			'office_hours' => 'Horario de atención',
			'phone' => 'Teléfono',
			'reward' => 'Reward',
			'filename' => 'Logo',
			'ext' => 'Ext',
			'enabled' => 'Estado',
			'created_at' => 'Creado',
			'id_user' => 'Usuario',
			'promotional_site' => 'Sitio promocional',
			'promotional_url_before' => 'URL del sitio promocional antes',
			'promotional_before_start_date' => 'Fecha de comienzo del sitio promocional antes',
			'promotional_before_end_date' => 'Fecha de fin del sitio promocional antes',
			'promotional_url_after' => 'URL del sitio promocional después',
			'promotional_after_start_date' => 'Fecha de comienzo del sitio promocional después',
			'promotional_after_end_date' => 'Fecha de fin del sitio promocional después',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		if($this->criterio!=''){
			$criteria->addSearchCondition('canal.title',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('t.phone',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('t.title',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('t.url',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('t.title_seo',$this->criterio,true,'OR', 'LIKE');
			$criteria->with = array('subcanals','subcanal','canal');
			$criteria->order ="canal.title,concat(IFNULL(subcanal.title,t.title),IFNULL(subcanal.title,'-1'),t.title)";

		}else{
			$criteria->compare('id',$this->id);
			$criteria->compare('id_channel',$this->id_channel);
			$criteria->compare('id_subchannel',$this->id_subchannel);
			$criteria->compare('title',$this->title,true);
			$criteria->compare('url',$this->url,true);
			$criteria->compare('title_seo',$this->title_seo,true);
			$criteria->compare('tracking_description',$this->tracking_description,true);
			$criteria->compare('tracking_analitycs',$this->tracking_analitycs,true);
			$criteria->compare('legals',$this->legals,true);
			$criteria->compare('phone_description',$this->phone_description,true);
			$criteria->compare('office_hours',$this->office_hours,true);
			$criteria->compare('phone',$this->phone,true);
			$criteria->compare('reward',$this->reward);
			$criteria->compare('filename',$this->filename,true);
			$criteria->compare('ext',$this->ext,true);
			$criteria->compare('enabled',$this->enabled);
			$criteria->compare('created_at',$this->created_at,true);
			$criteria->compare('id_user',$this->id_user);
			$criteria->with = array('subcanals','subcanal','canal');
			$criteria->order = "canal.title,concat(IFNULL(subcanal.title,t.title),IFNULL(subcanal.title,'-1'),t.title)";
		}
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function statusvalidate($attribute,$params) {
		if($this->enabled==1){
			if($this->phone=='' || $this->phone_description=='' || $this->filename==''){
				$this->addError($attribute, 'No puede activar el registro hasta que complete la segunda instancia del mismo.');
			}
		}
	}

	public function uniqueuni($attribute,$params) {
		if($this->isNewRecord){
			$uni=Subchannels::model()->findAllByAttributes(array('id_channel'=>$this->id_channel,'title'=>$this->title));
			if(count($uni)>0){
				$this->addError($attribute, 'La combinación de canal y titulo ya existe.');
			}
		}
	}

	public function timeout($attribute,$params) {
		if($this->timeout<0 || $this->timeout>3600){
			$this->addError($attribute, 'El tiempo de espera debe ser de 0 a 3600.');
		}
	}

	protected function beforeValidate(){
				return parent::beforeValidate();
    }

	public static function normalize_dates($model){
		$model->criterio=array();
				return $model;
	}

	protected function afterFind() {
		if($this->promotional_before_start_date!=''){ $this->promotional_before_start_date=Utils::date_spa($this->promotional_before_start_date); }
		if($this->promotional_before_end_date!=''){ $this->promotional_before_end_date=Utils::date_spa($this->promotional_before_end_date); }
		if($this->promotional_after_start_date!=''){ $this->promotional_after_start_date=Utils::date_spa($this->promotional_after_start_date); }
		if($this->promotional_after_end_date!=''){ $this->promotional_after_end_date=Utils::date_spa($this->promotional_after_end_date); }
		return parent::afterFind();
	}

	public function validatedatebefore($attribute,$params) {
		if($this->promotional_url_before!=''){
			if($this->promotional_before_start_date=='' || $this->promotional_before_end_date==''){
				$this->addError($attribute, 'Si completa la url debe completar las fechas.');
			}
		}
	}

	public function validatedateafter($attribute,$params) {
		if($this->promotional_url_after!=''){
			if($this->promotional_after_start_date=='' || $this->promotional_after_end_date==''){
				$this->addError($attribute, 'Si completa la url debe completar las fechas.');
			}
		}
	}

	protected function beforeSave ()
    {
		$this->id_user=Yii::app()->user->id;
		$this->created_at=date("Y-m-d H:i:s");
		if($this->promotional_site==0){
			$this->promotional_before_start_date='';
			$this->promotional_before_end_date='';
			$this->promotional_after_start_date='';
			$this->promotional_after_end_date='';
			$this->promotional_url_after='';
			$this->promotional_url_before='';
		}else{
			if($this->promotional_before_start_date!=''){ $this->promotional_before_start_date=Utils::date_spa_r($this->promotional_before_start_date); }
			if($this->promotional_before_end_date!=''){ $this->promotional_before_end_date=Utils::date_spa_r($this->promotional_before_end_date); }
			if($this->promotional_after_start_date!=''){ $this->promotional_after_start_date=Utils::date_spa_r($this->promotional_after_start_date); }
			if($this->promotional_after_end_date!=''){ $this->promotional_after_end_date=Utils::date_spa_r($this->promotional_after_end_date); }
		}
		foreach($this->attributes as $key=>$value){
			if($value==''){
				$this->$key=NULL;
			}
		}
		return parent::beforeSave ();
    }

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Subchannels the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
