<?php

/**
 * This is the model class for table "booking".
 *
 * The followings are the available columns in table 'booking':
 * @property  public $criterio;
 * @property integer $id
 * @property string $checkoutId
 * @property integer $id_channel
 * @property integer $id_subchannel
 * @property string $status
 * @property string $passenger
 * @property string $date
 * @property string $book_code
 * @property string $package
 * @property string $expire_date
 * @property string $json
 * @property string $email_client
 * @property string $email_operation
 * @property string $consumer
 * @property integer $id_providers
 */
class Booking extends CActiveRecord
{
	public $criterio_pendientes;
	public $criterio_confirmadas;
	public $criterio_canceladas;
	public $criterio_caidas;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'booking';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('checkoutId, id_channel, id_subchannel, status, passenger, date, book_code, package, expire_date, json, email_client, email_operation, consumer, id_providers', 'required'),
			array('id_channel, id_subchannel, id_providers', 'numerical', 'integerOnly'=>true),
			array('checkoutId, status, book_code', 'length', 'max'=>20),
			array('passenger, package', 'length', 'max'=>50),
			array('consumer', 'length', 'max'=>55),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, checkoutId, id_channel, id_subchannel, status, passenger, date, book_code, package, expire_date, json, email_client, email_operation, consumer, id_providers', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'checkoutId' => 'Checkout',
			'id_channel' => 'Id Channel',
			'id_subchannel' => 'Id Subchannel',
			'status' => 'Estado',
			'passenger' => 'Pasajero',
			'date' => 'Fecha',
			'book_code' => 'Book Code',
			'package' => 'Paquete',
			'expire_date' => 'Fecha Exp',
			'json' => 'Json',
			'email_client' => 'Email Cliente',
			'email_operation' => 'Email Operacion',
			'consumer' => 'Consumer',
			'id_providers' => 'Id Providers',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search_pendientes()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		
		if($this->criterio_pendientes!=''){
			$criteria->addSearchCondition('id',$this->criterio_pendientes,true,'OR', 'LIKE');
			$criteria->addSearchCondition('checkoutId',$this->criterio_pendientes,true,'OR', 'LIKE');
			$criteria->addSearchCondition('id_channel',$this->criterio_pendientes,true,'OR', 'LIKE');
			$criteria->addSearchCondition('id_subchannel',$this->criterio_pendientes,true,'OR', 'LIKE');
			$criteria->addSearchCondition('status',$this->criterio_pendientes,true,'OR', 'LIKE');
			$criteria->addSearchCondition('passenger',$this->criterio_pendientes,true,'OR', 'LIKE');
			$criteria->addSearchCondition('date',$this->criterio_pendientes,true,'OR', 'LIKE');
			$criteria->addSearchCondition('book_code',$this->criterio_pendientes,true,'OR', 'LIKE');
			$criteria->addSearchCondition('package',$this->criterio_pendientes,true,'OR', 'LIKE');
			$criteria->addSearchCondition('expire_date',$this->criterio_pendientes,true,'OR', 'LIKE');
			$criteria->addSearchCondition('json',$this->criterio_pendientes,true,'OR', 'LIKE');
			$criteria->addSearchCondition('email_client',$this->criterio_pendientes,true,'OR', 'LIKE');
			$criteria->addSearchCondition('email_operation',$this->criterio_pendientes,true,'OR', 'LIKE');
			$criteria->addSearchCondition('consumer',$this->criterio_pendientes,true,'OR', 'LIKE');
			$criteria->addSearchCondition('id_providers',$this->criterio_pendientes,true,'OR', 'LIKE');
	
		}else{	
			$criteria->compare('id',$this->id);
			$criteria->compare('checkoutId',$this->checkoutId,true);
			$criteria->compare('id_channel',$this->id_channel);
			$criteria->compare('id_subchannel',$this->id_subchannel);
			$criteria->compare('status',$this->status,true);
			$criteria->compare('passenger',$this->passenger,true);
			$criteria->compare('date',$this->date,true);
			$criteria->compare('book_code',$this->book_code,true);
			$criteria->compare('package',$this->package,true);
			$criteria->compare('expire_date',$this->expire_date,true);
			$criteria->compare('json',$this->json,true);
			$criteria->compare('email_client',$this->email_client,true);
			$criteria->compare('email_operation',$this->email_operation,true);
			$criteria->compare('consumer',$this->consumer,true);
			$criteria->compare('id_providers',$this->id_providers);
		}
		$criteria->compare('status','pending');
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	public function search_confirmadas()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		
		if($this->criterio_confirmadas!=''){
			$criteria->addSearchCondition('id',$this->criterio_confirmadas,true,'OR', 'LIKE');
			$criteria->addSearchCondition('checkoutId',$this->criterio_confirmadas,true,'OR', 'LIKE');
			$criteria->addSearchCondition('id_channel',$this->criterio_confirmadas,true,'OR', 'LIKE');
			$criteria->addSearchCondition('id_subchannel',$this->criterio_confirmadas,true,'OR', 'LIKE');
			$criteria->addSearchCondition('status',$this->criterio_confirmadas,true,'OR', 'LIKE');
			$criteria->addSearchCondition('passenger',$this->criterio_confirmadas,true,'OR', 'LIKE');
			$criteria->addSearchCondition('date',$this->criterio_confirmadas,true,'OR', 'LIKE');
			$criteria->addSearchCondition('book_code',$this->criterio_confirmadas,true,'OR', 'LIKE');
			$criteria->addSearchCondition('package',$this->criterio_confirmadas,true,'OR', 'LIKE');
			$criteria->addSearchCondition('expire_date',$this->criterio_confirmadas,true,'OR', 'LIKE');
			$criteria->addSearchCondition('json',$this->criterio_confirmadas,true,'OR', 'LIKE');
			$criteria->addSearchCondition('email_client',$this->criterio_confirmadas,true,'OR', 'LIKE');
			$criteria->addSearchCondition('email_operation',$this->criterio_confirmadas,true,'OR', 'LIKE');
			$criteria->addSearchCondition('consumer',$this->criterio_confirmadas,true,'OR', 'LIKE');
			$criteria->addSearchCondition('id_providers',$this->criterio_confirmadas,true,'OR', 'LIKE');
	
		}else{	
			$criteria->compare('id',$this->id);
			$criteria->compare('checkoutId',$this->checkoutId,true);
			$criteria->compare('id_channel',$this->id_channel);
			$criteria->compare('id_subchannel',$this->id_subchannel);
			$criteria->compare('status',$this->status,true);
			$criteria->compare('passenger',$this->passenger,true);
			$criteria->compare('date',$this->date,true);
			$criteria->compare('book_code',$this->book_code,true);
			$criteria->compare('package',$this->package,true);
			$criteria->compare('expire_date',$this->expire_date,true);
			$criteria->compare('json',$this->json,true);
			$criteria->compare('email_client',$this->email_client,true);
			$criteria->compare('email_operation',$this->email_operation,true);
			$criteria->compare('consumer',$this->consumer,true);
			$criteria->compare('id_providers',$this->id_providers);
		}
		$criteria->compare('status','confirm');
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function search_canceladas()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		
		if($this->criterio_canceladas!=''){
			$criteria->addSearchCondition('id',$this->criterio_canceladas,true,'OR', 'LIKE');
			$criteria->addSearchCondition('checkoutId',$this->criterio_canceladas,true,'OR', 'LIKE');
			$criteria->addSearchCondition('id_channel',$this->criterio_canceladas,true,'OR', 'LIKE');
			$criteria->addSearchCondition('id_subchannel',$this->criterio_canceladas,true,'OR', 'LIKE');
			$criteria->addSearchCondition('status',$this->criterio_canceladas,true,'OR', 'LIKE');
			$criteria->addSearchCondition('passenger',$this->criterio_canceladas,true,'OR', 'LIKE');
			$criteria->addSearchCondition('date',$this->criterio_canceladas,true,'OR', 'LIKE');
			$criteria->addSearchCondition('book_code',$this->criterio_canceladas,true,'OR', 'LIKE');
			$criteria->addSearchCondition('package',$this->criterio_canceladas,true,'OR', 'LIKE');
			$criteria->addSearchCondition('expire_date',$this->criterio_canceladas,true,'OR', 'LIKE');
			$criteria->addSearchCondition('json',$this->criterio_canceladas,true,'OR', 'LIKE');
			$criteria->addSearchCondition('email_client',$this->criterio_canceladas,true,'OR', 'LIKE');
			$criteria->addSearchCondition('email_operation',$this->criterio_canceladas,true,'OR', 'LIKE');
			$criteria->addSearchCondition('consumer',$this->criterio_canceladas,true,'OR', 'LIKE');
			$criteria->addSearchCondition('id_providers',$this->criterio_canceladas,true,'OR', 'LIKE');
	
		}else{	
			$criteria->compare('id',$this->id);
			$criteria->compare('checkoutId',$this->checkoutId,true);
			$criteria->compare('id_channel',$this->id_channel);
			$criteria->compare('id_subchannel',$this->id_subchannel);
			$criteria->compare('status',$this->status,true);
			$criteria->compare('passenger',$this->passenger,true);
			$criteria->compare('date',$this->date,true);
			$criteria->compare('book_code',$this->book_code,true);
			$criteria->compare('package',$this->package,true);
			$criteria->compare('expire_date',$this->expire_date,true);
			$criteria->compare('json',$this->json,true);
			$criteria->compare('email_client',$this->email_client,true);
			$criteria->compare('email_operation',$this->email_operation,true);
			$criteria->compare('consumer',$this->consumer,true);
			$criteria->compare('id_providers',$this->id_providers);
		}
		$criteria->compare('status','cancel');
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function search_caidas()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		
		if($this->criterio_caidas!=''){
			$criteria->addSearchCondition('id',$this->criterio_caidas,true,'OR', 'LIKE');
			$criteria->addSearchCondition('checkoutId',$this->criterio_caidas,true,'OR', 'LIKE');
			$criteria->addSearchCondition('id_channel',$this->criterio_caidas,true,'OR', 'LIKE');
			$criteria->addSearchCondition('id_subchannel',$this->criterio_caidas,true,'OR', 'LIKE');
			$criteria->addSearchCondition('status',$this->criterio_caidas,true,'OR', 'LIKE');
			$criteria->addSearchCondition('passenger',$this->criterio_caidas,true,'OR', 'LIKE');
			$criteria->addSearchCondition('date',$this->criterio_caidas,true,'OR', 'LIKE');
			$criteria->addSearchCondition('book_code',$this->criterio_caidas,true,'OR', 'LIKE');
			$criteria->addSearchCondition('package',$this->criterio_caidas,true,'OR', 'LIKE');
			$criteria->addSearchCondition('expire_date',$this->criterio_caidas,true,'OR', 'LIKE');
			$criteria->addSearchCondition('json',$this->criterio_caidas,true,'OR', 'LIKE');
			$criteria->addSearchCondition('email_client',$this->criterio_caidas,true,'OR', 'LIKE');
			$criteria->addSearchCondition('email_operation',$this->criterio_caidas,true,'OR', 'LIKE');
			$criteria->addSearchCondition('consumer',$this->criterio_caidas,true,'OR', 'LIKE');
			$criteria->addSearchCondition('id_providers',$this->criterio_caidas,true,'OR', 'LIKE');
	
		}else{	
			$criteria->compare('id',$this->id);
			$criteria->compare('checkoutId',$this->checkoutId,true);
			$criteria->compare('id_channel',$this->id_channel);
			$criteria->compare('id_subchannel',$this->id_subchannel);
			$criteria->compare('status',$this->status,true);
			$criteria->compare('passenger',$this->passenger,true);
			$criteria->compare('date',$this->date,true);
			$criteria->compare('book_code',$this->book_code,true);
			$criteria->compare('package',$this->package,true);
			$criteria->compare('expire_date',$this->expire_date,true);
			$criteria->compare('json',$this->json,true);
			$criteria->compare('email_client',$this->email_client,true);
			$criteria->compare('email_operation',$this->email_operation,true);
			$criteria->compare('consumer',$this->consumer,true);
			$criteria->compare('id_providers',$this->id_providers);
		}
		$criteria->compare('status','cancel');
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}


	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Booking the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
