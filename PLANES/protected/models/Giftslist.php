<?php

/**
 * This is the model class for table "gifts_list".
 *
 * The followings are the available columns in table 'gifts_list':
 * @property  public $criterio;
 * @property integer $id
 * @property integer $id_gift
 * @property string $description
 * @property string $price
 * @property integer $order
 * @property integer $id_user
 * @property string $created_at
 */
class Giftslist extends CActiveRecord
{
	public $criterio;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'gifts_list';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
					
				array('id_gift, description, price, order', 'required','message'=>'Debe completar este campo.'),
				array('id_gift, order, id_user', 'numerical', 'integerOnly'=>true),
				array('description', 'length', 'max'=>256),
				array('price', 'length', 'max'=>12),
					// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, id_gift, description, price, order, id_user, created_at', 'safe', 'on'=>'search'),
		);
	}

	public function behaviors(){
		return array(
			// Classname => path to Class
			'ActiveRecordLogableBehavior'=>
				'application.behaviors.ActiveRecordLogableBehavior',
		);
	}

	/**
	 * @return array relational rules.
	 */
	
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
					
		 );
	}	

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_gift' => 'Id Gift',
			'description' => 'Description',
			'price' => 'Price',
			'order' => 'Order',
			'id_user' => 'Id User',
			'created_at' => 'Created At',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		
		if($this->criterio!=''){
			$criteria->addSearchCondition('id',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('id_gift',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('description',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('price',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('order',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('id_user',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('created_at',$this->criterio,true,'OR', 'LIKE');
	
		}else{	
			$criteria->compare('id',$this->id);
			$criteria->compare('id_gift',$this->id_gift);
			$criteria->compare('description',$this->description,true);
			$criteria->compare('price',$this->price,true);
			$criteria->compare('order',$this->order);
			$criteria->compare('id_user',$this->id_user);
			$criteria->compare('created_at',$this->created_at,true);
		}
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	protected function beforeValidate(){
						
						
						
						
						
						
						
				return parent::beforeValidate();
    }	
	
	public static function normalize_dates($model){
		$model->criterio=array();
					
					
					
					
					
					
					
				return $model;		
	}
	
	protected function afterFind() {
						
					
						
					
						
					
						
					
						
					
						
					
						
					
						
		return parent::afterFind();
	}	
	
	protected function beforeSave ()
    {	
		$this->created_at=date("Y-m-d H:i:s");
																																													foreach($this->attributes as $key=>$value){
			if($value==''){
				$this->$key=NULL;
			}
		}			
		return parent::beforeSave ();
    }	
	
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Giftslist the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
