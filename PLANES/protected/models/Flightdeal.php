<?php

/**
 * This is the model class for table "flight_deal".
 *
 * The followings are the available columns in table 'flight_deal':
 * @property  public $criterio;
 * @property integer $id
 * @property string $title
 * @property string $url
 * @property integer $type_currency
 * @property integer $price
 * @property integer $points
 * @property integer $total_points
 * @property integer $discount
 * @property string $expire_date_from
 * @property string $expire_date_to
 * @property integer $id_airline
 * @property integer $direct
 * @property integer $deal
 * @property integer $enabled
 * @property string $created_at
 * @property integer $id_user
 * @property integer $promotional
 * @property string $detail
 * @property integer $order
 */
class Flightdeal extends CActiveRecord
{
	public $criterio;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'flight_deal';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('expire_date_from, expire_date_to', 'type', 'type' => 'date', 'message' => '{attribute}: no es una fecha', 'dateFormat' => 'dd/mm/yyyy'),
				array('url', 'url'),
				array('title, url, expire_date_from, expire_date_to, id_airline', 'required','message'=>'Debe completar este campo.'),
				array(' type_currency, price, points, total_points, discount, id_airline, direct, deal, promotional, enabled,order, id_user', 'numerical', 'integerOnly'=>true),
				array('title', 'length', 'max'=>32),
				array('url', 'length', 'max'=>256),
				array('detail', 'length', 'max'=>520),
					// The following rule is used by search().
					// @todo Please remove those attributes that should not be searched.
				array('id, title, url, type_currency, price, points, total_points, discount, expire_date_from, expire_date_to, id_airline, direct, deal, enabled, created_at, id_user, promotional, detail', 'safe', 'on'=>'search'),
		);
	}

	public function behaviors(){
		return array(
			// Classname => path to Class
			'ActiveRecordLogableBehavior'=>
				'application.behaviors.ActiveRecordLogableBehavior',
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'imagenes' => array(self::HAS_MANY, 'Fligthdealimages', 'id_fligth_deal', 'order'=>'imagenes.order ASC'),
			'relational' => array(self::HAS_MANY, 'Flightdealrelational', 'id_flight_deal'),
			/*'canal' => array(self::BELONGS_TO, 'Channels', 'id_channel'),
			'subcanal' => array(self::BELONGS_TO, 'Subchannels', 'id_subchannel'),*/
			'moneda' => array(self::BELONGS_TO, 'Typecurrency', 'type_currency'),
			'aerolinea' => array(self::BELONGS_TO, 'Airliners', 'id_airline'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'title' => 'Destino / Ruta',
			'url' => 'URL',
			'order' => 'Categoría',
			/*'id_channel' => 'Canal',
			'id_subchannel' => 'Subcanal',*/
			'type_currency' => 'Moneda',
			'price' => 'Tarifa',
			'points' => 'Puntos',
			'total_points' => 'Puntos totales',
			'discount' => '% de descuento',
			'expire_date_from' => 'Vigencia desde',
			'expire_date_to' => 'Vigencia hasta',
			'id_airline' => 'Aerolínea',
			'direct' => 'Vuelo directo',
			'deal' => 'Destacado',
			'enabled' => 'Estado',
			'created_at' => 'Creado',
			'id_user' => 'Usuario',
			'promotional' => 'Promotional',
			'detail' => 'Detalle',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		if($this->criterio!=''){
			$criteria->addSearchCondition('id',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('title',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('url',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('type_currency',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('price',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('points',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('total_points',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('discount',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('expire_date_from',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('expire_date_to',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('id_airline',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('direct',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('deal',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('enabled',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('created_at',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('id_user',$this->criterio,true,'OR', 'LIKE');

		}else{
			$criteria->compare('id',$this->id);
			$criteria->compare('title',$this->title,true);
			$criteria->compare('url',$this->url,true);
			$criteria->compare('type_currency',$this->type_currency);
			$criteria->compare('price',$this->price);
			$criteria->compare('points',$this->points);
			$criteria->compare('total_points',$this->total_points);
			$criteria->compare('discount',$this->discount);
			$criteria->compare('expire_date_from',$this->expire_date_from,true);
			$criteria->compare('expire_date_to',$this->expire_date_to,true);
			$criteria->compare('id_airline',$this->id_airline);
			$criteria->compare('direct',$this->direct);
			$criteria->compare('deal',$this->deal);
			$criteria->compare('enabled',$this->enabled);
			$criteria->compare('created_at',$this->created_at,true);
			$criteria->compare('id_user',$this->id_user);
		}
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	protected function beforeValidate(){


















				return parent::beforeValidate();
    }

	public static function normalize_dates($model){
		$model->criterio=array();


















				return $model;
	}

	protected function afterFind() {




















						$this->expire_date_from =Utils::date_spa($this->expire_date_from);


						$this->expire_date_to =Utils::date_spa($this->expire_date_to);















		return parent::afterFind();
	}

	protected function beforeSave ()
    {
    	$this->id_user=Yii::app()->user->id;
		$this->created_at=date("Y-m-d H:i:s");																																																																	$this->expire_date_from = date_create_from_format("d/m/Y",$this->expire_date_from);
		$this->expire_date_from =date_format( $this->expire_date_from,"Y-m-d");
		$this->expire_date_to = date_create_from_format("d/m/Y",$this->expire_date_to);
		$this->expire_date_to =date_format( $this->expire_date_to,"Y-m-d");																																									
		foreach($this->attributes as $key=>$value){
			if($value==''){
				$this->$key=NULL;
			}
		}
		return parent::beforeSave ();
    }

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Flightdeal the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
