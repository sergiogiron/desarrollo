<?php

/**
 * This is the model class for table "financial_cost".
 *
 * The followings are the available columns in table 'financial_cost':
 * @property  public $criterio;
 * @property integer $id
 * @property integer $id_provider
 * @property integer $id_product
 * @property string $percentage_financial_cost
 */
class Financialcost extends CActiveRecord
{
	public $criterio;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'financial_cost';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			//array('id_provider, id_product', 'unique', 'on'=>'insert', 'attributeName'=>'id_provider'),
			array('id_provider', 'ext.validators.UniqueattributesValidator', 'with'=>'id_product'),
			array('id_provider, id_product, percentage_financial_cost', 'required'),
			array('id_provider, id_product', 'numerical', 'integerOnly'=>true),
			array('percentage_financial_cost', 'length', 'max'=>10),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, id_provider, id_product, percentage_financial_cost', 'safe', 'on'=>'search'),
		);
	}

	public function behaviors(){
		return array(
			// Classname => path to Class
			'ActiveRecordLogableBehavior'=>
				'application.behaviors.ActiveRecordLogableBehavior',
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'producto' => array(self::BELONGS_TO, 'Products', 'id_product'),
			'proveedor' => array(self::BELONGS_TO, 'Providers', 'id_provider'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'enabled' => 'Estado',
			'id_provider' => 'Proveedor',
			'id_product' => 'Producto',
			'percentage_financial_cost' => 'Porcentaje de costo financiero',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		
		if($this->criterio!=''){
			$criteria->addSearchCondition('percentage_financial_cost',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('proveedor.title',$this->criterio,true,'OR', 'LIKE');			
			$criteria->addSearchCondition('producto.title',$this->criterio,true,'OR', 'LIKE');
			
			$criteria->with = array('proveedor','producto');		
	
		}else{	
			$criteria->compare('id',$this->id);
			$criteria->compare('id_provider',$this->id_provider);
			$criteria->compare('id_product',$this->id_product);
			$criteria->compare('percentage_financial_cost',$this->percentage_financial_cost,true);
		}
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Financialcost the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
