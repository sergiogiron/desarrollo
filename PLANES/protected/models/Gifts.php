<?php

/**
 * This is the model class for table "gifts".
 *
 * The followings are the available columns in table 'gifts':
 * @property  public $criterio;
 * @property integer $id
 * @property integer $id_user
 * @property integer $id_type_currency
 * @property string $date
 * @property string $boyfriends
 * @property string $destinations
 * @property string $dk
 * @property string $created_at
 * @property integer $enabled
 */
class Gifts extends CActiveRecord
{
	public $criterio;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'gifts';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('date', 'type', 'type' => 'date', 'message' => '{attribute}: no es una fecha', 'dateFormat' => 'dd/mm/yyyy'),
					
				array('id_user, date, boyfriends, destinations, dk, id_type_currency, enabled', 'required','message'=>'Debe completar este campo.'),
				array('id_user, enabled, id_type_currency', 'numerical', 'integerOnly'=>true),
				array('boyfriends', 'unique'),
				array('boyfriends, destinations', 'length', 'max'=>100),
				array('dk', 'length', 'max'=>64),
				array('created_at', 'safe'),
					// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, id_user, date, boyfriends, destinations, dk, id_type_currency, created_at, enabled', 'safe', 'on'=>'search'),
		);
	}

	public function behaviors(){
		return array(
			// Classname => path to Class
			'ActiveRecordLogableBehavior'=>
				'application.behaviors.ActiveRecordLogableBehavior',
		);
	}

	/**
	 * @return array relational rules.
	 */
	
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'usuario' => array(self::BELONGS_TO, 'Users', 'id_user'),
			'imagenes' => array(self::HAS_MANY, 'Giftsimages', 'id_gifts', 'order'=>'imagenes.order ASC'),
			'currency' => array(self::BELONGS_TO, 'Typecurrency', 'id_type_currency'),
			'giftslist' => array(self::HAS_MANY, 'Giftslist', 'id_gift', 'order'=>'giftslist.order ASC'),			
		 );
	}	

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_user' => 'Asociado',
			'date' => 'Fecha',
			'boyfriends' => 'Novios',
			'destinations' => 'Destino',
			'dk' => 'DK',
			'id_type_currency' => 'Moneda',
			'created_at' => 'Created At',
			'enabled' => 'Estado',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		
		if($this->criterio!=''){
			$criteria->addSearchCondition('id',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('id_user',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('date',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('boyfriends',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('destinations',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('dk',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('id_type_currency',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('created_at',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('enabled',$this->criterio,true,'OR', 'LIKE');
	
		}else{	
			$criteria->compare('id',$this->id);
			$criteria->compare('id_user',$this->id_user);
			$criteria->compare('date',$this->date,true);
			$criteria->compare('boyfriends',$this->boyfriends,true);
			$criteria->compare('destinations',$this->destinations,true);
			$criteria->compare('dk',$this->dk,true);
			$criteria->compare('id_type_currency',$this->id_type_currency,true);
			$criteria->compare('created_at',$this->created_at,true);
			$criteria->compare('enabled',$this->enabled);
		}
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	protected function beforeValidate(){
						
						
						
						
						
						
						
						
						
						
						
				return parent::beforeValidate();
    }	
	
	public static function normalize_dates($model){
		$model->criterio=array();
					
					
					
					
					
					
					
					
					
					
					
				return $model;		
	}
	
	protected function afterFind() {
						
					
						
					
						$this->date =Utils::date_spa($this->date);
				
					
						
					
						
					
						
					
						
					
						
					
						
					
						
					
						
					
						
		return parent::afterFind();
	}	
	
	protected function beforeSave ()
    {	
		$this->created_at=date("Y-m-d H:i:s");
																	$this->date = date_create_from_format("d/m/Y",$this->date);
		$this->date =date_format( $this->date,"Y-m-d");		  
																																																								foreach($this->attributes as $key=>$value){
			if($value==''){
				$this->$key=NULL;
			}
		}			
		return parent::beforeSave ();
    }	
	
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Gifts the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
