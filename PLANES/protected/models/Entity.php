<?php

/**
 * This is the model class for table "entity".
 *
 * The followings are the available columns in table 'entity':
 * @property integer $id
 * @property string $name
 * @property string $filename
 * @property string $expire_date_from
 * @property string $expire_date_to
 * @property integer $enabled
 * @property integer $entity_highlight
 *
 * The followings are the available model relations:
 * @property PaymentMethod[] $paymentMethods
 */
class Entity extends CActiveRecord
{
	public $criterio;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'payments_entity';
	}

	public function behaviors()
	{
	    return array(
	        // Classname => path to Class
	        'ActiveRecordLogableBehavior'=>
	            'application.behaviors.ActiveRecordLogableBehavior',
	    );
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title,expire_date_from,expire_date_to,filename', 'required'),
			array('enabled,entity_highlight', 'numerical', 'integerOnly'=>true),
			array('title', 'length', 'max'=>45),
			array('filename', 'length', 'max'=>128),
			array('ext', 'length', 'max'=>12),
			array('expire_date_from, expire_date_to', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, title, filename, expire_date_from, expire_date_to, enabled', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation title and the related
		// class title for the relations automatically generated below.
		return array(
			'financing' => array(self::HAS_MANY, 'Paymentsentityfinancing','id_entity','order'=>'financing.amount_dues DESC'),
			'cruisessupplier' => array(self::HAS_MANY, 'Paymentsentitycruisessupplier','id_entity','order'=>'cruisessupplier.amount_dues DESC'),
			'airliners' => array(self::HAS_MANY, 'Paymentsentityairliners','id_entity'),
			'accounttype' => array(self::HAS_MANY, 'Paymentsentityaccounttype','id_entity'),
			'paymentMethods' => array(self::HAS_MANY, 'PaymentMethod', 'id_bank'),
			'imagenes' => array(self::HAS_MANY, 'Paymentsentityimages', 'id_payments_entity', 'order'=>'imagenes.order ASC'),
		);
	}

	/**
	 * @return array customized attribute labels (title=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'title' => 'Entidad',
			'filename' => 'Logo',
			'expire_date_from' => 'Vigencia desde',
			'expire_date_to' => 'Vigencia hasta',
			'enabled' => 'Estado',
			'entity_highlight' => 'Entidad destacada',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		$criteria=new CDbCriteria;
		if($this->criterio!=''){
			$criteria->addSearchCondition('title',$this->criterio, true,'OR', 'LIKE');
		}else{	
			$criteria->compare('title',$this->title,true);
			$criteria->compare('enabled',$this->enabled);
		}
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	protected function beforeValidate(){
		if($this->expire_date_to!=''){
			$this->expire_date_to = date_create_from_format("d/m/Y",$this->expire_date_to);
			$this->expire_date_to =date_format( $this->expire_date_to,"d/m/Y");
		}
		if($this->expire_date_from!=''){
			$this->expire_date_from = date_create_from_format("d/m/Y",$this->expire_date_from);
			$this->expire_date_from =date_format( $this->expire_date_from,"d/m/Y");
		}
		return parent::beforeValidate();
    }

	public static function normalize_dates($model){
		$model->criterio=array();
		$expire_date_to= explode(" ",$model->expire_date_to);
		$model->expire_date_to=$expire_date_to[0];

		$expire_date_from= explode(" ",$model->expire_date_from);
		$model->expire_date_from=$expire_date_from[0];

		return $model;
	}

	protected function afterFind() {
		$this->expire_date_to =Utils::date_spa($this->expire_date_to);
		$this->expire_date_from =Utils::date_spa($this->expire_date_from);
		return parent::afterFind();
	}

	protected function beforeSave (){
		$this->created_at=date("Y-m-d H:i:s");
		$date_expire_date_to = DateTime::createFromFormat('d/m/Y', $this->expire_date_to);
		$this->expire_date_to=$date_expire_date_to->format('Y-m-d');
		$date_expire_date_from = DateTime::createFromFormat('d/m/Y', $this->expire_date_from);
		$this->expire_date_from=$date_expire_date_from->format('Y-m-d');
		return parent::beforeSave ();
    }

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Entity the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
