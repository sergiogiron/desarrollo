<?php

/**
 * This is the model class for table "gifts_images".
 *
 * The followings are the available columns in table 'gifts_images':
 * @property  public $criterio;
 * @property integer $id
 * @property integer $id_gifts
 * @property integer $position
 * @property string $filename
 * @property string $ext
 * @property string $link
 * @property string $title
 * @property integer $order
 * @property string $coords
 * @property string $created_at
 * @property integer $id_user
 *
 * The followings are the available model relations:
 * @property Gifts $idGifts
 * @property Users $idUser
 */
class Giftsimages extends CActiveRecord
{
	public $criterio;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'gifts_images';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('link', 'url', 'message'=>'{attribute}: no es una url válida'),
					
				array('id_gifts, filename', 'required','message'=>'Debe completar este campo.'),
				array('id_gifts, position, order, id_user', 'numerical', 'integerOnly'=>true),
				array('filename', 'length', 'max'=>64),
				array('ext', 'length', 'max'=>12),
				array('link', 'length', 'max'=>260),
				array('title', 'length', 'max'=>65),
				array('coords, created_at', 'safe'),
					// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, id_gifts, position, filename, ext, link, title, order, coords, created_at, id_user', 'safe', 'on'=>'search'),
		);
	}

	public function behaviors(){
		return array(
			// Classname => path to Class
			'ActiveRecordLogableBehavior'=>
				'application.behaviors.ActiveRecordLogableBehavior',
		);
	}

	/**
	 * @return array relational rules.
	 */
	
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idGifts' => array(self::BELONGS_TO, 'Gifts', 'id_gifts','alias'=>'gifts'),
'idUser' => array(self::BELONGS_TO, 'Users', 'id_user','alias'=>'users'),
		
		 );
	}	

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_gifts' => 'Id Gifts',
			'position' => 'Position',
			'filename' => 'Filename',
			'ext' => 'Ext',
			'link' => 'Link',
			'title' => 'Title',
			'order' => 'Order',
			'coords' => 'Coords',
			'created_at' => 'Created At',
			'id_user' => 'Id User',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		
		if($this->criterio!=''){
			$criteria->addSearchCondition('id',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('id_gifts',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('position',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('filename',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('ext',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('link',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('title',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('order',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('coords',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('created_at',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('id_user',$this->criterio,true,'OR', 'LIKE');
	
		}else{	
			$criteria->compare('id',$this->id);
			$criteria->compare('id_gifts',$this->id_gifts);
			$criteria->compare('position',$this->position);
			$criteria->compare('filename',$this->filename,true);
			$criteria->compare('ext',$this->ext,true);
			$criteria->compare('link',$this->link,true);
			$criteria->compare('title',$this->title,true);
			$criteria->compare('order',$this->order);
			$criteria->compare('coords',$this->coords,true);
			$criteria->compare('created_at',$this->created_at,true);
			$criteria->compare('id_user',$this->id_user);
		}
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	protected function beforeValidate(){
						
						
						
						
						
						
						
						
						
						
						
				return parent::beforeValidate();
    }	
	
	public static function normalize_dates($model){
		$model->criterio=array();
					
					
					
					
					
					
					
					
					
					
					
				return $model;		
	}
	
	protected function afterFind() {
						
					
						
					
						
					
						
					
						
					
						
					
						
					
						
					
						
					
						
					
						
					
						
		return parent::afterFind();
	}	
	
	protected function beforeSave ()
    {	
		$this->created_at=date("Y-m-d H:i:s");
																																																																					foreach($this->attributes as $key=>$value){
			if($value==''){
				$this->$key=NULL;
			}
		}			
		return parent::beforeSave ();
    }	
	
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Giftsimages the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
