<?php

/**
 * This is the model class for table "api_key".
 *
 * The followings are the available columns in table 'api_key':
 * @property  public $criterio;
 * @property integer $id
 * @property integer $id_user_services
 * @property string $api_key
 * @property string $expire_date_from
 * @property string $expire_date_to
 * @property integer $valid_expire
 * @property integer $enabled
 * @property string $created_at
 * @property integer $id_user
 *
 * The followings are the available model relations:
 * @property UserServices $idUserServices
 */
class Apikey extends CActiveRecord
{
	public $criterio;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'api_key';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
		/*	array('expire_date_from, expire_date_to', 'date', 'format'=>'dd/mm/yyyy hh:mm:ss', 'message'=>'{attribute}: no es una fecha'),*/

			array('id_user_services, api_key, created_at, id_user', 'required'),
			array('id_user_services, valid_expire, enabled, id_user', 'numerical', 'integerOnly'=>true),
			array('api_key', 'length', 'max'=>128),
			array('expire_date_from, expire_date_to', 'safe'),
					// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, id_user_services, api_key, expire_date_from, expire_date_to, valid_expire, enabled, created_at, id_user', 'safe', 'on'=>'search'),
		);
	}

	public function behaviors(){
		return array(
			// Classname => path to Class
			'ActiveRecordLogableBehavior'=>
				'application.behaviors.ActiveRecordLogableBehavior',
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'usuario' => array(self::BELONGS_TO, 'Userservices', 'id_user_services'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_user_services' => 'Usuario de Servicio',
			'api_key' => 'Api Key',
			'expire_date_from' => 'Vigencia desde',
			'expire_date_to' => 'Vigencia hasta',
			'valid_expire' => 'Expira',
			'enabled' => 'Activa',
			'created_at' => 'Fecha de registro',
			'id_user' => 'Usuario',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		if($this->criterio!=''){
			$criteria->addSearchCondition('id',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('id_user_services',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('api_key',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('expire_date_from',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('expire_date_to',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('valid_expire',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('enabled',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('created_at',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('id_user',$this->criterio,true,'OR', 'LIKE');

		}else{

			$criteria->compare('id',$this->id);
			$criteria->compare('id_user_services',$this->id_user_services);
			$criteria->compare('api_key',$this->api_key,true);
			$criteria->compare('expire_date_from',$this->expire_date_from,true);
			$criteria->compare('expire_date_to',$this->expire_date_to,true);
			$criteria->compare('valid_expire',$this->valid_expire);
			$criteria->compare('enabled',$this->enabled);
			$criteria->compare('created_at',$this->created_at,true);
			$criteria->compare('id_user',$this->id_user);
		}
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	/*
		protected function beforeValidate(){


		if($this->expire_date_from!='' && $_POST['expire_date_from_hour']!=''){
		$this->expire_date_from = date_create_from_format("d/m/Y H:i:s",$this->expire_date_from.' '.$_POST['expire_date_from_hour'].':00');
		$this->expire_date_from =date_format( $this->expire_date_from,"d/m/Y H:i:s");
		}

		if($this->expire_date_to!='' && $_POST['expire_date_to_hour']!=''){
		$this->expire_date_to = date_create_from_format("d/m/Y H:i:s",$this->expire_date_to.' '.$_POST['expire_date_to_hour'].':00');
		$this->expire_date_to =date_format( $this->expire_date_to,"d/m/Y H:i:s");
		}


		return parent::beforeValidate();
<<<<<<< HEAD
		}	
=======
		}
>>>>>>> 3e8d0263b7e1abb0820952986c2ab156f26973db

		public static function normalize_dates($model){
		$model->criterio=array();



		$expire_date_from= explode(" ",$model->expire_date_from);
		$model->expire_date_from=$expire_date_from[0];
		$model->criterio=array_merge($model->criterio,array('expire_date_from_hour'=>$expire_date_from[1]));

		$expire_date_to= explode(" ",$model->expire_date_to);
		$model->expire_date_to=$expire_date_to[0];
		$model->criterio=array_merge($model->criterio,array('expire_date_to_hour'=>$expire_date_to[1]));

<<<<<<< HEAD
		return $model;		
=======
		return $model;
>>>>>>> 3e8d0263b7e1abb0820952986c2ab156f26973db
		}

		protected function afterFind() {


		$this->expire_date_from =Utils::datetime_spa($this->expire_date_from);


		$this->expire_date_to =Utils::datetime_spa($this->expire_date_to);

		return parent::afterFind();
<<<<<<< HEAD
		}	

		protected function beforeSave ()
		{	
=======
		}

		protected function beforeSave ()
		{
>>>>>>> 3e8d0263b7e1abb0820952986c2ab156f26973db
		$this->created_at=date("Y-m-d H:i:s");
		$date_expire_date_from = DateTime::createFromFormat('d/m/Y H:i:s', $this->expire_date_from;
		$this->expire_date_from=$date_expire_date_from->format('Y-m-d H:i:s');
		$date_expire_date_to = DateTime::createFromFormat('d/m/Y H:i:s', $this->expire_date_to);
		$this->expire_date_to=$date_expire_date_to->format('Y-m-d H:i:s');
		return parent::beforeSave ();
<<<<<<< HEAD
		}	
=======
		}
>>>>>>> 3e8d0263b7e1abb0820952986c2ab156f26973db
	*/
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Apikey the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
