<?php

/**
 * This is the model class for table "offers_product".
 *
 * The followings are the available columns in table 'offers_product':
 * @property  public $criterio;
 * @property integer $id
 * @property integer $id_channel
 * @property integer $id_subchannel
 * @property integer $id_product
 * @property integer $id_email_server_sales
 * @property string $sale_user
 * @property string $sale_show_as
 * @property integer $id_email_server_alerts
 * @property string $alerts_pass
 * @property string $alerts_to
 * @property string $script
 * @property string $sale_pass
 * @property string $sale_to
 * @property string $alerts_user
 * @property string $alerts_show_as
 * @property string $alerts_subject_patterns 
 * @property string $title
 * @property string $description
 * @property string $legal
 * @property integer $enabled
 * @property string $created_at
 * @property string $thankyou_text
 * @property integer $id_user
 * @property integer $online
 * @property string $tracking_description
 * @property string $tracking_analitycs
 * @property integer $order
 */
class Offersproduct extends CActiveRecord
{
	public $criterio;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'offers_product';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
				//array('id_channel, id_subchannel, id_product, title, order,description,thankyou_text', 'required','message'=>'Debe completar este campo.'),
				array('id_channel, id_subchannel, id_product', 'required','message'=>'Debe completar este campo.'),
				array('id_channel,id_subchannel, id_product', 'uniqueuni', 'attributeName'=>'id_channel,id_subchannel, id_product'),
				array('id_channel, id_subchannel, id_product, enabled, order, id_user, id_email_server_sales,id_email_server_alerts', 'numerical', 'integerOnly'=>true),
				array('title, description', 'length', 'max'=>65),
				array('phone_description,office_hours', 'length', 'max'=>64),
				array('phone', 'length', 'max'=>35),
				array('tracking_analitycs', 'length', 'max'=>45),
				array('tracking_description', 'length', 'max'=>1024),
				array('sale_user,sale_show_as,alerts_pass,sale_pass,alerts_user,alerts_show_as,alerts_subject_patterns', 'length', 'max'=>45),
				array('script, sale_to,alerts_to,thankyou_text', 'safe'),
				array('enabled', 'statusvalidate', 'attributeName'=>'enabled'),
				array('id_email_server_sales', 'emailsendvalidate', 'attributeName'=>'id_email_server_sales'),
					// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, id_channel, id_subchannel,thankyou_text, id_product, title, description, enabled, created_at, id_user', 'safe', 'on'=>'search'),
			array('legal', 'safe'),
		);
	}

	public function behaviors(){
		return array(
			// Classname => path to Class
			'ActiveRecordLogableBehavior'=>
				'application.behaviors.ActiveRecordLogableBehavior',
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'canal' => array(self::BELONGS_TO, 'Channels', 'id_channel'),
			'subcanal' => array(self::BELONGS_TO, 'Subchannels', 'id_subchannel'),
			'producto' => array(self::BELONGS_TO, 'Products', 'id_product'),
			
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_channel' => 'Canal',
			'order' => 'Orden de ofertas',
			'id_subchannel' => 'Subcanal',
			'id_product' => 'Producto',
			'title' => 'Título del Producto',
			'description' => 'Título de Página',
			'enabled' => 'Estado',
			'created_at' => 'Creado',
			'id_user' => 'Usuario',
			'legal' => 'Legal',
			'tracking_description' => 'Descripción SEO',
			'tracking_analitycs' => 'Header',
			'thankyou_text' => 'Texto de gracias',
			'id_email_server_sales' => 'Servidor de email de contacto',
			'sale_user' => 'Usuario',
			'sale_show_as' => 'Mostrar como',
			'id_email_server_alerts' => 'Servidor de email de alertas',
			'alerts_pass' => 'Contraseña',
			'alerts_to' => 'Notificar a ',
			'script' => 'Script a ejecutar en pantalla de gracias',
			'phone'=>'Teléfono',
			'phone_description'=>'Texto del teléfono',
			'office_hours'=>'Horario de atención',			
			'sale_pass' => 'Contraseña',
			'sale_to' => 'Notificar a',
			'alerts_user' => 'Usuario',
			'alerts_show_as' => 'Mostrar como',
			'alerts_subject_patterns' => 'Patrón del asunto',	
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		
		if($this->criterio!=''){
			$criteria->addSearchCondition('id',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('id_channel',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('id_subchannel',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('id_product',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('title',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('description',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('enabled',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('created_at',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('id_user',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('legal',$this->criterio,true,'OR', 'LIKE');
	
		}else{	
			$criteria->compare('id',$this->id);
			$criteria->compare('id_channel',$this->id_channel);
			$criteria->compare('id_subchannel',$this->id_subchannel);
			$criteria->compare('id_product',$this->id_product);
			$criteria->compare('title',$this->title,true);
			$criteria->compare('legal',$this->legal,true);
			$criteria->compare('description',$this->description,true);
			$criteria->compare('enabled',$this->enabled);
			$criteria->compare('created_at',$this->created_at,true);
			$criteria->compare('id_user',$this->id_user);
		}
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	public function emailsendvalidate($attribute,$params) {
		if($this->id_email_server_sales!=''){
				if($this->id_email_server_sales==''  || $this->sale_user=='' || $this->sale_show_as=='' || $this->id_email_server_alerts=='' || $this->alerts_pass=='' || $this->sale_pass=='' || $this->sale_to==''|| $this->alerts_user=='' || $this->alerts_show_as=='' || $this->sale_pass=='' || $this->alerts_subject_patterns==''){
				$this->addError($attribute, 'Debe completar todos los datos de envio de email');
			}
		}
	}	
	
	public function statusvalidate($attribute,$params) {
		/*if($this->enabled==1){
				if($this->title=='' || $this->id_channel=='' || $this->id_subchannel=='' || $this->id_product=='' || $this->id_email_server_sales==''  || $this->sale_user=='' || $this->sale_show_as=='' || $this->id_email_server_alerts=='' || $this->alerts_pass=='' || $this->sale_pass=='' || $this->sale_to==''|| $this->alerts_user=='' || $this->alerts_show_as=='' || $this->sale_pass=='' || $this->alerts_subject_patterns==''){
				$this->addError($attribute, 'No puede activar el registro hasta que complete la segunda instancia del mismo.');
			}
		}*/
	}
	
	protected function beforeValidate(){

				return parent::beforeValidate();
    }	
	
	public static function normalize_dates($model){
		$model->criterio=array();
					
				return $model;		
	}
	
	protected function afterFind() {

		return parent::afterFind();
	}	
	
	protected function beforeSave ()
    {	
    	$this->id_user=Yii::app()->user->id;
		$this->created_at=date("Y-m-d H:i:s");
																																									
		foreach($this->attributes as $key=>$value){
			if($value==''){
				$this->$key=NULL;
			}
		}			
		return parent::beforeSave ();
    }	
	
	public function uniqueuni($attribute,$params) {
		if($this->isNewRecord){
			$uni=Offersproduct::model()->findAllByAttributes(array('id_channel'=>$this->id_channel,'id_subchannel'=>$this->id_subchannel,'id_product'=>$this->id_product));
			if(count($uni)>0){
				$this->addError($attribute, 'La combinación de canal,subcanal y producto ya existe.');
			}
		}
	}	
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Offersproduct the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
