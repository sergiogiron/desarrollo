<?php

/**
 * This is the model class for table "config_cron".
 *
 * The followings are the available columns in table 'config_cron':
 * @property integer $id
 * @property string $cron
 * @property integer $timevar
 * @property string $lastrun
 * @property string $created_at
 * @property integer $id_user
 */
class Configcron extends CActiveRecord
{
	public $criterio;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'config_cron';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('cron, timevar, lastrun', 'required'),
			array('id_user, timevar', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, cron, timevar, lastrun, created_at, id_user', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'cron' => 'Nombre del Proceso',
			'timevar' => 'Tiempo de Ejecución',
			'lastrun' => 'Última Ejecución',
			'created_at' => 'Fecha de Creación',
			'id_user' => 'Id de Usuario',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		if($this->criterio!=''){
			$criteria->addSearchCondition('cron',$this->criterio,true, 'OR', 'LIKE');
			$criteria->addSearchCondition('timevar',$this->criterio, true, 'OR', 'LIKE');
			$criteria->addSearchCondition('lastrun',$this->criterio,true, 'OR', 'LIKE');	
		}else{	
			$criteria->compare('cron',$this->cron,true);
			$criteria->compare('timevar',$this->timevar);
			$criteria->compare('lastrun',$this->lastrun,true);
		}
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Configcron the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
