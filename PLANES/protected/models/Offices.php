<?php

/**
 * This is the model class for table "offices".
 *
 * The followings are the available columns in table 'offices':
 * @property  public $criterio;
 * @property integer $id
 * @property string $name
 * @property string $schedule
 * @property string $phone
 * @property string $fax
 * @property string $address
 * @property string $mail
 * @property string $info
 * @property integer $order
 * @property integer $responsable
 * @property integer $public
 * @property integer $enabled
 * @property string $created_at
 * @property integer $id_user
 */

class Offices extends CActiveRecord
{
	public $criterio;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'offices';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, phone, mail, info, enabled, order',  'required'),
			array('order, responsable, public, enabled, id_user', 'numerical', 'integerOnly'=>true),
			array('name, phone, fax', 'length', 'max'=>32),
			array('schedule', 'length', 'max'=>512),
			array('address', 'length', 'max'=>256),
			array('mail', 'length', 'max'=>128),
			array('info', 'length', 'max'=>1024),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, schedule, phone, fax, address, mail, info, order, responsable, public, enabled, created_at, id_user', 'safe', 'on'=>'search'),
		);
	}

	public function behaviors(){
		return array(
			// Classname => path to Class
			'ActiveRecordLogableBehavior'=>
				'application.behaviors.ActiveRecordLogableBehavior',
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(

			'usuario' => array(self::BELONGS_TO, 'Users', 'responsable'),
			'imagenes' => array(self::HAS_MANY, 'Officesimages', 'id_office', 'order'=>'imagenes.order ASC'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Sucursal',
			'schedule' => 'Horario',
			'phone' => 'Teléfono',
			'fax' => 'Fax',
			'address' => 'Dirección',
			'mail' => 'Mail',
			'info' => 'Mapa de Google',
			'order' => 'Orden',
			'responsable' => 'Responsable',
			'public' => 'Atención al público',
			'enabled' => 'Estado',
			'created_at' => 'Created At',
			'id_user' => 'Id User',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		
		if($this->criterio!=''){
			
			$criteria->addSearchCondition('name',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('phone',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('mail',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('info',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('responsable',$this->criterio,true,'OR', 'LIKE');
	
		}else{	
			$criteria->compare('id',$this->id);
			$criteria->compare('name',$this->name,true);
			$criteria->compare('schedule',$this->schedule,true);
			$criteria->compare('phone',$this->phone,true);
			$criteria->compare('fax',$this->fax,true);
			$criteria->compare('address',$this->address,true);
			$criteria->compare('mail',$this->mail,true);
			$criteria->compare('info',$this->info,true);
			$criteria->compare('order',$this->order);
			$criteria->compare('responsable',$this->responsable);
			$criteria->compare('public',$this->public);
			$criteria->compare('enabled',$this->enabled);
			$criteria->compare('created_at',$this->created_at,true);
			$criteria->compare('id_user',$this->id_user);
		}
		$criteria->order = '`order` ASC';
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Offices the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
