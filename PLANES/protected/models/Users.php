<?php

/**
 * This is the model class for table "users".
 *
 * The followings are the available columns in table 'users':
 * @property integer $id
 * @property string $id_user
 * @property string $id_perfil
 * @property string $username
 * @property integer $enabled
 * @property string $name
 * @property string $lastname
 * @property string $doc_type
 * @property integer $doc_number
 * @property integer $passport_number
 * @property string $passport_expire_date
 * @property integer $cuil
 * @property string $birthdate
 * @property string $nationality
 * @property string $status_civil
 * @property integer $phone
 * @property integer $celular
 * @property string $email
 * @property string $address
 * @property string $state
 * @property string $postal_code
 * @property string $studies
 * @property string $several_studies
 * @property string $start_date
 * @property integer $file
 * @property string $img_type
 * @property string $location
 * @property string $sector
 * @property string $job
 * @property string $top_boss
 * @property integer $interno
 * @property integer $personal_phone
 * @property integer $mobile
 * @property string $created_at
 * @property string $image
 */
class Users extends CActiveRecord
{
	public $criterio;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'usuarios';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, lastname, doc_type, doc_number, birthdate, nationality, status_civil, email, start_date, location, sector, job, file', 'required'),
			//array('image', 'file', 'types'=>'jpg, gif, png', 'safe' => false),
			array('enabled, doc_number, passport_number, cuil, phone, celular, file, interno, personal_phone, mobile', 'numerical', 'integerOnly'=>true),
			array('id_user, name, lastname', 'length', 'max'=>60),
			array('username,email', 'unique', 'attributeName'=>'username'),
			array('email', 'unique', 'attributeName'=>'email'),
			array('username, doc_type', 'length', 'max'=>20),
			array('passport_expire_date, start_date', 'length', 'max'=>10),
			array('nationality, status_civil, location', 'length', 'max'=>30),
			array('email', 'length', 'max'=>75),
			array('address, job', 'length', 'max'=>100),
			array('state, studies, top_boss', 'length', 'max'=>25),
			array('postal_code', 'length', 'max'=>8),
			array('several_studies', 'length', 'max'=>1024),
			array('img_type', 'length', 'max'=>3),
			array('sector', 'length', 'max'=>50),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, id_user,  username, enabled, name, lastname, doc_type, doc_number, passport_number, passport_expire_date, cuil, birthdate, nationality, status_civil, phone, celular, email, address, state, postal_code, studies, several_studies, start_date, file, img_type, location, sector, job, top_boss, interno, personal_phone, mobile, created_at', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'perfiles' => array(self::HAS_MANY, 'Profileuser', 'id_user'),
			'imagenes' => array(self::HAS_MANY, 'Usersimages', 'id_user', 'order'=>'imagenes.order ASC'),
		);
	}
	
	public function behaviors(){
		return array(
			// Classname => path to Class
			'ActiveRecordLogableBehavior'=>
				'application.behaviors.ActiveRecordLogableBehavior',
		);
	}
	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_user' => 'Id User',
			'username' => 'Usuario',
			'enabled' => 'Estado',
			'name' => 'Nombre',
			'lastname' => 'Apellido',
			'doc_type' => 'Tipo',
			'doc_number' => 'Documento',
			'passport_number' => 'Pasaporte',
			'passport_expire_date' => 'Vencimiento',
			'cuil' => 'Cuil',
			'birthdate' => 'Nacimiento',
			'nationality' => 'Nacionalidad',
			'status_civil' => 'Estado civil',
			'phone_code' => 'Código',
			'phone' => 'Teléfono',
			'celular' => 'Celular',
			'email' => 'Email',
			'address' => 'Dirección',
			'state' => 'Provincia',
			'postal_code' => 'Código Postal',
			'studies' => 'Estudios',
			'several_studies' => 'Otros estudios',
			'start_date' => 'Fecha de Ingreso',
			'file' => 'Legajo',
			'img_type' => 'Img Type',
			'location' => 'Ubicación',
			'sector' => 'Sector',
			'job' => 'Puesto',
			'top_boss' => 'Superior',
			'interno' => 'Interno',
			'personal_phone' => 'Línea Directa',
			'mobile' => 'Mobile',
			'created_at' => 'Fecha de Alta',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		if($this->criterio!=''){
			$criteria->addSearchCondition('username',$this->criterio, true, 'OR', 'LIKE');
			$criteria->addSearchCondition('name',$this->criterio, true, 'OR', 'LIKE');
			$criteria->addSearchCondition('lastname',$this->criterio, true, 'OR', 'LIKE');
			$criteria->addSearchCondition('doc_type',$this->criterio, true, 'OR', 'LIKE');
			$criteria->addSearchCondition('doc_number',$this->criterio, true,'OR', 'LIKE');
			$criteria->addSearchCondition('passport_number',$this->criterio,true, 'OR', 'LIKE');
			$criteria->addSearchCondition('passport_expire_date',$this->criterio, true, 'OR', 'LIKE');
			$criteria->addSearchCondition('cuil',$this->criterio,true, 'OR', 'LIKE');
			$criteria->addSearchCondition('birthdate',$this->criterio, true, 'OR', 'LIKE');
			$criteria->addSearchCondition('nationality',$this->criterio, true, 'OR', 'LIKE');
			$criteria->addSearchCondition('status_civil',$this->criterio, true, 'OR', 'LIKE');
			$criteria->addSearchCondition('phone',$this->criterio,true, 'OR', 'LIKE');
			$criteria->addSearchCondition('celular',$this->criterio, true,'OR', 'LIKE');
			$criteria->addSearchCondition('email',$this->criterio, true, 'OR', 'LIKE');
			$criteria->addSearchCondition('address',$this->criterio, true, 'OR', 'LIKE');
			$criteria->addSearchCondition('state',$this->criterio, true, 'OR', 'LIKE');
			$criteria->addSearchCondition('postal_code',$this->criterio, true, 'OR', 'LIKE');
			$criteria->addSearchCondition('studies',$this->criterio, true, 'OR', 'LIKE');
			$criteria->addSearchCondition('several_studies',$this->criterio, true, 'OR', 'LIKE');
			$criteria->addSearchCondition('start_date',$this->criterio, true, 'OR', 'LIKE');
			$criteria->addSearchCondition('img_type',$this->criterio, true, 'OR', 'LIKE');
			$criteria->addSearchCondition('location',$this->criterio, true, 'OR', 'LIKE');
			$criteria->addSearchCondition('sector',$this->criterio, true, 'OR', 'LIKE');
			$criteria->addSearchCondition('job',$this->criterio, true, 'OR', 'LIKE');
			$criteria->addSearchCondition('top_boss',$this->criterio, true, 'OR', 'LIKE');
			$criteria->addSearchCondition('interno',$this->criterio,true, 'OR', 'LIKE');
			$criteria->addSearchCondition('personal_phone',$this->criterio,true, 'OR', 'LIKE');
			$criteria->addSearchCondition('mobile',$this->criterio, true,'OR', 'LIKE');		
		}else{	
			$criteria->compare('username',$this->username,true);
			$criteria->compare('enabled',$this->enabled);
			$criteria->compare('name',$this->name,true);
			$criteria->compare('lastname',$this->lastname,true);
			$criteria->compare('doc_type',$this->doc_type,true);
			$criteria->compare('doc_number',$this->doc_number);
			$criteria->compare('passport_number',$this->passport_number);
			$criteria->compare('passport_expire_date',$this->passport_expire_date,true);
			$criteria->compare('cuil',$this->cuil);
			$criteria->compare('birthdate',$this->birthdate,true);
			$criteria->compare('nationality',$this->nationality,true);
			$criteria->compare('status_civil',$this->status_civil,true);
			$criteria->compare('phone',$this->phone);
			$criteria->compare('celular',$this->celular);
			$criteria->compare('email',$this->email,true);
			$criteria->compare('address',$this->address,true);
			$criteria->compare('state',$this->state,true);
			$criteria->compare('postal_code',$this->postal_code,true);
			$criteria->compare('studies',$this->studies,true);
			$criteria->compare('several_studies',$this->several_studies,true);
			$criteria->compare('start_date',$this->start_date,true);
			$criteria->compare('location',$this->location,true);
			$criteria->compare('sector',$this->sector,true);
			$criteria->compare('job',$this->job,true);
			$criteria->compare('top_boss',$this->top_boss,true);
			$criteria->compare('interno',$this->interno);
			$criteria->compare('personal_phone',$this->personal_phone);
			$criteria->compare('mobile',$this->mobile);
		}	

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Users the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	public static function normalize_dates($model){
	/*$model->criterio=array();
		$from_date= explode(" ",$model->from_date);
		$model->from_date=$from_date[0];
		$model->criterio=array_merge($model->criterio);
		$to_date= explode(" ",$model->to_date);
		$model->to_date=$to_date[0];
		$model->criterio=array_merge($model->criterio);
		return $model;		*/
	}
	
	protected function afterFind() {
		$this->birthdate =Utils::date_spa($this->birthdate);
		return parent::afterFind();
	}	

	protected function beforeSave(){
		$this->created_at=date("Y-m-d H:i:s");

		$birthdate = DateTime::createFromFormat('d/m/Y', $this->birthdate);
		$this->birthdate = $birthdate->format('Y-m-d');
		return parent::beforeSave();
	}		
	
}
