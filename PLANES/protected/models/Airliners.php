<?php

/**
 * This is the model class for table "airliners".
 *
 * The followings are the available columns in table 'airliners':
 * @property  public $criterio;
 * @property integer $id
 * @property string $code
 * @property string $title
 * @property integer $enabled
 * @property string $created_at
 * @property integer $id_user
 */
class Airliners extends CActiveRecord
{
	public $criterio;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'airliners';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(					
				array('code,title', 'required','message'=>'Debe completar este campo.'),
				array('code,title', 'unique'),
				array('enabled, id_user', 'numerical', 'integerOnly'=>true),
				array('code', 'length', 'max'=>2),
				array('title', 'length', 'max'=>32),
				
					// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, code, title, enabled, created_at, id_user', 'safe', 'on'=>'search'),
		);
	}

	public function behaviors(){
		return array(
			// Classname => path to Class
			'ActiveRecordLogableBehavior'=>
				'application.behaviors.ActiveRecordLogableBehavior',
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'imagenes' => array(self::HAS_MANY, 'Airlinersimages', 'id_airliners', 'order'=>'imagenes.order ASC'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'code' => 'Código IATA',
			'name' => 'Aerolínea',
			
			'enabled' => 'Estado',
			'created_at' => 'Creado',
			'id_user' => 'Usuario',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		
		if($this->criterio!=''){
			$criteria->addSearchCondition('id',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('code',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('title',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('enabled',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('created_at',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('id_user',$this->criterio,true,'OR', 'LIKE');
	
		}else{	
			$criteria->compare('id',$this->id);
			$criteria->compare('code',$this->code,true);
			$criteria->compare('title',$this->title,true);
			$criteria->compare('enabled',$this->enabled);
			$criteria->compare('created_at',$this->created_at,true);
			$criteria->compare('id_user',$this->id_user);
		}
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	protected function beforeValidate(){
						
						
						
						
						
						
						
				return parent::beforeValidate();
    }	
	
	public static function normalize_dates($model){
		$model->criterio=array();
					
					
					
					
					
					
					
				return $model;		
	}
	
	protected function afterFind() {
						
					
						
					
						
					
						
					
						
					
						
					
						
					
						
		return parent::afterFind();
	}	
	
	protected function beforeSave ()
    {	
		$this->created_at=date("Y-m-d H:i:s");
																																													return parent::beforeSave ();
    }	
	
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Airliners the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
