<?php

/**
 * This is the model class for table "profile".
 *
 * The followings are the available columns in table 'profile':
 * @property integer $id
 * @property integer $id_user
 * @property string $description
 * @property string $title
 * @property string $created_at
 * @property integer $enabled
 */
class Profile extends CActiveRecord
{
	public $criterio;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'profile';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_user, description, title, created_at, enabled', 'required'),
			array('id_user, enabled', 'numerical', 'integerOnly'=>true),
			array('description', 'length', 'max'=>60),
			array('title', 'length', 'max'=>20),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, id_user, description, title, created_at, enabled', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'menues' => array(self::HAS_MANY, 'Profilemenurelation', 'id_profile'),
			'widgets' => array(self::HAS_MANY, 'Profilewidgetrelation', 'id_profile'),
		);
	}
	
	public function behaviors(){
		return array(
			// Classname => path to Class
			'ActiveRecordLogableBehavior'=>
				'application.behaviors.ActiveRecordLogableBehavior',
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_user' => 'Id User',
			'description' => 'Descripción',
			'title' => 'TÍtulo',
			'created_at' => 'Created At',
			'enabled' => 'Estado',
		);
	}

	// protected function beforeSave(){
	// 	$this->created_at=date("Y-m-d H:i:s");
	// 	parent::beforeSave();
	// }	
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		if($this->criterio!=''){
			$criteria->addSearchCondition('title', $this->criterio, true, 'OR', 'LIKE');
			$criteria->addSearchCondition('description', $this->criterio, true, 'OR', 'LIKE');		
		}else{	
			$criteria->compare('description',$this->description,true);
			$criteria->compare('title',$this->title,true);
		}

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Profile the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
