<?php

/**
 * This is the model class for table "email_server".
 *
 * The followings are the available columns in table 'email_server':
 * @property  public $criterio;
 * @property integer $id
 * @property string $title
 * @property string $host_server_mail
 * @property string $Port
 * @property integer $ssl
 * @property integer $enabled
 * @property string $created_at
 * @property integer $id_user
 */
class Emailserver extends CActiveRecord
{
	public $criterio;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'email_server';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
					
				array('host_server_mail, title, Port, ssl, enabled', 'required','message'=>'Debe completar este campo.'),
				array('title', 'unique', 'attributeName'=>'title'),
				array('ssl, enabled, id_user', 'numerical', 'integerOnly'=>true),
				array('title, host_server_mail', 'length', 'max'=>45),
				array('Port', 'length', 'max'=>5),
					// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, title, host_server_mail, Port, ssl, enabled, created_at, id_user', 'safe', 'on'=>'search'),
		);
	}

	public function behaviors(){
		return array(
			// Classname => path to Class
			'ActiveRecordLogableBehavior'=>
				'application.behaviors.ActiveRecordLogableBehavior',
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'title' => 'Título',
			'host_server_mail' => 'Host',
			'Port' => 'Puerto',
			'ssl' => 'Ssl',
			'enabled' => 'Estado',
			'created_at' => 'Created At',
			'id_user' => 'Id User',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		
		if($this->criterio!=''){
			$criteria->addSearchCondition('id',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('title',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('host_server_mail',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('Port',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('ssl',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('enabled',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('created_at',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('id_user',$this->criterio,true,'OR', 'LIKE');
	
		}else{	
			$criteria->compare('id',$this->id);
			$criteria->compare('title',$this->title,true);
			$criteria->compare('host_server_mail',$this->host_server_mail,true);
			$criteria->compare('Port',$this->Port,true);
			$criteria->compare('ssl',$this->ssl);
			$criteria->compare('enabled',$this->enabled);
			$criteria->compare('created_at',$this->created_at,true);
			$criteria->compare('id_user',$this->id_user);
		}
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	protected function beforeValidate(){
		return parent::beforeValidate();
    }	
	
	public static function normalize_dates($model){
		$model->criterio=array();
		return $model;		
	}
	
	protected function afterFind() {
		return parent::afterFind();
	}	
	
	protected function beforeSave ()
    {	
		$this->created_at=date("Y-m-d H:i:s");
		$this->id_user=Yii::app()->user->id;
		return parent::beforeSave ();
    }	
	
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Emailserver the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
