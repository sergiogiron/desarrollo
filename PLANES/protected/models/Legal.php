<?php

/**
 * This is the model class for table "legal".
 *
 * The followings are the available columns in table 'legal':
 * @property integer $id
 * @property string $description
 * @property integer $id_entity
 * @property string $title
 *
 * The followings are the available model relations:
 * @property TransactionCost[] $transactionCosts
 */
class Legal extends CActiveRecord
{
	public $criterio;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'legal';
	}

	public function behaviors()
	{
	    return array(
	        // Classname => path to Class
	        'ActiveRecordLogableBehavior'=>
	            'application.behaviors.ActiveRecordLogableBehavior',
	    );
	}
	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_entity', 'numerical', 'integerOnly'=>true),
			array('title', 'length', 'max'=>45),
			array('description', 'safe'),
			array('description,id_entity,title','required'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, description, id_entity, title', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'transactionCosts' => array(self::HAS_MANY, 'TransactionCost', 'id_legal'),
			'entidad' => array(self::BELONGS_TO, 'Entity', 'id_entity'),

		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'description' => 'Descripción',
			'id_entity' => 'Entidad',
			'title' => 'Título',
			'enabled' => 'Estado',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		
		$criteria=new CDbCriteria;
		if($this->criterio!=''){
			$criteria->addSearchCondition('title',$this->criterio, true,'OR', 'LIKE');
			$criteria->addSearchCondition('description',$this->criterio, true,'OR', 'LIKE');
			$criteria->addSearchCondition('entidad.title',$this->criterio,true,'OR', 'LIKE');
			$criteria->with = array('entidad');	
		}else{	
			$criteria->compare('description',$this->description,true);
			$criteria->compare('id_entity',$this->id_entity);
			$criteria->compare('title',$this->title,true);
		}	


		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Legal the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
