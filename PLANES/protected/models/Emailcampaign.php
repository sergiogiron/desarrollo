<?php

/**
 * This is the model class for table "email_campaign".
 *
 * The followings are the available columns in table 'email_campaign':
 * @property integer $id
 * @property integer $id_user
 * @property integer $id_email_header
 * @property integer $id_email_footer
 * @property string $name
 * @property string $subject
 * @property string $body
 * @property string $last_updated_stat
 * @property string $last_updated_stat_detail 
 * @property integer $processing_stats
 * @property string $date_to
 * @property string $send_date
 * @property string $send_hour
 * @property integer $status
 * @property integer $enabled
 * @property string $created_at
 * @property integer $id_datamedia
 * @property integer $id_list
 * @property integer $suscribers
 * @property tinyint $deleted
 */
class Emailcampaign extends CActiveRecord
{
	public $criterio;
	public $criterio2;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'email_campaign';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('name, subject, body, enabled', 'required'),
			array('name', 'validador', 'attributeName'=>'name'),
			array('id, id_user, id_email_header, id_email_footer, status, enabled', 'numerical', 'integerOnly'=>true),
			array('name, subject', 'length', 'max'=>255),
			array('send_hour', 'length', 'max'=>32),
			array('send_date, created_at', 'safe'), 
			array('id, id_user, id_email_header, id_email_footer, name, subject, body, date_to, send_date, send_hour, status, enabled, created_at', 'safe', 'on'=>'search'),
			array('date_to', 'isValidDate', 'except'=> 'send_real'),
			 

		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'emailheader' => array(self::BELONGS_TO, 'Emailheader', 'id_email_header'),
			'emailfooter' => array(self::BELONGS_TO, 'Emailfooter', 'id_email_footer'),
			'vendedor' => array(self::HAS_MANY, 'User', 'id_vendedor'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels(){
		return array(
			'id' => 'ID',
			'id_user' => 'Id User',
			'id_email_header' => 'Header',
			'id_email_footer' => 'Footer',
			'name' => 'Nombre',
			'subject' => 'Asunto',
			'body' => 'Cuerpo',
			'last_updated_stat' => 'Fecha de actualizacion de estadisticas',
			'last_updated_stat_detail' => 'Fecha de actualizacion de los detalles de la estadistica',
			'date_to' => 'Fecha de finalización',
			'send_date' => 'Fecha de envío',
			'send_hour' => 'Hora de envio',
			'status' => 'Estado',
			'enabled' => 'Estado',
			'created_at' => 'Fecha de creación',
			'deleted' => 'Eliminada',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.
		$criteria=new CDbCriteria;
		if($this->criterio!=''){
			$criteria->addSearchCondition('name',$this->criterio, true, 'OR', 'LIKE');
			$criteria->addSearchCondition('subject',$this->criterio, true, 'OR', 'LIKE');
			$criteria->addSearchCondition('body',$this->criterio, true, 'OR', 'LIKE');
			$criteria->addSearchCondition('date_to',$this->criterio, true, 'OR', 'LIKE');
			$criteria->addSearchCondition('send_date',$this->criterio, true, 'OR', 'LIKE');
			$criteria->addSearchCondition('send_hour',$this->criterio, true, 'OR', 'LIKE');			
			$criteria->addSearchCondition('enabled',$this->criterio, true, 'OR', 'LIKE');
		}else{
			$criteria->compare('name',$this->name,true);
			$criteria->compare('subject',$this->subject,true);
			$criteria->compare('body',$this->body,true);
			$criteria->compare('date_to',$this->date_to,true);
			$criteria->compare('send_date',$this->send_date,true);
			$criteria->compare('send_hour',$this->send_hour,true);				
			$criteria->compare('enabled',$this->enabled);
		}
		$criteria->compare('deleted',0);
		$criteria->compare('status',1);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	
	public function search2() //resultados segunda tab
	{
		// Busca solo las campañas que hayan sido enviadas enviadas
		$criteria=new CDbCriteria;
		if($this->criterio2!=''){
			$criteria->addSearchCondition('name',$this->criterio2, true, 'OR', 'LIKE');
			$criteria->addSearchCondition('subject',$this->criterio2, true, 'OR', 'LIKE');
			$criteria->addSearchCondition('body',$this->criterio2, true, 'OR', 'LIKE');
			$criteria->addSearchCondition('date_to',$this->criterio2, true, 'OR', 'LIKE');
			$criteria->addSearchCondition('send_date',$this->criterio2, true, 'OR', 'LIKE');
			$criteria->addSearchCondition('send_hour',$this->criterio2, true, 'OR', 'LIKE');			
			$criteria->addSearchCondition('enabled',$this->criterio2, true, 'OR', 'LIKE');	
		}else{
			$criteria->compare('name',$this->name,true);
			$criteria->compare('subject',$this->subject,true);
			$criteria->compare('body',$this->body,true);
			$criteria->compare('date_to',$this->date_to,true);
			$criteria->compare('send_date',$this->send_date,true);
			$criteria->compare('send_hour',$this->send_hour,true);				
			$criteria->compare('enabled',$this->enabled);
		}
		$criteria->compare('deleted',0);
		$criteria->compare('status',3, true,'OR');		
		$criteria->compare('status',2, true,'OR');	
		$criteria->addCondition('status != 1');
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Emailcampaign the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	 
	
	
	public function validateSendHour($attribute, $params)
	{
	  if (!empty($this->send_date) && $this->send_date!="0000-00-00") {
			$ev = CValidator::createValidator('required', $this, $attribute, $params);
			$ev->validate($this);
	  }
	}
	
	public function validador($attribute,$params) {
		$campaigns=Emailcampaign::model()->findAllByAttributes(array('name'=>$this->name),'deleted=0');
		if(count($campaigns)>0){
			foreach($campaigns as $c){
				if($this->isNewRecord || $this->id != $c['id']){
					$this->addError($attribute, 'El nombre está en uso.');
				}
			}
		}
	}
	
	public function isValidDate($attribute, $params)
	{	 
		 
		if(empty($this->$attribute))
		{
			$this->addError($attribute, 'La fecha de finalización es obligatoria');
		}
		
		if(!strtotime($this->$attribute))
		{
			$this->addError($attribute, 'La fecha de finalización no es una fecha válida');
		}
		$today = date("d-m-Y");

		if(strtotime($this->$attribute)<strtotime($today))
		{
			$this->addError($attribute, 'La fecha de finalización debe ser mayor a hoy');
		}
		if(!empty($this->send_date)){
			if(strtotime($this->$attribute)<strtotime($this->send_date))
			{
				$this->addError($attribute, 'La fecha de Finalización de la campaña debe ser posterior al día de envío.');
			}
		}
	}



protected function beforeValidate()
{
	if(!empty($this->date_to))
	{
		$var= date_create_from_format('d/m/Y',$this->date_to);
		$this->date_to= $var->format('d-m-Y');
	}

	if(!empty($this->send_date)){ 
		$var= date_create_from_format('d/m/Y',$this->send_date);
		$this->send_date= $var->format('d-m-Y'); 
		}
	 
return parent::beforeValidate();
}	



protected function afterValidate(){

	if(!empty($this->date_to))
	{
		if ($this->date_to instanceof DateTime) 
		{
			$this->date_to = $this->date_to->format('d/m/Y');
		}
		else
			{
			$this->date_to = date_create_from_format('d-m-Y',$this->date_to);
			$this->date_to = $this->date_to->format('d/m/Y');
			}
	}

	if(!empty($this->send_date)){ 
			$this->send_date = date_create_from_format('d-m-Y',$this->send_date);
			$this->send_date = $this->send_date->format('d/m/Y');
		}

	
	return parent::afterValidate();
}



protected function afterFind() {
	if(!empty($this->date_to)){
			$this->date_to =Utils::date_spa($this->date_to);}

	if(!empty($this->send_date)){
			$this->send_date =Utils::date_spa($this->send_date);}
			
	return parent::afterFind();
}
	


protected function beforeSave ()
{
	if(empty($this->created_at)) 
		{
		$this->created_at=date("Y-m-d H:i:s");
		}

	 if(!empty($this->date_to)){
		$date_date_to = DateTime::createFromFormat('d/m/Y', $this->date_to);
		$this->date_to=$date_date_to->format('Y-m-d');}

	 if(!empty($this->send_date)){
		$date_send_date = DateTime::createFromFormat('d/m/Y', $this->send_date);
		$this->send_date=$date_send_date->format('Y-m-d');} 

	return parent::beforeSave ();
}





}
