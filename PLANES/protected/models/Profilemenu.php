<?php

/**
 * This is the model class for table "profile_menu".
 *
 * The followings are the available columns in table 'profile_menu':
 * @property integer $id
 * @property integer $id_user
 * @property integer $id_menu
 * @property integer $enabled
 * @property string $icono
 * @property string $title
 * @property string $description
 * @property string $url
 * @property string $created_at
 */
class Profilemenu extends CActiveRecord
{
	public $criterio;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'profile_menu';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array( 'title, description', 'required'),
			array('id_user,id_menu, enabled', 'numerical', 'integerOnly'=>true),
			array('icono', 'length', 'max'=>100),
			array('title', 'length', 'max'=>64),
			array('description', 'length', 'max'=>64),
			array('url', 'length', 'max'=>128),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, id_user, enabled, icono, title, description, url, created_at', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'menu' => array(self::BELONGS_TO, 'Profilemenu', 'id_menu'),
		);
	}
	
	public function behaviors(){
		return array(
			// Classname => path to Class
			'ActiveRecordLogableBehavior'=>
				'application.behaviors.ActiveRecordLogableBehavior',
		);
	}
	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_user' => 'Id User',
			'id_menu' => 'Menu padre',
			'enabled' => 'Estado',
			'icono' => 'Icono',
			'title' => 'Titulo',
			'description' => 'Descripcion',
			'url' => 'Url',
			'created_at' => 'Created At',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		if($this->criterio!=''){
			$criteria->addSearchCondition('title', $this->criterio, true, 'OR', 'LIKE');
			$criteria->addSearchCondition('description', $this->criterio, true, 'OR', 'LIKE');
			$criteria->addSearchCondition('url', $this->criterio, true, 'OR', 'LIKE');			
		}else{	
			$criteria->compare('id',$this->id);
			$criteria->compare('id_user',$this->id_user);
			$criteria->compare('enabled',$this->enabled);
			$criteria->compare('icono',$this->icono,true);
			$criteria->compare('title',$this->title,true);
			$criteria->compare('description',$this->description,true);
			$criteria->compare('url',$this->url,true);
			$criteria->compare('created_at',$this->created_at,true);
		}

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	protected function beforeSave(){
		$this->created_at=date("Y-m-d H:i:s");
		return  parent::beforeSave();
	}	
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Profilemenu the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
