<?php

/**
 * This is the model class for table "highlight_products".
 *
 * The followings are the available columns in table 'highlight_products':
 * @property  public $criterio;
 * @property integer $id
 * @property integer $id_provider
 * @property integer $id_product
 * @property integer $id_highlights_criteria
 * @property string $highlight_order_criteria
 * @property integer $Number_of_results
 * @property string $title
 * @property string $from_date
 * @property string $to_date
 * @property integer $offer
 * @property integer $highlight
 * @property string $order
 * @property integer $enabled
 * @property string $created_at
 * @property integer $id_user
 * @property integer $id_channel
 * @property integer $id_subchannel
 *
 * The followings are the available model relations:
 * @property HighlightsCriteria $idHighlightsCriteria
 * @property Products $idProduct
 * @property Providers $idProvider
 */
class HighlightProducts extends CActiveRecord
{
	public $criterio;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'highlight_products';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
				//array('id_product', 'ext.validators.UniqueattributesValidator', 'with'=>'id_channel,id_subchannel,id_provider'),
			//array('from_date, to_date', 'date', 'format'=>'dd/mm/yyyy hh:mm:ss', 'message'=>'{attribute}: no es una fecha'),
					
				//array('id', 'required','message'=>'Debe completar este campo.'),
				array('id_channel,id_subchannel,id_provider,highlight_dynamic,highlight_static,offer,highlight,', 'uniqueuni', 'attributeName'=>'id_channel,id_subchannel,id_provider,highlight_dynamic,highlight_static,offer,highlight'),


				array('id, id_provider, id_product, id_highlights_criteria,  Number_of_results, offer, highlight, order, enabled, id_user,id_channel, id_subchannel,highlight_static,highlight_dynamic', 'numerical', 'integerOnly'=>true),
				array('title', 'length', 'max'=>64),
				array('ids_paquetes', 'length', 'max'=>1024),
				array('highlight_order_criteria', 'length', 'max'=>4),
				array('from_date, to_date, created_at', 'safe'),

				array('title,id_provider, enabled, id_channel, id_subchannel,from_date,to_date', 'required'),
					// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, id_provider, id_product, id_highlights_criteria, highlight_order_criteria, Number_of_results, title, from_date, to_date, offer, highlight, order, enabled, created_at, id_user', 'safe', 'on'=>'search'),
		);
	}

	public function behaviors(){
		return array(
			// Classname => path to Class
			'ActiveRecordLogableBehavior'=>
				'application.behaviors.ActiveRecordLogableBehavior',
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idHighlightsCriteria' => array(self::BELONGS_TO, 'HighlightsCriteria', 'id_highlights_criteria'),
			'idProduct' => array(self::BELONGS_TO, 'Products', 'id_product'),
			'idProvider' => array(self::BELONGS_TO, 'Providers', 'id_provider'),
			'canal' => array(self::BELONGS_TO, 'Channels', 'id_channel'),
			'subcanal' => array(self::BELONGS_TO, 'Subchannels', 'id_subchannel'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'Id Productos Destacados',
			'id_provider' => 'Proveedor',
			'id_product' => 'Producto',
			'id_highlights_criteria' => 'Criterio',
			'highlight_order_criteria' => 'Ordenar',
			'Number_of_results' => 'Cant. Resultados',
			'title' => 'Titulo Destacado',
			'from_date' => 'Vigencia Desde',
			'to_date' => 'Vigencia Hasta',
			'ids_paquetes' => 'ID\'s Paquetes',

			'highlight_static' => 'Estático',
			'highlight_dynamic' => 'Dinámico',
			'offer' => 'Oferta',
			'highlight' => 'Destacado',
			'order' => 'Prioridad',
			'enabled' => 'Estado',
			'created_at' => 'Created At',
			'id_user' => 'Id User',
			'id_channel' => 'Canal',
			'id_subchannel' => 'Subcanal'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		
		if($this->criterio!=''){
			$criteria->addSearchCondition('highlight_order_criteria',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('Number_of_results',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('t.title',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('from_date',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('to_date',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('offer',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('highlight',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('canal.title',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('subcanal.title',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('idProduct.title',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('idProvider.title',$this->criterio,true,'OR', 'LIKE');
			$criteria->with = array('subcanal','canal','idProduct','idProvider','idHighlightsCriteria');		
	
		}else{	
			$criteria->compare('id',$this->id);
			$criteria->compare('id_provider',$this->id_provider);
			$criteria->compare('id_product',$this->id_product);
			$criteria->compare('id_highlights_criteria',$this->id_highlights_criteria);
			$criteria->compare('highlight_order_criteria',$this->highlight_order_criteria);
			$criteria->compare('Number_of_results',$this->Number_of_results);
			$criteria->compare('title',$this->title,true);
			$criteria->compare('from_date',$this->from_date,true);
			$criteria->compare('to_date',$this->to_date,true);
			$criteria->compare('offer',$this->offer);
			$criteria->compare('highlight',$this->highlight);
			$criteria->compare('order',$this->order);
			$criteria->compare('enabled',$this->enabled);
			$criteria->compare('created_at',$this->created_at,true);
			$criteria->compare('id_user',$this->id_user);
			$criteria->compare('id_channel',$this->id_channel);
			$criteria->compare('id_subchannel',$this->id_subchannel);
		}
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	protected function beforeValidate(){				
		if($this->from_date!=''){
			$this->from_date = date_create_from_format("d/m/Y",$this->from_date);
			$this->from_date =date_format( $this->from_date,"d/m/Y");
		}
				
		if($this->to_date!='' ){
			$this->to_date = date_create_from_format("d/m/Y",$this->to_date);
			$this->to_date =date_format( $this->to_date,"d/m/Y");
		}
		return parent::beforeValidate();
    }	

    public function afterValidate(){
        if($this->highlight_dynamic==1 && ($this->id_highlights_criteria =='')) {
            $this->addError('highlight_dynamic','Todos los campos dinamicos son obligatorios');
            return; // or continue
        }
        if($this->highlight_static==1 && ($this->ids_paquetes =='')) {
            $this->addError('ids_paquetes','Debe elegir al menos un id para destacar');
            return; // or continue
        }
        return parent::afterValidate();
	}
	
	public static function normalize_dates($model){
		$model->criterio=array();
		$from_date= explode(" ",$model->from_date);
		$model->from_date=$from_date[0];
		$model->criterio=array_merge($model->criterio);
		$to_date= explode(" ",$model->to_date);
		$model->to_date=$to_date[0];
		$model->criterio=array_merge($model->criterio);
		return $model;		
	}
	
	protected function afterFind() {
		$this->from_date =Utils::date_spa($this->from_date);
		$this->to_date =Utils::date_spa($this->to_date);
		return parent::afterFind();
	}	

	public function uniqueuni($attribute,$params) {
		if($this->isNewRecord){
			$cat = array(	'id_channel'=>$this->id_channel,
							'id_subchannel'=>$this->id_subchannel,
							'id_provider'=>$this->id_provider,
							'highlight_dynamic'=>$this->highlight_dynamic,
							'highlight_static'=>$this->highlight_static,
							'offer'=>$this->offer,
							'highlight'=>$this->highlight
							);
		$uni=HighlightProducts::model()->findAllByAttributes($cat);
			if(count($uni)>0){
				$this->addError($attribute, 'La combinación ya existe.');
			}
		}
	}

	protected function beforeSave()
    {	
    	$this->id_product = 3;
    	$this->id_user=Yii::app()->user->id;
		$this->created_at=date("Y-m-d");
		$m_date = DateTime::createFromFormat('d/m/Y', $this->from_date);
		$this->from_date=$m_date->format('Y-m-d');
		$date_to_date = DateTime::createFromFormat('d/m/Y', $this->to_date);
		$this->to_date=$date_to_date->format('Y-m-d');
		return parent::beforeSave();
    }	
	
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return HighlightProducts the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
