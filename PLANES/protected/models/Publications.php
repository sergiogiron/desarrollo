<?php

/**
 * This is the model class for table "publications".
 *
 * The followings are the available columns in table 'publications':
 * @property  public $criterio;
 * @property integer $id
 * @property integer $id_user
 * @property string $content
 * @property integer $id_channel
 * @property integer $id_subchannel
 * @property integer $id_routes_all
 * @property string $publication_date
 * @property string $model
 * @property string $created_at
 */
class Publications extends CActiveRecord
{
	public $criterio;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'publications';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_user, content, publication_date, model', 'required'),
			array('id_user, id_routes_all, id_channel, id_subchannel', 'numerical', 'integerOnly'=>true),
			array('model', 'length', 'max'=>64),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, id_user, content, id_routes_all, id_channel, id_subchannel, publication_date, model, created_at', 'safe', 'on'=>'search'),
		);
	}

	public function behaviors(){
		return array(
			// Classname => path to Class
			'ActiveRecordLogableBehavior'=>
				'application.behaviors.ActiveRecordLogableBehavior',
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'user' => array(self::BELONGS_TO, 'Users', 'id_user'),
			'channels' => array(self::BELONGS_TO, 'Channels', 'id_channel'),
			'subchannels' => array(self::BELONGS_TO, 'Subchannels', 'id_subchannel'),		
			'route_general' => array(self::BELONGS_TO, 'Publicationsroutesall', 'id_routes_all'),		
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_user' => 'Id User',
			'id_routes_all' => 'Publicaciones generales',
			'content' => 'Content',
			'id_channel' => 'Canal',
			'id_subchannel' => 'Subcanal',
			'publication_date' => 'Fecha de publicación',
			'model' => 'Modelo',
			'created_at' => 'Created At',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		
		if($this->criterio!=''){
			$criteria->addSearchCondition('t.content',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('subchannels.title',$this->criterio,true,'OR', 'LIKE');			
			$criteria->addSearchCondition('route_general.name',$this->criterio,true,'OR', 'LIKE');			
			$criteria->addSearchCondition('channels.title',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('t.publication_date',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('t.model',$this->criterio,true,'OR', 'LIKE');
			$criteria->with = array('subchannels','channels','route_general');
	
		}else{	
			$criteria->compare('content',$this->content,true);
			$criteria->compare('id_channel',$this->id_channel);
			$criteria->compare('id_routes_all',$this->id_routes_all);
			$criteria->compare('id_subchannel',$this->id_subchannel);
			$criteria->compare('publication_date',$this->publication_date,true);
			$criteria->compare('model',$this->model,true);
		}
		$criteria->order='id desc';
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $classtitle active record class title.
	 * @return Publications the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
