<?php

/**
 * This is the model class for table "iata_country_master".
 *
 * The followings are the available columns in table 'iata_country_master':
 * @property  public $criterio;
 * @property integer $id
 * @property string $IATA
 * @property string $Country
 * @property string $currency
 * @property string $currency_name
 *
 * The followings are the available model relations:
 * @property ExcludeRules[] $excludeRules
 */
class Iatacountrymaster extends CActiveRecord
{
	public $criterio;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'iata_country_master';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(

				array('IATA, Country, currency, currency_name', 'required','message'=>'Debe completar este campo.'),
				array('IATA, currency', 'length', 'max'=>3),
				array('Country, currency_name', 'length', 'max'=>40),
					// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, IATA, Country, currency, currency_name', 'safe', 'on'=>'search'),
		);
	}

	public function behaviors(){
		return array(
			// Classname => path to Class
			'ActiveRecordLogableBehavior'=>
				'application.behaviors.ActiveRecordLogableBehavior',
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'excludeRules' => array(self::HAS_MANY, 'ExcludeRules', 'id_iata_country_master'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'IATA' => 'Iata',
			'Country' => 'Country',
			'currency' => 'Currency',
			'currency_name' => 'Currency Name',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		if($this->criterio!=''){
			$criteria->addSearchCondition('id',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('IATA',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('Country',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('currency',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('currency_name',$this->criterio,true,'OR', 'LIKE');

		}else{
			$criteria->compare('id',$this->id);
			$criteria->compare('IATA',$this->IATA,true);
			$criteria->compare('Country',$this->Country,true);
			$criteria->compare('currency',$this->currency,true);
			$criteria->compare('currency_name',$this->currency_name,true);
		}
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	protected function beforeValidate(){





				return parent::beforeValidate();
    }

	public static function normalize_dates($model){
		$model->criterio=array();





				return $model;
	}

	protected function afterFind() {











		return parent::afterFind();
	}

	protected function beforeSave ()
    {
		$this->created_at=date("Y-m-d H:i:s");
																																	return parent::beforeSave ();
    }

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return IataCountryMaster the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
