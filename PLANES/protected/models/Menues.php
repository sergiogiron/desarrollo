<?php

/**
 * This is the model class for table "menues".
 *
 * The followings are the available columns in table 'menues':
 * @property integer $id_menu
 * @property integer $activo
 * @property string $icono
 * @property string $titulo
 * @property string $descripcion
 * @property string $url
 * @property string $date_stamp
 * @property string $user_stamp
 */
class Menues extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'menues';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('activo, titulo, descripcion, url, date_stamp, user_stamp', 'required'),
			array('activo', 'numerical', 'integerOnly'=>true),
			array('icono', 'length', 'max'=>100),
			array('titulo', 'length', 'max'=>20),
			array('descripcion, user_stamp', 'length', 'max'=>60),
			array('url', 'length', 'max'=>128),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_menu, activo, icono, titulo, descripcion, url, date_stamp, user_stamp', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}
	
	public function behaviors(){
		return array(
			// Classname => path to Class
			'ActiveRecordLogableBehavior'=>
				'application.behaviors.ActiveRecordLogableBehavior',
		);
	}
	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_menu' => 'Id Menu',
			'activo' => 'Activo',
			'icono' => 'Icono',
			'titulo' => 'Titulo',
			'descripcion' => 'Descripcion',
			'url' => 'Url',
			'date_stamp' => 'Date Stamp',
			'user_stamp' => 'User Stamp',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_menu',$this->id_menu);
		$criteria->compare('activo',$this->activo);
		$criteria->compare('icono',$this->icono,true);
		$criteria->compare('titulo',$this->titulo,true);
		$criteria->compare('descripcion',$this->descripcion,true);
		$criteria->compare('url',$this->url,true);
		$criteria->compare('date_stamp',$this->date_stamp,true);
		$criteria->compare('user_stamp',$this->user_stamp,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Menues the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
