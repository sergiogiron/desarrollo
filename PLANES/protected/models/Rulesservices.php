<?php

/**
 * This is the model class for table "rules_services".
 *
 * The followings are the available columns in table 'rules_services':
 * @property  public $criterio;
 * @property integer $id
 * @property integer $id_provider
 * @property string $description
 * @property string $external_endpoint
 * @property string $internal_endpoint
 * @property string $expire_date_from
 * @property string $expire_date_to
 * @property integer $id_product
 * @property integer $id_channel
 * @property integer $id_subchannel
 * @property string $client_user
 * @property string $client_pass
 * @property string $time_out_msg
 * @property integer $enabled
 * @property integer $time_out
 * @property time $start_time
 * @property string $created_at
 * @property integer $id_user
 */
class Rulesservices extends CActiveRecord
{
	public $criterio;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'rules_services';
	}

	/**

	cache' => 'Cache',
			'start_cache_days' => 'Anticipación Min en Dias',
			'max_cache_days' => 'Anticipación Max en Dias',
			'frecuency' => 'Ejecutar cada n horas',
			'start_titme' => 'Primer ejecución diaria',


	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs. 
		return array(
			array('id_channel,id_subchannel,id_provider, id_product,description, external_endpoint, internal_endpoint, expire_date_from, expire_date_to', 'required'),
			array('id_provider, id_product, id_channel, id_subchannel, enabled, id_user,time_out,anticipation_from_min_days,anticipation_to_max_days,cache,start_cache_days,max_cache_days,frequency', 'numerical', 'integerOnly'=>true),
			array('description, client_user, client_pass,start_time', 'length', 'max'=>45),
			array('external_endpoint, internal_endpoint,time_out_msg', 'length', 'max'=>255),
					// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, id_provider, description, external_endpoint, internal_endpoint, expire_date_from, expire_date_to, id_product, id_channel, id_subchannel, client_user, client_pass, enabled, created_at, id_user, anticipation_from_min_days, anticipation_to_max_days,time_out,time_out_msg,start_time', 'safe', 'on'=>'search'),
		);
	}

	public function behaviors(){
		return array(
			// Classname => path to Class
			'ActiveRecordLogableBehavior'=>
				'application.behaviors.ActiveRecordLogableBehavior',
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(

			'proveedor' => array(self::BELONGS_TO, 'Providers', 'id_provider'),
			'producto' => array(self::BELONGS_TO, 'Products', 'id_product'),
			'canal' => array(self::BELONGS_TO, 'Channels', 'id_channel'),
			'subcanal' => array(self::BELONGS_TO, 'Subchannels', 'id_subchannel'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{//, anticipation_from_min_days, anticipation_to_max_days
		return array(
			'id' => 'ID',
			'id_provider' => 'Proveedor',
			'description' => 'Descripción',
			'external_endpoint' => 'Endpoint Externo',
			'internal_endpoint' => 'Endpoint Interno',
			'expire_date_from' => 'Vigencia Desde',
			'expire_date_to' => 'Vigencia Hasta',
			'anticipation_from_min_days' => 'Anticipación Desde',
			'anticipation_to_max_days' => 'Anticipación Hasta',
			'id_product' => 'Producto',
			'id_channel' => 'Channel',
			'id_subchannel' => 'Subchannel',
			'client_user' => 'Cliente Usuario',
			'client_pass' => 'Cliente Password',
			'enabled' => 'Activo',
			'time_out' => 'Tiempo de Espera',
			'time_out_msg' => 'Mensaje de Tiempo de Espera',
			'created_at' => 'Fecha de registro',

			'cache' => 'Cache',
			'start_cache_days' => 'Anticipación Min en Dias',
			'max_cache_days' => 'Anticipación Max en Dias',
			'frequency' => 'Ejecutar cada n horas',
			'start_time' => 'Primer ejecución diaria',

			'id_user' => 'Usuario',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		if($this->criterio!=''){
			$criteria->addSearchCondition('proveedor.title',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('external_endpoint',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('internal_endpoint',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('expire_date_from',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('expire_date_to',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('producto.title',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('canal.title',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('subcanal.title',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('client_user',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('client_pass',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('start_time',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('anticipation_from_min_days',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('anticipation_to_max_days',$this->criterio,true,'OR', 'LIKE');
			$criteria->with = array('subcanal','canal','producto','proveedor');	

		}else{
			$criteria->compare('id',$this->id);
			$criteria->compare('id_provider',$this->id_provider);
			$criteria->compare('description',$this->description,true);
			$criteria->compare('external_endpoint',$this->external_endpoint,true);
			$criteria->compare('internal_endpoint',$this->internal_endpoint,true);
			$criteria->compare('expire_date_from',$this->expire_date_from,true);
			$criteria->compare('expire_date_to',$this->expire_date_to,true);
			$criteria->compare('id_product',$this->id_product);
			$criteria->compare('id_channel',$this->id_channel);
			$criteria->compare('id_subchannel',$this->id_subchannel);
			$criteria->compare('client_user',$this->client_user,true);
			$criteria->compare('client_pass',$this->client_pass,true);
			$criteria->compare('enabled',$this->enabled);
			$criteria->compare('created_at',$this->created_at,true);
			$criteria->compare('id_user',$this->id_user);
			$criteria->compare('start_time',$this->start_time);
			$criteria->compare('anticipation_from_min_days',$this->anticipation_from_min_days,true);
			$criteria->compare('anticipation_to_max_days',$this->anticipation_to_max_days,true);
		}
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function afterValidate(){
		if($this->cache==1 && ($this->start_cache_days =='' || $this->max_cache_days =='' || $this->frequency ==''  || $this->start_time =='')) {
            $this->addError('cache','Todos los campos cache son obligatorios');
            return; // or continue
        }
        return parent::afterValidate();
	}

	protected function beforeValidate(){
		if($this->expire_date_to!='' ){
			$this->expire_date_to = date_create_from_format("d/m/Y",$this->expire_date_to);
			$this->expire_date_to =date_format( $this->expire_date_to,"d/m/Y");
		}

		if($this->expire_date_from!=''){
			$this->expire_date_from = date_create_from_format("d/m/Y",$this->expire_date_from);
			$this->expire_date_from =date_format( $this->expire_date_from,"d/m/Y");
		}

		if($_POST['start_time'] !=''){
			$this->start_time = $_POST['start_time'];	
		}

		return parent::beforeValidate();
    }

	public static function normalize_dates($model){
		$model->criterio=array();
		$expire_date_to= explode(" ",$model->expire_date_to);
		$model->expire_date_to=$expire_date_to[0];
		$expire_date_from= explode(" ",$model->expire_date_from);
		$model->expire_date_from=$expire_date_from[0];

		$model->criterio=array_merge($model->criterio,array('start_time'=>$model->start_time));
		//$model->start_time=$start_time;
		return $model;
	}

	protected function afterFind() {
		$this->expire_date_to =Utils::date_spa($this->expire_date_to);
		$this->expire_date_from =Utils::date_spa($this->expire_date_from);
		$this->start_time =$this->start_time;
		return parent::afterFind();
	}

	protected function beforeSave ()
    {
    	 if($this->cache!=1){
    	 	$this->start_time = null;
    	 }
		$this->created_at=date("Y-m-d");
		$this->id_user=Yii::app()->user->id;
		$date_expire_date_to = DateTime::createFromFormat('d/m/Y', $this->expire_date_to);
		$this->expire_date_to=$date_expire_date_to->format('Y-m-d');

		$date_expire_date_from = DateTime::createFromFormat('d/m/Y', $this->expire_date_from);
		$this->expire_date_from=$date_expire_date_from->format('Y-m-d');
		return parent::beforeSave ();
    }

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Rulesservices the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
