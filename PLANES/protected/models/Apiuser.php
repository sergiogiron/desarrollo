<?php

/**
 * This is the model class for table "api_user".
 *
 * The followings are the available columns in table 'api_user':
 * @property  public $criterio;
 * @property integer $id
 * @property integer $id_user_services
 * @property integer $id_master_api
 * @property integer $enabled
 * @property string $created_at
 * @property integer $id_user
 *
 * The followings are the available model relations:
 * @property MasterApi $idMasterApi
 * @property UserServices $idUserServices
 */
class Apiuser extends CActiveRecord
{
	public $criterio;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'api_user';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(

			array('id_user_services, id_master_api, created_at, id_user', 'required'),
			array('id_user_services, id_master_api, enabled, id_user', 'numerical', 'integerOnly'=>true),
					// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, id_user_services, id_master_api, enabled, created_at, id_user', 'safe', 'on'=>'search'),
		);
	}

	public function behaviors(){
		return array(
			// Classname => path to Class
			'ActiveRecordLogableBehavior'=>
				'application.behaviors.ActiveRecordLogableBehavior',
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'masterapi' => array(self::BELONGS_TO, 'Masterapi', 'id_master_api'),
			'usuario' => array(self::BELONGS_TO, 'Userservices', 'id_user_services'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_user_services' => 'Usuario',
			'id_master_api' => 'Master Api',
			'enabled' => 'Estado',
			'created_at' => 'Fecha de registro',
			'id_user' => 'Usuario',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		if($this->criterio!=''){
			$criteria->addSearchCondition('masterapi.name',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('usuario.user',$this->criterio,true,'OR', 'LIKE');
			$criteria->with = array('masterapi','usuario');	

		}else{
			$criteria->compare('id',$this->id);
			$criteria->compare('id_user_services',$this->id_user_services);
			$criteria->compare('id_master_api',$this->id_master_api);
			$criteria->compare('enabled',$this->enabled);
			$criteria->compare('created_at',$this->created_at,true);
			$criteria->compare('id_user',$this->id_user);
		}
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	protected function beforeValidate(){






				return parent::beforeValidate();
    }

	public static function normalize_dates($model){
		$model->criterio=array();






				return $model;
	}

	protected function afterFind() {













		return parent::afterFind();
	}

	protected function beforeSave ()
    {
		$this->created_at=date("Y-m-d H:i:s");
																																							return parent::beforeSave ();
    }

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Apiuser the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
