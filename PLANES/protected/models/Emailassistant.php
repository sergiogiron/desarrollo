<?php

/**
 * This is the model class for table "email_assistant".
 *
 * The followings are the available columns in table 'email_assistant':
 * @property integer $id
 * @property integer $id_assistant
 * @property integer $id_user
 * @property string $created_at	
 * @property string $usuarios_name
 * @property string $usuarios_lastname
 * @property string $usuarios_email
 * @property string $usuarios_personal_phone
 */
class Emailassistant extends CActiveRecord
{	
	public $criterio;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'email_assistant';
	}
	public $usuarios_name;
	public $usuarios_lastname;
	public $usuarios_email;
	public $usuarios_personal_phone;

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_assistant', 'required'),
			array('id_assistant, id_user', 'numerical', 'integerOnly'=>true),
			array('created_at', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, id_assistant, id_user, created_at, usuarios_name,usuarios_lastname,usuarios_email,usuarios_personal_phone', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'usuarios' => array(self::BELONGS_TO, 'Users', 'id_assistant'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_assistant' => 'Nombre',
			'id_user' => 'Id User',
			'created_at' => 'Fecha de Alta',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.
		//echo $this->usuarios_search='walter';
		$sorter = new CSort;
		$sorter->attributes = array(
				'usuarios.name'=>'usuarios.name',
				'usuarios.lastname'=>'usuarios.lastname', 
				'usuarios.email'=>'usuarios.email',
				'usuarios.created_at' => 'created_at',
		);

		$criteria=new CDbCriteria;
		$criteria->with=array('usuarios');
		if($this->criterio!=''){
			$criteria->addSearchCondition('usuarios.name',$this->criterio, true, 'OR', 'LIKE');
			$criteria->addSearchCondition('usuarios.lastname',$this->criterio, true, 'OR', 'LIKE');
			$criteria->addSearchCondition('usuarios.email',$this->criterio, true, 'OR', 'LIKE'); 		
		}else{
			$criteria->compare('usuarios.lastname',$this->id_assistant, true);  		//Todo			
		}
		$criteria->compare('t.id_user',Yii::app()->user->id, true,'AND');  
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array('pageSize'=>9),
			'sort'=>$sorter,
		));
	}
	
	
	public function searchbyUserid($userid)
	{
		$sorter = new CSort;
		$sorter->attributes = array(
				'usuarios.name'=>'usuarios.name',
				'usuarios.lastname'=>'usuarios.lastname', 
				'usuarios.email'=>'usuarios.email',
				'usuarios.created_at' => 'created_at',
		);
		
		$criteria=new CDbCriteria;
		$criteria->with=array('usuarios');
		if($this->criterio!=''){
			$criteria->addSearchCondition('usuarios.name',$this->criterio, true, 'OR', 'LIKE');
			$criteria->addSearchCondition('usuarios.lastname',$this->criterio, true, 'OR', 'LIKE');
			$criteria->addSearchCondition('usuarios.email',$this->criterio, true, 'OR', 'LIKE'); 		
		}else{
			$criteria->compare('usuarios.lastname',$this->id_assistant, true);  		//Todo			
		}
		$criteria->compare('t.id_user',$userid, true,'AND');  
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array('pageSize'=>9),
			'sort'=>$sorter,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Emailassistant the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
