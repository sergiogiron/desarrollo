<?php

/**
 * This is the model class for table "payments_entity_financing".
 *
 * The followings are the available columns in table 'payments_entity_financing':
 * @property  public $criterio;
 * @property integer $id
 * @property integer $id_entity
 * @property integer $id_user
 * @property integer $amount_dues
 * @property double $interests
 * @property string $legal
 * @property string $cftna
 * @property double $posnet_cost
 * @property integer $rewards
 * @property string $date_validity_start
 * @property string $date_validity_end
 * @property string $created_at
 *
 * The followings are the available model relations:
 * @property Users $idUser
 */
class Paymentsentityfinancing extends CActiveRecord
{
	public $criterio;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'payments_entity_financing';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('date_validity_start, date_validity_end', 'type', 'type' => 'date', 'message' => '{attribute}: no es una fecha', 'dateFormat' => 'dd/mm/yyyy'),

				array('id_entity, id_user, amount_dues, interests, legal, cftna, posnet_cost, date_validity_start, date_validity_end', 'required','message'=>'Debe completar este campo.'),
				array('id_entity, id_user', 'numerical', 'integerOnly'=>true),
				array('interests', 'numerical'),
				array(' amount_dues', 'length', 'max'=>16),
					// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, id_entity, id_user, amount_dues, interests, legal, cftna, posnet_cost, date_validity_start, date_validity_end, created_at', 'safe', 'on'=>'search'),
		);
	}

	public function behaviors(){
		return array(
			// Classname => path to Class
			'ActiveRecordLogableBehavior'=>
				'application.behaviors.ActiveRecordLogableBehavior',
		);
	}

	/**
	 * @return array relational rules.
	 */

	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idUser' => array(self::BELONGS_TO, 'Users', 'id_user','alias'=>'users'),
			'financing' => array(self::HAS_MANY, 'Paymentsentityfinancingrelational', 'id_payments_entity_financing'),
			'services' => array(self::HAS_MANY, 'Paymentsentityfinancingservices', 'id_payments_entity_financing'),
		 );
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_entity' => 'Id Entity',
			'id_user' => 'Id User',
			'amount_dues' => 'Amount Dues',
			'interests' => 'Interests',
			'legal' => 'Legal',
			'cftna' => 'Cftna',
			'posnet_cost' => 'Posnet Cost',
			'rewards' => 'Aplica Rewards',
			'date_validity_start' => 'Date Validity Start',
			'date_validity_end' => 'Date Validity End',
			'created_at' => 'Created At',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		if($this->criterio!=''){
			$criteria->addSearchCondition('id',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('id_entity',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('id_user',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('amount_dues',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('interests',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('legal',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('cftna',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('posnet_cost',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('rewards',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('date_validity_start',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('date_validity_end',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('created_at',$this->criterio,true,'OR', 'LIKE');

		}else{
			$criteria->compare('id',$this->id);
			$criteria->compare('id_entity',$this->id_entity);
			$criteria->compare('id_user',$this->id_user);
			$criteria->compare('amount_dues',$this->amount_dues);
			$criteria->compare('interests',$this->interests);
			$criteria->compare('legal',$this->legal,true);
			$criteria->compare('cftna',$this->cftna);
			$criteria->compare('posnet_cost',$this->posnet_cost);
			$criteria->compare('rewards',$this->posnet_cost);
			$criteria->compare('date_validity_start',$this->date_validity_start,true);
			$criteria->compare('date_validity_end',$this->date_validity_end,true);
			$criteria->compare('created_at',$this->created_at,true);
		}
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	protected function beforeValidate(){








				return parent::beforeValidate();
    }

	public static function normalize_dates($model){
		$model->criterio=array();
				return $model;
	}

	protected function afterFind() {
















						$this->date_validity_start =Utils::date_spa($this->date_validity_start);


						$this->date_validity_end =Utils::date_spa($this->date_validity_end);





		return parent::afterFind();
	}

	protected function beforeSave ()
    {
		$this->created_at=date("Y-m-d H:i:s");
		$this->date_validity_start =Utils::date_spa_r($this->date_validity_start);
		$this->date_validity_end =Utils::date_spa_r($this->date_validity_end);
		$this->amount_dues= Utils::limpia_espacios($this->amount_dues);
		foreach($this->attributes as $key=>$value){
			if($value==''){
				$this->$key=NULL;
			}
		}
		return parent::beforeSave ();
    }

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Paymentsentityfinancing the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
