<?php

/**
 * This is the model class for table "bines".
 *
 * The followings are the available columns in table 'bines':
 * @property  public $criterio;
 * @property integer $id
 * @property integer $id_entity
 * @property integer $id_credit_card
 * @property integer $id_provider
 * @property integer $id_account_type
 * @property integer $id_product
 * @property integer $bin
 */
class Bines extends CActiveRecord
{
	public $criterio;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'bines';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
					
				array('id_entity, id_credit_card, id_provider, id_account_type, id_product, bin', 'numerical', 'integerOnly'=>true),
					// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, id_entity, id_credit_card, id_provider, id_account_type, id_product, bin', 'safe', 'on'=>'search'),
		);
	}

	public function behaviors(){
		return array(
			// Classname => path to Class
			'ActiveRecordLogableBehavior'=>
				'application.behaviors.ActiveRecordLogableBehavior',
		);
	}

	/**
	 * @return array relational rules.
	 */
	
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
					
		 );
	}	

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_entity' => 'Id Entity',
			'id_credit_card' => 'Id Credit Card',
			'id_provider' => 'Id Provider',
			'id_account_type' => 'Id Account Type',
			'id_product' => 'Id Product',
			'bin' => 'Bin',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		
		if($this->criterio!=''){
			$criteria->addSearchCondition('id',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('id_entity',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('id_credit_card',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('id_provider',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('id_account_type',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('id_product',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('bin',$this->criterio,true,'OR', 'LIKE');
	
		}else{	
			$criteria->compare('id',$this->id);
			$criteria->compare('id_entity',$this->id_entity);
			$criteria->compare('id_credit_card',$this->id_credit_card);
			$criteria->compare('id_provider',$this->id_provider);
			$criteria->compare('id_account_type',$this->id_account_type);
			$criteria->compare('id_product',$this->id_product);
			$criteria->compare('bin',$this->bin);
		}
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	protected function beforeValidate(){
						
						
						
						
						
						
						
				return parent::beforeValidate();
    }	
	
	public static function normalize_dates($model){
		$model->criterio=array();
					
					
					
					
					
					
					
				return $model;		
	}
	
	protected function afterFind() {
						
					
						
					
						
					
						
					
						
					
						
					
						
					
						
		return parent::afterFind();
	}	
	
	protected function beforeSave ()
    {	
		$this->created_at=date("Y-m-d H:i:s");
																																													foreach($this->attributes as $key=>$value){
			if($value==''){
				$this->$key=NULL;
			}
		}			
		return parent::beforeSave ();
    }	
	
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Bines the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
