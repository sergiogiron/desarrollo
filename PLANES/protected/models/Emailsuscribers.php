<?php

/**
 * This is the model class for table "email_suscribers".
 *
 * The followings are the available columns in table 'email_suscribers':
 * @property integer $id
 * @property string $id_user
 * @property string $email
 * @property string $name
 * @property string $lastname
 * @property string $admin
 * @property integer $classified
 * @property string $origen
 * @property integer $dk
 * @property integer $share
 * @property integer $suscripto
 * @property integer $admin_status
 * @property integer $total_errors
 * @property integer $list
 * @property string $start_date
 * @property string $date_unsubscribe
 * @property string $description
 * @property string $created_at
 */
class Emailsuscribers extends CActiveRecord
{
	public $criterio;
		
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'email_suscribers';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('email, admin', 'required'),
			array('classified, dk, share, suscripto, admin_status, total_errors, list', 'numerical', 'integerOnly'=>true),
			array('id_user', 'length', 'max'=>11),
			array('name, lastname', 'length', 'max'=>32),
			array('email', 'length', 'max'=>128),
			array('origen', 'length', 'max'=>255),
			array('admin', 'length', 'max'=>100),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, id_user, email, name, lastname, admin, classified, origen, dk, share, suscripto, admin_status, total_errors, list, start_date, date_unsubscribe, description, created_at', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'usuarios' => array(self::BELONGS_TO, 'Users', 'admin'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_user' => 'Id User',
			'email' => 'Email',
			'name' => 'Nombre',
			'lastname' => 'Apellido',
			'admin' => 'Administrador',
			'classified' => 'Clasificado',
			'origen' => 'Origen',
			'dk' => 'Dk',
			'share' => 'Compartido',
			'suscripto' => 'Suscripto',
			'admin_status' => 'Estado del administrador',
			'total_errors' => 'Total de errores',
			'list' => 'List',
			'start_date' => 'Fecha de inicio',
			'date_unsubscribe' => 'Fecha de desuscripcion',
			'description' => 'Descripcion',
			'created_at' => 'Fecha de Alta',
		);
	}
	


	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		if($this->criterio!=''){
			$criteria->addSearchCondition('email',$this->criterio, true, 'OR', 'LIKE');
			$criteria->addSearchCondition('name',$this->criterio, true, 'OR', 'LIKE');
			$criteria->addSearchCondition('lastname',$this->criterio, true, 'OR', 'LIKE');
		}else{
			$criteria->compare('id',$this->id);
			$criteria->compare('id_user',$this->id_user,true);
			$criteria->compare('email',$this->email,true);
			$criteria->compare('name',$this->name,true);
			$criteria->compare('lastname',$this->lastname,true);
			$criteria->compare('admin',$this->admin,true);
			$criteria->compare('classified',$this->classified);
			$criteria->compare('origen',$this->origen,true);
			$criteria->compare('dk',$this->dk);
			$criteria->compare('share',$this->share);
			$criteria->compare('suscripto',$this->suscripto);
			$criteria->compare('admin_status',$this->admin_status);
			$criteria->compare('total_errors',$this->total_errors);
			$criteria->compare('list',$this->list);
			$criteria->compare('start_date',$this->start_date,true);
			$criteria->compare('date_unsubscribe',$this->date_unsubscribe,true);
			$criteria->compare('description',$this->description,true);
			$criteria->compare('created_at',$this->created_at,true);
			#$criteria->condition=" (id_user='".Yii::app()->user->id."') or (admin='".Yii::app()->user->id."' and share=1)";
			}
			$criteria->condition=" (id_user='".Yii::app()->user->id."') or (admin='".Yii::app()->user->id."' and share=1)";
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Emailsuscribers the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	
	
	
 
 


}
