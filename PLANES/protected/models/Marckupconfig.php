<?php

/**
 * This is the model class for table "marckup_config".
 *
 * The followings are the available columns in table 'marckup_config':
 * @property integer $id
 * @property integer $id_marckup_concept
 * @property integer $id_product
 * @property integer $id_provider
 * @property integer $id_channel
 * @property integer $id_subchannel
 * @property string $marckup_percentage
 * @property integer $enabled
 *
 * The followings are the available model relations:
 * @property Marckupconcept $idMarckupsConcepts
 * @property Channels $idChannel
 */
class Marckupconfig extends CActiveRecord
{
	public $criterio;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'marckup_config';
	}

	public function behaviors()
	{
	    return array(
	        // Classname => path to Class
	        'ActiveRecordLogableBehavior'=>
	            'application.behaviors.ActiveRecordLogableBehavior',
	    );
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_marckup_concept, id_product, id_provider, id_channel, id_subchannel, enabled', 'numerical', 'integerOnly'=>true),
			array('id_marckup_concept, id_product, id_provider, id_channel, id_subchannel,marckup_percentage','required'),
			array('id_channel,id_subchannel,id_provider,id_product', 'uniqueuni', 'attributeName'=>'id_channel,id_subchannel,id_provider,id_product'),
			array('created_at', 'safe'),

			//array('marckup_percentage', 'length', 'max'=>2),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, id_marckup_concept, id_product, id_provider, id_channel, id_subchannel, marckup_percentage, enabled', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'concepto' => array(self::BELONGS_TO, 'Marckupconcept', 'id_marckup_concept'),
			'canal' => array(self::BELONGS_TO, 'Channels', 'id_channel'),
			'proveedor' => array(self::BELONGS_TO, 'Providers', 'id_provider'),
			'subcanal' => array(self::BELONGS_TO, 'Subchannels', 'id_subchannel'),
			'producto' => array(self::BELONGS_TO, 'Products', 'id_product'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_marckup_concept' => 'Aplica sobre',
			'id_product' => 'Producto',
			'id_provider' => 'Proveedor',
			'id_channel' => 'Canal',
			'id_subchannel' => 'Subcanal',
			'marckup_percentage' => 'Porcentaje de Markup',
			'enabled' => 'Estado',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		if($this->criterio!=''){
		$criteria->addSearchCondition('concepto.title',$this->criterio,true,'OR', 'LIKE');
		$criteria->addSearchCondition('producto.title',$this->criterio,true,'OR', 'LIKE');
		$criteria->addSearchCondition('proveedor.title',$this->criterio,true,'OR', 'LIKE');
		$criteria->addSearchCondition('canal.title',$this->criterio,true,'OR', 'LIKE');
		$criteria->addSearchCondition('subcanal.title',$this->criterio,true,'OR', 'LIKE');
		$criteria->addSearchCondition('t.marckup_percentage',$this->criterio,true,'OR', 'LIKE');

		$criteria->with = array('concepto','producto','proveedor','canal','subcanal');

		}else{

		$criteria->compare('id_marckup_concept',$this->id_marckup_concept);
		$criteria->compare('id_product',$this->id_product);
		$criteria->compare('id_provider',$this->id_provider);
		$criteria->compare('id_channel',$this->id_channel);
		$criteria->compare('id_subchannel',$this->id_subchannel);
		$criteria->compare('marckup_percentage',$this->marckup_percentage,true);
		$criteria->compare('enabled',$this->enabled);

		}

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function uniqueuni($attribute,$params) {
		if($this->isNewRecord){
			$cat = array(	'id_channel'=>$this->id_channel,
							'id_subchannel'=>$this->id_subchannel,
							'id_provider'=>$this->id_provider,
							'id_product'=>$this->id_product,
							);
		$uni=Marckupconfig::model()->findAllByAttributes($cat);
			if(count($uni)>0){
				$this->addError($attribute, 'La combinación ya existe.');
			}
		}
	}
	
	protected function beforeSave()
    {	
    	$this->id_user=Yii::app()->user->id;
		$this->created_at=date("Y-m-d");
		return parent::beforeSave();
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return MarckupConfig the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
