<?php

/**
 * This is the model class for table "profile_widgets".
 *
 * The followings are the available columns in table 'profile_widgets':
 * @property integer $id
 * @property integer $id_user
 * @property string $title
 * @property string $description
 * @property string $url
 * @property integer $enabled
 * @property string $created_at
 * @property string $size
 */
class Profilewidgets extends CActiveRecord
{
	public $criterio;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'profile_widgets';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title, description, url, size', 'required'),
			array('id_user, enabled', 'numerical', 'integerOnly'=>true),
			array('title', 'length', 'max'=>20),
			array('description', 'length', 'max'=>60),
			array('url', 'length', 'max'=>128),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, id_user, title, description, url, enabled, created_at', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_user' => 'Id User',
			'title' => 'Título',
			'description' => 'Descripción',
			'url' => 'URL',
			'enabled' => 'Estado',
			'created_at' => 'Created At',
			'size'	=> 'Tamaño',
		);
	}
	
	public function behaviors(){
		return array(
			// Classname => path to Class
			'ActiveRecordLogableBehavior'=>
				'application.behaviors.ActiveRecordLogableBehavior',
		);
	}
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		if($this->criterio!=''){
			$criteria->addSearchCondition('title', $this->criterio, true, 'OR', 'LIKE');
			$criteria->addSearchCondition('description', $this->criterio, true, 'OR', 'LIKE');
			$criteria->addSearchCondition('url', $this->criterio, true, 'OR', 'LIKE');			
		}else{
			$criteria->compare('id',$this->id);
			$criteria->compare('id_user',$this->id_user);
			$criteria->compare('title',$this->title,true);
			$criteria->compare('description',$this->description,true);
			$criteria->compare('url',$this->url,true);
			$criteria->compare('enabled',$this->enabled);
			$criteria->compare('created_at',$this->created_at,true);			
		}

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Profilewidgets the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	protected function beforeSave(){
		$this->created_at=date("Y-m-d H:i:s");
		return parent::beforeSave();
	}		
}
