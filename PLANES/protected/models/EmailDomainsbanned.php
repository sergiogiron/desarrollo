<?php

/**
 * This is the model class for table "email_domainsbanned".
 *
 * The followings are the available columns in table 'email_domainsbanned':
 * @property  public $criterio;
 * @property integer $id
 * @property string $domain
 * @property string $description
 * @property string $created_at
 * @property integer $enabled
 */
class EmailDomainsbanned extends CActiveRecord
{
	public $criterio;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'email_domainsbanned';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
					
				array('domain, description, created_at, enabled', 'required','message'=>'Debe completar este campo.'),
				array('enabled', 'numerical', 'integerOnly'=>true),
				array('domain', 'length', 'max'=>128),
				array('description', 'length', 'max'=>256),
					// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, domain, description, created_at, enabled', 'safe', 'on'=>'search'),
		);
	}

	public function behaviors(){
		return array(
			// Classname => path to Class
			'ActiveRecordLogableBehavior'=>
				'application.behaviors.ActiveRecordLogableBehavior',
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'domain' => 'Dominio',
			'description' => 'Descripción',
			'created_at' => 'Fecha de creación',
			'enabled' => 'Estado',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		
		if($this->criterio!=''){
			$criteria->addSearchCondition('id',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('domain',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('description',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('created_at',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('enabled',$this->criterio,true,'OR', 'LIKE');
	
		}else{	
			$criteria->compare('id',$this->id);
			$criteria->compare('domain',$this->domain,true);
			$criteria->compare('description',$this->description,true);
			$criteria->compare('created_at',$this->created_at,true);
			$criteria->compare('enabled',$this->enabled);
		}
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
protected function beforeValidate(){
	return parent::beforeValidate();
}	
	
public static function normalize_dates($model){
		$model->criterio=array();
		return $model;		
	}
	
protected function afterFind() {
	return parent::afterFind();
	}	
	
	protected function beforeSave ()
    {	
		return parent::beforeSave ();
    }	
	
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return EmailDomainsbanned the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
