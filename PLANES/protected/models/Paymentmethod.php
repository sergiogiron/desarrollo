<?php

/**
 * This is the model class for table "payment_method".
 *
 * The followings are the available columns in table 'payment_method':
 * @property  public $criterio;
 * @property integer $id
 * @property integer $id_product
 * @property integer $id_bank
 * @property integer $id_credit_card
 * @property integer $id_provider
 * @property string $rules
 * @property string $date_from
 * @property string $date_to
 * @property string $enabled
 * @property integer $id_channel
 * @property integer $id_subchannel
 */
class Paymentmethod extends CActiveRecord
{
	public $criterio;
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'payment_method';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
	
		return array(
        	//array('rules', 'ext.validators.UniqueattributesValidator', 'with'=>'id_channel,id_subchannel,id_product'),

			array('rules,id_channel,id_subchannel,id_product', 'unique', 'on'=>'insert', 'attributeName'=>'rules'),

		//	array('rules,id_channel,id_subchannel,id_product', 'unique', 'attributeName'=>'rules'),
			
			array('rules,id_product, id_bank, id_credit_card, id_provider, id_channel, id_subchannel,date_from, date_to', 'required'),
			array('id_product, id_bank, id_credit_card, id_provider, id_channel, id_subchannel', 'numerical', 'integerOnly'=>true),
			array('rules, enabled', 'length', 'max'=>45),
			array('date_from, date_to', 'safe'),

			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, id_product, id_bank, id_credit_card, id_provider, rules, date_from, date_to, enabled, id_channel, id_subchannel', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'producto' => array(self::BELONGS_TO, 'Products', 'id_product','alias'=>'products'),
			'banco' => array(self::BELONGS_TO, 'Entity', 'id_bank','alias'=>'payments_entity'),
			'proveedor' => array(self::BELONGS_TO, 'Providers', 'id_provider','alias'=>'providers'),
			'credito' => array(self::BELONGS_TO, 'Creditcards', 'id_credit_card','alias'=>'credit_cards'),
			'canal' => array(self::BELONGS_TO, 'Channels', 'id_channel','alias'=>'channels'),
			'subcanal' => array(self::BELONGS_TO, 'Subchannels', 'id_subchannel','alias'=>'subchannels'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_product' => 'Producto',
			'id_bank' => 'Banco',
			'id_credit_card' => 'Tarjeta de Crédito',
			'id_provider' => 'Proveedor',
			'rules' => 'Regla',
			'date_from' => 'Vigencia desde',
			'date_to' => 'Vigencia hasta',
			'enabled' => 'Estado',
			'id_channel' => 'Canal',
			'id_subchannel' => 'Subcanal',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		
		if($this->criterio!=''){			
			$criteria->addSearchCondition('subchannels.title',$this->criterio,true,'OR', 'LIKE');			
			$criteria->addSearchCondition('channels.title',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('products.title',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('payments_entity.title',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('credit_cards.name',$this->criterio,true,'OR', 'LIKE');	
			$criteria->addSearchCondition('providers.title',$this->criterio,true,'OR', 'LIKE');	
			$criteria->addSearchCondition('t.rules',$this->criterio,true,'OR', 'LIKE');
			$criteria->with = array('subcanal','canal','producto','banco','credito','proveedor');			
		}else{	
			$criteria->compare('id',$this->id);
			$criteria->compare('id_product',$this->id_product);
			$criteria->compare('id_bank',$this->id_bank);
			$criteria->compare('id_credit_card',$this->id_credit_card);
			$criteria->compare('id_provider',$this->id_provider);
			$criteria->compare('rules',$this->rules,true);
			$criteria->compare('date_from',$this->date_from,true);
			$criteria->compare('date_to',$this->date_to,true);
			$criteria->compare('enabled',$this->enabled,true);
			$criteria->compare('id_channel',$this->id_channel);
			$criteria->compare('id_subchannel',$this->id_subchannel);
		}
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	protected function beforeValidate(){				
		if($this->date_from!=''){
			$this->date_from = date_create_from_format("d/m/Y",$this->date_from);
			$this->date_from =date_format( $this->date_from,"d/m/Y");
		}
				
		if($this->date_to!=''){
			$this->date_to = date_create_from_format("d/m/Y",$this->date_to);
			$this->date_to =date_format( $this->date_to,"d/m/Y");
		}
		return parent::beforeValidate();
    }	
	
	public static function normalize_dates($model){
		$model->criterio=array();
		
		$date_from= explode(" ",$model->date_from);
		$model->date_from=$date_from[0];
		//$model->criterio=array_merge($model->criterio,array('date_from_hour'=>$date_from[1]));

		$date_to= explode(" ",$model->date_to);
		$model->date_to=$date_to[0];
		//$model->criterio=array_merge($model->criterio,array('date_to_hour'=>$date_to[1]));
		
		return $model;		
	}
	
	protected function afterFind() {
		$this->date_from =Utils::date_spa($this->date_from);
		$this->date_to =Utils::date_spa($this->date_to);
		return parent::afterFind();
	}	
	
	protected function beforeSave()
    {	
		$m_date = DateTime::createFromFormat('d/m/Y', $this->date_from);
		$this->date_from=$m_date->format('Y-m-d');
		$date_date_to = DateTime::createFromFormat('d/m/Y', $this->date_to);
		$this->date_to=$date_date_to->format('Y-m-d');
		return parent::beforeSave();
    }	
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Paymentmethod the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
