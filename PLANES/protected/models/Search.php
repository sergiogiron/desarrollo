<?php

/**
 * This is the model class for table "search".
 *
 * The followings are the available columns in table 'search':
 * @property  public $criterio;
 * @property integer $id
 * @property integer $id_channel
 * @property integer $id_subchannel
 * @property integer $id_product
 * @property integer $id_email_server_sales
 * @property string $sale_user
 * @property string $sale_pass
 * @property string $sale_show_as
 * @property string $sale_to
 * @property string $script
 * @property integer $id_email_server_alerts
 * @property string $alerts_user
 * @property string $alerts_pass
 * @property string $alerts_show_as
 * @property string $alerts_subject_patterns
 * @property string $alerts_to
 * @property string $tracking_description
 * @property string $tracking_analitycs
 * @property string $landings_order_price
 * @property string $landings_order_departure
 * @property string $landings_telephone_sales
 * @property integer $landings_max_results_per_pages
 * @property integer $enabled
 * @property string $created_at
 * @property integer $id_user
 * @property string $url_iframe
 * @property string $thankyou_text
 * @property string $legal
 * @property string $error_text
 * @property integer $online_payment
 *
 * The followings are the available model relations:
 * @property Channels $idChannel
 * @property EmailServer $idEmailServerSales
 * @property EmailServer $idEmailServerAlerts
 * @property Products $idProduct
 * @property Subchannels $idSubchannel
 */
class Search extends CActiveRecord
{
	public $criterio;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'search';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
				array('enabled', 'statusvalidate', 'attributeName'=>'enabled'),
				array('url_iframe', 'url', 'message'=>'{attribute}: no es una url válida'),
				array('url_iframe', 'length', 'max'=>260),
				array('id_channel,id_subchannel, id_product', 'uniqueuni', 'attributeName'=>'id_channel,id_subchannel, id_product'),
				array('url_iframe,landings_order_price,landings_order_departure,landings_max_results_per_pages,id_channel, id_subchannel, id_product,sale_user,sale_pass,sale_show_as,sale_to,alerts_user,alerts_pass,alerts_show_as,alerts_subject_patterns,alerts_to, id_email_server_sales, id_email_server_alerts,', 'required','message'=>'Debe completar este campo.'),
				array('id_channel,default_view,anticipation_to_max_days,anticipation_from_min_days,max_search_months,order, id_subchannel, id_product, id_email_server_sales, id_email_server_alerts, landings_max_results_per_pages, enabled, id_user', 'numerical', 'integerOnly'=>true),
				array('rewards_conversion_ratio,rewards_conversion_min,rewards_conversion_max,rewards_multiple_logic', 'numerical','integerOnly'=>false,'allowEmpty'=>true,'min'=>0,'max'=>1024),
				array('rewards_date_from,rewards_date_to', 'date', 'format'=>'dd/MM/yyyy'),
				array('sale_user, sale_pass, sale_show_as, alerts_user, alerts_pass, alerts_show_as, alerts_subject_patterns', 'length', 'max'=>45),
				array('sale_to, alerts_to', 'length', 'max'=>1024),
				array('landings_order_price, landings_order_departure', 'length', 'max'=>4),
				array('landings_telephone_sales', 'length', 'max'=>11),
				array('ext', 'length', 'max'=>12),
				array('phone', 'length', 'max'=>35),
				array('tracking_description', 'length', 'max'=>1024),
				array('phone_description,office_hours,filename', 'length', 'max'=>64),
				array('script, tracking_analitycs,thankyou_text, legal, error_text, online_payment', 'safe'),
					// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, id_channel,script, tracking_analitycs,id_subchannel, id_product, id_email_server_sales, sale_user, sale_pass, sale_show_as, sale_to, id_email_server_alerts, alerts_user, alerts_pass, alerts_show_as, alerts_subject_patterns, alerts_to, tracking_description,  landings_order_price, landings_order_departure, landings_telephone_sales, landings_max_results_per_pages, enabled, created_at, id_user', 'safe', 'on'=>'search'),
		);
	}

	public function behaviors(){
		return array(
			// Classname => path to Class
			'ActiveRecordLogableBehavior'=>
				'application.behaviors.ActiveRecordLogableBehavior',
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'imagenes' => array(self::HAS_MANY, 'Searchimages', 'id_search', 'order'=>'imagenes.order ASC'),
			'filtros' => array(self::HAS_MANY, 'Searchfacetsextendfilters', 'id_search', 'condition'=>'is_facets=0'),
			'facetas' => array(self::HAS_MANY, 'Searchfacetsextendfilters', 'id_search', 'condition'=>'is_facets=1'),
			'canal' => array(self::BELONGS_TO, 'Channels', 'id_channel'),
			'idEmailServerSales' => array(self::BELONGS_TO, 'EmailServer', 'id_email_server_sales'),
			'idEmailServerAlerts' => array(self::BELONGS_TO, 'EmailServer', 'id_email_server_alerts'),
			'producto' => array(self::BELONGS_TO, 'Products', 'id_product'),
			'subcanal' => array(self::BELONGS_TO, 'Subchannels', 'id_subchannel'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_channel' => 'Canal',
			'id_subchannel' => 'Subcanal',
			'id_product' => 'Producto',
			'filename' => 'Imagen',
			'id_email_server_sales' => 'Servidor de email de contacto',
			'sale_user' => 'Usuario',
			'url_iframe' => 'URL del iframe',
			'sale_pass' => 'Contraseña',
			'script' => 'Script a ejecutar en pantalla de gracias',
			'sale_show_as' => 'Mostrar como',
			'sale_to' => 'Notificar a:',
			'id_email_server_alerts' => 'Servidor de email de alertas',
			'alerts_user' => 'Usuario',
			'alerts_pass' => 'Contraseña',
			'alerts_show_as' => 'Mostrar como',
			'alerts_subject_patterns' => 'Patrón del asunto',
			'alerts_to' => 'Notificar a ',
			'tracking_description' => 'Descripción SEO',
			'tracking_analitycs' => 'Header',
			'landings_order_price' => 'Ordenar resultados de salida por precio',
			'landings_order_departure' => 'Ordenar resultados por fecha de Salida',
			'landings_telephone_sales' => 'Telefono venta  telefonica',
			'landings_max_results_per_pages' => 'Cantidad Máxima de Resultados por página',
			'enabled' => 'Estado',
			'created_at' => 'Created At',
			'id_user' => 'Id User',
			'phone'=>'Teléfono',
			'phone_description'=>'Texto del teléfono',
			'office_hours'=>'Horario de atención',
			'default_view'=>'Establecer como buscador predeterminado',
			'order'=>'Orden',
			'max_search_months'=>'Máximo de busqueda (Meses)',
			'anticipation_to_max_days'=>'Anticipación hasta(dias)',
			'anticipation_from_min_days'=>'Anticipación desde(dias)',
			'thankyou_text' => 'Texto de gracias',
			'error_text' => 'Texto de error',
			'online_payment' => 'Pago Online',
			'legal' => 'Legal',
			'rewards_date_from' => 'Vigencia Desde',	
			'rewards_date_to' => 'Vigencia Hasta',	
			'rewards_conversion_ratio' => 'Ratio Conversion Rewards',	
			'rewards_conversion_min' => 'Mínimo a convertir',	
			'rewards_conversion_max' => 'Máximo a convertir',	
			'rewards_multiple_logic' => 'Lógica de múltiplos',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		if($this->criterio!=''){
			$criteria->addSearchCondition('subcanal.title',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('canal.title',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('producto.title',$this->criterio,true,'OR', 'LIKE');
			$criteria->with = array('subcanal','canal','producto');
			$criteria->order = 'subcanal.title,canal.title,producto.title asc';
		}else{
			$criteria->compare('id',$this->id);
			$criteria->compare('id_channel',$this->id_channel);
			$criteria->compare('id_subchannel',$this->id_subchannel);
			$criteria->compare('id_product',$this->id_product);
			$criteria->compare('id_email_server_sales',$this->id_email_server_sales);
			$criteria->compare('sale_user',$this->sale_user,true);
			$criteria->compare('sale_pass',$this->sale_pass,true);
			$criteria->compare('sale_show_as',$this->sale_show_as,true);
			$criteria->compare('sale_to',$this->sale_to,true);
			$criteria->compare('id_email_server_alerts',$this->id_email_server_alerts);
			$criteria->compare('alerts_user',$this->alerts_user,true);
			$criteria->compare('alerts_pass',$this->alerts_pass,true);
			$criteria->compare('alerts_show_as',$this->alerts_show_as,true);
			$criteria->compare('alerts_subject_patterns',$this->alerts_subject_patterns,true);
			$criteria->compare('alerts_to',$this->alerts_to,true);
			$criteria->compare('tracking_description',$this->tracking_description,true);
			$criteria->compare('tracking_analitycs',$this->tracking_analitycs,true);
			$criteria->compare('landings_order_price',$this->landings_order_price,true);
			$criteria->compare('landings_order_departure',$this->landings_order_departure,true);
			$criteria->compare('landings_telephone_sales',$this->landings_telephone_sales,true);
			$criteria->compare('landings_max_results_per_pages',$this->landings_max_results_per_pages);
			$criteria->compare('enabled',$this->enabled);
			$criteria->compare('created_at',$this->created_at,true);
			$criteria->compare('id_user',$this->id_user);
			$criteria->with = array('subcanal','canal','producto');
			$criteria->order = 'subcanal.title,canal.title,producto.title asc';
		}
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function uniqueuni($attribute,$params) {
		if($this->isNewRecord){
			$uni=Search::model()->findAllByAttributes(array('id_channel'=>$this->id_channel,'id_subchannel'=>$this->id_subchannel,'id_product'=>$this->id_product));
			if(count($uni)>0){
				$this->addError($attribute, 'La combinación de canal,subcanal y producto ya existe.');
			}
		}
	}

	protected function beforeValidate(){
		if($this->rewards_date_from!='' ){
			$this->rewards_date_from = date_create_from_format("d/m/Y",$this->rewards_date_from);
			$this->rewards_date_from =date_format( $this->rewards_date_from,"d/m/Y");
		}

		if($this->rewards_date_to!=''){
			$this->rewards_date_to = date_create_from_format("d/m/Y",$this->rewards_date_to);
			$this->rewards_date_to =date_format( $this->rewards_date_to,"d/m/Y");
		}
		return parent::beforeValidate();
    }

	public static function normalize_dates($model){
		$model->criterio=array();
		$rewards_date_from= explode(" ",$model->rewards_date_from);
		$model->rewards_date_from=$rewards_date_from[0];
		$rewards_date_to= explode(" ",$model->rewards_date_to);
		$model->rewards_date_to=$rewards_date_to[0];
		return $model;
	}

	protected function afterFind() {
		if($this->rewards_date_to){
			$this->rewards_date_to =Utils::date_spa($this->rewards_date_to);
		}
		if($this->rewards_date_from){
			$this->rewards_date_from =Utils::date_spa($this->rewards_date_from);
		}
		return parent::afterFind();
	}

	protected function beforeSave ()
    {
		$this->created_at=date("Y-m-d H:i:s");

		if($this->rewards_date_to){
			$date_expire_date_to = DateTime::createFromFormat('d/m/Y', $this->rewards_date_to);
			$this->rewards_date_to=$date_expire_date_to->format('Y-m-d');
		}
		if($this->rewards_date_from){
			$date_expire_date_from = DateTime::createFromFormat('d/m/Y', $this->rewards_date_from);
			$this->rewards_date_from=$date_expire_date_from->format('Y-m-d');	
		}
		foreach($this->attributes as $key=>$value){
			if($value==''){
				$this->$key=NULL;
			}
		}
		
		return parent::beforeSave();
    }

	public function statusvalidate($attribute,$params) {
		if($this->enabled==1){
			if($this->filename=='' || $this->anticipation_from_min_days=='' || $this->anticipation_to_max_days=='' || $this->max_search_months=='' || $this->order==''){
				$this->addError($attribute, 'No puede activar el registro hasta que complete la segunda instancia del mismo.');
			}
		}
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Search the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
