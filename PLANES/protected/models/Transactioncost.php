<?php

/**
 * This is the model class for table "transaction_cost".
 *
 * The followings are the available columns in table 'transaction_cost':
 * @property  public $criterio;
 * @property integer $id
 * @property integer $id_payment_method
 * @property integer $dues
 * @property string $interest
 * @property string $cft
 * @property string $expire_date_from
 * @property string $expire_date_to
 * @property integer $enabled
 * @property integer $id_legal
 * @property integer $rewards
 * @property double $ratio_convertion
 * @property string $percentage_total_amount
 * @property double $max_conv_amount
 * @property double $interest_hide
 */
class Transactioncost extends CActiveRecord
{
	public $criterio;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'transaction_cost';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('dues', 'ext.validators.UniqueattributesValidator', 'with'=>'id_payment_method,rewards'),
			array('id_payment_method, dues, enabled, id_legal, rewards', 'numerical', 'integerOnly'=>true),
			array('ratio_convertion, max_conv_amount, interest_hide,interest, cft', 'numerical'),
			array('interest, cft', 'length', 'max'=>8),
			array('percentage_total_amount', 'length', 'max'=>10),
			array('expire_date_from, expire_date_to', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, id_payment_method, dues, interest, cft, expire_date_from, expire_date_to, enabled, id_legal, rewards, ratio_convertion, percentage_total_amount, max_conv_amount, interest_hide', 'safe', 'on'=>'search'),
			array('id_payment_method, dues, interest, expire_date_from, expire_date_to, enabled, id_legal, rewards', 'required'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'mediospago' => array(self::BELONGS_TO, 'Paymentmethod', 'id_payment_method'),
			'legales' => array(self::BELONGS_TO, 'Legal', 'id_legal'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_payment_method' => 'Medios de Pago',
			'dues' => 'Cuotas',
			'interest' => 'Intereses',
			'cft' => 'CFT',
			'expire_date_from' => 'Vigencia desde',
			'expire_date_to' => 'Vigencia hasta',
			'enabled' => 'Estado',
			'id_legal' => 'Legales',
			'rewards' => 'Aplica Rewards',
			'ratio_convertion' => 'Ratio de Conversión',
			'percentage_total_amount' => 'Porc. Tot de Conv',
			'max_conv_amount' => 'Monto máx de conv',
			'interest_hide' => 'Intereses Ocultos',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		if($this->criterio!=''){

			$criteria->addSearchCondition('mediospago.rules',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('legales.title',$this->criterio,true,'OR', 'LIKE');

			$criteria->with = array('mediospago','legales');


		}else{
			$criteria->compare('id',$this->id);
			$criteria->compare('id_payment_method',$this->id_payment_method);
			$criteria->compare('dues',$this->dues);
			$criteria->compare('interest',$this->interest,true);
			$criteria->compare('cft',$this->cft,true);
			$criteria->compare('expire_date_from',$this->expire_date_from,true);
			$criteria->compare('expire_date_to',$this->expire_date_to,true);
			$criteria->compare('enabled',$this->enabled);
			$criteria->compare('id_legal',$this->id_legal);
			$criteria->compare('rewards',$this->rewards);
			$criteria->compare('ratio_convertion',$this->ratio_convertion);
			$criteria->compare('percentage_total_amount',$this->percentage_total_amount,true);
			$criteria->compare('max_conv_amount',$this->max_conv_amount);
			$criteria->compare('interest_hide',$this->interest_hide);
		}
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function afterValidate(){
        if($this->rewards==1 && ($this->ratio_convertion =='' && $this->max_conv_amount =='' && $this->percentage_total_amount =='')) {
            $this->addError('rewards','Todos los campos rewards son obligatorios');
            return; // or continue
        }
        return parent::afterValidate();
	}

	protected function beforeValidate(){
		if($this->expire_date_to!='' ){
			$this->expire_date_to = date_create_from_format("d/m/Y",$this->expire_date_to);
			$this->expire_date_to =date_format( $this->expire_date_to,"d/m/Y");
		}

		if($this->expire_date_from!=''){
			$this->expire_date_from = date_create_from_format("d/m/Y",$this->expire_date_from);
			$this->expire_date_from =date_format( $this->expire_date_from,"d/m/Y");
		}

		return parent::beforeValidate();
    }

	public static function normalize_dates($model){
		$model->criterio=array();
		$expire_date_to= explode(" ",$model->expire_date_to);
		$model->expire_date_to=$expire_date_to[0];
		$expire_date_from= explode(" ",$model->expire_date_from);
		$model->expire_date_from=$expire_date_from[0];
		return $model;
	}

	protected function afterFind() {
		$this->expire_date_to =Utils::date_spa($this->expire_date_to);
		$this->expire_date_from =Utils::date_spa($this->expire_date_from);
		return parent::afterFind();
	}

	protected function beforeSave ()
    {
    	$this->id_user=Yii::app()->user->id;
		$this->created_at=date("Y-m-d");
		$date_expire_date_to = DateTime::createFromFormat('d/m/Y', $this->expire_date_to);
		$this->expire_date_to=$date_expire_date_to->format('Y-m-d');

		$date_expire_date_from = DateTime::createFromFormat('d/m/Y', $this->expire_date_from);
		$this->expire_date_from=$date_expire_date_from->format('Y-m-d');
		return parent::beforeSave ();
    }

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Transactioncost the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
