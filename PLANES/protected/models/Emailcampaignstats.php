<?php

/**
 * This is the model class for table "email_campaignstats".
 *
 * The followings are the available columns in table 'email_campaignstats':
 * @property integer $id
 * @property integer $id_campaign
 * @property string $updated_info
 * @property string $updated_detail 
 * @property string $name
 * @property string $subject
 * @property string $start_date
 * @property integer $sent_mails
 * @property integer $delivered
 * @property integer $bounced
 * @property string $date_end
 * @property string $from_name
 * @property string $from_mail
 * @property integer $track_links
 * @property integer $track_reads
 * @property integer $g_analytics
 * @property integer $clicktale
 * @property string $date_created
 * @property integer $failed
 * @property integer $unsus
 * @property integer $sent
 * @property string $date
 * @property integer $fail
 * @property integer $bounced_soft
 * @property integer $bounced_hard
 * @property integer $bounced_spam
 * @property integer $bounced_invalid
 * @property text $bounced_topdomains
 * @property text $bounced_detail
 * @property integer $not_send
 * @property integer $pending
 * @property integer $reads_val
 * @property integer $reads_total
 * @property date $first_day_reads
 * @property string $reads_data_timelapse
 * @property string $reads_data_weekly
 * @property string $reads_data_hourly
 * @property integer $not_reads
 * @property text $open_detail
 * @property integer $refered
 * @property integer $refered_reads
 * @property integer $new_suscribers
 * @property integer $clicks_unique
 * @property integer $clicks_total
 * @property string $clicks_data
 * @property double $ctr
 * @property double $ctor
 * @property string $device_dev_type
 * @property string $device_client
 * @property string $device_browser
 * @property string $device_os
 * @property text $device_data
 * @property string $lists
 * @property string $reply_to
 * @property integer $subscribers
 * @property string $status
 * @property string $id_owner 
 * @property integer $id_datamedia

 */
class Emailcampaignstats extends CActiveRecord
{
	public $criterio;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'email_campaignstats';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_campaign, name, start_date, sent_mails, date_end, from_mail', 'required'),
			array('id_campaign, sent_mails, delivered, bounced, not_reads, refered, refered_reads, new_suscribers, clicks_unique, clicks_total, subscribers', 'numerical', 'integerOnly'=>true),
			array('ctr, ctor', 'numerical'),
			array('name, from_name, from_mail, reply_to', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, id_campaign, name, start_date, sent_mails, delivered, bounced, date_end, from_name, from_mail, date_created, failed, unsus, sent, reply_to', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'emailcampaign' => array(self::BELONGS_TO, 'Emailcampaign', 'id_campaign'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_campaign' => 'Id de Campaña',
			'updated_info' => 'Última Actualización',
			'updated_detail' => 'Última Actualización de Detalles',			
			'name' => 'Nombre',
			'start_date' => 'Fecha de Inicio',
			'sent_mails' => 'Mails Enviados',
			'delivered' => 'Delivered',
			'bounced' => 'Bounced',
			'date_end' => 'Date End',
			'from_name' => 'From Name',
			'from_mail' => 'From Mail',
			'track_links' => 'Track Links',
			'track_reads' => 'Track Reads',
			'g_analytics' => 'G Analytics',
			'clicktale' => 'Clicktale',
			'date_created' => 'Date Created',
			'failed' => 'Failed',
			'unsus' => 'Unsus',
			'sent' => 'Sent',
			'date' => 'Date',
			'fail' => 'Fail',
			'bounced_soft' => 'Bounced Soft',
			'bounced_hard' => 'Bounced Hard',
			'bounced_spam' => 'Bounced Spam',
			'bounced_invalid' => 'Bounced Invalid',
			'bounced_topdomains' => 'Top 10 Dominios',
			'bounced_detail' => 'Detalle rechazados',
			'not_send' => 'Not Send',
			'pending' => 'Pending',
			'reads_val' => 'Reads Val',
			'reads_total' => 'Reads Total',
			'first_day_reads' => 'First Day Reads',
			'reads_data_timelapse' => 'Reads Data Timelapse',
			'reads_data_weekly' => 'Reads Data Weekly',
			'reads_data_hourly' => 'Reads Data Hourly',
			'not_reads' => 'Not Reads',
			'open_detail' => 'Detalle de aperturas',
			'refered' => 'Refered',
			'refered_reads' => 'Refered Reads',
			'new_suscribers' => 'New Suscribers',
			'clicks_unique' => 'Clicks Unique',
			'clicks_total' => 'Clicks Total',
			'clicks_data' => 'Clicks Data',
			'ctr' => 'Ctr',
			'ctor' => 'Ctor',
			'device_dev_type' => 'Device Dev Type',
			'device_client' => 'Device Client',
			'device_browser' => 'Device Browser',
			'device_os' => 'Device Os',
			'device_data' => 'Datos de dispositivos',
			'lists' => 'Lists',
			'reply_to' => 'Reply To',
			'subscribers' => 'Suscriptores',
			'id_owner' => 'Dueño',
			'subscribers' => 'Suscriptores',
			'id_datamedia' => 'Identificador de Datamedia',
			'clicks_detail' => 'Detalle de clicks',
			
			
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{  
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('id_campaign',$this->id_campaign);
		$criteria->compare('updated_info',$this->updated_info);
		$criteria->compare('updated_detail',$this->updated_detail);
		$criteria->compare('name',$this->name);
		$criteria->compare('start_date',$this->start_date);
		$criteria->compare('sent_mails',$this->sent_mails);
		$criteria->compare('delivered',$this->delivered);
		$criteria->compare('bounced',$this->bounced);
		$criteria->compare('date_end',$this->date_end);
		$criteria->compare('from_name',$this->from_name);
		$criteria->compare('from_mail',$this->from_mail);
		$criteria->compare('track_links',$this->track_links);
		$criteria->compare('track_reads',$this->track_reads);
		$criteria->compare('g_analytics',$this->g_analytics);
		$criteria->compare('clicktale',$this->clicktale);
		$criteria->compare('date_created',$this->date_created);
		$criteria->compare('failed',$this->failed);
		$criteria->compare('unsus',$this->unsus);
		$criteria->compare('sent',$this->sent);
		$criteria->compare('date',$this->date);
		$criteria->compare('fail',$this->fail);
		$criteria->compare('bounced_soft',$this->bounced_soft);
		$criteria->compare('bounced_hard',$this->bounced_hard);
		$criteria->compare('bounced_spam',$this->bounced_spam);
		$criteria->compare('bounced_invalid',$this->bounced_invalid);
		$criteria->compare('bounced_top_domains',$this->bounced_top_domains);
		$criteria->compare('not_send',$this->not_send);
		$criteria->compare('pending',$this->pending);
		$criteria->compare('reads_val',$this->reads_val);
		$criteria->compare('reads_total',$this->reads_total);
		$criteria->compare('first_day_reads',$this->first_day_reads);
		$criteria->compare('reads_data_timelapse',$this->reads_data_timelapse);
		$criteria->compare('reads_data_weekly',$this->reads_data_weekly);
		$criteria->compare('reads_data_hourly',$this->reads_data_hourly);
		$criteria->compare('not_reads',$this->not_reads);
		$criteria->compare('open_detail',$this->not_reads);
		$criteria->compare('refered',$this->refered);
		$criteria->compare('refered_reads',$this->refered_reads);
		$criteria->compare('new_suscribers',$this->new_suscribers);
		$criteria->compare('clicks_unique',$this->clicks_unique);
		$criteria->compare('clicks_total',$this->clicks_total);
		$criteria->compare('clicks_data',$this->clicks_data);
		$criteria->compare('clicks_detail',$this->clicks_data);
		$criteria->compare('ctr',$this->ctr);
		$criteria->compare('ctor',$this->ctor);
		$criteria->compare('device_dev_type',$this->device_dev_type);
		$criteria->compare('device_client',$this->device_client);
		$criteria->compare('device_browser',$this->device_browser);
		$criteria->compare('device_os',$this->device_os);
		$criteria->compare('device_data',$this->device_data);
		$criteria->compare('lists',$this->lists);
		$criteria->compare('reply_to',$this->reply_to);
		$criteria->compare('subscribers',$this->subscribers);
		$criteria->compare('id_owner',$this->id_owner);
		$criteria->compare('id_datamedia',$this->id_owner);
		$var= new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
		return($var);
	}

	
	public function searchMyCampaigns()
	{
		$sorter = new CSort;
		$sorter->defaultOrder = 'id';
		$sorter->attributes = array(
				'id'=>'id',
				'name'=>'name',
				'start_date'=>'start_date', 
				'updated_info'=>'updated_info',
				'sent_mails'=>'sent_mails',
				'date_end'=>'date_end',
		);

		$id_user=Yii::app()->user->id;		
		
		$criteria = new CDbCriteria;
		$criteria->select = 't.*';
		$criteria->join = 'INNER JOIN email_campaign AS ec ON id_campaign = ec.id';
		$criteria->addCondition("ec.deleted='0'");
		if(!Yii::app()->user->checkAccess('marketingadmin')){
			//$criteria->distinct=true;
			$criteria->group='ec.id';
			#$id_user=0; // stat de campaña general
		}
		else{
			$criteria->addCondition('id_owner = :idUser');
			$criteria->params=array(':idUser'=>$id_user,);
		}
		
		if($this->criterio!=''){
			$criteria->addSearchCondition('ec.name', $this->criterio);
		}
		
		$var= new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=>$sorter
		));		 
		return $var;
	}
	
	public function searchCampaignsbyid($id)
	{
		$id_user=$id;	
		if(Yii::app()->user->checkAccess('permisos')){
			#$id_user=0; // stat de campaña general
		}
		$sorter = new CSort;
		$sorter->defaultOrder = 'start_date DESC';
		$sorter->attributes = array(
				'id'=>'id',
				'name'=>'name',
				'start_date'=>'start_date', 
				'updated_info'=>'updated_info',
				'sent_mails'=>'sent_mails',
				'date_end'=>'date_end',
		);
		
		$criteria = new CDbCriteria;
		$criteria->select = '*';
		$criteria->join = 'INNER JOIN email_campaign AS ec ON id_campaign = ec.id';
		$criteria->addCondition("ec.deleted='0'");
		$criteria->addCondition('id_owner = :idUser');
		$criteria->params=array(
			':idUser'=>$id_user,
		);
		if($this->criterio!=''){
			$criteria->addSearchCondition('ec.name', $this->criterio);
		}
		
		$var= new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=>$sorter
		));		 
		return $var;
	}
	
	public function getSubStats($id_camp=0)
	{	
	
		$criteria = new CDbCriteria;
		$criteria->select = '*';
		$criteria->addCondition("id_owner <> 0 ");
		$criteria->addCondition('id_campaign = :idCamp');
		$criteria->params=array(
			':idCamp'=>$id_camp,
		);
		$var= new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));		 
		return $var;
	}
	
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Emailcampaignstats the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	
	public function getSentPercentage()
	{
		if(!empty($this->sent_mails)){
			return round(($this->delivered*100/$this->sent_mails),1);
		}
		else{
			return 0;			
	}
	}
	
	public function getRejectedPercentage()
	{
		if(!empty($this->sent_mails)){
			return round(($this->bounced*100/$this->sent_mails),1);
		}
		else{
			return 0;
		}
	}
}
