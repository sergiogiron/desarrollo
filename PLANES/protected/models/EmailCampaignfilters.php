<?php

/**
 * This is the model class for table "email_campaignfilters".
 *
 * The followings are the available columns in table 'email_campaignfilters':
 * @property  public $criterio;
 * @property integer $id
 * @property integer $id_campaign
 * @property string $filter_name
 * @property string $content
 *
 * The followings are the available model relations:
 * @property EmailCampaign $idCampaign
 */
class EmailCampaignfilters extends CActiveRecord
{
	public $criterio;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'email_campaignfilters';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
					
			array('id_campaign, filter_name, content', 'required'),
			array('id_campaign', 'numerical', 'integerOnly'=>true),
			array('filter_name', 'length', 'max'=>32),
					// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, id_campaign, filter_name, content', 'safe', 'on'=>'search'),
		);
	}

	public function behaviors(){
		return array(
			// Classname => path to Class
			'ActiveRecordLogableBehavior'=>
				'application.behaviors.ActiveRecordLogableBehavior',
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'campana' => array(self::BELONGS_TO, 'EmailCampaign', 'id_campaign'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_campaign' => 'Id Campaign',
			'filter_name' => 'Filter Name',
			'content' => 'Content',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		
		if($this->criterio!=''){
			$criteria->addSearchCondition('id',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('id_campaign',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('filter_name',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('content',$this->criterio,true,'OR', 'LIKE');
	
		}else{	
			$criteria->compare('id',$this->id);
			$criteria->compare('id_campaign',$this->id_campaign);
			$criteria->compare('filter_name',$this->filter_name,true);
			$criteria->compare('content',$this->content,true);
		}
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	protected function beforeValidate()
	{
		return parent::beforeValidate();
    }	
	
	public static function normalize_dates($model){
		$model->criterio=array();
		return $model;		
	}
	
	protected function afterFind() 
	{
		return parent::afterFind();
	}
	
	protected function beforeSave ()
    {
		return parent::beforeSave ();
    }
	
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return EmailCampaignfilters the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
