<?php

/**
 * This is the model class for table "exclude_rules".
 *
 * The followings are the available columns in table 'exclude_rules':
 * @property  public $criterio;
 * @property integer $id
 * @property integer $id_product
 * @property integer $id_iata_country_master
 * @property string $expire_date_to
 * @property string $expire_date_from
 *
 * The followings are the available model relations:
 * @property IataCountryMaster $idIataCountryMaster
 * @property Products $idProduct
 */
class Excluderules extends CActiveRecord
{
	public $criterio;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'exclude_rules';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_iata_country_master', 'ext.validators.UniqueattributesValidator', 'with'=>'id_channel,id_subchannel,id_product'),

			//array('expire_date_to, expire_date_from', 'date', 'format'=>'dd/mm/yyyy hh:mm:ss', 'message'=>'{attribute}: no es una fecha'),

				array('id_product,id_channel,id_subchannel, id_iata_country_master, expire_date_to, expire_date_from', 'required','message'=>'Debe completar este campo.'),
				array('enabled,id_product,id_channel,id_subchannel, id_iata_country_master', 'numerical', 'integerOnly'=>true),
					// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, id_product, id_iata_country_master, expire_date_to, expire_date_from', 'safe', 'on'=>'search'),
		);
	}

	public function behaviors(){
		return array(
			// Classname => path to Class
			'ActiveRecordLogableBehavior'=>
				'application.behaviors.ActiveRecordLogableBehavior',
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'countrymaster' => array(self::BELONGS_TO, 'Iatacountrymaster', 'id_iata_country_master'),
			'producto' => array(self::BELONGS_TO, 'Products', 'id_product'),
			'canal' => array(self::BELONGS_TO, 'Channels', 'id_channel'),
			'subcanal' => array(self::BELONGS_TO, 'Subchannels', 'id_subchannel'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_product' => 'Producto',
			'id_iata_country_master' => 'Pais',
			'expire_date_to' => 'Expira hasta',
			'expire_date_from' => 'Expira desde',
			'id_channel' => 'Canal',
			'id_subchannel' => 'Subcanal',
			'enabled' => 'Estado',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		if($this->criterio!=''){
			$criteria->addSearchCondition('canal.title',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('subcanal.title',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('producto.title',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('countrymaster.Country',$this->criterio,true,'OR', 'LIKE');

			$criteria->addSearchCondition('expire_date_to',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('expire_date_from',$this->criterio,true,'OR', 'LIKE');

			$criteria->with = array('subcanal','canal','producto','countrymaster');
		}else{
			$criteria->compare('id',$this->id);
			$criteria->compare('id_product',$this->id_product);
			$criteria->compare('id_channel',$this->id_channel);
			$criteria->compare('id_subchannel',$this->id_subchannel);
			$criteria->compare('id_iata_country_master',$this->id_iata_country_master);
			$criteria->compare('expire_date_to',$this->expire_date_to,true);
			$criteria->compare('expire_date_from',$this->expire_date_from,true);
		}
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	protected function beforeValidate(){
		if($this->expire_date_to!='' ){
			$this->expire_date_to = date_create_from_format("d/m/Y",$this->expire_date_to);
			$this->expire_date_to =date_format( $this->expire_date_to,"d/m/Y");
		}

		if($this->expire_date_from!=''){
			$this->expire_date_from = date_create_from_format("d/m/Y",$this->expire_date_from);
			$this->expire_date_from =date_format( $this->expire_date_from,"d/m/Y");
		}

		return parent::beforeValidate();
    }

	public static function normalize_dates($model){
		$model->criterio=array();
		$expire_date_to= explode(" ",$model->expire_date_to);
		$model->expire_date_to=$expire_date_to[0];
		$expire_date_from= explode(" ",$model->expire_date_from);
		$model->expire_date_from=$expire_date_from[0];
		return $model;
	}

	protected function afterFind() {
		$this->expire_date_to =Utils::date_spa($this->expire_date_to);
		$this->expire_date_from =Utils::date_spa($this->expire_date_from);
		return parent::afterFind();
	}

	protected function beforeSave ()
    {
		$this->id_user=Yii::app()->user->id;
		$this->created_at=date("Y-m-d H:i:s");
		$date_expire_date_to = DateTime::createFromFormat('d/m/Y', $this->expire_date_to);
		$this->expire_date_to=$date_expire_date_to->format('Y-m-d');

		$date_expire_date_from = DateTime::createFromFormat('d/m/Y', $this->expire_date_from);
		$this->expire_date_from=$date_expire_date_from->format('Y-m-d');
		return parent::beforeSave ();
    }

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ExcludeRules the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
