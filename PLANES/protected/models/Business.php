<?php

/**
 * This is the model class for table "business".
 *
 * The followings are the available columns in table 'business':
 * @property  public $criterio;
 * @property integer $id
 * @property integer $id_user
 * @property string $filename
 * @property string $ext
 * @property integer $obdt
 * @property string $title
 * @property string $address
 * @property string $phone
 * @property string $email
 * @property string $created_at
 * @property string $name
 * @property integer $order
 * @property integer $enabled
 */
class Business extends CActiveRecord
{
	public $criterio;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'business';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('email', 'email', 'message'=>'{attribute}: no es un email válido'),

				array('filename, ext,title', 'required','message'=>'Debe completar este campo.'),
				array('id_user, obdt, order, enabled', 'numerical', 'integerOnly'=>true),
				array('filename', 'length', 'max'=>64),
				array('ext', 'length', 'max'=>12),
				array('title, address', 'length', 'max'=>50),
				array('phone', 'length', 'max'=>35),
				array('email', 'length', 'max'=>80),
				array('name', 'length', 'max'=>62),
					// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, id_user, filename, ext, obdt, title, address, phone, email, created_at, name, order', 'safe', 'on'=>'search'),
		);
	}

	public function behaviors(){
		return array(
			// Classname => path to Class
			'ActiveRecordLogableBehavior'=>
				'application.behaviors.ActiveRecordLogableBehavior',
		);
	}

	/**
	 * @return array relational rules.
	 */

	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(

		 );
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_user' => 'Id User',
			'filename' => 'Imagen',
			'ext' => 'Ext',
			'obdt' => 'Tiene OBT',
			'title' => 'Título',
			'address' => 'Dirección',
			'phone' => 'Teléfono de contacto',
			'email' => 'Email de contacto',
			'created_at' => 'Created At',
			'enabled' => 'Estado',
			'name' => 'Nombre de contacto',
			'order' => 'Orden',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		if($this->criterio!=''){
			$criteria->addSearchCondition('id',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('id_user',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('filename',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('ext',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('obdt',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('title',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('address',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('phone',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('email',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('created_at',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('name',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('order',$this->criterio,true,'OR', 'LIKE');


		}else{
			$criteria->compare('id',$this->id);
			$criteria->compare('enabled',$this->enabled);
			$criteria->compare('id_user',$this->id_user);
			$criteria->compare('filename',$this->filename,true);
			$criteria->compare('ext',$this->ext,true);
			$criteria->compare('obdt',$this->obdt);
			$criteria->compare('title',$this->title,true);
			$criteria->compare('address',$this->address,true);
			$criteria->compare('phone',$this->phone,true);
			$criteria->compare('email',$this->email,true);
			$criteria->compare('created_at',$this->created_at,true);
			$criteria->compare('name',$this->name,true);
			$criteria->compare('order',$this->order);
		}
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	protected function beforeValidate(){












				return parent::beforeValidate();
    }

	public static function normalize_dates($model){
		$model->criterio=array();












				return $model;
	}

	protected function afterFind() {

























		return parent::afterFind();
	}

	protected function beforeSave ()
    {
		$this->created_at=date("Y-m-d H:i:s");
																																																																											foreach($this->attributes as $key=>$value){
			if($value==''){
				$this->$key=NULL;
			}
		}
		return parent::beforeSave ();
    }

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Business the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
