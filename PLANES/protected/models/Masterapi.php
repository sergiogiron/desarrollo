<?php

/**
 * This is the model class for table "master_api".
 *
 * The followings are the available columns in table 'master_api':
 * @property  public $criterio;
 * @property integer $id
 * @property string $name
 * @property integer $expire_apy_key
 * @property integer $rules_time
 * @property integer $enabled
 * @property string $created_at
 * @property integer $id_user
 *
 * The followings are the available model relations:
 * @property ApiUser[] $apiUsers
 */
class Masterapi extends CActiveRecord
{
	public $criterio;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'master_api';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
					
			array('name', 'required'),
			array('expire_apy_key, rules_time, enabled, id_user', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>32),
					// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, expire_apy_key, rules_time, enabled, created_at, id_user', 'safe', 'on'=>'search'),
		);
	}

	public function behaviors(){
		return array(
			// Classname => path to Class
			'ActiveRecordLogableBehavior'=>
				'application.behaviors.ActiveRecordLogableBehavior',
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'apiUsers' => array(self::HAS_MANY, 'ApiUser', 'id_master_api'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Master API',
			'expire_apy_key' => 'Expira',
			'rules_time' => 'Regla de tiempo',
			'enabled' => 'Estado',
			'created_at' => 'Fecha de Registro',
			'id_user' => 'Usuario',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		
		if($this->criterio!=''){
			
			$criteria->addSearchCondition('name',$this->criterio,true,'OR', 'LIKE');
			
	
		}else{	
			$criteria->compare('id',$this->id);
			$criteria->compare('name',$this->name,true);
			$criteria->compare('expire_apy_key',$this->expire_apy_key);
			$criteria->compare('rules_time',$this->rules_time);
			$criteria->compare('enabled',$this->enabled);
			$criteria->compare('created_at',$this->created_at,true);
			$criteria->compare('id_user',$this->id_user);
		}
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	protected function beforeValidate(){
						
						
						
						
						
						
						
				return parent::beforeValidate();
    }	
	
	public static function normalize_dates($model){
		$model->criterio=array();
					
					
					
					
					
					
					
				return $model;		
	}
	
	protected function afterFind() {
						
					
						
					
						
					
						
					
						
					
						
					
						
					
						
		return parent::afterFind();
	}	
	
	protected function beforeSave ()
    {	
																																														return parent::beforeSave ();
    }	
	
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Masterapi the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
