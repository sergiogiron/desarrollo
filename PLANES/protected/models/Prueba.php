<?php

/**
 * This is the model class for table "prueba".
 *
 * The followings are the available columns in table 'prueba':
 * @property  public $criterio;
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property string $title
 * @property string $url
 * @property string $email
 * @property string $phone
 * @property integer $enabled
 * @property string $text_ritch
 */
class Prueba extends CActiveRecord
{
	public $criterio;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'prueba';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('enabled', 'numerical', 'integerOnly'=>true),

			array('name, description, title, url, email, phone, enabled, text_ritch', 'required'),
			array('name', 'length', 'max'=>62),
			array('description', 'length', 'max'=>520),
			array('title', 'length', 'max'=>65),
			array('url', 'url', 'message'=>'{attribute}: no es una url válida'),
			array('url', 'length', 'max'=>260),
			array('email', 'email', 'message'=>'{attribute}: no es un email válido'),
			array('email', 'length', 'max'=>80),
			array('phone', 'length', 'max'=>35),
					
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, description, title, url, email, phone, enabled, text_ritch', 'safe', 'on'=>'search'),
		);
	}

	public function behaviors(){
		return array(
			// Classname => path to Class
			'ActiveRecordLogableBehavior'=>
				'application.behaviors.ActiveRecordLogableBehavior',
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'description' => 'Description',
			'title' => 'Title',
			'url' => 'Url',
			'email' => 'Email',
			'phone' => 'Phone',
			'enabled' => 'Enabled',
			'text_ritch' => 'Text Ritch',
			'datepicker' => 'Date',
			'datetime' => 'Date Time',
			'testname' => 'Testname',
			'file' => 'File',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		
		if($this->criterio!=''){
			$criteria->addSearchCondition('id',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('name',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('description',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('title',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('url',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('email',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('phone',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('enabled',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('text_ritch',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('datepicker',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('timepicker',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('file',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('testname',$this->criterio,true,'OR', 'LIKE');
	
		}else{	
			$criteria->compare('id',$this->id);
			$criteria->compare('name',$this->name,true);
			$criteria->compare('description',$this->description,true);
			$criteria->compare('title',$this->title,true);
			$criteria->compare('url',$this->url,true);
			$criteria->compare('email',$this->email,true);
			$criteria->compare('phone',$this->phone,true);
			$criteria->compare('enabled',$this->enabled);
			$criteria->compare('text_ritch',$this->text_ritch,true);
			$criteria->compare('datepicker',$this->datepicker,true,'OR', 'LIKE');
			$criteria->compare('timepicker',$this->timepicker,true,'OR', 'LIKE');
			$criteria->compare('file',$this->file,true,'OR', 'LIKE');
			$criteria->compare('testname',$this->testname,true,'OR', 'LIKE');
		}
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	protected function beforeValidate(){
						
						
						
						
						
						
						
						
						
				return parent::beforeValidate();
    }	
	
	public static function normalize_dates($model){
		$model->criterio=array();
					
					
					
					
					
					
					
					
					
				return $model;		
	}
	
	protected function afterFind() {
						
					
						
					
						
					
						
					
						
					
						
					
						
					
						
					
						
					
						
		return parent::afterFind();
	}	
	
	protected function beforeSave ()
    {	
																																																										return parent::beforeSave ();
    }	
	
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Prueba the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
