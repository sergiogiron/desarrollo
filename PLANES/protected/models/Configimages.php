<?php

/**
 * This is the model class for table "config_images".
 *
 * The followings are the available columns in table 'config_images':
 * @property  public $criterio;
 * @property integer $id
 * @property integer $id_user
 * @property integer $id_menu
 * @property string $position
 * @property string $width
 * @property string $height
 * @property string $created_at
 * @property string $transition_interval
 * @property integer $title_required
 * @property integer $link_required 
 */
class Configimages extends CActiveRecord
{
	public $criterio;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'config_images';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
					
				array('id_user, id_menu, position, width, height, created_at', 'required','message'=>'Debe completar este campo.'),
				array('id_user, id_menu, title_required, link_required, transition_interval', 'numerical', 'integerOnly'=>true),
				array('position', 'length', 'max'=>64),
				array('width, height', 'length', 'max'=>5),
					// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, id_user, id_menu, position, width, height, created_at', 'safe', 'on'=>'search'),
		);
	}

	public function behaviors(){
		return array(
			// Classname => path to Class
			'ActiveRecordLogableBehavior'=>
				'application.behaviors.ActiveRecordLogableBehavior',
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_user' => 'Id User',
			'id_menu' => 'Id Menu',
			'position' => 'Posición',
			'title_required' => 'Requiere título',
			'link_required' => 'Requiere enlace',			
			'width' => 'Ancho',
			'enabled' => 'Estado',
			'height' => 'Alto',
			'created_at' => 'Created At',
			'transition_interval' => 'Intervalo de transición en segundos',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search($id)
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		
		if($this->criterio!=''){
			$criteria->addSearchCondition('width',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('height',$this->criterio,true,'OR', 'LIKE');
			$criteria->compare('id_menu',$id);
			$criteria->order='position asc ,width desc';
	
		}else{	
			$criteria->compare('width',$this->width,true);
			$criteria->compare('height',$this->height,true);
			$criteria->compare('id_menu',$id);
			$criteria->order='position asc,width desc';
		}
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	protected function beforeValidate(){
		return parent::beforeValidate();
    }	
	
	public static function normalize_dates($model){
		$model->criterio=array();
					
					
					
					
					
					
					
				return $model;		
	}
	
	protected function afterFind() {
						
					
						
					
						
					
						
					
						
					
						
					
						
					
						
		return parent::afterFind();
	}	
	
	protected function beforeSave ()
    {	
		$this->created_at=date("Y-m-d H:i:s");
																																													return parent::beforeSave ();
    }	
	
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Configimages the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
