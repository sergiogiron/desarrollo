<?php

/**
 * This is the model class for table "email_footer".
 *
 * The followings are the available columns in table 'email_footer':
 * @property integer $id
 * @property string $name
 * @property string $content
 * @property tinyint $enabled
 * @property string $id_user
 * @property string $created_at
 * @property tinyint $deleted
 */
class Emailfooter extends CActiveRecord
{
	public $criterio;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'email_footer';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name,content','required',			 
			'message'=>'Debe completar este campo.'
			 ),
			
			array('name', 'validadorFooter', 'attributeName'=>'name'),
			// array('name', 'unique', 'attributeName'=>'name'),
			array('enabled', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>32),
			array('id_user', 'length', 'max'=>11),
			array('content', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, content, enabled, id_user, created_at', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Título',
			'content' => 'Contenido',
			'enabled' => 'Estado',
			'id_user' => 'Id User',
			'created_at' => 'Fecha de Creación',
			'deleted' => 'Eliminado',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		if($this->criterio!=''){
			$criteria->addSearchCondition('id',$this->criterio, true, 'OR', 'LIKE');
			$criteria->addSearchCondition('name',$this->criterio, true, 'OR', 'LIKE');
			$criteria->addSearchCondition('content',$this->criterio, true, 'OR', 'LIKE');
			$criteria->addSearchCondition('enabled',$this->criterio, true, 'OR', 'LIKE');
			$criteria->addSearchCondition('id_user',$this->criterio, true, 'OR', 'LIKE');
			$criteria->addSearchCondition('created_at',$this->criterio, true, 'OR', 'LIKE');					
		}else{
			$criteria->compare('id',$this->id);
			$criteria->compare('name',$this->name,true);
			$criteria->compare('content',$this->content,true);
			$criteria->compare('enabled',$this->enabled);
			$criteria->compare('id_user',$this->id_user,true);
			$criteria->compare('created_at',$this->created_at,true);
		}
		$criteria->compare('deleted',0);
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Emailfooter the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	
	// Date stored in one format, but displayed in another format:
	
	protected function afterFind ()
    {
        // convert to display format
        /*$this->created_at = strtotime ($this->created_at);
        $this->created_at = date ('d/m/Y', $this->created_at);
        parent::afterFind ();*/
    }
	
	
	public function validadorFooter($attribute,$params) {
		$footers=Emailfooter::model()->findAllByAttributes(array('name'=>$this->name),'deleted=0');
		if(count($footers)>0){
			foreach($footers as $h){
				if($this->isNewRecord || $this->id != $h['id']){
					$this->addError($attribute, 'El nombre está en uso.');
				}
			}
		}
	}
}