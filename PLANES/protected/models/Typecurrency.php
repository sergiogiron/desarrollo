<?php

/**
 * This is the model class for table "type_currency".
 *
 * The followings are the available columns in table 'type_currency':
 * @property  public $criterio;
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property integer $enabled
 * @property string $created_ad
 * @property integer $id_user
 */
class Typecurrency extends CActiveRecord
{
	public $criterio;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'type_currency';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title, description, enabled', 'required'),
			array('enabled, id_user', 'numerical', 'integerOnly'=>true),
			array('title, description', 'length', 'max'=>32),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, title, description, enabled', 'safe', 'on'=>'search'),
		);
	}

	public function behaviors(){
		return array(
			// Classname => path to Class
			'ActiveRecordLogableBehavior'=>
				'application.behaviors.ActiveRecordLogableBehavior',
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'title' => 'Moneda',
			'description' => 'Simbolo',
			'enabled' => 'Activa',
			'created_ad' => 'Creada',
			'id_user' => 'Usuario',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		
		if($this->criterio!=''){
			
			$criteria->addSearchCondition('title',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('description',$this->criterio,true,'OR', 'LIKE');
			
	
		}else{	
			
			$criteria->compare('title',$this->title,true);
			$criteria->compare('description',$this->description,true);
			$criteria->compare('enabled',$this->enabled);
			
		}
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Typecurrency the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
