<?php

/**
 * This is the model class for table "business_exceptions".
 *
 * The followings are the available columns in table 'business_exceptions':
 * @property  public $criterio;
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property string $action
 * @property integer $id_product
 *
 * The followings are the available model relations:
 * @property Products $idProduct
 */
class Businessexceptions extends CActiveRecord
{
	public $criterio;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'business_exceptions';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
				array('id_product', 'ext.validators.UniqueattributesValidator', 'with'=>'description,action'),	
				array('id_product', 'numerical', 'integerOnly'=>true),
				array('description, action', 'length', 'max'=>45),
				array('title', 'safe'),
				array('title,id_product,description,action', 'required'),
					// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, title, description, action, id_product', 'safe', 'on'=>'search'),
		);
	}

	public function behaviors(){
		return array(
			// Classname => path to Class
			'ActiveRecordLogableBehavior'=>
				'application.behaviors.ActiveRecordLogableBehavior',
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'producto' => array(self::BELONGS_TO, 'Products', 'id_product'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'title' => 'Título',
			'description' => 'Título Interno',
			'action' => 'Acción',
			'id_product' => 'Producto',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		
		if($this->criterio!=''){
			$criteria->addSearchCondition('id',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('title',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('description',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('action',$this->criterio,true,'OR', 'LIKE');
			$criteria->addSearchCondition('id_product',$this->criterio,true,'OR', 'LIKE');
	
		}else{	
			$criteria->compare('id',$this->id);
			$criteria->compare('title',$this->title,true);
			$criteria->compare('description',$this->description,true);
			$criteria->compare('action',$this->action,true);
			$criteria->compare('id_product',$this->id_product);
		}
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	protected function beforeValidate(){
						
						
						
						
						
				return parent::beforeValidate();
    }	
	
	public static function normalize_dates($model){
		$model->criterio=array();
					
					
					
					
					
				return $model;		
	}
	
	protected function afterFind() {
						
					
						
					
						
					
						
					
						
					
						
		return parent::afterFind();
	}	
	
	protected function beforeSave ()
    {	
		$this->created_at=date("Y-m-d H:i:s");
																																	foreach($this->attributes as $key=>$value){
			if($value==''){
				$this->$key=NULL;
			}
		}			
		return parent::beforeSave ();
    }	
	
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Businessexceptions the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
