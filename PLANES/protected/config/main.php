<?php
//provisorio





switch ($_SERVER['ENVIRONMENT']) {
	case 'workstation':
		$cdnUrl = 'http://localhost/cdn';
		$appServer = 'http://localhost';
		$asterixHost = '10.85.253.67';
    $dbname = 'asterisk_reports';
    $dbuser = 'root';
    $dbpass = 'R00T782356';
		break;
	case 'development':
		$cdnUrl = 'http://dev.cdn.ttsviajes.com';
		$appServer = 'http://dev.app.ttsviajes.com';
		$asterixHost = '10.85.253.67';
    $dbname = 'asterisk_reports';
    $dbuser = 'root';
    $dbpass = 'R00T782356';
		break;
	case 'testing':
		$cdnUrl = 'http://cdn.test.ttsviajes.com';
		$appServer = 'http://app.test.ttsviajes.com';
		$asterixHost = 'localhost';
    $dbname = 'ttsviaje_asterix_reports';
    $dbuser = 'ttsviaje_office';
    $dbpass = '6p@fB4$!MaQG';
		break;
	case 'production':
		$cdnUrl = 'http://cdn.ttsviajes.com';
		$appServer = 'http://app.ttsviajes.com';
	 	$asterixHost = 'localhost';       
    $dbname = 'ttsviaje_asterix_reports';
    $dbuser = 'ttsviaje_office';
    $dbpass = '6p@fB4$!MaQG';
		break;
	default:
		$cdnUrl = 'http://cdn.ttsviajes.com';
		$appServer = 'http://app.ttsviajes.com';
		$asterixHost = 'localhost';       
    $dbname = 'DESARROLLO';
    $dbuser = 'root';
    $dbpass = 'hacienda2016';
		break;
}

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'TTS Office',
	'language'=>'es',
	// preloading 'log' component
	'preload'=>array('log'),

	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
	),

	'modules'=>array(
        'gii'=>array(
			'class'=>'system.gii.GiiModule',
			'password'=>'admin',
			'ipFilters'=>array('*','::1'),
			'generatorPaths'=>array(
				'application.gii'
			),
        ),

    ),

	'defaultController'=>'dashboard',

	'components'=>array(
		'user'=>array(
			'class' => 'WebUser',
			'loginUrl'=>array('login'),
			'allowAutoLogin'=>true,
		),
		'clientScript'=>array(
			'packages'=>array(
				'jquery'=>array(
					'baseUrl'=>$cdnUrl.'/plugins/jQuery/',
					'js'=>array('jQuery-2.2.0.min.js')
				  )
			),
		),
		'db'=>require(dirname(__FILE__).'/database.php'),
		'db_asterisk' => array(
		'class'=>'CDbConnection',
		'connectionString' => 'mysql:host=10.85.90.253;dbname=acd',
		'username' => 'tts_desarrollo',
		'password' => '',
		'charset' => 'utf8',
		),	
		'db_asterisk2' => array(
		'class'=>'CDbConnection',
		'connectionString' => 'mysql:host=10.85.253.247;dbname=acd',
		'username' => 'tts_desarrollo',
		'password' => '',
		'charset' => 'utf8',
		),		
		'db_asterisk_reports' => array(
			'class'=>'CDbConnection',
			'connectionString' => "mysql:host=$asterixHost;dbname=$dbname",
			'username' => $dbuser,
			'password' => $dbpass,
			'charset' => 'utf8',
		),				
		'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=>'site/error',
		),
		'urlManager'=>require(dirname(__FILE__).'/routes.php'),
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
				// uncomment the following to show log messages on web pages
				/*
				array(
					'class'=>'CWebLogRoute',
				),
				*/
			),
		),
		'ePdf' => array(
			'class'         => 'ext.yii-pdf.EYiiPdf',
			'params'        => array(
				'mpdf'     => array(
					'librarySourcePath' => 'application.vendors.mpdf.*',
					'constants'         => array(
						'_MPDF_TEMP_PATH' => Yii::getPathOfAlias('application.runtime'),
					),
					'class'=>'mpdf', 					
				),				 
			),
		),
	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>require(dirname(__FILE__).'/params.php'),
);
