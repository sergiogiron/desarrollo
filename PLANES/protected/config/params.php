<?php
switch ($_SERVER['ENVIRONMENT']) {
	case 'workstation':
		$cdnUrl = 'http://localhost/cdn';
		$appServer = 'http://localhost';
		break;
	case 'development':
		$cdnUrl = 'http://dev.cdn.ttsviajes.com';
		$appServer = 'http://dev.app.ttsviajes.com';
		break;
	case 'testing':
		$cdnUrl = 'http://cdn.test.ttsviajes.com';
		$appServer = 'http://app.test.ttsviajes.com';
		break;
	case 'production':
		$cdnUrl = 'http://cdn.ttsviajes.com';
		$appServer = 'http://app.ttsviajes.com';
		break;
	default:
		$cdnUrl = 'http://cdn.ttsviajes.com';
		$appServer = 'http://app.ttsviajes.com';
		break;
}
switch ($_SERVER['ENVIRONMENT']) {
	case 'workstation':
		// this contains the application parameters that can be maintained via GUI
		return array(
			//'api_url'=>'https://servicios.ttsviajes.com:8300/Api',
			'api_url'=>'http://dev.servicios.ttsviajes.com:8100/Api',

			// datamedia address
			'datamedia_url'=>'http://dattamedia2.fmmail.in/api.php',
			// datamedia user
			'datamedia_user'=>'desarrollador',
			'server_url'=>'http://www.ttsviajes.com',
			// datamedia password
			'datamedia_pass'=>'3563#tts',
			// Lista que estamos usando actualmente
			'datamedia_list'=>2438,
			// direccion de donde extraemos las novedades de contactos
			'contacts_news_api_url'=>'http://10.85.253.150:8300/api/Contacto/Novedades',
			// Dado un Id de contacto, devuelve el Mail: 10.85.253.150:8300/api/Contacto/GetMail/<id>
			'contact_getmail_api_url'=>'http://10.85.253.150:8300/api/Contacto/GetMail',
			// direccion de api de testing
			//'contacts_api_url'=>'https://servicios.ttsviajes.com:8300/Api',
			'contacts_api_url'=>'http://10.85.253.150:8300/Api',
			// direccion del subdominio (news.ttsviajes)
			'news_subdomain_url'=>'http://news.ttsviajes2-staging.wavenet.com',

			'api_reservas_url'=>'http://dev.servicios.ttsviajes.com:7000',

			'api_collects_url'=>'http://dev.servicios.ttsviajes.com:7000',

			// ID cron de Enviar Campañas
			'cron_campaign_id'=>1,
			// ID cron de Traer Estadisticas
			'cron_stats_id'=>2,
			// Flag para Enviar Campañas: 1 activado el envio de campañas || 0 desactivado
			'cron_campaign_flag'=>0,
			// Flag para Traer estadísticas: 1 activado traer stats || 0 desactivado
			'cron_stats_flag'=>0,
			// URL del CDN en el ambiente correspondiente
			'cdnUrl'=>$cdnUrl,

			'appServer' => $appServer,

			// URL del ES
			'elasticUrl'=>"http://dev.app.ttsviajes.com:9200"
		);
		break;
	case 'development':
		// this contains the application parameters that can be maintained via GUI
		return array(
			//'api_url'=>'https://servicios.ttsviajes.com:8300/Api',
			'api_url'=>'http://dev.servicios.ttsviajes.com:8100/Api',
			
			// datamedia address
			'datamedia_url'=>'http://dattamedia2.fmmail.in/api.php',
			// datamedia user
			'datamedia_user'=>'desarrollador',
			'server_url'=>'http://www.ttsviajes.com',
			// datamedia password
			'datamedia_pass'=>'3563#tts',
			// Lista que estamos usando actualmente
			'datamedia_list'=>2438,
			// direccion de donde extraemos las novedades de contactos
			'contacts_news_api_url'=>'http://10.85.253.150:8300/api/Contacto/Novedades',
			// Dado un Id de contacto, devuelve el Mail: 10.85.253.150:8300/api/Contacto/GetMail/<id>
			'contact_getmail_api_url'=>'http://10.85.253.150:8300/api/Contacto/GetMail',
			// direccion de api de testing
			//'contacts_api_url'=>'https://servicios.ttsviajes.com:8300/Api',
			'contacts_api_url'=>'http://10.85.253.150:8300/Api',
			// direccion del subdominio (news.ttsviajes)
			'news_subdomain_url'=>'http://news.ttsviajes2-staging.wavenet.com',

			'api_reservas_url'=>'http://10.85.253.150:7000',
			'api_collects_url'=>'http://10.85.253.150:7000',

			// ID cron de Enviar Campañas
			'cron_campaign_id'=>1,
			// ID cron de Traer Estadisticas
			'cron_stats_id'=>2,
			// Flag para Enviar Campañas: 1 activado el envio de campañas || 0 desactivado
			'cron_campaign_flag'=>0,
			// Flag para Traer estadísticas: 1 activado traer stats || 0 desactivado
			'cron_stats_flag'=>0,
			// URL del CDN en el ambiente correspondiente
			'cdnUrl'=>$cdnUrl,

			'appServer' => $appServer,

			// URL del ES
			'elasticUrl'=>"http://dev.app.ttsviajes.com:9200"
		);
		break;
	case 'testing':
		// this contains the application parameters that can be maintained via GUI
		return array(
			// direccion de api de testing
			'api_url'=>'http://servicios.test.ttsviajes.com:8100/Api',
			// datamedia address
			'datamedia_url'=>'http://dattamedia2.fmmail.in/api.php',
			// datamedia user
			'datamedia_user'=>'asociados',
			'server_url'=>'http://www.ttsviajes.com',
			// datamedia password
			'datamedia_pass'=>'sodTTS#3563',
			// Lista que estamos usando actualmente
			'datamedia_list'=>2438,
			// direccion de donde extraemos las novedades de contactos
			'contacts_news_api_url'=>'http://servicios.test.ttsviajes.com:8300/Api/Contacto/Novedades',
			// Dado un Id de contacto, devuelve el Mail: 10.85.253.150:8300/api/Contacto/GetMail/<id>
			'contact_getmail_api_url'=>'http://servicios.test.ttsviajes.com:8300/Api/Contacto/GetMail',
			// direccion de api de testing
			'contacts_api_url'=>'http://servicios.test.ttsviajes.com:8300/Api',
			// direccion del subdominio (news.ttsviajes)
			'news_subdomain_url'=>'http://test.news.ttsviajes.com',

			'api_reservas_url'=>'http://servicios.test.ttsviajes.com:7500',
			'api_collects_url'=>'http://servicios.test.ttsviajes.com:7500',

			// ID cron de Enviar Campañas
			'cron_campaign_id'=>1,
			// ID cron de Traer Estadisticas
			'cron_stats_id'=>2,
			// Flag para Enviar Campañas: 1 activado el envio de campañas || 0 desactivado
			'cron_campaign_flag'=>0,
			// Flag para Traer estadísticas: 1 activado traer stats || 0 desactivado
			'cron_stats_flag'=>0,
			// URL del CDN en el ambiente correspondiente
			'cdnUrl'=>$cdnUrl,

			'appServer' => $appServer,

			// URL del ES
			'elasticUrl'=>"http://app.test.ttsviajes.com:9200"
		);
		break;
	case 'production':
		// this contains the application parameters that can be maintained via GUI
		return array(
			// direccion de api de Prod
			'api_url'=>'https://servicios.ttsviajes.com:8100/Api',
			// datamedia address
			'datamedia_url'=>'http://dattamedia2.fmmail.in/api.php',
			// datamedia user
			'datamedia_user'=>'asociados',
			'server_url'=>'http://www.ttsviajes.com',
			// datamedia password
			'datamedia_pass'=>'sodTTS#3563',
			// Lista que estamos usando actualmente
			'datamedia_list'=>2438,
			// direccion de donde extraemos las novedades de contactos
			'contacts_news_api_url'=>'https://servicios.ttsviajes.com:8300/api/Contacto/Novedades',
			// Dado un Id de contacto, devuelve el Mail: 10.85.253.150:8300/api/Contacto/GetMail/<id>
			'contact_getmail_api_url'=>'https://servicios.ttsviajes.com:8300/api/Contacto/GetMail',
			// direccion de api de testing
			//'contacts_api_url'=>'https://servicios.ttsviajes.com:8300/Api',
			'contacts_api_url'=>'https://servicios.ttsviajes.com:8300/Api',
			// direccion del subdominio (news.ttsviajes)
			'news_subdomain_url'=>'http://news.ttsviajes.com',

			'api_reservas_url'=>'https://servicios.ttsviajes.com:7000',
			'api_collects_url'=>'https://servicios.ttsviajes.com:7000',

			// ID cron de Enviar Campañas
			'cron_campaign_id'=>1,
			// ID cron de Traer Estadisticas
			'cron_stats_id'=>2,
			// Flag para Enviar Campañas: 1 activado el envio de campañas || 0 desactivado
			'cron_campaign_flag'=>1,
			// Flag para Traer estadísticas: 1 activado traer stats || 0 desactivado
			'cron_stats_flag'=>1,
			// URL del CDN en el ambiente correspondiente
			'cdnUrl'=>$cdnUrl,

			'appServer' => $appServer,

			// URL del ES
			'elasticUrl'=>"http://app.ttsviajes.com:9200"

		);
		break;
	default:
		// this contains the application parameters that can be maintained via GUI
		return array(
			'api_url'=>'http://10.85.253.150:8300/Api',
			// datamedia address
			'datamedia_url'=>'http://dattamedia2.fmmail.in/api.php',
			// datamedia user
			'datamedia_user'=>'desarrollador',
			'server_url'=>'http://www.ttsviajes.com',
			// datamedia password
			'datamedia_pass'=>'3563#tts',
			// Lista que estamos usando actualmente
			'datamedia_list'=>2438,
			// direccion de donde extraemos las novedades de contactos
			'contacts_news_api_url'=>'http://10.85.253.150:8300/api/Contacto/Novedades',
			// Dado un Id de contacto, devuelve el Mail: 10.85.253.150:8300/api/Contacto/GetMail/<id>
			'contact_getmail_api_url'=>'http://10.85.253.150:8300/api/Contacto/GetMail',
			// direccion de api de testing
			//'contacts_api_url'=>'https://servicios.ttsviajes.com:8300/Api',
			'contacts_api_url'=>'http://10.85.253.150:8300/Api',
			// direccion del subdominio (news.ttsviajes)
			'news_subdomain_url'=>'http://news.ttsviajes2-staging.wavenet.com',

			'api_reservas_url'=>'https://servicios.ttsviajes.com:7000',
			'api_collects_url'=>'https://servicios.ttsviajes.com:7000',

			// ID cron de Enviar Campañas
			'cron_campaign_id'=>1,
			// ID cron de Traer Estadisticas
			'cron_stats_id'=>2,
			// Flag para Enviar Campañas: 1 activado el envio de campañas || 0 desactivado
			'cron_campaign_flag'=>0,
			// Flag para Traer estadísticas: 1 activado traer stats || 0 desactivado
			'cron_stats_flag'=>0,
			// URL del CDN en el ambiente correspondiente
			'cdnUrl'=>$cdnUrl,

			'appServer' => $appServer,

			// URL del ES
			'elasticUrl'=>"http://app.ttsviajes.com:9200"

		);
		break;
}
