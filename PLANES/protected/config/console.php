<?php
$myhostname=exec("hostname");
switch ($myhostname) {
	case 'localhost':
		$_SERVER['ENVIRONMENT']='workstation';
		$cdnUrl = 'http://localhost/cdn';
        $asterixHost = '10.85.253.67';
        $dbname = 'asterisk_reports';
        $dbuser = 'root';
        $dbpass = 'R00T782356';
		break;
	case 'dev.ttsviajes.com':
		$_SERVER['ENVIRONMENT']='development';
		$cdnUrl = 'http://dev.cdn.ttsviajes.com';
        $asterixHost = '10.85.253.67';
        $dbname = 'asterisk_reports';                
        $dbuser = 'root';
        $dbpass = 'R00T782356';                
		break;
	case 'ttsviajes2-staging.wavenet.com':
		$_SERVER['ENVIRONMENT']='testing';
		$cdnUrl = 'http://cdn.test.ttsviajes.com';
        $asterixHost = 'localhost';
        $dbname = 'ttsviaje_asterisk_reports';
        $dbuser = 'ttsviaje_office';
        $dbpass = '6p@fB4$!MaQG';                                                
		break;
	case 'ttsviajes2.wavenet.com':
		$_SERVER['ENVIRONMENT']='production';
		$cdnUrl = 'http://cdn.ttsviajes.com';
        $asterixHost = 'localhost';       
        $dbname = 'ttsviaje_asterisk_reports';
        $dbuser = 'ttsviaje_office';
        $dbpass = '6p@fB4$!MaQG';                                                                                
		break;
	default:
		$_SERVER['ENVIRONMENT']='testing';
		$cdnUrl = 'http://cdn.ttsviajes.com';
        $asterixHost = 'localhost';    
        $dbname = 'ttsviaje_asterisk_reports';
        $dbuser = 'ttsviaje_office';
        $dbpass = '6p@fB4$!MaQG';                                                    
		break;
}

// This is the configuration for yiic console application.
// Any writable CConsoleApplication properties can be configured here.
return array(
    'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
    'name'=>'TTS Office Console Application',

    // preloading 'log' component
    'preload'=>array('log'),
        'import'=>array(
                'application.components.*',
                'application.models.*',
				'application.controllers.*',
        ),

    // application components
    'components'=>array(
            
		'db'=>require(dirname(__FILE__).'/database.php'),
		'db_asterisk' => array(
		'class'=>'CDbConnection',
		'connectionString' => 'mysql:host=10.85.90.253;dbname=acd',
		'username' => 'tts_desarrollo',
		'password' => '',
		'charset' => 'utf8',
		),	
		'db_asterisk2' => array(
		'class'=>'CDbConnection',
		'connectionString' => 'mysql:host=10.85.253.247;dbname=acd',
		'username' => 'tts_desarrollo',
		'password' => '',
		'charset' => 'utf8',
		),

		'db_asterisk_reports' => array(
            'class'=>'CDbConnection',
            'connectionString' => "mysql:host=$asterixHost;dbname=$dbname",
            'username' => $dbuser,
            'password' => $dbpass,
            'charset' => 'utf8',
        ),      	
        
        'log'=>array(
            'class'=>'CLogRouter',
            'routes'=>array(
                array(
                    'class'=>'CFileLogRoute',
                                        'logFile'=>'cron.log',
                    'levels'=>'error, warning',
                ),
                                array(
                                        'class'=>'CFileLogRoute',
                                        'logFile'=>'cron_trace.log',
                                        'levels'=>'trace',
                                ),
            ),
                        
        ),
    ),
  	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>require(dirname(__FILE__).'/params.php'),  
);