<?php

return array(
  'urlFormat'=>'path',
  'showScriptName'=>false,
  'caseSensitive'=>false,
  'rules'=>array(
    'empresas'=>'business',
    'asterisk/dashboard'=>'asterisk/dashboard',
    'asterisk/contadores'=>'asterisk/contadores',
    'financiacion'=>'entitiesbines',
    'regalos'=>'gifts',
    'ofertaproducto'=>'offersproduct',
    'ofertaproductoadmin'=>'offersproductadmin',
    'excepcionesnegocio'=>'businessexceptions',
    'navieras'=>'cruisessupplier',
    'regalos/create'=>'gifts/create',
    'navieras/create'=>'cruisessupplier/create',
    'booking'=>'booking',
    'bookingrewards'=>'bookingrewards',
    'facetsextendedfilters'=>'facetsextendedfilters',
    'emailserver'=>'emailserver',
    'productosdestacados'=>'highlightproducts',
    'excluirdestinos'=>'excluderules',
    'costofinanciero'=>'financialcost',
    'filtersubchannel/<id>'=>'subchannels/filtersubchannel/id/<id>',
    'filterentities/<id_subchannel>'=>'entity/filterentities/id_subchannel/<id_subchannel>',
    'configimages/<id_menu>'=>'configimages/admin/id_menu/<id_menu>',
    'configimages/uploadone/<id_menu>'=>'configimages/uploadone/id_menu/<id_menu>',
    'configimages/create/<id_menu>'=>'configimages/create/id_menu/<id_menu>',
    'configimages/delete/<id>/<id_menu>'=>'configimages/delete/id/<id>/id_menu/<id_menu>',
    'configimages/update/<id>/<id_menu>'=>'configimages/update/id/<id>/id_menu/<id_menu>',
    'configimages/view/<id>/<id_menu>'=>'configimages/view/id/<id>/id_menu/<id_menu>',
    'publicationsroutesall'=>'publicationsroutesall',
    'rutas-publicacion'=>'publicationroutes',
    'publicaciones'=>'publications',
    'perfiles'=>'profile',
    'configcron'=>'configcron',
    'configcron/crear'=>'configcron/create',
    'configcron/borrar/<id>'=>'configcron/delete/id/<id>',
    'configcron/modificar/<id>'=>'configcron/update/id/<id>',
    'usuarios/roles/<id>'=>'users/roles/id/<id>',
    'perfil/borrar/<id>'=>'profile/delete/id/<id>',
    'perfil/modificar/<id>'=>'profile/update/id/<id>',
    'perfil/crear'=>'profile/create',
    'usuarios'=>'users',
    'usuarios/borrar/<id>'=>'users/delete/id/<id>',
    'usuarios/modificar/<id>'=>'users/update/id/<id>',
    'usuarios/create'=>'users/create',
    'estadisticas'=>'dashboard',
    'menues'=>'profilemenu',
    'menues/borrar/<id>'=>'profilemenu/delete/id/<id>',
    'menues/modificar/<id>'=>'profilemenu/update/id/<id>',
    'menues/crear'=>'profilemenu/create',
    'roles'=>'profilerole',
    'roles/borrar/<id>'=>'profilerole/delete/id/<id>',
    'roles/modificar/<id>'=>'profilerole/update/id/<id>',
    'roles/crear'=>'profilerole/create',
    'widgets'=>'profilewidgets',
    'widget/borrar/<id>'=>'profilewidgets/delete/id/<id>',
    'widget/modificar/<id>'=>'profilewidgets/update/id/<id>',
    'widget/crear'=>'profilewidgets/create',
    'estadisticas-email'=>'emailcampaignstats',
    'estadisticas-email/actualizar'=>'emailcampaignstats/getallstats',
    'estadisticas-email/actualizar/<id>'=>'emailcampaignstats/getstats/id/<id>',
    'estadisticas-email/ver/<id>'=>'emailcampaignstats/view/id/<id>',
    'estadisticas-email/get-link-details/<id_stat>/<id>'=>'emailcampaignstats/getlinkdetails/stat/<id_stat>/id/<id>',
    'iframe/estadisticas-email/<username>'=>'emailcampaignstatsiframe/adminiframe/username/<username>',
    'iframe/estadisticas-email/<username>/ver/<id>'=>'emailcampaignstatsiframe/viewiframe/username/<username>/id/<id>',
	'iframe/estadisticas-email/get-link-details/<id_stat>/<id>'=>'emailcampaignstatsiframe/getlinkdetails/stat/<id_stat>/id/<id>',
    'widgets'=>'profilewidgets',
    'asistente/borrar/<id>'=>'emailassistant/delete/id/<id>',
    'asistente/modificar/<id>'=>'emailassistant/update/id/<id>',
    'asistentes'=>'emailassistant',
    'asistentes/crear'=>'emailassistant/create',
    'asistentes/borrar/<id>'=>'emailassistant/delete/id/<id>',
    'asistentes/modificar/<id>'=>'emailassistant/update/id/<id>',
    'iframe/asistentes/<username>'=>'emailassistantiframe/adminiframe/username/<username>',
    'iframe/asistente/<username>/crear'=>'emailassistantiframe/createiframe/username/<username>',
    'iframe/asistente/<username>/borrar/<id>'=>'emailassistantiframe/deleteiframe/username/<username>/id/<id>',
    'campaña/createlist'=>'emailcampaign/createList',
    'campaña/sendreal'=>'emailcampaign/sendreal',
    'campaña/send/<id>'=>'emailcampaign/send/id/<id>',
    'campaña/preview/<id>'=>'emailcampaign/preview/id/<id>',
    'campaña/view'=>'emailcampaign/view',
    'campaña/ver-cuerpo/<id>'=>'emailcampaign/vercuerpo/id/<id>',
    'campaña/ver/<id>'=>'emailcampaign/viewform/id/<id>',
    'campañas'=>'emailcampaign',
    'campaña/crear'=>'emailcampaign/create',
    'campaña/borrar/<id>'=>'emailcampaign/delete/id/<id>',
    'campaña/modificar/<id>'=>'emailcampaign/update/id/<id>',
    'mailfooter'=>'emailfooter',
    'mailfooter/crear'=>'emailfooter/create',
    'mailfooter/borrar/<id>'=>'emailfooter/delete/id/<id>',
    'mailfooter/modificar/<id>'=>'emailfooter/update/id/<id>',
    'mailheader/crear'=>'emailheader/create',
    'mailheader/borrar/<id>'=>'emailheader/delete/id/<id>',
    'mailheader/modificar/<id>'=>'emailheader/update/id/<id>',
    'mailheader'=>'emailheader',
    'contactos'=>'emailsuscribers',
    'contactos/crear'=>'emailsuscribers/create',
    'contactos/modificar/<id>'=>'emailsuscribers/update/id/<id>',
    'contactos/borrar/<id>'=>'emailsuscribers/delete/id/<id>',
    'contactos/suscribir/<id>'=>'emailsuscribers/subscribe/id/<id>',
    'contactos/desuscribir/<id>'=>'emailsuscribers/unsubscribe/id/<id>',
    'contactos/desuscribirmail/<email>'=>'emailsuscribers/unsubscribemail/email/<email>',
    'contactos/compartir/<id>'=>'emailsuscribers/share/id/<id>',
    'contactos/ocultar/<id>'=>'emailsuscribers/unshare/id/<id>',
    'iframe/contactos/<username>'=>'emailsuscribersiframe/adminiframe/username/<username>',
    'iframe/contactos/suscribir/<id>/<username>'=>'emailsuscribersiframe/subscribeiframe/id/<id>/username/<username>',
    'iframe/contactos/desuscribir/<id>/<username>'=>'emailsuscribersiframe/unsubscribeiframe/id/<id>/username/<username>',
    'iframe/contactos/compartir/<id>/<username>'=>'emailsuscribersiframe/shareiframe/id/<id>/username/<username>',
    'iframe/contactos/ocultar/<id>/<username>'=>'emailsuscribersiframe/unshareiframe/id/<id>/username/<username>',
	'dominiosinvalidos'=>'emaildomainsbanned',
	'dominiosinvalidos/crear'=>'emaildomainsbanned/create',
	'dominiosinvalidos/modificar/<id>'=>'emaildomainsbanned/update/id/<id>',
	'dominiosinvalidos/borrar/<id>'=>'emaildomainsbanned/delete/id/<id>',
    'apikey'=>'apikey',
    'apikey/crear'=>'apikey/create',
    'apikey/modificar/<id>'=>'apikey/update/id/<id>',
    'apikey/borrar/<id>'=>'apikey/delete/id/<id>',
    'entidades'=>'entity',
    'entidades/create'=>'entity/create',
    'entidades/modificar/<id>'=>'entity/update/id/<id>',
    'entidades/borrar/<id>'=>'entity/delete/id/<id>',
    'tarjetascredito'=>'creditcards',
    'tarjetascredito'=>'creditcards',
    'tarjetascredito/crear'=>'creditcards/create',
    'tarjetascredito/modificar/<id>'=>'creditcards/update/id/<id>',
    'tarjetascredito/borrar/<id>'=>'creditcards/delete/id/<id>',
    'proveedores'=>'providers',
    'proveedores/crear'=>'providers/create',
    'proveedores/modificar/<id>'=>'providers/update/id/<id>',
    'proveedores/borrar/<id>'=>'providers/delete/id/<id>',
    'markupconcepto'=>'marckupconcept',
    'markupconcepto'=>'marckupconcept',
    'markupconcepto/crear'=>'marckupconcept/create',
    'markupconcepto/modificar/<id>'=>'marckupconcept/update/id/<id>',
    'markupconcepto/borrar/<id>'=>'marckupconcept/delete/id/<id>',
    'markupconfig'=>'marckupconfig',
    'markupconfig/crear'=>'marckupconfig/create',
    'markupconfig/modificar/<id>'=>'marckupconfig/update/id/<id>',
    'markupconfig/borrar/<id>'=>'marckupconfig/delete/id/<id>',
    'legales'=>'legal',
    'legales/crear'=>'legal/create',
    'legales/modificar/<id>'=>'legal/update/id/<id>',
    'legales/borrar/<id>'=>'legal/delete/id/<id>',
    'productos'=>'products',
    'productos/crear'=>'products/create',
    'productos/modificar/<id>'=>'products/update/id/<id>',
    'productos/borrar/<id>'=>'products/delete/id/<id>',
    'buscadores'=>'search',
    'buscadores/create'=>'search/create',
    'buscadores/modificar/<id>'=>'search/update/id/<id>',
    'buscadores/borrar/<id>'=>'search/delete/id/<id>',
    'buscadoresadmin'=>'searchadmin',
    'buscadoresadmin/modificar/<id>'=>'searchadmin/update/id/<id>',
    'buscadoresadmin/borrar/<id>'=>'searchadmin/delete/id/<id>',	
    'mediospago'=>'paymentmethod',
    'mediospago/crear'=>'paymentmethod/create',
    'mediospago/modificar/<id>'=>'paymentmethod/update/id/<id>',
    'mediospago/borrar/<id>'=>'paymentmethod/delete/id/<id>',
    'planfinanciacion'=>'transactioncost',
    'planfinanciacion/crear'=>'transactioncost/create',
    'planfinanciacion/modificar/<id>'=>'transactioncost/update/id/<id>',
    'planfinanciacion/borrar/<id>'=>'transactioncost/delete/id/<id>',
    'basecontactos'=>'contactlist',
    'basecontactos/crear'=>'contactList/create',
    'basecontactos/modificar'=>'contactList/modify',
    'etiquetas'=>'tags/index',
    'etiquetas/crear'=>'tags/create',
    'etiquetas/modificar'=>'tags/modify',
    'canales'=>'channels',
    'canales/crear'=>'channels/create',
    'canales/modificar/<id>'=>'channels/update/id/<id>',
    'canales/borrar/<id>'=>'channels/delete/id/<id>',
    'subcanalesadmin'=>'subchannelsadmin',
    'subcanalesadmin/crear'=>'subchannelsadmin/create',
    'subcanalesadmin/modificar/<id>'=>'subchannelsadmin/update/id/<id>',
    'subcanalesadmin/borrar/<id>'=>'subchannelsadmin/delete/id/<id>',	
    'subcanales'=>'subchannels',
    'subcanales/create'=>'subchannels/create',
    'subcanales/modificar/<id>'=>'subchannels/update/id/<id>',
    'subcanales/borrar/<id>'=>'subchannels/delete/id/<id>',
    'usuariosapi'=>'apiuser',
    'usuariosapi/crear'=>'apiuser/create',
    'usuariosapi/modificar/<id>'=>'apiuser/update/id/<id>',
    'usuariosapi/borrar/<id>'=>'apiuser/delete/id/<id>',
    'tiposervicios'=>'servicestype',
    'tiposervicios/crear'=>'servicestype/create',
    'tiposervicios/modificar/<id>'=>'servicestype/update/id/<id>',
    'tiposervicios/borrar/<id>'=>'servicestype/delete/id/<id>',
    'reglasservicios'=>'rulesservices',
    'reglasservicios/crear'=>'rulesservices/create',
    'reglasservicios/modificar/<id>'=>'rulesservices/update/id/<id>',
    'reglasservicios/borrar/<id>'=>'rulesservices/delete/id/<id>',
    'masterapi'=>'masterapi',
    'plataformaproveedor'=>'platformprovider',
    'plataformaproveedor/crear'=>'platformprovider/create',
    'plataformaproveedor/modificar/<id>'=>'platformprovider/update/id/<id>',
    'plataformaproveedor/borrar/<id>'=>'platformprovider/delete/id/<id>',
    'sucursales'=>'offices',
    'sucursales/create'=>'offices/create',
    'sucursales/modificar/<id>'=>'offices/update/id/<id>',
    'sucursales/borrar/<id>'=>'offices/delete/id/<id>',
    'tipomoneda'=>'typecurrency',
    'ofertavuelos'=>'flightdeal',
	'ofertavuelos/create'=>'flightdeal/create', 
    'aerolineas'=>'airliners',
    'aerolineas/create'=>'airliners/create',
    'alianzas'=>'partnerships',
    'alianzas/create'=>'partnerships/create',
    'redessociales'=>'socialnetwork',
    '<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
    'login'=>'login/admin',
    'salir'=>'login/logout',
    'basecontactos'=>'contactlist/index',
	'basecontactos/crear'=>'contactList/create',
	'basecontactos/modificar'=>'contactList/modify',
	'etiquetas'=>'tags/index',
	'etiquetas/crear'=>'tags/create',
	'etiquetas/modificar'=>'tags/modify',
	'calculadora'=>'calculadora',
    'calculadora/imprimir'=>'calculadora/imprimir',
	'iframe/calculadora/<username>'=>'calculadoraiframe/adminiframe/username/<username>',
	'iframe/calculadora/imprimir'=>'calculadoraiframe/ImprimirIframe',
	
    '<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
  )
)

 ?>
