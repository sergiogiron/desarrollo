<?php

class LoginController extends Controller
{
	public $layout='login';

	public function actionAdmin(){
		if(Yii::app()->getRequest()->getParam('id_sabre')!='' && Yii::app()->getRequest()->getParam('pcc')!='' ){
			$criteria = new CDbCriteria;
			$id_sabre=Yii::app()->getRequest()->getParam('id_sabre');
			$pcc=Yii::app()->getRequest()->getParam('pcc');
			$criteria->addCondition("pcc='$pcc' and id_sabre='$id_sabre'");
			$user=Usersabre::model()->find($criteria);	
			$model=new LoginForm;
			$model->username=$user->user->username;
			$model->password=$user->user->password;
			if($model->login('sabre')){
				$this->redirect(Yii::app()->user->returnUrl);
			}else{
				echo 'ERROR DE DATOS';
			}		
		}else{
			if (!defined('CRYPT_BLOWFISH')||!CRYPT_BLOWFISH)
				throw new CHttpException(500,"This application requires that PHP was compiled with Blowfish support for crypt().");

			$model=new LoginForm;

			// if it is ajax validation request
			if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
			{
				echo CActiveForm::validate($model);
				Yii::app()->end();
			}

			// collect user input data
			if(isset($_POST['LoginForm']))
			{
				$model->attributes=$_POST['LoginForm'];
				// validate user input and redirect to the previous page if valid
				if($model->validate() && $model->login()){
					$this->redirect(Yii::app()->user->returnUrl);
				}
					
			}
			// display the login form
			$this->render('index',array('model'=>$model));	
		}
	}

	public function actionLogout() {
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}
}
