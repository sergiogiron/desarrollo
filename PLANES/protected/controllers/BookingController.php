<?php

class BookingController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='pagina';
	public static $origin = ['historialreservas','historial','checkoutid'];

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		$internal=array('mailcliente','mailoperacion','cancel','confirm','view','search','comment','close','LiberateBlockedTask');            //mergea los permisos del usuario para las acciones
		return array(
			
			array('deny', //no entra a ninguna accion ningun usuario que no este logueado               
                'users'=>array('?'),
            ),
            array('allow', // acceden todos los usuarios autenticados a todas las acciones
				//'actions'=>Yii::app()->user->getRules(),
				'actions'=>array_merge(Yii::app()->user->getRules(),$internal),
				'users'=>array('@'),
            ),
			 array('deny',
                'users'=>array('*'),
            ),		
        );	
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function getDetalles($paquete_array){
		$value = $paquete_array;

		$array_body = array();
		$array_hoteles 	= array();
		$array_vuelos 	= array();
		$array_escalas 	= array();
		$array_transfer = array();
		$imagenes_hotel = array();
		$array_ac		= array();
		$array_hab		= array();
		
		$otc 			= $value->OTC;
		$booking_code 	= $value->BookingCode;
		$checkout_id 	= $value->Chkid;
		$nombre 		= $value->Name;
		$id_proveedor 	= $value->IdProvider;
		/*$desde 			= $value->DateFrom;
		$hasta 			= $value->DateTo;*/
		$desde 		= explode('T', $value->DateFrom);
		$hasta 		= explode('T', $value->DateTo);

		// echo "<pre>";
		// print_r($value);
		// echo "</pre>";die;
		foreach ($value->Rooms as $key => $rooms) {
			$cantidad_adultos = $rooms->AdultsCount;
			$cantidad_menores = $rooms->ChildrenAges;		
			$array_temp = array(
					"cantidad_adultos" => $cantidad_adultos,
					"cantidad_menores" => $cantidad_menores
				);	
			$array_hab[] = $array_temp;
			//echo "llego al foreach";
		}	//	die;
		$formas_pago=array();
		/*for($i=0;$i<count($value->PaymentMethod);$i++){
			if($value->PaymentMethod[$i]->Nombre!='Payment_Method'){
				$formas_pago[$value->PaymentMethod[$i]->Cuotas][$value->PaymentMethod[$i]->Nombre][]=array('indice'=>$i,'metodos'=>$value->PaymentMethod[$i],'precios'=>$value->Pricing[$i]);				
			}
		}*/
		
		// echo "<pre>";
		// print_r($value->Products);
		// echo "</pre>";die;
		foreach ($value->Products as $key => $val) {

			if($val->Type=='HOTEL'){
				if(count($val->Images)>0){
					foreach ($val->Images as $key => $img_hot) {
						$tmp_img = array('url'=>$img_hot->url);
						$imagenes_hotel[]= $tmp_img;
					}
				}
				$date_tmp_in 	= explode('T', $val->Checkin);
				$date_tmp_out 	= explode('T', $val->Checkout);
				
				$array_tmp_hot = array(
						'codigo' 			=> $val->Code,
						'nombre' 			=> $val->Name,
						'noches' 			=> $val->Nights,
						'hotel_clase' 		=> $val->HotelClass,
						'informacion'		=> $val->Information,
						'cat_habitacion'	=> $val->FareDescriptions->RoomCategory,
						'checkin'			=> $date_tmp_in[0],
						'checkout'			=> $date_tmp_out[0],
						'tipo_habitacion'	=> $val->FareDescriptions->RoomType,
						'imagenes' 			=> $imagenes_hotel
				 );
				$array_hoteles[] = $array_tmp_hot;
			}elseif($val->Type == 'VUE'){

				foreach ($val->Trips as $key => $vuel) {
					//print_r($vuel);die;
					$array_escalas = array();
					foreach ($vuel->Segments as $key => $esca) {
						$array_tmp_esc = array(
							"nro_vuelo" 	=> $esca->FlightNumber, 
							"clase_vuelo" 	=> $esca->FlightClass, 
							"fecha_salida" 	=> $esca->Departure,
							"aeropuerto_salida" => $esca->DepartureAirport->Description."(".$esca->DepartureAirport->Iata.")",
							"ciudad_salida" => $esca->DepartureCity->Description."(".$esca->DepartureCity->Iata.")",
							"pais_salida" => $esca->DepartureCountry->Description."(".$esca->DepartureCountry->Iata.")",
							"fecha_llegada" =>$esca->Arrival,
							"aeropuerto_llegada" => $esca->ArrivalAirport->Description."(".$esca->ArrivalAirport->Iata.")",
							"ciudad_llegada" => $esca->ArrivalCity->Description."(".$esca->ArrivalCity->Iata.")",
							"pais_llegada" => $esca->ArrivalCountry->Description."(".$esca->ArrivalCountry->Iata.")",
							"aerolinea_cod" => $esca->Supplier->Code,
							"aerolinea_nombre" => $esca->Supplier->Name,
							"aerolinea_imagen" => $esca->Supplier->Image,
						);
						$array_escalas[] = $array_tmp_esc;
					}

					$array_tmp = array(
						"escalas" =>$vuel->Stops,
						"fecha_salida" 	=> $vuel->Departure,
						"aeropuerto_salida" => $vuel->DepartureAirport->Description."(".$vuel->DepartureAirport->Iata.")",
						"ciudad_salida" => $vuel->DepartureCity->Description."(".$vuel->DepartureCity->Iata.")",
						"pais_salida" => $vuel->DepartureCountry->Description."(".$vuel->DepartureCountry->Iata.")",
						"fecha_llegada" => $vuel->Arrival,
						"aeropuerto_llegada" => $vuel->ArrivalAirport->Description."(".$vuel->ArrivalAirport->Iata.")",
						"ciudad_llegada" => $vuel->ArrivalCity->Description."(".$vuel->ArrivalCity->Iata.")",
						"pais_llegada" => $vuel->ArrivalCountry->Description."(".$vuel->ArrivalCountry->Iata.")",
						"aerolinea_cod" => $vuel->Segments[0]->Supplier->Code,
						"aerolinea_nombre" => $vuel->Segments[0]->Supplier->Name,
						"aerolinea_imagen" => $vuel->Segments[0]->Supplier->Image,
						"escalas_detalles" => $array_escalas,

					);
					
					$array_vuelos[] = $array_tmp;

					//if($vuel->Stops){
					
					//}

				}
			}elseif($val->Type == 'TRANSFER'){
				$array_tmp_transf = array(
					"nombre"=>$val->Name
				);
				$array_transfer[] = $array_tmp_transf;
			}elseif($val->Type == 'ASSIST CARD'){
				$array_tmp_ac = array(
					"nombre"=>$val->Name
				);
				$array_ac[] = $array_tmp_ac;
			}
			
		}



		$pricing_detalle = array(
			'descuento' => $value->Detail->Discount,
			'precio_por_adulto' => $value->Detail->AdultsTotalFare,
			'precio_por_menor' => $value->Detail->ChildrenTotalFare,
			'precio_por_infante' => $value->Detail->InfantTotalFare,
		);

		$cantidades = array(
			'cantidad_pasajeros' 	=> $value->Detail->TotalPaxCount,
			'cantidad_adultos' 		=> $value->Detail->TotalAdultsCount,
			'cantidad_menores' 		=> $value->Detail->TotalChildrenCount,
			'cantidad_infantes' 	=> $value->Detail->TotalInfantsCount
		);

		$array_temp = array(
			"OTC"=>$otc,
			"id_proveedor"=>$id_proveedor,
			"booking_code"=>$booking_code,
			"checkout_id"=>$checkout_id,
			"nombre"=>$nombre,
			"imagen"=>$value->Images[0]->Url,
			"desde"=>$desde[0],
			"hasta"=>$hasta[0],
			"total"=>$value->Detail->TotalFare,
			"hoteles" => $array_hoteles,
			"vuelos" => $array_vuelos,
			"transfer"=>$array_transfer,
			"assist_card"=>$array_ac,
			"habitaciones"=>$array_hab,
			"formas_pago"=>$formas_pago,
			"cantidad_noches"=>$value->Detail->Nights,
			"aereo"=>$value->Detail->Flights,
			"detalle_aereo"=>$value->Detail->FlightsDetails,
			"hotel"=>$value->Detail->Hotel,
			"traslados"=>$value->Detail->Transfers,
			"asistencia_viajero"=>$value->Detail->TravelInsurance,
			"descuento"=>$value->Detail->Discount,
			"importe_pasajero"=>$value->Detail->PricePerPax,
			"impuestos_tasas_cargos"=>$value->Detail->TotalTaxes,
			"maximo_cuotas"=>$value->Detail->MaximoCantCuotasSinInteres,
			"cantidades" => $cantidades,
			"pricing_detalle" => $pricing_detalle
		);
		return $array_temp;
	}

	

	public function actionClose($id)
	{	
		Utils::esBlock($id,self::$origin,true);
	}

	public function actionComment($id)
	{	
		$user = Users::model()->findByPk(Yii::app()->user->id);
		$body = array(
				'booking_code'=>$id,
				'type'=>'Comentario',
				'comment'=>$_POST['comment'],
				'date'=>date('d/m/Y H:i:s'),
				'user'=>$user->name." ".$user->lastname);
		$result = Utils::esInteract($id,'PUT',self::$origin,$body);
		if($result){
			echo json_encode($body);
		}
		//self::esInteract($id,'PUT',$body);
		//$historial = self::esInteract($id,'GET');
	}

	public function actionLiberateBlockedTask($id){
		if(Yii::app()->user->checkAccess('freeblockedtask')){
			$editable = Utils::esBlock($id,self::$origin,true);
		}
	}

	public function actionView($id)
	{	
		$editable = Utils::esBlock($id,self::$origin);		
		if(!$editable){
			$user = Users::model()->findByPk(Yii::app()->user->id);
			$body = array(
					'booking_code'=>$id,
					'type'=>'Visto',
					'comment'=>'',
					'date'=>date('d/m/Y H:i:s'),
					'user'=>$user->name." ".$user->lastname);
			Utils::esInteract($id,'PUT',self::$origin,$body);
		}
		$historial = Utils::esInteract($id,'GET',self::$origin);

		/*$tipos = ['comment','view','confirm','cancel'];
		$comments = ['Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam sapiente, quam quibusdam, ipsum explicabo itaque a porro. Ex accusantium dolor quo animi ratione, error voluptatibus totam eius, possimus natus in.',''];
		$historial = array();

		$user = Users::model()->findByPk(Yii::app()->user->id);

		for ($i=0; $i < 10 ; $i++) { 
			$tipo = array_rand($tipos);
			$historial_tmp['booking_code'] = 10000600 + $i;
			$historial_tmp['type'] = $tipos[$tipo];
			$historial_tmp['comment'] = ($tipo==0)?$comments[0]:$comments[1];
			$historial_tmp['date'] = date('d/m/Y H:i');
			$historial_tmp['user'] = $user->name." ".$user->lastname;
			array_push($historial, $historial_tmp);
		}

		echo json_encode($historial)."<br>"; 
		die;*/



		$opts = array('http' =>
			array(
				'method'  => 'GET',
				'ignore_errors' => true,
				'header'=>"Channel: Site\r\n" .
						  "SubChannel: TTS\r\n" .
						  "Consumer: Api\r\n"
				,
			)
		);
		set_time_limit(1000000);
		$context  = stream_context_create($opts);
		
		$url=Yii::app()->params['api_reservas_url'].'/Api/SolicitudPaquete/ByCheckoutId/'.$id;

		$json = json_decode(file_get_contents($url, false,$context),1);
		$json_r = $json['data']['JsonReserva'];
		$NroReserva = $json['data']['NroReserva'];
		$obj = CJSON::decode($json_r,false);
	
		$array_paquete = $obj->Package;

		$paquete 		= self::getDetalles($array_paquete);
		$providerModel 	= Providers::model()->findByPk($paquete['id_proveedor']);
		


		$url  = Yii::app()->params['api_collects_url'].'/Api/SolicitudPaquete/GetCollections/'.$id;
		$jsonTP = json_decode(file_get_contents($url, false,$context),1);
		
		$trys_payments = array();

		if(isset($jsonTP['status']) && $jsonTP['status'] == 'Ok'){
			$trys_payments = $jsonTP['data'];
		}

		$this->renderPartial('view',array(
			'paquete' 	=>$paquete,
			'pasajeros' => $obj->Passengers,
			'contacto' 	=> $obj->Contact,
			'formapago' => $obj->PaymentMethod,
			'total_to_pay' 	=> $obj->Package->TotalToPay,
			'checkout_id' 	=> $NroReserva,
			'estado' 		=> self::getEstado($json['data']['EstadoSolicitud']),
			'id_provider' 	=> $paquete['id_proveedor'],
			'proveedor'		=>$providerModel,
			'historial'		=>$historial,
			'id'			=>$id,
			'trys_payments' =>$trys_payments,
			'editable'		=>$editable
		));
	}

	public function actionCancel($id,$proveedor,$checkout_id){
		//$model = $this->loadModel($id);

		$user = Users::model()->findByPk(Yii::app()->user->id);
		$body = array(
				'booking_code'=>$checkout_id,
				'type'=>'Cancelado',
				'comment'=>'',
				'date'=>date('d/m/Y H:i:s'),
				'user'=>$user->name." ".$user->lastname);
		$result = Utils::esInteract($checkout_id,'PUT',self::$origin,$body);

		$opts = array('http' =>
			array(
				'method'  => 'GET',
				'ignore_errors' => true,
				'header'=>"Channel: Site\r\n" .
						  "SubChannel: TTS\r\n" .
						  "Consumer: api\r\n"
				,
			)
		);
		$context  = stream_context_create($opts);	
		$url =  Yii::app()->params['api_url'].'/paquetes/CancelarBooking/'.$id.','.$proveedor;
		$json = file_get_contents($url, false,$context);
		$json = json_decode($json);

		if($json->Body->Status != 1){
			echo "NOK";
		}else{
			echo "OK";
		}
		
		//$json = file_get_contents($url, false,$context);
		
		//die();
	}
	public function actionConfirm($id,$proveedor,$checkout_id){
		//$model = $this->loadModel($id);
		$user = Users::model()->findByPk(Yii::app()->user->id);
		$body = array(
				'booking_code'=>$checkout_id,
				'type'=>'Confirmado',
				'comment'=>'',
				'date'=>date('d/m/Y H:i:s'),
				'user'=>$user->name." ".$user->lastname);
		Utils::esInteract($checkout_id,'PUT',self::$origin,$body);
		$opts = array('http' =>
			array(
				'method'  => 'GET',
				'ignore_errors' => true,
				'header'=>"Channel: Site\r\n" .
						  "SubChannel: TTS\r\n" .
						  "Consumer: api\r\n"
				,
			)
		);
		$context  = stream_context_create($opts);	
		$url = Yii::app()->params['api_url'].'/paquetes/ConfirmarBooking/'.$id.','.$proveedor;

		$json = file_get_contents($url, false,$context);

		$json = json_decode($json);

		if($json->Body->Status != 1){
			echo "NOK";
		}else{
			echo "OK";
		}
		
		//die();
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Booking;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Booking']))
		{
			$model->attributes=$_POST['Booking'];
			if($model->save()){
				$this->redirect(array('admin'));
			}
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Booking']))
		{
			$model->attributes=$_POST['Booking'];
			if($model->save()){
				$this->redirect(array('admin'));
			}				
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	/*public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax'])){
			$this->redirect(array('admin'));
		}
	}*/

	/**
	 * Manages all models.
	 */
	public function getEstado($estado){
		switch ($estado) {
			case 'Pending':
				$estado = 'Pendiente';
				break;
			case 'Confirmed':
				$estado = 'Confirmado';
				break;
			case 'Canceled':
				$estado = 'Cancelado';
				break;
			case 'CanceledAutomatic':
				$estado = 'Canc. Autom';
				break;
			
			default:
				$estado = '';
				break;
		}
		return $estado;
	}

	public function actionSearch()
	{
		if(isset($_POST)){
			$search = $_POST['buscar'];
			$opts = array('http' =>
			array(
				'method'  => 'GET',
				'ignore_errors' => true,
				'header'=>"Channel: Site\r\n" .
						  "SubChannel: TTS\r\n" .
						  "Consumer: Api\r\n"
						  //"Accept: application/hal+json\r\n"
					,
				)
			);
			set_time_limit(1000000);
			$context  = stream_context_create($opts);
			$url=Yii::app()->params['api_reservas_url'].'/Api/SolicitudPaquete/ByFilter/'.$search;

			//$json = file_get_contents($url, false,$context);
			$json = json_decode(file_get_contents($url, false,$context),1);
			
			$result_search = [];
			foreach ($json['data'] as $key2 => $value2) {
			//hago el select del proveedor
				$proveedor=Providers::model()->findByPk($value2['IdProveedor']);
				$array_tmp['subchannel'] 		= $value2['SubChannel'];
				$array_tmp['nro_reserva'] 		= $value2['NroReserva'];
				$array_tmp['proveedor'] 		= $proveedor->title;
				$array_tmp['id_proveedor'] 		= $value2['IdProveedor'];
				$array_tmp['checkout_id'] 		= $value2['CheckoutId'];
				$array_tmp['cliente'] 			= $value2['ApellidoContacto'].', '.$value2['NombreContacto'];
				$array_tmp['email'] 			= $value2['EmailContacto'];
				$array_tmp['nombre_paquete'] 	= $value2['NombrePaquete'];
				$array_tmp['json'] 				= $value2['JsonReserva'];
				$array_tmp['estado'] 			= self::getEstado($value2['EstadoSolicitud']);
				$array_tmp['fecha'] 			= Utils::datetime_spa_format($value2['FechaAlta']);
				$array_tmp['expiracion']		= Utils::datetime_spa_format($value2['ExpirationDate']);
				//Hago el push en el array correspondiente segun el key del array
				//Dato: para que una variable tome nombre dinamico se pone entre {}
				//en este caso agarro el key (ej: Pending), strtolower, y lo concateno para el array.
				array_push($result_search, $array_tmp);
			}
			$this->renderPartial('_panel',array(
				'id'=>'resultados',
				'data'=>$result_search
			)); 
			
		}
	}
	public function actionAdmin()
	{
		$this->loadJQuery=false;
		$producto = 'Paquetes';
		$query=Yii::app()->db->createCommand("
					SELECT 
					    sch.title subchannel
					FROM search s
			        LEFT JOIN channels ch ON ch.id = s.id_channel
			        LEFT JOIN subchannels sch ON sch.id = s.id_subchannel
			        LEFT JOIN products p ON p.id = s.id_product
			        LEFT JOIN subchannels sch_chldr ON sch.id_subchannel = sch_chldr.id
					WHERE p.title = '$producto'
					AND ch.title = 'Site'
					AND sch.reward != 1 ")->queryAll();
		$subchannelsLst = '';
		
		foreach ($query as $key => $value) {
			$subchannelsLst .= $value['subchannel'].",";
		}
		$subchannelsLst = rtrim($subchannelsLst,',');

		$opts = array('http' =>
			array(
				'method'  => 'GET',
				'ignore_errors' => true,
				'header'=>"Channel: Site\r\n" .
						  "SubChannel: TTS\r\n" .
						  "Consumer: Api\r\n"
						  //"Accept: application/hal+json\r\n"
				,
			)
		);

		set_time_limit(1000000);
		$context  = stream_context_create($opts);
		$url=Yii::app()->params['api_reservas_url'].'/Api/SolicitudPaquete/'.$subchannelsLst.'/1/1000';
		
		$json = json_decode(file_get_contents($url, false,$context),1);	

		//$json = json_decode(file_get_contents('http://demo3004907.mockable.io/getreservas'),1);
		$pending_view = [];
		$confirmed_view = [];
		$canceled_view = [];
		$cancelledautomatic_view = [];
		//recorro el array de datos
		foreach ($json['data'] as $key => $value) {
			//capturo los datos a guardar en los array correspondiente por estado
			foreach ($value as $key2 => $value2) {
				//hago el select del proveedor
				$proveedor=Providers::model()->findByPk($value2['IdProveedor']);
				
				$array_tmp['subchannel'] 		= $value2['SubChannel'];
				$array_tmp['nro_reserva'] 		= $value2['NroReserva'];
				$array_tmp['proveedor'] 		= $proveedor->title;
				$array_tmp['id_proveedor'] 		= $value2['IdProveedor'];
				$array_tmp['checkout_id'] 		= $value2['CheckoutId'];
				$array_tmp['cliente'] 			= $value2['ApellidoContacto'].', '.$value2['NombreContacto'];
				$array_tmp['email'] 			= $value2['EmailContacto'];
				$array_tmp['nombre_paquete'] 	= $value2['NombrePaquete'];
				$array_tmp['json'] 				= $value2['JsonReserva'];
				$array_tmp['expiracion']		= Utils::datetime_spa_format($value2['ExpirationDate']);
				$array_tmp['estado'] 			= self::getEstado($value2['EstadoSolicitud']);
				$array_tmp['fecha'] 			= Utils::datetime_spa_format($value2['FechaAlta']);
				//Hago el push en el array correspondiente segun el key del array
				//Dato: para que una variable tome nombre dinamico se pone entre {}
				//en este caso agarro el key (ej: Pending), strtolower, y lo concateno para el array.
				array_push(${strtolower($key)."_view"}, $array_tmp);
			}
		}
		$have_blocked = Utils::esUserHaveBlockedTask(self::$origin);

		$this->render('admin',array(
			'pendientes'	=>	$pending_view,
			'confirmadas'	=>	$confirmed_view,
			'canceladas'	=>	$canceled_view,
			'caidas'		=>	$cancelledautomatic_view,
			'id_blocked'	=> $have_blocked,				
		));	

		/*$model_pendientes=new Booking('search_pendientes');
		$model_pendientes->unsetAttributes();  // clear any default values
		$model_confirmadas=new Booking('search_confirmadas');
		$model_confirmadas->unsetAttributes();  // clear any default values
		$model_canceladas=new Booking('search_canceladas');
		$model_canceladas->unsetAttributes();  // clear any default values
		$model_caidas=new Booking('search_caidas');
		$model_caidas->unsetAttributes();  // clear any default values
		if(isset($_GET['Booking'])){
			$model_pendientes->attributes=$_GET['Booking'];
			$model_confirmadas->attributes=$_GET['Booking'];
			$model_canceladas->attributes=$_GET['Booking'];
			$model_caidas->attributes=$_GET['Booking'];
		}
		if(isset($_GET['criterio_pendientes'])){
			$model_pendientes->criterio_pendientes=$_GET['criterio_pendientes'];
		}
		if(isset($_GET['criterio_confirmadas'])){
			$model_confirmadas->criterio_confirmadas=$_GET['criterio_confirmadas'];
		}
		if(isset($_GET['criterio_canceladas'])){
			$model_canceladas->criterio_canceladas=$_GET['criterio_canceladas'];
		}
		if(isset($_GET['criterio_caidas'])){
			$model_caidas->criterio_caidas=$_GET['criterio_caidas'];
		}	
		*/	
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Booking the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Booking::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Booking $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='booking-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}


	public function actionMailcliente($id){
		
		$model=Booking::model()->findByPk($id); 
		$url_es=$model->email_client;
		$opts = array('http' =>
			array(
				'header'=>"apikey: 9393a82e4ea2137bb712bfc7ee54ea31\r\n" .
						  "Action: GET\r\n",
			)
		);
		$context  = stream_context_create($opts);	
		$json = file_get_contents($url_es, false,$context);
		$mail = json_decode($json);
		if(isset($mail->_source->Mail)){
			echo $mail->_source->Mail;
		}else{
			echo "No se encuentra el email cliente";
		}
		
	}
	public function actionMailoperacion($id){
		
		$model=Booking::model()->findByPk($id); 
		$url_es=$model->email_operation;
		$opts = array('http' =>
			array(
				'header'=>"apikey: 5c35c2f043552cd53098f81857e5a142\r\n" .
						  "Action: GET\r\n",
			)
		);
		$context  = stream_context_create($opts);	
		$json = file_get_contents($url_es, false,$context);
		$mail = json_decode($json);
		if(isset($mail->_source->Mail)){
			echo $mail->_source->Mail;
		}else{
			echo "No se encuentra el email de operaciones";
		}
	}
}
