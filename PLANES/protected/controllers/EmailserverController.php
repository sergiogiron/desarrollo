<?php

class EmailserverController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='pagina';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('deny', //no entra a ninguna accion ningun usuario que no este logueado               
                'users'=>array('?'),
            ),
            array('allow', // acceden todos los usuarios autenticados a todas las acciones
				'actions'=>Yii::app()->user->getRules(),
				'users'=>array('@'),
            ),
			 array('deny',
                'users'=>array('*'),
            ),		
        );	
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Emailserver;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Emailserver']))
		{
			$model->attributes=$_POST['Emailserver'];
			if($model->save()){
				$this->redirect(array('admin'));
			}
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);		
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Emailserver']))
		{
			$model->attributes=$_POST['Emailserver'];
			if($model->save()){
				$this->redirect(array('admin'));
			}				
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax'])){
			$this->redirect(array('admin'));
		}
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$this->loadJQuery=false;
		$model=new Emailserver('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Emailserver'])){
			$model->attributes=$_GET['Emailserver'];
		}
		if(isset($_GET['criterio'])){
			$model->criterio=$_GET['criterio'];
		}	
		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Emailserver the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Emailserver::model()->findByPk($id);
		$model=Emailserver::normalize_dates($model);	
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Emailserver $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='emailserver-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

	public function actionPublish(){
		$this->layout='pagina_clear';
		
		if(isset($_POST['hour']) && isset($_POST['date']))
		{
			header('Content-Type: application/json');
			$criteria = new CDbCriteria;
			$criteria->addCondition('enabled=1');
			$model=Emailserver::model()->findAll($criteria);
			$content=array();
			$date = date_create_from_format('d/m/Y', $_POST['date']);	
			$publication_date=date_format($date,'Y-m-d').' '.$_POST['hour'].':00';	
			
			$publication_app=Publish::Run(array('app'),'email_server',array(),array(),$_POST['date'],$_POST['hour'],$model,array());			
			
		}else{
			$this->render('publish',array('title'=>Yii::app()->user->Title_nocss(),'breadcrumb'=>Yii::app()->user->Breadcrumb_nocss()));
		}
	}
}
