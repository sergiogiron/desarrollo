<?php

class SearchadminController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='pagina';
	
	public function filters()
	{		
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}
	
	public function accessRules()
	{
		$internal=array('getorden');
		return array(
			array('deny', //no entra a ninguna accion ningun usuario que no este logueado               
                'users'=>array('?'),
            ),
            array('allow', // acceden todos los usuarios autenticados a todas las acciones
				'actions'=>array_merge(Yii::app()->user->getRules(),$internal),
				'users'=>array('@'),
            ),
			 array('deny',
                'users'=>array('*'),
            ),		
        );	
	}

	

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	 
	public function actionPublish(){
		$this->layout='pagina_clear';
		$query=Yii::app()->db->createCommand("SELECT distinct search.id_channel,search.id_subchannel,channels.title as channel_folder,subchannels.title as subchannel_folder FROM search LEFT JOIN subchannels ON search.id_subchannel=subchannels.id  LEFT JOIN channels ON search.id_channel=channels.id where  search.enabled=1")->queryAll();
		$channel=array();
		$subchannel=array();
		foreach($query as $q){
			array_push($channel,array('id'=>$q['id_channel'],'name'=>$q['channel_folder']));
			array_push($subchannel,array('id'=>$q['id_subchannel'],'name'=>$q['subchannel_folder']));
		}
		if(isset($_POST['hour']) && isset($_POST['channel']) && isset($_POST['subchannel']) && isset($_POST['date'])){
			header('Content-Type: application/json');	
			$criteria = new CDbCriteria;
			$id_menu=Yii::app()->user->id_menu();
			$criteria->addCondition("id_menu=18");			
			$images_sizes=Configimages::model()->findAll($criteria);
			$content_app_server=array();
			foreach($_POST['subchannel'] as $subcanal){
				$content=array();	
				$criteria = new CDbCriteria;
				$criteria->addCondition('enabled=1 and id_channel='.$_POST['channel'].' and id_subchannel='.$subcanal);
				$model=Search::model()->findAll($criteria);	
				foreach($model as $search){
					$array_imagenes=array();
					foreach($search->imagenes as $imagenes){	
						$array_imagenes[]=array(
							'title'=>$imagenes->title,
							'link'=>$imagenes->link,
							'filename'=>'http://'.$_SERVER['SERVER_NAME'].'/'.Yii::app()->request->baseUrl.'/uploads/search/'.$imagenes->id_search.'/'.$imagenes->filename.'/'.$imagenes->filename,
							'ext'=>$imagenes->ext
						);
						foreach($images_sizes as $sizes){
							$array_imagenes[]=array(
								'title'=>$imagenes->title,
								'width'=>$sizes->width,
								'height'=>$sizes->height,
								'link'=>$imagenes->link,							
								'filename'=>'http://'.$_SERVER['SERVER_NAME'].'/'.Yii::app()->request->baseUrl.'/uploads/search/'.$imagenes->id_search.'/'.$imagenes->filename.'/'.$imagenes->filename.'_'.$sizes->width.'x'.$sizes->height,
								'ext'=>$imagenes->ext
							);						
						}
					}	
					array_push($content,array(
							'img'=>array(
								array(
									'filename'=>'http://'.$_SERVER['SERVER_NAME'].Yii::app()->request->baseUrl.'/uploads/search/'.$search->id.'/'.$search->filename.'/'.$search->filename,
									'ext'=>$search->ext,
									'title'=>strtolower($search->producto->title)
								),
							),
							'imagenes'=>$array_imagenes,
							'name'=>$search->producto->title,
							'tracking_description'=>$search->tracking_description,
							'tracking_analitycs'=>$search->tracking_analitycs,
							'url_iframe'=>$search->url_iframe,
							'phone'=>$search->phone,
							'phone_description'=>$search->phone_description,
							'office_hours'=>$search->office_hours,				
							'order'=>$search->order,
							'active'=>$search->default_view,
							'script'=>$search->script,
							'anticipation_from_min_days'=>$search->anticipation_from_min_days,
							'anticipation_to_max_days'=>$search->anticipation_to_max_days,
							'max_search_months'=>$search->max_search_months,
							'thankyou_text'=>$search->thankyou_text,
						)
					);
					$content_app_server=Search::model()->findAllByAttributes(array('id_subchannel'=>$subcanal,'enabled'=>1,'id_channel'=>$_POST['channel']), array('select'=>'id,id_channel,id_subchannel,id_product,id_email_server_sales,id_email_server_alerts,filename,ext,sale_user,sale_pass,sale_show_as,url_iframe,sale_to,alerts_user,alerts_pass,alerts_show_as,alerts_subject_patterns,alerts_to,tracking_description,tracking_analitycs,landings_order_price,landings_order_departure,landings_telephone_sales,landings_max_results_per_pages,phone,phone_description,office_hours,default_view,max_search_months,anticipation_to_max_days,anticipation_from_min_days,enabled,created_at,id_user'));
				}
				if(count($model)>0){
					$publication_site=Publish::Run(array('site'),'search',$_POST['channel'],array('id_subchannel'=>$subcanal),$_POST['date'],$_POST['hour'],$content,array("img","imagenes"));
					$criteria = new CDbCriteria;
					$criteria->addCondition('enabled=1 and id_channel='.$_POST['channel'].' and id='.$subcanal);				
					$filters=Searchfacetsextendfilters::model()->findAll($criteria);
					$publication_app=Publish::Run(array('app'),'search',$_POST['channel'],array('id_subchannel'=>$subcanal),$_POST['date'],$_POST['hour'],$content_app_server,'');
					$publication_app=Publish::Run(array('app'),'search_facets_extend_filters',$_POST['channel'],array('id_subchannel'=>$subcanal),$_POST['date'],$_POST['hour'],$filters,'');						
				}				
			}
		}else{
			$this->render('publish',array('title'=>Yii::app()->user->Title_nocss(),'breadcrumb'=>Yii::app()->user->Breadcrumb_nocss(),'channel'=>$channel,'subchannel'=>$subchannel));
		}
	}
	
	public function actionPublicationrecord(){
	
	}
	
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Search;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Search']))
		{
			$model->attributes=$_POST['Search'];
			$model->id_user=Yii::app()->user->id;		
			$model->setIsNewRecord(true);		
			if($model->save()){
				if(isset($_POST['id_filters'])){
					foreach($_POST['id_filters'] as $filters){
						$filter=new Searchfacetsextendfilters;
						$filter->id_search=$model->id;
						$filter->id_products=$model->id_product;
						$filter->id_channel=$model->id_channel;
						$filter->id_subchannel=$model->id_subchannel;							
						$filter->id_facets=$filters;
						$filter->is_facets=0;
						$filter->enabled=1;
						$filter->id_user=Yii::app()->user->id;
						$filter->created_at=date("Y-m-d H:i:s");
						$filter->save();				
					}
				}
				if(isset($_POST['id_facets'])){
					foreach($_POST['id_facets'] as $facets){
						$facet=new Searchfacetsextendfilters;
						$facet->id_search=$model->id;
						$facet->id_channel=$model->id_channel;
						$facet->id_subchannel=$model->id_subchannel;							
						$facet->id_products=$model->id_product;
						$facet->id_facets=$facets;
						$facet->is_facets=1;
						$facet->enabled=1;
						$facet->id_user=Yii::app()->user->id;
						$facet->created_at=date("Y-m-d H:i:s");	
						$facet->save();				
					}					
				}							
				$this->redirect(array('admin'));
			}				
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Search']))
		{
			$model->attributes=$_POST['Search'];
			if($model->save()){
				Searchfacetsextendfilters::model()->deleteAll("id_search=".$model->id);
				if(isset($_POST['id_filters'])){
					foreach($_POST['id_filters'] as $filters){
						$filter=new Searchfacetsextendfilters;
						$filter->id_search=$model->id;
						$filter->id_products=$model->id_product;
						$filter->id_channel=$model->id_channel;
						$filter->id_subchannel=$model->id_subchannel;							
						$filter->id_facets=$filters;
						$filter->is_facets=0;
						$filter->enabled=1;
						$filter->id_user=Yii::app()->user->id;
						$filter->created_at=date("Y-m-d H:i:s");
						$filter->save();				
					}
				}
				if(isset($_POST['id_facets'])){
					foreach($_POST['id_facets'] as $facets){
						$facet=new Searchfacetsextendfilters;
						$facet->id_search=$model->id;
						$facet->id_products=$model->id_product;
						$facet->id_facets=$facets;
						$facet->id_channel=$model->id_channel;
						$facet->id_subchannel=$model->id_subchannel;							
						$facet->is_facets=1;
						$facet->enabled=1;
						$facet->id_user=Yii::app()->user->id;
						$facet->created_at=date("Y-m-d H:i:s");	
						$facet->save();				
					}					
				}
				$this->redirect(array('admin'));				
			}
			
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	public function actionGetorden($id_channel,$id_subchannel,$type,$default=null){
		$sql = "SELECT 1 FROM search WHERE id_channel=:id_channel AND id_subchannel = :id_subchannel";

		$parameters = array(":id_channel"=>$id_channel, ":id_subchannel"=>$id_subchannel);

		$result = Yii::app()->db->createCommand($sql)->execute($parameters);
		$result=($type=='create'?$result+1:$result);
		echo $result;
//		$array = array('result' => $result);
		/*$result=($type=='create'?$result+1:$result);
		for ($i=1; $i <= $result ; $i++) {
			if($default){
				$selected = ($i==$default)?'selected':'';
				echo '<option '.$selected.' value="'.$i.'">'.$i.'</option>';
			} else{
				echo '<option value="'.$i.'">'.$i.'</option>';
			}
		}*/



	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Search');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}
	
	public function actionAdmin()
	{
		$this->loadJQuery=false;
		$model=new Search('search');	
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Search'])){
			$model->attributes=$_GET['Search'];
		}

		if(isset($_GET['criterio'])){
			$model->criterio=$_GET['criterio'];
		}	
		$this->render('admin',array(
			'model'=>$model
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Search the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Search::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Search $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='search-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}