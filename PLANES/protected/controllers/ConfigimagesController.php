<?php

class ConfigimagesController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='pagina';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id,$id_menu)
	{
		self::Getrole($id_menu);
		$this->render('view',array('model'=>$this->loadModel($id),'id_menu'=>$id_menu));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate($id_menu)
	{
		self::Getrole($id_menu);
		$model=new Configimages;
		$menu=Profilemenu::model()->findByPk($id_menu);
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Configimages']))
		{
			$model->attributes=$_POST['Configimages'];
			$model->id_user=Yii::app()->user->id;
			$model->id_menu=$id_menu;
			$model->created_at=date("Y-m-d H:i:s");				
			if($model->save()){
				$this->redirect(array('configimages/'.$id_menu));
			}
		}

		$this->render('create',array(
			'model'=>$model,'id_menu'=>$id_menu,'menu'=>$menu
		));
	}
	public function actionUploadedit($id_menu,$menu,$position){
		$this->layout='pagina_clear';
		self::Getrole($id_menu);
		$config_images=Configimages::model()->findAllByAttributes(array('id_menu'=>$id_menu,'position'=>$position));
		$this->render('upload-edit',array('id_menu'=>$id_menu,'config_images'=>$config_images,'position'=>$position,'data'=>$_POST
		));			
	}
	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	
	public function actionUploadone($id_menu){
		if(isset($_FILES["file"])){
			$error =$_FILES["file"]["error"];
			$ret[]=self::resizeone();
			echo json_encode($ret);
		}	
	}
	
	public function actionUpload($id_menu,$position){
		
		if(isset($_POST['0-x'])){
			$imagen = array();
			$this->layout=false;
			$cantidad=(count($_POST)-4)/9;
			if(!is_dir('uploads/tmp/'.Yii::app()->user->id.'/'.$_POST['file_selected'])){
				mkdir('uploads/tmp/'.Yii::app()->user->id.'/'.$_POST['file_selected']);
			}
			$coords=array();
			for($i=0;$i<$cantidad;$i++){
				$thumb_image_location = 'uploads/tmp/'.Yii::app()->user->id.'/'.$_POST['file_selected'].'/'.$_POST['file_selected'].'.'.$_POST['ext']; //Ponerla desde JavaScript por parametros 
				$targ_w = $_POST[$i.'-w'];
				$targ_h = $_POST[$i.'-h'];
				$image_size_original =getimagesize('uploads/tmp/'.Yii::app()->user->id.'/'.$_POST['file_selected'].'/'.$_POST['file_selected'].'.'.$_POST['ext']);
				$width_original=$image_size_original[0];
				$height_original=$image_size_original[1];
				$dst_r = ImageCreateTrueColor( $_POST[$i.'-width'], $_POST[$i.'-height']);
				$src = 'uploads/tmp/'.Yii::app()->user->id.'/'.$_POST['file_selected'].'/'.$_POST['file_selected'].'_'.$_POST[$i.'-width'].'x'.$_POST[$i.'-height'].'.'.$_POST['ext']; //La ruta donde guardará la imagen.				
				switch ($_POST['ext']) {
					case 'jpg':	
						$img_quality = 90;
						$img_r = imagecreatefromjpeg($thumb_image_location);
						imagecopyresampled($dst_r,$img_r,0,0,$_POST[$i.'-x'],$_POST[$i.'-y'],$_POST[$i.'-width'], $_POST[$i.'-height'],$_POST[$i.'-w'],$_POST[$i.'-h']);
						imagejpeg($dst_r,$src,$img_quality);						
						break;					
					case 'png':
						$img_quality = 9;
						$img = imagecreatefrompng($thumb_image_location);
						imagealphablending($img, true);
						imagesavealpha($dst_r, true);
						imagealphablending($dst_r, false);
						$transparent = imagecolorallocatealpha($dst_r, 0, 0, 0, 127);
						imagefill($dst_r, 0, 0, $transparent);						
						imagecopyresampled($dst_r,$img,0,0,$_POST[$i.'-x'],$_POST[$i.'-y'],$_POST[$i.'-width'], $_POST[$i.'-height'],$_POST[$i.'-w'],$_POST[$i.'-h']);
						imagepng($dst_r,$src,$img_quality);						
						break;
					case 'gif':	
						$img_quality = 9;
						$img = imagecreatefromgif($thumb_image_location);
						$transparent = imagecolorallocatealpha($dst_r, 0, 0, 0, 127);
						imagefill($dst_r, 0, 0, $transparent);
						imagealphablending($dst_r, true); 						
						imagecopyresampled($dst_r,$img,0,0,$_POST[$i.'-x'],$_POST[$i.'-y'],$_POST[$i.'-width'], $_POST[$i.'-height'],$_POST[$i.'-w'],$_POST[$i.'-h']);
						imagegif($dst_r,$src,$img_quality);	
						break;
				}				
				$coords[]=array('w'=>$_POST[$i.'-w'],'h'=>$_POST[$i.'-h'],'x'=>$_POST[$i.'-x'],'y'=>$_POST[$i.'-y'],'x2'=>$_POST[$i.'-x2'],'y2'=>$_POST[$i.'-y2'],'width'=>$_POST[$i.'-width'],'height'=>$_POST[$i.'-height']);
			}
			$imagen['url']=Yii::app()->request->baseUrl.'/uploads/tmp/'.Yii::app()->user->id.'/'.$_POST['file_selected'].'/'.$_POST['file_selected'].'.'.$_POST['ext'];
			$imagen['name']=$_POST['file_selected'];
			$imagen['json']=array('name'=>$_POST['file_selected'],'ext'=>$_POST['ext'],'position'=>$_POST['position'],'title'=>$_POST['title'],'link'=>$_POST['link'],'coords'=>$coords,'order'=>0);
			echo json_encode($imagen);
		}else{	
			$this->layout='pagina_clear';
			self::Getrole($id_menu);
			$criteria=new CDbCriteria;
			$criteria->order='width asc';			
			$config_images=Configimages::model()->findAllByAttributes(array('id_menu'=>$id_menu,'position'=>$position),$criteria);
			$this->render('upload',array('id_menu'=>$id_menu,'position'=>$position,
				'config_images'=>$config_images
			));			
		}
	}
	
	public function actionUploadfile($id_menu,$position){
		if(isset($_FILES["file"])){
			$error =$_FILES["file"]["error"];
			//You need to handle  both cases//If Any browser does not support serializing of multiple files using FormData() 
			$config_images=Configimages::model()->findAllByAttributes(array('id_menu'=>$id_menu,'position'=>$position));
			$ret[]=self::resize($config_images);
			echo json_encode($ret);
		}	
	}

	public function actionUpdate($id,$id_menu)
	{
		self::Getrole($id_menu);
		$model=$this->loadModel($id);	
		$menu=Profilemenu::model()->findByPk($id_menu);
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Configimages']))
		{
			$model->attributes=$_POST['Configimages'];
			if($model->save()){
				$this->redirect(array('configimages/'.$id_menu));
			}				
		}

		$this->render('update',array(
			'model'=>$model,'id_menu'=>$id_menu,'menu'=>$menu
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id,$id_menu)
	{
		self::Getrole($id_menu);
		$this->loadModel($id)->delete();
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin($id_menu)
	{
		self::Getrole($id_menu);
		$this->loadJQuery=false;
		$model=new Configimages('search');
		$menu=Profilemenu::model()->findByPk($id_menu);
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Configimages'])){
			$model->attributes=$_GET['Configimages'];
		}
		if(isset($_GET['criterio'])){
			$model->criterio=$_GET['criterio'];
		}	
		$this->render('admin',array(
			'model'=>$model,'id_menu'=>$id_menu,'menu'=>$menu
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Configimages the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Configimages::model()->findByPk($id);
		$model=Configimages::normalize_dates($model);	
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Configimages $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='configimages-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	
	protected function Getrole($id_menu){
		$result=0;
		$menu_actual=Profilemenu::model()->findByPk($id_menu);		
		foreach(Yii::app()->user->getRoles() as $perfiles){
			foreach($perfiles->profile->menues as $menu){
				if($menu->menu->url==$menu_actual->url){	
					foreach($menu->roles as $roles){
						if($roles->role->action=='configimages'){
							$result=1;
						}
					}	
				}
			}
		}	
		if($result==1){
			
		}else{
			throw new CHttpException(404,'The specified post cannot be found.');
		}		
	}	
	
	public static function resize($config_images){
		$ret = array();
		$image_size=getimagesize($_FILES["file"]["tmp_name"]);
		$filename = explode(".", $_FILES["file"]["name"]); 
		$ext = end($filename); 		
		$error=0;
		foreach($config_images as $sizes){
			if($sizes->width>$image_size[0] || $sizes->height>$image_size[1]){
				$error=1;
			}
		}
		if($error==0){
			$hash=md5(Yii::app()->user->id.date("Y-m-d H:i:s").rand(0,1000));
			$name_img=$hash.'.'.$ext;
			if(!file_exists("uploads/tmp/")){
				mkdir("uploads/tmp/", 0777);
			}		
			if(!is_dir("uploads/tmp/".Yii::app()->user->id."/")){	
				mkdir("uploads/tmp/".Yii::app()->user->id."/", 0777);
			}		
			if(!is_dir('uploads/tmp/'.Yii::app()->user->id.'/'.$hash)){
				mkdir('uploads/tmp/'.Yii::app()->user->id.'/'.$hash);
			}
			$output_dir = 'uploads/tmp/'.Yii::app()->user->id.'/'.$hash.'/';
			$fileName_large=$output_dir.$name_img;
			move_uploaded_file($_FILES["file"]["tmp_name"],$fileName_large);	
			$ret['name']=$hash;
			$ret['url']=$fileName_large;
			$ret['width']=$image_size[0];
			$ret['height']=$image_size[1];
			$ret['ext']=$ext;
			return $ret;			
		}else{
			$ret['error']='error';
			return $ret;	
		}
	}	
	
	public static function resizeone(){
		$ret = array();
		$image_size=getimagesize($_FILES["file"]["tmp_name"]);
		$filename = explode(".", $_FILES["file"]["name"]); 
		$ext = end($filename); 		
		$error=0;
		if($error==0){
			$hash=md5(Yii::app()->user->id.date("Y-m-d H:i:s").rand(0,1000));
			$name_img=$hash.'.'.$ext;
			if(!file_exists("uploads/tmp/")){
				mkdir("uploads/tmp/", 0777);
			}		
			if(!is_dir("uploads/tmp/".Yii::app()->user->id."/")){	
				mkdir("uploads/tmp/".Yii::app()->user->id."/", 0777);
			}		
			if(!is_dir('uploads/tmp/'.Yii::app()->user->id.'/'.$hash)){
				mkdir('uploads/tmp/'.Yii::app()->user->id.'/'.$hash);
			}
			$output_dir = 'uploads/tmp/'.Yii::app()->user->id.'/'.$hash.'/';
			$fileName_large=$output_dir.$name_img;
			move_uploaded_file($_FILES["file"]["tmp_name"],$fileName_large);	
			$ret['name']=$hash;
			$ret['url']=$fileName_large;
			$ret['width']=$image_size[0];
			$ret['height']=$image_size[1];
			$ret['ext']=$ext;
			return $ret;			
		}else{
			$ret['error']='error';
			return $ret;	
		}
	}	
	
}
