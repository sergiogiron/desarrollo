<?php

class AirlinersController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='pagina';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('deny', //no entra a ninguna accion ningun usuario que no este logueado               
                'users'=>array('?'),
            ),
            array('allow', // acceden todos los usuarios autenticados a todas las acciones
				'actions'=>Yii::app()->user->getRules(),
				'users'=>array('@'),
            ),
			 array('deny',
                'users'=>array('*'),
            ),		
        );	
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}


	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Airliners;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Airliners']))
		{
			$model->attributes=$_POST['Airliners'];
			if($model->save()){
 
				if(count($_POST['image'])>0){
					foreach($_POST['image'] as $images){
						$images_json=json_decode($images);
						$airliners_images=new Airlinersimages;
						$airliners_images->id_airliners=$model->id;
						$airliners_images->id_user=Yii::app()->user->id;
						$airliners_images->ext=$images_json->ext;
						$airliners_images->filename=$images_json->name;
						$airliners_images->position=$images_json->position;
						$airliners_images->title=$images_json->title;
						$airliners_images->link=$images_json->link;
						$airliners_images->order=$images_json->order;
						$airliners_images->coords=json_encode($images_json->coords);
						$airliners_images->save();
						Utils::mover_directorio($model->id,$images_json->name);
					}					
					Utils::vaciarcacheimages('uploads/tmp/'.Yii::app()->user->id.'/');
				}				

				$this->redirect(array('admin'));
			}
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);		
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Airliners']))
		{
			$model->attributes=$_POST['Airliners'];
			if($model->save()){

				Utils::vaciarcacheprodimages('uploads/airliners/'.$model->id.'/');
				Airlinersimages::model()->deleteAll("id_airliners=$id");
				if(isset($_POST['image'])){
					
					foreach($_POST['image'] as $images){					
						$images_json=json_decode($images);
						$airliners_images=new Airlinersimages;
						$airliners_images->id_airliners=$model->id;
						$airliners_images->ext=$images_json->ext;
						$airliners_images->filename=$images_json->name;
						$airliners_images->position=$images_json->position;
						$airliners_images->title=$images_json->title;
						$airliners_images->link=$images_json->link;
						$airliners_images->coords=json_encode($images_json->coords);
						$airliners_images->order=$images_json->order;
						$airliners_images->id_user=Yii::app()->user->id;
						$airliners_images->save();
						Utils::mover_directorio($model->id,$images_json->name);
					}					
				}
				Utils::vaciarcacheimages('uploads/tmp/'.Yii::app()->user->id.'/');
				
				$this->redirect(array('admin'));
			}				
		}else{
			foreach($model->imagenes as $imagenes){
				Utils::mover_directorio_prod($model->id,$imagenes->filename);
			}			
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	public function actionPublish(){
		$this->layout='pagina_clear';		
		if(isset($_POST['hour']) && isset($_POST['date']))
		{
			header('Content-Type: application/json');
			$criteria = new CDbCriteria;
			$criteria->addCondition('enabled=1');
			$model=Airliners::model()->findAll($criteria);
			$content=array();
			$criteria = new CDbCriteria;
			$id_menu=Yii::app()->user->id_menu();
			$criteria->addCondition("id_menu=$id_menu");			
			$images_sizes=Configimages::model()->findAll($criteria);	
			
			foreach($model as $air){
				$array_imagenes=array();
				foreach($air->imagenes as $imagenes){					
					$array_imagenes[]=array(
						'title'=>$imagenes->title,
						'filename'=>'http://'.$_SERVER['SERVER_NAME'].'/'.Yii::app()->request->baseUrl.'/uploads/airliners/'.$imagenes->id_airliners.'/'.$imagenes->filename.'/'.$imagenes->filename,
						'ext'=>$imagenes->ext
					);
					foreach($images_sizes as $sizes){
						$array_imagenes[]=array(
							'title'=>$imagenes->title,
							'width'=>$sizes->width,
							'height'=>$sizes->height,							
							'filename'=>'http://'.$_SERVER['SERVER_NAME'].'/'.Yii::app()->request->baseUrl.'/uploads/airliners/'.$imagenes->id_airliners.'/'.$imagenes->filename.'/'.$imagenes->filename.'_'.$sizes->width.'x'.$sizes->height,
							'ext'=>$imagenes->ext
						);						
					}
				}
				array_push($content,array(
					'imagenes'=>$array_imagenes,
					'id'=>$air->id,
					'name'=>$air->title,
					'code'=>$air->code,
					'enabled'=>$air->enabled
					));
			}
			$publication_cdn=Publish::Run(array('cdn'),'airliners',array(),array(),$_POST['date'],$_POST['hour'],$content,array("imagenes"));				
		}else{
			$this->render('publish',array('title'=>Yii::app()->user->Title_nocss(),'breadcrumb'=>Yii::app()->user->Breadcrumb_nocss()));
		}
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax'])){
			$this->redirect(array('admin'));
		}
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$this->loadJQuery=false;
		$model=new Airliners('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Airliners'])){
			$model->attributes=$_GET['Airliners'];
		}
		if(isset($_GET['criterio'])){
			$model->criterio=$_GET['criterio'];
		}	
		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Airliners the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Airliners::model()->findByPk($id);
		$model=Airliners::normalize_dates($model);	
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Airliners $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='airliners-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
