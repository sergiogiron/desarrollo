<?php

class GiftsController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='pagina';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('deny', //no entra a ninguna accion ningun usuario que no este logueado               
                'users'=>array('?'),
            ),
            array('allow', // acceden todos los usuarios autenticados a todas las acciones
				'actions'=>Yii::app()->user->getRules(),
				'users'=>array('@'),
            ),
			 array('deny',
                'users'=>array('*'),
            ),		
        );	
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->loadJQuery=false;
		$model=$this->loadModel($id);
		$gift_list_selected=array();
		foreach($model->giftslist as $giftslist){
			$gift_list_selected[]=array(
				'id'=>$giftslist->id,
				'order'=>$giftslist->order,
				'description'=>$giftslist->description,
				'price'=>$giftslist->price,
			);
		}		
		$this->render('view',array(
			'model'=>$model,'gift_list_selected'=>$gift_list_selected
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$this->loadJQuery=false;
		$model=new Gifts;
		$gift_list_selected=array();
		// Uncomment the following line if AJAX validation is needed
		 $this->performAjaxValidation($model);

		if(isset($_POST['Gifts']))
		{
			$model->attributes=$_POST['Gifts'];
			if($model->save()){
				if(!is_dir('uploads/gifts')){
					mkdir('uploads/gifts');
				}					
				if(!is_dir('uploads/gifts/'.$model->id)){
					mkdir('uploads/gifts/'.$model->id);
				}					
				if(isset($_POST['image'])){
					foreach($_POST['image'] as $images){					
						$images_json=json_decode($images);
						$gifts_images=new Giftsimages;
						$gifts_images->id_gifts=$model->id;
						$gifts_images->ext=$images_json->ext;
						$gifts_images->filename=$images_json->name;
						$gifts_images->position=$images_json->position;
						$gifts_images->title=$images_json->title;
						$gifts_images->link=$images_json->link;
						$gifts_images->coords=json_encode($images_json->coords);
						$gifts_images->order=$images_json->order;
						$gifts_images->save();
						Utils::mover_directorio($model->id,$images_json->name);
					}					
				}				
				if(isset($_POST['gifts_json'])){
					$gifts_json=json_decode($_POST['gifts_json']);
					foreach($gifts_json as $gj){
						$gift_list=new Giftslist;
						$gift_list->id_gift=$model->id;
						$gift_list->description=$gj->description;
						$gift_list->price=$gj->price;
						$gift_list->order=$gj->order;
						$gift_list->id_user=Yii::app()->user->id;
						$gift_list->save();
					}		
				}	
				Utils::vaciarcacheimages('uploads/tmp/'.Yii::app()->user->id.'/');	
				$this->redirect(array('admin'));
			}
		}

		$this->render('create',array(
			'model'=>$model,'gift_list_selected'=>$gift_list_selected
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$this->loadJQuery=false;
		$model=$this->loadModel($id);
		$gift_list_selected=array();
		foreach($model->giftslist as $giftslist){
			$gift_list_selected[]=array(
				'id'=>$giftslist->id,
				'order'=>$giftslist->order,
				'description'=>$giftslist->description,
				'price'=>$giftslist->price,
			);
		}		
		// Uncomment the following line if AJAX validation is needed
		 $this->performAjaxValidation($model);

		if(isset($_POST['Gifts']))
		{
			$model->attributes=$_POST['Gifts'];
			if($model->save()){
				Utils::vaciarcacheprodimages('uploads/gifts/'.$model->id.'/');
				Giftsimages::model()->deleteAll("id_gifts=$id");				
				if(isset($_POST['image'])){
					foreach($_POST['image'] as $images){					
						$images_json=json_decode($images);
						$gifts_images=new Giftsimages;
						$gifts_images->id_gifts=$model->id;
						$gifts_images->ext=$images_json->ext;
						$gifts_images->filename=$images_json->name;
						$gifts_images->position=$images_json->position;
						$gifts_images->title=$images_json->title;
						$gifts_images->link=$images_json->link;
						$gifts_images->coords=json_encode($images_json->coords);
						$gifts_images->order=$images_json->order;
						$gifts_images->save();
						Utils::mover_directorio($model->id,$images_json->name);
					}					
				}						
				Giftslist::model()->deleteAll("id_gift=$id");
				if(isset($_POST['gifts_json'])){
					$gifts_json=json_decode($_POST['gifts_json']);
					foreach($gifts_json as $gj){
						$gift_list=new Giftslist;
						$gift_list->id_gift=$model->id;
						$gift_list->description=$gj->description;
						$gift_list->price=$gj->price;
						$gift_list->order=$gj->order;
						$gift_list->id_user=Yii::app()->user->id;
						$gift_list->save();
					}		
				}					
				if(!is_dir('uploads/gifts')){
					mkdir('uploads/gifts');
				}					
				if(!is_dir('uploads/gifts/'.$model->id)){
					mkdir('uploads/gifts/'.$model->id);
				}	
				Utils::vaciarcacheimages('uploads/tmp/'.Yii::app()->user->id.'/');					
				$this->redirect(array('admin'));
			}				
		}else{
			foreach($model->imagenes as $imagenes){
				Utils::mover_directorio_prod($model->id,$imagenes->filename);
			}		
		}

		$this->render('update',array(
			'model'=>$model,'gift_list_selected'=>$gift_list_selected
		));
	}
	
	public function actionPublish(){
		$this->layout='pagina_clear';

		if(isset($_POST['hour']) && isset($_POST['date']))
		{
			header('Content-Type: application/json');
			$criteria = new CDbCriteria;
			$criteria->addCondition('enabled=1');
			$criteria->order='date asc';
			$model=Gifts::model()->findAll($criteria);
			$id_menu=Yii::app()->user->id_menu();
			$criteria = new CDbCriteria;
			$criteria->addCondition("id_menu=$id_menu");			
			$images_sizes=Configimages::model()->findAll($criteria);			
			$content=array();
			$date = date_create_from_format('d/m/Y', $_POST['date']);
			$publication_date=date_format($date,'Y-m-d').' '.$_POST['hour'].':00';
			$criteria = new CDbCriteria;
			$id_menu=Yii::app()->user->id_menu();
			$moment=array();
			foreach($model as $gift){
				$array_imagenes=array();
				foreach($gift->imagenes as $imagenes){
					foreach($images_sizes as $sizes){
						$array_imagenes[]=array(
							'title'=>$imagenes->title,
							'width'=>$sizes->width,
							'height'=>$sizes->height,
							'link'=>$imagenes->link,							
							'filename'=>'http://'.$_SERVER['SERVER_NAME'].'/'.Yii::app()->request->baseUrl.'/uploads/gifts/'.$imagenes->id_gifts.'/'.$imagenes->filename.'/'.$imagenes->filename.'_'.$sizes->width.'x'.$sizes->height,
							'ext'=>$imagenes->ext
						);						
					}
				}					
				$moment[]=array(
					'img'=>$array_imagenes,
					'id'=>$gift->id,
					'user'=>$gift->usuario->name.' '.$gift->usuario->lastname,
					'user_email'=>$gift->usuario->email,
					'id_type_currency'=>$gift->currency->description,
					'date'=>$gift->date,
					'boyfriends'=>$gift->boyfriends,
					'destinations'=>$gift->destinations,
					'dk'=>$gift->dk,
					'created_at'=>$gift->created_at,
					'gift_list'=>$gift->giftslist
				);
			}
			$publication_cdn=Publish::Run(array('cdn'),'gifts',array(),array(),$_POST['date'],$_POST['hour'],$moment,array('img'));

		}else{
			$this->render('publish',array('title'=>Yii::app()->user->Title_nocss(),'breadcrumb'=>Yii::app()->user->Breadcrumb_nocss()));
		}
	}
	
	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax'])){
			$this->redirect(array('admin'));
		}
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$this->loadJQuery=false;
		$model=new Gifts('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Gifts'])){
			$model->attributes=$_GET['Gifts'];
		}
		if(isset($_GET['criterio'])){
			$model->criterio=$_GET['criterio'];
		}	
		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Gifts the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Gifts::model()->findByPk($id);
		$model=Gifts::normalize_dates($model);	
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Gifts $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='gifts-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
