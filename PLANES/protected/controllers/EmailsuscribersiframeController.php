<?php

class EmailsuscribersiframeController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
		public $layout='pagina_clear_iframe';

	
	  
	/* Acciones para que trabajen con iframes*/
	
	/**
	 * Manages all models.
	 */
	public function actionAdminiframe($username='')
	{

		$usuarioActual=Users::model()->findByAttributes(array('username'=>$username));
		$idUsuarioActual=$usuarioActual->id; 
		$usuariosAdministradores=Emailassistant::model()->findAllByAttributes(array('id_assistant'=>$idUsuarioActual));
		 
		if(!empty($usuariosAdministradores)){
			foreach($usuariosAdministradores as $key => $val ){
				foreach($val as $k => $v ){ 
					if($k=='id_user'){					
						$usuariosAdministradores2=Users::model()->findByAttributes(array('id'=>$v));
						$usersAdmin[]=$usuariosAdministradores2->username;
					}
				}
			}
		}
		else{
			$usersAdmin[]="";
			}
 
		$urlFinal=Yii::app()->params['contacts_api_url'].'/Contacto/Listado';
		$data=array(
				'UserName'=>$usuarioActual->username,				
				'AdminUsers'=> $usersAdmin,		 
			);	

		$opts = array('http' =>
			array(
				'method'  => 'POST',
				'ignore_errors' => true,
				'header'=>"Content-Type: application/json\r\n"
				,				
				'content'=>json_encode($data)
			)
		);

		$context  = stream_context_create($opts);
		$url = $urlFinal;
		$json = file_get_contents($url, false,$context); 
		$json=json_decode($json);

		$this->loadJQuery=false;
		$array_contactos = array();
		$bodyJson = $json->data;
		$contactos_totales=0;
		$contactos_suscriptos=0;
		$contactos_desuscriptos=0;
		$contactos_invalidos=0;
		$contactos_bloqueados=0;
		$contactos_desuscriptosxusr=0;
		$arrayContadores = [];
		Yii::import('application.controllers.EmailcampaignController');
		$domains = EmailcampaignController::getBannedDomains();
		foreach ($bodyJson as $key => $value) {
			$idContacto = $value->IdContacto;
			$nombre = $value->Nombre;
			$email = $value->Email;
			$suscripto = $value->Suscripto;
			$compartido = $value->Compartido;
			$admin = $value->Admin;
			$nombreAdmin = $value->NombreAdmin;
			$emailAdmin = $value->EmailAdmin;
			foreach($domains as $key => $val){
				if(strpos($email,$val)){
				$esProhibido=true;}
			else{$esProhibido=false;}}
			if(empty($esProhibido))$esProhibido=false;
			$array = array('id'=>$idContacto,'nombre'=>$nombre, 'email'=>$email,'suscripto'=>$suscripto,'compartido'=>$compartido,'admin'=>$admin,'nombreAdmin'=>$nombreAdmin,'emailAdmin'=>$emailAdmin, 'prohibido'=>$esProhibido);
			array_push($array_contactos, $array);
			
			if(strcasecmp($admin, $usuarioActual->username) == 0)
			{
				//contadores
				$contactos_totales++;
				switch($suscripto){
					case 0:
						$contactos_suscriptos++;
						break;
					case 1:
						$contactos_desuscriptos++;
						break;
					case 2:
						$contactos_invalidos++;
						break;
					case 3:
						$contactos_bloqueados++;
						break;
					case 4:
						$contactos_desuscriptosxusr++;
						break;
				}
			}
			else
			{
				 if (isset($arrayContadores[$admin]['contactos_totales']))
					{
						$arrayContadores[$admin]['contactos_totales']++;
					} else {
						$arrayContadores[$admin]['contactos_totales']=1;
					}
				switch($suscripto){
					case 0:
						 if (isset($arrayContadores[$admin]['contactos_suscriptos']))
						{
							$arrayContadores[$admin]['contactos_suscriptos']++;
						} else {
							$arrayContadores[$admin]['contactos_suscriptos']=1;
							}
						break;
					case 1:
						if (isset($arrayContadores[$admin]['contactos_desuscriptos']))
						{
							$arrayContadores[$admin]['contactos_desuscriptos']++;
						} else {
							$arrayContadores[$admin]['contactos_desuscriptos']=1;
							}
						break;
					case 2:
						if (isset($arrayContadores[$admin]['contactos_invalidos']))
						{
							$arrayContadores[$admin]['contactos_invalidos']++;
						} else {
							$arrayContadores[$admin]['contactos_invalidos']=1;
							}
						break;
					case 3:
						if (isset($arrayContadores[$admin]['contactos_bloqueados']))
						{
							$arrayContadores[$admin]['contactos_bloqueados']++;
						} else {
							$arrayContadores[$admin]['contactos_bloqueados']=1;
							}
						break;
					case 4:
						if (isset($arrayContadores[$admin]['contactos_desuscriptosxusr']))
						{
							$arrayContadores[$admin]['contactos_desuscriptosxusr']++;
						} else {
							$arrayContadores[$admin]['contactos_desuscriptosxusr']=1;
							}
						break;
				}
			}
		}
	 
		$arrayBk=$array_contactos;
		if(isset($_GET['criterio']) && $_GET['criterio']!=''){
			
			$array_criterio = array();
			foreach($array_contactos as $key => $val){
				if (strpos($val['nombre'], $_GET['criterio']) !== false || strpos($val['email'], $_GET['criterio']) !== false) {
					$array = array('id'=>$val['id'],'nombre'=>$val['nombre'], 'email'=>$val['email'],'suscripto'=>$val['suscripto'],'compartido'=>$val['compartido'],'admin'=>$val['admin'],'nombreAdmin'=>$val['nombreAdmin'],'emailAdmin'=>$val['emailAdmin'],'prohibido'=>$val['prohibido']);
					array_push($array_criterio, $array);
				}
			}			
			$array_contactos=$array_criterio;
		}
 
		$this->render('//emailsuscribers/adminiframe',array(
			 'contactosiframe'=>$array_contactos,
			 'usernameiframe'=>$username,
			 'contactos_totales'=>$contactos_totales,
			 'contactos_suscriptos'=>$contactos_suscriptos,
			 'contactos_desuscriptos'=>$contactos_desuscriptos,
			 'contactos_invalidos'=>$contactos_invalidos,
			 'contactos_bloqueados'=>$contactos_bloqueados,
			 'contactos_desuscriptosxusr'=>$contactos_desuscriptosxusr,
			 'array_contadores'=>$arrayContadores,
		));
	
	}
	
	public function actionSubscribeiframe($id, $username='')
	{
		$opts = array('http' =>
            array(
                'method' => 'GET',
                'ignore_errors' => false,		
				'header'=>"Content-Type: application/json\r\n",					
            )
        );
		
        $context  = stream_context_create($opts);		
		
		$url=Yii::app()->params['contacts_api_url'].'/Contacto/GetMail/'.$id;
		$json = file_get_contents($url,false,$context); 		
		$json=json_decode($json,1);
		if($json['status']=='Ok')
		{
			 $data= $json['data'];
			 $email=$data['Email'];
			
			$opts = array('http' =>
				array(
					'method' => 'PUT',
					'ignore_errors' => false,		
					'header'=>"Content-Type: application/json\r\n",		 
					'header'=>"Content-length: ".strlen(''),				
				)
			);
			$context  = stream_context_create($opts);		
			
			$url=Yii::app()->params['contacts_api_url'].'/Contacto/Suscribir/'.$id.'/'.$username;
			$json = file_get_contents($url,false,$context); 		
			$json=json_decode($json,1);

			if($json['status']=='Ok'){		
				echo $json['msg'];
				/*$url = Yii::app()->params['datamedia_url'];
				$data['login_username'] = Yii::app()->params['datamedia_user'];
				$data['login_password'] = Yii::app()->params['datamedia_pass'];
				$data["ListID"]=Yii::app()->params['datamedia_list'];
				$data["function"]="Subscribe";
				$data["action"]="subscribe";	//[subscribe|unsubscribe|status]
				$data["email"]=$email;	 
				$ch = curl_init($url);
				curl_setopt($ch, CURLOPT_POST, 1);
				curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($ch, CURLOPT_HEADER, 0);
				curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-type: multipart/form-data"));
				 
				if($resutl = curl_exec($ch))
				{
				curl_close($ch);
				echo $json['msg'];
				}
				print_r($resutl );die();
				*/
			}
			else{
				echo 'Ocurrió un error grabando en Terrena';
			}
		}
	}

	public function actionUnsubscribeiframe($id, $username)
	{
		$opts = array('http' =>
            array(
                'method' => 'GET',
                'ignore_errors' => false,		
				'header'=>"Content-Type: application/json\r\n",					
            )
        );
		
        $context  = stream_context_create($opts);		
		
		$url=Yii::app()->params['contacts_api_url'].'/Contacto/GetMail/'.$id;
		$json = file_get_contents($url,false,$context); 		
		$json=json_decode($json,1);
		if($json['status']=='Ok')
		{
			 $data= $json['data'];
			 $email=$data['Email'];
			
			
			$opts = array('http' =>
				array(
					'method'  => 'PUT',
					'ignore_errors' => false,
					'header'=>"Content-Type: application/json\r\n",		 
					'header'=>"Content-length: ".strlen(''),	
				)
			);
			$context  = stream_context_create($opts);		
			$url=Yii::app()->params['contacts_api_url'].'/Contacto/Desuscribir/'.$id.'/'.$username; 		
			$json = file_get_contents($url,false,$context); 
			$json=json_decode($json,true); 
			
			if($json['status']=='Ok'){			
				echo $json['msg'];			
				/*$url = Yii::app()->params['datamedia_url'];
				$data['login_username'] = Yii::app()->params['datamedia_user'];
				$data['login_password'] = Yii::app()->params['datamedia_pass'];
				$data["ListID"]=Yii::app()->params['datamedia_list'];
				$data["function"]="Subscribe";
				$data["action"]="unsubscribe";	//[subscribe|unsubscribe|status]
				$data["email"]=$email;	 
				$ch = curl_init($url);
				curl_setopt($ch, CURLOPT_POST, 1);
				curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($ch, CURLOPT_HEADER, 0);
				curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-type: multipart/form-data"));
				 
				if($resutl = curl_exec($ch))
				{
				curl_close($ch);
				}
				echo $json['msg'];
			*/
			}
			else{
				echo 'Ocurrió un error grabando en Terrena';
			}
		}
	}

	public function actionShareiframe($id, $username)
	{
		$opts = array('http' =>
            array(
                'method'  => 'PUT',
                'ignore_errors' => true,
				'header'=>"Content-Type: application/json\r\n",		 
				'header'=>"Content-length: ".strlen(''),
            )
        );
        $context  = stream_context_create($opts);		
		$url=Yii::app()->params['contacts_api_url'].'/Contacto/Compartir/'.$id.'/'.$username;
		$json = file_get_contents($url,false,$context); 
		$json=json_decode($json);
		echo ($json->msg);
		
	}
	
	public function actionUnshareiframe($id, $username)
	{
		$opts = array('http' =>
            array(
                'method'  => 'PUT',
                'ignore_errors' => true,
				'header'=>"Content-Type: application/json\r\n",		 
				'header'=>"Content-length: ".strlen(''),
            )
        );
        $context  = stream_context_create($opts);		
		$url=Yii::app()->params['contacts_api_url'].'/Contacto/Ocultar/'.$id.'/'.$username;
		$json = file_get_contents($url,false,$context); 
		$json=json_decode($json);
		echo ($json->msg); 
	}
	
	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Emailsuscribers the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Emailsuscribers::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Emailsuscribers $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='email-suscribers-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	
}
