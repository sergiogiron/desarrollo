<?php

class EmailassistantiframeController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='pagina_clear_iframe';


	
	/*  IFRAME */
	
	
	public function actionCreateiframe($username)
	{
		$this->layout='pagina_clear_iframe';
		$usuarioActual=Users::model()->findByAttributes(array('username'=>$username));
		if(isset($_POST['asistentes']))
		{
			$asistentes=Yii::app()->request->getPost('asistentes');
			foreach($asistentes as $asistente){
				$model=new Emailassistant;
				$model->id_assistant=$asistente;
				$model->created_at=date("Y-m-d H:i:s");
				$model->id_user=$usuarioActual->id;
				$model->save();
			}
		}

		$this->render('//emailassistant/createiframe',array(
			'usernameiframe'=>$username,'idiframeuser'=>$usuarioActual->id,
		));
	}
	
	/**
	 * Manages all models.
	 */
	public function actionAdminiframe($username='')
	{
		$this->loadJQuery=false;
		$usuarioActual=Users::model()->findByAttributes(array('username'=>$username));
		$this->layout='pagina_clear_iframe';
		$this->loadJQuery=false;
		$model=new Emailassistant('searchbyUserid',array('userid' => $usuarioActual->id));
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Emailassistant'])){
			$model->attributes=$_GET['Emailassistant'];
		}     
		if(isset($_GET['criterio'])){
			$model->criterio=$_GET['criterio'];
		}			
		$this->render('//emailassistant/adminiframe',array(
			'model'=>$model,'usernameiframe'=>$username,'idiframeuser'=>$usuarioActual->id,
		));
	}
	
	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Emailassistant the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Emailassistant::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Emailassistant $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='email-assistant-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	
	
		/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDeleteiframe($username, $id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('adminiframe','username'=>$username));
	}
	
	
	
	
}
