<?php

class CreditcardsController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='pagina';

	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	public function accessRules()
	{
		$internal=array('cruisessupplier','cruisessupplieredit','airliners','airlinersedit','financing','financingedit');
		return array(
			array('deny', //no entra a ninguna accion ningun usuario que no este logueado
                'users'=>array('?'),
            ),
            array('allow', // acceden todos los usuarios autenticados a todas las acciones
				'actions'=>array_merge(Yii::app()->user->getRules(),$internal),
				'users'=>array('@'),
            ),
			 array('deny',
                'users'=>array('*'),
            ),
        );
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */

	public function actionCruisessupplier()
	{
		$this->renderPartial('cruisessupplier');
	}

	public function actionCruisessupplieredit()
	{
		$this->renderPartial('cruisessupplier_edit',array('data'=>$_POST));
	}

	public function actionAirliners()
	{
		$this->renderPartial('airliners');
	}

	public function actionAirlinersedit()
	{
		$this->renderPartial('airliners_edit',array('data'=>$_POST));
	}

	public function actionFinancing()
	{
		$this->renderPartial('financing');
	}

	public function actionFinancingedit()
	{
		$this->renderPartial('financing_edit',array('data'=>$_POST));
	}

	public function actionCreate($origin=null)
	{
		$model=new Creditcards;
		$error='';
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Creditcards']))
		{
			$model->attributes=$_POST['Creditcards'];
			if(isset($_POST['image'])){
				if($model->save()){
					if(isset($_POST['image'])){
						foreach($_POST['image'] as $images){
							$images_json=json_decode($images);
							$creditcards_images=new Creditcardsimages;
							$creditcards_images->id_credit_card=$model->id;
							$creditcards_images->id_user=Yii::app()->user->id;
							$creditcards_images->ext=$images_json->ext;
							$creditcards_images->filename=$images_json->name;
							$creditcards_images->position=$images_json->position;
							$creditcards_images->title=$images_json->title;
							$creditcards_images->link=$images_json->link;
							$creditcards_images->order=$images_json->order;
							$creditcards_images->coords=json_encode($images_json->coords);
							$creditcards_images->save();
							Utils::mover_directorio($model->id,$images_json->name);
						}
						Utils::vaciarcacheimages('uploads/tmp/'.Yii::app()->user->id.'/');
					}
					if(isset($_POST['financing'])){
						$financing=json_decode($_POST['financing']);
						Creditcardsfinancing::model()->deleteAll("id_credit_cards=".$model->id);
						foreach($financing as $fn){
							$financing_entity=new Creditcardsfinancing;
							$financing_entity->id_credit_cards=$model->id;
							$financing_entity->amount_dues=$fn->amount_dues;
							$financing_entity->interests=$fn->interests;
							$financing_entity->id_user=Yii::app()->user->id;
							$financing_entity->cftna=$fn->cftna;
							$financing_entity->posnet_cost=$fn->posnet_cost;
							$financing_entity->rewards=$fn->rewards;
							$financing_entity->legal=$fn->legal;
							$financing_entity->date_validity_start=$fn->date_validity_start;
							$financing_entity->date_validity_end=$fn->date_validity_end;
							$financing_entity->save();
							foreach($fn->services as $financing_services){
								$financing_services_relational=new Creditcardsfinancingservices;
								$financing_services_relational->id_credit_cards_financing=$financing_entity->id;
								$financing_services_relational->id_product=$financing_services->id;
								$financing_services_relational->save();
							}
							foreach($fn->entitys as $financing_entitys){
								$financing_entitys_relational=new Creditcardsentityrelational;
								$financing_entitys_relational->id_credit_cards_financing=$financing_entity->id;
								$financing_entitys_relational->id_entity=$financing_entitys->id;
								$financing_entitys_relational->save();
							}
						}
					}
					if(isset($_POST['airliners'])){
						$airliners=json_decode($_POST['airliners']);
						Creditcardsairliners::model()->deleteAll("id_credit_cards=".$model->id);
						foreach($airliners as $fn){
							$airliners_entity=new Creditcardsairliners;
							$airliners_entity->id_credit_cards=$model->id;
							$airliners_entity->amount_dues=$fn->amount_dues;
							$airliners_entity->interests=$fn->interests;
							$airliners_entity->id_user=Yii::app()->user->id;
							$airliners_entity->cftna=$fn->cftna;
							$airliners_entity->posnet_cost=$fn->posnet_cost;
							$airliners_entity->rewards=$fn->rewards;
							$airliners_entity->bank=$fn->bank;
							$airliners_entity->date_validity_start=$fn->date_validity_start;
							$airliners_entity->date_validity_end=$fn->date_validity_end;
							$airliners_entity->legal=$fn->legal;
							$airliners_entity->save();
							foreach($fn->airliners as $airliners_relacional){
								$airliners_entity_relational=new Creditcardsairlinersrelational;
								$airliners_entity_relational->id_credit_cards_airliners=$airliners_entity->id;
								$airliners_entity_relational->id_airliner=$airliners_relacional->id;
								$airliners_entity_relational->id_user=Yii::app()->user->id;
								$airliners_entity_relational->save();
							}
						}
					}
					if(isset($_POST['cruisessupplier'])){
						$cruisessupplier=json_decode($_POST['cruisessupplier']);
						Creditcardscruisessupplier::model()->deleteAll("id_credit_cards=".$model->id);
						foreach($cruisessupplier as $fn){
							$cruisessupplier_entity=new Creditcardscruisessupplier;
							$cruisessupplier_entity->id_credit_cards=$model->id;
							$cruisessupplier_entity->amount_dues=$fn->amount_dues;
							$cruisessupplier_entity->interests=$fn->interests;
							$cruisessupplier_entity->id_user=Yii::app()->user->id;
							$cruisessupplier_entity->cftna=$fn->cftna;
							$cruisessupplier_entity->posnet_cost=$fn->posnet_cost;
							$cruisessupplier_entity->rewards=$fn->rewards;
							$cruisessupplier_entity->bank=$fn->bank;
							$cruisessupplier_entity->date_validity_start=$fn->date_validity_start;
							$cruisessupplier_entity->date_validity_end=$fn->date_validity_end;
							$cruisessupplier_entity->legal=$fn->legal;
							$cruisessupplier_entity->save();
							foreach($fn->cruisessupplier as $cruisessupplier_relacional){
								$cruisessupplier_entity_relational=new Creditcardscruisessupplierrelational;
								$cruisessupplier_entity_relational->id_credit_cards_cruises_supplier=$cruisessupplier_entity->id;
								$cruisessupplier_entity_relational->id_cruises_supplier=$cruisessupplier_relacional->id;
								$cruisessupplier_entity_relational->id_user=Yii::app()->user->id;
								$cruisessupplier_entity_relational->save();
							}
						}
					}
					if($origin){
						$this->redirect(array('//'.$origin.'/admin'));
					}else{
						$this->redirect(array('admin'));
					}
				}
			}else{
				$error='Debe subir el logo';
			}

		}

		$this->render('create',array(
			'model'=>$model,'error'=>$error
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		$error='';

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Creditcards']))
		{
			$model->attributes=$_POST['Creditcards'];
			if(isset($_POST['image'])){
				if($model->save()){
					Utils::vaciarcacheprodimages('uploads/creditcards/'.$model->id.'/');
					if(isset($_POST['image'])){
						Creditcardsimages::model()->deleteAll("id_credit_card=$id");
						foreach($_POST['image'] as $images){
							$images_json=json_decode($images);
							$creditcards_images=new Creditcardsimages;
							$creditcards_images->id_credit_card=$model->id;
							$creditcards_images->id_user=Yii::app()->user->id;
							$creditcards_images->ext=$images_json->ext;
							$creditcards_images->filename=$images_json->name;
							$creditcards_images->position=$images_json->position;
							$creditcards_images->title=$images_json->title;
							$creditcards_images->link=$images_json->link;
							$creditcards_images->coords=json_encode($images_json->coords);
							$creditcards_images->order=$images_json->order;
							$creditcards_images->save();
							Utils::mover_directorio($model->id,$images_json->name);
						}
					}
					Utils::vaciarcacheimages('uploads/tmp/'.Yii::app()->user->id.'/');
					if(isset($_POST['financing'])){
						$financing=json_decode($_POST['financing']);
						Creditcardsfinancing::model()->deleteAll("id_credit_cards=".$model->id);
						foreach($financing as $fn){
							$financing_entity=new Creditcardsfinancing;
							$financing_entity->id_credit_cards=$model->id;
							$financing_entity->amount_dues=$fn->amount_dues;
							$financing_entity->interests=$fn->interests;
							$financing_entity->id_user=Yii::app()->user->id;
							$financing_entity->cftna=$fn->cftna;
							$financing_entity->posnet_cost=$fn->posnet_cost;
							$financing_entity->rewards=$fn->rewards;
							$financing_entity->legal=$fn->legal;
							$financing_entity->date_validity_start=$fn->date_validity_start;
							$financing_entity->date_validity_end=$fn->date_validity_end;
							$financing_entity->save();
							foreach($fn->services as $financing_services){
								$financing_services_relational=new Creditcardsfinancingservices;
								$financing_services_relational->id_credit_cards_financing=$financing_entity->id;
								$financing_services_relational->id_product=$financing_services->id;
								$financing_services_relational->save();
							}
							foreach($fn->entitys as $financing_entitys){
								$financing_entitys_relational=new Creditcardsentityrelational;
								$financing_entitys_relational->id_credit_cards_financing=$financing_entity->id;
								$financing_entitys_relational->id_entity=$financing_entitys->id;
								$financing_entitys_relational->save();
							}
						}
					}
					if(isset($_POST['airliners'])){
						$airliners=json_decode($_POST['airliners']);
						Creditcardsairliners::model()->deleteAll("id_credit_cards=".$model->id);
						foreach($airliners as $fn){
							$airliners_entity=new Creditcardsairliners;
							$airliners_entity->id_credit_cards=$model->id;
							$airliners_entity->amount_dues=$fn->amount_dues;
							$airliners_entity->interests=$fn->interests;
							$airliners_entity->id_user=Yii::app()->user->id;
							$airliners_entity->cftna=$fn->cftna;
							$airliners_entity->posnet_cost=$fn->posnet_cost;
							$airliners_entity->rewards=$fn->rewards;
							$airliners_entity->bank=$fn->bank;
							$airliners_entity->date_validity_start=$fn->date_validity_start;
							$airliners_entity->date_validity_end=$fn->date_validity_end;
							$airliners_entity->legal=$fn->legal;
							$airliners_entity->save();
							foreach($fn->airliners as $airliners_relacional){
								$airliners_entity_relational=new Creditcardsairlinersrelational;
								$airliners_entity_relational->id_credit_cards_airliners=$airliners_entity->id;
								$airliners_entity_relational->id_airliner=$airliners_relacional->id;
								$airliners_entity_relational->id_user=Yii::app()->user->id;
								$airliners_entity_relational->save();
							}
						}
					}
					if(isset($_POST['cruisessupplier'])){
						$cruisessupplier=json_decode($_POST['cruisessupplier']);
						Creditcardscruisessupplier::model()->deleteAll("id_credit_cards=".$model->id);
						foreach($cruisessupplier as $fn){
							$cruisessupplier_entity=new Creditcardscruisessupplier;
							$cruisessupplier_entity->id_credit_cards=$model->id;
							$cruisessupplier_entity->amount_dues=$fn->amount_dues;
							$cruisessupplier_entity->interests=$fn->interests;
							$cruisessupplier_entity->id_user=Yii::app()->user->id;
							$cruisessupplier_entity->cftna=$fn->cftna;
							$cruisessupplier_entity->posnet_cost=$fn->posnet_cost;
							$cruisessupplier_entity->rewards=$fn->rewards;
							$cruisessupplier_entity->bank=$fn->bank;
							$cruisessupplier_entity->date_validity_start=$fn->date_validity_start;
							$cruisessupplier_entity->date_validity_end=$fn->date_validity_end;
							$cruisessupplier_entity->legal=$fn->legal;
							$cruisessupplier_entity->save();
							foreach($fn->cruisessupplier as $cruisessupplier_relacional){
								$cruisessupplier_entity_relational=new Creditcardscruisessupplierrelational;
								$cruisessupplier_entity_relational->id_credit_cards_cruises_supplier=$cruisessupplier_entity->id;
								$cruisessupplier_entity_relational->id_cruises_supplier=$cruisessupplier_relacional->id;
								$cruisessupplier_entity_relational->id_user=Yii::app()->user->id;
								$cruisessupplier_entity_relational->save();
							}
						}
					}
					$this->redirect(array('admin'));
				}
			}else{
				$error='Debe subir el logo';
			}
		}else{
			foreach($model->imagenes as $imagenes){
				Utils::mover_directorio_prod($model->id,$imagenes->filename);
			}
		}

		$this->render('update',array(
			'model'=>$model,'error'=>$error
		));
	}

	public function actionPublish(){
		$this->layout='pagina_clear';

		if(isset($_POST['hour']) && isset($_POST['date']))
		{
			header('Content-Type: application/json');
			$criteria = new CDbCriteria;
			$criteria->addCondition('enabled=1');
			$model=Creditcards::model()->findAll($criteria);
			$content=array();
			$date = date_create_from_format('d/m/Y', $_POST['date']);
			$publication_date=date_format($date,'Y-m-d').' '.$_POST['hour'].':00';
			$criteria = new CDbCriteria;
			$id_menu=Yii::app()->user->id_menu();
			$criteria->addCondition("id_menu=$id_menu");
			$images_sizes=Configimages::model()->findAll($criteria);

			foreach($model as $creditcard){
				$array_imagenes=array();
				foreach($creditcard->imagenes as $imagenes){
					$array_imagenes[]=array(
						'title'=>$imagenes->title,
						'folder'=>'credit_cards',
						'filename'=>'http://'.$_SERVER['SERVER_NAME'].Yii::app()->request->baseUrl.'/uploads/creditcards/'.$imagenes->id_credit_card.'/'.$imagenes->filename.'/'.$imagenes->filename,
						'ext'=>$imagenes->ext
					);
					foreach($images_sizes as $sizes){
						$array_imagenes[]=array(
							'title'=>$imagenes->title,
							'width'=>$sizes->width,
							'folder'=>'credit_cards',
							'height'=>$sizes->height,
							'filename'=>'http://'.$_SERVER['SERVER_NAME'].Yii::app()->request->baseUrl.'/uploads/creditcards/'.$imagenes->id_credit_card.'/'.$imagenes->filename.'/'.$imagenes->filename.'_'.$sizes->width.'x'.$sizes->height,
							'ext'=>$imagenes->ext
						);
					}
				}
				$financing_value=array();
				foreach($creditcard->financing as $financing){
					$services_selected=array();
					foreach($financing->services as $service_select){
						$services_selected[]=(object) array('id'=>$service_select->id_product,'title'=>$service_select->product->title);
					}
					$financing_value[]= (object) array(
						'id'=>$financing->id,
						'services' =>$services_selected,
						'amount_dues' => $financing->amount_dues,
						'interests' => $financing->interests,
						'cftna' => $financing->cftna,
						'legal' => $financing->legal,
						'date_validity_start' => $financing->date_validity_start,
						'date_validity_end' => $financing->date_validity_end,
						'rewards' => $financing->rewards,
					  );
				}
				$airliners_value=array();
				foreach($creditcard->airliners as $airliners){
					$airliners_selected=array();
					foreach($airliners->airliners as $airliners_select){
						$airliners_selected[]=(object) array('id'=>$airliners_select->id_airliner,'title'=>$airliners_select->airliners->title);
					}
					$airliners_value[]= (object) array(
						'id'=>$airliners->id,
						'airliners' =>$airliners_selected,
						'amount_dues' => $airliners->amount_dues,
						'interests' => $airliners->interests,
						'cftna' => $airliners->cftna,
						'legal' => $airliners->legal,
						'date_validity_start' => $airliners->date_validity_start,
						'date_validity_end' => $airliners->date_validity_end,
						'bank'=>$airliners->bank,
						'rewards' => $airliners->rewards,
					  );
				}
				$cruisessupplier_value=array();
				foreach($creditcard->cruisessupplier as $cruisessupplier){
					$cruisessupplier_selected=array();
					foreach($cruisessupplier->cruisessupplier as $cruisessupplier_select){
						$cruisessupplier_selected[]=(object) array('id'=>$cruisessupplier_select->id_cruises_supplier,'title'=>$cruisessupplier_select->cruisessupplier->title);
					}
					$cruisessupplier_value[]= (object) array(
						'id'=>$cruisessupplier->id,
						'cruisessupplier' =>$cruisessupplier_selected,
						'amount_dues' => $cruisessupplier->amount_dues,
						'interests' => $cruisessupplier->interests,
						'cftna' => $cruisessupplier->cftna,
						'bank'=>$cruisessupplier->bank,
						'legal' => $cruisessupplier->legal,
						'date_validity_start' => $cruisessupplier->date_validity_start,
						'date_validity_end' => $cruisessupplier->date_validity_end,
						'rewards' => $cruisessupplier->rewards,
					  );
				}
				array_push($content,array(
				'financing'=>$financing_value,
				'airliners'=>$airliners_value,
				'cruisessupplier'=>$cruisessupplier_value,
				'imagenes'=>$array_imagenes,
				'title'=>$creditcard->name,
				'brand'=>$creditcard->brand,
				'enabled'=>$creditcard->enabled,
				'creditcards_highlight'=>$creditcard->creditcards_highlight
				));
			}

			$max_cuotas = Utils::getMaxCuotas();
			$financing_entity = Utils::getPaymentFull();
			$publication_app_json = Publish::Run(array('appjson'),'financing',array(),array(),$_POST['date'],$_POST['hour'],$financing_entity,array());
			$publication_cdn=Publish::Run(array('cdn'),'credit_cards',array(),array(),$_POST['date'],$_POST['hour'],$content,array("imagenes"));
			$publication_app=Publish::Run(array('app'),'credit_cards',array(),array(),$_POST['date'],$_POST['hour'],$model,array());
			$publication_cdn_max_cuotas = Publish::Run(array('cdn'),'max_cuotas',array(),array(),$_POST['date'],$_POST['hour'],$max_cuotas,array());

		}else{
			$this->render('publish',array('title'=>Yii::app()->user->Title_nocss(),'breadcrumb'=>Yii::app()->user->Breadcrumb_nocss()));
		}
	}

	public function actionPublicationrecord(){

	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Creditcards');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$this->loadJQuery=false;
		$model=new Creditcards('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Creditcards'])){
			$model->attributes=$_GET['Creditcards'];
		}

		if(isset($_GET['criterio'])){
			$model->criterio=$_GET['criterio'];
		}

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Creditcards the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Creditcards::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Creditcards $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='credit-cards-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
