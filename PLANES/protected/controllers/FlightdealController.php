<?php

class FlightdealController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='pagina';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		$internal=array('statusreward','promotional','origencode');
		return array(
			array('deny', //no entra a ninguna accion ningun usuario que no este logueado
                'users'=>array('?'),
            ),
            array('allow', // acceden todos los usuarios autenticados a todas las acciones
				'actions'=>array_merge(Yii::app()->user->getRules(),$internal),
				'users'=>array('@'),
            ),
			 array('deny',
                'users'=>array('*'),
            ),
        );
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$error='';
		$img_error='';
		$this->render('view',array(
			'model'=>$this->loadModel($id),'error'=>$error,'img_error'=>$img_error
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$this->loadJQuery=false;
		$model=new Flightdeal;
		$error='';
		$img_error='';
		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);
		if(isset($_POST['Flightdeal']))
		{
		$model->attributes=$_POST['Flightdeal'];
			if(isset($_POST['id_subchannel']) and isset($_POST['image'])){
				$error=0;
				if(isset($_POST['Flightdeal']['points'])){
					if($_POST['Flightdeal']['discount']=='' || $_POST['Flightdeal']['points']=='' || $_POST['Flightdeal']['total_points']==''){
						$error=1;
					}
				}else{
					if($_POST['Flightdeal']['type_currency']=='' || $_POST['Flightdeal']['price']=='' ){
						$error=1;
					}					
				}
				if($error==0){
					if($model->save()){
						$_POST['id_subchannel']=json_decode($_POST['id_subchannel']);
						if(count($_POST['id_subchannel'])>0){
							foreach($_POST['id_subchannel'] as $subchannel){
								$flightdeal_relational=new Flightdealrelational;
								$flightdeal_relational->id_flight_deal=$model->id;
								$flightdeal_relational->id_channel=$_POST['id_channel'];
								$flightdeal_relational->id_subchannel=$subchannel;
								$flightdeal_relational->id_user=Yii::app()->user->id;
								$flightdeal_relational->created_at=date("Y-m-d H:i:s");
								$flightdeal_relational->save();
							}
						}
						if(count($_POST['image'])>0){
							foreach($_POST['image'] as $images){
								$images_json=json_decode($images);
								$flightdeal_images=new Fligthdealimages;
								$flightdeal_images->id_fligth_deal=$model->id;
								$flightdeal_images->id_user=Yii::app()->user->id;
								$flightdeal_images->ext=$images_json->ext;
								$flightdeal_images->filename=$images_json->name;
								$flightdeal_images->position=$images_json->position;
								$flightdeal_images->title=$images_json->title;
								$flightdeal_images->link=$images_json->link;
								$flightdeal_images->order=$images_json->order;
								$flightdeal_images->coords=json_encode($images_json->coords);
								$flightdeal_images->save();
								Utils::mover_directorio($model->id,$images_json->name);
							}
							Utils::vaciarcacheimages('uploads/tmp/'.Yii::app()->user->id.'/');
						}

						$this->redirect(array('admin'));
					}					
				}

			}
		}

		$this->render('create',array('model'=>$model,'error'=>$error,'img_error'=>$img_error));
	}
	
	public function actionUpdate($id)
	{
		$this->loadJQuery=false;
		$error='';
		$img_error='';
		$model=$this->loadModel($id);
		// Uncomment the following line if AJAX validation is needed
		 $this->performAjaxValidation($model);

		if(isset($_POST['Flightdeal']))
		{
			$model->attributes=$_POST['Flightdeal'];
			if(!isset($_POST['Flightdeal']['price'])){
				$model->price=NULL;
				$model->type_currency=NULL;
			}
			if(isset($_POST['id_subchannel']) and isset($_POST['image'])){
				$error=0;
				if(isset($_POST['Flightdeal']['points'])){
					if($_POST['Flightdeal']['discount']=='' || $_POST['Flightdeal']['points']=='' || $_POST['Flightdeal']['total_points']==''){
						$error=1;
					}
				}else{
					if($_POST['Flightdeal']['type_currency']=='' || $_POST['Flightdeal']['price']=='' ){
						$error=1;
					}					
				}
				if($error==0){
					if($model->save()){
						Utils::vaciarcacheprodimages('uploads/flightdeal/'.$model->id.'/');
						Fligthdealimages::model()->deleteAll("id_fligth_deal=$id");
						Flightdealrelational::model()->deleteAll("id_flight_deal=$id");
						$id_subchannel=json_decode($_POST['id_subchannel']);
						if(count($id_subchannel)>0){
							foreach($id_subchannel as $subchannel){
								$flightdeal_relational=new Flightdealrelational;
								$flightdeal_relational->id_flight_deal=$model->id;
								$flightdeal_relational->id_channel=$_POST['id_channel'];
								$flightdeal_relational->id_subchannel=$subchannel;
								$flightdeal_relational->id_user=Yii::app()->user->id;
								$flightdeal_relational->created_at=date("Y-m-d H:i:s");
								$flightdeal_relational->save();
							}
						}				
						if(isset($_POST['image'])){
							foreach($_POST['image'] as $images){
								$images_json=json_decode($images);
								$flightdeal_images=new Fligthdealimages;
								$flightdeal_images->id_fligth_deal=$model->id;
								$flightdeal_images->ext=$images_json->ext;
								$flightdeal_images->filename=$images_json->name;
								$flightdeal_images->position=$images_json->position;
								$flightdeal_images->title=$images_json->title;
								$flightdeal_images->link=$images_json->link;
								$flightdeal_images->coords=json_encode($images_json->coords);
								$flightdeal_images->order=$images_json->order;
								$flightdeal_images->id_user=Yii::app()->user->id;
								$flightdeal_images->save();
								Utils::mover_directorio($model->id,$images_json->name);
							}
						}
						Utils::vaciarcacheimages('uploads/tmp/'.Yii::app()->user->id.'/');

						$this->redirect(array('admin'));
					}
				}
			}
		}else{
			foreach($model->imagenes as $imagenes){
				Utils::mover_directorio_prod($model->id,$imagenes->filename);
			}
		}
		$this->render('update',array(
			'model'=>$model,'error'=>$error,'img_error'=>$img_error
		));
	}
	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionPromotional(){
		$reward=0;
		$not_reward=0;
		if(isset($_POST['id_subchannel'])){
			if(count($_POST['id_subchannel'])>0){
				$criteria = new CDbCriteria;
				$criteria->addCondition('id in ('.implode(",", $_POST['id_subchannel']).')');
				$sub_channels=Subchannels::model()->findAll($criteria);
				foreach($sub_channels as $sbc){
					if($sbc->reward==1){
						$reward++;
					}else{
						$not_reward++;
					}			
				}
			}
		}
		$this->renderPartial('promotional',array('reward'=>$reward,'not_reward'=>$not_reward));		
	}
	
	public function actionOrigencode($code){
		$criteria = new CDbCriteria;
		$criteria->addCondition("code='$code'");
		$origen=Airliners::model()->find($criteria);
		echo $origen->id;
	}
	
	public function actionStatusreward()
	{	
		$reward=0;
		$not_reward=0;
		if(isset($_POST['id_subchannel'])){
			if(count($_POST['id_subchannel'])>0){
				$criteria = new CDbCriteria;
				$criteria->addCondition('id in ('.implode(",", $_POST['id_subchannel']).')');
				$sub_channels=Subchannels::model()->findAll($criteria);
				foreach($sub_channels as $sbc){
					if($sbc->reward==1){
						$reward++;
					}else{
						$not_reward++;
					}			
				}
			}
		}
		echo json_encode(array('reward'=>$reward,'not_reward'=>$not_reward));
	}


	public function actionPublish(){
		$this->layout='pagina_clear';
		$query=Yii::app()->db->createCommand("SELECT distinct marckup_config.id_channel,marckup_config.id_subchannel,channels.title as channel_title,subchannels.title as subchannel_title FROM marckup_config LEFT JOIN subchannels ON marckup_config.id_subchannel=subchannels.id  LEFT JOIN channels ON marckup_config.id_channel=channels.id where marckup_config.enabled=1")->queryAll();
		$channel=array();
		$content=array();
		$subchannel=array();
		foreach($query as $q){
			array_push($channel,array('id'=>$q['id_channel'],'name'=>$q['channel_title']));
			array_push($subchannel,array('id'=>$q['id_subchannel'],'name'=>$q['subchannel_title']));
		}
		if(isset($_POST['hour']) && isset($_POST['channel']) && isset($_POST['subchannel']) && isset($_POST['date']))
		{
			header('Content-Type: application/json');
			$criteria = new CDbCriteria;
			$id_menu=Yii::app()->user->id_menu();
			$criteria->addCondition("id_menu=$id_menu");
			$images_sizes=Configimages::model()->findAll($criteria);

			foreach($_POST['subchannel'] as $subcanal){
				$content=array();
				$criteria = new CDbCriteria;
				$criteria->addCondition('t.enabled=1 and relational.id_channel='.$_POST['channel'].' and relational.id_subchannel='.$subcanal);

				$model=Flightdeal::model()->with('relational')->findAll($criteria);

				foreach($model as $ofertas){
					$array_imagenes=array();
					foreach($ofertas->imagenes as $imagenes){
						$array_imagenes[]=array(
							'title'=>$imagenes->title,
							'filename'=>'http://'.$_SERVER['SERVER_NAME'].'/'.Yii::app()->request->baseUrl.'/uploads/flightdeal/'.$imagenes->id_fligth_deal.'/'.$imagenes->filename.'/'.$imagenes->filename,
							'ext'=>$imagenes->ext
						);
						foreach($images_sizes as $sizes){
							$array_imagenes[]=array(
								'title'=>$imagenes->title,
								'width'=>$sizes->width,
								'height'=>$sizes->height,
								'filename'=>'http://'.$_SERVER['SERVER_NAME'].'/'.Yii::app()->request->baseUrl.'/uploads/flightdeal/'.$imagenes->id_fligth_deal.'/'.$imagenes->filename.'/'.$imagenes->filename.'_'.$sizes->width.'x'.$sizes->height,
								'ext'=>$imagenes->ext
							);
						}
					}
					array_push($content,array(
						'img'=>$array_imagenes,
						'id'=>$ofertas->id,
						'name'=>$ofertas->title,
						'url'=>$ofertas->url,
						'type_currency'=>$ofertas->type_currency,
						'price'=>$ofertas->price,
						'points'=>$ofertas->points,
						'total_points'=>$ofertas->total_points,
						'active_from'=>$ofertas->expire_date_from,
						'active_to'=>$ofertas->expire_date_to,
						'id_airline'=>$ofertas->id_airline,
						'direct'=>$ofertas->direct,
						'enabled'=>$ofertas->enabled,
						'promotional'=>$ofertas->promotional,
						'detail'=>$ofertas->detail,
						'order'=>$ofertas->order,
						'discount'=>$ofertas->discount

				));
			}
			$publication_site=Publish::Run(array('site'),'flight_deal',$_POST['channel'],array('id_subchannel'=>$subcanal),$_POST['date'],$_POST['hour'],$content,array("img"));

			}
		}else{
			$this->render('publish',array('title'=>Yii::app()->user->Title_nocss(),'breadcrumb'=>Yii::app()->user->Breadcrumb_nocss(),'channel'=>$channel,'subchannel'=>$subchannel));
		}
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax'])){
			$this->redirect(array('admin'));
		}
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$this->loadJQuery=false;
		$model=new Flightdeal('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Flightdeal'])){
			$model->attributes=$_GET['Flightdeal'];
		}
		if(isset($_GET['criterio'])){
			$model->criterio=$_GET['criterio'];
		}
		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Flightdeal the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Flightdeal::model()->findByPk($id);
		$model=Flightdeal::normalize_dates($model);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Flightdeal $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='flightdeal-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
