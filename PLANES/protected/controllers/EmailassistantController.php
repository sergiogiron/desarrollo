<?php

class EmailassistantController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='pagina';


	public function filters()
	{		
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}
	
	public function accessRules()
	{
		return array(
			array('deny', //no entra a ninguna accion ningun usuario que no este logueado               
                'users'=>array('?'),
            ),
            array('allow', // acceden todos los usuarios autenticados a todas las acciones
				'actions'=>Yii::app()->user->getRules(),
				'users'=>array('@'),
            ),
			 array('deny',
                'users'=>array('*'),
            ),		
        );	
	}


	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		if(isset($_POST['asistentes']))
		{
			$asistentes=Yii::app()->request->getPost('asistentes');
			foreach($asistentes as $asistente){
				$model=new Emailassistant;
				$model->id_assistant=$asistente;
				$model->created_at=date("Y-m-d H:i:s");
				$model->id_user=Yii::app()->user->id;	
				$model->save();
			}
		}

		$this->render('create');
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Emailassistant']))
		{
			$model->attributes=$_POST['Emailassistant'];
			if($model->save()){
				$this->redirect(array('/asistentes'));
			}
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Emailassistant');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$this->loadJQuery=false;
		$model=new Emailassistant('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Emailassistant'])){
			$model->attributes=$_GET['Emailassistant'];
		}     
		if(isset($_GET['criterio'])){
			$model->criterio=$_GET['criterio'];
		}			
		$this->render('admin',array(
			'model'=>$model
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Emailassistant the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Emailassistant::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Emailassistant $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='email-assistant-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
