<?php

class EntitiesbinesController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='pagina';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		$internal=array('providersform','binesform','savebines','publishpreview');
		return array(
			array('deny', //no entra a ninguna accion ningun usuario que no este logueado
                'users'=>array('?'),
            ),
            array('allow', // acceden todos los usuarios autenticados a todas las acciones
				'actions'=>array_merge(Yii::app()->user->getRules(),$internal),
				'users'=>array('@'),
            ),
			 array('deny',
                'users'=>array('*'),
            ),
        );
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public static function getFinanciacion($content){
	  $finish=array();
	  $json  = json_encode($content);
	  $data = json_decode($json, true);
	  $array_finish=array();
	  for($h=0;$h<count($data);$h++){
	    $array_generic=array();
	    if((count($data[$h]['financing'])>0 and $data[$h]['enabled']=1) or (count($data[$h]['airliners'])>0 and $data[$h]['enabled']=1) or (count($data[$h]['cruisessupplier'])>0 and $data[$h]['enabled']=1)){
	      $bucle=array('financing','airliners','cruisessupplier');
	      foreach($bucle as $b){
	        for($i=0;$i<count($data[$h][$b]);$i++){
	        	
	          if(Utils::getVigencia(array('active_from'=>$data[$h][$b][$i]['date_validity_start'],'active_to'=>$data[$h][$b][$i]['date_validity_end']))>=0 and $data[$h][$b][$i]['rewards'] == "0"){
	            if(strpos($data[$h][$b][$i]['amount_dues'], '-')){
	              $pieces = explode("-", $data[$h][$b][$i]['amount_dues']);
	              rsort($pieces);
	              $data[$h][$b][$i]['amount_dues_max']=$pieces[0];
	            }elseif(strpos($data[$h][$b][$i]['amount_dues'], ',')){
	              $pieces = explode(",", $data[$h][$b][$i]['amount_dues']);
	              rsort($pieces);
	              $data[$h][$b][$i]['amount_dues_max']=$pieces[0];
	            }else{
	              $data[$h][$b][$i]['amount_dues_max']=$data[$h][$b][$i]['amount_dues'];
	            }
	            $c_f=array();
	            $count_financing='';
	            if(count($data[$h][$b][$i]['account_type'])==0){
	              $count_financing='todas las tarjetas ';
	              $c_f[$b]=$count_financing;
	            }else{
	              foreach($data[$h][$b][$i]['account_type'] as $fin){
	                $count_financing.=$fin['title'].',';
	              }
	              $c_f[$b]=$count_financing;
	            }
	            $data[$h][$b][$i]['text_2']='Para clientes con '.substr($c_f[$b],0,-1);
	            if($b=='financing'){
	              $c_f=array();
	              $count_financing='';
	              if(count($data[$h][$b][$i]['services'])==0){
	                $count_financing='todos los productos ';
	                $c_f[$b]=$count_financing;
	              }elseif(count($data[$h][$b][$i]['services'])>1){
	                $count_financing='productos seleccionados';
	                $c_f[$b]=$count_financing;
	              }else{
	                foreach($data[$h][$b][$i]['services'] as $fin){
	                  $count_financing.=$fin['title'].',';
	                }
	                $c_f[$b]=substr($count_financing,0,-1);
	              }
	              $data[$h][$b][$i]['text_1']='en '.$c_f[$b];
	            }elseif($b=='airliners'){
	              $c_f=array();
	              $count_financing='';
	              if(count($data[$h][$b][$i]['airliners'])==0){
	                $count_financing='todas las aerol&iacute;neas ';
	                $c_f[$b]=$count_financing;
	              }else{
	                foreach($data[$h][$b][$i]['airliners'] as $fin){
	                  $count_financing.=$fin['title'].',';
	                }
	                $c_f[$b]=substr($count_financing,0,-1);
	              }
	              $data[$h][$b][$i]['text_1']='con '.$c_f[$b];
	            }else{
	              $c_f=array();
	              $count_financing='';
	              if(count($data[$h][$b][$i]['cruisessupplier'])==0){
	                $count_financing='todas las navieras ';
	                $c_f[$b]=$count_financing;
	              }else{
	                foreach($data[$h][$b][$i]['cruisessupplier'] as $fin){
	                  $count_financing.=$fin['title'].',';
	                }
	                $c_f[$b]=substr($count_financing,0,-1);
	              }
	              $data[$h][$b][$i]['text_1']='con '.$c_f[$b];
	            }
	            $date_new=date_create_from_format('d/m/Y', $data[$h][$b][$i]['date_validity_end']);
	            $data[$h][$b][$i]['date_validity_end_max']= strtotime(date_format($date_new, 'Y-m-d'));
	            $array_generic[]=$data[$h][$b][$i];
	          }
	        }
	      }
	      
	      if(count($array_generic)>0){
	        $amount_dues_max=array();
	        $date_validity_end=array();
	        foreach ($array_generic as $key => $row) {
	          $amount_dues_max[$key]  = $row['amount_dues_max'];
	          $date_new=date_create_from_format('d/m/Y', $row['date_validity_end']);
	          $date_validity_end[$key] = strtotime(date_format($date_new, 'Y-m-d'));
	        }
	       // array_multisort($amount_dues_max, SORT_DESC, $date_validity_end, SORT_DESC, $array_generic);
	        $array_finish[]=array('entity_highlight'=>$data[$h]['entity_highlight'],'creditcards_highlight'=>0,'title'=>$data[$h]['title'],'banners'=>$data[$h]['banners'],'amount_dues_max_f'=>$array_generic[0]['amount_dues_max'],'date_validity_end_max'=>$array_generic[0]['date_validity_end_max'],'expire_date_from'=>$data[$h]['expire_date_from'],'expire_date_to'=>$data[$h]['expire_date_to'],'banners'=>$data[$h]['banners'],'imagenes'=>$data[$h]['imagenes'],'entity_highlight'=>$data[$h]['entity_highlight'],'data'=>$array_generic);
	      }
	    }
	  }
	  
	  $amount_dues_max=array();
	  $date_validity_end=array();
	  $entity_highlight=array();
	  $creditcards_highlight=array();
	  foreach ($array_finish as $key => $row) {
	    $amount_dues_max[$key]  = $row['amount_dues_max_f'];
	    $date_validity_end[$key] =  $row['date_validity_end_max'];
	    $entity_highlight[$key] =  $row['entity_highlight'];
	    $creditcards_highlight[$key] =  $row['creditcards_highlight'];
	  }
	  //array_multisort($entity_highlight, SORT_DESC,$creditcards_highlight, SORT_DESC,$amount_dues_max, SORT_DESC, $date_validity_end, SORT_DESC, $array_finish);
	  return $array_finish;
	}
	public function actionPublishpreview(){
		$this->layout='pagina_clear';
		
		$content = self::getPublishContent();
		$financiacion = self::getFinanciacion($content);
		$this->render('_publishpreview',array('financiacion' => $financiacion, 'title'=>Yii::app()->user->Title_nocss(),'breadcrumb'=>Yii::app()->user->Breadcrumb_nocss()));
	}
	
	public static function getPublishContent(){
		$criteria = new CDbCriteria;
			$criteria->addCondition('enabled=1 ORDER BY display_order ASC');
			$model=Entity::model()->findAll($criteria);
			$financing_model=Paymentsentityfinancing::model()->findAll();
			$content=array();
			
			$criteria = new CDbCriteria;
			$id_menu=Yii::app()->user->id_menu();
			$criteria->addCondition("id_menu=$id_menu");
			$images_sizes=Configimages::model()->findAll($criteria);


			foreach($model as $entity){
				$array_imagenes=array();
				foreach($entity->imagenes as $imagenes){
					//IMAGEN ORIGINAL
					/*$array_imagenes[]=array(
						'title'=>$imagenes->title,
						'folder'=>'entity',
						'filename'=>'http://'.$_SERVER['SERVER_NAME'].'/'.Yii::app()->request->baseUrl.'/uploads/entity/'.$entity->id.'/'.$imagenes->filename.'/'.$imagenes->filename,
						'ext'=>$imagenes->ext
					);*/
					foreach($images_sizes as $sizes){
						$array_imagenes[]=array(
							'title'=>$imagenes->title,
							'width'=>$sizes->width,
							'transition_interval'=>$sizes->transition_interval,
							'folder'=>'entity',
							'height'=>$sizes->height,
							'filename'=>'http://'.$_SERVER['SERVER_NAME'].'/'.Yii::app()->request->baseUrl.'/uploads/entity/'.$entity->id.'/'.$imagenes->filename.'/'.$imagenes->filename.'_'.$sizes->width.'x'.$sizes->height,
							'ext'=>$imagenes->ext
						);
					}
				}
				if($entity->filename!=''){
					$images_entity=array(
						array(
							'filename'=>'http://'.$_SERVER['SERVER_NAME'].Yii::app()->request->baseUrl.'/uploads/entity/'.$entity->id.'/'.$entity->filename.'/'.$entity->filename,
							'ext'=>$entity->ext,
							'folder'=>'entity',
							'title'=>strtolower($entity->title)
						),
					);
				}else{
					$images_entity=array();
				}
				$financing_value=array();
				foreach($entity->financing as $financing){
					$financing_selected=array();
					foreach($financing->financing as $financing_select){
						$financing_selected[]=(object) array('id'=>$financing_select->id_payments_entity_account_type,'title'=>$financing_select->accountype->credito->name.' '.$financing_select->accountype->account_type);
					}
					$services_selected=array();
					foreach($financing->services as $service_select){
						$services_selected[]=(object) array('id'=>$service_select->id_product,'title'=>$service_select->product->title);
					}
					$financing_value[]= (object) array(
						'id'=>$financing->id,
						'account_type' =>$financing_selected,
						'services' =>$services_selected,
						'amount_dues' => $financing->amount_dues,
						'interests' => $financing->interests,
						'cftna' => $financing->cftna,
						'legal' => $financing->legal,
						'date_validity_start' => $financing->date_validity_start,
						'date_validity_end' => $financing->date_validity_end,
						'rewards' => $financing->rewards,
					  );
				}
				$airliners_value=array();
				foreach($entity->airliners as $airliners){
					$airliners_selected=array();
					foreach($airliners->airliners as $airliners_select){
						$airliners_selected[]=(object) array('id'=>$airliners_select->id_airliner,'title'=>$airliners_select->airliners->title);
					}
					$services_selected=array();
					foreach($airliners->services as $service_select){
						$services_selected[]=(object) array('id'=>$service_select->id_payments_entity_account_type,'title'=>$service_select->accountype->credito->name.' '.$service_select->accountype->account_type);
					}
					$airliners_value[]= (object) array(
						'id'=>$airliners->id,
						'airliners' =>$airliners_selected,
						'amount_dues' => $airliners->amount_dues,
						'interests' => $airliners->interests,
						'cftna' => $airliners->cftna,
						'account_type' =>$services_selected,
						'legal' => $airliners->legal,
						'date_validity_start' => $airliners->date_validity_start,
						'date_validity_end' => $airliners->date_validity_end,
						'rewards' => $airliners->rewards,
					  );
				}
				$cruisessupplier_value=array();
				foreach($entity->cruisessupplier as $cruisessupplier){
					$cruisessupplier_selected=array();
					foreach($cruisessupplier->cruisessupplier as $cruisessupplier_select){
						$cruisessupplier_selected[]=(object) array('id'=>$cruisessupplier_select->id_cruises_supplier,'title'=>$cruisessupplier_select->cruisessupplier->title);
					}
					$services_selected=array();
					$services_list='';
					foreach($cruisessupplier->services as $service_select){
						$services_selected[]=(object) array('id'=>$service_select->id_payments_entity_account_type,'title'=>$service_select->accountype->credito->name.' '.$service_select->accountype->account_type);
					}
					$cruisessupplier_value[]= (object) array(
						'id'=>$cruisessupplier->id,
						'cruisessupplier' =>$cruisessupplier_selected,
						'amount_dues' => $cruisessupplier->amount_dues,
						'interests' => $cruisessupplier->interests,
						'cftna' => $cruisessupplier->cftna,
						'account_type' =>$services_selected,
						'legal' => $cruisessupplier->legal,
						'date_validity_start' => $cruisessupplier->date_validity_start,
						'date_validity_end' => $cruisessupplier->date_validity_end,
						'rewards' => $cruisessupplier->rewards,
					  );
				}
				array_push($content,array(
					'imagenes'=>$images_entity,
					'banners'=>$array_imagenes,
					'financing'=>$financing_value,
					'airliners'=>$airliners_value,
					'cruisessupplier'=>$cruisessupplier_value,
					'entity_highlight'=>$entity->entity_highlight,
					'title'=>$entity->title,
					'expire_date_from'=>$entity->expire_date_from,
					'expire_date_to'=>$entity->expire_date_to,
					'enabled'=>$entity->enabled
					)
				);
			}
			return $content;
	}

	public function actionPublish(){
		$this->layout='pagina_clear';
		
		if(isset($_POST['hour']) && isset($_POST['date'])){
			header('Content-Type: application/json');
			$date = date_create_from_format('d/m/Y', $_POST['date']);
			$publication_date=date_format($date,'Y-m-d').' '.$_POST['hour'].':00';
			$content = self::getPublishContent();
			$criteria = new CDbCriteria;
			$criteria->addCondition('enabled=1 ORDER BY display_order ASC');
			$model=Entity::model()->findAll($criteria);

			$financing_entity = Utils::getPaymentFull();
			$max_cuotas = Utils::getMaxCuotas();
			$publication_app_json = Publish::Run(array('appjson'),'financing',array(),array(),$_POST['date'],$_POST['hour'],$financing_entity,array());
			$publication_cdn=Publish::Run(array('cdn'),'entity',array(),array(),$_POST['date'],$_POST['hour'],$content,array("imagenes","banners"));

			$publication_app=Publish::Run(array('app'),'payments_entity',array(),array(),$_POST['date'],$_POST['hour'],$model,array());
			$publication_cdn_max_cuotas = Publish::Run(array('cdn'),'max_cuotas',array(),array(),$_POST['date'],$_POST['hour'],$max_cuotas,array());
				
			$modelBines=Bines::model()->findAll();
			$publication_app=Publish::Run(array('app'),'bines',array(),array(),$_POST['date'],$_POST['hour'],$modelBines,array());		

			$modelMerchants=Merchants::model()->findAll();
			$publication_app=Publish::Run(array('app'),'merchants',array(),array(),$_POST['date'],$_POST['hour'],$modelMerchants,array());	
						
		}else{
			$this->render('publish',array('title'=>Yii::app()->user->Title_nocss(),'breadcrumb'=>Yii::app()->user->Breadcrumb_nocss()));
		}
	}

			
	
	

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$query=Yii::app()->db->createCommand("
				SELECT id_entity,entity,card_name,entity_image_name,entity_image_ext,enabled,expire_date_from,expire_date_to,dues,entity_highlight,type,provider_name
				FROM all_entities_fulldata
				group by entity,card_name,provider_name
				order by all_entities_fulldata.display_order asc,all_entities_fulldata.dues desc;")->queryAll();
		$entities = array();

		foreach($query as $key ) {
			$entity = $key['entity'];
			if(array_key_exists($entity,$entities)){
				array_push($entities[$entity],$key);
			}else{
				$entities[$entity][0] = $key;
			}
		}

		$this->render('admin',array(
			'entites'=>$entities,
		));
	}

	public function actionUpdate($id)
	{
	//	$nombre_banco = 

		$tarjetas=Yii::app()->db->createCommand("
				SELECT id_credit_card,card_name,entity,credit_card_image_name,credit_card_image_ext 
				FROM all_entities_fulldata
				WHERE id_entity = $id
				GROUP BY card_name
				ORDER BY card_name ASC;"
				)->queryAll();
		$nombre_entidad = $tarjetas[0]['entity'];

		$this->render('update',array(
			'id_entidad' => $id,
			'nombre_entidad'=>$nombre_entidad,
			'tarjetas'=>$tarjetas,
		));
	}

	public function getType($type){
		switch ($type) {
			case 'Airlines':
				$type = 'Aerolineas';
				break;
			case 'Cruiser':
				$type = 'Navieras';
				break;			
			default:
				//$type = $type;
				break;
		}
		return $type;
	}

	public function getTypeReverse($type){
		switch ($type) {
			case 'Aerolineas':
				$type = 'Airlines';
				break;
			case 'Navieras':
				$type = 'Cruiser';
				break;			
			default:
				//$type = $type;
				break;
		}
		return $type;
	}

	public function getClassCreditCard($account_type){
		$class = str_replace(' ', '-', strtolower($account_type));
		return $class;
	}

	public function actionProvidersform($id_entidad,$id_cc){
		$tts_financing = [];
		$airliners_financing = [];
		$cruiser_financing = [];
		$financings = [];

		$query=Yii::app()->db->createCommand("
		SELECT * 
		FROM all_entities_fulldata
		WHERE id_entity = $id_entidad
		AND id_credit_card = $id_cc;"
		)->queryAll();

		foreach($query as $key ) {
			$provider = self::getType($key['type']);
			if(array_key_exists($provider,$financings)){
				array_push($financings[$provider],$key);
			}else{
				$financings[$provider][0] = $key;
			}
		}
		$this->renderPartial('_form_accounttypes',array(
			'providers'=>$financings,
		));
	}

	
	
	
	public function actionSavebines(){
		$builder = Yii::app()->db->schema->commandBuilder;
		$bines = explode('|', $_POST['bines']);
		$enabled_products = explode('|', $_POST['enabled_products']);

		$deleteCriteria = array(
				'id_entity' 		=> $_POST['id_entity'],
				'id_credit_card' 	=> $_POST['id_credit_card'],
				'id_account_type' 	=> $_POST['id_account_type'],
				'providertype' 		=> self::getTypeReverse($_POST['providertype'])
		);
		foreach (array('bines','merchants') as $table) {
			$queryDelete = "DELETE FROM $table WHERE id_entity = :id_entity AND id_credit_card = :id_credit_card AND id_provider is null AND id_account_type = :id_account_type AND type = :providertype";
			$commandBines = Yii::app()->db->createCommand($queryDelete);
			$commandBines->execute($deleteCriteria);
		}




		$tmparray = [];
		$insertBines = [];
		$insertMerchants = [];
		$tmpBines['id_entity']=$_POST['id_entity'];
		$tmpBines['id_credit_card']=$_POST['id_credit_card'];
		$tmpBines['id_provider']='';
		$tmpBines['id_account_type']=$_POST['id_account_type'];
		$tmpBines['enabled']=1;
		$tmpBines['created_at']=date("Y-m-d H:i:s");
		$tmpBines['id_user']=Yii::app()->user->id;
		if(sizeof($enabled_products)>0){
			foreach ($enabled_products as $kp => $enabled_prod) {
				$tmpBines['id_product']=$enabled_prod;
				$tmpBines['type']= self::getTypeReverse($_POST['providertype']);
				$tmpMerchant = $tmpBines;
				$tmpMerchant['id_merchant'] = $_POST['merchant'];
				unset($tmpMerchant['bin']);
				array_push($insertMerchants, $tmpMerchant);
				foreach ($bines as $key => $bin) {
					$tmpBines['bin']=$bin;
					array_push($insertBines, $tmpBines);
				}
			}
		}else{
			$tmpBines['id_product']='';
			$tmpBines['type']= self::getTypeReverse($_POST['providertype']);
			$tmpMerchant = $tmpBines;
			$tmpMerchant['id_merchant'] = $_POST['merchant'];
			unset($tmpMerchant['bin']);
			array_push($insertMerchants, $tmpMerchant);
			foreach ($bines as $key => $bin) {
				$tmpBines['bin']=$bin;
				$tmpBines['type']= self::getTypeReverse($_POST['providertype']);
				array_push($insertBines, $tmpBines);
			}
		}
		

		$commandBines = $builder->createMultipleInsertCommand('bines',$insertBines);
		$commandMerchants = $builder->createMultipleInsertCommand('merchants',$insertMerchants);
		if($commandBines->execute() && $commandMerchants->execute()){
			echo "OK";
		}

	}
	public function actionBinesform($id_account_type,$providertype,$id_entity,$id_credit_card){
		$array_data = array(
			'id_account_type' => $id_account_type, 
			'id_entity' => $id_entity, 
			'providertype' => $providertype, 
			'id_credit_card' => $id_credit_card, 
		);

		$tables = array(
			array('name'=>'bines','column'=>'bin'),
			array('name'=>'merchants','column'=>'id_merchant')
		);
		$array_bines = [];
		$array_merchants = [];

		foreach ($tables as $table) {
			$querySelect = "SELECT distinct ".$table['column']." FROM ".$table['name']." WHERE id_entity = $id_entity AND id_credit_card = $id_credit_card AND id_provider is null AND id_account_type = $id_account_type AND type = '".self::getTypeReverse($providertype)."'";
			${"array_".$table['name']} = Yii::app()->db->createCommand($querySelect)->queryAll();
		}

		$bines 		= implode('|', array_column($array_bines, 'bin'));
		$merchants 	= implode('|', array_column($array_merchants, 'id_merchant'));

		$entities=Yii::app()->db->createCommand("
		SELECT aef.id_product,aef.product,aef.id_provider,aef.provider_name
		FROM all_entities_fulldata aef
		WHERE 
		aef.id_entity = $id_entity 
		AND aef.id_credit_card = $id_credit_card 
		AND aef.id_account_type = $id_account_type
		AND aef.type = '".self::getTypeReverse($providertype)."'"
		)->queryAll();
		$products_by_entity =array();
		$products_final =array();
		$enabled_products = '';

		if($providertype=='TTS'){
			foreach ($entities as $key => $e) {
				if($e['id_product']!=''){
					$products_by_entity[$key]['id'] = $e['id_product']; 
					$products_by_entity[$key]['title'] = $e['product'];	
					$enabled_products.=$e['id_product'].'|';
				}
			}
			if(sizeof($products_by_entity)>0){
				$products_final = $products_by_entity;
			}else{
				$products_final=Yii::app()->db->createCommand("SELECT id,title from products where enabled = 1")->queryAll();
			}
		}else {			
			foreach ($entities as $key => $e) {
				if($e['id_provider']!=''){
					$products_by_entity[$key]['id'] = $e['id_provider']; 
					$products_by_entity[$key]['title'] = $e['provider_name'];	
					$enabled_products.=$e['id_provider'].'|';
				}
				 
			}
			if(sizeof($products_by_entity)>0){
				$products_final = $products_by_entity;
			}
		}

		$this->renderPartial('_formBinesMerchant',array(
			'products'=>$products_final,
			'array_data'=>$array_data,
			'bines'=>$bines,
			'merchants'=>$merchants,
			'enabled_products'=>rtrim($enabled_products,'|'),
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Excluderules the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Excluderules::model()->findByPk($id);
		$model=Excluderules::normalize_dates($model);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Excluderules $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='exclude-rules-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
