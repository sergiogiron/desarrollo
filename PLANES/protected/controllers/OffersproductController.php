<?php

class OffersproductController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='pagina';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('deny', //no entra a ninguna accion ningun usuario que no este logueado               
                'users'=>array('?'),
            ),
            array('allow', // acceden todos los usuarios autenticados a todas las acciones
				'actions'=>Yii::app()->user->getRules(),
				'users'=>array('@'),
            ),
			 array('deny',
                'users'=>array('*'),
            ),		
        );	
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('_view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Offersproduct;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Offersproduct']))
		{
			
			$model->attributes=$_POST['Offersproduct'];
			
			if($model->save()){
				$this->redirect(array('admin'));
			}
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$errors=array();
		$model=$this->loadModel($id);		
		$criteria = new CDbCriteria;
		$criteria->addCondition('id='.$model->id_subchannel.' and id_channel='.$model->id_channel);
		$model_subchannel=Subchannels::model()->find($criteria);	
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Offersproduct']))
		{
			$model->attributes=$_POST['Offersproduct'];
			$campos_validate = array("title", "order", "description","thankyou_text","legal");
			foreach($_POST['Offersproduct'] as $key => $value){
				if (in_array($key, $campos_validate)) {
					if($value==''){
						$errors[$key]='Debe completar este campo.';
					}
				}
			}	
			if(count($errors)==0){			
				if($model->save()){
					$this->redirect(array('admin'));
				}		
			}
		}

		$this->render('update',array(
			'model'=>$model,'errors'=>$errors,'model_subchannel'=>$model_subchannel
		));
	}

	public function actionPublish(){
		$this->layout='pagina_clear';
		$query=Yii::app()->db->createCommand("SELECT distinct offers_product.id_channel,offers_product.id_subchannel,channels.title as channel_title,subchannels.title as subchannel_title FROM offers_product LEFT JOIN subchannels ON offers_product.id_subchannel=subchannels.id  LEFT JOIN channels ON offers_product.id_channel=channels.id where offers_product.enabled=1")->queryAll();
		$channel=array();
		$subchannel=array();
		$content=array();
		foreach($query as $q){
			array_push($channel,array('id'=>$q['id_channel'],'name'=>$q['channel_title']));
			array_push($subchannel,array('id'=>$q['id_subchannel'],'name'=>$q['subchannel_title']));
		}
		if(isset($_POST['hour']) && isset($_POST['channel']) && isset($_POST['subchannel']) && isset($_POST['date']))
		{
			foreach($_POST['subchannel'] as $subcanal){
				$content=array();
				$criteria = new CDbCriteria;
				$criteria->addCondition('enabled=1 and id_channel='.$_POST['channel'].' and id_subchannel='.$subcanal);

				$model=Offersproduct::model()->findAll($criteria);	
				
				foreach($model as $offerproducts){
					array_push($content,array(
						'id'=>$offerproducts->id,
						'id_channel'=>$offerproducts->id_channel,
						'id_subchannel'=>$offerproducts->id_subchannel,
						'id_product'=>$offerproducts->id_product,
						'producto'=>strtolower($offerproducts->producto->title),
						'title'=>$offerproducts->title,
						'description'=>$offerproducts->description,
						'legal'=>$offerproducts->legal,
						'enabled'=>$offerproducts->enabled,
						'order'=>$offerproducts->order,
						'thankyou_text'=>$offerproducts->thankyou_text,
						'id_email_server_sales'=>$offerproducts->id_email_server_sales,
						'sale_user'=>$offerproducts->sale_user,
						'sale_show_as'=>$offerproducts->sale_show_as,
						'id_email_server_alerts'=>$offerproducts->id_email_server_alerts,
						'alerts_pass'=>$offerproducts->alerts_pass,
						'alerts_to'=>$offerproducts->alerts_to,
						'script'=>$offerproducts->script,
						'phone'=>$offerproducts->phone,
						'phone_description'=>$offerproducts->phone_description,
						'office_hours'=>$offerproducts->office_hours,								
						'sale_pass'=>$offerproducts->sale_pass,
						'sale_to'=>$offerproducts->sale_to,
						'alerts_user'=>$offerproducts->alerts_user,
						'alerts_show_as'=>$offerproducts->alerts_show_as,
						'alerts_subject_patterns'=>$offerproducts->alerts_subject_patterns							
					));
				}	
				$publication_site=Publish::Run(array('site'),'offers_product',$_POST['channel'],array('id_subchannel'=>$subcanal),$_POST['date'],$_POST['hour'],$content,'');
			}
			$criteria = new CDbCriteria;

			$criteria->condition='id_channel= '.$_POST['channel'].' and id_subchannel IN ('.implode(',',$_POST['subchannel']).') and enabled=1';
			
			$model=Offersproduct::model()->findAll($criteria);
			
			$publication_app=Publish::Run(array('app'),'offers_product',$_POST['channel'],$_POST['subchannel'],$_POST['date'],$_POST['hour'],$model,'');		
		}else{
			$this->render('publish',array('title'=>Yii::app()->user->Title_nocss(),'breadcrumb'=>Yii::app()->user->Breadcrumb_nocss(),'channel'=>$channel,'subchannel'=>$subchannel));
		}
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax'])){
			$this->redirect(array('admin'));
		}
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$this->loadJQuery=false;
		$model=new Offersproduct('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Offersproduct'])){
			$model->attributes=$_GET['Offersproduct'];
		}
		if(isset($_GET['criterio'])){
			$model->criterio=$_GET['criterio'];
		}	
		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Offersproduct the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Offersproduct::model()->findByPk($id);
		$model=Offersproduct::normalize_dates($model);	
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Offersproduct $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='offersproduct-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
