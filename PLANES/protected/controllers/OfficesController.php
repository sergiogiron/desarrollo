<?php

class OfficesController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='pagina';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('deny', //no entra a ninguna accion ningun usuario que no este logueado               
                'users'=>array('?'),
            ),
            array('allow', // acceden todos los usuarios autenticados a todas las acciones
				'actions'=>Yii::app()->user->getRules(),
				'users'=>array('@'),
            ),
			 array('deny',
                'users'=>array('*'),
            ),		
        );	
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$order=Offices::model()->findAll();
		$this->render('view',array(
			'model'=>$this->loadModel($id),
			'order'=>$order
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Offices;
		$order=Offices::model()->findAll();
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
		$model->order=count($order)+1;
		if(isset($_POST['Offices']))
		{
			$order_aux=$model->order+1;
			foreach($order as $o){
				if($o->order==$_POST['Offices']['order']){
					$o->order=$model->order;
					$o->save();
				}
			}			
			$model->attributes=$_POST['Offices'];
			$model->created_at=date("Y-m-d H:i:s");
			$model->id_user=Yii::app()->user->id;
			if($model->save()){
				
				if(!empty($_POST['image']) && count($_POST['image'])>0){
					foreach($_POST['image'] as $images){
						$images_json=json_decode($images);
						$offices_images=new Officesimages;
						$offices_images->id_office=$model->id;
						$offices_images->id_user=Yii::app()->user->id;
						$offices_images->ext=$images_json->ext;
						$offices_images->filename=$images_json->name;
						$offices_images->position=$images_json->position;
						$offices_images->title=$images_json->title;
						$offices_images->link=$images_json->link;
						$offices_images->order=$images_json->order;
						$offices_images->coords=json_encode($images_json->coords);
						$offices_images->save();
						Utils::mover_directorio($model->id,$images_json->name);
					}					
					Utils::vaciarcacheimages('uploads/tmp/'.Yii::app()->user->id.'/');
				}

				$this->redirect(array('admin'));
			}
		}

		$this->render('create',array(
			'model'=>$model,
			'order'=>$order
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		$order=Offices::model()->findAll();
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Offices']))
		{
			foreach($order as $o){
				if($o->order==$_POST['Offices']['order'] && $o->id!=$id){
					$o->order=$model->order;
					$o->save();
				}
			}			
			$model->attributes=$_POST['Offices'];
			$model->created_at=date("Y-m-d H:i:s");
			$model->id_user=Yii::app()->user->id;
			if($model->save()){
				Utils::vaciarcacheprodimages('uploads/offices/'.$model->id.'/');
				Officesimages::model()->deleteAll("id_office=$id");
				if(isset($_POST['image'])){
					
					foreach($_POST['image'] as $images){					
						$images_json=json_decode($images);
						$offices_images=new Officesimages;
						$offices_images->id_office=$model->id;
						$offices_images->ext=$images_json->ext;
						$offices_images->filename=$images_json->name;
						$offices_images->position=$images_json->position;
						$offices_images->title=$images_json->title;
						$offices_images->link=$images_json->link;
						$offices_images->coords=json_encode($images_json->coords);
						$offices_images->order=$images_json->order;
						$offices_images->id_user=Yii::app()->user->id;
						$offices_images->save();
						Utils::mover_directorio($model->id,$images_json->name);
					}					
				}
				Utils::vaciarcacheimages('uploads/tmp/'.Yii::app()->user->id.'/');
				
				$this->redirect(array('admin'));
			}				
		}else{
			foreach($model->imagenes as $imagenes){
				Utils::mover_directorio_prod($model->id,$imagenes->filename);
			}			
		}

		$this->render('update',array(
			'model'=>$model,
			'order'=>$order
		));
	}




	public function actionPublish(){
		$this->layout='pagina_clear';		
		if(isset($_POST['hour']) && isset($_POST['date']))
		{
			header('Content-Type: application/json');
			$criteria = new CDbCriteria;
			$criteria->addCondition('enabled=1');
			$model=Offices::model()->findAll($criteria);
			$content=array();
			$criteria = new CDbCriteria;
			$id_menu=Yii::app()->user->id_menu();
			$criteria->addCondition("id_menu=$id_menu");			
			$images_sizes=Configimages::model()->findAll($criteria);


				
			
			foreach($model as $oficina){
				$array_imagenes=array();
				foreach($oficina->imagenes as $imagenes){
					foreach($images_sizes as $sizes){
						$array_imagenes[]=array(
							'title'=>$imagenes->title,
							'width'=>$sizes->width,
							'height'=>$sizes->height,							
							'filename'=>'http://'.$_SERVER['SERVER_NAME'].'/'.Yii::app()->request->baseUrl.'/uploads/offices/'.$imagenes->id_office.'/'.$imagenes->filename.'/'.$imagenes->filename.'_'.$sizes->width.'x'.$sizes->height,
							'ext'=>$imagenes->ext
						);						
					}
				}
				/*array_push($content,array(
					'imagenes'=>$array_imagenes,
					'id'=>$oficina->id,
					'name'=>$oficina->name,
					'schedule'=>$oficina->schedule,
					'phone'=>$oficina->phone,
					'fax'=>$oficina->fax,
					'address'=>$oficina->address,
					'mail'=>$oficina->mail,
					'info'=>$oficina->info,
					'order'=>$oficina->order,
					'responsable'=>$oficina->responsable,
					'email_contact'=>$oficina->usuario->email,
					'public'=>$oficina->public,
					'enabled'=>$oficina->enabled,
					));*/

					$content[str_replace(" ","-",$oficina->name)] = array(
						'imagenes'=>$array_imagenes,
						'id'=>$oficina->id,
						'name'=>$oficina->name,
						'schedule'=>$oficina->schedule,
						'phone'=>$oficina->phone,
						'fax'=>$oficina->fax,
						'address'=>$oficina->address,
						'mail'=>$oficina->mail,
						'info'=>$oficina->info,
						'order'=>$oficina->order,
						'responsable'=>$oficina->responsable,
						'email_contact'=>$oficina->usuario->email,
						'public'=>$oficina->public,
						'enabled'=>$oficina->enabled,
					);

			}
			$publication_cdn=Publish::Run(array('cdn'),'offices',array(),array(),$_POST['date'],$_POST['hour'],$content,array("imagenes"));				
		}else{
			$this->render('publish',array('title'=>Yii::app()->user->Title_nocss(),'breadcrumb'=>Yii::app()->user->Breadcrumb_nocss()));
		}
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax'])){
			$this->redirect(array('admin'));
		}
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$this->loadJQuery=false;
		$model=new Offices('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Offices'])){
			$model->attributes=$_GET['Offices'];
		}
		if(isset($_GET['criterio'])){
			$model->criterio=$_GET['criterio'];
		}	
		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Offices the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Offices::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Offices $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='offices-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
