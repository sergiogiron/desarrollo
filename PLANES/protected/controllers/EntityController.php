<?php

class EntityController extends Controller
{
	public $criterio;
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='pagina';

	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	public function accessRules()
	{
		$internal=array('cruisessupplier','cruisessupplieredit','airliners','airlinersedit','financing','financingedit','typeaccount','typeaccountedit','typeaccounteautocomplete','filterentities','disableentity','updateorder');
		return array(
			array('deny', //no entra a ninguna accion ningun usuario que no este logueado
                'users'=>array('?'),
            ),
            array('allow', // acceden todos los usuarios autenticados a todas las acciones
				'actions'=>array_merge(Yii::app()->user->getRules(),$internal),
				'users'=>array('@'),
            ),
			 array('deny',
                'users'=>array('*'),
            ),
        );
	}


	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	public function actionCruisessupplier()
	{
		$select_accounts=array();
		$_POST['data']=json_decode($_POST['data']);
		foreach($_POST['data'] as $data){
			$select_accounts[]=array('id'=>$data->id,'title'=>$data->credit_cards_name.' '.$data->account_type);
		}
		$this->renderPartial('cruisessupplier',array('select_accounts'=>$select_accounts));
	}

	public function actionCruisessupplieredit()
	{
		$select_accounts=array();
		$_POST['select']=json_decode($_POST['select']);
		foreach($_POST['select'] as $data){
			$select_accounts[]=array('id'=>$data->id,'title'=>$data->credit_cards_name.' '.$data->account_type);
		}
		$this->renderPartial('cruisessupplier_edit',array('select_accounts'=>$select_accounts,'data'=>$_POST));
	}

	public function actionAirliners()
	{
		$select_accounts=array();
		$_POST['data']=json_decode($_POST['data']);
		foreach($_POST['data'] as $data){
			$select_accounts[]=array('id'=>$data->id,'title'=>$data->credit_cards_name.' '.$data->account_type);
		}
		$this->renderPartial('airliners',array('select_accounts'=>$select_accounts));
	}

	public function actionAirlinersedit()
	{
		$select_accounts=array();
		$_POST['select']=json_decode($_POST['select']);
		foreach($_POST['select'] as $data){
			$select_accounts[]=array('id'=>$data->id,'title'=>$data->credit_cards_name.' '.$data->account_type);
		}
		$this->renderPartial('airliners_edit',array('select_accounts'=>$select_accounts,'data'=>$_POST));
	}

	public function actionFinancing()
	{
		$select_accounts=array();
		$_POST['data']=json_decode($_POST['data']);
		foreach($_POST['data'] as $data){
			$select_accounts[]=array('id'=>$data->id,'title'=>$data->credit_cards_name.' '.$data->account_type);
		}
		$this->renderPartial('financing',array('select_accounts'=>$select_accounts));
	}

	public function actionFinancingedit()
	{
		$select_accounts=array();
		$_POST['select']=json_decode($_POST['select']);
		foreach($_POST['select'] as $data){
			$select_accounts[]=array('id'=>$data->id,'title'=>$data->credit_cards_name.' '.$data->account_type);
		}
		$this->renderPartial('financing_edit',array('data'=>$_POST,'select_accounts'=>$select_accounts));
	}

	public function actionTypeaccountedit()
	{
		$this->renderPartial('typeaccount_edit',array('data'=>$_POST));
	}

	public function actionTypeaccounteautocomplete($id_entity){

	}

	public function actionTypeaccount()
	{
		$this->renderPartial('typeaccount');
	}

	public function actionCreate($origin)
	{
		$model=new Entity;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Entity']))
		{
			$model->attributes=$_POST['Entity'];
			if($model->save()){
				Utils::vaciarcacheprodimages('uploads/entity/'.$model->id.'/');
				Paymentsentityimages::model()->deleteAll("id_payments_entity=".$model->id);
				if(isset($_POST['image'])){
					foreach($_POST['image'] as $images){
						$images_json=json_decode($images);
						$search_images=new Paymentsentityimages;
						$search_images->id_payments_entity=$model->id;
						$search_images->id_user=Yii::app()->user->id;
						$search_images->ext=$images_json->ext;
						$search_images->filename=$images_json->name;
						$search_images->position=$images_json->position;
						$search_images->title=$images_json->title;
						$search_images->link=$images_json->link;
						$search_images->coords=json_encode($images_json->coords);
						$search_images->order=$images_json->order;
						$search_images->save();
						Utils::mover_directorio($model->id,$images_json->name);
					}
				}
				if(!is_dir('uploads/entity')){
					mkdir('uploads/entity');
				}
				if(!is_dir('uploads/entity/'.$model->id)){
					mkdir('uploads/entity/'.$model->id);
				}
				if(!is_dir('uploads/entity/'.$model->id.'/'.$model->filename)){
					mkdir('uploads/entity/'.$model->id.'/'.$model->filename);
				}
				copy('uploads/tmp/'.Yii::app()->user->id.'/'.$model->filename.'/'.$model->filename.'.'.$model->ext,'uploads/entity/'.$model->id.'/'.$model->filename.'/'.$model->filename.'.'.$model->ext);
				Utils::vaciarcacheimages('uploads/tmp/'.Yii::app()->user->id.'/');
				if(isset($_POST['accounttype'])){
					$accounttype=json_decode($_POST['accounttype']);
					$account_insert=array();
					Paymentsentityaccounttype::model()->deleteAll("id_entity=".$model->id);
					foreach($accounttype as $at){
						$accounttype_entity=new Paymentsentityaccounttype;
						$accounttype_entity->id_entity=$model->id;
						$accounttype_entity->id_credit_cards=$at->id_credit_cards;
						$accounttype_entity->account_type=$at->account_type;
						$accounttype_entity->id_user=Yii::app()->user->id;
						$accounttype_entity->save();
						$account_insert[]=array('id_new'=>$at->id,'id'=>$accounttype_entity->id);
					}
				}
				if(isset($_POST['financing'])){
					$financing=json_decode($_POST['financing']);
					Paymentsentityfinancing::model()->deleteAll("id_entity=".$model->id);
					foreach($financing as $fn){
						$financing_entity=new Paymentsentityfinancing;
						$financing_entity->id_entity=$model->id;
						$financing_entity->amount_dues=$fn->amount_dues;
						$financing_entity->interests=$fn->interests;
						$financing_entity->id_user=Yii::app()->user->id;
						$financing_entity->cftna=$fn->cftna;
						$financing_entity->posnet_cost=$fn->posnet_cost;
						$financing_entity->rewards=$fn->rewards;
						$financing_entity->legal=$fn->legal;
						$financing_entity->date_validity_start=$fn->date_validity_start;
						$financing_entity->date_validity_end=$fn->date_validity_end;
						$financing_entity->save();
						foreach($fn->account_type as $account_type_relacional){
							$account_entity_relational=new Paymentsentityfinancingrelational;
							$account_entity_relational->id_payments_entity_financing=$financing_entity->id;
							foreach($account_insert as $acin){
								if($acin['id_new']==$account_type_relacional->id){
									$account_entity_relational->id_payments_entity_account_type=$acin['id'];
								}
							}
							$account_entity_relational->id_user=Yii::app()->user->id;
							$account_entity_relational->save();
						}
						foreach($fn->services as $financing_services){
							$financing_services_relational=new Paymentsentityfinancingservices;
							$financing_services_relational->id_payments_entity_financing=$financing_entity->id;
							$financing_services_relational->id_product=$financing_services->id;
							$financing_services_relational->save();
						}
					}
				}
				if(isset($_POST['airliners'])){
					$airliners=json_decode($_POST['airliners']);
					Paymentsentityairliners::model()->deleteAll("id_entity=".$model->id);
					foreach($airliners as $fn){
						$airliners_entity=new Paymentsentityairliners;
						$airliners_entity->id_entity=$model->id;
						$airliners_entity->amount_dues=$fn->amount_dues;
						$airliners_entity->interests=$fn->interests;
						$airliners_entity->id_user=Yii::app()->user->id;
						$airliners_entity->cftna=$fn->cftna;
						$airliners_entity->posnet_cost=$fn->posnet_cost;
						$airliners_entity->rewards=$fn->rewards;
						$airliners_entity->date_validity_start=$fn->date_validity_start;
						$airliners_entity->date_validity_end=$fn->date_validity_end;
						$airliners_entity->legal=$fn->legal;
						$airliners_entity->save();
						foreach($fn->airliners as $airliners_relacional){
							$airliners_entity_relational=new Paymentsentityairlinersrelational;
							$airliners_entity_relational->id_payments_entity_airliners=$airliners_entity->id;
							$airliners_entity_relational->id_airliner=$airliners_relacional->id;
							$airliners_entity_relational->id_user=Yii::app()->user->id;
							$airliners_entity_relational->save();
						}
						foreach($fn->account_type as $airliners_account_type_relacional){
							$airliners_entity_relational_account=new Paymentsentityairlinersaccounttype;
							$airliners_entity_relational_account->id_payments_entity_airliners=$airliners_entity->id;
							foreach($account_insert as $acin){
								if($acin['id_new']==$airliners_account_type_relacional->id){
									$airliners_entity_relational_account->id_payments_entity_account_type=$acin['id'];
								}
							}
							$airliners_entity_relational_account->save();
						}
					}
				}
				if(isset($_POST['cruisessupplier'])){
					$cruisessupplier=json_decode($_POST['cruisessupplier']);
					Paymentsentitycruisessupplier::model()->deleteAll("id_entity=".$model->id);
					foreach($cruisessupplier as $fn){
						$cruisessupplier_entity=new Paymentsentitycruisessupplier;
						$cruisessupplier_entity->id_entity=$model->id;
						$cruisessupplier_entity->amount_dues=$fn->amount_dues;
						$cruisessupplier_entity->interests=$fn->interests;
						$cruisessupplier_entity->id_user=Yii::app()->user->id;
						$cruisessupplier_entity->date_validity_start=$fn->date_validity_start;
						$cruisessupplier_entity->date_validity_end=$fn->date_validity_end;
						$cruisessupplier_entity->cftna=$fn->cftna;
						$cruisessupplier_entity->posnet_cost=$fn->posnet_cost;
						$cruisessupplier_entity->rewards=$fn->rewards;
						$cruisessupplier_entity->legal=$fn->legal;
						$cruisessupplier_entity->save();
						foreach($fn->cruisessupplier as $cruisessupplier_relacional){
							$cruisessupplier_entity_relational=new Paymentsentitycruisessupplierrelational;
							$cruisessupplier_entity_relational->id_payments_entity_cruises_supplier=$cruisessupplier_entity->id;
							$cruisessupplier_entity_relational->id_cruises_supplier=$cruisessupplier_relacional->id;
							$cruisessupplier_entity_relational->id_user=Yii::app()->user->id;
							$cruisessupplier_entity_relational->save();
						}

						foreach($fn->account_type as $cruisessupplier_relacional_account){
							$cruisessupplier_entity_relational_account=new Paymentsentitycruisessupplieraccounttype;
							$cruisessupplier_entity_relational_account->id_payments_entity_cruises_supplier=$cruisessupplier_entity->id;
							foreach($account_insert as $acin){
								if($acin['id_new']==$cruisessupplier_relacional_account->id){
									$cruisessupplier_entity_relational_account->id_payments_entity_account_type=$acin['id'];
								}
							}
							$cruisessupplier_entity_relational_account->save();
						}

					}
				}
				$this->redirect(array('//'.$origin.'/admin'));
			}
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	public function actionDisableentity()
	{
		if(isset($_POST['id_entity'])!=''){
			$model=$this->loadModel($_POST['id_entity']);
			$model->enabled=$_POST['status'];
			if($model->save()){
				echo "OK";
			}
		}else{
			echo "NOK";
		}
	}
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Entity']))
		{
			$model->attributes=$_POST['Entity'];
			if($model->save()){
				Utils::vaciarcacheprodimages('uploads/entity/'.$model->id.'/');
				Paymentsentityimages::model()->deleteAll("id_payments_entity=$id");
				if(isset($_POST['image'])){
					foreach($_POST['image'] as $images){
						$images_json=json_decode($images);
						$search_images=new Paymentsentityimages;
						$search_images->id_payments_entity=$model->id;
						$search_images->id_user=Yii::app()->user->id;
						$search_images->ext=$images_json->ext;
						$search_images->filename=$images_json->name;
						$search_images->position=$images_json->position;
						$search_images->title=$images_json->title;
						$search_images->link=$images_json->link;
						$search_images->coords=json_encode($images_json->coords);
						$search_images->order=$images_json->order;
						$search_images->save();
						Utils::mover_directorio($model->id,$images_json->name);
					}
				}
				if(!is_dir('uploads/entity')){
					mkdir('uploads/entity');
				}
				if(!is_dir('uploads/entity/'.$model->id)){
					mkdir('uploads/entity/'.$model->id);
				}
				if(!is_dir('uploads/entity/'.$model->id.'/'.$model->filename)){
					mkdir('uploads/entity/'.$model->id.'/'.$model->filename);
				}
				copy('uploads/tmp/'.Yii::app()->user->id.'/'.$model->filename.'/'.$model->filename.'.'.$model->ext,'uploads/entity/'.$model->id.'/'.$model->filename.'/'.$model->filename.'.'.$model->ext);
				Utils::vaciarcacheimages('uploads/tmp/'.Yii::app()->user->id.'/');
				if(isset($_POST['accounttype'])){
					$accounttype=json_decode($_POST['accounttype']);
					$account_insert=array();
					Paymentsentityaccounttype::model()->deleteAll("id_entity=".$model->id);
					foreach($accounttype as $at){
						$accounttype_entity=new Paymentsentityaccounttype;
						$accounttype_entity->id_entity=$model->id;
						$accounttype_entity->id_credit_cards=$at->id_credit_cards;
						$accounttype_entity->account_type=$at->account_type;
						$accounttype_entity->id_user=Yii::app()->user->id;
						$accounttype_entity->save();
						$account_insert[]=array('id_new'=>$at->id,'id'=>$accounttype_entity->id);
					}
				}
				if(isset($_POST['financing'])){
					$financing=json_decode($_POST['financing']);
					Paymentsentityfinancing::model()->deleteAll("id_entity=".$model->id);
					foreach($financing as $fn){
						$financing_entity=new Paymentsentityfinancing;
						$financing_entity->id_entity=$model->id;
						$financing_entity->amount_dues=$fn->amount_dues;
						$financing_entity->interests=$fn->interests;
						$financing_entity->id_user=Yii::app()->user->id;
						$financing_entity->cftna=$fn->cftna;
						$financing_entity->posnet_cost=$fn->posnet_cost;
						$financing_entity->rewards=$fn->rewards;
						$financing_entity->legal=$fn->legal;
						$financing_entity->date_validity_start=$fn->date_validity_start;
						$financing_entity->date_validity_end=$fn->date_validity_end;
						$financing_entity->save();
						foreach($fn->account_type as $account_type_relacional){
							$account_entity_relational=new Paymentsentityfinancingrelational;
							$account_entity_relational->id_payments_entity_financing=$financing_entity->id;
							foreach($account_insert as $acin){
								if($acin['id_new']==$account_type_relacional->id){
									$account_entity_relational->id_payments_entity_account_type=$acin['id'];
								}
							}
							$account_entity_relational->id_user=Yii::app()->user->id;
							$account_entity_relational->save();
						}
						foreach($fn->services as $financing_services){
							$financing_services_relational=new Paymentsentityfinancingservices;
							$financing_services_relational->id_payments_entity_financing=$financing_entity->id;
							$financing_services_relational->id_product=$financing_services->id;
							$financing_services_relational->save();
						}
					}
				}
				if(isset($_POST['airliners'])){
					$airliners=json_decode($_POST['airliners']);
					Paymentsentityairliners::model()->deleteAll("id_entity=".$model->id);
					foreach($airliners as $fn){
						$airliners_entity=new Paymentsentityairliners;
						$airliners_entity->id_entity=$model->id;
						$airliners_entity->amount_dues=$fn->amount_dues;
						$airliners_entity->interests=$fn->interests;
						$airliners_entity->id_user=Yii::app()->user->id;
						$airliners_entity->cftna=$fn->cftna;
						$airliners_entity->posnet_cost=$fn->posnet_cost;
						$airliners_entity->rewards=$fn->rewards;
						$airliners_entity->date_validity_start=$fn->date_validity_start;
						$airliners_entity->date_validity_end=$fn->date_validity_end;
						$airliners_entity->legal=$fn->legal;
						$airliners_entity->save();
						foreach($fn->airliners as $airliners_relacional){
							$airliners_entity_relational=new Paymentsentityairlinersrelational;
							$airliners_entity_relational->id_payments_entity_airliners=$airliners_entity->id;
							$airliners_entity_relational->id_airliner=$airliners_relacional->id;
							$airliners_entity_relational->id_user=Yii::app()->user->id;
							$airliners_entity_relational->save();
						}
						foreach($fn->account_type as $airliners_account_type_relacional){
							$airliners_entity_relational_account=new Paymentsentityairlinersaccounttype;
							$airliners_entity_relational_account->id_payments_entity_airliners=$airliners_entity->id;
							foreach($account_insert as $acin){
								if($acin['id_new']==$airliners_account_type_relacional->id){
									$airliners_entity_relational_account->id_payments_entity_account_type=$acin['id'];
								}
							}
							$airliners_entity_relational_account->save();
						}
					}
				}
				if(isset($_POST['cruisessupplier'])){
					$cruisessupplier=json_decode($_POST['cruisessupplier']);
					Paymentsentitycruisessupplier::model()->deleteAll("id_entity=".$model->id);
					foreach($cruisessupplier as $fn){
						$cruisessupplier_entity=new Paymentsentitycruisessupplier;
						$cruisessupplier_entity->id_entity=$model->id;
						$cruisessupplier_entity->amount_dues=$fn->amount_dues;
						$cruisessupplier_entity->interests=$fn->interests;
						$cruisessupplier_entity->id_user=Yii::app()->user->id;
						$cruisessupplier_entity->date_validity_start=$fn->date_validity_start;
						$cruisessupplier_entity->date_validity_end=$fn->date_validity_end;
						$cruisessupplier_entity->posnet_cost=$fn->posnet_cost;
						$cruisessupplier_entity->rewards=$fn->rewards;
						$cruisessupplier_entity->cftna=$fn->cftna;
						$cruisessupplier_entity->legal=$fn->legal;
						$cruisessupplier_entity->save();
						foreach($fn->cruisessupplier as $cruisessupplier_relacional){
							$cruisessupplier_entity_relational=new Paymentsentitycruisessupplierrelational;
							$cruisessupplier_entity_relational->id_payments_entity_cruises_supplier=$cruisessupplier_entity->id;
							$cruisessupplier_entity_relational->id_cruises_supplier=$cruisessupplier_relacional->id;
							$cruisessupplier_entity_relational->id_user=Yii::app()->user->id;
							$cruisessupplier_entity_relational->save();
						}

						foreach($fn->account_type as $cruisessupplier_relacional_account){
							$cruisessupplier_entity_relational_account=new Paymentsentitycruisessupplieraccounttype;
							$cruisessupplier_entity_relational_account->id_payments_entity_cruises_supplier=$cruisessupplier_entity->id;
							foreach($account_insert as $acin){
								if($acin['id_new']==$cruisessupplier_relacional_account->id){
									$cruisessupplier_entity_relational_account->id_payments_entity_account_type=$acin['id'];
								}
							}
							$cruisessupplier_entity_relational_account->save();
						}

					}
				}
				$this->redirect(array('admin'));
			}
		}else{
			foreach($model->imagenes as $imagenes){
				Utils::mover_directorio_prod($model->id,$imagenes->filename);
			}
			if($model->filename!=''){
				Utils::mover_directorio_prod($model->id,$model->filename);
			}
		}
		$this->render('update',array(
			'model'=>$model,
		));
	}

	public function actionUpdateorder(){
		$entities = explode(',', $_POST['order']);
		foreach ($entities as $key => $entity) {
			$sql = "UPDATE payments_entity SET display_order = $key where id = $entity";
			$query=Yii::app()->db->createCommand($sql)->execute();
		}
		echo "OK";

	}
	public function actionPublish(){
		$this->layout='pagina_clear';

		if(isset($_POST['hour']) && isset($_POST['date']))
		{
			header('Content-Type: application/json');
			$criteria = new CDbCriteria;
			$criteria->addCondition('enabled=1');
			$model=Entity::model()->findAll($criteria);
			$financing_model=Paymentsentityfinancing::model()->findAll();
			$content=array();
			$date = date_create_from_format('d/m/Y', $_POST['date']);
			$publication_date=date_format($date,'Y-m-d').' '.$_POST['hour'].':00';
			$criteria = new CDbCriteria;
			$id_menu=Yii::app()->user->id_menu();
			$criteria->addCondition("id_menu=$id_menu");
			$images_sizes=Configimages::model()->findAll($criteria);


			foreach($model as $entity){
				$array_imagenes=array();
				foreach($entity->imagenes as $imagenes){
					//IMAGEN ORIGINAL
					/*$array_imagenes[]=array(
						'title'=>$imagenes->title,
						'folder'=>'entity',
						'filename'=>'http://'.$_SERVER['SERVER_NAME'].'/'.Yii::app()->request->baseUrl.'/uploads/entity/'.$entity->id.'/'.$imagenes->filename.'/'.$imagenes->filename,
						'ext'=>$imagenes->ext
					);*/
					foreach($images_sizes as $sizes){
						$array_imagenes[]=array(
							'title'=>$imagenes->title,
							'width'=>$sizes->width,
							'transition_interval'=>$sizes->transition_interval,
							'folder'=>'entity',
							'height'=>$sizes->height,
							'filename'=>'http://'.$_SERVER['SERVER_NAME'].'/'.Yii::app()->request->baseUrl.'/uploads/entity/'.$entity->id.'/'.$imagenes->filename.'/'.$imagenes->filename.'_'.$sizes->width.'x'.$sizes->height,
							'ext'=>$imagenes->ext
						);
					}
				}
				if($entity->filename!=''){
					$images_entity=array(
						array(
							'filename'=>'http://'.$_SERVER['SERVER_NAME'].Yii::app()->request->baseUrl.'/uploads/entity/'.$entity->id.'/'.$entity->filename.'/'.$entity->filename,
							'ext'=>$entity->ext,
							'folder'=>'entity',
							'title'=>strtolower($entity->title)
						),
					);
				}else{
					$images_entity=array();
				}
				$financing_value=array();
				foreach($entity->financing as $financing){
					$financing_selected=array();
					foreach($financing->financing as $financing_select){
						$financing_selected[]=(object) array('id'=>$financing_select->id_payments_entity_account_type,'title'=>$financing_select->accountype->credito->name.' '.$financing_select->accountype->account_type);
					}
					$services_selected=array();
					foreach($financing->services as $service_select){
						$services_selected[]=(object) array('id'=>$service_select->id_product,'title'=>$service_select->product->title);
					}
					$financing_value[]= (object) array(
						'id'=>$financing->id,
						'account_type' =>$financing_selected,
						'services' =>$services_selected,
						'amount_dues' => $financing->amount_dues,
						'interests' => $financing->interests,
						'cftna' => $financing->cftna,
						'legal' => $financing->legal,
						'date_validity_start' => $financing->date_validity_start,
						'date_validity_end' => $financing->date_validity_end,
						'rewards' => $financing->rewards,
					  );
				}
				$airliners_value=array();
				foreach($entity->airliners as $airliners){
					$airliners_selected=array();
					foreach($airliners->airliners as $airliners_select){
						$airliners_selected[]=(object) array('id'=>$airliners_select->id_airliner,'title'=>$airliners_select->airliners->title);
					}
					$services_selected=array();
					foreach($airliners->services as $service_select){
						$services_selected[]=(object) array('id'=>$service_select->id_payments_entity_account_type,'title'=>$service_select->accountype->credito->name.' '.$service_select->accountype->account_type);
					}
					$airliners_value[]= (object) array(
						'id'=>$airliners->id,
						'airliners' =>$airliners_selected,
						'amount_dues' => $airliners->amount_dues,
						'interests' => $airliners->interests,
						'cftna' => $airliners->cftna,
						'account_type' =>$services_selected,
						'legal' => $airliners->legal,
						'date_validity_start' => $airliners->date_validity_start,
						'date_validity_end' => $airliners->date_validity_end,
						'rewards' => $airliners->rewards,
					  );
				}
				$cruisessupplier_value=array();
				foreach($entity->cruisessupplier as $cruisessupplier){
					$cruisessupplier_selected=array();
					foreach($cruisessupplier->cruisessupplier as $cruisessupplier_select){
						$cruisessupplier_selected[]=(object) array('id'=>$cruisessupplier_select->id_cruises_supplier,'title'=>$cruisessupplier_select->cruisessupplier->title);
					}
					$services_selected=array();
					$services_list='';
					foreach($cruisessupplier->services as $service_select){
						$services_selected[]=(object) array('id'=>$service_select->id_payments_entity_account_type,'title'=>$service_select->accountype->credito->name.' '.$service_select->accountype->account_type);
					}
					$cruisessupplier_value[]= (object) array(
						'id'=>$cruisessupplier->id,
						'cruisessupplier' =>$cruisessupplier_selected,
						'amount_dues' => $cruisessupplier->amount_dues,
						'interests' => $cruisessupplier->interests,
						'cftna' => $cruisessupplier->cftna,
						'account_type' =>$services_selected,
						'legal' => $cruisessupplier->legal,
						'date_validity_start' => $cruisessupplier->date_validity_start,
						'date_validity_end' => $cruisessupplier->date_validity_end,
						'rewards' => $cruisessupplier->rewards,
					  );
				}
				array_push($content,array(
					'imagenes'=>$images_entity,
					'banners'=>$array_imagenes,
					'financing'=>$financing_value,
					'airliners'=>$airliners_value,
					'cruisessupplier'=>$cruisessupplier_value,
					'entity_highlight'=>$entity->entity_highlight,
					'title'=>$entity->title,
					'expire_date_from'=>$entity->expire_date_from,
					'expire_date_to'=>$entity->expire_date_to,
					'enabled'=>$entity->enabled
					)
				);
			}
			
			$financing_entity = Utils::getPaymentFull();
			$max_cuotas = Utils::getMaxCuotas();
			$publication_app_json = Publish::Run(array('appjson'),'financing',array(),array(),$_POST['date'],$_POST['hour'],$financing_entity,array());
			$publication_cdn=Publish::Run(array('cdn'),'entity',array(),array(),$_POST['date'],$_POST['hour'],$content,array("imagenes","banners"));

			$publication_app=Publish::Run(array('app'),'payments_entity',array(),array(),$_POST['date'],$_POST['hour'],$model,array());
			$publication_cdn_max_cuotas = Publish::Run(array('cdn'),'max_cuotas',array(),array(),$_POST['date'],$_POST['hour'],$max_cuotas,array());

		}else{
			$this->render('publish',array('title'=>Yii::app()->user->Title_nocss(),'breadcrumb'=>Yii::app()->user->Breadcrumb_nocss()));
		}
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Entity');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		//var_dump('expression');die;
		$this->loadJQuery=false;
		$model=new Entity('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Entity'])){
			$model->attributes=$_GET['Entity'];
		}

		if(isset($_GET['criterio'])){
			$model->criterio=$_GET['criterio'];
		}

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Entity the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Entity::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Entity $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='entity-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

	public function actionFilterentities($id_subchannel=null)
	{
		$criteria = new CDbCriteria;
		//$criteria->addCondition("id_channel=$id_channel");
		if(!empty($id_subchannel)){
			$criteria->addCondition("id_subchannel=$id_subchannel");
			$model_subchannel=Subchannelsentity::model()->findAll($criteria);
			if(!empty($model_subchannel)){
				foreach($model_subchannel as $entity){
				$model=Entity::model()->findByPk($entity->id_entity);
				echo '<option value="'.$model->id.'">'.$model->title.'</option>';
				}
			}else{
				$model=Entity::model()->findAll();
				foreach($model as $entity){
					echo '<option value="'.$entity->id.'">'.$entity->title.'</option>';
				}
			}
		}else{
			$model=Entity::model()->findAll();
			foreach($model as $entity){
				echo '<option value="'.$entity->id.'">'.$entity->title.'</option>';
			}
		}
	}



}
