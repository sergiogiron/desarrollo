<?php

class ProfileController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='pagina';

	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}
	
	public function accessRules()
	{
		$internal=array('assignroles','assignrolesedit');
		return array(
			array('deny', //no entra a ninguna accion ningun usuario que no este logueado               
                'users'=>array('?'),
            ),
            array('allow', // acceden todos los usuarios autenticados a todas las acciones
				'actions'=>array_merge(Yii::app()->user->getRules(),$internal),
				'users'=>array('@'),
            ),
			 array('deny',
                'users'=>array('*'),
            ),		
        );	
	}
	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}
	
	public function actionAssignroles()
	{
		$this->layout='pagina_clear';
		$this->render('assign_roles');
	}	
	
	public function actionAssignrolesedit()
	{
		$this->layout='pagina_clear';
		$this->render('assign_roles_edit');
	}	
	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Profile;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Profile']))
		{
			$model->attributes=$_POST['Profile'];
			$model->created_at=date("Y-m-d H:i:s");
			$model->id_user=Yii::app()->user->id;
			$model->enabled=1;			
			if($model->save()){		
				$roles=json_decode($_POST['result_roles']);
				foreach($roles as $rl){
					if($rl->type==1){
						$menu=new Profilemenurelation;
						$menu->id_menu=$rl->name;
						$menu->id_profile=$model->id;
						$menu->id_user=Yii::app()->user->id;
						$menu->created_at=date("Y-m-d H:i:s");
						$menu->save();						
						foreach($rl->roles as $permisos){
							$permisosroles=new Profilemenurelationrole;
							$permisosroles->id_profile_menu_relation=$menu->id;
							$permisosroles->id_role=$permisos;
							$permisosroles->id_user=Yii::app()->user->id;
							$permisosroles->created_at=date("Y-m-d H:i:s");
							$permisosroles->save();
						}
					}else{
						$widget=new Profilewidgetrelation;
						$widget->id_widget=$rl->name;
						$widget->id_profile=$model->id;
						$widget->id_user=Yii::app()->user->id;
						$widget->created_at=date("Y-m-d H:i:s");
						$widget->save();						
						foreach($rl->roles as $permisos){
							$permisosroles=new Profilewidgetrelationrole;
							$permisosroles->id_profile_widget_relation=$widget->id;
							$permisosroles->id_role=$permisos;
							$permisosroles->id_user=Yii::app()->user->id;
							$permisosroles->created_at=date("Y-m-d H:i:s");
							$permisosroles->save();
						}				
					}
				}		
				$this->redirect(array('/perfiles'));
			}				
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Profile']))
		{
			$model->attributes=$_POST['Profile'];
			$model->created_at=date("Y-m-d H:i:s");
			$model->id_user=Yii::app()->user->id;
			$model->enabled=1;			
			if($model->save()){
				$criteria = new CDbCriteria();
				$criteria->condition = "id_profile =$id";
				$profilerelacionados = Profilemenurelation::model()->findAll($criteria);
				foreach($profilerelacionados as $pfr){
					$id_relational_role=$pfr->id;
					Profilemenurelationrole::model()->deleteAll("id_profile_menu_relation=$id_relational_role");
					$pfr->delete();
				}
				$criteria = new CDbCriteria();
				$criteria->condition = "id_profile =$id";
				$profilerelacionados = Profilewidgetrelation::model()->findAll($criteria);
				foreach($profilerelacionados as $pfr){
					$id_relational_role=$pfr->id;
					Profilewidgetrelationrole::model()->deleteAll("id_profile_widget_relation=$id_relational_role");
					$pfr->delete();
				}
				$roles=json_decode($_POST['result_roles']);
				foreach($roles as $rl){
					if($rl->type==1){
						$menu=new Profilemenurelation;
						$menu->id_menu=$rl->name;
						$menu->id_profile=$model->id;
						$menu->id_user=Yii::app()->user->id;
						$menu->created_at=date("Y-m-d H:i:s");
						$menu->save();						
						foreach($rl->roles as $permisos){
							$permisosroles=new Profilemenurelationrole;
							$permisosroles->id_profile_menu_relation=$menu->id;
							$permisosroles->id_role=$permisos;
							$permisosroles->id_user=Yii::app()->user->id;
							$permisosroles->created_at=date("Y-m-d H:i:s");
							$permisosroles->save();
						}						
					}else{
						$widget=new Profilewidgetrelation;
						$widget->id_widget=$rl->name;
						$widget->id_profile=$model->id;
						$widget->id_user=Yii::app()->user->id;
						$widget->created_at=date("Y-m-d H:i:s");
						$widget->save();						
						foreach($rl->roles as $permisos){
							$permisosroles=new Profilewidgetrelationrole;
							$permisosroles->id_profile_widget_relation=$widget->id;
							$permisosroles->id_role=$permisos;
							$permisosroles->id_user=Yii::app()->user->id;
							$permisosroles->created_at=date("Y-m-d H:i:s");
							$permisosroles->save();
						}				
					}
				}			
				$this->redirect(array('/perfiles'));
			}		
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Profile');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Profile('search');
		$this->loadJQuery=false;
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Profile'])){
			$model->attributes=$_GET['Profile'];
		}
		if(isset($_GET['criterio'])){
			$model->criterio=$_GET['criterio'];
		}			

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Profile the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Profile::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Profile $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='profile-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
