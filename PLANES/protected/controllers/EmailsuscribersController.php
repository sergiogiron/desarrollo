<?php

class EmailsuscribersController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='pagina';

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	
	public function filters(){
		return array('accessControl', // perform access control for CRUD operations	
		);		
	}
	public function accessRules(){

		return array(
			array('allow',
                'actions'=>array('unsubscribemail'),
            ),
			array('deny', //no entra a ninguna accion ningun usuario que no este logueado
			'actions'=>array('*'),
			'users'=>array('?'),
			),
			array('allow', // acceden todos los usuarios autenticados a todas las acciones
			'users'=>array('@'),
			),
			array('deny',
			'users'=>array('*'),
			),
		); 
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}
	
	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Emailsuscribers;

		if(isset($_POST['Emailsuscribers']))
		{
			$model->attributes=$_POST['Emailsuscribers'];
			$model->created_at=date("Y-m-d H:i:s");
			$model->id_user=Yii::app()->user->id;
			if($model->save()){
				$this->redirect(array('contactos'));
			}
		}
		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		if(isset($_POST['Emailsuscribers']))
		{
			$model->attributes=$_POST['Emailsuscribers'];
			if($model->save()){
				$this->redirect(array('contactos'));
			}			
		}
		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Emailsuscribers');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$this->loadJQuery=false;
		
		$idUsuarioActual=Yii::app()->user->id;
		$usuarioActual=Users::model()->findByPk($idUsuarioActual);
 
		$usuariosAdministradores=Emailassistant::model()->findAllByAttributes(array('id_assistant'=>$idUsuarioActual));
		if(!empty($usuariosAdministradores)){
			foreach($usuariosAdministradores as $key => $val ){
				foreach($val as $k => $v ){ 
					if($k=='id_user'){					
						$usuariosAdministradores2=Users::model()->findByAttributes(array('id'=>$v));
						$usersAdmin[]=$usuariosAdministradores2->username;
					}
				}
			}
		}
		else{
			$usersAdmin[]="";
			}

		$urlFinal=Yii::app()->params['contacts_api_url'].'/Contacto/Listado';
		 
		$data=array(
				'UserName'=>$usuarioActual->username,
				'AdminUsers'=> $usersAdmin,		 
			);	
		
		$opts = array('http' =>
			array(
				'method'  => 'POST',
				'ignore_errors' => true,
				'header'=>"Content-Type: application/json\r\n",				
				'content'=>json_encode($data)
			)
		);
		
		$context  = stream_context_create($opts);
		$url = $urlFinal;
		if($json = @file_get_contents($url, false,$context)){
			$json=json_decode($json);
		}
		
		$array_contactos = array();
		$contactos_totales=0;
		$contactos_suscriptos=0;
		$contactos_desuscriptos=0;
		$contactos_invalidos=0;
		$contactos_bloqueados=0;
		$contactos_desuscriptosxusr=0;
		$arrayContadores = [];
		 
		Yii::import('application.controllers.EmailcampaignController');
		$domains = EmailcampaignController::getBannedDomains();
		#var_dump($domains);die;
		$bodyJson = $json->data; 
		foreach ($bodyJson as $key => $value) {
			$idContacto = $value->IdContacto;
			$nombre = $value->Nombre;
			//$apellido = $value->Apellido;
			$email = $value->Email;
			$suscripto = $value->Suscripto;
			$compartido = $value->Compartido;
			$admin = $value->Admin;
			$nombreAdmin = $value->NombreAdmin;
			$emailAdmin = $value->EmailAdmin;
			foreach($domains as $key => $val){
				if(strpos($email,$val)){
				$esProhibido=true;}
			else{$esProhibido=false;}}
			if(empty($esProhibido))$esProhibido=false;
			$array = array('id'=>$idContacto,'nombre'=>$nombre, 'email'=>$email,'suscripto'=>$suscripto,'compartido'=>$compartido,'admin'=>$admin,'nombreAdmin'=>$nombreAdmin,'emailAdmin'=>$emailAdmin, 'prohibido'=>$esProhibido);
			array_push($array_contactos, $array);
			
			if(strcasecmp($admin, $usuarioActual->username) == 0)
			{
				//contadores
				$contactos_totales++;
				switch($suscripto){
					case 0:
						$contactos_suscriptos++;
						break;
					case 1:
						$contactos_desuscriptos++;
						break;
					case 2:
						$contactos_invalidos++;
						break;
					case 3:
						$contactos_bloqueados++;
						break;
					case 4:
						$contactos_desuscriptosxusr++;
						break;
				}
			}
			else
			{
				 if (isset($arrayContadores[$admin]['contactos_totales']))
					{
						$arrayContadores[$admin]['contactos_totales']++;
					} else {
						$arrayContadores[$admin]['contactos_totales']=1;
					}
				switch($suscripto){
					case 0:
						 if (isset($arrayContadores[$admin]['contactos_suscriptos']))
						{
							$arrayContadores[$admin]['contactos_suscriptos']++;
						} else {
							$arrayContadores[$admin]['contactos_suscriptos']=1;
							}
						break;
					case 1:
						if (isset($arrayContadores[$admin]['contactos_desuscriptos']))
						{
							$arrayContadores[$admin]['contactos_desuscriptos']++;
						} else {
							$arrayContadores[$admin]['contactos_desuscriptos']=1;
							}
						break;
					case 2:
						if (isset($arrayContadores[$admin]['contactos_invalidos']))
						{
							$arrayContadores[$admin]['contactos_invalidos']++;
						} else {
							$arrayContadores[$admin]['contactos_invalidos']=1;
							}
						break;
					case 3:
						if (isset($arrayContadores[$admin]['contactos_bloqueados']))
						{
							$arrayContadores[$admin]['contactos_bloqueados']++;
						} else {
							$arrayContadores[$admin]['contactos_bloqueados']=1;
							}
						break;
					case 4:
						if (isset($arrayContadores[$admin]['contactos_desuscriptosxusr']))
						{
							$arrayContadores[$admin]['contactos_desuscriptosxusr']++;
						} else {
							$arrayContadores[$admin]['contactos_desuscriptosxusr']=1;
							}
						break;
				}
			}
		}
		$arrayBk=$array_contactos;
		if(isset($_GET['criterio']) && $_GET['criterio']!=''){
			$array_criterio = array();
			foreach($array_contactos as $key => $val){
				if (strpos($val['nombre'], $_GET['criterio']) !== false || strpos($val['email'], $_GET['criterio']) !== false) {
					$array = array('id'=>$val['id'],'nombre'=>$val['nombre'], 'email'=>$val['email'],'suscripto'=>$val['suscripto'],'compartido'=>$val['compartido'],'admin'=>$val['admin'],'nombreAdmin'=>$val['nombreAdmin'],'emailAdmin'=>$val['emailAdmin'],'prohibido'=>$val['prohibido']);
					array_push($array_criterio, $array);
				}
			}			
			$array_contactos=$array_criterio;
		}
		
		$this->render('admin',array(
			 'contactos'=>$array_contactos,
			 'contactos_totales'=>$contactos_totales,
			 'contactos_suscriptos'=>$contactos_suscriptos,
			 'contactos_desuscriptos'=>$contactos_desuscriptos,
			 'contactos_invalidos'=>$contactos_invalidos,
			 'contactos_bloqueados'=>$contactos_bloqueados,
			 'contactos_desuscriptosxusr'=>$contactos_desuscriptosxusr,
			 'array_contadores'=>$arrayContadores,
		));
	}
	
	public function actionSubscribe($id)
	{
		$username = Yii::app()->user->username;
		$opts = array('http' =>
            array(
                'method' => 'GET',
                'ignore_errors' => false,		
				'header'=>"Content-Type: application/json\r\n",					
            )
        );
		
        $context  = stream_context_create($opts);		
		
		$url=Yii::app()->params['contact_getmail_api_url'].'/'.$id;
		if($json = @file_get_contents($url,false,$context)){
			$json=json_decode($json,1);
		}	
		if(!empty($json['status']) && $json['status']=='Ok')
		{
			 $data= $json['data'];
			 $email=$data['Email'];
			
			$opts = array('http' =>
				array(
					'method' => 'PUT',
					'ignore_errors' => false,		
					'header'=>"Content-Type: application/json\r\n",		 
					'header'=>"Content-length: ".strlen(''),				
				)
			);
			$context  = stream_context_create($opts);		
			
			$url=Yii::app()->params['contacts_api_url'].'/Contacto/Suscribir/'.$id.'/'.$username;
			if($json = @file_get_contents($url,false,$context)){
				$json=json_decode($json,1);
			}
			
			if(!empty($json['status']) && $json['status']=='Ok'){	
				echo $json['msg'];			
			}
			else{
				echo 'Ocurrió un error grabando en Terrena';
			}
		}
	}

	public function actionUnsubscribe($id)
	{
		$username = Yii::app()->user->username;
		$opts = array('http' =>
            array(
                'method' => 'GET',
                'ignore_errors' => false,		
				'header'=>"Content-Type: application/json\r\n",					
            )
        );
		
        $context  = stream_context_create($opts);		
		
		$url=Yii::app()->params['contact_getmail_api_url'].'/'.$id;
		if($json = @file_get_contents($url,false,$context)){
			$json=json_decode($json,1);
		}
		
		if(!empty($json['status']) && $json['status']=='Ok')
		{
			 $data= $json['data'];
			 $email=$data['Email'];
			
			
			$opts = array('http' =>
				array(
					'method'  => 'PUT',
					'ignore_errors' => false,
					'header'=>"Content-Type: application/json\r\n",		 
					'header'=>"Content-length: ".strlen(''),	
				)
			);
			$context  = stream_context_create($opts);		
			$url=Yii::app()->params['contacts_api_url'].'/Contacto/Desuscribir/'.$id.'/'.$username; 		
			$json = file_get_contents($url,false,$context); 
			$json=json_decode($json,true); 
			
			if($json['status']=='Ok'){		
				echo $json['msg'];	
			}
			else{
				echo 'Ocurrió un error grabando en Terrena';
			}
		}
	}

	public function actionShare($id)
	{
		$opts = array('http' =>
            array(
                'method'  => 'PUT',
                'ignore_errors' => true,
				'header'=>"Content-Type: application/json\r\n",		 
				'header'=>"Content-length: ".strlen(''),
            )
        );
        $context  = stream_context_create($opts);		
		$url=Yii::app()->params['contacts_api_url'].'/Contacto/Compartir/'.$id.'/'.Yii::app()->user->username;
		if($json = @file_get_contents($url,false,$context)){
			$json=json_decode($json);
			echo ($json->msg);
		}
		else{
			echo 'Ocurrió un error compartiendo el contacto.';
		}
		
	}
	
	public function actionUnshare($id)
	{
		$opts = array('http' =>
            array(
                'method'  => 'PUT',
                'ignore_errors' => true,
				'header'=>"Content-Type: application/json\r\n",		 
				'header'=>"Content-length: ".strlen(''),
            )
        );
        $context  = stream_context_create($opts);		
		$url=Yii::app()->params['contacts_api_url'].'/Contacto/Ocultar/'.$id.'/'.Yii::app()->user->username;
		if($json = @file_get_contents($url,false,$context)){
			$json=json_decode($json);
		echo ($json->msg);
		}
		 else{
			 echo 'Ocurrió un error ocultando el contacto.';
		 }
	}
	
	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Emailsuscribers the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Emailsuscribers::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Emailsuscribers $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='email-suscribers-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	
	
	
	public function actionUnsubscribemail($email)
	{
			$opts = array('http' =>
				array(
					'method'  => 'PUT',
					'ignore_errors' => false,
					'header'=>"Content-Type: application/json\r\n",		 
					'header'=>"Content-length: ".strlen(''),	
				)
			);
			$context  = stream_context_create($opts);		
			$url=Yii::app()->params['contacts_api_url'].'/Contacto/Desuscribir/'.$email; 		
			if($json = @file_get_contents($url,false,$context)){
				$json=json_decode($json,true); 
			}
			if(!empty($json['status']) && $json['status']=='Ok'){
				echo $json['msg'];
			}
			else{
				echo 'Ocurrió un error.';
			}
	}
}
