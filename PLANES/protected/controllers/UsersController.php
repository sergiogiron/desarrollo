<?php
class UsersController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='pagina';
	
	public function filters()
	{		
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}
	
	public function accessRules()
	{
		return array(
			array('deny', //no entra a ninguna accion ningun usuario que no este logueado               
                'users'=>array('?'),
            ),
            array('allow', // acceden todos los usuarios autenticados a todas las acciones
				'actions'=>Yii::app()->user->getRules(),
				'users'=>array('@'),
            ),
			 array('deny',
                'users'=>array('*'),
            ),		
        );	
	}
	
	public function actionView($id)
	{
		$usuarios=Users::model()->findAll();
		$model=$this->loadModel($id);
		$usuarios_selected=$model->top_boss;
		foreach($usuarios as $key=>$user){
			$username=$user['username'];
			$usuarios_aux[$username]=$user->lastname.', '.$user->name;
		}
		$this->render('view',array(
			'model'=>$model,'usuarios'=>$usuarios_aux, 'usuarios_selected'=>$usuarios_selected,
		));
	}

	public function actionCreate()
	{
		$model=new Users;
		$usuarios=Users::model()->findAll();
		$usuarios_aux=[];
		$usuarios_selected=null;
		foreach($usuarios as $key=>$user){
			$username=$user['username'];
			$usuarios_aux[$username]=$user->lastname.', '.$user->name;
		}
		if(isset($_POST['Users']))
		{
			if(!empty($_POST['Users']['top_boss'])){$usuarios_selected=$_POST['Users']['top_boss'];}
			$model->attributes=$_POST['Users'];
			$model->created_at=date("Y-m-d H:i:s");
			$model->id_user=Yii::app()->user->id;
			if($model->doc_number!=''){
				$model->password=md5($model->doc_number);
			}
			$model->enabled=1;
			/*if($_FILES['Users']['name']['image']!=''){
				$rnd = rand(0,9999);
				$uploadedFile=CUploadedFile::getInstance($model,'image');
				$fileName = md5("{$rnd}").'.jpg';
				$model->image = $fileName;	
			}*/
			if($model->save()){
				 
			#if(0){
				/*if($model->image!=''){
					$uploadedFile->saveAs(Yii::app()->basePath.'/../uploads/users/'.$fileName); 
				}*/			
				if(!empty($_POST['image']) && count($_POST['image'])>0){
					foreach($_POST['image'] as $images){
						$images_json=json_decode($images);
						$user_images=new Usersimages;
						$user_images->id_user=$model->id;
						$user_images->ext=$images_json->ext;
						$user_images->filename=$images_json->name;
						$user_images->position=$images_json->position;
						$user_images->title=$images_json->title;
						$user_images->link=$images_json->link;
						$user_images->order=$images_json->order;
						$user_images->coords=json_encode($images_json->coords);
						$user_images->save();
						Utils::mover_directorio($model->id,$images_json->name);
					}					
					Utils::vaciarcacheimages('uploads/tmp/'.Yii::app()->user->id.'/');
				}				
				$this->redirect(array('/usuarios'));
			}
				
		}

		$this->render('create',array(
			'model'=>$model,
			'usuarios'=>$usuarios_aux,
			'usuarios_selected'=>$usuarios_selected,
		));
	}
	
	
	
	public function actionUpdate($id)
	{		
		$model=$this->loadModel($id);
		$usuarios=Users::model()->findAll();
		$usuarios_aux=[];
		$usuarios_selected=$model->top_boss;
		foreach($usuarios as $key=>$user){
			$username=$user['username'];
			$usuarios_aux[$username]=$user->lastname.', '.$user->name;
		}	
		if(isset($_POST['Users']))
		{
			if(!empty($_POST['Users']['top_boss'])){$usuarios_selected=$_POST['Users']['top_boss'];}
			$model->attributes=$_POST['Users'];
			$model->created_at=date("Y-m-d H:i:s");
			$model->id_user=Yii::app()->user->id;
			$model->enabled=1;	
			if($model->doc_number!=''){
				$model->password=md5($model->doc_number);
			}		
			/*if($_FILES['Users']['name']['image']!=''){
				$rnd = rand(0,9999);
				$uploadedFile=CUploadedFile::getInstance($model,'image');
				$fileName = md5("{$rnd}").'.jpg';
				$model->image= $fileName;	
			}else{
				$model->image = $imagen_anterior;
			}*/
			if($model->save()){
				Utils::vaciarcacheprodimages('uploads/users/'.$model->id.'/');
				Usersimages::model()->deleteAll("id_user=$id");
				if(isset($_POST['image'])){
					foreach($_POST['image'] as $images){					
						$images_json=json_decode($images);
						$user_images=new Usersimages;
						$user_images->id_user=$model->id;
						$user_images->position=$images_json->position;
						$user_images->filename=$images_json->name;
						$user_images->ext=$images_json->ext;
						$user_images->title=$images_json->title;
						$user_images->link=$images_json->link;
						$user_images->coords=json_encode($images_json->coords);
						$user_images->order=$images_json->order;
						$user_images->save();
						Utils::mover_directorio($model->id,$images_json->name);
					}					
				}
				Utils::vaciarcacheimages('uploads/tmp/'.Yii::app()->user->id.'/');
				/*if($_FILES['Users']['name']['image']!=''){
					$uploadedFile->saveAs(Yii::app()->basePath.'/../uploads/users/'.$fileName); 
				}*/
				$this->redirect(array('/usuarios'));
			}				
		}else{
			foreach($model->imagenes as $imagenes){
				Utils::mover_directorio_prod($model->id,$imagenes->filename);
			}			
		}
		$this->render('update',array(
			'model'=>$model,
			'usuarios'=>$usuarios_aux,
			'usuarios_selected'=>$usuarios_selected,
		));
	}
	
	public function actionRoles($id)
	{
		$criteria=new CDbCriteria;
		$criteria->condition="id_user=$id";
		$roles=Profileuser::model()->findAll($criteria);
		$usuario=Users::model()->findByPk($id);
		if(isset($_POST['id_profile']))
		{		
			$usuario->username=$_POST['username'];
			$usuario->save();
			
			Profileuser::model()->deleteAll("id_user=$id");
			foreach($_POST['id_profile'] as $perfiles){
				$permisos=new Profileuser;
				$permisos->id_profile=$perfiles;
				$permisos->id_user=$id;
				$permisos->created_at=date("Y-m-d H:i:s");
				$permisos->id_user_assing=Yii::app()->user->id;	
				$permisos->save();		
			}
			$this->redirect(array('/usuarios'));	
		}

		$this->render('roles',array(
			'roles'=>$roles,'usuario'=>$usuario
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Users');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$this->loadJQuery=false;
		$model=new Users('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Users'])){
			$model->attributes=$_GET['Users'];
		}			
		if(isset($_GET['criterio'])){
			$model->criterio=$_GET['criterio'];
		}	
		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Users the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Users::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Users $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='users-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
