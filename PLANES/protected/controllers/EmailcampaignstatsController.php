<?php

class EmailcampaignstatsController extends Controller
{
	public $layout='pagina';
	
	public function filters(){
		return array('accessControl', // perform access control for CRUD operations	
		);		
	}
	
	public function accessRules(){
		return array(
			array('deny', //no entra a ninguna accion ningun usuario que no este logueado
			'actions'=>array('*'),
			'users'=>array('?'),
			),
			array('allow', // acceden todos los usuarios autenticados a todas las acciones
			'users'=>array('@'),
			),
			array('deny',
			'users'=>array('*'),
			), 
		); 
	}
		
	public function actionAdmin($idDatamedia=18599)
	{
		$this->loadJQuery=false;
		$model=new Emailcampaignstats('searchMyCampaigns');
		
		$model->unsetAttributes(); 
		if(isset($_GET['Emailcampaignstats']))
			$model->attributes=$_GET['Emailcampaignstats'];
 
		if(isset($_GET['criterio'])){
			$model->criterio=$_GET['criterio'];
		}

		$this->render('admin',array(
			'model'=>$model,
		)); 
	}	
	
	public function actionView($id)
	{
		//$model=$this->loadModel($id);
		/*
		$model2=$this->loadModel(31);
		$model3=$this->loadModel(32);
		$arr=[];
		array_push($arr,$model2);
		array_push($arr,$model3);	 	 
		$model= new Emailcampaignstats;
		$model=self::acumular_stats($arr); 
		 */
		 if(Yii::app()->request->getParam('export')) {
			$this->actionExport($id);
			Yii::app()->end();
		} 

		if(!Yii::app()->user->checkAccess('marketingadmin')){
			$model=Emailcampaignstats::model()->findByPk($id);
			$id_campa=$model->id_campaign;
			$allstats=Emailcampaignstats::model()->findAllByAttributes(array('id_campaign'=>$id_campa));
			$model= new Emailcampaignstats; 
			$model=self::acumular_stats($allstats); 
		}else{
			$model=$this->loadModel($id);
		}
		
		$device_dev_type=!empty($model->device_dev_type)?json_decode($model->device_dev_type,1):null;
		$device_client2=!empty($model->device_client)?json_decode($model->device_client,1):null; 
		if(!empty($device_client2)){foreach ($device_client2 as $key => $val){if(strtolower($key)=="Web Browser"){$key2="Otros";}else{$key2=ucfirst($key);}$device_client[$key2]=$val;}}else{$device_client=null;}
		$device_browser2=!empty($model->device_browser)?json_decode($model->device_browser,1):null;
		if(!empty($device_browser2)){foreach ($device_browser2 as $key => $val){if(strtolower($key)=="other"){$key2="Otros";}else if((strtolower($key)=="ie")){$key2="Internet Explorer";}else{$key2=ucfirst($key);}$device_browser[$key2]=$val;}}else{$device_browser=null;}
		$device_os2=!empty($model->device_os)?json_decode($model->device_os,1):null;
		if(!empty($device_os2)){foreach ($device_os2 as $key => $val){if(strtolower($key)=="other"){$key2="Otros";}else if((strtolower($key)=="ios")){$key2="iOS";}else if((strtolower($key)=="macintosh")){$key2="Mac";}else{$key2=ucfirst($key);}$device_os[$key2]=$val;}}else{$device_os=null;}
		$reads_data_timelapse=!empty($model->reads_data_timelapse)?json_decode($model->reads_data_timelapse,1):null;
		$reads_data_weekly=!empty($model->reads_data_weekly)?json_decode($model->reads_data_weekly,1):null;
		$reads_data_hourly=!empty($model->reads_data_hourly)?json_decode($model->reads_data_hourly,1):null;
		$open_detail=self::actionGetopendetails($id);
		$open_detail==!empty($open_detail)?json_decode($model->open_detail,1):null;
		
		$campaign_detail=[];
		foreach($allstats as $stat){
			//print_r($stat);die;
			$owner=Users::model()->findByAttributes(array('id'=>$stat['id_owner']));						
			$seller_detail=[];
			if(!empty($owner['name']) && !empty($owner['lastname'])){
				$seller_detail['NombreVendedor']=$owner['name'].' '.$owner['lastname'];
			}else{
				$seller_detail['NombreVendedor']=$owner['username'];
			}
			$seller_detail['Vendedor']=$stat['from_mail'];
			$seller_detail['Suscriptores']=number_format($stat['subscribers'],0,",",".");
			#$seller_detail['Suscriptores']=$stat['subscribers'];						
			$seller_detail['AperturasUnicas']=number_format($stat['reads_val'],0,",",".");
			#$seller_detail['AperturasUnicas']=$stat['reads_val'];
			$seller_detail['AperturasTotales']=number_format($stat['reads_total'],0,",",".");
			#$seller_detail['AperturasTotales']=$stat['reads_total'];
			$seller_detail['ClicksUnicos']=number_format($stat['clicks_unique'],0,",",".");
			#$seller_detail['ClicksUnicos']=$stat['clicks_unique'];
			$seller_detail['ClicksTotales']=number_format($stat['clicks_total'],0,",",".");
			#$seller_detail['ClicksTotales']=$stat['clicks_total'];
			$seller_detail['Rechazados']=number_format($stat['bounced'],0,",",".");
			#$seller_detail['Rechazados']=$stat['bounced'];
			$seller_detail['Invalidos']=number_format($stat['bounced_invalid'],0,",",".");
			#$seller_detail['Invalidos']=$stat['bounced_invalid'];
			array_push($campaign_detail,$seller_detail);
			//print_r($seller_detail);die;	
		}
		//var_dump($campaign_detail);
		//die;
		$this->render('view',array(
			'model'=>$model,
			'open_details'=>$open_detail,
			'device_dev_type'=>$device_dev_type,
			'device_client'=>$device_client,
			'device_browser'=>$device_browser,
			'device_os'=>$device_os,
			'reads_data_timelapse'=>$reads_data_timelapse,
			'reads_data_weekly'=>$reads_data_weekly,
			'reads_data_hourly'=>$reads_data_hourly,
			'campaign_detail'=>$campaign_detail,
		));
	}
	
	public function actionExport($id)
	{
		//print_r(is_numeric("1.000"));die;
		Yii::import('ext.ECSVExport');
		$allstats=Emailcampaignstats::model()->findAllByAttributes(array('id_campaign'=>$id));
		$campaign_detail=[];
		$seller_detail['Vendedor']="Vendedor";
		$seller_detail['EmailVendedor']="Email Vendedor";
		$seller_detail['Suscriptores']="Suscriptores";
		$seller_detail['AperturasUnicas']="Aperturas Unicas";
		$seller_detail['AperturasPorcentual']="Aperturas Porcentual";
		$seller_detail['AperturasTotales']="Aperturas Totales";
		$seller_detail['ClicksUnicos']="Clicks Unicos";
		$seller_detail['ClicksPorcentual']="Clicks Porcentual";
		$seller_detail['ClicksTotales']="Clicks Totales";
		$seller_detail['Rechazados']="Rechazados";
		$seller_detail['RechazadosPorcentual']="Rechazados Porcentual";
		$seller_detail['Invalidos']="Invalidos";
		$seller_detail['InvalidosPorcentual']="Invalidos Porcentual";
		array_push($campaign_detail,$seller_detail);
		
		foreach($allstats as $stat){
			$owner=Users::model()->findByAttributes(array('id'=>$stat['id_owner']));	
			$seller_detail=[];
			if(!empty($owner['name']) && !empty($owner['lastname'])){
				$seller_detail['Vendedor']=$owner['name'].' '.$owner['lastname'];
			}else{
				$seller_detail['Vendedor']=$owner['username'];
			}
			$seller_detail['EmailVendedor']=$stat['from_mail'];
			$seller_detail['Suscriptores']=$stat['subscribers'];
			$seller_detail['AperturasUnicas']=$stat['reads_val'];
			$seller_detail['AperturasPorcentual']=round($stat['reads_val']*100/$stat['subscribers'],2);
			$seller_detail['AperturasTotales']=$stat['reads_total'];
			$seller_detail['ClicksUnicos']=$stat['clicks_unique'];
			$seller_detail['ClicksPorcentual']=round($stat['clicks_unique']*100/$stat['subscribers'],2);
			$seller_detail['ClicksTotales']=$stat['clicks_total'];
			$seller_detail['Rechazados']=$stat['bounced'];
			$seller_detail['RechazadosPorcentual']=round($stat['bounced']*100/$stat['subscribers'],2);
			$seller_detail['Invalidos']=$stat['bounced_invalid'];
			$seller_detail['InvalidosPorcentual']=round($stat['bounced_invalid']*100/$stat['subscribers'],2);
			array_push($campaign_detail,$seller_detail);
		}
		#$csv = new ECSVExport($campaign_detail);
		#$csv->setDelimiter(";");
		#$content = $csv->toCSV();      
		$filename=md5($id);
		#Yii::app()->getRequest()->sendFile($filename.'.xls', $content, "text/csv", false);
		Yii::import('application.extensions.phpexcel.JPhpExcel');
		$xls = new JPhpExcel('UTF-8', true, $stat['name']);
		$xls->addArray($campaign_detail);
		$xls->generateXML($stat['name']);

		
		exit();
	}
 


	public function actionCreate()
	{
		$this->render('create');
	}

	public function actionDelete()
	{
		$this->render('delete');
	}

	public function actionIndex()
	{
		$this->render('index');
	}

	public function actionUpdate()
	{
		$this->render('update');
	}
	
	public function actionGetlinkdetails($stat,$id)
	{
		$model=Emailcampaignstats::model()->findByPk($stat);
		if(!empty($model)){
			$clicks_detail=$model->clicks_detail;
			if(!empty($clicks_detail)){				
				$html='<table class="table table-striped" style="padding-left:20px;">
						<thead>
						<tr>
						  <th style="width: 85%;">Email</th>
						  <th style="width: 15%">Accesos</th> 
						  <th style="width: 15%">Último acceso</th> 
						</tr>
						</thead>
						<tbody>';
						
						
				$clicks=json_decode($clicks_detail,1);
				$array=[];
				$array2=[];
				foreach ($clicks as $click){
					if($click["link_id"]==$id){
						if(!empty($array[$click["email"]])){
							$array[$click["email"]]++;
							$array2[$click["email"]]=$click["date"];
						}else{
							$array[$click["email"]]=1;
							$array2[$click["email"]]=$click["date"];
						}
					}
				}	
				arsort($array);
				foreach ($array as $key => $val){
						  $html.='<tr>';
						  $html.='<td style="width: 85%;">'.$key.'</td>';
						  $html.='<td style="width: 15%">'.$val.'</td>';
						 $html.='<td style="width: 25%">'.date('d/m/Y',$array2[$key]).'</td>';	
						  $html.='</tr>';
				}
				 $html.='</tbody>
				</table>';	
			
			}
		}
		echo $html;
	}
	 
 
 
	/**
	 * Gets all stats from all campaigns. //deprecated
	 */
	
	static public function actionGetAllStatsOriginal()
	{
		$criteria = new CDbCriteria();
		$criteria->condition = 'status=3 AND deleted=0 AND id_datamedia IS NOT NULL';
		$models = Emailcampaign::model()->findAll($criteria);
		foreach($models as $model){				
			if (strtotime($model->date_to)< strtotime(date('d/m/Y'))){				
				self::actionGetStats($model->id_datamedia);
			}
		}
	}
	
	
	
	
	/**
	 * Gets all stats from all campaigns.
	 */
	
	static public function actionGetAllStats()
	{
		$criteria = new CDbCriteria();
		$criteria->condition = 'status=3 AND deleted=0 AND id_datamedia IS NOT NULL';
		$criteria->order = 'last_updated_stat ASC';
		$models = Emailcampaign::model()->findAll($criteria);
		$flag=0;
		if(!empty($models)){
			foreach($models as $model)
			{
				if ($model->date_to > date('d/m/Y'))
				{
					if($model->last_updated_stat < date("Y-m-d H:i:s", strtotime("+30 minutes"))){
						#if(!$flag)
						if($flag<10)
						{
							/*if($model->processing_stats==0 || ( $model->processing_stats==1 && strtotime(date("Y-m-d H:i:s", strtotime("-1 days"))) ) )
							{*/
								//echo $model->id.' ';
								/*
								if(!empty($model)){
									$model->setScenario('send_real');
									$model->processing_stats= 1;
									$model->save();
								}
								*/
								$flag+=self::actionGetStats($model->id_datamedia);
								/*
								if(!empty($model)){
									$model->setScenario('send_real');
									$model->processing_stats= 0;
									$model->save();
								}
								*/
							/*}*/
						}
					}
				}
			}
		}
		else{echo "no hay ninguna campania con status=3 , deleted=0 y id_datamedia NOT NULL para traerle estadisticas";}
	}
	
	
	/**
	 * Gets all details from the campaign it should.
	 */
	
	static public function actionGetAllDetailedStats()
	{
		$criteria = new CDbCriteria();
		$criteria->condition = 'status=3 AND deleted=0 AND id_datamedia IS NOT NULL';
		$criteria->order = 'last_updated_stat_detail ASC';
		$models = Emailcampaign::model()->findAll($criteria);
		$flag=0;
		if(!empty($models)){
			foreach($models as $model){
				if ($model->date_to > date('d/m/Y')){
					if($model->last_updated_stat_detail < date("Y-m-d H:i:s", strtotime("+30 minutes"))){	
						//$modelStat=Emailcampaignstats::model()->findAllByAttributes(array('id_campaign'=>$model->id, 'id_owner'=>$model->id_user));
						if($flag < 10)
						{
							$flag=self::actionGetNextDetailedStat($model->id, $model->id_user, $model->id_datamedia);
							 
						}
					}
				}
			}
		}
	}
	
	/**
	 * Gets all details for a campaign stat.
	 */
	static public function actionGetNextDetailedStat($campaign_id, $owner_id, $id_datamedia)
	{
		$flag=0;
		$models2 = Emailcampaignstats::model()->findAllByAttributes(array('id_campaign'=>$campaign_id));		
		if(!empty($models2)){
			foreach($models2 as $model2){		
				$model2->bounced_detail = self::get_detailed_stats($campaign_id, $owner_id, $model2->id_datamedia, 'bounce');
				$model2->open_detail = self::get_detailed_stats($campaign_id, $owner_id, $model2->id_datamedia, 'open');
				$model2->clicks_detail = self::get_detailed_stats($campaign_id, $owner_id, $model2->id_datamedia, 'click');
				$model2->updated_detail = date("Y-m-d H:i:s");
				if($model2->save()){
					$flag++;
				}
			}
		}
		if($flag){
			 
			$campania = Emailcampaign::model()->findByPk($campaign_id);
			$campania->last_updated_stat_detail=date("Y-m-d H:i:s"); 
			$campania->setScenario('send_real');
			$campania->save();
			//print_r($campania->getErrors());
		}
		echo $flag.' detalles updateados para la campania '.$campaign_id;
		return $flag;
	}
	
	/**
	 * Gets stats from a single campaign.
	 */
	
	static public function actionGetStats($id=18756)
	{
		$idDatamedia=$id;
		//header("Content-type: text/plain; charset=utf-8");
		$sudata['login_username']= Yii::app()->params['datamedia_user'];
		$sudata['login_password']= Yii::app()->params['datamedia_pass'];
		$sudata['function']='GetCamp';
		$sudata['MessageID']=$idDatamedia;
		$sudata['server']= Yii::app()->params['datamedia_url'];
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $sudata['server']);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_TIMEOUT, 500);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $sudata);
		$data = curl_exec($ch);
		curl_close($ch); 
		$data2= json_decode($data, 1);

		if($data2['status']=="success"){
			$campaign=Emailcampaign::model()->findByAttributes(array('id_datamedia'=>$idDatamedia));
			if(!empty($campaign)){
				$campaniaGeneral = new Emailcampaignstats;
				$campaniaGeneral->id_campaign=$campaign->id;
				$campaniaGeneral->updated_info = date("Y-m-d H:i:s");				
				$campaniaGeneral->name = !empty($data2['stats']['camp_name'])? $data2['stats']['camp_name']:!empty($campaign->name)? $campaign->name : '';
				$campaniaGeneral->from_name = !empty($data2['stats']['from_name'])? $data2['stats']['from_name']:'';
				$campaniaGeneral->from_mail = !empty($data2['stats']['from_mail'])? $data2['stats']['from_mail']:'';
				$campaniaGeneral->start_date = !empty($data2['stats']['dateStart'])? date('Y-m-d',$data2['stats']['dateStart']):!empty($campaign->start_date)? $campaign->start_date : '0000-00-00';
				$campaniaGeneral->first_day_reads = $campaniaGeneral->start_date;
				$campaniaGeneral->date_end = !empty($data2['stats']['dateEnd'])? date('Y-m-d',$data2['stats']['dateEnd']):!empty($campaign->date_to)? $campaign->date_to : '0000-00-00';
				$campaniaGeneral->track_links = !empty($data2['stats']['track_links'])? $data2['stats']['track_links']:null;
				$campaniaGeneral->g_analytics = !empty($data2['stats']['g_analytics'])? $data2['stats']['g_analytics']:null;
				$campaniaGeneral->clicktale = !empty($data2['stats']['clicktale'])? $data2['stats']['clicktale']:null;
				$campaniaGeneral->date_created = !empty($data2['stats']['dateCreated']) ? date('Y-m-d',$data2['stats']['dateCreated']):!empty($campaign->created_at)? $campaign->created_at : '0000-00-00';
				$campaniaGeneral->status = !empty($data2['stats']['status'])? $data2['stats']['status']:'';				
				$campaniaGeneral->reads_data_hourly = array_fill(0, 24, 0);
				$campaniaGeneral->reads_data_weekly = array_fill(0, 7, 0);
				$cant_timelapse = !empty(reset($data2['stats']['senders_stats'])['reads']['data']['timelapse'])? sizeof(reset($data2['stats']['senders_stats'])['reads']['data']['timelapse']):0;
				if ($cant_timelapse<1){
					$campaniaGeneral->reads_data_timelapse = null;
				}else
				{
					$campaniaGeneral->reads_data_timelapse = array_fill(0, $cant_timelapse, 0);
				}
				
				if(!empty($data2['stats']['senders_stats']))
				{
					$cant_senders_stats=0;
					foreach($data2['stats']['senders_stats'] as $key => $val){
						$username = explode("@", $key);
						$username = $username[0];		
						$owner=Users::model()->findByAttributes(array('username'=>$username));			
							if(!empty($owner)){
								$cant_senders_stats++;		
								self::replace_sender_stats($campaign->id, $owner->id, $campaniaGeneral, $val);
							}
					}
					if(!empty($cant_senders_stats)){
						$campaign->setScenario('send_real');
						$campaign->last_updated_stat= date("Y-m-d H:i:s");
						//echo's';die;
						$campaign->save();
					}
					
					//print_r($campaign->getErrors());
					echo 'Actualizadas '.$cant_senders_stats.' estadisticas -por vendedor- para la campania '.$campaign->id;
					return $cant_senders_stats;
				}
				else 
					{
					echo'El conjunto de campañas enviadas por Datamedia está vacío';
					return 0;
					}
			}
			else
				{
				echo'No encuentro campañas que coincidan con la estadística';
				return 0;
				}
		}
		else
		{
			echo'Error en la conección a Datamedia';
			return 0;
		}
		return 1;
	}
	
	public function loadModel($id)
	{
		$model=Emailcampaignstats::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
	
	
	static public function replace_sender_stats($campaign_id, $owner_id, &$campaniaGeneral, $campaignstat)
	{
		$model=Emailcampaignstats::model()->findByAttributes(array('id_campaign'=>$campaign_id, 'id_owner'=>$owner_id));
		if (empty($model)){
			$model = new Emailcampaignstats;
			$model->id_owner=$owner_id;	
			$model->id_campaign = $campaign_id;
		}
		$model->id_datamedia = !empty($campaignstat['id'])? $campaignstat['id']:0;
		$model->name = !empty($campaignstat['camp_name'])? $campaignstat['camp_name']:'';
		$model->subject = !empty($campaignstat['subject'])? $campaignstat['subject']:'';
		$model->start_date = !empty($campaignstat['dateStart'])? date('Y-m-d',$campaignstat['dateStart']):'';
		$model->date_end = isset($campaignstat['dateEnd'])? date('Y-m-d',$campaignstat['dateEnd']):''; 
		$model->from_name = !empty($campaignstat['from_name'])? $campaignstat['from_name']:'';
		$model->from_mail = !empty($campaignstat['from_mail'])? $campaignstat['from_mail']:'';
		$model->track_links = !empty($campaignstat['track_links'])? $campaignstat['track_links']:0;
		$model->g_analytics = !empty($campaignstat['g_analytics'])? $campaignstat['g_analytics']:0;
		$model->clicktale = !empty($campaignstat['clicktale'])? $campaignstat['clicktale']:0;
		$model->date_created =!empty( $campaignstat['dateCreated'])? date('Y-m-d',$campaignstat['dateCreated']):'';
		$model->sent_mails = !empty($campaignstat['sent']['value'])? (int)$campaignstat['sent']['value']:0;
		$model->delivered = !empty($campaignstat['delivered']['value'])? (int)$campaignstat['delivered']['value']:0;
		$model->bounced = !empty($campaignstat['bounced']['value'])? (int)$campaignstat['bounced']['value']:0;	 	
		$model->unsus = !empty($campaignstat['unsubscribed']['value'])? $campaignstat['unsubscribed']['value']:0;		
		$model->sent = !empty($campaignstat['sent']['value'])? $campaignstat['sent']['value']:0;
		$model->bounced_soft = !empty($campaignstat['bounced']['type']['soft']['value'])? $campaignstat['bounced']['type']['soft']['value']:0; 
		$model->bounced_hard = !empty($campaignstat['bounced']['type']['hard']['value'])? $campaignstat['bounced']['type']['hard']['value']:0; 
		$model->bounced_spam = !empty($campaignstat['bounced']['type']['spam']['value'])? $campaignstat['bounced']['type']['spam']['value']:0; 
		$model->bounced_topdomains = !empty($campaignstat['bounced']['top_domains'])? json_encode($campaignstat['bounced']['top_domains']):''; 
		$model->bounced_invalid = !empty($campaignstat['bounced']['type']['invalid']['value'])? $campaignstat['bounced']['type']['invalid']['value']:0; 
		$model->not_send = !empty($campaignstat['not_send'])? $campaignstat['not_send']:0;		
		$model->reads_val = !empty($campaignstat['reads']['value'])? $campaignstat['reads']['value']:0;
		$model->reads_total = !empty($campaignstat['reads']['total'])? $campaignstat['reads']['total']:0;

		if (!empty($campaignstat['reads']['firstDayReads']['month']) && !empty($campaignstat['reads']['firstDayReads']['day']) && !empty($campaignstat['reads']['firstDayReads']['year']))
		{
			$model->first_day_reads = date('Y-m-d',mktime(0,0,0,$campaignstat['reads']['firstDayReads']['month'],$campaignstat['reads']['firstDayReads']['day'],$campaignstat['reads']['firstDayReads']['year']));				
		}
		if (!empty($campaignstat['reads']['data']['timelapse']))
		{
			$i=0;
			$array_temp2=[];				
			foreach($campaignstat['reads']['data']['timelapse'] as $key => $val)
			{
				$array_temp2[$i]=$val;
				$i++;
			}
			$model->reads_data_timelapse = json_encode($array_temp2); 
		}
		else{
			$model->reads_data_timelapse =null;
			}

		if (!empty($campaignstat['reads']['data']['weekly']))
		{
			$model->reads_data_weekly = json_encode($campaignstat['reads']['data']['weekly']); 
		}
		else{
			$model->reads_data_weekly =null;
			}
			
		if (!empty($campaignstat['reads']['data']['hourly']))
		{
			$model->reads_data_hourly = json_encode($campaignstat['reads']['data']['hourly']); 
		}
		else{
			$model->reads_data_hourly =null;
			}

		$model->not_reads = !empty($campaignstat['not_reads']['value'])? $campaignstat['not_reads']['value']:0; 
		$model->refered = !empty($campaignstat['referred']['value'])? $campaignstat['referred']['value']:0; 
		$model->refered_reads = !empty($campaignstat['referred']['reads']['value'])? $campaignstat['referred']['reads']['value']:0; 
		$model->new_suscribers = !empty($campaignstat['new_suscribers'])? $campaignstat['new_suscribers']:0;		
		$model->clicks_unique = !empty($campaignstat['clicks']['subscribers'])? $campaignstat['clicks']['subscribers']:0;
		$model->clicks_total = !empty($campaignstat['clicks']['total'])? $campaignstat['clicks']['total']:0;
		$model->clicks_data = !empty($campaignstat['clicks']['data_list'])? json_encode($campaignstat['clicks']['data_list']):'';
		if (!empty($campaignstat['clicks']['data_list']))
		{
			$model->clicks_data = json_encode($campaignstat['clicks']['data_list']); 
		}
		else{
			$model->clicks_data =null;
			}
		
		$model->ctr = !empty($campaignstat['ctr'])? (int)$campaignstat['ctr']:0;
		$model->ctor = !empty($campaignstat['ctor'])? (int)$campaignstat['ctor']:0;
		
		$model->device_dev_type =!empty($campaignstat['device']['devType'])? json_encode($campaignstat['device']['devType']):null; 		 
		$model->device_client = !empty($campaignstat['device']['client'])? json_encode($campaignstat['device']['client']):null; 
		$model->device_browser = !empty($campaignstat['device']['browser'])? json_encode($campaignstat['device']['browser']):null; 
		$model->device_os = !empty($campaignstat['device']['os'])? json_encode($campaignstat['device']['os']):null; 
		$model->device_data = !empty($campaignstat['device_data'])? json_encode($campaignstat['device_data']):null; 
		$model->reply_to = !empty($campaignstat['reply_to']) ? $campaignstat['reply_to']:'';
		$model->lists = !empty($campaignstat['lists']) ? json_encode($campaignstat['lists']):null; 		

		$model->subscribers = !empty($campaignstat['subscribers'])? $campaignstat['subscribers']:0;  	
		 
		$model->status = !empty($campaignstat['status'])? $campaignstat['status']:'';  
		$model->updated_info = date("Y-m-d H:i:s");
		//$model->bounced_detail = self::get_detailed_stats($campaign_id, $owner_id, $model->id_datamedia, 'bounce');
		//$model->open_detail = self::get_detailed_stats($campaign_id, $owner_id, $model->id_datamedia, 'open');
		//$model->clicks_detail = self::get_detailed_stats($campaign_id, $owner_id, $model->id_datamedia, 'click');
		 
		if($model->save())
		{
			return;
		}
		else 
		{
			return 0;
		}
	}
	
	
	static public function get_detailed_stats($campaign_id, $owner_id, $idDatamedia, $action)
	{
		//header("Content-type: text/plain; charset=utf-8");
		$sudata['login_username']= Yii::app()->params['datamedia_user'];
		$sudata['login_password']= Yii::app()->params['datamedia_pass'];
		$sudata['function']='GetCampDetailedList';		
		$sudata['action'] = $action; 	//	unsubscribe, open, click, bounce, o blocked		
		$sudata['CampID']= $idDatamedia;
		$sudata['server']= Yii::app()->params['datamedia_url'];
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $sudata['server']);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_TIMEOUT, 100);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $sudata);
		$data = curl_exec($ch);
		curl_close($ch); 
		$detalle=json_decode($data,1);
		
		if($detalle['status']=='success'){
			$array_bounced['invalido']=[];
			$array_bounced['bloqueado']=[];			
			$emailsi['Emails']=[];			
			$emailsb['Emails']=[];			
			$array=[];
			$click=[];
			foreach($detalle['data'] as $key =>$val){
					if($action=='bounce' && !empty($val['data']['status']) && $val['data']['status']=="5.1.1"){
						array_push($array_bounced['invalido'], array($val['email'],$val['date']));
							array_push($emailsi['Emails'], $val['email']);
							$emailsi['Estado']=2;
					}
					if($action=='bounce' && !empty($val['data']['status']) && $val['data']['status']=="5.7.1"){
						array_push($array_bounced['bloqueado'], array($val['email'],$val['date']));
						array_push($emailsb['Emails'], $val['email']);
						$emailsb['Estado']=3;
					}
					if($action=='bounce' && !empty($val['data']['status']) && $val['data']['status']=="5.0.0"){
						array_push($array_bounced['bloqueado'], array($val['email'],$val['date']));
						array_push($emailsb['Emails'], $val['email']);
						$emailsb['Estado']=3;
					}
					if($action=='click' && !empty($val['email']) && !empty($val['data']['linkID']) && !empty($val['data']['linkURL'])){
						$click['email']=$val['email'];
						$click['date']=$val['date'];
						$click['link_id']=$val['data']['linkID'];
						$click['link_url']=$val['data']['linkURL'];
						array_push($array, $click);
					}
					if($action=='open' && !empty($val['email']) && !empty($val['date'])){
						$apertura['email']=$val['email'];
						$apertura['date']= $val['date'];
						/*if(!empty($val['opens'])){
						$apertura['cant']= $val['opens'];
						}*/
						array_push($array, $apertura);
					}
			}
			 
			 if($action=='bounce'){
				self::reportar_mails($emailsi);
				self::reportar_mails($emailsb);
				return json_encode($array_bounced);
			 }
			else{
				return json_encode($array);
			}			
		}
		else return 0;
	}
 
 /* Reporta a la Api los emails invalidos y bloqueados */
	static public function reportar_mails($array)
	{
		$urlFinal=Yii::app()->params['contacts_api_url'].'/Contacto/Estado';
		$opts = array('http' =>
			array(
				'method'  => 'PUT',
				'ignore_errors' => true,
				'header'=>"Content-Type: application/json\r\n",				
				'content'=>json_encode($array),
				'timeout' => 190 
			)
		);
		
		$context  = stream_context_create($opts);
		$url = $urlFinal;
		set_time_limit(60);
		if($json = @file_get_contents($url, false,$context)){
			return 1;
		}else{
			return; 0;
		}
		
	}
	
 /* Devuelve un modelo acumulando los datos que recibe en un arreglo de estadisticas */
	static public function acumular_stats($stats_array)
	{
		/* Falta:   
		$model->bounced_detail 
		$model->open_detail
		$model->clicks_detail
		clicks_data
		y las ultimas sql inserts......
		*/
		$cantidad_elementos_acumulados=0;
		$first_elem = reset($stats_array);
		
		$model= new Emailcampaignstats;
		$model->id_campaign = $first_elem->id_campaign;
		$modelCampaign=Emailcampaign::model()->findByAttributes(array('id'=>$model->id_campaign));
		$model->emailcampaign=$modelCampaign;
	
		$model->name = $first_elem->name;
		$model->start_date = $first_elem->start_date;
		$model->date_end = $first_elem->date_end;
		$model->from_name = !empty($first_elem['from_name'])? $first_elem['from_name']:'';
		$model->from_mail = !empty($first_elem['from_mail'])? $first_elem['from_mail']:'';
		$model->track_links = $first_elem->track_links;
		$model->g_analytics = $first_elem->g_analytics;
		$model->clicktale = $first_elem->clicktale;
		$model->date_created = $first_elem->date_created;		
		$model->first_day_reads = $model->start_date;
		$model->status = !empty($first_elem->status)? $first_elem->status:'';
		$model->sent_mails = 0;
		$model->sent = 0;
		$model->delivered = 0;
		$model->unsus = 0;
		$model->bounced_soft = 0;
		$model->bounced_hard = 0;
		$model->bounced_spam = 0;
		$model->bounced_invalid = 0;
		$model->not_send = 0;
		$model->reads_val = 0;
		$model->reads_total = 0;
		$model->reads_data_hourly = array_fill(0, 24, 0);
		$model->reads_data_weekly = array_fill(0, 7, 0);
		$cant_timelapse = !empty($first_elem->reads_data_timelapse)&& !is_array($first_elem->reads_data_timelapse)? sizeof(json_decode($first_elem->reads_data_timelapse)):0;
		$model->reads_data_timelapse = array_fill(0, $cant_timelapse, 0);
		$model->not_reads = 0;
		$model->refered = 0;
		$model->refered_reads = 0;
		$model->new_suscribers = 0;
		$model->clicks_unique = 0;
		$model->clicks_total = 0;
		$model->clicks_data = !empty($first_elem->clicks_data)? $first_elem->clicks_data:[];
		$model->ctr = 0;
		$model->ctor = 0;
		$model->subscribers = 0;
		if (!empty($first_elem->first_day_reads)){
			$model->first_day_reads = $first_elem->first_day_reads;	
		}
		$model->reply_to = !empty($first_elem->reply_to) ? $first_elem->reply_to:'';
		$model->lists = !empty($first_elem->lists) ? $first_elem->lists:null; 		 
		$model->updated_info =  !empty($first_elem->updated_info) ? ($first_elem->updated_info):null; 		  
		$model->delivered=0;
		foreach($stats_array as $key => $val){
			$cantidad_elementos_acumulados++;
			if(strtotime($model->first_day_reads) > strtotime($val->first_day_reads)){
				$model->first_day_reads = $val->first_day_reads;
			}
			if (!empty($val->reads_data_timelapse))
			{
				if(!is_array($val->reads_data_timelapse))
				{
					$val->reads_data_timelapse =json_decode($val->reads_data_timelapse);
				}
				$i=0;
				$array_temp=$model->reads_data_timelapse;

				foreach($val->reads_data_timelapse as $k => $v)
				{
					if(isset($array_temp[$i]))
					{$array_temp[$i]+=$v;
					$i++;}else{$array_temp[$i]=$v;}
				}
				$model->reads_data_timelapse=$array_temp;					
			}
			if (!empty($val->reads_data_weekly)){
				$i=0;
				$array_temp=$model->reads_data_weekly;
				if(!is_array($val->reads_data_weekly))
				{
					$val->reads_data_weekly=json_decode($val->reads_data_weekly);
				}
				foreach($val->reads_data_weekly as $k => $v)
				{
					$array_temp[$i]+=$v;
					$i++;
				}
				$model->reads_data_weekly=$array_temp;
			}
			if (!empty($val->reads_data_hourly))
			{
				$i=0;
				$array_temp=$model->reads_data_hourly;
				if(!is_array($val->reads_data_hourly)){
					$val->reads_data_hourly = json_decode($val->reads_data_hourly);
				}
				foreach($val->reads_data_hourly as $k => $v)
				{
					$array_temp[$i]+=$v;
					$i++;
				}
				$model->reads_data_hourly=$array_temp;
			}
			$model->not_reads += !empty($val->not_reads)? $val->not_reads:0; 
			$model->refered += !empty($val->refered)? $val->refered:0; 
			$model->refered_reads += !empty($val->refered_reads)? $val->refered_reads:0; 
			$model->new_suscribers += !empty($val->new_suscribers)? $val->new_suscribers:0;		
			$model->clicks_unique += !empty($val->clicks_unique)? $val->clicks_unique:0;
			$model->clicks_total += !empty($val->clicks_total)? $val->clicks_total:0;
			
			if (!empty($val->clicks_data))
			{
				$array=[];
				if(is_array($model->clicks_data)){
					$array=$model->clicks_data;
				}else{
					$array=json_decode($model->clicks_data,1);
				}
				
				
				if(!is_array($val->clicks_data))
				{
					$clicks_array=json_decode($val->clicks_data,1);
				}
				foreach ($clicks_array as $k => $v)
				{ 
					if(!empty($model->clicks_data[$v['url']]))
					{
						$array[$v['url']]['clicks']+=$v['clicks'];
						$array[$v['url']]['clicks_totales']+=$v['clicks_totales'];
					}
					else
					{
						$array[$v['url']]['clicks']=$v['clicks'];
						$array[$v['url']]['clicks_totales']=$v['clicks_totales'];
					}
				}
				$model->clicks_data=$array;
			}
			
			$model->subscribers += !empty($val->subscribers)? $val->subscribers:0;  
			 
			$devdata=$model->device_data;
			if (!empty($val->device_data))			
			{
				$array_device=json_decode($val->device_data,1);
				foreach ($array_device as $k => $v){
					foreach ($v as $kk => $vv){
						if(isset($devdata[$k][$kk]['value'])&&isset($devdata[$k][$kk]['total'])){
							$devdata[$k][$kk]['value']+=(float)$vv['value'];
							$devdata[$k][$kk]['total']+=(float)$vv['total'];	
						}
						else{
							$devdata[$k][$kk]['value']=(float)$vv['value'];
							$devdata[$k][$kk]['total']=(float)$vv['total'];
						}						
					}
			 	}  				
			}
			$model->device_data = $devdata; 
			$model->sent += !empty($val->sent)? $val->sent:0; 
			$model->delivered += !empty($val->delivered)? $val->delivered:0; 
			$model->unsus += !empty($val->unsus)? $val->unsus:0; 
			$model->bounced_soft += !empty($val->bounced_soft)? $val->bounced_soft:0; 
			$model->bounced_hard += !empty($val->bounced_hard)? $val->bounced_hard:0; 
			$model->bounced_spam += !empty($val->bounced_spam)? $val->bounced_spam:0; 
			$model->bounced_invalid += !empty($val->bounced_invalid)? $val->bounced_invalid:0; 
			$model->not_send += !empty($val->not_send)? $val->not_send:0; 
			$model->reads_val += !empty($val->reads_val)? $val->reads_val:0; 
			$model->reads_total += !empty($val->reads_total)? $val->reads_total:0; 
			$model->sent_mails += !empty($val->sent_mails)? $val->sent_mails:0; 
		}
		
		if(!empty($model->clicks_unique) && !empty($model->reads_total)) {$model->ctr = $model->clicks_unique/$model->reads_total;}else{$model->ctr =0;}
		if(!empty($model->clicks_unique) && !empty($model->sent_mails)) {$model->ctor = $model->clicks_unique/$model->sent;}else{$model->ctor =0;}
		
		$model->reads_data_timelapse = !empty($model->reads_data_timelapse)? json_encode($model->reads_data_timelapse):null; 		
		$model->reads_data_weekly = !empty($model->reads_data_weekly)? json_encode($model->reads_data_weekly):null; 		
		$model->reads_data_hourly = !empty($model->reads_data_hourly)? json_encode($model->reads_data_hourly):null; 		
		$model->clicks_data = !empty($model->clicks_data)? json_encode($model->clicks_data):null; 		 
		$model->device_dev_type =!empty($model->device_data['devType'])? json_encode($model->device_data['devType']):null; 		 
		$model->device_client = !empty($model->device_data['client'])? json_encode($model->device_data['client']):null; 
		$model->device_browser = !empty($model->device_data['browser'])? json_encode($model->device_data['browser']):null; 
		$model->device_os = !empty($model->device_data['os'])? json_encode($model->device_data['os']):null; 
		$devdata=!empty($devdata)? json_encode($devdata):null; 
		$model->device_data = $devdata;

		return $model;
	}
	
	
	
	public function actionGetopendetails($stat)
	{
		$model=Emailcampaignstats::model()->findByPk($stat);
		if(!empty($model)){
			$open_detail=$model->open_detail;
			if(!empty($open_detail)){	
				return($open_detail);
			}
		}
	}
	
	
}