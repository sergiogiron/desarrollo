<?php

class ContactListController extends Controller
{
	public $layout='pagina';

	public function actionIndex()
	{
		$model = new ContactList();
		$this->render('index', array('model'=>$model));
	}

	public function actionCreate()
	{
		//$this->loadJQuery=true;
		$model = new ContactList();
		$this->render('create', array('model'=>$model));
	}

	public function actionModify()
	{
		$model = new ContactList();
		$this->render('modify', array('model'=>$model));
	}

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}
