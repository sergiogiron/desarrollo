<?php

class HighlightProductsController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='pagina';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('deny', //no entra a ninguna accion ningun usuario que no este logueado               
                'users'=>array('?'),
            ),
            array('allow', // acceden todos los usuarios autenticados a todas las acciones
				'actions'=>Yii::app()->user->getRules(),
				'users'=>array('@'),
            ),
			 array('deny',
                'users'=>array('*'),
            ),		
        );	
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}


	public function actionPublish(){;
		$this->layout='pagina_clear';
		$query=Yii::app()->db->createCommand("SELECT distinct highlight_products.id_channel,highlight_products.id_subchannel,channels.title as channel_title,subchannels.title as subchannel_title FROM highlight_products LEFT JOIN subchannels ON highlight_products.id_subchannel=subchannels.id  LEFT JOIN channels ON highlight_products.id_channel=channels.id where highlight_products.enabled=1")->queryAll();
		$channel=array();
		$subchannel=array();
		foreach($query as $q){
			array_push($channel,array('id'=>$q['id_channel'],'name'=>$q['channel_title']));
			array_push($subchannel,array('id'=>$q['id_subchannel'],'name'=>$q['subchannel_title']));
		}
		
		//print_r($channel);
		//print_r($subchannel);
		if(isset($_POST['hour']) && isset($_POST['channel']) && isset($_POST['subchannel']) && isset($_POST['date']))
		{
			header('Content-Type: application/json');
			foreach($_POST['subchannel'] as $subcanal){
				$criteria = new CDbCriteria;
				$criteria->condition='id_channel= '.$_POST['channel'].' and id_subchannel = '.$subcanal.' and enabled=1';
				$model=HighlightProducts::model()->findAll($criteria);
				
				$publication_app=Publish::Run(array('app'),'highlight_products',$_POST['channel'],array('id_subchannel'=>$subcanal),$_POST['date'],$_POST['hour'],$model,'');
			}

		}else{
			$this->render('publish',array('channel'=>$channel,'subchannel'=>$subchannel,'title'=>Yii::app()->user->Title_nocss(),'breadcrumb'=>Yii::app()->user->Breadcrumb_nocss()));
		}
	}
	
	public function actionPublicationrecord(){
	
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new HighlightProducts;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['HighlightProducts']))
		{
			
			$model->attributes=$_POST['HighlightProducts'];
			
			if($model->save()){
				$this->redirect(array('admin'));
			}else{

				$errores = $model->getErrors();

			}
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);		
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['HighlightProducts']))
		{
			$model->attributes=$_POST['HighlightProducts'];
			if($model->save()){
				$this->redirect(array('admin'));
			}else{
				$errores = $model->getErrors();
			}		
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax'])){
			$this->redirect(array('admin'));
		}
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$this->loadJQuery=false;
		$model=new HighlightProducts('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['HighlightProducts'])){
			$model->attributes=$_GET['HighlightProducts'];
		}
		if(isset($_GET['criterio'])){
			$model->criterio=$_GET['criterio'];
		}	
		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return HighlightProducts the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=HighlightProducts::model()->findByPk($id);
		$model=HighlightProducts::normalize_dates($model);	
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param HighlightProducts $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='highlight-products-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
