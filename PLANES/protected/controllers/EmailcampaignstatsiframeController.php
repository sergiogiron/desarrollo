<?php

class EmailcampaignstatsiframeController extends Controller
{
	public $layout='pagina_clear_iframe';
	
/**
 * Specifies the access control rules.
 * This method is used by the 'accessControl' filter.
 * @return array access control rules
 */
 		
	public function actionAdminiframe($username)
	{
		$this->loadJQuery=false;
		$usuarioActual=Users::model()->findByAttributes(array('username'=>$username));
		$model=new Emailcampaignstats('searchCampaignsbyid',array('id'=>$usuarioActual->id));
		
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Emailcampaignstats']))
			$model->attributes=$_GET['Emailcampaignstats'];
		
		if(isset($_GET['criterio'])){
			$model->criterio=$_GET['criterio'];
		}	
		
		$this->render('//emailcampaignstats/adminiframe',array(
			'model'=>$model,'username'=>$username,'user_id'=>$usuarioActual->id,
		)); 
	}	
	
	public function actionViewiframe($username,$id)
	{
		$model=$this->loadModel($id);
		$device_dev_type=!empty($model->device_dev_type)?json_decode($model->device_dev_type,1):null;
		//$device_client=!empty($model->device_client)?json_decode($model->device_client,1):null; 
		$device_client2=!empty($model->device_client)?json_decode($model->device_client,1):null; 
		if(!empty($device_client2)){foreach ($device_client2 as $key => $val){if($key=="Web Browser"){$key2="Otros";}else{$key2=ucfirst($key);}$device_client[$key2]=$val;}}else{$device_client=null;}
		//$device_browser=!empty($model->device_browser)?json_decode($model->device_browser,1):null;
		$device_browser2=!empty($model->device_browser)?json_decode($model->device_browser,1):null;
		if(!empty($device_browser2)){foreach ($device_browser2 as $key => $val){if($key=="other"){$key2="Otros";}else{$key2=ucfirst($key);}$device_browser[$key2]=$val;}}else{$device_browser=null;}
		//$device_os=!empty($model->device_os)?json_decode($model->device_os,1):null;
		$device_os2=!empty($model->device_os)?json_decode($model->device_os,1):null;
		if(!empty($device_os2)){foreach ($device_os2 as $key => $val){if($key=="other"){$key2="Otros";}else{$key2=ucfirst($key);}$device_os[$key2]=$val;}}else{$device_os=null;}
		$reads_data_timelapse=!empty($model->reads_data_timelapse)?json_decode($model->reads_data_timelapse,1):null;
		$reads_data_weekly=!empty($model->reads_data_weekly)?json_decode($model->reads_data_weekly,1):null;
		$reads_data_hourly=!empty($model->reads_data_hourly)?json_decode($model->reads_data_hourly,1):null;
		$open_details=self::actionGetopendetails($id);
		$open_details==!empty($open_details)?json_decode($open_details,1):null;
		$this->render('//emailcampaignstats/viewiframe',array(
			'model'=>$model,
			'username'=>$username,
			'open_details'=>$open_details,
			'device_dev_type'=>$device_dev_type,
			'device_client'=>$device_client,
			'device_browser'=>$device_browser,
			'device_os'=>$device_os,
			'reads_data_timelapse'=>$reads_data_timelapse,
			'reads_data_weekly'=>$reads_data_weekly,
			'reads_data_hourly'=>$reads_data_hourly,
		));
	}
	
	public function loadModel($id)
	{
		$model=Emailcampaignstats::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
	
	public function actionGetlinkdetails($stat,$id)
	{
		$model=Emailcampaignstats::model()->findByPk($stat);
		if(!empty($model)){
			$clicks_detail=$model->clicks_detail;
			if(!empty($clicks_detail)){				
				$html='<table class="table table-striped" style="padding-left:20px;">
						<thead>
						<tr>
						  <th style="width: 85%;">Email</th>
						  <th style="width: 15%">Accesos</th> 
						    <th style="width: 15%">Fecha</th> 
						</tr>
						</thead>
						<tbody>';
						
						
				$clicks=json_decode($clicks_detail,1);
				$array=[];
				$array2=[];
				foreach ($clicks as $click){
					if($click["link_id"]==$id){
						if(!empty($array[$click["email"]])){
							$array[$click["email"]]++;
							$array2[$click["email"]]=$click["date"];
						}else{
							$array[$click["email"]]=1;
							$array2[$click["email"]]=$click["date"];
						}
					}
				}
				arsort($array);
				foreach ($array as $key => $val){
						  $html.='<tr>';
						  $html.='<td style="width: 85%;">'.$key.'</td>';
						  $html.='<td style="width: 15%">'.$val.'</td>';		
						  $html.='<td style="width: 25%">'.date('d/m/Y',$array2[$key]).'</td>';		
						  $html.='</tr>';
				}
				 $html.='</tbody>
				</table>';	
			}
		}
		echo $html;
	}	


	public function actionGetopendetails($stat)
	{
		$model=Emailcampaignstats::model()->findByPk($stat);
		if(!empty($model)){
			$open_detail=$model->open_detail;
			if(!empty($open_detail)){	
				return($open_detail);
			}	
		}
	}
}