<?php

class AsteriskController extends Controller
{

	public $layout='pagina';
	
	public function filters()
	{		
		return array(
			'accessControl', 
		);
	}
	
	/*public function accessRules()
	{
		return array(
			array('deny', 
                'users'=>array('?'),
            ),
            array('allow', 
				'actions'=>array('admin'),
				'users'=>array('@'),
            ),
			 array('deny',
                'users'=>array('*'),
            ),		
        );	
	}*/
	public function actionLlamadasagente($fecha,$id_agente){
		$this->layout='pagina_clear_iframe';
		$querySelect = "SELECT * FROM llamados_x_agente where date(fecha)>='$fecha' and id_agente=$id_agente";
		$user= Yii::app()->db_asterisk_reports->createCommand($querySelect)->queryAll();
		$this->render('llamados_x_agente',array('user'=>$user));			
	}
	public function actionContadores(){
		$this->layout='pagina_clear_iframe';
		$desde=date("Y-m-d");
		$hasta=date("Y-m-d");
		$desde_f='';
		$hasta_f='';	
		if(isset($_POST['desde']) || isset($_POST['hasta'])){
			if(isset($_POST['desde'])){
				$desde=Utils::date_spa_r($_POST['desde']);
			}
			if(isset($_POST['hasta'])){
				$hasta=Utils::date_spa_r($_POST['hasta']);
			}				
		}
		$where=" where date(fecha)>='$desde' and date(fecha)<='$hasta'";
		$querySelect = "SELECT * FROM llamados_x_fecha $where";
		$result = Yii::app()->db_asterisk_reports->createCommand($querySelect)->queryAll();	
		$data=array('aereos_total'=>0,'hoteles_total'=>0,'paquetes_total'=>0,'cruceros_total'=>0,'otros_total'=>0,'total'=>0,'sin_atender'=>0,'atendidas'=>0,'aereos_sin_atender'=>0,'hoteles_sin_atender'=>0,'paquetes_sin_atender'=>0,'cruceros_sin_atender'=>0,'otros_sin_atender'=>0,'aereos_atendidos'=>0,'hoteles_atendidos'=>0,'paquetes_atendidos'=>0,'cruceros_atendidos'=>0,'otros_atendidos'=>0);
		foreach($result as $rs){
			$data['total']+=(int) $rs['total'];
			$data['atendidas']+=(int) $rs['aereos_atendidos']+(int) $rs['hoteles_atendidos']+(int) $rs['paquetes_atendidos']+(int) $rs['cruceros_atendidos']+(int) $rs['otros_atendidos'];
			$data['aereos_atendidos']+=(int) $rs['aereos_atendidos'];
			$data['hoteles_atendidos']+=(int) $rs['hoteles_atendidos'];
			$data['paquetes_atendidos']+=(int) $rs['paquetes_atendidos'];
			$data['cruceros_atendidos']+=(int) $rs['cruceros_atendidos'];
			$data['otros_atendidos']+=(int) $rs['otros_atendidos'];
			$data['aereos_sin_atender']+=(int) $rs['aereos_sin_atender'];
			$data['hoteles_sin_atender']+=(int) $rs['hoteles_sin_atender'];
			$data['paquetes_sin_atender']+=(int) $rs['paquetes_sin_atender'];
			$data['cruceros_sin_atender']+=(int) $rs['cruceros_sin_atender'];
			$data['otros_sin_atender']+=(int) $rs['otros_sin_atender'];	
			$data['aereos_total']+=(int) $rs['aereos_sin_atender']+(int) $rs['aereos_atendidos'];
			$data['hoteles_total']+=(int) $rs['hoteles_sin_atender']+(int) $rs['hoteles_atendidos'];
			$data['paquetes_total']+=(int) $rs['paquetes_sin_atender']+(int) $rs['paquetes_atendidos'];
			$data['cruceros_total']+=(int) $rs['cruceros_sin_atender']+(int) $rs['cruceros_atendidos'];
			$data['otros_total']+=(int) $rs['otros_sin_atender']+(int) $rs['otros_atendidos'];				
			$data['sin_atender']+=(int) $rs['aereos_sin_atender']+(int) $rs['hoteles_sin_atender']+(int) $rs['paquetes_sin_atender']+(int) $rs['cruceros_sin_atender']+(int) $rs['otros_sin_atender'];
		}
		$this->render('contadores',array('data'=>$data,'desde'=>Utils::cero_return_null(Utils::date_spa($desde)),'hasta'=>Utils::cero_return_null(Utils::date_spa($hasta))));			
	}
	
	public function actionAdmin($fecha)
	{		
		$sede='Parque patricios';
		Yii::app()->db_asterisk_reports->createCommand("DELETE FROM agente_x_stats where date(fecha)='$fecha'")->query();

		$querySelect = "SELECT queue_stats.datetime,agentes.nombre,queue_stats.qagent,eventos.event FROM acd.queue_stats inner join acd.agentes on agentes.agent_id=queue_stats.qagent inner join acd.eventos on queue_stats.qevent=eventos.event_id where date(queue_stats.datetime)='$fecha' order by queue_stats.datetime asc, queue_stats.queue_stats_id asc";
		$result = Yii::app()->db_asterisk->createCommand($querySelect)->queryAll();	
		$agent_array=array();
		foreach($result as $rr){
			//me fijo si se creo para ese usuario el login		
			if(!isset($agent_array[$rr['qagent']]['login'])){
				if($rr['event']=='AGENTLOGIN'){
					if(!isset($agent_array[$rr['qagent']]['tiempo_login'])){
						$agent_array[$rr['qagent']]['agente']=$rr['nombre'];
						$agent_array[$rr['qagent']]['id_agente']=$rr['qagent'];
						$agent_array[$rr['qagent']]['primer_login']=$rr['datetime'];
						$agent_array[$rr['qagent']]['fecha']=$rr['datetime'];
					}				
					$agent_array[$rr['qagent']]['login']=$rr['datetime'];
				}				
			}	
			//me fijo si se creo el logout para ese usuario
			if(!isset($agent_array[$rr['qagent']]['logout'])){
				if($rr['event']=='AGENTLOGOFF'){
					$agent_array[$rr['qagent']]['logout']=$rr['datetime'];
					$agent_array[$rr['qagent']]['ultimo_logout']=$rr['datetime'];
				}
			}		
			//si estan ambos saco la diferencia
			if(isset($agent_array[$rr['qagent']]['logout']) and isset($agent_array[$rr['qagent']]['login'])){
				//pregunto si ya se habia calculado antes el tiempo de logueo, porque siendo asi le sumo mas horas alo anterior 
				if(isset($agent_array[$rr['qagent']]['tiempo_login'])){
					$logintime=Utils::datediff($agent_array[$rr['qagent']]['login'],$agent_array[$rr['qagent']]['logout']);
					$agent_array[$rr['qagent']]['tiempo_login']=Utils::SumaHoras($agent_array[$rr['qagent']]['tiempo_login'],$logintime);					
				}else{
					$agent_array[$rr['qagent']]['tiempo_login']=Utils::datediff($agent_array[$rr['qagent']]['login'],$agent_array[$rr['qagent']]['logout']);					
				}
				unset($agent_array[$rr['qagent']]['login']);
				unset($agent_array[$rr['qagent']]['logout']);
			}		

			//me fijo si ya se habia atendido una llamada		
			if(!isset($agent_array[$rr['qagent']]['connect'])){
				if($rr['event']=='CONNECT'){
					$agent_array[$rr['qagent']]['connect']=$rr['datetime'];
					if(isset($agent_array[$rr['qagent']]['cantidad_llamadas_atendidas'])){
						$agent_array[$rr['qagent']]['cantidad_llamadas_atendidas']+=1;
					}else{
						$agent_array[$rr['qagent']]['cantidad_llamadas_atendidas']=1;
					}					
				}				
			}		
			if($rr['event']=='RINGNOANSWER'){
				if(isset($agent_array[$rr['qagent']]['cantidad_llamadas_no_atendidas'])){
					$agent_array[$rr['qagent']]['cantidad_llamadas_no_atendidas']+=1;
				}else{
					$agent_array[$rr['qagent']]['cantidad_llamadas_no_atendidas']=1;
				}					
			}			
			//me fijo si corto la llamada
			if(!isset($agent_array[$rr['qagent']]['completecaller'])){
				if($rr['event']=='COMPLETECALLER' || $rr['event']=='ABANDON' || $rr['event']=='COMPLETEAGENT'){
					$agent_array[$rr['qagent']]['completecaller']=$rr['datetime'];
				}
				if($rr['event']=='TRANSFER'){
					$agent_array[$rr['qagent']]['derivada']=$rr['datetime'];
				}
			}		
			//si estan ambos saco la diferencia
			if(isset($agent_array[$rr['qagent']]['connect']) and isset($agent_array[$rr['qagent']]['completecaller'])){	
				//pregunto si ya se habia calculado antes , porque siendo asi le sumo mas horas alo anterior 
				if(isset($agent_array[$rr['qagent']]['atendio'])){
					$logintime=Utils::datediff($agent_array[$rr['qagent']]['connect'],$agent_array[$rr['qagent']]['completecaller']);
					$agent_array[$rr['qagent']]['atendio']=Utils::SumaHoras($agent_array[$rr['qagent']]['atendio'],$logintime);
				}else{
					$agent_array[$rr['qagent']]['atendio']=Utils::datediff($agent_array[$rr['qagent']]['connect'],$agent_array[$rr['qagent']]['completecaller']);
				}
				unset($agent_array[$rr['qagent']]['connect']);
				unset($agent_array[$rr['qagent']]['completecaller']);
			}		
			//me fijo si ya se habia pausado una llamada		
			if(!isset($agent_array[$rr['qagent']]['pause'])){
				if($rr['event']=='PAUSE' || $rr['event']=='PAUSEALL'){				
					$agent_array[$rr['qagent']]['pause']=$rr['datetime'];
				}				
			}			
			//me fijo si corto la llamada
			if(!isset($agent_array[$rr['qagent']]['unpause'])){
				if($rr['event']=='UNPAUSE' || $rr['event']=='UNPAUSEALL'){
					$agent_array[$rr['qagent']]['unpause']=$rr['datetime'];
				}
			}		
			//si estan ambos saco la diferencia
			if(isset($agent_array[$rr['qagent']]['pause']) and isset($agent_array[$rr['qagent']]['unpause'])){	
				//pregunto si ya se habia calculado antes , porque siendo asi le sumo mas horas alo anterior 
				if(isset($agent_array[$rr['qagent']]['tiempo_pausado'])){
					$logintime=Utils::datediff($agent_array[$rr['qagent']]['pause'],$agent_array[$rr['qagent']]['unpause']);
					$agent_array[$rr['qagent']]['tiempo_pausado']=Utils::SumaHoras($agent_array[$rr['qagent']]['tiempo_pausado'],$logintime);
				}else{
					$agent_array[$rr['qagent']]['tiempo_pausado']=Utils::datediff($agent_array[$rr['qagent']]['pause'],$agent_array[$rr['qagent']]['unpause']);
				}
				unset($agent_array[$rr['qagent']]['pause']);
				unset($agent_array[$rr['qagent']]['unpause']);
			}		

			//me fijo si ya se habia empezado una llamada		
			if(!isset($agent_array[$rr['qagent']]['ready_on'])){
				if($rr['event']=='AGENTLOGIN' || $rr['event']=='UNPAUSE' || $rr['event']=='UNPAUSEALL' || $rr['event']=='COMPLETECALLER' || $rr['event']=='ABANDON' || $rr['event']=='COMPLETEAGENT'){				
					$agent_array[$rr['qagent']]['ready_on']=$rr['datetime'];
				}				
			}			
			//me fijo si corto la llamada
			if(!isset($agent_array[$rr['qagent']]['ready_off'])){
				if($rr['event']=='CONNECT'){
					$agent_array[$rr['qagent']]['ready_off']=$rr['datetime'];
				}
			}							
			//si estan ambos saco la diferencia
			if(isset($agent_array[$rr['qagent']]['ready_on']) and isset($agent_array[$rr['qagent']]['ready_off'])){	
				//pregunto si ya se habia calculado antes , porque siendo asi le sumo mas horas alo anterior 
				if(isset($agent_array[$rr['qagent']]['tiempo_listo'])){
					$logintime=Utils::datediff($agent_array[$rr['qagent']]['ready_on'],$agent_array[$rr['qagent']]['ready_off']);
					$agent_array[$rr['qagent']]['tiempo_listo']=Utils::SumaHoras($agent_array[$rr['qagent']]['tiempo_listo'],$logintime);
				}else{
					$agent_array[$rr['qagent']]['tiempo_listo']=Utils::datediff($agent_array[$rr['qagent']]['ready_on'],$agent_array[$rr['qagent']]['ready_off']);
				}
				unset($agent_array[$rr['qagent']]['ready_on']);
				unset($agent_array[$rr['qagent']]['ready_off']);
			}				
		}
		foreach($agent_array as $agarray){
			if(isset($agarray['tiempo_login'])){
				$agente=$agarray['agente'];
				$id_agente=$agarray['id_agente'];
				$primer_login=$agarray['primer_login'];
				$tiempo_login=$agarray['tiempo_login'];
				if(isset($agarray['atendio'])){
					$tiempo_hablando=$agarray['atendio'];
				}else{
					$tiempo_hablando='';
				}
				if(isset($agarray['tiempo_pausado'])){
					$tiempo_pausado=$agarray['tiempo_pausado'];
				}else{
					$tiempo_pausado='';
				}
				if(isset($agarray['tiempo_listo'])){
					$tiempo_listo=$agarray['tiempo_listo'];
				}else{
					$tiempo_listo='';
				}		
				if(isset($agarray['ultimo_logout'])){
					$ultimo_logout=$agarray['ultimo_logout'];
				}else{
					$ultimo_logout='';
				}				
				if(isset($agarray['cantidad_llamadas_atendidas'])){
					$cantidad_llamadas_atendidas=$agarray['cantidad_llamadas_atendidas'];
				}else{
					$cantidad_llamadas_atendidas=0;
				}	
				if(isset($agarray['cantidad_llamadas_no_atendidas'])){
					$cantidad_llamadas_no_atendidas=$agarray['cantidad_llamadas_no_atendidas'];
				}else{
					$cantidad_llamadas_no_atendidas=0;
				}		
				if(isset($agarray['derivada'])){
					$derivada=$agarray['derivada'];
				}else{
					$derivada=0;
				}					
				Yii::app()->db_asterisk_reports->createCommand("INSERT INTO agente_x_stats (agente,fecha, id_agente, primer_login, tiempo_login, tiempo_hablando,tiempo_pausado,sede,tiempo_listo,ultimo_logout,cantidad_llamadas_atendidas,cantidad_llamadas_no_atendidas,cantidad_llamadas_derivadas) VALUES ('$agente', '$fecha','$id_agente', '$primer_login','$tiempo_login','$tiempo_hablando','$tiempo_pausado','$sede','$tiempo_listo','$ultimo_logout','$cantidad_llamadas_atendidas','$cantidad_llamadas_no_atendidas','$derivada')")->query();				
			}
		}

		Yii::app()->db_asterisk_reports->createCommand("DELETE FROM llamados_x_fecha where date(fecha)='$fecha'")->query();
		$querySelect = "call acd.llamados_x_fecha('$fecha')";
		$result = Yii::app()->db_asterisk->createCommand($querySelect)->queryAll();
		$this->insert_data_llamados_x_fecha($result,'Parque patricios');
		
		Yii::app()->db_asterisk_reports->createCommand("DELETE FROM llamados_x_agente where date(fecha)='$fecha'")->query();
		$querySelect = "call acd.llamados_x_agente('$fecha')";
		$result = Yii::app()->db_asterisk->createCommand($querySelect)->queryAll();
		$this->insert_data_llamados_x_agente($result,'Parque patricios');		
	}
	
	public function insert_data_llamados_x_agente($result,$sede){
		foreach($result as $r){	
			$uniqueid=$r['uniqueid'];
			$calldate=$r['calldate'];
			$src=$r['src'];
			$agent=$r['agent'];
			$agent_id=$r['agent_id'];
			$nombre=$r['nombre'];
			$answered=$r['answered'];
			$hangup=$r['hangup'];
			Yii::app()->db_asterisk_reports->createCommand("INSERT INTO llamados_x_agente (uniqueid, fecha, src, agent,id_agente,name, answered, hangup, sede) VALUES ('$uniqueid', '$calldate', '$src', '$agent', '$agent_id','$nombre','$answered', '$hangup', '$sede')")->query();
		}		
	}
	
	public function insert_data_llamados_x_fecha($result,$sede){
		foreach($result as $r){	
			$did=$r['did'];
			$fecha=$r['fecha'];
			$agentes=$r['agentes'];
			$aereos_atendidos=$r['aereos_atendidos'];
			$aereos_sin_atender=$r['aereos_sin_atender'];
			$hoteles_atendidos=$r['hoteles_atendidos'];
			$hoteles_sin_atender=$r['hoteles_sin_atender'];
			$paquetes_atendidos=$r['paquetes_atendidos']; 
			$paquetes_sin_atender=$r['paquetes_sin_atender']; 
			$cruceros_atendidos=$r['cruceros_atendidos'];
			$cruceros_sin_atender=$r['cruceros_sin_atender']; 
			$otros_atendidos=$r['otros_atendidos'];
			$otros_sin_atender=$r['otros_sin_atender']; 
			$total=$r['total'];
			Yii::app()->db_asterisk_reports->createCommand("INSERT INTO llamados_x_fecha (did, fecha, agentes, aereos_atendidos, aereos_sin_atender, hoteles_atendidos, hoteles_sin_atender, paquetes_atendidos, paquetes_sin_atender, cruceros_atendidos, cruceros_sin_atender, otros_atendidos, otros_sin_atender, total, sede) VALUES ('$did', '$fecha', '$agentes', '$aereos_atendidos', '$aereos_sin_atender', '$hoteles_atendidos', '$hoteles_sin_atender', '$paquetes_atendidos', '$paquetes_sin_atender', '$cruceros_atendidos', '$cruceros_sin_atender', '$otros_atendidos', '$otros_sin_atender', '$total', '$sede')")->query();
		}		
	}
	
	public function actionDashboard(){
		$this->layout='pagina_clear_iframe';
		$desde=date("Y-m-d");
		$hasta=date("Y-m-d");
		$desde_f='';
		$hasta_f='';
		if(isset($_POST['desde']) || isset($_POST['hasta']) || isset($_POST['sede'])){
			if(isset($_POST['desde'])){
				$desde=Utils::date_spa_r($_POST['desde']);
			}
			if(isset($_POST['hasta'])){
				$hasta=Utils::date_spa_r($_POST['hasta']);
			}			
		}
		$where=" where date(fecha)>='$desde' and date(fecha)<='$hasta'";
		$querySelect = "SELECT * FROM llamados_x_fecha $where";
		$result = Yii::app()->db_asterisk_reports->createCommand($querySelect)->queryAll();	
		$data=array('aereos_total'=>0,'hoteles_total'=>0,'paquetes_total'=>0,'cruceros_total'=>0,'otros_total'=>0,'total'=>0,'sin_atender'=>0,'atendidas'=>0,'aereos_sin_atender'=>0,'hoteles_sin_atender'=>0,'paquetes_sin_atender'=>0,'cruceros_sin_atender'=>0,'otros_sin_atender'=>0,'aereos_atendidos'=>0,'hoteles_atendidos'=>0,'paquetes_atendidos'=>0,'cruceros_atendidos'=>0,'otros_atendidos'=>0);
		foreach($result as $rs){
			$data['total']+=(int) $rs['total'];
			$data['atendidas']+=(int) $rs['aereos_atendidos']+(int) $rs['hoteles_atendidos']+(int) $rs['paquetes_atendidos']+(int) $rs['cruceros_atendidos']+(int) $rs['otros_atendidos'];
			$data['aereos_atendidos']+=(int) $rs['aereos_atendidos'];
			$data['hoteles_atendidos']+=(int) $rs['hoteles_atendidos'];
			$data['paquetes_atendidos']+=(int) $rs['paquetes_atendidos'];
			$data['cruceros_atendidos']+=(int) $rs['cruceros_atendidos'];
			$data['otros_atendidos']+=(int) $rs['otros_atendidos'];
			$data['aereos_sin_atender']+=(int) $rs['aereos_sin_atender'];
			$data['hoteles_sin_atender']+=(int) $rs['hoteles_sin_atender'];
			$data['paquetes_sin_atender']+=(int) $rs['paquetes_sin_atender'];
			$data['cruceros_sin_atender']+=(int) $rs['cruceros_sin_atender'];
			$data['otros_sin_atender']+=(int) $rs['otros_sin_atender'];	
			$data['aereos_total']+=(int) $rs['aereos_sin_atender']+(int) $rs['aereos_atendidos'];
			$data['hoteles_total']+=(int) $rs['hoteles_sin_atender']+(int) $rs['hoteles_atendidos'];
			$data['paquetes_total']+=(int) $rs['paquetes_sin_atender']+(int) $rs['paquetes_atendidos'];
			$data['cruceros_total']+=(int) $rs['cruceros_sin_atender']+(int) $rs['cruceros_atendidos'];
			$data['otros_total']+=(int) $rs['otros_sin_atender']+(int) $rs['otros_atendidos'];				
			$data['sin_atender']+=(int) $rs['aereos_sin_atender']+(int) $rs['hoteles_sin_atender']+(int) $rs['paquetes_sin_atender']+(int) $rs['cruceros_sin_atender']+(int) $rs['otros_sin_atender'];
		}
		$querySelect = "SELECT * FROM agente_x_stats $where";
		$agentes = Yii::app()->db_asterisk_reports->createCommand($querySelect)->queryAll();	
		$this->render('dashboard',array('agentes'=>$agentes,'result'=>$result,'data'=>$data,'desde'=>Utils::cero_return_null(Utils::date_spa($desde)),'hasta'=>Utils::cero_return_null(Utils::date_spa($hasta))));	
	}	
}