<?php
class EmailcampaignController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='pagina';
	
	public function filters()
	{		
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}
	
	public function accessRules()
	{
		$internal=array('preview','send', 'createlist', 'view', 'viewform');			//mergea los permisos del usuario para las acciones
		return array(
			array('allow',
                'actions'=>array('vercuerpo','sendreal'),
            ),
			array('deny', //no entra a ninguna accion ningun usuario que no este logueado               
                'users'=>array('?'),
            ),
            array('allow', // acceden todos los usuarios autenticados a todas las acciones
				'actions'=>array_merge(Yii::app()->user->getRules(),$internal),
				'users'=>array('@'),
            ),
			 array('deny',
                'users'=>array('*'),
            ),		
        );	
	}
	
	
	
	
	public function actionViewform($id)
	{
		$agentes_temp = self::actionGetAgentes();
		$agentes=[];
		$gerencias=[];
		$sectores=[];
		foreach($agentes_temp['data'] as $key => $val){
			foreach($val as $k => $v){
				if(!isset($gerencias[$val['IdGerencia']]) && empty($gerencias[$val['IdGerencia']])){
					 
					$gerencias[$val['IdGerencia']]=$val['Gerencia'];
				}
				if(!isset($sectores[$val['IdSector']]) && empty($sectores[$val['IdSector']])){
					$sectores[$val['IdSector']]=$val['Sector'];
				}
				if(!isset($agentes[$val['Admin']]) && empty($agentes[$val['Admin']])){					 
					$agentes[$val['Admin']]=$val['Admin'];
				}
			}
		}
 
		$agentes_selected=EmailCampaignfilters::model()->findByAttributes(array('filter_name'=>'agentes','id_campaign'=>$id));
		if(!empty($agentes_selected)){$agentes_selected = json_decode($agentes_selected->content,1);}
		else {$agentes_selected['Admin']="";}
		
		$gerencias_selected=EmailCampaignfilters::model()->findByAttributes(array('filter_name'=>'gerencias','id_campaign'=>$id));
		if(!empty($gerencias_selected)){$gerencias_selected = json_decode($gerencias_selected->content,1);}
		else {$gerencias_selected['Gerencias']="";}
		
		$sectores_selected=EmailCampaignfilters::model()->findByAttributes(array('filter_name'=>'sectores','id_campaign'=>$id));
		if(!empty($sectores_selected)){$sectores_selected = json_decode($sectores_selected->content,1);}
		else {$sectores_selected['Sectores']="";}
		 
		$tags=[];
	 
		$this->render('ver',array(
			'model'=>$this->loadModel($id),
			'agentes'=>$agentes,
			'agentes_selected'=>$agentes_selected['Admin'],
			'gerencias_selected'=>$gerencias_selected['Admin'],
			'sectores_selected'=>$sectores_selected['Admin'],
			'gerencias'=>$gerencias,
			'sectores'=>$sectores,
			'tags'=>$tags,
		));
	}
	
	
	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionPreview($id)
	{
		$model=Emailcampaign::model()->findByPk($id);
		$this->renderPartial('preview',array(
			'content'=>$model,
		));
	}

	
	public function actionview(){
		$body=Yii::app()->request->getPost('body');
		$footer=Yii::app()->request->getPost('id_footer');
		$header=Yii::app()->request->getPost('id_header');
		if($footer!=''){
			$footer_all=Emailfooter::model()->findByPk($footer);
			$footer_var=$footer_all->content;			
		}else{
			$footer_var='';
		}
		if($header!=''){
			$header_all=Emailheader::model()->findByPk($header);
			$header_var=$header_all->content;
		}else{
			$header_var='';
		}
		$this->renderPartial('view',array('body'=>$body,'header'=>$header_var,'footer'=>$footer_var));
	}
	

	public function actionVercuerpo($id){
		
		$model=Emailcampaign::model()->findByPk($id); 		
		if(!empty($model) && $model->deleted != 1 && $model->status != 1){
			$body=$model->body;
			$this->renderPartial('vercuerpo',array('body'=>$body));
		}
		else{
			throw new CHttpException(404,'The requested campaign does not exist.');			
		}
	}


	
	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Emailcampaign;
		# Uncomment the following line if AJAX validation is needed
		# $this->performAjaxValidation($model);
		if(isset($_POST['Emailcampaign']))
		{
			$datosPost = yii::app()->request->getPost('Emailcampaign');
			$model->attributes = $datosPost;
			$model->created_at=date("Y-m-d H:i:s");
			$model->id_user=Yii::app()->user->id;
			if(empty($model->send_date))
			{
				$model->send_date = null;
			}
			$model->status=1;
			
			if(!empty($datosPost['agentes'])){
				$agentes_selected = $datosPost['agentes'];
			}
			else{$agentes_selected=[];}
				
			if(!empty($datosPost['sectores'])){
				$sectores_selected = $datosPost['sectores'];
			}
			else{$sectores_selected=[];}
				
			if(!empty($datosPost['gerencias'])){
				$gerencias_selected = $datosPost['gerencias'];
			}
			else{$gerencias_selected=[];}
					
			if($model->save()){
				$id_id = $model->id;
				$agentes_save=[];
				if(!empty($datosPost['agentes']))
				{
					foreach ($datosPost['agentes'] as $key => $val)
					{
						if($val!='')
						{
							array_push($agentes_save,$val);						
						}
					}
				}
				$agentes_save2['Admin']=$agentes_save;
				$agentes_filter=new EmailCampaignfilters;
				$agentes_filter->campana=$model;
				$agentes_filter->id_campaign=$id_id;
				$agentes_filter->filter_name='agentes';
				$agentes_filter->content=json_encode($agentes_save2);
				 
				if($agentes_filter->save()) 
				{}
				else
				{
					print_r($agentes_filter->getErrors());
					die();
				}
				
				
				$sectores_save=[];
				if(!empty($datosPost['sectores']))
				{
					foreach ($datosPost['sectores'] as $key => $val)
					{
						if($val!='')
						{
							array_push($sectores_save,$val);						
						}
					}
				}
				$sectores_save2['Sectores']=$sectores_save;
				$sectores_filter=new EmailCampaignfilters;
				$sectores_filter->campana=$model;
				$sectores_filter->id_campaign=$id_id;
				$sectores_filter->filter_name='sectores';
				$sectores_filter->content=json_encode($sectores_save2);
				 
				if($sectores_filter->save()) 
				{}
				else
				{
					print_r($sectores_filter->getErrors());
					die();
				}
				
				$gerencias_save=[];
				if(!empty($datosPost['gerencias']))
				{
					foreach ($datosPost['gerencias'] as $key => $val)
					{
						if($val!='')
						{
							array_push($gerencias_save,$val);						
						}
					}
				}
				$gerencias_save2['Gerencias']=$gerencias_save;
				$gerencias_filter=new EmailCampaignfilters;
				$gerencias_filter->campana=$model;
				$gerencias_filter->id_campaign=$id_id;
				$gerencias_filter->filter_name='gerencias';
				$gerencias_filter->content=json_encode($gerencias_save2);
				 
				if($gerencias_filter->save()) 
				{}
				else
				{
					print_r($gerencias_filter->getErrors());
					die();
				}
				
				
				$this->redirect(array('/campañas'));
			}
		}
		else{
				$agentes_selected=[];
				$gerencias_selected=[];
				$sectores_selected=[];
			}
		
		$agentes_temp = self::actionGetAgentes();
		$agentes=[];
		$gerencias=[];
		$sectores=[];
		$fullfilter=[];
		foreach($agentes_temp['data'] as $key => $val){
			foreach($val as $k => $v){
				if(!isset($sectores[$val['IdSector']]) && empty($sectores[$val['IdSector']])){
					$sectores[$val['IdSector']]=$val['Sector'];
				}
				if(!isset($gerencias[$val['IdGerencia']]) && empty($gerencias[$val['IdGerencia']])){					 
					$gerencias[$val['IdGerencia']]=$val['Gerencia'];
				}				
				if(!isset($agentes[$val['Admin']]) && empty($agentes[$val['Admin']])){					 
					$agentes[$val['Admin']]=$val['Admin'];
				}
			}
			if(!isset($fullfilter[$val['IdSector']]) && empty($fullfilter[$val['IdSector']])){
					$fullfilter[$val['IdSector']]=[];
				}
				if(!isset($fullfilter[$val['IdSector']][$val['IdGerencia']]) && empty($fullfilter[$val['IdSector']][$val['IdGerencia']])){					 
					$fullfilter[$val['IdSector']][$val['IdGerencia']]=[];
				}
				array_push ($fullfilter[$val['IdSector']][$val['IdGerencia']] , $val['Admin']);
		}
 
		$tags='';
		$this->render('create',array(
			'model'=>$model,
			'agentes'=>$agentes,
			'agentes_selected'=>$agentes_selected,
			'gerencias_selected'=>$gerencias_selected,
			'sectores_selected'=>$sectores_selected,
			'gerencias'=>$gerencias,
			'sectores'=>$sectores,
			'tags'=>$tags,
			'fullfilter'=>$fullfilter,
		));
	}



	static public function actionGetAgentes()
	{
		$opts = array('http' =>
			array(
				'method' => 'GET',
				'ignore_errors' => false,		
				'header'=>"Content-Type: application/json\r\n",		
				'timeout' => 150,  //150 Seg es 2.5 Minutos				
			)
		);

		$context  = stream_context_create($opts);		
		$url=Yii::app()->params['contacts_api_url'].'/Agentes';
		
		if($json = @file_get_contents($url,false,$context)){
            return(json_decode($json,1));
        }
        else return $arr=[];
    }



	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
		if(isset($_POST['Emailcampaign']))
		{
			$datosPost = yii::app()->request->getPost('Emailcampaign');
			$model->attributes=$datosPost;
			if(empty($model->send_date)){
				$model->send_date = null;
			}
			if(!empty($datosPost['agentes']))
			{
				$agentes_selected = $datosPost['agentes'];
			}
			else
				{
				 if(empty($agentes_selected)){$agentes_selected=[];}
				}
			if(!empty($datosPost['sectores'])){
				$sectores_selected = $datosPost['sectores'];
			}
			else{$sectores_selected=[];}
				
			if(!empty($datosPost['gerencias'])){
				$gerencias_selected = $datosPost['gerencias'];
			}
			else{$gerencias_selected=[];}

			if($model->save())
			{	
					$id_id = $model->id;
					$agentes_save=[];
					if(!empty($datosPost['agentes'])){
						foreach ($datosPost['agentes'] as $key => $val)
						{
							if($val!='')
							{
								array_push($agentes_save,$val);						
							}
						}
					}
					$agentes_save2['Admin']=$agentes_save;
					if (empty($agentes_filter))
					{
						$agentes_filter= EmailCampaignfilters::model()->with(array('campana'=>array('condition'=>'campana.id=' . $id_id)))->findByAttributes(array('filter_name' => 'agentes'));
						if(empty($agentes_filter)){
							$agentes_filter=new EmailCampaignfilters;
							$agentes_filter->campana=$model;
							$agentes_filter->id_campaign=$id_id;
							$agentes_filter->filter_name='agentes';
						}
						//$agentes_filter=$this->loadModel($id);
					}
				 
					$agentes_filter->content=json_encode($agentes_save2);
					if($agentes_filter->save()) 
					{}
					else
					{
					print_r($agentes_filter->getErrors());
					die();
					//throw new CDbException('No se puede guardar el filtro de agentes de venta.');
					}	

					$sectores_save=[];
					if(!empty($datosPost['sectores'])){
						foreach ($datosPost['sectores'] as $key => $val)
						{
							if($val!='')
							{
								array_push($sectores_save,$val);						
							}
						}
					}
					$sectores_save2['Sectores']=$sectores_save;
					if (empty($sectores_filter))
					{
						$sectores_filter= EmailCampaignfilters::model()->with(array('campana'=>array('condition'=>'campana.id=' . $id_id)))->findByAttributes(array('filter_name' => 'sectores'));
						if(empty($sectores_filter)){
							$sectores_filter=new EmailCampaignfilters;
							$sectores_filter->campana=$model;
							$sectores_filter->id_campaign=$id_id;
							$sectores_filter->filter_name='sectores';
						}
						//$sectores_filter=$this->loadModel($id);
					}
				 
					$sectores_filter->content=json_encode($sectores_save2);
					if($sectores_filter->save()) 
					{}
					else
					{
					print_r($sectores_filter->getErrors());
					die();
					//throw new CDbException('No se puede guardar el filtro de agentes de venta.');
					}		

					$gerencias_save=[];
					if(!empty($datosPost['gerencias'])){
						foreach ($datosPost['gerencias'] as $key => $val)
						{
							if($val!='')
							{
								array_push($gerencias_save,$val);						
							}
						}
					}
					$gerencias_save2['Gerencias']=$gerencias_save;
					if (empty($gerencias_filter))
					{
						$gerencias_filter= EmailCampaignfilters::model()->with(array('campana'=>array('condition'=>'campana.id=' . $id_id)))->findByAttributes(array('filter_name' => 'gerencias'));
						if(empty($gerencias_filter)){
							$gerencias_filter=new EmailCampaignfilters;
							$gerencias_filter->campana=$model;
							$gerencias_filter->id_campaign=$id_id;
							$gerencias_filter->filter_name='gerencias';
						}
						//$gerencias_filter=$this->loadModel($id);
					}
				 
					$gerencias_filter->content=json_encode($gerencias_save2);
					if($gerencias_filter->save()) 
					{}
					else
					{
					print_r($gerencias_filter->getErrors());
					die();
					//throw new CDbException('No se puede guardar el filtro de agentes de venta.');
					}					

					$this->redirect(array('/campañas'));
			}
		}
		else
			{
				 $agentes_selected=[];
				 $gerencias_selected=[];
				 $sectores_selected=[];
			 
			}
			
		$agentes_temp = self::actionGetAgentes();
		 
		$agentes=[];
		$gerencias=[];
		$sectores=[];
		$fullfilter=[];
		foreach($agentes_temp['data'] as $key => $val){
			foreach($val as $k => $v){
				if(!isset($gerencias[$val['IdGerencia']]) && empty($gerencias[$val['IdGerencia']])){					 
					$gerencias[$val['IdGerencia']]=$val['Gerencia'];
				}
				if(!isset($sectores[$val['IdSector']]) && empty($sectores[$val['IdSector']])){
					$sectores[$val['IdSector']]=$val['Sector'];
				}
				if(!isset($agentes[$val['Admin']]) && empty($agentes[$val['Admin']])){
					$agentes[$val['Admin']]=$val['Admin'];
				}
			}
			if(!isset($fullfilter[$val['IdSector']]) && empty($fullfilter[$val['IdSector']])){
				$fullfilter[$val['IdSector']]=[];
			}
			if(!isset($fullfilter[$val['IdSector']][$val['IdGerencia']]) && empty($fullfilter[$val['IdSector']][$val['IdGerencia']])){
				$fullfilter[$val['IdSector']][$val['IdGerencia']]=[];
			}
			array_push ($fullfilter[$val['IdSector']][$val['IdGerencia']] , $val['Admin']);
		}
		 
		$agentes_selected=EmailCampaignfilters::model()->findByAttributes(array('filter_name'=>'agentes','id_campaign'=>$id));
		$agentes_filter =$agentes_selected;
		if(!empty($agentes_selected ))
		{
		$agentes_selected = json_decode($agentes_selected->content,1);
		$agentes_selected = $agentes_selected['Admin'];
		}
		$sectores_selected=EmailCampaignfilters::model()->findByAttributes(array('filter_name'=>'sectores','id_campaign'=>$id));
		$sectores_filter =$sectores_selected;
		if(!empty($sectores_selected ))
		{
		$sectores_selected = json_decode($sectores_selected->content,1);
		$sectores_selected = $sectores_selected['Sectores'];
		}
		$gerencias_selected=EmailCampaignfilters::model()->findByAttributes(array('filter_name'=>'gerencias','id_campaign'=>$id));
		$gerencias_filter =$gerencias_selected;
		if(!empty($gerencias_selected ))
		{
		$gerencias_selected = json_decode($gerencias_selected->content,1);
		$gerencias_selected = $gerencias_selected['Gerencias'];
		}
		
		
		$tags=[];

		
 
		$this->render('update',array(
			'model'=>$model,
			'agentes'=>$agentes,
			'agentes_selected'=>$agentes_selected,
			'gerencias_selected'=>$gerencias_selected,
			'sectores_selected'=>$sectores_selected,
			'gerencias'=>$gerencias,
			'sectores'=>$sectores,
			'tags'=>$tags,
			'fullfilter'=>$fullfilter,
		));
	}



	public function actionDelete($id)
	{	
		$campaign = $this->loadModel($id);
		if($campaign->status != 1 )
		{
			throw new CDbException('No se puede eliminar una campaña enviada o en proceso de envío.');
		}
		else
		{
			$campaign->setScenario('delete'); 
			$campaign->deleted = '1';
			$campaign->save();  // equivalent to $model->update(); 			
		}
		//$this->loadModel($id)->delete();
		
		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}



	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Emailcampaign');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}


	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$this->loadJQuery=false;
		$model=new Emailcampaign('search');
		$model2=new Emailcampaign('search2');
		$model->unsetAttributes();
		$model2->unsetAttributes();
		if(isset($_GET['Emailcampaign'])){
			$model->attributes=$_GET['Emailcampaign'];
			$model2->attributes=$_GET['Emailcampaign'];
		}		
		if(isset($_GET['criterio'])){
			$model->criterio=$_GET['criterio'];
		}	
		if(isset($_GET['criterio2'])){
			$model2->criterio2=$_GET['criterio2'];
		}			
		$this->render('admin',array(
			'model'=>$model,'model2'=>$model2,
		));
	}



   /*
	* Acción de envío simbólico. Setea los parametros de envío de la campaña para su ingreso en la próxima vuelta del CRON
	* En dicha próxima pasada del cron se cambiará el status 
	*/
	public function actionSend($id)
	{
		$campaign = $this->loadModel($id);
		$campaign->send_date = date("d/m/Y");
		$campaign->send_hour = date("H:i", time());
		$campaign->status = 2; 
		if ($campaign->save())
		{
			Yii::app()->end();
		}
		else
			{
				print_r($campaign->getErrors());
			}
	}


   /*
	* Acción de envío real a la que le pega el CRON para enviarle a datamedia las campañas que correspondan
	* En este proceso, si una campaña tuviese estado en cola, y su fecha de fin es menor a la fecha actual, se debera marcar como completada -> estado=2
	*/
	static public function actionSendReal()
	{
		$campañas_enviadas=0;
		 
		$criteria = new CDbCriteria();
		$criteria->condition = 'status!=3 AND enabled=1 AND deleted=0 AND id_datamedia IS NULL';
		$models = Emailcampaign::model()->findAll($criteria);
 
		foreach($models as $model){
 
			$model->setScenario('send_real'); 	 
			$send_date = str_replace('/', '-', $model->send_date);
			$send_date =  date('Y-m-d', strtotime($send_date));	
			$date_to = str_replace('/', '-', $model->date_to);
			$date_to =   date('Y-m-d', strtotime($date_to));

			if(!empty($send_date)  && !empty($model->send_hour) ){

				if (strtotime($send_date)<= strtotime(date('Y-m-d')) && date('H:i',strtotime($model->send_hour)) <= date('H:i') )
				{
					if(strtotime($date_to) >= strtotime(date('Y-m-d'))){
						echo 'Se procede al envío de la campaña: '.$model->id;
						#$this->traerNovedades();
						/**********************/
						#$lista=self::createDatamediaList($model);
						$lista=self::createDatamediaList_divididas($model);
						//$lista=2465;
						$resultado=self::createDatamediaCampaign($model,$lista);

						if(!empty($resultado) && $resultado['status']=="success")
						{
							$campañas_enviadas++;
							$model->status=3;
							$model->id_datamedia=$resultado['CampID'];
							$model->id_list=$lista;
							$model->suscribers=$resultado['subscribers'];
							$model->save();
						}
						else
						{
							
						}
						// GUARDAR UN LOG aca!
					}
				}
				if(strtotime($date_to)< strtotime(date('Y-m-d')) && 0){
					//La campaña tiene fecha "hasta" menor a hoy =>
					$model->status=3;
					if ($model->save())
						{
							 
						}
						else
						{
							#print_r($model->getErrors());
						}
				}
			}
		}
		if(empty($lista)){$lista='';}
		echo 'Se enviaron '.$campañas_enviadas.' campa&ntilde;as'.'| LISTA:'.$lista;
	}
	


	
	

	static protected function getCampaignAgents($id_campaign)
	{	
		$agentes_selected=EmailCampaignfilters::model()->findByAttributes(array('filter_name'=>'agentes','id_campaign'=>$id_campaign));
		if(!empty($agentes_selected ))
		{
			
			$agentes_selected = json_decode($agentes_selected->content,1);
			
			if(empty($agentes_selected['Admin'] ))			
			{ 
				$agentes_selected = self::actionGetAgentes();
				$agentes_selected = $agentes_selected['data'];
				$agentes_selected = array_column($agentes_selected, 'Admin');
				return $agentes_selected;
			} 
			else{
				return $agentes_selected['Admin'];
			}
		}
		print_r($agentes_selected);die;
		
		return 0;
	}


	static public function getBannedDomains()
	{
		$criteria = new CDbCriteria();
		$criteria->distinct=true;
		$criteria->condition = "enabled=1";
		$banned_domains=EmailDomainsbanned::model()->findAll($criteria);
		$dominios=[];
		if(!empty($banned_domains)){
			foreach($banned_domains as $d => $v){				
				array_push($dominios,$v->domain);			
			}
		}
		return $dominios;
	}


	static protected function getCampaignsIadmin ()
	{
		$coincidencia = Yii::app()->user->username;
		$coincidencia = addcslashes($coincidencia, '%_'); 
		$q = new CDbCriteria( array(
			'condition' => "content LIKE :coincidencia",
			'params'    => array(':coincidencia' => "%$coincidencia%") 
		) );
		 
		$filtroscoincidentes = EmailCampaignfilters::model()->findAll( $q );
		$arr=[];
		foreach($filtroscoincidentes as $filtro){ 
			array_push($arr,$filtro->id_campaign);
		}
		return $arr;
	}

	
	static protected function createDatamediaList_divididas($campaign)
	{
		$agents = self::getCampaignAgents($campaign->id); 
		$domains = self::getBannedDomains();
		
		$filters['Admin']=$agents;
		$filters['Gerencia']=[];
		$filters['Sectores']=[];
		$filters['Dominios']=$domains;
		$filters=json_encode($filters);

		$url_novedades=Yii::app()->params['contacts_api_url'].'/Contacto/Novedades';
		$cn = curl_init($url_novedades);
		curl_setopt($cn, CURLOPT_POST, 1);
		curl_setopt($cn, CURLOPT_POSTFIELDS, $filters);
		curl_setopt($cn, CURLOPT_HEADER, 0);
		curl_setopt($cn, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($cn, CURLOPT_HTTPHEADER, array("Content-Type: application/json"));
		#curl_setopt($cn, CURLOPT_SSL_VERIFYHOST, 0);
		#curl_setopt($cn, CURLOPT_SSL_VERIFYPEER, 0);
		$result = curl_exec($cn);
		
		//var_dump($result);die;
		curl_close($cn); 
		ini_set('memory_limit', '-1');		
		$resultado_novedades=json_decode($result,1);

		if(!empty($resultado_novedades['status']) && $resultado_novedades['status']=="Ok" && !empty($resultado_novedades['data']))
		{
			$contactos=$resultado_novedades['data'];
			$cant_contactos=count($contactos);
			$i=$cant_contactos;	//aux
			echo 'Cantidad de contactos a los que se les enviará la campaña: '.$cant_contactos;
			$j=0;
			$listas_array=[];
			$listas_array[$j]='EMAIL;NOMBRE;FROM_MAIL;FROM_NAME;TELEFONO_REMITENTE;IMAGEN_REMITENTE;'.PHP_EOL;
			 set_time_limit ( 900 );
			foreach($contactos as $key => $val)
			{
				if ($i % 1000 == 0)
				{
					$j++;
					$listas_array[$j]='EMAIL;NOMBRE;FROM_MAIL;FROM_NAME;TELEFONO_REMITENTE;IMAGEN_REMITENTE;'.PHP_EOL;
				}
				
				$nombre=$val['Nombre']!='' ? $val['Nombre']:'';
				$mail=$val['Email']!='' ? $val['Email']:'';
				$imagen=$val['Imagen_remitente']!='' ? $val['Imagen_remitente']:'';
				$remitente=$val['Remitente']!='' ? $val['Remitente']:'';  
				$remitente_mail=$val['Email_remitente']!='' ? $val['Email_remitente']:''; 	
				$remitente_tel=$val['Telefono_remitente']!='' ? $val['Telefono_remitente']:''; 	
				$listas_array[$j].= $mail.';'.$nombre.';'.$remitente_mail.';'.$remitente.';'.$remitente_tel.';'.$imagen.';'.PHP_EOL;
				$i--;
			}
			
			$cant_listas=count($listas_array);
			$lista_actual=0;
			$id_lista=null;
			$flag=false;
			foreach($listas_array as $key => $lista) 
			{
				if($lista_actual==0)
				{
					$data['ListName'] = 'ttsviajes_'.date("Ymd-Hi");
				}
				else
				{
					if(!empty($id_lista)){$data['ListID'] = $id_lista;}
				}
				$urlSendNovedades = Yii::app()->params['datamedia_url'];
				$data['login_username'] = Yii::app()->params['datamedia_user'];
				$data['login_password'] = Yii::app()->params['datamedia_pass'];
				$data['function'] = 'ImportList';
				$data['data'] = $listas_array[$lista_actual]; 
				$data['Cols'] = "email,nombre,from_mail,from_name,telefono_remitente,imagen_remitente";  //Estas son columnas definitivas a mandar
				$data['action_url'] = Yii::app()->params['news_subdomain_url'].'/unsuscribe.php'; //accion de dessuscribir a la q le pega DM cuando un usuario se desuscribe
				$chn = curl_init($urlSendNovedades);
				curl_setopt($chn, CURLOPT_POST, 1);
				curl_setopt($chn, CURLOPT_POSTFIELDS, $data);
				curl_setopt($chn, CURLOPT_HEADER, 0);
				curl_setopt($chn, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($chn, CURLOPT_HTTPHEADER, array("Content-type: multipart/form-data"));
				//curl_setopt($chn, CURLOPT_TIMEOUT_MS, 20000);
				$result2 = curl_exec($chn);
				curl_close($chn);
				
				var_dump($result2);
				
				$result2=json_decode($result2,1);
				if(!empty($result2['status']) && $result2['status']=="success")	
				{
					if($lista_actual==0 && !empty($result2['ListID']))
					{
						$id_lista= $result2['ListID'];
						echo 'Lista creada con el id: '.$id_lista;
					}
				}
				else
				{
					$flag=true;
				}
				$lista_actual++;
			}
			if(!$flag && !empty($id_lista)){return $id_lista;}else{return 0;}
		}
		else
			{
				echo'Ocurrio un error trayendo novedades.';
				return 0;
			}
	}
	
	
	
	
	
	
	static protected function createDatamediaCampaign($campaign, $datamediaList)
	{
		$url = Yii::app()->params['datamedia_url'];
		$data['login_username'] = Yii::app()->params['datamedia_user'];
		$data['login_password'] = Yii::app()->params['datamedia_pass'];

		$footer_all=Emailfooter::model()->findByPk($campaign->id_email_footer);
		$footer_var=(!empty($footer_all->content)?$footer_all->content:'');		

		$header_all=Emailheader::model()->findByPk($campaign->id_email_header);
		$header_var=(!empty($header_all->content)?$header_all->content:'');	 

		$data["name"] = $campaign->name;
		$data["subject"] = $campaign->subject;
		$data["html"] = '<html style="background-color:#FFF;"><body><table style="background-color:#FFF; text-align:center; width:100%;" border="0" cellpadding="0" cellspacing="0"><tr><td>';
		$data["html"] .= 'Si no puede visualizar este email haga click <a href="'.Yii::app()->params['news_subdomain_url'].'/?id='.$campaign->id.'" target="_blank">aqu&iacute;</a><br/>';
		$data["html"] .= '</td></tr><tr><td style="line-height:10px;">&nbsp;</td></tr><tr align="center"><td align="center"><div style="background-color:#FFF; border:1px solid #CCC; border-radius:10px; width:650px;"><table style="color:#666; font-size:12px; width:650px;" border="0" cellpadding="0" cellspacing="0">';
		$data["html"] .= $header_var.'<br/>';
		$data["html"] .= $campaign->body.'<br/>';
		$data["html"] .= $footer_var;
		$data["html"] .= '</table></div></td></tr><tr><td style="line-height:10px;">&nbsp;</td></tr></table></body></html>';
		$data["track_links"] = 1;
		$data["g_analytics"] = true;
		$data["clicktale"] = 0;
		$data["split_sender"] = true;
		$data["from_name"] = "TTS Viajes";
		//$data["url"] = $_POST["url"]; 							//url : [string] URL de donde se obtendrá el cuerpo del mensaje HMTL (opcional) *
		$data["from_mail"] = "online@ttsviajes.com";				/* Agarra el parametro de la Lista. Se pone este por si esta vacio el de la lista. */
		#$data["reply_to"] = "online@ttsviajes.com";				/* Este parametro sobreescribe el de la lista*/
		$data["lists"] = $datamediaList;
		#$data["dateStart"] = 0; 									/* MANDAR UNIX TIME*/  
		$data['function'] = 'CreateCamp';
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-type: multipart/form-data"));
		$result = curl_exec($ch);
		curl_close($ch);
		var_dump($result);
		$result = json_decode($result,1);
		return $result;
	}
	



	public function loadModel($id)
	{
		$model=Emailcampaign::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}


	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='email-campaign-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}



	
	/*deprecated*/
static protected function createDatamediaList($campaign)
	{
		$agents = self::getCampaignAgents($campaign->id); 
		$domains = self::getBannedDomains();
		
		$filters['Admin']=$agents;
		$filters['Gerencia']=[];
		$filters['Sectores']=[];
		$filters['Dominios']=$domains;
		$filters=json_encode($filters);

		$url_novedades=Yii::app()->params['contacts_api_url'].'/Contacto/Novedades';
		$cn = curl_init($url_novedades);
		curl_setopt($cn, CURLOPT_POST, 1);
		curl_setopt($cn, CURLOPT_POSTFIELDS, $filters);
		curl_setopt($cn, CURLOPT_HEADER, 0);
		curl_setopt($cn, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($cn, CURLOPT_HTTPHEADER, array("Content-Type: application/json"));
		$result = curl_exec($cn);
		curl_close($cn); 
		ini_set('memory_limit', '-1');	//ojo
		$resultado_novedades=json_decode($result,1);

		if(!empty($resultado_novedades['status']) && $resultado_novedades['status']=="Ok" && !empty($resultado_novedades['data']))
		{
			$_data = 'EMAIL;NOMBRE;FROM_MAIL;FROM_NAME;TELEFONO_REMITENTE;IMAGEN_REMITENTE;'.PHP_EOL;

			foreach($resultado_novedades['data'] as $key => $val)
			{
				 $nombre=$val['Nombre']!='' ? $val['Nombre']:'';
				 $mail=$val['Email']!='' ? $val['Email']:'';
				 $imagen=$val['Imagen_remitente']!='' ? $val['Imagen_remitente']:'';
				 $remitente=$val['Remitente']!='' ? $val['Remitente']:'';  
				 $remitente_mail=$val['Email_remitente']!='' ? $val['Email_remitente']:''; 	
				 $remitente_tel=$val['Telefono_remitente']!='' ? $val['Telefono_remitente']:''; 	
				 $_data.= $mail.';'.$nombre.';'.$remitente_mail.';'.$remitente.';'.$remitente_tel.';'.$imagen.';'.PHP_EOL;
			}


			if ( !empty($resultado_novedades['data'])){
				$urlSendNovedades = Yii::app()->params['datamedia_url'];
				$data['login_username'] = Yii::app()->params['datamedia_user'];
				$data['login_password'] = Yii::app()->params['datamedia_pass'];
				$data['function'] = 'ImportList';
				#$data['ListID'] = $lista = Yii::app()->params['datamedia_list'];
				$data['ListName'] = 'ttsviajes_'.date("Ymd-Hi");
				$data['data'] = $_data; 
				$data['Cols'] = "email,nombre,from_mail,from_name,telefono_remitente,imagen_remitente";  //Estas son columnas definitivas a mandar
				$data['action_url'] = Yii::app()->params['news_subdomain_url'].'/unsuscribe.php?email='; //accion de dessuscribir a la q le pega DM cuando un usuario se desuscribe
				#var_dump($data);die;
				$chn = curl_init($urlSendNovedades);
				curl_setopt($chn, CURLOPT_POST, 1);
				curl_setopt($chn, CURLOPT_POSTFIELDS, $data);
				curl_setopt($chn, CURLOPT_HEADER, 0);
				curl_setopt($chn, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($chn, CURLOPT_HTTPHEADER, array("Content-type: multipart/form-data"));
				$result2 = curl_exec($chn);
				curl_close($chn);
				$result2=json_decode($result2,1);
 
				if(!empty($result2['status']) && $result2['status']=="success" && !empty($result2['ListID']))
				{
					return $result2['ListID'];
				}
				else return 0;
			}
		}
	}
	
	
	
	/* deprecated */
	protected function traerNovedades()
	{
		$url_novedades=Yii::app()->params['contacts_news_api_url'];
		$cn = curl_init($url_novedades);
		curl_setopt($cn, CURLOPT_CUSTOMREQUEST, "GET"); 
		curl_setopt($cn, CURLOPT_HEADER, 0);
		curl_setopt($cn, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($cn, CURLOPT_HTTPHEADER, array("Content-type: multipart/form-data"));
		$result = curl_exec($cn);
		curl_close($cn); 
		
		$resultado_novedades=json_decode($result,1);
		if(!empty($resultado_novedades['status']) && $resultado_novedades['status']=="Ok" && !empty($resultado_novedades['data']))
		{
			$_data = 'EMAIL;NOMBRE;FROM_MAIL;FROM_NAME;TELEFONO_REMITENTE;IMAGEN_REMITENTE;'.PHP_EOL;

			foreach($resultado_novedades['data'] as $key => $val)
			{
				 $nombre=$val['Nombre']!='' ? $val['Nombre']:'';
				 $mail=$val['Email']!='' ? $val['Email']:'';
				 $imagen=$val['Imagen_remitente']!='' ? $val['Imagen_remitente']:'';
				 $remitente=$val['Remitente']!='' ? $val['Remitente']:'';  
				 $remitente_mail=$val['Email_remitente']!='' ? $val['Email_remitente']:''; 	
				 $remitente_tel=$val['Telefono_remitente']!='' ? $val['Telefono_remitente']:''; 	
				 $_data.= $mail.';'.$nombre.';'.$remitente_mail.';'.$remitente.';'.$remitente_tel.';'.$imagen.';'.PHP_EOL;
			}
			 
			if ( !empty($resultado_novedades['data'])){
				
				$urlSendNovedades = Yii::app()->params['datamedia_url'];
				$dataSendNovedades['login_username'] = Yii::app()->params['datamedia_user'];
				$dataSendNovedades['login_password'] = Yii::app()->params['datamedia_pass'];
				$dataSendNovedades['function'] = 'ImportList';
				$dataSendNovedades['ListID'] = Yii::app()->params['datamedia_list'];
				$dataSendNovedades['data'] = $_data; 
				$dataSendNovedades['Cols'] = "email,nombre,from_mail,from_name,telefono_remitente,imagen_remitente"; //Estas son columnas definitivas a mandar
				
				$chn = curl_init($urlSendNovedades);
				curl_setopt($chn, CURLOPT_POST, 1);
				curl_setopt($chn, CURLOPT_POSTFIELDS, $dataSendNovedades);
				curl_setopt($chn, CURLOPT_HEADER, 0);
				curl_setopt($chn, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($chn, CURLOPT_HTTPHEADER, array("Content-type: multipart/form-data"));
				$resutl = curl_exec($chn);
				curl_close($chn);
			}
		}
	}



	/* deprecated */
	/*
	public function actionCreateList()
	{
$_data = <<< EOL
Email;Nombre;from_mail;from_name;Telefono_remitente;IMAGEN_REMITENTE;
juanignaciobowden@gmail.com;Juan;JIB1;test@ttsviajes.com;TTS;Test;123456;
ignaciobowden@gmail.com;Juan2;JIB2;test@ttsviajes.com;TTS;Test;123456;
jbowden@ttsviajes.com;Juan3;JIB3;test@ttsviajes.com;TTS;Test;123456;
EOL;

		$url = 'http://dattamedia2.fmmail.in/api.php';
		$data['login_username'] = Yii::app()->params['datamedia_user'];
		$data['login_password'] = Yii::app()->params['datamedia_pass'];
		$data['function'] = 'ImportList';
		#$data['ListID'] = 126;
		$data['ListName'] = 'ttsviajes_test23';												# si tiene nombre entonces crea una lista nueva
		$data['data'] = $_data; 
		$data['Cols'] = "email,nombre,apellido,from_mail,from_name,telefono_remitente,imagen_remitente"; 	#Son las columnas definitivas   --> Tienen que estar en minusculas, sino tira error
		#{"status":"success","ListID":2437,"Cols":["%sus_email%","%sus_name%","%pf_apellido%","%pf_from_mail%","%pf_from_name%","%pf_telefono_remitente%"],"imported":"3","updated":"0","failed":"0","duplicated":"0"}bool(true)
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-type: multipart/form-data"));
		$resutl = curl_exec($ch);
		curl_close($ch); 
	}
	*/


	/* deprecated */
	/*
	public function actionUnsuscribe($mails)
	{		
		$url = Yii::app()->params['datamedia_url'];
		$list=Yii::app()->params['datamedia_list'];
		$data['login_username'] = Yii::app()->params['datamedia_user'];
		$data['login_password'] = Yii::app()->params['datamedia_pass'];
		$data["ListID"]=$list;
		$data["function"]="Subscribe";
		$data["action"]="unsubscribe";	//[subscribe|unsubscribe|status]
		$data["email"]=$mails;
 
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-type: multipart/form-data"));
		 
		if($resutl = curl_exec($ch))
		{
			curl_close($ch);
			return 1;
		}
		#$resultado=json_decode($resutl,1);
		#if($resultado['status']=="success"){
		#	return $resultado['msg']
		#}
	}
	*/
	
	

}
