<?php

class SearchController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='pagina';

	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	public function accessRules()
	{
		$internal=array('validatepublish','getorden');
		return array(
			array('deny', //no entra a ninguna accion ningun usuario que no este logueado
                'users'=>array('?'),
            ),
            array('allow', // acceden todos los usuarios autenticados a todas las acciones
				'actions'=>array_merge(Yii::app()->user->getRules(),$internal),
				'users'=>array('@'),
            ),
			 array('deny',
                'users'=>array('*'),
            ),
        );
	}



	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */

	public function actionPublish(){
		$this->layout='pagina_clear';
		$query=Yii::app()->db->createCommand("SELECT distinct search.id_channel,search.id_subchannel,channels.title as channel_folder,subchannels.title as subchannel_folder FROM search LEFT JOIN subchannels ON search.id_subchannel=subchannels.id  LEFT JOIN channels ON search.id_channel=channels.id where  search.enabled=1")->queryAll();
		$channel=array();
		$subchannel=array();
		foreach($query as $q){
			array_push($channel,array('id'=>$q['id_channel'],'name'=>$q['channel_folder']));
			array_push($subchannel,array('id'=>$q['id_subchannel'],'name'=>$q['subchannel_folder']));
		}
		if(isset($_POST['hour']) && isset($_POST['channel']) && isset($_POST['subchannel']) && isset($_POST['date'])){
			header('Content-Type: application/json');
			$criteria = new CDbCriteria;
			$id_menu=Yii::app()->user->id_menu();
			$criteria->addCondition("id_menu=$id_menu");
			$images_sizes=Configimages::model()->findAll($criteria);
			$content_app_server=array();
			foreach($_POST['subchannel'] as $subcanal){
				$content=array();
				$criteria = new CDbCriteria;
				$criteria->addCondition('enabled=1 and id_channel='.$_POST['channel'].' and id_subchannel='.$subcanal);
				$model=Search::model()->findAll($criteria);
				foreach($model as $search){
					$array_imagenes=array();
					foreach($search->imagenes as $imagenes){
						$array_imagenes[]=array(
							'title'=>$imagenes->title,
							'link'=>$imagenes->link,
							'filename'=>'http://'.$_SERVER['SERVER_NAME'].'/'.Yii::app()->request->baseUrl.'/uploads/search/'.$imagenes->id_search.'/'.$imagenes->filename.'/'.$imagenes->filename,
							'ext'=>$imagenes->ext
						);
						foreach($images_sizes as $sizes){
							$array_imagenes[]=array(
								'title'=>$imagenes->title,
								'width'=>$sizes->width,
								'height'=>$sizes->height,
								'link'=>$imagenes->link,
								'filename'=>'http://'.$_SERVER['SERVER_NAME'].'/'.Yii::app()->request->baseUrl.'/uploads/search/'.$imagenes->id_search.'/'.$imagenes->filename.'/'.$imagenes->filename.'_'.$sizes->width.'x'.$sizes->height,
								'ext'=>$imagenes->ext
							);
						}
					}
					array_push($content,array(
							'img'=>array(
								array(
									'filename'=>'http://'.$_SERVER['SERVER_NAME'].Yii::app()->request->baseUrl.'/uploads/search/'.$search->id.'/'.$search->filename.'/'.$search->filename,
									'ext'=>$search->ext,
									'title'=>strtolower($search->producto->title)
								),
							),
							'imagenes'=>$array_imagenes,
							'name'=>$search->producto->title,
							'tracking_description'=>$search->tracking_description,
							'tracking_analitycs'=>$search->tracking_analitycs,
							'url_iframe'=>$search->url_iframe,
							'phone'=>$search->phone,
							'phone_description'=>$search->phone_description,
							'office_hours'=>$search->office_hours,
							'order'=>$search->order,
							'active'=>$search->default_view,
							'online_payment'=>$search->online_payment,
							'script'=>$search->script,
							'anticipation_from_min_days'=>$search->anticipation_from_min_days,
							'anticipation_to_max_days'=>$search->anticipation_to_max_days,
							'max_search_months'=>$search->max_search_months,
							'thankyou_text'=>$search->thankyou_text,
							'error_text'=>$search->error_text,
						)
					);
					$content_app_server=Search::model()->findAllByAttributes(array('id_subchannel'=>$subcanal,'enabled'=>1,'id_channel'=>$_POST['channel']), array('select'=>'id,id_channel,id_subchannel,id_product,id_email_server_sales,id_email_server_alerts,filename,ext,sale_user,sale_pass,sale_show_as,url_iframe,sale_to,alerts_user,alerts_pass,alerts_show_as,alerts_subject_patterns,alerts_to,tracking_description,tracking_analitycs,landings_order_price,landings_order_departure,landings_telephone_sales,landings_max_results_per_pages,phone,phone_description,office_hours,default_view,max_search_months,anticipation_to_max_days,anticipation_from_min_days,enabled,created_at,id_user,rewards_date_from,rewards_date_to,rewards_conversion_ratio,rewards_conversion_min,rewards_conversion_max,rewards_multiple_logic'));
				}
				if(count($model)>0){
					$publication_site=Publish::Run(array('site'),'search',$_POST['channel'],array('id_subchannel'=>$subcanal),$_POST['date'],$_POST['hour'],$content,array("img","imagenes"));
					$criteria = new CDbCriteria;
					$criteria->addCondition('enabled=1 and id_channel='.$_POST['channel'].' and id='.$subcanal);
					$filters=Searchfacetsextendfilters::model()->findAll($criteria);
					$publication_app=Publish::Run(array('app'),'search',$_POST['channel'],array('id_subchannel'=>$subcanal),$_POST['date'],$_POST['hour'],$content_app_server,'');
					$publication_app=Publish::Run(array('app'),'search_facets_extend_filters',$_POST['channel'],array('id_subchannel'=>$subcanal),$_POST['date'],$_POST['hour'],$filters,'');
				}
			}
		}else{
			$this->render('publish',array('title'=>Yii::app()->user->Title_nocss(),'breadcrumb'=>Yii::app()->user->Breadcrumb_nocss(),'channel'=>$channel,'subchannel'=>$subchannel));
		}
	}

	public function actionPublicationrecord(){

	}

	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	public function actionCreate()
	{
		$model=new Search;
		$model_subchannel=array();
		$this->render('create',array('model'=>$model,'model_subchannel'=>$model_subchannel,'errors'=>array(),'order'=>array()));
	}
	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$errors=array();
		$model=$this->loadModel($id);
		$criteria = new CDbCriteria;
		$criteria->addCondition('id='.$model->id_subchannel.' and id_channel='.$model->id_channel);
		$model_subchannel=Subchannels::model()->find($criteria);
		$order=Search::model()->findAllByAttributes(array('id_subchannel'=>$model->id_subchannel));

		if(isset($_POST['Search']))
		{
			foreach($order as $o){
				if($o->order==$_POST['Search']['order'] && $o->id!=$id){
					$o->order=$model->order;
					$o->save();
				}
			}
			
			$model->attributes=$_POST['Search'];
			$campos_validate = array("filename", "anticipation_from_min_days", "anticipation_to_max_days", "thankyou_text","max_search_months", "order");
			foreach($_POST['Search'] as $key => $value){
				if (in_array($key, $campos_validate)) {
					if($value==''){
						$errors[$key]='Debe completar este campo.';
					}
				}
			}
			if(count($errors)==0){
				if($model->default_view == 1){
					$search=Search::model()->updateAll(array('default_view'=>0),'id_subchannel="'.$model->id_subchannel.'" and id_channel="'.$model->id_channel.'"');
				}
				if($model->save()){
					Utils::vaciarcacheprodimages('uploads/search/'.$model->id.'/');
					Searchimages::model()->deleteAll("id_search=$id");
					if(isset($_POST['image'])){
						foreach($_POST['image'] as $images){
							$images_json=json_decode($images);
							$search_images=new Searchimages;
							$search_images->id_search=$model->id;
							$search_images->ext=$images_json->ext;
							$search_images->filename=$images_json->name;
							$search_images->position=$images_json->position;
							$search_images->title=$images_json->title;
							$search_images->link=$images_json->link;
							$search_images->coords=json_encode($images_json->coords);
							$search_images->order=$images_json->order;
							$search_images->save();
							Utils::mover_directorio($model->id,$images_json->name);
						}
					}
					if(!is_dir('uploads/search')){
						mkdir('uploads/search');
					}
					if(!is_dir('uploads/search/'.$model->id)){
						mkdir('uploads/search/'.$model->id);
					}
					if(!is_dir('uploads/search/'.$model->id.'/'.$model->filename)){
						mkdir('uploads/search/'.$model->id.'/'.$model->filename);
					}
					copy('uploads/tmp/'.Yii::app()->user->id.'/'.$model->filename.'/'.$model->filename.'.'.$model->ext,'uploads/search/'.$model->id.'/'.$model->filename.'/'.$model->filename.'.'.$model->ext);
					Utils::vaciarcacheimages('uploads/tmp/'.Yii::app()->user->id.'/');
					$this->redirect(array('admin'));
				}
			}
		}else{
			foreach($model->imagenes as $imagenes){
				Utils::mover_directorio_prod($model->id,$imagenes->filename);
			}
			if($model->filename!=''){
				Utils::mover_directorio_prod($model->id,$model->filename);
			}
		}

		$this->render('update',array(
			'model'=>$model,'errors'=>$errors,'order'=>$order,'model_subchannel'=>$model_subchannel
		));
	}

	public function actionGetorden($id_channel,$id_subchannel,$type,$default=null){
		$sql = "SELECT 1 FROM search WHERE id_channel=:id_channel AND id_subchannel = :id_subchannel";

		$parameters = array(":id_channel"=>$id_channel, ":id_subchannel"=>$id_subchannel);

		$result = Yii::app()->db->createCommand($sql)->execute($parameters);
		$result=($type=='create'?$result+1:$result);
		echo $result;
//		$array = array('result' => $result);
		/*$result=($type=='create'?$result+1:$result);
		for ($i=1; $i <= $result ; $i++) {
			if($default){
				$selected = ($i==$default)?'selected':'';
				echo '<option '.$selected.' value="'.$i.'">'.$i.'</option>';
			} else{
				echo '<option value="'.$i.'">'.$i.'</option>';
			}
		}*/



	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Search');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	public function actionValidatepublish(){
		$sql = "SELECT * FROM search as s WHERE (s.anticipation_from_min_days is null and s.enabled=1) or (s.anticipation_to_max_days is null and s.enabled=1) or (s.max_search_months is null and s.enabled=1) or (s.order is null and s.enabled=1) or (s.filename is null and s.enabled=1)";
		$result = Yii::app()->db->createCommand($sql)->queryAll();
		if(count($result)>0){
			echo 'false';
		}else{
			echo 'true';
		}
	}

	public function actionAdmin()
	{
		$this->loadJQuery=false;
		$model=new Search('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Search'])){
			$model->attributes=$_GET['Search'];
		}

		if(isset($_GET['criterio'])){
			$model->criterio=$_GET['criterio'];
		}

		$this->render('admin',array(
			'model'=>$model
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Search the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Search::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Search $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='search-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
