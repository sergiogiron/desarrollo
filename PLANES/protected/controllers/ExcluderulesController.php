<?php

class ExcluderulesController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='pagina';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('deny', //no entra a ninguna accion ningun usuario que no este logueado
                'users'=>array('?'),
            ),
            array('allow', // acceden todos los usuarios autenticados a todas las acciones
				'actions'=>Yii::app()->user->getRules(),
				'users'=>array('@'),
            ),
			 array('deny',
                'users'=>array('*'),
            ),
        );
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */

	public function actionPublish(){
		$this->layout='pagina_clear';
		
		if(isset($_POST['hour']) && isset($_POST['date'])){
			header('Content-Type: application/json');
			$query=Yii::app()->db->createCommand("SELECT 
												    icm.IATA iata,
												    icm.Country country,
												    er.expire_date_to,
												    er.expire_date_from,
												    ch.title channel,
												    sch.url subchannel,
												    p.title producto
												FROM exclude_rules er
												LEFT JOIN channels ch ON ch.id = er.id_channel
										        LEFT JOIN subchannels sch ON sch.id = er.id_subchannel
										        LEFT JOIN products p ON p.id = er.id_product 
										        LEFT JOIN iata_country_master icm ON icm.id = er.id_iata_country_master
												WHERE er.enabled = 1")->queryAll();
			$content=array();
			$date = date_create_from_format('d/m/Y', $_POST['date']);	
			$publication_date=date_format($date,'Y-m-d').' '.$_POST['hour'].':00';	
			$productos = [];
			$excludes = [];
			$subchannels = [];
			$channels = [];

			foreach ($query as $key => $value) {
				$tmp['iata'] 				= $value['iata'];
				$tmp['country'] 			= $value['country'];
				$tmp['expire_date_to'] 		= $value['expire_date_to'];
				$tmp['expire_date_from'] 	= $value['expire_date_from'];
				//array_push($excludes, $tmp);

				if(!array_key_exists($value['channel'],$content)){
					$content[$value['channel']] = [];
				}
				if(!array_key_exists($value['subchannel'],$content[$value['channel']])){
					$content[$value['channel']][$value['subchannel']] = [];
				}


					if(array_key_exists($value['producto'],$content[$value['channel']][$value['subchannel']])){
						array_push($content[$value['channel']][$value['subchannel']][$value['producto']], $tmp);
						//$productos[$value['producto']] = $excludes;
					}else{
						$content[$value['channel']][$value['subchannel']][$value['producto']] = [];
						array_push($content[$value['channel']][$value['subchannel']][$value['producto']], $tmp);
					}

			}

			$publication_cdn=Publish::Run(array('cdn'),'exclude_rules',array(),array(),$_POST['date'],$_POST['hour'],$content,array());
						
		}else{
			$this->render('publish',array('title'=>Yii::app()->user->Title_nocss(),'breadcrumb'=>Yii::app()->user->Breadcrumb_nocss()));
		}
	}


	
	public function actionPublicationrecord(){
	
	}
	


	public function actionCreate()
	{
		

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
		$model=new Excluderules;
		if(isset($_POST['Excluderules'])){
			$exclude=$_POST['Excluderules'];

			foreach ($exclude['id_product'] as $key => $value) {
				$model=new Excluderules;
				$model->enabled 				= $exclude['enabled'];
				$model->id_channel 				= $exclude['id_channel'];
				$model->id_subchannel 			= $exclude['id_subchannel'];
				$model->id_iata_country_master 	= $exclude['id_iata_country_master'];
				$model->expire_date_from 		= $exclude['expire_date_from'];
				$model->expire_date_to 			= $exclude['expire_date_to'];
				$model->id_product				= $value;
				if($model->save()){}else{
					echo "error";die();
				}
			}
			$this->redirect(array('admin'));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Excluderules']))
		{
			$model->attributes=$_POST['Excluderules'];
			if($model->save()){
				$this->redirect(array('admin'));
			}
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax'])){
			$this->redirect(array('admin'));
		}
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$this->loadJQuery=false;
		$model=new Excluderules('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Excluderules'])){
			$model->attributes=$_GET['Excluderules'];
		}
		if(isset($_GET['criterio'])){
			$model->criterio=$_GET['criterio'];
		}
		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Excluderules the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Excluderules::model()->findByPk($id);
		$model=Excluderules::normalize_dates($model);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Excluderules $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='exclude-rules-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
