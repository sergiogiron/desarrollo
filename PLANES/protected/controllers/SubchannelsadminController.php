<?php

class SubchannelsadminController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='pagina';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('deny', //no entra a ninguna accion ningun usuario que no este logueado
                'users'=>array('?'),
            ),
            array('allow', // acceden todos los usuarios autenticados a todas las acciones
				'actions'=>Yii::app()->user->getRules(),
				'users'=>array('@'),
            ),
			 array('deny',
                'users'=>array('*'),
            ),
        );
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Subchannels;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Subchannels']))
		{
			$model->attributes=$_POST['Subchannels'];
			if($model->save()){
				if(isset($_POST['id_entity'])){
					foreach($_POST['id_entity'] as $id_entity){
						$subchannel_entity=new Subchannelsentity;
						$subchannel_entity->id_entity=$id_entity;
						$subchannel_entity->id_subchannel=$model->id;
						$subchannel_entity->id_user=Yii::app()->user->id;
						$subchannel_entity->created_at=date("Y-m-d H:i:s");
						$subchannel_entity->save();
					}
				}
				if(isset($_POST['id_creditcard'])){
					foreach($_POST['id_creditcard'] as $id_creditcard){
						$subchannel_creditcards=new Subchannelscreditcards;
						$subchannel_creditcards->id_creditcard=$id_creditcard;
						$subchannel_creditcards->id_subchannel=$model->id;
						$subchannel_creditcards->id_user=Yii::app()->user->id;
						$subchannel_creditcards->created_at=date("Y-m-d H:i:s");
						$subchannel_creditcards->save();
					}
				}
				$this->redirect(array('admin'));
			}
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Subchannels']))
		{
			$model->attributes=$_POST['Subchannels'];
			if($model->save()){
				Subchannelsentity::model()->deleteAll("id_subchannel=".$model->id);
				if(isset($_POST['id_entity'])){
					foreach($_POST['id_entity'] as $id_entity){
						$subchannel_entity=new Subchannelsentity;
						$subchannel_entity->id_entity=$id_entity;
						$subchannel_entity->id_subchannel=$model->id;
						$subchannel_entity->id_user=Yii::app()->user->id;
						$subchannel_entity->created_at=date("Y-m-d H:i:s");
						$subchannel_entity->save();
					}
				}
				Subchannelscreditcards::model()->deleteAll("id_subchannel=".$model->id);
				if(isset($_POST['id_creditcard'])){
					foreach($_POST['id_creditcard'] as $id_creditcard){
						$subchannel_creditcards=new Subchannelscreditcards;
						$subchannel_creditcards->id_creditcard=$id_creditcard;
						$subchannel_creditcards->id_subchannel=$model->id;
						$subchannel_creditcards->id_user=Yii::app()->user->id;
						$subchannel_creditcards->created_at=date("Y-m-d H:i:s");
						$subchannel_creditcards->save();
					}
				}
				$this->redirect(array('admin'));
			}
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	public function actionPublish(){
		$this->layout='pagina_clear';
		//query para traer canales
		$query=Yii::app()->db->createCommand("SELECT distinct subchannels.id_channel,channels.title as channel_folder,subchannels.title as subchannel_folder, subchannels.id FROM subchannels LEFT JOIN channels ON subchannels.id_channel=channels.id where  subchannels.enabled=1")->queryAll();
		$channel=array();
		$subchannel=array();
		foreach($query as $q){
			array_push($channel,array('id'=>$q['id_channel'],'name'=>$q['channel_folder']));
			array_push($subchannel,array('id'=>$q['id'],'name'=>$q['subchannel_folder']));
		}
		//query para traer canales
		if(isset($_POST['hour']) && isset($_POST['channel']) && isset($_POST['subchannel']) && isset($_POST['date']))
		{
			header('Content-Type: application/json');
			$criteria = new CDbCriteria;
			$id_menu=Yii::app()->user->id_menu();
			$criteria->addCondition("id_menu=42");
			$images_sizes=Configimages::model()->findAll($criteria);

			foreach($_POST['subchannel'] as $subcanal){
				$content=array();
				$content_app_server=array();
				$criteria = new CDbCriteria;
				$criteria->addCondition('enabled=1 and id_channel='.$_POST['channel'].' and id='.$subcanal);
				$model=Subchannels::model()->findAll($criteria);
				foreach($model as $sub){
					$array_imagenes=array();
					foreach($sub->imagenes as $imagenes){
						$array_imagenes[]=array(
							'title'=>$imagenes->title,
							'filename'=>'http://'.$_SERVER['SERVER_NAME'].'/'.Yii::app()->request->baseUrl.'uploads/subchannels/'.$imagenes->id_subchannel.'/'.$imagenes->filename.'/'.$imagenes->filename,
							'ext'=>$imagenes->ext
						);
						foreach($images_sizes as $sizes){
							$array_imagenes[]=array(
								'title'=>$imagenes->title,
								'width'=>$sizes->width,
								'height'=>$sizes->height,
								'filename'=>'http://'.$_SERVER['SERVER_NAME'].'/'.Yii::app()->request->baseUrl.'uploads/subchannels/'.$imagenes->id_subchannel.'/'.$imagenes->filename.'/'.$imagenes->filename.'_'.$sizes->width.'x'.$sizes->height,
								'ext'=>$imagenes->ext
							);
						}
					}
					array_push($content,array(
						'id' => $sub->id,
						'id_channel' => $sub->id_channel,
						'id_subchannel' => $sub->id_subchannel,
						'title' => $sub->title,
						'url' => $sub->url,
						'title_seo' => $sub->title_seo,
						'timeout' => $sub->timeout,
						'tracking_description' => $sub->tracking_description,
						'tracking_analitycs' => $sub->tracking_analitycs,
						'legals' => $sub->legals,
						'phone_description' => $sub->phone_description,
						'office_hours' => $sub->office_hours,
						'phone' => $sub->phone,
						'reward' => $sub->reward,
						'filename' => $sub->filename,
						'script' => $sub->script,
						'ext' => $sub->ext,
						'enabled' => $sub->enabled,
						'id_user' => $sub->id_user,
						'created_at' => $sub->created_at,
						'promotional_site'=>$sub->promotional_site,
						'promotional_before_start_date'=>$sub->promotional_before_start_date,
						'promotional_before_end_date'=>$sub->promotional_before_end_date,
						'promotional_after_start_date'=>$sub->promotional_after_start_date,
						'promotional_after_end_date'=>$sub->promotional_after_end_date,
						'promotional_url_after'=>$sub->promotional_url_after,
						'promotional_url_before'=>$sub->promotional_url_before,
						'imagenes'=>$array_imagenes,
						'entidades_validas'=>$sub->entidades,
						'tarjetas_validas'=>$sub->creditcards,
						'img'=>array(
							array(
								'filename'=>'http://'.$_SERVER['SERVER_NAME'].'/'.Yii::app()->request->baseUrl.'uploads/subchannels/'.$sub->id.'/'.$sub->filename.'/'.$sub->filename,
								'ext'=>$sub->ext,
								'title'=>strtolower($sub->title)
							),
						),
					));
				}
				$financing_entity = Utils::getPaymentFull();
				$max_cuotas = Utils::getMaxCuotas();
				$publication_app_json = Publish::Run(array('appjson'),'financing',array(),array(),$_POST['date'],$_POST['hour'],$financing_entity,array());
				$publication_site=Publish::Run(array('site'),'subchannels',$_POST['channel'],array('id_subchannel'=>$subcanal),$_POST['date'],$_POST['hour'],$content,array("img","imagenes"));
				$content_app_server=Subchannels::model()->findAllByAttributes(array('id'=>$subcanal,'enabled'=>1,'id_channel'=>$_POST['channel']), array('select'=>'id,id_channel,id_subchannel,title,url,timeout,enabled,created_at,id_user'));
				$publication_app=Publish::Run(array('app'),'subchannels',$_POST['channel'],array('id_subchannel'=>$subcanal),$_POST['date'],$_POST['hour'],$content_app_server,'');
				$publication_cdn_max_cuotas = Publish::Run(array('cdn'),'max_cuotas',array(),array(),$_POST['date'],$_POST['hour'],$max_cuotas,array());
			}

			$content_app_server_scc=Subchannelscreditcards::model()->findAll();
			$publication_app=Publish::Run(array('app'),'subchannels_credit_cards',array(),array(),$_POST['date'],$_POST['hour'],$content_app_server_scc,'');

			$content_app_server_se=Subchannelsentity::model()->findAll();
			$publication_app=Publish::Run(array('app'),'subchannels_entity',array(),array(),$_POST['date'],$_POST['hour'],$content_app_server_se,'');


			$content_cdn_server=array();
			$criteria = new CDbCriteria;
			$criteria->addCondition('enabled=1');
			$model=Subchannels::model()->findAll($criteria);
			foreach($model as $sub){
				$array_imagenes=array();
				foreach($sub->imagenes as $imagenes){
					$array_imagenes[]=array(
						'title'=>$imagenes->title,
						'filename'=>'http://'.$_SERVER['SERVER_NAME'].'/'.Yii::app()->request->baseUrl.'uploads/subchannels/'.$imagenes->id_subchannel.'/'.$imagenes->filename.'/'.$imagenes->filename,
						'ext'=>$imagenes->ext
					);
					foreach($images_sizes as $sizes){
						$array_imagenes[]=array(
							'title'=>$imagenes->title,
							'width'=>$sizes->width,
							'height'=>$sizes->height,
							'filename'=>'http://'.$_SERVER['SERVER_NAME'].'/'.Yii::app()->request->baseUrl.'uploads/subchannels/'.$imagenes->id_subchannel.'/'.$imagenes->filename.'/'.$imagenes->filename.'_'.$sizes->width.'x'.$sizes->height,
							'ext'=>$imagenes->ext
						);
					}
				}
				array_push($content_cdn_server,array(
						'id'=>$sub->id,
						'id_channel'=>$sub->id_channel,
						'id_subchannel'=>$sub->id_subchannel,
						'title'=>$sub->title,
						'url'=>$sub->url,
						'reward'=>$sub->reward,
						'promotional_site'=>$sub->promotional_site,
						'promotional_before_start_date'=>$sub->promotional_before_start_date,
						'promotional_before_end_date'=>$sub->promotional_before_end_date,
						'promotional_after_start_date'=>$sub->promotional_after_start_date,
						'promotional_after_end_date'=>$sub->promotional_after_end_date,
						'promotional_url_after'=>$sub->promotional_url_after,
						'promotional_url_before'=>$sub->promotional_url_before,
						'enabled'=>$sub->enabled,
						'created_at'=>$sub->created_at,
						'id_user'=>$sub->id_user,
						'imagenes'=>$array_imagenes,
						'img'=>array(
							array(
								'filename'=>'http://'.$_SERVER['SERVER_NAME'].'/'.Yii::app()->request->baseUrl.'uploads/subchannels/'.$sub->id.'/'.$sub->filename.'/'.$sub->filename,
								'ext'=>$sub->ext,
								'title'=>strtolower($sub->title)
							),
						),
					)
				);
			}
			//
			$publication_cdn=Publish::Run(array('cdn'),'subchannels',array(),array(),$_POST['date'],$_POST['hour'],$content_cdn_server,array("img","imagenes"));
		}else{
			$this->render('publish',array('title'=>Yii::app()->user->Title_nocss(),'breadcrumb'=>Yii::app()->user->Breadcrumb_nocss(),'channel'=>$channel,'subchannel'=>$subchannel));
		}
	}

	public function actionPublicationrecord(){

	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax'])){
			$this->redirect(array('admin'));
		}
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$this->loadJQuery=false;
		$model=new Subchannels('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Subchannels'])){
			$model->attributes=$_GET['Subchannels'];
		}
		if(isset($_GET['criterio'])){
			$model->criterio=$_GET['criterio'];
		}
		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Subchannels the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Subchannels::model()->findByPk($id);
		$model=Subchannels::normalize_dates($model);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Subchannels $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='subchannels-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
