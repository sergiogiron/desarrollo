<?php

class ConfigcronController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='pagina';

	public function filters()
	{		
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}
	
	public function accessRules()
	{
		return array(
			array('deny', //no entra a ninguna accion ningun usuario que no este logueado               
                'users'=>array('?'),
            ),
            array('allow', // acceden todos los usuarios autenticados a todas las acciones
				'actions'=>Yii::app()->user->getRules(),
				'users'=>array('@'),
            ),
			 array('deny',
                'users'=>array('*'),
            ),		
        );	
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}
	/*proceso de batch mas adelante
	$model = Configcron::model()->findAll();
		foreach($model as $record) {
		$last = strtotime($record->lastrun);
		$interval = $record->timevar * 60;
		$condition = $currentDate - $last;
		if ($condition > $interval ) {
			//Si el cron está fuera del intervalo actualizo el campo fecha
			//EJECUTAR CRON ACÁ
			 $record->lastrun = date("Y-m-d H:i:s", $currentDate);
			 $record->save();
		}
	}
	*/
	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Configcron;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Configcron']))
		{
			$model->attributes=$_POST['Configcron'];
			$model->id_user=Yii::app()->user->id;
			$model->created_at=date("Y-m-d H:i:s");			
			if($model->save())
				$this->redirect(array('/configcron'));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Configcron']))
		{
			$model->attributes=$_POST['Configcron'];
			if($model->save())
				$this->redirect(array('/configcron'));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Configcron');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Configcron('search');
		$this->loadJQuery=false;
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Configcron'])){
			$model->attributes=$_GET['Configcron'];
		}			
		if(isset($_GET['criterio'])){
			$model->criterio=$_GET['criterio'];
		}
		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Configcron the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Configcron::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Configcron $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='configcron-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	

	public function actionProcesar($action)
	{
		/* el cron en el S.O. deberia tener una linea similar(adaptada) a:  C:\xampp\php\php.exe c:/xampp/htdocs/ttsoffice_frontend/protected/yiic.php cronjob run */
		/* Llamo a la accion de obtener Estadísticas*/
		
		/*  $action deberia empezar a traer: s para stats // sd para stats detailed // c para campañas    */
		
		//echo $action;
		switch($action){
			
			case 's':
			//estadisticas
				$idStat=Yii::app()->params['cron_stats_id'];
				$modelStat=Configcron::model()->findByPk($idStat);
				if($modelStat===null){
					throw new CHttpException(404,'The requested page does not exist.');
				}
				else{
					$this->actionStats($modelStat);			
				}
				break;
			case 'sd':
			//detalle de estadisticas
				$idStat=Yii::app()->params['cron_stats_id'];
				$modelStat=Configcron::model()->findByPk($idStat);
				if($modelStat===null){
					throw new CHttpException(404,'The requested page does not exist.');
				}
				else{
					$this->actionStatsDetail($modelStat);			
				}
				break;
			case 'c':
			//campañas
				$idCamp=Yii::app()->params['cron_campaign_id'];
				$modelCamp=Configcron::model()->findByPk($idCamp);
				if($modelCamp===null){
					throw new CHttpException(404,'The requested page does not exist.');
				}
				else{
					$this->actionCampaigns($modelCamp);			
				}
				break;
			default:
				$idStat=Yii::app()->params['cron_stats_id'];
				$modelStat=Configcron::model()->findByPk($idStat);
				if($modelStat===null){
					throw new CHttpException(404,'The requested page does not exist.');
				}
				else{
					$this->actionStats($modelStat);			
				}
				/* Llamo a la accion de enviar Campañas*/
				$idCamp=Yii::app()->params['cron_campaign_id'];
				$modelCamp=Configcron::model()->findByPk($idCamp);
				if($modelCamp===null){
					throw new CHttpException(404,'The requested page does not exist.');
				}
				else{
					$this->actionCampaigns($modelCamp);			
				}
				break;
		}
		
	}
	
	public function actionCampaigns($croncampania=0)
	{
		$procesarCampania=Yii::app()->params['cron_campaign_flag'];
		if (!$procesarCampania){
			echo 'Envío de campañas desactivada por parametro cron_campaign_flag';
		}
		if(!empty($croncampania) && strtotime($croncampania->lastrun) + strtotime('+'.$croncampania->timevar.'minutes') >= strtotime(date('Y-m-d H:i:s'))&& $procesarCampania)
		{
			Yii::import('application.controllers.EmailcampaignController');
			EmailcampaignController::actionSendReal();
		}
	}
 
	public function actionStats($cronstats=0)
	{
		$procesarEstadistica=Yii::app()->params['cron_stats_flag'];
		if (!$procesarEstadistica){
			echo 'Busqueda de estadisticas desactivada por parametro cron_stats_flag';
		}
		if(!empty($cronstats) && strtotime($cronstats->lastrun) + strtotime('+'.$cronstats->timevar.'minutes') >= strtotime(date('Y-m-d H:i:s')) && $procesarEstadistica){
			Yii::import('application.controllers.EmailcampaignstatsController');
			//EmailcampaignstatsController::actionGetAllStats();
			EmailcampaignstatsController::actionGetAllStats();
		}
	}
	
	public function actionStatsDetail($cronstats=0)
	{
		$procesarEstadistica=Yii::app()->params['cron_stats_flag'];
		if (!$procesarEstadistica){
			echo 'Busqueda de estadisticas (detail) desactivada por parametro cron_stats_flag';
		}
		if($procesarEstadistica){
			Yii::import('application.controllers.EmailcampaignstatsController');
			EmailcampaignstatsController::actionGetAllDetailedStats();
		}
	}
	
}