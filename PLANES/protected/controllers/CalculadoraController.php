<?php
class CalculadoraController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='pagina';


	public function filters()
	{		
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}
	
	public function accessRules()
	{
		$internal=array('view','admin','CalculadoraCuotas', 'imprimir');			//mergea los permisos del usuario para las acciones
		return array(
			array('deny', //no entra a ninguna accion ningun usuario que no este logueado               
                'users'=>array('?'),
            ),
            array('allow', // acceden todos los usuarios autenticados a todas las acciones
				#'actions'=>Yii::app()->user->getRules(),
				'actions'=>array_merge(Yii::app()->user->getRules(),$internal),
				'users'=>array('@'),
            ),
			 array('deny',
                'users'=>array('*'),
            ),		
        );	
	}


	/**
	 * Displays the calculator.
	 */
	public function actionAdmin()
	{
		$bucle=array('financing');
		$financiacion = Utils::getFinanciacion($bucle, false);
		$cdn_route = Yii::app()->params['cdnUrl'].'/json/';
		//echo'<pre>';print_r($financiacion);die;
		$array_financing=[];
		if(!empty($financiacion)){
			foreach($financiacion as $key => $entidad){
				
				if(empty($array_financing[$entidad['title']])){
					$array_financing[$entidad['title']]=[];					
				}
				if(!empty($entidad['imagenes'][0])){
					$array_financing[$entidad['title']]['imagen']=$cdn_route.$entidad['imagenes'][0]['folder'].'/'.$entidad['imagenes'][0]['filename'];
				}
				
				if(is_array($entidad['data'])){
					foreach ($entidad['data'] as $k=>$v){
						if(!empty($v['account_type'])){
							foreach ($v['account_type'] as $kk=>$vv){						
								if(!is_string($v['amount_dues']) && is_array($v['amount_dues'])){
									foreach ($v['amount_dues'] as $k0=>$v0){
										$tarjeta=$vv['title'];
										$cuotas=$v0;
										$interes=$v['interests'];
										$legal=$v['legal'];
										$legal = str_replace('@vigencia_desde',$v['date_validity_start'],$legal);
										$legal = str_replace('@vigencia_hasta',$v['date_validity_end'],$legal);
										if(isset($array_financing[$entidad['title']]['cuotas'][$cuotas]) && is_array($array_financing[$entidad['title']]['cuotas'][$cuotas])){
											
											if(is_array($array_financing[$entidad['title']]['cuotas'][$cuotas]['interes'][$interes])){
												array_push($array_financing[$entidad['title']]['cuotas'][$cuotas]['interes'][$interes], $tarjeta);
											 
											}else{
												$array_financing[$entidad['title']]['cuotas'][$cuotas]['interes'][$interes]=[];
												array_push($array_financing[$entidad['title']]['cuotas'][$cuotas]['interes'][$interes], $tarjeta);
											}
										}else{
											$array_financing[$entidad['title']]['cuotas'][$cuotas]=[];
											$array_financing[$entidad['title']]['cuotas'][$cuotas]['interes'][$interes]=[];
											array_push($array_financing[$entidad['title']]['cuotas'][$cuotas]['interes'][$interes], $tarjeta);
										}
										$array_financing[$entidad['title']]['cuotas'][$cuotas]['legal']=$legal;
									}
								}else{
									$tarjeta=$vv['title'];
									$cuotas=$v['amount_dues'];
									$interes=$v['interests'];
									$legal=$v['legal'];									
									$legal = str_replace('@vigencia_desde',$v['date_validity_start'],$legal);
									$legal = str_replace('@vigencia_hasta',$v['date_validity_end'],$legal);
									if(isset($array_financing[$entidad['title']]['cuotas'][$cuotas]) && is_array($array_financing[$entidad['title']]['cuotas'][$cuotas])){
										if(is_array($array_financing[$entidad['title']]['cuotas'][$cuotas]['interes'][$interes])){
											array_push($array_financing[$entidad['title']]['cuotas'][$cuotas]['interes'][$interes], $tarjeta);
										}else{
											$array_financing[$entidad['title']]['cuotas'][$cuotas]['interes'][$interes]=[];
											array_push($array_financing[$entidad['title']]['cuotas'][$cuotas]['interes'][$interes], $tarjeta);
										}
									}else{
										$array_financing[$entidad['title']]['cuotas'][$cuotas]=[];
										$array_financing[$entidad['title']]['cuotas'][$cuotas]['interes'][$interes]=[];
										array_push($array_financing[$entidad['title']]['cuotas'][$cuotas]['interes'][$interes], $tarjeta);
									}
									$array_financing[$entidad['title']]['cuotas'][$cuotas]['legal']=$legal;
								}
								
							}
							
						}else{
							if(isset($v['amount_dues']) && is_array($v['amount_dues'])){
								foreach ($v['amount_dues'] as $k0=>$v0){
									$tarjeta=$vv['title'];
									$cuotas=$v0;
									$interes=$v['interests'];
									$legal=$v['legal'];
									$legal = str_replace('@vigencia_desde',$v['date_validity_start'],$legal);
									$legal = str_replace('@vigencia_hasta',$v['date_validity_end'],$legal);
									if(is_array($array_financing[$entidad['title']]['cuotas'][$cuotas])){
										if(is_array($array_financing[$entidad['title']]['cuotas'][$cuotas]['interes'][$interes])){
												array_push($array_financing[$entidad['title']]['cuotas'][$cuotas]['interes'][$interes], $tarjeta);
										}else{
											$array_financing[$entidad['title']]['cuotas'][$cuotas]['interes'][$interes]=[];
											array_push($array_financing[$entidad['title']]['cuotas'][$cuotas]['interes'][$interes], $tarjeta);
										}
									}else{
										$array_financing[$entidad['title']]['cuotas'][$cuotas]=[];
										$array_financing[$entidad['title']]['cuotas'][$cuotas]['interes'][$interes]=[];
										array_push($array_financing[$entidad['title']]['cuotas'][$cuotas]['interes'][$interes], $tarjeta);
									}
									$array_financing[$entidad['title']]['cuotas'][$cuotas]['legal']=$legal;							
								}
							}
							else{
								$tarjeta=$vv['title'];
								$cuotas=$v['amount_dues'];
								$interes=$v['interests'];	
								$legal=$v['legal'];
								$legal = str_replace('@vigencia_desde',$v['date_validity_start'],$legal);
								$legal = str_replace('@vigencia_hasta',$v['date_validity_end'],$legal);
								if(isset($array_financing[$entidad['title']]['cuotas'][$cuotas]) && is_array($array_financing[$entidad['title']]['cuotas'][$cuotas])){
									if(is_array($array_financing[$entidad['title']]['cuotas'][$cuotas]['interes'][$interes])){
										array_push($array_financing[$entidad['title']]['cuotas'][$cuotas]['interes'][$interes], $tarjeta);
									}else{
										$array_financing[$entidad['title']]['cuotas'][$cuotas]['interes'][$interes]=[];
										array_push($array_financing[$entidad['title']]['cuotas'][$cuotas]['interes'][$interes], $tarjeta);
									}
								}else{
									$array_financing[$entidad['title']]['cuotas'][$cuotas]=[];
									$array_financing[$entidad['title']]['cuotas'][$cuotas]['interes'][$interes]=[];
									array_push($array_financing[$entidad['title']]['cuotas'][$cuotas]['interes'][$interes], $tarjeta);
								}
								$array_financing[$entidad['title']]['cuotas'][$cuotas]['legal']=$legal;
							}
						}
					}
				}	
			}
			
			//Ordeno las tarjetas alfabeticamente:
			$array_aux=[];
			foreach($array_financing as $key => $entidad){
				if(empty($array_aux[$key])){
					$array_aux[$key]=[];
					$array_aux[$key]["imagen"]=$entidad["imagen"];
				}
				foreach($entidad ["cuotas"] as $cuota => $intereses ){
					if(empty($array_aux[$key]["cuotas"][$cuota])){
						$array_aux[$key]["cuotas"][$cuota]=[];
						$array_aux[$key]["cuotas"][$cuota]['legal']=$intereses['legal'];
					}
					foreach($intereses['interes'] as $interes => $tarjetas ){
						if(empty($array_aux[$key]["cuotas"][$cuota]['interes'][$interes]))$array_aux[$key]["cuotas"][$cuota]['interes'][$interes]=[];
						$array_aux2=$intereses['interes'][$interes]; 
						unset($intereses['interes'][$interes]); 
						$array_aux2=array_unique($array_aux2);
						usort($array_aux2, function($x, $y) {
							   return strcasecmp($x , $y);
						 });
						$array_aux[$key]["cuotas"][$cuota]['interes'][$interes]=$array_aux2;
					}
				} 
				ksort ($array_aux[$key]["cuotas"]);
			}
			$array_financing = $array_aux;
			
			
			//ordeno entidades por mayor cantidad de cuotas ofrecidas
			$ordered=[];
			foreach($array_financing as $key =>$val){
				$ordered[$key]=count($val['cuotas']);
			}
			arsort($ordered);
			$final=[];
			foreach($ordered as $key =>$val){
				$final[$key]=$array_financing[$key];
			}
			$array_financing=$final;
			
			
			
			//extraigo los legales y armo arreglo de cuotas
			$cuotas=[];
			$legales=[];
			foreach($array_financing as $key =>$entidad){
				foreach($entidad['cuotas'] as $keyE =>$cuota){
					$existe_legal=false;
					if (!isset($legales[$key]) || !is_array($legales[$key])){
						$legales[$key]=[];
					}
					$entidadReducida=str_replace(' ','',$key);
					$entidadReducida=mb_strtolower($entidadReducida,'UTF-8');
					$entidadReducida=urlencode($entidadReducida);
					if(!isset($cuotas[$keyE]))
					{$cuotas[$keyE]=[];array_push($cuotas[$keyE],$entidadReducida);}
					else{array_push($cuotas[$keyE],$entidadReducida);}
					
					foreach($legales[$key] as $k=>$v){
						if (strcasecmp($v,trim(preg_replace('/\s+/', ' ', $cuota['legal'])))==0){
							$existe_legal=true;
							$array_financing[$key]['cuotas'][$keyE]['legal']=$k;
						}
					}
					if(!$existe_legal){
						array_push($legales[$key],trim(preg_replace('/\s+/', ' ', $cuota['legal'])));
						$array_financing[$key]['cuotas'][$keyE]['legal']=max(array_keys($legales[$key]));
					}
				}
			}
			 
	}
//echo'<pre>';var_dump($cuotas);die;
	 
		$this->render('calculadora',array(
			'array_financing'=>$array_financing,
			'cdn_route'=>Yii::app()->params['cdnUrl'].'/json/',
			'legales'=>$legales,
			'cuotas'=>$cuotas,
		));
	}

	
	public function actionImprimir(){

		if(isset($_POST)){
			$data=Yii::app()->request->getPost('content');
			$style=Yii::app()->request->getPost('style');
			$legal=Yii::app()->request->getPost('legal');

		    
		    $fileSubchannel = Yii::app()->params['cdnUrl'].'/json/subchannels.json';
		    $fileSubchannelData = Utils::getFileData($fileSubchannel);

		    foreach ($fileSubchannelData as $subchannel) {
	    		if($subchannel['id_channel']  == '9' && $subchannel['title']  == 'TTS'){
	    			$imgSrc = Yii::app()->params['cdnUrl'].'/json/subchannels/'.$subchannel['img'][0]['filename'];
	    		}
		    }

	 		$headerFooter = '
	 			<htmlpageheader class="MyHeader1" name="MyHeader1">
	 				<header>
						<img class="logoTTS" src="'.$imgSrc.'" />
						<p class="date">Financiacion al '.date('d/m/Y H:i').' hs</p>
	 				</header>
				</htmlpageheader>
				<htmlpagefooter class="MyFooter1" name="MyFooter1">
					<footer>
						<a class="linkPage" href="http://www.ttsviajes.com" target="_blank">www.ttsviajes.com</a>
						<p class="page">{PAGENO}/{nbpg}</p>
					</footer>
				</htmlpagefooter>
				<sethtmlpageheader name="MyHeader1" value="on" show-this-page="1" />
				<sethtmlpagefooter name="MyFooter1" value="on" />
			';

			$data=base64_decode($data);
			$style=base64_decode($style);
			$legal=base64_decode($legal);
			
			$data=json_decode($data,1);
			$style=json_decode($style,1);
			$legal=json_decode($legal,1);
			$data = preg_replace('/<a href=\"(.*?)\">(.*?)<\/a>/', "\\2", $data);
	 		$data = mb_convert_encoding($data, 'UTF-8', 'UTF-8');

			$html='<html><head><title>Financiacion al '.date('d/m/Y H:i').' hs</title><style>'.$style.'</style></head><body>'.$headerFooter.$data.$legal.'</body></html>';
			
			 # mPDF
			$mode = '';
			$format = 'A4';
			$font_size = '12';
			$font = '';
			$margin_left = '10';
			$margin_right = '10';
			$margin_top = '35';
			$margin_bottom = '20';
			$margin_header = '10';
			$margin_footer = '10';
			$orientation = '';
	        $mPDF1 = Yii::app()->ePdf->mpdf($mode, $format, $font_size, $font, $margin_left, $margin_right, $margin_top, $margin_bottom, $margin_header, $margin_footer, $orientation);
	 
	        # You can easily override default constructor's params
	        //$mPDF1 = Yii::app()->ePdf->mpdf('', 'A3');
	        $mPDF1->use_kwt = true;
	 
	        # Header
	        /*$mPDF1->SetHTMLHeaderByName('MyHeader1');
	        //var_dump($headerFooter);die;
	        $mPDF1->SetHeader($headerFooter);
	        $mPDF1->SetFooter($headerFooter);
	        $mPDF1->defaultheaderfontsize=14;
	        $mPDF1->defaultheaderfontstyle='B';
	        $mPDF1->defaultheaderline=0;
	        $mPDF1->defaultfooterfontsize=14;
	        $mPDF1->defaultfooterfontstyle='I';
	        $mPDF1->defaultfooterline=0;*/
	 
	        # render (full page)
	        $mPDF1->WriteHTML($html);
	 
	        # Outputs ready PDF
	        $mPDF1->Output("Financiacion al ".date('d/m/Y H:i')." hs.pdf","D");
		}
	}
}
