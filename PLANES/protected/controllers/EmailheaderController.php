<?php

class EmailheaderController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='pagina';


	public function filters()
	{		
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}
	
	public function accessRules()
	{
		$internal=array('view');			//mergea los permisos del usuario para las acciones
		return array(
			array('deny', //no entra a ninguna accion ningun usuario que no este logueado               
                'users'=>array('?'),
            ),
            array('allow', // acceden todos los usuarios autenticados a todas las acciones
				#'actions'=>Yii::app()->user->getRules(),
				'actions'=>array_merge(Yii::app()->user->getRules(),$internal),
				'users'=>array('@'),
            ),
			 array('deny',
                'users'=>array('*'),
            ),		
        );	
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Emailheader;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Emailheader']))
		{
			$model->attributes=$_POST['Emailheader'];
			$model->created_at=date("Y-m-d H:i:s");
			$model->id_user=Yii::app()->user->id;
			#$model->enabled=1;
			if($model->save()){
				$this->redirect(array('/mailheader'));
			}
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
		if(isset($_POST['Emailheader']))
		{
			foreach($_POST['Emailheader'] as $name=>$value)
			{
				if($name != 'created_at' )
					$model->$name=$value;
			}
			
			if($model->save()){
				$this->redirect(array('/mailheader'));
			}
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}
 

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{ 
		$campanaCreada=Emailcampaign::model()->findByAttributes(array('id_email_header'=>$id,'status'=>1));
		$campanaEnProceso=Emailcampaign::model()->findByAttributes(array('id_email_header'=>$id,'status'=>2));
		if(count($campanaCreada)>0 ||count($campanaEnProceso)>0 )
		{
			throw new CDbException('Hay al menos una campaña activa con el header que intenta borrar asignado.');
			 
		}
		else
		{
			$header = $this->loadModel($id);
			$header->deleted = '1';
			$header->save();  // equivalent to $model->update();  
		}
		 
		//$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Emailheader');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$this->loadJQuery=false;
		$model=new Emailheader('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Emailheader'])){
			$model->attributes=$_GET['Emailheader'];
		}			
		if(isset($_GET['criterio'])){
			$model->criterio=$_GET['criterio'];
		}	
		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Emailheader the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Emailheader::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Emailheader $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='email-header-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
