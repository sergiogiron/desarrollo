<?php

class DashboardController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='pagina';
	
	public function filters()
	{		
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}
	
	public function accessRules()
	{
		return array(
			array('deny', //no entra a ninguna accion ningun usuario que no este logueado               
                'users'=>array('?'),
            ),
            array('allow', // acceden todos los usuarios autenticados a todas las acciones
				'actions'=>array('admin'),
				'users'=>array('@'),
            ),
			 array('deny',
                'users'=>array('*'),
            ),		
        );	
	}

	/**
	 * Lists all models.
	 */
	public function actionAdmin()
	{
		$this->render('admin');
	}
	
	public function actionIndex()
	{
		header("Content-type: text/plain; charset=utf-8");
		$sudata['login_username']="ttsviajes";
		$sudata['login_password']="tts2012";
		$sudata['function']='GetCamp';
		$sudata['MessageID']=18440;
		$sudata['server']="http://dattamedia.fmmail.in/api.php";
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $sudata['server']);curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_TIMEOUT, 100);curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $sudata);
		$data = curl_exec($ch);curl_close($ch);
		print_r(json_decode($data, 1));
		die();	
		$this->render('index',array());
	}

	/**
	 * Performs the AJAX validation.
	 * @param EmailCampaign $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='email-campaign-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
