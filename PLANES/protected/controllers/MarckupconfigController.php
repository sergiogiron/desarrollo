<?php

class MarckupconfigController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='pagina';
	
	public function filters()
	{		
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}
	
	public function accessRules()
	{
		return array(
			array('deny', //no entra a ninguna accion ningun usuario que no este logueado               
                'users'=>array('?'),
            ),
            array('allow', // acceden todos los usuarios autenticados a todas las acciones
				'actions'=>Yii::app()->user->getRules(),
				'users'=>array('@'),
            ),
			 array('deny',
                'users'=>array('*'),
            ),		
        );	
	}

	public function actionPublish(){;
		$this->layout='pagina_clear';
		$query=Yii::app()->db->createCommand("SELECT distinct marckup_config.id_channel,marckup_config.id_subchannel,channels.title as channel_title,subchannels.title as subchannel_title FROM marckup_config LEFT JOIN subchannels ON marckup_config.id_subchannel=subchannels.id  LEFT JOIN channels ON marckup_config.id_channel=channels.id where marckup_config.enabled=1")->queryAll();
		$channel=array();
		$subchannel=array();
		foreach($query as $q){
			array_push($channel,array('id'=>$q['id_channel'],'name'=>$q['channel_title']));
			array_push($subchannel,array('id'=>$q['id_subchannel'],'name'=>$q['subchannel_title']));
		}

		if(isset($_POST['hour']) && isset($_POST['channel']) && isset($_POST['subchannel']) && isset($_POST['date']))
		{
			header('Content-Type: application/json');
			$criteria = new CDbCriteria;

			$criteria->condition='id_channel= '.$_POST['channel'].' and id_subchannel IN ('.implode(',',$_POST['subchannel']).') and enabled=1';
			
			$model=Marckupconfig::model()->findAll($criteria);
			
			$publication_app=Publish::Run(array('app'),'marckup_config',$_POST['channel'],$_POST['subchannel'],$_POST['date'],$_POST['hour'],$model,'');
		}else{
			$this->render('publish',array('channel'=>$channel,'subchannel'=>$subchannel,'title'=>Yii::app()->user->Title_nocss(),'breadcrumb'=>Yii::app()->user->Breadcrumb_nocss()));
		}
	}
	

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Marckupconfig;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Marckupconfig']))
		{
			$model->attributes=$_POST['Marckupconfig'];
			if($model->save())
				$this->redirect(array('/markupconfig'));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Marckupconfig']))
		{
			$model->attributes=$_POST['Marckupconfig'];
			if($model->save())
				$this->redirect(array('/markupconfig'));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Marckupconfig');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$this->loadJQuery=false;
		$model=new Marckupconfig('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Marckupconfig'])){
			$model->attributes=$_GET['Marckupconfig'];
		}

		if(isset($_GET['criterio'])){
			$model->criterio=$_GET['criterio'];
		}	

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Marckupconfig the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Marckupconfig::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Marckupconfig $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='marckup-config-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
