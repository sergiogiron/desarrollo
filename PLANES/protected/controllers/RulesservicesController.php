<?php

class RulesservicesController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='pagina';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('deny', //no entra a ninguna accion ningun usuario que no este logueado
                'users'=>array('?'),
            ),
            array('allow', // acceden todos los usuarios autenticados a todas las acciones
				'actions'=>Yii::app()->user->getRules(),
				'users'=>array('@'),
            ),
			 array('deny',
                'users'=>array('*'),
            ),
        );
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionPublish(){;
		$this->layout='pagina_clear';
		$query=Yii::app()->db->createCommand("SELECT distinct rules_services.id_channel,rules_services.id_subchannel,channels.title as channel_title,subchannels.title as subchannel_title FROM rules_services LEFT JOIN subchannels ON rules_services.id_subchannel=subchannels.id  LEFT JOIN channels ON rules_services.id_channel=channels.id where rules_services.enabled=1")->queryAll();
		$channel=array();
		$subchannel=array();
		foreach($query as $q){
			array_push($channel,array('id'=>$q['id_channel'],'name'=>$q['channel_title']));
			array_push($subchannel,array('id'=>$q['id_subchannel'],'name'=>$q['subchannel_title']));
		}

		//print_r($channel);
		//print_r($subchannel);
		if(isset($_POST['hour']) && isset($_POST['channel']) && isset($_POST['subchannel']) && isset($_POST['date']))
		{
			header('Content-Type: application/json');
			foreach($_POST['subchannel'] as $subcanal){

				$criteria = new CDbCriteria;
				$criteria->condition='id_channel= '.$_POST['channel'].' and id_subchannel = '.$subcanal;
				$model=Rulesservices::model()->findAll($criteria);

				$publication_app=Publish::Run(array('app'),'rules_services',$_POST['channel'],array($subcanal),$_POST['date'],$_POST['hour'],$model,'');
			}

			$criteria = new CDbCriteria;
			$model=Rulesservices::model()->findAll();
			$query2=Yii::app()->db->createCommand("SELECT distinct 
				rules_services.id,
				rules_services.id_provider,
				rules_services.description,
				rules_services.external_endpoint,
				rules_services.internal_endpoint,
				rules_services.expire_date_from,
				rules_services.expire_date_to,
				products.title as products,
				rules_services.id_channel,
				rules_services.id_subchannel,
				rules_services.client_user,
				rules_services.client_pass,
				rules_services.enabled,
				rules_services.created_at,
				rules_services.id_user,
				rules_services.anticipation_from_min_days,
				rules_services.anticipation_to_max_days,
				rules_services.time_out,
				rules_services.time_out_msg,
				rules_services.cache,
				rules_services.start_cache_days,
				rules_services.max_cache_days,
				rules_services.frequency,
				rules_services.start_time,
				rules_services.cache_origins_destinations,
				rules_services.publish_cache_origins_destinations
				FROM rules_services 
				JOIN subchannels ON rules_services.id_subchannel=subchannels.id  
				JOIN channels ON rules_services.id_channel=channels.id 
				JOIN products on products.id = rules_services.id_product
				")->queryAll();
			$opts = array(
					'http'=>array(
					'method'=>'POST',
					'content'=>CJSON::encode($query2),
					'header'=>	"Channel: Site\r\n".
							  	"SubChannel: TTS\r\n" .
				  				"Consumer: Api\r\n".
							  	"Content-Type: application/json\r\n"
				)
			);

			$context  = stream_context_create($opts);
			$url = Yii::app()->params['api_url']."/paquetes/UpdateOrigenesDestinos";

			$json = file_get_contents($url, false,$context);
			$json=json_decode($json);
			

		}else{
			$this->render('publish',array('channel'=>$channel,'subchannel'=>$subchannel,'title'=>Yii::app()->user->Title_nocss(),'breadcrumb'=>Yii::app()->user->Breadcrumb_nocss()));

		}
	}


	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Rulesservices;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Rulesservices']))
		{
			$model->attributes=$_POST['Rulesservices'];
			if($model->save()){
				$this->redirect(array('admin'));
			}
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Rulesservices']))
		{
			$model->attributes=$_POST['Rulesservices'];

			if($model->save()){
				$this->redirect(array('admin'));
			}
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax'])){
			$this->redirect(array('admin'));
		}
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$this->loadJQuery=false;
		$model=new Rulesservices('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Rulesservices'])){
			$model->attributes=$_GET['Rulesservices'];
		}
		if(isset($_GET['criterio'])){
			$model->criterio=$_GET['criterio'];
		}
		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Rulesservices the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Rulesservices::model()->findByPk($id);
		$model=Rulesservices::normalize_dates($model);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Rulesservices $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='rulesservices-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
