<?php

class ActiveRecordLogableBehavior extends CActiveRecordBehavior
{
    private $_oldattributes = array();
 
    public function afterSave($event)
    {
        if (!$this->Owner->isNewRecord) {
            $newattributes = $this->Owner->getAttributes();
            $oldattributes = $this->getOldAttributes();            
			$result=json_encode($oldattributes);
			$log=new Recordlog;
			$log->action='update';
			$log->model=get_class($this->Owner);
			$log->id_model=$this->Owner->getPrimaryKey();
			$log->content=$result;
			$log->created_at=new CDbExpression('NOW()');
			$log->id_user=Yii::app()->user->id;
			$log->save();
        } 
    }
 
    public function afterDelete($event)
    {
		$newattributes = $this->Owner->getAttributes();
		$oldattributes = $this->getOldAttributes();              
		$result=json_encode($oldattributes);
		$log=new Recordlog;
        $log->action='delete';
        $log->model=get_class($this->Owner);
        $log->id_model=$this->Owner->getPrimaryKey();
        $log->content=$result;
        $log->created_at=new CDbExpression('NOW()');
        $log->id_user=Yii::app()->user->id;
        $log->save();
    }
 
    public function afterFind($event)
    {
        // Save old values
        $this->setOldAttributes($this->Owner->getAttributes());
    }
 
    public function getOldAttributes()
    {
        return $this->_oldattributes;
    }
 
    public function setOldAttributes($value)
    {
        $this->_oldattributes=$value;
    }
}