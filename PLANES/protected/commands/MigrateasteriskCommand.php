<?php

//C:\wamp64\www\ttsoffice>php protected/yiic migrateasterisk 2017-03-14

class MigrateasteriskCommand extends CConsoleCommand 
{
    public function run($args){
		Yii::import('application.controllers.AsteriskController');
		$controller_instance = new AsteriskController("Admin"); 
		$controller_instance->actionAdmin($args[0]);
    }
}