<?php
/**
 * The following variables are available in this template:
 * - $this: the CrudCode object
 */
?>
<?php echo "<?php\n"; ?>
/* @var $this <?php echo $this->getControllerClass(); ?> */
/* @var $model <?php echo $this->getModelClass(); ?> */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php echo "<?php \$form=\$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl(\$this->route),
	'method'=>'get',
)); ?>\n"; ?>
<div class="col-md-12" style="clear: both">
<div class="row">
<?php foreach($this->tableSchema->columns as $column): ?>
<?php
	$field=$this->generateInputField($this->modelClass,$column);
	if(strpos($field,'password')!==false)
		continue;
?>			
			<div class="col-md-3">
				<div class="row row_space">
					<?php echo "<?php echo \$form->label(\$model,'{$column->name}'); ?>\n"; ?>
					<?php echo "<?php echo ".Utils::generateActiveField($this->modelClass,$column)."; ?>\n"; ?>
				</div>
			</div>
<?php endforeach; ?>
</div>
	<div class="row buttons">
		<?php echo "<?php echo CHtml::submitButton('Buscar',array('class'=>'btn btn-default b_customize')); ?>\n"; ?>
	</div>
<br />
<?php echo "<?php \$this->endWidget(); ?>\n"; ?>
</div>
</div><!-- search-form -->