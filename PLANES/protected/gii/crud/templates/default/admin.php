<?php
/**
 * The following variables are available in this template:
 * - $this: the CrudCode object
 */
?>
<?php echo "<?php\n"; ?>
/* @var $this <?php echo $this->getControllerClass(); ?> */
/* @var $model <?php echo $this->getModelClass(); ?> */

Yii::app()->clientScript->registerScript('search', "
	search();
	searchAdd();
	deleteBtn();
");
?>
<section class="content-header">
	<?php echo "<?php\n"; ?>
	echo Yii::app()->user->Title();
	echo Yii::app()->user->Breadcrumb();
	<?php echo "?>"; ?>
	<p class="registros">(<?php echo "<?="; ?>$model->count();<?php echo "?>"; ?> registros encontrados)</p>
</section>
<section class="content">
	<div class="box box-danger">
		<div class="box-header with-border">	
			<?php echo "<?php"; ?>
			if(Yii::app()->user->checkAccess('create')){
				echo '<a href="'.Yii::app()->request->baseUrl.'/'.Yii::app()->controller->id.'/create" class="btn btn-default b_customize agregar"><i class="fa fa-plus i_customize"></i> Agregar</a>';
			}
			<?php echo "?>"; ?>
			<?php echo "<?php echo Utils::SearchObject(); ?>"; ?>
			<?php echo "<?php echo CHtml::link('Búsqueda avanzada','#',array('class'=>'search-button')); ?>"; ?>

			<div class="search-form" style="display:none">
			<?php echo "<?php \$this->renderPartial('_search',array(
				'model'=>\$model,
			)); ?>\n"; ?>
			</div><!-- search-form -->

			<?php echo "<?php"; ?> $this->widget('zii.widgets.grid.CGridView', array(
				'id'=>'<?php echo $this->class2id($this->modelClass); ?>-grid',
				'pager' => array(
					'cssFile'=>false,
					'header'=> '',
					'firstPageLabel' => '<i class="fa fa-forward fa-flip-horizontal" data-toggle="tooltip" title="Primera página"></i>',
					'prevPageLabel'  => '<i class="fa fa-caret-right fa-flip-horizontal" data-toggle="tooltip" title="Anterior"></i>',
					'nextPageLabel'  => '<i class="fa fa-caret-right" data-toggle="tooltip" title="Siguiente"></i>',
					'lastPageLabel'  => '<i class="fa fa-forward" data-toggle="tooltip" title="Última página"></i>', 
				),
				'afterAjaxUpdate'=> 'function(){
					enableSwitch();
					deleteBtn();
					search();
					searchAdd();
				}',					
				'summaryText' => '', 
				'itemsCssClass' => 'table table-bordered table-striped table-list',	
				'dataProvider'=>$model->search(),
				'columns'=>array(
			<?php
			$count=0;
			foreach($this->tableSchema->columns as $column)
			{
				if($column->name=='enabled'){
				?>	
					array(
						'name' => 'enabled',
						'type' => 'raw',
						'value' => 'Utils::activeSwitch($data)',
						'htmlOptions'=>array('class'=>'activeSwitch'),
					),						
				<?php
				}
			}			
			foreach($this->tableSchema->columns as $column)
			{
				if($column->name!='id' && $column->name!='created_at' && $column->name!='id_user'){
					if(++$count==7){
						echo "\t\t/*\n";
					}
					if($column->name!='enabled'){
						echo "\t\t'".$column->name."',\n";						
					}
				}
			}
			if($count>=7)
				echo "\t\t*/\n";
			?>
					array(
						'class'=>'CButtonColumn',
						'template'=>'{view}{update}{erase}',	
						'buttons'=>array(		
							'view' => array(
								'label'=>'<i class="fa fa-eye icon_cbutton"></i>',
								'url'=>'Yii::app()->createUrl(strtolower(Yii::app()->controller->id).\'/view/id/\'. $data->id)',
								'visible'=>'Yii::app()->user->checkAccess(\'view\')',
								'options'=>array('title'=>'Ver','data-toggle'=>'tooltip'),
								'imageUrl'=>false,
							),						
							'update' => array(
								'label'=>'<i class="fa fa-edit icon_cbutton"></i>',
								'visible'=>'Yii::app()->user->checkAccess(\'update\')',
								'url'=>'Yii::app()->createUrl(strtolower(Yii::app()->controller->id).\'/update/id/\'. $data->id)',
								'options'=>array('title'=>'Editar','data-toggle'=>'tooltip'),
								'imageUrl'=>false,
							),					
							'erase' => array(
								'label'=>'<i class="fa fa-trash-o icon_cbutton"></i>',
								'url'=>'Yii::app()->createUrl(strtolower(Yii::app()->controller->id).\'/delete/id/\'. $data->id)',
								'visible'=>'Yii::app()->user->checkAccess(\'delete\')',
								'options' => array(
									'class' => 'delete_action',
									'title'=>'Borrar',
									'imageUrl'=>false,
									'data-toggle'=>'tooltip'
								)	
							),
						),
					),
				)
			)); ?>
		</div>
	</div>
 </section>
<?php echo "<?php "; ?>
Yii::app()->clientScript->registerCssFile(Yii::app()->params['cdnUrl'].'/plugins/lcswich/lc_switch-tts.css'); 
Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/lcswich/lc_switch.js',CClientScript::POS_END);
Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/lcswich/lc_switch-tts.js',CClientScript::POS_END);
<?php echo " ?>"; ?>