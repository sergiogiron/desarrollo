<section class="content-header">
  <h1>
	<?php echo $this->modelClass; ?>
	<small>Ver</small>
  </h1>
  <ol class="breadcrumb">
	<li><a href="<?php echo Yii::app()->request->baseUrl; ?>"><i class="fa fa fa-home"></i> Home</a></li>
	<li><a href="<?php echo Yii::app()->request->baseUrl; ?>/<?php echo strtolower($this->modelClass); ?>"><?php echo $this->modelClass; ?></a></li>
	<li class="active"><a href="<?php echo Yii::app()->request->baseUrl; ?>/<?php echo strtolower($this->modelClass); ?>/create">Crear</a></li>
  </ol>
</section>
<section class="content">
	<div class="box box-danger">
		<div class="box-header with-border">
			<?php echo "<?php \$this->renderPartial('_form', array('model'=>\$model)); ?>"; ?>
		</div>
	</div>
</section>
