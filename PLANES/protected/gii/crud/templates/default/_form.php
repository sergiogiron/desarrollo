<?php
/**
 * The following variables are available in this template:
 * - $this: the CrudCode object
 */
?>
<?php echo "<?php\n"; ?>
/* @var $this <?php echo $this->getControllerClass(); ?> */
/* @var $model <?php echo $this->getModelClass(); ?> */
/* @var $form CActiveForm */
?>
<?php echo "<?php \$form=\$this->beginWidget('CActiveForm', array(
	'id'=>'".$this->class2id($this->modelClass)."-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>\n"; ?>
<div class="box-body">
	<span class="right_f">
<?php	
	foreach($this->tableSchema->columns as $column)
	{	
		if($column->name=='enabled'){
			echo "<?php echo ".Utils::generateActiveField($this->modelClass,$column)."; ?>\n";
		}		
	}
?>	
	</span>

<div class="col-md-12 col-data">
<?php
$datepicker=0;
$datetimepicker=0;
$textarea=0;
foreach($this->tableSchema->columns as $column)
{
	if($column->name!='enabled' && $column->name!='created_at' && $column->name!='id_user'){
		if(($column->dbType=='date' and  $column->name!='created_at') || ($column->dbType=='datetime' and  $column->name!='created_at')){
			if($column->dbType=='datetime'){
				$datetimepicker=1;
			}
			$datepicker=1;
		}
		if($column->dbType=='longtext'){
			$textarea=1;
		}
	if($column->autoIncrement)
		continue;
if($column->allowNull==''){
	$required='required';
}else{
	$required='';
}
?>
<div class="form-group col-md-6 <?php echo $required; ?>">
	<?php echo "<?php echo ".$this->generateActiveLabel($this->modelClass,$column)."; ?>\n"; ?>
	<?php if($column->dbType=='date' and  $column->name!='created_at' || $column->dbType=='datetime' and  $column->name!='created_at' ){ echo '<div class="input-group">';} ?>
	<?php if($column->dbType=='varchar(35)' || $column->dbType=='varchar(80)' || $column->dbType=='varchar(260)'){ echo '<div class="input-group">';  } ?>
	<?php echo "<?php echo ".Utils::generateActiveField($this->modelClass,$column)."; ?>\n"; ?>
	<?php if($column->dbType=='varchar(260)'){ echo '<div class="input-group-addon pointer"><i class="fa fa-globe"></i></div>'; } ?>
	<?php if($column->dbType=='varchar(35)'){ echo '<div class="input-group-addon pointer"><i class="fa fa-phone"></i></div>'; } ?>
	<?php if($column->dbType=='varchar(80)'){ echo '<div class="input-group-addon pointer"><i class="fa fa-envelope-o"></i></div>'; } ?>
	<?php if($column->dbType=='varchar(35)' || $column->dbType=='varchar(80)' || $column->dbType=='varchar(260)'){ echo '</div>'; }  ?>
	<?php if($column->dbType=='date' and  $column->name!='created_at' || $column->dbType=='datetime' and  $column->name!='created_at' ){ echo '<div class="input-group-addon pointer"><i class="fa fa-calendar" data-toggle="tooltip" title="Abrir el calendario"></i></div>'; } ?>
	<?php if($column->dbType=='date' and  $column->name!='created_at' || $column->dbType=='datetime' and  $column->name!='created_at' ){ echo '</div>';} ?>
	<?php echo "<?php echo \$form->error(\$model,'{$column->name}'); ?>\n"; ?>
</div>
<?php if($column->dbType=='datetime'){?>		
	<div class="form-group col-md-6">		
		<div class="bootstrap-timepicker">
			<div class="form-group">
				<label for="Profile_title" class="required">Hora<span class="required">*</span></label>
				<div class="input-group">
					<input type="text" name="<?=$column->name?>_hour" id="<?=$column->name?>_hour" class="form-control timepicker" value="<?php echo '<?php if(isset($model->criterio["'.$column->name.'_hour"])){ echo $model->criterio["'.$column->name.'_hour"]; } ?>'; ?>" > 
					<div class="input-group-addon">
						<i class="fa fa-clock-o"  data-toggle="tooltip" title="Abrir el reloj"></i>
					</div>
				</div>
			</div>
		</div>	
	</div>
<?php
	}
	}
}
?>
<div class="box-footer">
		<?php echo "<?php\n"; ?>
		$title = (Yii::app()->controller->action->id!='view') ? 'Volver sin guardar cambios' : 'Volver';
		echo '<a href="'.Yii::app()->request->baseUrl.'/'.strtolower(Yii::app()->controller->id).'" class="btn btn-secundary" data-toggle="tooltip" title="'.$title.'" >Cerrar</a>';
		if(Yii::app()->controller->action->id!='view'){
			echo CHtml::submitButton('Guardar',array('class'=>'btn btn-primary','data-toggle'=>'tooltip','title'=>'Guardar cambios'));
		}
		<?php echo "?>"; ?>
</div>
<?php echo "<?php \$this->endWidget(); ?>\n"; ?>

</div><!-- form -->
</div>
<?php 
echo "<?php
	Yii::app()->clientScript->registerCssFile(Yii::app()->params['cdnUrl'].'/plugins/lcswich/lc_switch-tts.css'); 
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/lcswich/lc_switch.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/lcswich/lc_switch-tts.js',CClientScript::POS_END);
";
?>
<?php if($datepicker==1 ){
echo "	
	Yii::app()->clientScript->registerCssFile(Yii::app()->params['cdnUrl'].'/plugins/datepicker/datepicker3.css');
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/datepicker/bootstrap-datepicker.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/input-mask/jquery.inputmask.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/input-mask/jquery.inputmask.date.extensions.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/datepicker/datepicker-tts.js',CClientScript::POS_END);
	";
}
if($datetimepicker==1){
echo "
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/timepicker/bootstrap-timepicker.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/input-mask/jquery.inputmask.bundle.js',CClientScript::POS_END);
	Yii::app()->clientScript->registerCssFile(Yii::app()->params['cdnUrl'].'/plugins/timepicker/bootstrap-timepicker.css');
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/timepicker/timepicker-tts.js',CClientScript::POS_END);
	";
}
if($textarea==1){
echo "
	Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/plugins/ckeditor-standard/ckeditor.js',CClientScript::POS_END);
";
}


echo "
echo '<script>viewPage = \"'.Yii::app()->controller->action->id.'\";</script>';
";

echo "Yii::app()->clientScript->registerScriptFile(Yii::app()->params['cdnUrl'].'/js/form-validators.js',CClientScript::POS_END)";

echo " ?> 
";
?>
<script>
$( document ).ready(function() {
	<?php echo "<?php if(Yii::app()->controller->action->id=='view'){ echo \"$('form').find('input, textarea, button, checkbox, select').attr('disabled','disabled');\"; } ?>" ?>
<?php if($datepicker==1 || $datetimepicker==1){
echo "
	$('[data-mask]').inputmask('dd/mm/yyyy', {'placeholder': 'dd/mm/aaaa'});
	$('.fa-calendar').datepicker().on('changeDate', function(e) {
		$(this).parent().prev().val(e.format('dd/mm/yyyy'));
	    $(this).datepicker('hide');
    });";
}
if($datetimepicker==1){
echo "
	$('.timepicker').timepicker({
      showInputs: false,
	  showMeridian:false
    });
	$('.timepicker').inputmask(
        'hh:mm', 
        {
            mask: 'h:s',
            placeholder: 'hh:mm',
            alias: 'datetime',
			insertMode:false, 
        }
     ); 	
	$('.fa-clock-o').on('click', function() {
		$(this).parent().prev().trigger('click');
    });	
";
}	
?>
});		
</script>