<section class="content-header">
	<?php echo "<?php\n"; ?>
	echo Yii::app()->user->Title();
	echo Yii::app()->user->Breadcrumb();
	<?php echo "?>"; ?>
</section>
<section class="content">
	<div class="box box-danger">
		<div class="box-header with-border">
			<?php echo "<?php \$this->renderPartial('_form', array('model'=>\$model)); ?>"; ?>
		</div>
	</div>
</section>