<?php
/**
 * This is the template for generating the model class of a specified table.
 * - $this: the ModelCode object
 * - $tableName: the table name for this class (prefix is already removed if necessary)
 * - $modelClass: the model class name
 * - $columns: list of table columns (name=>CDbColumnSchema)
 * - $labels: list of attribute labels (name=>label)
 * - $rules: list of validation rules
 * - $relations: list of relations (name=>relation declaration)
 */
?>
<?php echo "<?php\n"; ?>

/**
 * This is the model class for table "<?php echo $tableName; ?>".
 *
 * The followings are the available columns in table '<?php echo $tableName; ?>':
 * @property  public $criterio;
<?php foreach($columns as $column): ?>
 * @property <?php echo $column->type.' $'.$column->name."\n"; ?>
<?php endforeach; ?>
<?php if(!empty($relations)): ?>
 *
 * The followings are the available model relations:
<?php foreach($relations as $name=>$relation): ?>
 * @property <?php
	if (preg_match("~^array\(self::([^,]+), '([^']+)', '([^']+)'\)$~", $relation, $matches))
    {
        $relationType = $matches[1];
        $relationModel = $matches[2];

        switch($relationType){
            case 'HAS_ONE':
                echo $relationModel.' $'.$name."\n";
            break;
            case 'BELONGS_TO':
                echo $relationModel.' $'.$name."\n";
            break;
            case 'HAS_MANY':
                echo $relationModel.'[] $'.$name."\n";
            break;
            case 'MANY_MANY':
                echo $relationModel.'[] $'.$name."\n";
            break;
            default:
                echo 'mixed $'.$name."\n";
        }
	}
    ?>
<?php endforeach; ?>
<?php endif; ?>
 */
class <?php echo $modelClass; ?> extends <?php echo $this->baseClass."\n"; ?>
{
	public $criterio;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '<?php echo $tableName; ?>';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
		<?php
		$date=array();
		$datetime=array();
		$email=array();
		$url=array();
		foreach($columns as $name=>$column)
		{
			if($column->dbType=='date'){
				$date[]=$column->name;
			}
			if($column->dbType=='datetime' and  $column->name!='created_at'){
				$datetime[]=$column->name;
			}
			if($column->size==80){
				$email[]=$column->name;
			}	
			if($column->size==260){
				$url[]=$column->name;
			}			
		}
		if(count($date)>0){ echo "\t";?>
array('<?php echo implode(', ', $date); ?>', 'type', 'type' => 'date', 'message' => '{attribute}: no es una fecha', 'dateFormat' => 'dd/mm/yyyy'),
		<?php }	
		if(count($datetime)>0){ echo "\t";?>
array('<?php echo implode(', ', $datetime); ?>', 'date', 'format'=>'dd/mm/yyyy hh:mm:ss', 'message'=>'{attribute}: no es una fecha'),
		<?php }
		if(count($email)>0){ echo "\t";?>
array('<?php echo implode(', ', $email); ?>', 'email', 'message'=>'{attribute}: no es un email válido'),
		<?php }		
		if(count($url)>0){ echo "\t";?>
array('<?php echo implode(', ', $url); ?>', 'url', 'message'=>'{attribute}: no es una url válida'),
		<?php }?>			
		<?php 
		foreach($rules as $rule): ?>
		<?php 
		if(strpos($rule,'required')){
			echo str_replace("'required'),","'required','message'=>'Debe completar este campo.'),",$rule.",\n");
		}else{
			echo $rule.",\n";
		}	
		?>
		<?php endforeach; ?>
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('<?php echo implode(', ', array_keys($columns)); ?>', 'safe', 'on'=>'search'),
		);
	}

	public function behaviors(){
		return array(
			// Classname => path to Class
			'ActiveRecordLogableBehavior'=>
				'application.behaviors.ActiveRecordLogableBehavior',
		);
	}

	/**
	 * @return array relational rules.
	 */
	
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			<?php
				foreach($relations as $name=>$relation){
					if (preg_match("~^array\(self::([^,]+), '([^']+)', '([^']+)'\)$~", $relation, $matches)){	
						$model= new $matches[2];
						$tabla=$model->tableName();
					}
					$relation=str_replace(")",",'alias'=>'$tabla')",$relation);
					echo "'$name' => $relation,\n";
				}
			?>
		<?php echo "\n\t\t"; ?> );
	}	

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
<?php foreach($labels as $name=>$label): ?>
			<?php echo "'".$name."' => '".str_replace("'","\'",$label)."',\n"; ?>
<?php endforeach; ?>
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		
		if($this->criterio!=''){
<?php
foreach($columns as $name=>$column)
{
	echo "\t\t\t\$criteria->addSearchCondition('$name',\$this->criterio,true,'OR', 'LIKE');\n";
}
?>	
		}else{	
<?php
foreach($columns as $name=>$column)
{
	if($column->type==='string')
	{
		echo "\t\t\t\$criteria->compare('$name',\$this->$name,true);\n";
	}
	else
	{
		echo "\t\t\t\$criteria->compare('$name',\$this->$name);\n";
	}
}
?>
		}
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	protected function beforeValidate(){
		<?php foreach($columns as $column): ?>
		<?php  if($column->dbType=='datetime' and  $column->name!='created_at'){?>
		if($this-><?=$column->name?>!='' && $_POST['<?=$column->name?>_hour']!=''){
			$this-><?=$column->name?> = date_create_from_format("d/m/Y H:i:s",$this-><?=$column->name?>.' '.$_POST['<?=$column->name?>_hour'].':00');
			$this-><?=$column->name?> =date_format( $this-><?=$column->name?>,"d/m/Y H:i:s");
		}
		<?php } ?>		
		<?php endforeach; ?>
		return parent::beforeValidate();
    }	
	
	public static function normalize_dates($model){
		$model->criterio=array();
		<?php foreach($columns as $column): ?>
		<?php  if($column->dbType=='datetime' and  $column->name!='created_at'){?>
		$<?=$column->name?>= explode(" ",$model-><?=$column->name?>);
		$model-><?=$column->name?>=$<?=$column->name?>[0];
		$model->criterio=array_merge($model->criterio,array('<?=$column->name?>_hour'=>$<?=$column->name?>[1]));
		<?php } ?>	
		<?php endforeach; ?>
		return $model;		
	}
	
	protected function afterFind() {
		<?php foreach($columns as $column): ?>
		<?php  if($column->dbType=='date'){?>
		$this-><?=$column->name?> =Utils::date_spa($this-><?=$column->name?>);
		<?php } ?>		
		<?php  if($column->dbType=='datetime' and  $column->name!='created_at'){?>
		$this-><?=$column->name?> =Utils::datetime_spa($this-><?=$column->name?>);
		<?php } ?>			
		<?php endforeach; ?>				
		return parent::afterFind();
	}	
	
	protected function beforeSave ()
    {	
		$this->created_at=date("Y-m-d H:i:s");
	<?php foreach($columns as $column): ?>
		<?php  if($column->dbType=='date'){ ?>
		$this-><?=$column->name?> = date_create_from_format("d/m/Y",$this-><?=$column->name?>);
		$this-><?=$column->name?> =date_format( $this-><?=$column->name?>,"Y-m-d");		  
		<?php } ?>
		<?php  if($column->dbType=='datetime' and  $column->name!='created_at'){ ?>
		$date_<?=$column->name?> = DateTime::createFromFormat('d/m/Y H:i:s', $this-><?=$column->name?>);
		$this-><?=$column->name?>=$date_<?=$column->name?>->format('Y-m-d H:i:s');
		<?php } ?>
		<?php endforeach; ?>
		foreach($this->attributes as $key=>$value){
			if($value==''){
				$this->$key=NULL;
			}
		}			
		return parent::beforeSave ();
    }	
	
<?php if($connectionId!='db'):?>
	/**
	 * @return CDbConnection the database connection used for this class
	 */
	public function getDbConnection()
	{
		return Yii::app()-><?php echo $connectionId ?>;
	}

<?php endif?>
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return <?php echo $modelClass; ?> the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
