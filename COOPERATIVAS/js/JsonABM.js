
var currentLocation = window.location;
$(function(){
	
	
  //Buscamos Beneficiarios
  $("#BuscaBeneficiario").autocomplete({
	  source: function(request, response) {
        $.ajax({
			type: 'post',
          url: baseurl + 'adjudicaciones/listarBeneficiarios',
               dataType: "json",
          data: {
            term : request.term,
            Ciudad : valorSeleccionado,
			programa:programaSelecionado,
          },
          success: function(data) {
            response(data);
          }
        });
		 minLength: 2
      },
           
             select: function(event, ui) {  
				$('#nombre_beneficiarios').val(ui.item.nombre_beneficios);
				$('#bene_codigo').val(ui.item.id);
				$('#dni').val(ui.item.documento);
				$('#ciudad_bene').val(ui.item.localidad);
				$('#barrio').val(ui.item.barrio);
				$('#mza').val(ui.item.mza);
				$('#lote').val(ui.item.lote);
				$('#obs').val(ui.item.obs);
				$('#id_ciudad').val(ui.item.id_localidad);
				
				
           }  
		   
    });
	
	
});
















function CambiaBene() { 
$('#BuscaBeneficiario').prop("disabled", false);
$('#estadobene').prop("disabled", false);
$('#beneficiarioActObs').prop("disabled", false);
$('#btnconfirmabene').prop("disabled", false);
document.getElementById('estadobene').style.display = 'block';
document.getElementById('beneficiarioActObs').style.display = 'block';
document.getElementById('BuscaBeneficiario').style.display = 'block';
document.getElementById('btnconfirmabene').style.display = 'block';
}


function SelecionarBeneficiario()
{
	$('#btnCambiobene').prop("disabled", true);
	$('#estadobene').prop("disabled", true);
	$('#beneficiarioActObs').prop("disabled", true);
	$('#BuscaBeneficiario').prop("disabled", true);
	$('#btnconfirmabene').prop("disabled", true);
	document.getElementById('updateBene').style.display = 'block';

}



function UpdateBene()
{
	$('#mensaje').html( '<center><img src="'+ baseurl +'images/load16.gif" alt="procesando" title="procesando" /></center>' );
$('#updateBene').prop("disabled", true);
//alert (beneficiario_id);
var estado=document.getElementById("estadobene").value;
var observacion=document.getElementById("beneficiarioActObs").value;
var beneficiario2_id = $('#bene_codigo').val();
$.ajax({
                type: 'post',
                url: baseurl + 'adjudicaciones/UpdateBene',
				data: {
			 BeneficiarioOld:beneficiario_id,
			 Estado:estado,
			 Observacion:observacion,
             BeneficiarioNvo : beneficiario2_id,
			 Resolucion:resolucion,
			 
			 
          },
           
	   
				});


 
 $('#mensaje').html("<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Informacion Guardada Correctamente</div>");
 document.getElementById('updateBene').remove();
 document.getElementById('btnconfirmabene').remove();
 document.getElementById('btnCambiobene').remove();
 
}

function cambioFecha()
{
	$('#fecha2').prop("disabled", false);
	document.getElementById('updatefecha').style.display = 'block';
}

function Updatefecha()
{
	var fecha =document.getElementById("fecha2").value; 
	$.ajax({
                type: 'post',
                url: baseurl + 'adjudicaciones/Updatefecha',
				data: {
			 Resolucion:resolucion,
			 Fecha:fecha,
			 
			 
          },
		  });
		  $('#mensaje').html("<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Informacion Guardada Correctamente</div>");
		  document.getElementById('updatefecha').remove();
 document.getElementById('btnCambiofecha').remove();
$('#fecha2').prop("disabled", true);
	
}

function cambiaNota()
{
	document.getElementById('notaNvo').style.display = 'block';
document.getElementById('updateNota').style.display = 'block';

}

function cambiaAnio()
{
	document.getElementById('anioNvo').style.display = 'block';
document.getElementById('updateanio').style.display = 'block';

}

function UpdateAnio()
{
	var anio =document.getElementById("anioNvo").value; 
	document.getElementById('anioNvo').style.display = 'block';
	$.ajax({
                type: 'post',
                url: baseurl + 'adjudicaciones/UpdateAnio',
				data: {
			 Resolucion:resolucion,
			 Anio:anio,
			 
			 
          },
		  });
		  $('#mensaje').html("<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Informacion Guardada Correctamente</div>");
		   document.getElementById('btnCambioanio').remove();
 document.getElementById('updateanio').remove();
$('#anioNvo').prop("disabled", true);
}




function UpdateNota()
{
	document.getElementById('notaNvo').style.display = 'block';
	var nota =document.getElementById("notaNvo").value; 
	$.ajax({
                type: 'post',
                url: baseurl + 'adjudicaciones/UpdateNota',
				data: {
			 Resolucion:resolucion,
			 Nota:nota,
			 
			 
          },
		  });
	 $('#mensaje').html("<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Informacion Guardada Correctamente</div>");
		  document.getElementById('btnCambionota').remove();
 document.getElementById('updateNota').remove();
$('#notaNvo').prop("disabled", true);
	
}

function cambioExpte()
	{
		
		document.getElementById('expteNvo').style.display = 'block';
document.getElementById('updateExpte').style.display = 'block';
	}
	
function UpdateExpte()
{
	
	document.getElementById('notaNvo').style.display = 'block';
	var expte =document.getElementById("expteNvo").value; 
	$.ajax({
                type: 'post',
                url: baseurl + 'adjudicaciones/UpdateExpte',
				data: {
			 Resolucion:resolucion,
			 Expte:expte,
			 
			 
          },
		  });
	 $('#mensaje').html("<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Informacion Guardada Correctamente</div>");
	 document.getElementById('btnCambioexpte').remove();
 document.getElementById('updateExpte').remove();
$('#expteNvo').prop("disabled", true);
}


function Eliminar(){

$.ajax({
                type: 'post',
                url: baseurl + 'adjudicaciones/Eliminar',
				data: {
			 Resolucion:resolucion,
			 Beneficiario:beneficiario_id,
			 Programa:programaSelecionado,
			 Cuidad:valorSeleccionado,
			 Cooperativa:cooperativa,
			 
			 
          },
		  });
	
	
	$('#mensaje').html("<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Adjudicacion Eliminada Correctamente, la operacion se completara en 5 segundos </div>");
window.location=baseurl+'adjudicaciones/todo';

}




/* $('#btneliminar').click(function Eliminar() {
	
         $.ajax({
                type: 'post',
                url: baseurl + 'adjudicaciones/Eliminar',
				data: {
			 Resolucion:resolucion,
			 Beneficiario:beneficiario_id,
			 Programa:programaSelecionado,
			 Cuidad:valorSeleccionado,
			 Cooperativa:cooperativa,
			 
			 
          },
		  });
	
	
	$('#mensaje').html("<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Adjudicacion Eliminada Correctamente , la operacion se completara en 5 segundos <meta http-equiv='refresh' content='5' URL=window.location></div>");
    });
	
	 */






$(document).ready(function(){
$('#beneficiarioActObs').prop("disabled", true);
	$('#estadobene').prop("disabled", true);
	$('#BuscaBeneficiario').prop("disabled", true);
	$('#btnconfirmabene').prop("disabled", true);
	document.getElementById('updateBene').style.display = 'none';
document.getElementById('updatefecha').style.display = 'none';
document.getElementById('notaNvo').style.display = 'none';
document.getElementById('updateNota').style.display = 'none';
document.getElementById('anioNvo').style.display = 'none';
document.getElementById('updateanio').style.display = 'none';
document.getElementById('expteNvo').style.display = 'none';
document.getElementById('updateExpte').style.display = 'none';
document.getElementById('estadobene').style.display = 'none';
document.getElementById('beneficiarioActObs').style.display = 'none';
document.getElementById('BuscaBeneficiario').style.display = 'none';
document.getElementById('btnconfirmabene').style.display = 'none';

}); 
