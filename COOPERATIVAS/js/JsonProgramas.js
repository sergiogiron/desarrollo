$(document).ready(function(){
		$("#Nombre").focus();
		
		$("form#formulario").submit(function() //Nombre del Formulario para nuevo programa
		{
				var Prog		= new Object(); //Crea el objeto Prog
				Prog.Id          = $('input#id').val(); //Obtiene el id ingresado
				Prog.Nombre      = $('input#Nombre').val(); //Obtiene el nombre ingresado
		    	Prog.Tipo        = $('select#tipo').val();  //Obtiene el Tipo de Programa seleccionado
				Prog.Cantidad     = $('input#Cantidad').val(); //Obtiene la Cantidad ingresada
				 
				//Muestra proceso de guardado...
				$("#mensaje").append("<div class='modal1'><div class='center1'> <center> <img src='"+ baseurl +"/img/gif-load.gif'> Guardando Informacion...</center></div></div>");
				var DatosJson = JSON.stringify(Prog); //Guardo los datos obtenidos como una cadena Json
				
				$.post(baseurl + 'programas/savePrograma', //Envío los datos obtenidos al controlador
					{ 
						ProgramasPost: DatosJson
					},
					function(data, textStatus) {
						$("#"+data.campo+"").focus();
						$("#mensaje").html(data.error_msg);
					}, 
					"json"		
				);
				return false;		
			
	});

});