$(document).ready(function(){
		$("#Reclamo").focus(); //nombre del textarea...
		
			//Cargamos Coopertativa 
	var cooperativa = $("#cooperativa");
	cooperativa.append("<option value='0'>---Elige Cooperativa---</option>");
	$.getJSON( baseurl + "cooperativas/coope",function(objetosretorna1){
		$.each(objetosretorna1, function(i,ObjetoReturn1){
			var seleccion3 = "";
			if(idcoope==ObjetoReturn1.id){
				seleccion3 = "selected='selected'";
			}
			var nuevaFila = "<option value='"+ObjetoReturn1.id+"' "+seleccion3+">" + ObjetoReturn1.razonsocial+"  "+"MATRICULA: " + ObjetoReturn1.matricula+"    </option>";
			cooperativa.append(nuevaFila);
		});
	}); 	

		$("form#formulario").submit(function() //Nombre del Formulario para nueva denuncia
		{
				var denuncia			= new Object(); //Crea el objeto denuncia
				denuncia.Id          	= $('input#id').val(); //Obtiene el id ingresado
				denuncia.Denunciante    = $('input#denunciante').val();
				denuncia.Cooperativa	= $('select#cooperativa').val();	//Obtiene la cooperativa seleccionada
				denuncia.Reclamo      	= $('textarea#reclamo').val(); 		//Obtiene el reclamo ingresado
		    	denuncia.Fecha       	= $('input#fecha').val();  		 	//Obtiene fecha de una denuncia
		    	denuncia.Imagen			= $('input#imagen').val();		 	//Obtiene imagen de una denuncia
				denuncia.Observaciones  = $('input#observaciones').val();	//Obtiene observaciones de una denuncia
				denuncia.Documento      = $('input#documento').val();
				 

				//Muestra proceso de guardado...
				$("#mensaje").append("<div class='modal1'><div class='center1'> <center> <img src='"+ baseurl +"/img/gif-load.gif'> Guardando Informacion...</center></div></div>");
				var DatosJson = JSON.stringify(denuncia); //Guardo los datos obtenidos como una cadena Json
				
				$.post(baseurl + 'denuncias/saveDenuncias', //Envío los datos obtenidos al controlador
					{ 
						DenunciasPost: DatosJson
					},
					function(data, textStatus) {
						$("#"+data.campo+"").focus();
						$("#mensaje").html(data.error_msg);
					}, 
					"json"		
				);
				return false;		
			
	});

});