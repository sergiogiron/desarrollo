var valorSeleccionado;
var prog = new Object();
var programaSelecionado;
var valores;
var bandera;
var Beneficiario = new Array();
var CiudadesBene = new Array();
var personas =new Array();
var contador=0;
var numero=0;
var restantes=0;
var nota=0;
var currentLocation = window.location;
$(function(){
	
	
//busca cooperativa dependiendo del termino escrito		
  $("#BuscaCoperativa").autocomplete({

	  source: function(request, response) {
        $.ajax({
			  type: 'post',
          url: baseurl + 'adjudicaciones/buscarCooperativa',
               dataType: "json",
          data: {
            term : request.term,
            Ciudad : $('#localidad').val(),
			bandera:bandera,
	
          },
          success: function(data) {
            response(data);
          }
        });
		 minLength: 2
      },
	  
             select: function(event, ui) {  
				$('#lblNombrecliente').text(ui.item.RAZONSOCIAL +' '+ ui.item.MATRICULA);
				$('#lblidcliente').text(ui.item.ID);
				$('#Cliente').val(ui.item.ID);
				$('#CupoCoperativa').val(ui.item.cupo);
				$('#AgregaProducto').prop("disabled", false);
				document.getElementById("BuscaBeneficiario").focus();
				$('#BuscaCoperativa').prop("disabled", true);
				$('#btnCamCop').prop("disabled", false);
				
				
				
           }  
		   
	
    });
	
	
	
	
	
	
	var localidad = $("#localidad");
	localidad.append("<option value='0'>---Elige Localidad---</option>");
	$.getJSON( baseurl + "adjudicaciones/localidades",function(objetosretorna1){
		$.each(objetosretorna1, function(i,ObjetoReturn1){
			var seleccion3 = "";
			var nuevaFila = "<option value='"+ObjetoReturn1.id+"' "+seleccion3+">" + ObjetoReturn1.nombre_localidad+"</option>";
			localidad.append(nuevaFila);
		});
	}); 
	
var programa = $("#programa");
	programa.append("<option value='0'>---Elige Programa---</option>");
	$.getJSON( baseurl + "adjudicaciones/programas",function(objetosretorna1){
		$.each(objetosretorna1, function(i,ObjetoReturn1){
			var seleccion3 = "";
			var nuevaFila = "<option value='"+ObjetoReturn1.id+"' "+seleccion3+">" + ObjetoReturn1.nombre_programa+"</option>";
			programa.append(nuevaFila);
		});
	}); 
	
  //Buscamos Beneficiarios
  $("#BuscaBeneficiario").autocomplete({
	  source: function(request, response) {
        $.ajax({
			type: 'post',
          url: baseurl + 'adjudicaciones/listarBeneficiarios',
               dataType: "json",
          data: {
            term : request.term,
            Ciudad : valorSeleccionado,
			programa:document.getElementById("programa").value,
          },
          success: function(data) {
            response(data);
          }
        });
		 minLength: 2
      },
           
             select: function(event, ui) {  
				$('#nombre_beneficiarios').val(ui.item.nombre_beneficios);
				$('#bene_codigo').val(ui.item.id);
				$('#dni').val(ui.item.documento);
				$('#ciudad_bene').val(ui.item.localidad);
				$('#barrio').val(ui.item.barrio);
				$('#mza').val(ui.item.mza);
				$('#lote').val(ui.item.lote);
				$('#obs').val(ui.item.obs);
				$('#id_ciudad').val(ui.item.id_localidad);
				
				
           }  
		   
    });
	
	
});

function traerDatos()
{
	var restantes=parseInt(document.getElementById("Restantes").value);
	var cantidad=parseInt(document.getElementById("cantidad").value);
	var cupos =parseInt(document.getElementById("CupoCiudad").value);
	var CupoCop= parseInt(document.getElementById("CupoCoperativa").value);
	
	if (cantidad>restantes || cantidad>cupos || cantidad>CupoCop  )
	{
		if (cantidad>restantes)
		{alert ("La cantidad Que eligio es Mayor Al Cupo Del Programa \n Por Seguridad Ud debera Cargar Nuevamente Los datos \n" +"el cupo restante es :" +restantes);}
	   if (cantidad>cupos)
	   { alert ("La cantidad Que eligio es Mayor Al Cupo De la ciudad \n Por Seguridad Ud debera Cargar Nuevamente Los datos \n" +"el cupo restante de la cuidad es  :" +cupos);                  }
      
	  
	  if  (cantidad>CupoCop  )
	  {
		  alert ("La cantidad Que eligio es Mayor Al Cupo De la Cooperativa \n Por Seguridad Ud debera Cargar Nuevamente Los datos \n" +"el cupo restante de la cooperativa  es :" +CupoCop);
		  
	  }
  window.location=baseurl+'adjudicaciones';
}
	
else	{

	//trae cod de adj
	$.ajax({
                type: 'get',
                url: baseurl + 'adjudicaciones/TraerCodigo',
           
			 }).done(function(resp){
				adjudica = eval(resp)
		var resultado = JSON.parse(resp);
       codigo=parseInt(resultado[0].id);
	   if (isNaN(codigo)){
	   codigo=1;
	    document.getElementById("ultimo").value=codigo;}
	   else
	   {  codigo=codigo+1;
	   document.getElementById("ultimo").value=codigo;
	   }
				});
	
	
	
	
	
	
	
	//trae datos de beneficiarios
            $('#resultado').html( '<img src="'+ baseurl +'images/load16.gif" alt="procesando" title="procesando" />' );
			
             $.ajax({
                type: 'post',
                url: baseurl + 'adjudicaciones/TraerDatos',
                data: {
            localidad :document.getElementById("localidad").value,
            cantidad : $('#cantidad').val(),
			programa :document.getElementById("programa").value,
          },
			 }).done(function(resp){
		
		valores = eval(resp);
		html="<table class='table table-bordered'><thead><tr><th>#</th><th>Nombre</th><th>DNI</th><th>Ciudad</th><th>Barrio</th><th>Manzana</th><th>Lote</th><th>Observacion</th></tr></thead><tbody>";
		for(i=0;i<valores.length;i++){
			html+="<tr><td>"+(i+1)+"</td><td>"+valores[i][1]+"</td><td>"+valores[i][2]+"</td><td>"+valores[i][9]+"</td><td>"+valores[i][4]+"</td><td>"+valores[i][5]+"</td><td>"+valores[i][6]+"</td><td>"+valores[i][7]+"</td></tr>";
			personas[i] =valores[i][0];
			
		}
		html+="</tbody></table>"
		$("#resultado").html(html);
		
	});     
         
    $("#btnGuardar").prop("disabled", false);
	document.getElementById('btnGuardar').style.visibility="visible";
}

}



function Adjudicar()
{
	ActualizarCupo(valores.length);  
	var Cop = document.getElementById("lblidcliente").innerHTML;
	var anio =document.getElementById("anio").value;
	var cantidad =valores.length;
	var id_adj=$('#ultimo').val();
	var Localidad = document.getElementById("localidad").value;
	var Programa = document.getElementById("programa").value;
	var fecha = document.getElementById("btnFecha").value;
		$.ajax({
                type: 'post',
                url: baseurl + 'adjudicaciones/Adjudicacion',
                data: {
			cantidad : cantidad,
			codigo: id_adj,
			coperativa:Cop,
			
          },
		  })
	//de adj  js para insertar los id de beneficiaarios
	 
 
   for(i=0;i<personas.length;i++){
	 
	    bene=personas[i];
	   
             $.ajax({
                type: 'post',
                url: baseurl + 'adjudicaciones/GuardarDatosAuto',
                data: {
             localidad : Localidad,
             programa : Programa,
		    cooperativa : Cop,
			codigo : id_adj,
			beneficiario : bene,
			anio:anio,
			nota:nota,
			impresion:fecha,
          },
		  })
		 
	nota=nota+1;	  
   }
  
	
  window.location=baseurl+'adjudicaciones';
  
  
  
}





function myFunction()
{
$('#cantidad').prop("disabled", false);
$("#cantidad").focus();
$('#localidad').prop("disabled", true);
$('#BuscaCoperativa').prop("disabled", false);
var lista = document.getElementById("localidad");
valorSeleccionado = lista.options[lista.selectedIndex].text;
            


$("#btnCamLoc").prop("disabled", false);
$('#cbo1').prop("disabled", true);
$('#Desbloquear').prop("disabled", true);


var Prog = document.getElementById("programa").value;
var Ciudad=document.getElementById("localidad").value;
$.ajax({
                type: 'post',
                url: baseurl + 'adjudicaciones/TraerCupoCiudad',
				data: {
             programa : Prog,
			 localidad :Ciudad,
          },
           
			 }).done(function(resp){
				 
				 try {
	var resultado = JSON.parse(resp);
    var  cupo=parseInt(resultado[0].cupo);
		diferencia=parseInt(resultado[0].diferencia_cupo)
		var total=cupo-diferencia;
			document.getElementById("CupoCiudad").value=total; }

catch(err) {
    alert ("no hay cupos en el programa  para esta cuidad");
	document.getElementById("CupoCiudad").value=0;
}
				 
		
	   
				});



}

function cambioCoop() { 
$('#BuscaCoperativa').prop("disabled", false);
document.getElementById("BuscaCoperativa").value="";
}


function CambiaPrograma()
{
	$('#programa').prop("disabled", false);
	document.getElementById("localidad").value="0";
}

function CambiarLoc()
{
$('#localidad').prop("disabled", false);
document.getElementById("BuscaCoperativa").value="";
}

function PromptDemo() {
var contr  = $('input#passw').val();

$.ajax({
                type: 'get',
                url: baseurl + 'adjudicaciones/Verificar',
            data: {
			pass : contr,
			
          },
			 }).done(function(resp){
		var resultado = JSON.parse(resp);
       
	   if (resultado.ID!=999){
	   alert ("la autorizacion es correcta");
	   
$('#Desbloquear').prop("disabled", true);
document.getElementById('Desbloquear').style.display = 'none';
document.getElementById('passw').style.display = 'none';
$('#manual').prop("disabled", false);
	   }
	    
	   else
	   {  
	   alert ("Clave Erronea");}
	   
				});



}


function myFunction2()
{
$('#localidad').prop("disabled", false);
$('#programa').prop("disabled", true);
var Prog = document.getElementById("programa").value;
$.ajax({
                type: 'post',
                url: baseurl + 'adjudicaciones/TraerCupo',
				data: {
             programa : Prog,
          },
           
			 }).done(function(resp){
		var resultado = JSON.parse(resp);
       codigo=parseInt(resultado[0].restantes);
	   document.getElementById("Restantes").value=codigo;
	   
				});






$("#btnCambioProg").prop("disabled", false);
}


function PedidoNum()
{
	$('#btneli').prop("disabled", false);
	$('#BuscaBeneficiario').prop("disabled", false);
$('#cantidad').prop("disabled", true);
$('#confirmar').prop("disabled", true);	
contador=document.getElementById("cantidad").value;
document.getElementById("beneres").value=contador;
restantes=contador;
$('#cantidad').prop("disabled", true);
if (document.getElementById("priv").value!=1){
$('#cbo1').prop("disabled", true);
$('#Desbloquear').prop("disabled", false);
$('#passw').prop("disabled", false);


}


}

function ControlBan()
{
	
	$.ajax({
                type: 'post',
                url: baseurl + 'adjudicaciones/TraeNota',
           
			 }).done(function(resp){
				adjudica = eval(resp)
		var resultado = JSON.parse(resp);
		
		try {
        anio=parseInt(resultado[0].anio);
		nota=parseInt(resultado[0].nro_nota);
		nota=nota+1;
		document.getElementById("nota").value=nota;
		 var fecha = new Date();
          anioActual = fecha.getFullYear();
		   if (anio!=anioActual) 
		  {
			  document.getElementById("anio").value=anioActual;
			  nota=1;
			  document.getElementById("nota").value=nota;
			  
		  }
		  else
		  {
			 document.getElementById("anio").value=anio;
			  
		  }
		
		
		
}
catch(err) {
        nota=1;
	    document.getElementById("nota").value=nota;
		 var fecha = new Date();
          anioActual = fecha.getFullYear()
		  document.getElementById("anio").value=anioActual;
}
      
	   
	   
				});
	
	
	
	
	if (bandera==1)
		//selecionado el checkbox
	{
		
		$('#btnAsignar').prop("disabled", true);
		$('#btnAgregarBene').prop("disabled", false);
		//trae cod de adj
	$.ajax({
                type: 'get',
                url: baseurl + 'adjudicaciones/TraerCodigo',
           
			 }).done(function(resp){
				adjudica = eval(resp)
		var resultado = JSON.parse(resp);
       codigo=parseInt(resultado[0].id);
	   if (isNaN(codigo)){
	   codigo=1;
	    document.getElementById("ultimo").value=codigo;}
	   else
	   {  codigo=codigo+1;
	   document.getElementById("ultimo").value=codigo;
	   }
				});
	}
	
	if(bandera==0){
		
		$('#btnAsignar').prop("disabled", false);
		
	}
	
}

function agregarBeneficiario()
{
var res=parseInt(document.getElementById("Restantes").value);
	var cantidad=parseInt(document.getElementById("cantidad").value);
	var cupos =parseInt(document.getElementById("CupoCiudad").value);
	var CupoCop= parseInt(document.getElementById("CupoCoperativa").value);
	
	
	if (cantidad>res||cantidad>cupos ||cantidad>CupoCop )
	{
		if (cantidad>res)
		{alert ("La cantidad Que eligio es Mayor Al Cupo Del Programa  \n Por Seguridad Ud debera Cargar Nuevamente Los datos  \n "+"el Cupo Restante es :"+res);}
	if (cantidad>cupos)
	{alert ("La cantidad Que eligio es Mayor Al Cupo De la ciudad \n Por Seguridad Ud debera Cargar Nuevamente Los datos \n" +"el cupo restante es :" +cupos);}

  if (cantidad>CupoCop)
  {alert ("La cantidad Que eligio es Mayor Al Cupo De la Cooperativa \n Por Seguridad Ud debera Cargar Nuevamente Los datos \n" +"el cupo restante es :" +CupoCop);}
	
	
	
      window.location=baseurl+'adjudicaciones';
}
	
else	{


document.getElementById("BuscaBeneficiario").value="";
restantes=restantes-1
document.getElementById("beneres").value=restantes;
var ciudadbene =$('#id_ciudad').val();
var ciudad=$('#ciudad_bene').val();
var nombre = $('#nombre_beneficiarios').val();
var dni = $('#dni').val();
var id = $('#bene_codigo').val();
var barrio = $('#barrio').val();
var mza = $('#mza').val();
var lote = $('#lote').val();
var obs = $('#obs').val();
CiudadesBene[numero]=ciudadbene;
Beneficiario[numero]=id;
numero=numero+1;
	
var nuevaFila =
							"<tr>"
							+"<td>"+numero+" </td>"
							+"<td>"+nombre+"</td>"
							+"<td>"+dni+"</td>"
							+"<td>"+ciudad+"</td>"
							+"<td>"+barrio+"</td>"
							+"<td> "+mza+"</td>"
							+"<td> "+lote+"</td>"
							+"<td> "+obs+"</td>"
			$(nuevaFila).appendTo("#mitabla");
		$("#buscarCooperativa").focus();
	if (restantes==0){
	document.getElementById('btnManual').style.visibility="visible";
	document.getElementById('btnAgregarBene').style.visibility="hidden";
	document.getElementById('beneres').style.visibility="hidden";
	
	}
}
}


function Guardado_Manual()
{    
	var cantidad =Beneficiario.length;
	var id_adj=$('#ultimo').val();
	var coo =document.getElementById("lblidcliente").innerHTML;
	$.ajax({
                type: 'post',
                url: baseurl + 'adjudicaciones/Adjudicacion',
                data: {
			cantidad : cantidad,
			codigo: id_adj,
			coperativa:coo,
			
          },
		  })
	
	var anio =document.getElementById("anio").value;
	var Programa = document.getElementById("programa").value;
	var Cop = document.getElementById("lblidcliente").innerHTML;
    var id_adj=$('#ultimo').val();	
	var fecha = document.getElementById("btnFecha").value;
	for (i = 0; i < Beneficiario.length; i++) { 
	 //alert (i);
	 $.ajax({
                type: 'post',
                url: baseurl + 'adjudicaciones/GuardarDatosAuto',
                data: {
             localidad : CiudadesBene[i],
             programa : Programa,
		    cooperativa : Cop,
			codigo : id_adj,
			beneficiario : Beneficiario[i],
			anio:anio,
			nota:nota,
			impresion:fecha,
          },
		  })
    nota=nota+1;
}
	 ActualizarCupo(Beneficiario.length);
	 window.location=baseurl+'adjudicaciones';
}

function ActualizarCupo(cupo){
	var localidad=document.getElementById("localidad").value;
	var programa = document.getElementById("programa").value;
	
	$.ajax({
                type: 'post',
                url: baseurl + 'adjudicaciones/ActualizaCupo',
                data: {
             cupo : cupo,
             programa : programa,
			 localidad:localidad,
          },
		  })
		  
		  
	
	
	
}


function EliminaPers (num)
{
	$('#mitabla tbody').empty();
	restantes =document.getElementById("cantidad").value;
	document.getElementById("beneres").value=restantes;
	Beneficiario=[];
	CiudadesBene=[];
	numero=0;
	document.getElementById('btnManual').style.visibility="hidden";
	document.getElementById('btnAgregarBene').style.visibility="visible";
	document.getElementById('beneres').style.visibility="visible";
}


function auto (){
document.getElementById('oculto1').style.display = 'block';	
document.getElementById('auto').style.display = 'none';	
document.getElementById('manual').style.display = 'none';	
bandera=0;
document.getElementById('btnAgregarBene').style.display = 'none';
document.getElementById('Desbloquear').style.display = 'none';
document.getElementById('passw').style.display = 'none';
document.getElementById('cbo1').style.display = 'none';
document.getElementById('beneres').style.display = ' none';
document.getElementById('BuscaBeneficiario').style.display = ' none';
document.getElementById('tdbeneficio').style.display = ' none';
document.getElementById('lbltexto').style.display = ' none';
document.getElementById('btneli').style.display = ' none';

}

function asiManual (){
bandera=1;

document.getElementById('oculto1').style.display = 'block';	
document.getElementById('auto').style.display = 'none';	
document.getElementById('manual').style.display = 'none';
document.getElementById('btnAsignar').style.display = 'none';
document.getElementById('Desbloquear').remove();
document.getElementById('passw').remove();
document.getElementById('lbltexto').style.display = 'none';
}


$(document).ready(function(){
$('#passw').prop("disabled", false);
	$('#cantidad').prop("disabled", true);
	$("#programa").focus();
	$("#btnCambioProg").prop("disabled", true);
	$("#btnCamLoc").prop("disabled", true);
	$('#btnCamCop').prop("disabled", true);
	$('#localidad').prop("disabled", true);
	$('#BuscaCoperativa').prop("disabled", true);
	$('#BuscaBeneficiario').prop("disabled", true);
	$('#btneli').prop("disabled", true);btneli
	$("#btnAsignar").prop("disabled", true);
	$("#btnGuardar").prop("disabled", true);
}); 
