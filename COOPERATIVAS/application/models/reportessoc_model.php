<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class reportessoc_model extends CI_Model {
	function __construct()
     {
          parent::__construct();
     }
     public function GeneraReporte($localidad)
	 {	 
		if ($localidad!=0){
		
		$sql="SELECT  socios.nombre,socios.id_localidad, socios.documento, socios.cuit, socios.fechanac, cooperativas.matricula, cooperativas.razonsocial, socios.cargo, localidades.nombre_localidad
FROM            socios, cooperativas, localidades
WHERE        socios.id_cooperativa = cooperativas.id AND socios.id_localidad = localidades.id AND(socios.id_localidad = '".$localidad."')";
		$query=$this->db->query($sql);
		return $query->result();}
		 
		 else
		 {
			$sql="SELECT        socios.nombre, socios.documento, socios.cuit, socios.fechanac, cooperativas.matricula, cooperativas.razonsocial, socios.cargo, localidades.nombre_localidad, socios.id_localidad
FROM            socios, cooperativas, localidades
WHERE        socios.id_cooperativa = cooperativas.id AND socios.id_localidad = localidades.id
ORDER BY localidades.nombre_localidad, cooperativas.razonsocial, cooperativas.matricula";
		$query=$this->db->query($sql);
		return $query->result(); 
			 
		 }
		
		
		
	}
	
	 public function GeneraReporte2($programa,$estado,$localidad)
	 {
		 
		 
		 if ($localidad!=0)
		 { 
		 $sql="SELECT        adj_moviles.nro_nota, adj_moviles.anio, adj_moviles.nro_expediente, beneficios.nombre_beneficios, beneficios.documento, beneficios.estado, localidades.nombre_localidad, programas.nombre_programa, 
                         cooperativas.numero, cooperativas.matricula, cooperativas.razonsocial, beneficios.barrio, beneficios.mza, beneficios.lote
FROM            adj_moviles, beneficios, localidades, programas, cooperativas
WHERE        adj_moviles.id_beneficio = beneficios.id AND adj_moviles.id_localidad = localidades.id AND adj_moviles.id_programa = programas.id AND adj_moviles.id_coperativa = cooperativas.id AND 
                         (adj_moviles.id_localidad = '".$localidad."') AND (adj_moviles.id_programa = '".$programa."')";
		 $query=$this->db->query($sql);
		 return $query->result();}
		 else
		 {
			$sql="SELECT        adj_moviles.nro_nota, adj_moviles.anio, adj_moviles.nro_expediente, beneficios.nombre_beneficios, beneficios.documento, beneficios.estado, localidades.nombre_localidad, programas.nombre_programa, 
                         cooperativas.numero, cooperativas.matricula, cooperativas.razonsocial, beneficios.barrio, beneficios.mza, beneficios.lote
FROM            adj_moviles, beneficios, localidades, programas, cooperativas
WHERE        adj_moviles.id_beneficio = beneficios.id AND adj_moviles.id_localidad = localidades.id AND adj_moviles.id_programa = programas.id AND adj_moviles.id_coperativa = cooperativas.id AND 
                         (adj_moviles.id_programa = '".$programa."')ORDER BY localidades.nombre_localidad";
		 $query=$this->db->query($sql);
		 return $query->result();} 
			 
			 
		 }
		 
	 
	
	
	
	public function UpdateExistenciasProducto($codigo,$cantidad){
		$sql="update productos set cantidad= cantidad - '".$cantidad."' where codigo='".$codigo."'";
		$query=$this->db->query($sql);
		return True;
	}
	public function reportesGenera($FInicial, $FFinal, $Documento){
		$sql="SELECT * FROM documentos   WHERE FECHA between '".$FInicial."' AND '".$FFinal."' AND TIPO='".$Documento."'";
		//echo $sql;
		$query=$this->db->query($sql);
		return $query->result();
	}
	
	public function Localidades(){
		$sql="SELECT *	from localidades";
		$query=$this->db->query($sql);
	return $query->result();}

}