<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class resolucion_model extends CI_Model {
	function __construct()
     {
          parent::__construct();
     }
     public function listarResolucion(){
     	$sql="SELECT * from resolucion order by codigo asc";
		$query=$this->db->query($sql);
		return $query->result();
     }
     /*
	public function listarResolucion(){
		$sql="SELECT a.id_denuncia,a.denunciante, a.reclamo, b.razonsocial, a.fecha_denuncia ,a.observaciones, a.foto_denuncia FROM cooperativas b, denuncias a WHERE b.id=a.id_cooperativa order by a.id_denuncia DESC";
		$query=$this->db->query($sql);
		return $query->result();
	}*/
	public function buscarResolucion($id){
		//Obtiene todos los registros de la tabla Denuncias por id, pero muestra un registro.
		$sql="SELECT * from resolucion where id_resolucion='".$id."' limit 1"; 
		$query=$this->db->query($sql); //Realiza la consulta a la BD
		return $query->result();	   //Obtiene el resultado de la consulta
	}
	public function SaveResolucion($RegistraResolucion){
		$this->db->trans_start();
     	$this->db->insert('resolucion', $RegistraResolucion);
     	$this->db->trans_complete();
	}
	public function updateResolucion($UpdateResolucion,$id){
		$this->db->trans_start();
		$this->db->where('id_resolucion', $id);
		$this->db->update('resolucion', $UpdateResolucion); 
		$this->db->trans_complete();
	}
	public function eliminarResolucion($id){
		$this->db->where('id_resolucion',$id);
		return $this->db->delete('resolucion');
	}
	
	//-----------------exportar--------------// 
	public function get()
	{
		$fields = $this->db->field_data('resolucion');
		$query = $this->db->select('*')->get('resolucion');
		return array("fields" => $fields, "query" => $query);
	}
	
	
	
	
}