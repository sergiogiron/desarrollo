<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class inscripciones_model extends CI_Model {
	function __construct()
     {
          parent::__construct();
     }
	public function ListarInscripciones(){
		$sql="SELECT B.id as ID, B.id_localidad, B.documento,B.estado,B.nombre_beneficios,B.barrio,B.mza,
		B.lote,B.id_localidad,L.nombre_localidad , L.id , B.id_programa,P.nombre_programa as nombre_programa 
		from beneficios as B
		INNER JOIN localidades AS L ON B.id_localidad = L.id 
		INNER JOIN programas AS P ON B.id_programa = P.id 
		order by nombre_beneficios asc";
		
	
	
		$query=$this->db->query($sql);
		return $query->result();
	}
	public function BuscaInscripciones($id){
		$sql="SELECT * from beneficios where id='".$id."' limit 1";
		$query=$this->db->query($sql);
		return $query->result();
	}
	public function saveInscripcion($RegistraInscripcion){
		$this->db->trans_start();
     	$this->db->insert('beneficios', $RegistraInscripcion);
     	$this->db->trans_complete();
	}
	public function updateInscripciones($UpdateInscripcion,$id){
		$this->db->trans_start();
		$this->db->where('id', $id);
		$this->db->update('beneficios', $UpdateInscripcion); 
		$this->db->trans_complete();
	}
	public function Localidades(){
		$sql="SELECT  * FROM localidades ";
		$query=$this->db->query($sql);
		return $query->result();
	}
	public function eliminarInscripcion($id){
		$this->db->where('id',$id);
		return $this->db->delete('beneficios');
	}
	public function ExisteDNI($dni){
		$this->db->where("documento",$dni);
        $check_exists = $this->db->get("beneficios");
        if($check_exists->num_rows() == 0){
            return false;
        }else{
            return true;
        }
   }		
}