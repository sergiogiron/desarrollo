<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class dependencias_model extends CI_Model {
	function __construct()
     {
          parent::__construct();
     }
     public function listarDep(){
     	$sql="SELECT * from dependencias order by codigo_dep asc";
		$query=$this->db->query($sql);
		return $query->result();
     }
     /*
	public function listarResolucion(){
		$sql="SELECT a.id_denuncia,a.denunciante, a.reclamo, b.razonsocial, a.fecha_denuncia ,a.observaciones, a.foto_denuncia FROM cooperativas b, denuncias a WHERE b.id=a.id_cooperativa order by a.id_denuncia DESC";
		$query=$this->db->query($sql);
		return $query->result();
	}*/
	public function buscarDep($id){
		$sql="SELECT * from dependencias where id_dep='".$id."' limit 1"; 
		$query=$this->db->query($sql); 
		return $query->result();	   
	}
	public function SaveDep($RegistraDep){
		$this->db->trans_start();
     	$this->db->insert('dependencias', $RegistraDep);
     	$this->db->trans_complete(); 
	}
	public function updateDep($UpdateDep,$id){
		$this->db->trans_start();
		$this->db->where('id_dep', $id);
		$this->db->update('dependencias', $UpdateDep); 
		$this->db->trans_complete();
	}
	public function eliminarDep($id){
		$this->db->where('id_dep',$id);
		return $this->db->delete('dependencias');
	}
	
	//-----------------exportar--------------// 
	public function get(){
		$fields = $this->db->field_data('areas');
		$query = $this->db->select('*')->get('areas');
		return array("fields" => $fields, "query" => $query);
	}
	
	//--------------------------busca el valor maximo para sacar el codigo------//
	public function maxCodigo(){
		$sql="SELECT max(codigo_dep) as max FROM dependencias ";
		$query=$this->db->query($sql);
		return $query->result();	
	}
	
	
}