<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class cupos_model extends CI_Model {
	function __construct()
     {
          parent::__construct();
     }
	public function ListarCupos(){
	$sql="SELECT        C.id, C.id_programa, C.id_localidad, P.nombre_programa, L.nombre_localidad, C.cupo, C.diferencia_cupo
FROM            cupos C, programas P, localidades L
WHERE        C.id_programa = P.id AND C.id_localidad = L.id
ORDER BY C.id_programa"; 
	
	
	
	
	//	$sql="SELECT * from cupos order by id asc";
		$query=$this->db->query($sql);
		return $query->result();
	}
	public function BuscaCupos($id){
		$sql="SELECT * from cupos where id='".$id."' limit 1";
		$query=$this->db->query($sql);
		return $query->result();
	}
	public function SaveCupos($RegistraCupos){
		$this->db->trans_start();
     	$this->db->insert('cupos', $RegistraCupos);
     	$this->db->trans_complete();
	}
	public function UpdateCupos($UpdateCupo,$id){
		$this->db->trans_start();
		$this->db->where('id', $id);
		$this->db->update('cupos', $UpdateCupo); 
		$this->db->trans_complete();
	}
		public function EliminarCupo($id)
	{
		# code...
		$this->db->where('id',$id);
		return $this->db->delete('cupos');
	}
	
	public function Localidades(){
		$sql="SELECT  * FROM localidades ";
		$query=$this->db->query($sql);
		return $query->result();
	}	
	public function Programas(){
		$sql="SELECT  * FROM programas ";
		$query=$this->db->query($sql);
		return $query->result();
	}
	
	
	public function traeCupo($programa){
		$sql="SELECT * from programas where id='".$programa."' limit 1";
		$query=$this->db->query($sql);
		return $query->result();
	}
	
	public function Contar($localidad,$programa)
	
	{
		$sql="SELECT        id_programa, SUM(cupo) AS Expr1
FROM            cupos
WHERE        (id_programa = '".$programa."')";
		$query=$this->db->query($sql);
		if($query->num_rows() > 0 )
        {
            return $query->result();
        }
		
		else 
			
			{
				return null;
			} 
		
	}
	 
	 
}