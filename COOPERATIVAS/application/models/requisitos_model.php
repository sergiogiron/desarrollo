<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class requisitos_model extends CI_Model {
	function __construct()
     {
          parent::__construct();
     }
	 
	 public function BuscarRequisitos($id){
		$sql="SELECT * from requisitos where id_cooperativa='".$id."' limit 1";
		$query=$this->db->query($sql);
		return $query->result();
	 
	 }

	
	public function SaveRequisitos($RegistraRequisito){
		$this->db->trans_start();
     	$this->db->insert('requisitos', $RegistraRequisito);
     	$this->db->trans_complete();
	}
	public function UpdateRequisitos($UpdateRequisito,$id){
		$this->db->trans_start();
		$this->db->where('id_cooperativa', $id);
		$this->db->update('requisitos', $UpdateRequisito); 
		$this->db->trans_complete();
	}


	//----------------
	 
	public function ListarSocios(){
		$sql="SELECT *,S.id, S.telefono,C.razonsocial as razon from socios as S 
        INNER JOIN cooperativas  AS C ON S.id_cooperativa = C.id
		order by nombre asc";
		$query=$this->db->query($sql);
		return $query->result();
		}
	public function EliminarSocio($id)
	{
		# code...
		$this->db->where('id',$id);
		return $this->db->delete('socios');
	}
	
}