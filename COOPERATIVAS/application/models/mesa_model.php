<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class mesa_model extends CI_Model {
	function __construct()
     {
          parent::__construct();
     }
	public function ListarSolicitudes(){
	$sql="SELECT        id_mesa, descripcion, solicitante, estado,respuesta,fecha
FROM            mesa"; 
	
	
	
	
	//	$sql="SELECT * from cupos order by id asc";
		$query=$this->db->query($sql);
		return $query->result();
	}
	public function Busca($id){
		$sql="SELECT * from mesa where id_mesa='".$id."' limit 1";
		$query=$this->db->query($sql);
		return $query->result();
	}
	public function Save($RegistraCupos){
		$this->db->trans_start();
     	$this->db->insert('mesa', $RegistraCupos);
     	$this->db->trans_complete();
	}
	public function Update($UpdateCupo,$id){
		$this->db->trans_start();
		$this->db->where('id_mesa', $id);
		$this->db->update('mesa', $UpdateCupo); 
		$this->db->trans_complete();
	}
		public function buscar($id){
		//Obtiene todos los registros de la tabla Denuncias por id, pero muestra un registro.
		$sql="SELECT * from mesa where id_mesa='".$id."' limit 1"; 
		$query=$this->db->query($sql); //Realiza la consulta a la BD
		return $query->result();	   //Obtiene el resultado de la consulta
		}
	
	public function Localidades(){
		$sql="SELECT  * FROM localidades ";
		$query=$this->db->query($sql);
		return $query->result();
	}	
	public function Programas(){
		$sql="SELECT  * FROM programas ";
		$query=$this->db->query($sql);
		return $query->result();
	}
	
	
	public function traeCupo($programa){
		$sql="SELECT * from programas where id='".$programa."' limit 1";
		$query=$this->db->query($sql);
		return $query->result();
	}
	
	public function Contar($localidad,$programa)
	
	{
		$sql="SELECT        id_programa, SUM(cupo) AS Expr1
FROM            cupos
WHERE        (id_programa = '".$programa."')";
		$query=$this->db->query($sql);
		if($query->num_rows() > 0 )
        {
            return $query->result();
        }
		
		else 
			
			{
				return null;
			} 
		
	}
	 
	 
	 
	 public function updateMesa($UpdateDenuncia,$id){
		$this->db->trans_start();
		$this->db->where('id_mesa', $id);
		$this->db->update('mesa', $UpdateDenuncia); 
		$this->db->trans_complete();
	}
	 
}