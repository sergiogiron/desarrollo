<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class parentesco_model extends CI_Model {
	function __construct()
     {
          parent::__construct();
     }
   	//Busqueda de socio y su coop, por nombre o dni
     public function BuscarSocios($filtro){
     	$sql="SELECT s.id,c.razonsocial,c.id AS 'id_coop', concat(s.documento,'-', nombre) AS label, s.documento, nombre
			  FROM socios as s
		INNER JOIN cooperativas as c ON s.id_cooperativa=c.id
		WHERE ( s.documento LIKE  '%".$filtro."%' or nombre LIKE '%".$filtro."%') LIMIT 0 , 10";
		$query=$this->db->query($sql);
		 return $query->result();
     }
     /* 
     public function BuscarSocios($filtro){ 
		$sql="SELECT id,concat(documento,'-', nombre) AS label, documento, nombre
			  FROM socios   WHERE ( documento LIKE  '%".$filtro."%' or nombre LIKE '%".$filtro."%') LIMIT 0 , 10 ";
		$query=$this->db->query($sql);
		 return $query->result();	
	}
	*/
	public function ListarSocios(){
		$sql="SELECT *,S.id_pariente, S.documento,S.telefono,C.razonsocial as razon from sociosFamilia as S 
        INNER JOIN cooperativas  AS C ON S.id_cooperativa = C.id
		order by nombre_pariente asc";
		$query=$this->db->query($sql);
		return $query->result(); 
	}
	
	/*
	public function BuscaSocios($id){
		$sql="SELECT s.nombre, s.id, f.documento, f.id_cooperativa, f.parentesco, f.cuit, f.nombre_pariente, f.fechanac, f.sexo, f.estadoCivil, f.caractCelular, f.celular, f.email, f.fechaIngreso, f.sitRevista, f.fechaVencimiento, f.id_localidad, f.domicilio, f.nro, f.piso, f.dpto, f.bloque, f.telefono, f.id_socio, f.id_pariente
			FROM socios as s
			INNER JOIN sociosfamilia AS f ON s.id=f.id_socio
			where f.id_pariente='".$id."' limit 1";
		$query=$this->db->query($sql);
		return $query->result();

	}
	*/
	//Busqueda de socio y su coop, para editar
	public function BuscaSocios($id){
		$sql="SELECT c.razonsocial,s.nombre, s.id, f.documento, f.id_socio,f.id_cooperativa as 'id_coop', f.parentesco, f.cuit, f.nombre_pariente, f.fechanac, f.sexo, f.estadoCivil, f.caractCelular, f.celular, f.email, f.fechaIngreso, f.sitRevista, f.fechaVencimiento, f.id_localidad, f.domicilio, f.nro, f.piso, f.dpto, f.bloque, f.telefono, f.id_pariente
			FROM socios as s
			INNER JOIN sociosfamilia AS f ON s.id=f.id_socio INNER JOIN cooperativas as c ON c.id=f.id_cooperativa
			where f.id_pariente='".$id."' limit 1";
			$query=$this->db->query($sql);
		return $query->result();
	}
	public function SaveSocios($RegistraSocio){
		$this->db->trans_start();
     	$this->db->insert('sociosFamilia', $RegistraSocio);
     	$this->db->trans_complete();
	}
	public function UpdateSocios($UpdateSocio,$id){
		$this->db->trans_start();
		$this->db->where('id_pariente', $id);
		$this->db->update('sociosFamilia', $UpdateSocio); 
		$this->db->trans_complete();
	}
	public function EliminarSocio($id)
	{
		# code...
		$this->db->where('id_pariente',$id);
		return $this->db->delete('sociosFamilia');
	}
	public function ExisteDocumento($codigo){
		$this->db->where("documento",$codigo);
        $check_exists = $this->db->get("sociosFamilia");
        if($check_exists->num_rows() == 0){
            return false;
        }else{
            return true;
        }
       }
    public function ExisteCargo($codigo,$codigo1){
	   $this->db->where("id_cooperativa",$codigo);
	   $this->db->where("cargo",$codigo1);
        $check_exists = $this->db->get("sociosFamilia");
        if($check_exists->num_rows() == 0){
            return false;
        }else{
            return true;
        }
       }

	      
	   
	   
	   
	   
	   
	   
	   
	   
}