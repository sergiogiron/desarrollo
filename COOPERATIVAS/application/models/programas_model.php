<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class programas_model extends CI_Model {
	function __construct()
     {
          parent::__construct();
     }
     /**
      * Lista todos los programas cargados por nombre en orden ascendente
      * @return type
      */
	public function listarPrograma(){
		$sql="SELECT * from programas order by nombre_programa asc";
		$query=$this->db->query($sql);
		return $query->result();
	}
	/**
	 * Permite realizar una busqueda de programas
	 * @param integer  $id  identificador del programa a buscar
	 * @return type
	 */
	public function buscarPrograma($id){
		//Obtiene todos los registros de la tabla Programas por id, pero muestra un registro.
		$sql="SELECT * from programas where id='".$id."' limit 1"; 
		$query=$this->db->query($sql); //Realiza la consulta a la BD
		return $query->result();	   //Obtiene el resultado de la consulta
	}
	/**
	 * Guarda los datos de un nuevo programa cargado
	 * @param type $RegistraPrograma  variable que almacena el nombre, tipo de Programa y cantidad.
	 * @return type
	 */
	public function savePrograma($RegistraPrograma){
		$this->db->trans_start();
     	$this->db->insert('programas', $RegistraPrograma);
     	$this->db->trans_complete();
	}
	/**
	 * Guarda la actualizacion de los datos 
	 * @param type  $UpdatePrograma  variable que almacena el nombre, tipo de Programa y cantidad.
	 * @param type 	$id   			 identificador del programa a actualizar
	 * @return type
	 */
	public function updatePrograma($UpdatePrograma,$id){
		$this->db->trans_start();
		$this->db->where('id', $id);
		$this->db->update('programas', $UpdatePrograma); 
		$this->db->trans_complete();
	}
	/**
	 * Elimina los datos de un programa
	 * @param type $id  identificador del programa a actualizar
	 * @return type
	 */
	public function eliminarPrograma($id){
		$this->db->where('id',$id);
		return $this->db->delete('programas');
	}
}