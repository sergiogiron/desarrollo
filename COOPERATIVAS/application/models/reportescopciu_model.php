<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class reportescopciu_model extends CI_Model {
	function __construct()
     {
          parent::__construct();
     }
     public function GeneraReporte($filtro){
		 
		$sql="SELECT        adj_moviles.id_coperativa, adj_moviles.id_localidad, adj_moviles.id_programa, beneficios.nombre_beneficios, beneficios.documento, localidades.nombre_localidad, cooperativas.razonsocial, 
                         cooperativas.matricula, cooperativas.telefono, cooperativas.celular, programas.nombre_programa,adj_moviles.nro_expediente

FROM            adj_moviles, beneficios, localidades, cooperativas, programas
WHERE        adj_moviles.id_beneficio = beneficios.id AND adj_moviles.id_localidad = localidades.id AND adj_moviles.id_coperativa = cooperativas.id AND adj_moviles.id_programa = programas.id AND 
                         (adj_moviles.id_coperativa = '".$filtro."')";
		$query=$this->db->query($sql);
		return $query->result();
	}
	
	
	 public function GeneraReporte2($filtro){
		 
		$sql="SELECT        socios.id, socios.nombre, socios.documento, socios.domicilio, socios.nro, socios.manzana, socios.lote, socios.barrio, socios.telefono, socios.cargo, cooperativas.razonsocial, localidades.nombre_localidad, 
                         socios.obra
FROM            socios, cooperativas, localidades
WHERE        socios.id_cooperativa = cooperativas.id AND socios.id_localidad = localidades.id AND (socios.id_cooperativa = '".$filtro."')";
		$query=$this->db->query($sql);
		return $query->result();
	}
	
	
	
	
	 public function GeneraReporte3($filtro){
		 
		$sql="SELECT        localidades.nombre_localidad, cooperativas.razonsocial, cooperativas.matricula, cooperativas.domicilio, cooperativas.telefono, cooperativas.celular, cooperativas.tipo, cooperativas.cuit, 
                         cooperativas.resolucion, cooperativas.banco, cooperativas.cbu, cooperativas.email, cooperativas.numero, cooperativas.estado

FROM            cooperativas, localidades
WHERE       cooperativas.id_localidad = localidades.id AND (cooperativas.id = '".$filtro."')";
		$query=$this->db->query($sql);
		return $query->result();
	}
	
	
	
	 public function DatosNecesarios($localidad)
	 {
		 
		 $sql="SELECT        adj_moviles.id_localidad, cooperativas.resolucion, cooperativas.razonsocial, cooperativas.cantidad_bene, localidades.nombre_localidad, adj_moviles.id_coperativa
FROM            adj_moviles, cooperativas, localidades
WHERE        adj_moviles.id_coperativa = cooperativas.id AND adj_moviles.id_localidad = localidades.id
GROUP BY adj_moviles.id_localidad, cooperativas.resolucion, cooperativas.razonsocial, cooperativas.cantidad_bene, localidades.nombre_localidad, adj_moviles.id_coperativa
HAVING        (adj_moviles.id_localidad = '".$localidad."')";
		 $query=$this->db->query($sql);
		return $query->result();
		 
	 }
	
	
	
	public function UpdateExistenciasProducto($codigo,$cantidad){
		$sql="update productos set cantidad= cantidad - '".$cantidad."' where codigo='".$codigo."'";
		$query=$this->db->query($sql);
		return True;
	}
	public function reportesGenera($FInicial, $FFinal, $Documento){
		$sql="SELECT * FROM documentos   WHERE FECHA between '".$FInicial."' AND '".$FFinal."' AND TIPO='".$Documento."'";
		//echo $sql;
		$query=$this->db->query($sql);
		return $query->result();
	}
	
	public function Localidades(){
		$sql="SELECT *	from localidades";
		$query=$this->db->query($sql);
	return $query->result();}

}