<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class areas_model extends CI_Model {
	function __construct()
     {
          parent::__construct();
     }
	public function ListarAreas(){
		$sql="SELECT * from areas order by codigo_area asc";
		$query=$this->db->query($sql);
		return $query->result();
	}
	public function BuscaAreas($id){
		$sql="SELECT * from areas where id_area='".$id."' limit 1";
		$query=$this->db->query($sql);
		return $query->result();
	}
	public function SaveAreas($RegistraArea){
		$this->db->trans_start();
     	$this->db->insert('areas', $RegistraArea);
     	$this->db->trans_complete();
	}
	public function UpdateAreas($UpdateArea,$id){
		$this->db->trans_start();
		$this->db->where('id_area', $id);
		$this->db->update('areas', $UpdateArea); 
		$this->db->trans_complete();
	}
	public function EliminarArea($id)
	{
		# code...
		$this->db->where('id_area',$id);
		return $this->db->delete('areas');
	}
	
	//--------------------------busca el valor maximo para sacar el codigo------//
	public function maxCodigo(){
		$sql="SELECT max(codigo_area) as max FROM areas ";
		$query=$this->db->query($sql);
		return $query->result();	
	}
	
	
}