<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class usuarios_model extends CI_Model {
	private $db_b;//manejador de la 2da base de datos
	function __construct()
     {
          parent::__construct();
		  $this->db_b = $this->load->database('prueba', true);//instancia de la 2base
     }
     /*
	public function ListarUsuarios(){
		$sql="SELECT * from usuarios order by NOMBRE asc";
		$query=$this->db->query($sql);
		return $query->result();
	}
	*/
	public function ListarUsuarios(){
		$sql="SELECT U.ID, U.NOMBRE, U.APELLIDOS, U.EMAIL, U.FECHA_REGISTRO, U.ESTATUS, U.TIPO, U.PASSWORD, A.nombre_area, U.PRIVILEGIOS, U.ONLINE
		FROM usuarios as U INNER JOIN areas as A ON U.ID_AREA=A.id_area ORDER BY ID ASC";
		$query=$this->db_b->query($sql);
		return $query->result();
	}
	public function BuscaUsuario($id){
		$sql="SELECT * from usuarios where id='".$id."' limit 1";
		$query=$this->db_b->query($sql);
		return $query->result();
	}
	public function SaveUs($RegistraUsuarios){
		$this->db_b->trans_start();
     	$this->db_b->insert('usuarios', $RegistraUsuarios);
     	$this->db_b->trans_complete();
	}
	public function UpdateUs($UpdateUsuario,$id){
		$this->db_b->trans_start();
		$this->db_b->where('id', $id);
		$this->db_b->update('usuarios', $UpdateUsuario); 
		$this->db_b->trans_complete();
	}
	public function EliminarUsuario($id)
	{
		# code...
		$this->db_b->where('id',$id);
		return $this->db_b->delete('usuarios');
	}
	
	public function ExisteEmail($email){
		$this->db_b->where("EMAIL",$email);
        $check_exists = $this->db->get("usuarios");
        if($check_exists->num_rows() == 0){
            return false;
        }else{
            return true;
        }
	}
	
	public function traerAreas(){
		$sql="SELECT  * FROM areas ";
		$query=$this->db_b->query($sql);
		return $query->result();
	}
	
	
	
	
	
	
	
	
	
}