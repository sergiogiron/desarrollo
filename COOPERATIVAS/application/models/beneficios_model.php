<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class beneficios_model extends CI_Model {
	function __construct()
     {
          parent::__construct();
     }
		 
	 
	public function ListarBeneficios(){
		$sql="SELECT        B.id AS ID, B.id_localidad, B.documento, B.estado, B.nombre_beneficios, B.barrio, B.mza, B.lote, L.nombre_localidad AS NLoca, L.id AS Expr1, B.id_programa, P.nombre_programa, B.institucion
FROM            beneficios B, localidades L, programas P
WHERE        B.id_localidad = L.id AND B.id_programa = P.id AND (B.institucion <> 2)
ORDER BY B.nombre_beneficios";
		$query=$this->db->query($sql);
		return $query->result();
	}
	public function BuscaBeneficios($id){
		$sql="SELECT * from beneficios where id='".$id."' limit 1";
		$query=$this->db->query($sql);
		return $query->result();
	}
	public function SaveBene($RegistraProfesor){
		$this->db->trans_start();
     	$this->db->insert('beneficios', $RegistraProfesor);
     	$this->db->trans_complete();
	}
	public function UpdateBeneficios($UpdateProfesor,$id){
		$this->db->trans_start();
		$this->db->where('id', $id);
		$this->db->update('beneficios', $UpdateProfesor); 
		$this->db->trans_complete();
	}
	public function EliminarBeneficios($id)
	{
		# code...
		$this->db->where('id',$id);
		return $this->db->delete('beneficios');
	}
	
	public function Localidades(){
		$sql="SELECT  * FROM localidades ";
		$query=$this->db->query($sql);
		return $query->result();
	}
	
	

public function controlaCalle($mza,$lote,$barrio,$localidad)
	{
		$sql="SELECT        COUNT(0) AS Expr1
FROM            beneficios
WHERE        (id_localidad = '".$localidad."') AND (lote ='".$lote."') AND (mza = '".$mza."') AND (barrio = '".$barrio."') ";
		$query=$this->db->query($sql);
		return $query->result();
		
	}
	
public function ExisteDocumento($codigo){
		$this->db->where("documento",$codigo);
        $check_exists = $this->db->get("beneficios");
        if($check_exists->num_rows() == 0){
            return false;
        }else{
            return true;
        }
       }

	
	//-----------------exportar--------------// 
	public function get()
	{
		$fields = $this->db->field_data('beneficios');
		$query = $this->db->select('*')->get('beneficios');
		return array("fields" => $fields, "query" => $query);
	}
	
	
	
}