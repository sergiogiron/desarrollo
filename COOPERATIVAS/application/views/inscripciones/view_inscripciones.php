<style type="text/css">
th, td { white-space: nowrap; }
    div.dataTables_wrapper {
        width: 100%;
        margin: 0 auto;
    }
</style>
<script type="text/javascript">
var baseurl = "<?php echo base_url(); ?>";
var currentLocation = window.location;

function eliminarInscripcion(nombre, id){
    confirmar=confirm("Eliminar a " + nombre + ", Recuerda Una vez Eliminado No podras Recuperarlo"); 

    if (confirmar){
    	document.getElementById('mensaje').innerHTML = "<div class='modal1'><div class='center1'> <center> <img src='"+ baseurl +"/img/gif-load.gif'> Eliminando Beneficiarios...</center></div></div>";
    	 var insc		 = new Object();
		insc.Id      	 = id;

		var DatosJson = JSON.stringify(insc);
		$.post(currentLocation + '/deleteInscripciones',
		{ 
			InscripcionesPost: DatosJson
		},
		function(data, textStatus) {
			//
			$("#mensaje").html(data.error_msg);
		}, 
		"json"		
		);
    } else{
    } 
  }
  
</script>
<h1 class="page-header"><span class="glyphicon glyphicon-list-alt"></span> Inscripciones </h1>
<div id="mensaje"></div>
<p align="right">
 	 <a href="inscripciones/nuevo">
 	 	<button type="button" class="btn btn-primary"><span class="glyphicon glyphicon-ok"></span> Nueva Inscripcion</button>
 	 </a>  
 	 </p>
 	 <br/>
	<table id="inscripciones" border="0" cellpadding="0" cellspacing="0" width="100%" class="pretty">
		<thead>
			<tr>
				<th></th>
				<th>Id</th>
				<th>Programa</th>
				<th>Estado</th>
				<th>Documento</th>
				<th>Beneficiario/a</th>
				<th>Localidad</th>
				<th>Barrio</th>
				<th>Mza</th>
				<th>Lote</th>
				<!--<th>Obs</th>-->
			</tr>
		</thead>
		<tbody>
			<?php
				if($inscripciones){
					foreach($inscripciones as $insc){
					if($insc->estado==1){
					
					
				    	$id     = base64_encode($insc->ID);
						echo '<tr>';
						echo '<td>';
						echo '<a href="inscripciones/editarInscripciones/'.$id.'"><button type="button" title="Editar Beneficiario" class="btn btn-success btn-xs"><span class="glyphicon glyphicon-edit"></span></button></a> &nbsp;';
						?>
						<button type="button" onclick="eliminarInscripcion('<?php echo $insc->nombre_beneficios; ?>','<?php echo $id; ?>');" title="Eliminar Beneficiario" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span></button>
						<?php
						echo '</td>';
						
						echo '<td>'.str_pad((int) $insc->ID,4,"0",STR_PAD_LEFT).'</td>';
						
						$i=$insc->estado;
				        switch ($i) {
				         case 0:
				                $Estado= "Elegir";
				                break;
						 case 1:
				                $Estado= "Inscripcion";
				                break;
				         case 2:
				                $Estado= "Habilitado";
				                break;
						 case 3:
				                $Estado= "Adjudicado";
								 break;
						 case 4:
				                $Estado= "Pendiente";
								 break;	
					     case 5:
				                $Estado= "Denuncia";
				                break;
						  };
                        echo '<td>'.$insc->nombre_programa.'</td>';
						echo '<td>'.$Estado.'</td>';
						echo '<td>'.$insc->documento.'</td>'; 	
						echo '<td>'.$insc->nombre_beneficios.'</td>';
						echo '<td>'.$insc->nombre_localidad.'</td>';
						echo '<td>'.$insc->barrio.'</td>';
						echo '<td>'.$insc->mza.'</td>';
						echo '<td>'.$insc->lote.'</td>';
						//echo '<td>'.$insc->obs.'</td>';
						echo '</tr>';
					}
					}
				}else{
					echo '<tr><td colspan=5><center>No Existe Informacion</center></td></tr>';
				}
			?>
		</tbody>
	</table>
<script type="text/javascript">

            $(document).ready(function() {
    $('#inscripciones').dataTable( {
        "scrollX": false
    } );
} );

</script>
			