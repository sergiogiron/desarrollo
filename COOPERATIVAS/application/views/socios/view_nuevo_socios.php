<input type="hidden" id="id" name="id" value="<?php echo @$socio[0]->id; ?>" >
<input type="hidden" id="loca" name="loca" value="<?php echo @$socio[0]->id_localidad; ?>" > 
<input type="hidden" id="coope"name="coope" value="<?php echo @$socio[0]->id_cooperativa; ?>" >
<script type="text/javascript">
  var baseurl = "<?php echo base_url(); ?>";
  
  var idloca        = 0;
  var idcoope        = 0;
  var ids           = document.getElementById("id").value;
  ids               = parseInt(ids.length);
  
  if(ids==0){
        idloca       = 0;
		idcoope      = 0;
  }else{
    
    idloca           = document.getElementById("loca").value;
	idcoope           = document.getElementById("coope").value;
  }
  function regresar(){
    window.location="<?php echo base_url()?>socios";
  }
</script>




<script type="text/javascript">
  var baseurl = "<?php echo base_url(); ?>";
 
  function regresar(){
    window.location="<?php echo base_url()?>socios";
  }
</script>
<?php
//Nombre
  $nombre       = array(
  'name'        => 'nombre',
  'id'          => 'nombre',
  'size'        => 50,
  'value'       => set_value('nombre',@$socio[0]->nombre),
  'type'        => 'text',
  'class'       => 'form-control',
  'style'       => 'text-transform:uppercase',
  'onkeypress'  => 'return validarn(event);',
  );
    $documento       = array(
  'name'        => 'documento',
  'id'          => 'documento',
  'size'        => 50,
  'value'       => set_value('documento',@$socio[0]->documento),
  'type'        => 'number',
  'class'       => 'form-control',
 // 'style'       => 'text-transform:uppercase',
  'onBlur'  => 'buscarDni();',
  );
  
    $cuit       = array(
  'name'        => 'cuit',
  'id'          => 'cuit',
  'size'        => 50,
  'value'       => set_value('cuit',@$socio[0]->cuit),
  'type'        => 'text',
  'class'       => 'form-control',
 // 'style'       => 'text-transform:uppercase',
 // 'onkeypress'  => 'return validarn(event);',
  );
  
 
  $fechanac    = array(
  'name'        => 'fechanac',
  'id'          => 'fechanac',
  'size'        => 50,
  'value'       => set_value('fechanac',@$socio[0]->fechanac),
  'type'        => 'date',
  'class'       => 'form-control',
  //'style'       => 'text-transform:uppercase',
  );
    
  $domicilio    = array(
  'name'        => 'domicilio',
  'id'          => 'domicilio',
  'size'        => 50,
  'value'       => set_value('domicilio',@$socio[0]->domicilio),
  'type'        => 'text',
  'class'       => 'form-control',
  'style'       => 'text-transform:uppercase',
  );
  $nro    = array(
  'name'        => 'nro',
  'id'          => 'nro',
  'size'        => 50,
  'value'       => set_value('nro',@$socio[0]->nro),
  'type'        => 'number',
  'class'       => 'form-control',
 // 'style'       => 'text-transform:uppercase',
  ); 
  $manzana    = array(
  'name'        => 'manzana',
  'id'          => 'manzana',
  'size'        => 50,
  'value'       => set_value('manzana',@$socio[0]->manzana),
  'type'        => 'text',
  'class'       => 'form-control',
 // 'style'       => 'text-transform:uppercase',
  ); 
    $lote    = array(
  'name'        => 'lote',
  'id'          => 'lote',
  'size'        => 50,
  'value'       => set_value('lote',@$socio[0]->lote),
  'type'        => 'text',
  'class'       => 'form-control',
//  'style'       => 'text-transform:uppercase',
  );  
     $barrio    = array(
  'name'        => 'barrio',
  'id'          => 'barrio',
  'size'        => 50,
  'value'       => set_value('barrio',@$socio[0]->barrio),
  'type'        => 'text',
  'class'       => 'form-control',
  'style'       => 'text-transform:uppercase',
  ); 
   
      
  $id_provincia    = array(
  '0'             => '---Elegir Opción---',
  '1'             => 'BUENOS AIRES',
  '2'             => 'CORDOBA',
  '3'             => 'SANTA FE',
  '4'             => 'MENDOZA',
  '5'             => 'TUCUMAN',
  '6'             => 'ENTRE RIOS', 
  '7'             => 'SALTA',
  '8'             => 'MISIONES',
  '9'             => 'CHACO',
  '10'             => 'CORRIENTES',
  '11'             => 'SANTIAGO DEL ESTERO',
  '12'             => 'JUJUY',
  '13'             => 'SAN JUAN', 
   '14'             => 'RIO NEGRO',
   '15'             => 'FORMOSA',
  '16'             => 'CORDOBA',
  '17'             => 'NEUQUEN',
  '18'             => 'CHUBUT',
  '19'             => 'SAN LUIS',
  '20'             => 'CATAMARCA', 
  '21'             => 'LA RIOJA',
  '22'             => 'LA PAMPA',
  '23'             => 'SANTA CRUZ',
  '24'             => 'TIERRA DE FUEGO',
   
);
   
  $telefono     = array(
  'name'        => 'telefono',
  'id'          => 'telefono',
  'size'        => 50,
  'value'       => set_value('telefono',@$socio[0]->telefono),
  'type'        => 'text',
  'class'       => 'form-control',
  'onkeypress'  => 'return validarNumeros(event);',
   
  );
  
  $email     = array(
  'name'        => 'email',
  'id'          => 'email',
  'size'        => 50,
  'value'       => set_value('email',@$socio[0]->email),
  'type'        => 'text',
  'class'       => 'form-control',
 // 'onkeypress'  => 'return validarNumeros(event);',
   
  );
  
    $cargo  = array(
  '0'             => '---Elegir Opción---',
  '1'             => 'Presidente',
  '2'             => 'Secretario',
  '3'            => 'Tesorero',
  '4'             => 'Vocal Primero',
  '5'             => 'Vocal Segundo',
  '6'             => 'Sindico titular',
  '7'             => 'Sindico Suplente', 
   '8'            => 'Socio',
   
);

   $tipo  = array(
  '0'             => '---Elegir Opción---',
  '1'             => 'SOCIO',
  '2'             => 'ADHENTE',         
 );
  
 
   $plan  = array(
  '0'             => '---Elegir Opción---',
  '1'             => 'Si',
  '2'             => 'No',
  );
//Nombre
  $obra       = array(
  'name'        => 'obra',
  'id'          => 'obra',
  'size'        => 50,
  'value'       => set_value('nombre',@$socio[0]->obra),
  'type'        => 'text',
  'class'       => 'form-control',
  'style'       => 'text-transform:uppercase',
  'onkeypress'  => 'toUpperCase(this.value);',
  );
  
   $monotributo  = array(
  '0'             => '---Elegir Opción---',
  '1'             => 'Si',
  '2'             => 'No',
  );
  
 
?>
<script src="<?php echo base_url();?>js/JsonSocios.js"></script>
<h1 class="page-header"><span class="glyphicon glyphicon-th-list"></span> <?php echo $titulo; ?></h1>
<div id="mensaje"></div>
<form class="form-horizontal" name="formulario" id="formulario" role="form">
  <input type="hidden" value="0" id="validamail" name="validamail">
  <input type="hidden" value="0" id="validarfc" name="validarfc">
  <div class="form-group">
  
  <div class="form-group">
    <label for="documento" class="col-lg-3 control-label">Documento:</label>
    <div class="col-lg-3">
      <?php echo form_input($documento); ?>
    </div>
  </div>
  
  
    <label for="nombre" class="col-lg-3 control-label">Nombre:</label>
    <div class="col-lg-3">
      <?php echo form_input($nombre); ?>
    </div>
  </div>
  
    

    <div class="form-group">
    <label for="cuit" class="col-lg-3 control-label">Cuit:</label>
    <div class="col-lg-3">
      <?php echo form_input($cuit); ?>
    </div>
  </div> 
  
  
   <div class="form-group">
    <label for="fechanac" class="col-lg-3 control-label">Fecha Nacimiento:</label>
    <div class="col-lg-3">
      <?php echo form_input($fechanac); ?>
    </div>
  </div>
      
  
  <div class="form-group">
    <label for="domiclilio" class="col-lg-3 control-label">Domicilio:</label>
    <div class="col-lg-3">
      <?php echo form_input($domicilio); ?>
    </div>
  </div>

  <div class="form-group">
    <label for="nro" class="col-lg-3 control-label">Nro:</label>
    <div class="col-lg-3">
      <?php echo form_input($nro); ?>
    </div>
  </div>
  <div class="form-group">
    <label for="manzana" class="col-lg-3 control-label">Manzana:</label>
    <div class="col-lg-3">
      <?php echo form_input($manzana); ?>
    </div>
  </div>
  <div class="form-group">
    <label for="lote" class="col-lg-3 control-label">Lote:</label>
    <div class="col-lg-3">
      <?php echo form_input($lote); ?>
    </div>
  </div>
    
 <div class="form-group">
    <label for="barrio" class="col-lg-3 control-label">Barrio:</label>
    <div class="col-lg-3">
      <?php echo form_input($barrio); ?>
    </div>
  </div>
	
	
	
	
  
  <div class="form-group">
    <label for="localidad" class="col-lg-3 control-label">Localidad:</label>
    <div class="col-lg-3">
	<select name="localidad" id="localidad" class="form-control"></select>
       </div>
  </div>
   
  
    <div class="form-group">
    <label for="id_provincia" class="col-lg-3 control-label">Provincia:</label>
    <div class="col-lg-3">
	<?php echo  form_dropdown('id_provincia', $id_provincia, set_value('id_provincia',@$socio[0]->id_provincia),'class="form-control" id="id_provincia"'); ?>
    </div>
  </div>
    
  
   <div class="form-group">
    <label for="telefono" class="col-lg-3 control-label">Telefono:</label>
    <div class="col-lg-3">
      <?php echo form_input($telefono); ?>
    </div>
  </div>
    
  
 <div class="form-group">
    <label for="cargo" class="col-lg-3 control-label">Cargo:</label>
    <div class="col-lg-3">
	<?php echo  form_dropdown('cargo', $cargo, set_value('cargo',@$socio[0]->cargo),'class="form-control" id="cargo"'); ?>
      </div>
  </div>
  
 <div class="form-group">
    <label for="tipo" class="col-lg-3 control-label">Tipo:</label>
    <div class="col-lg-3">
	<?php echo  form_dropdown('tipo', $tipo, set_value('tipo',@$socio[0]->tipo),'class="form-control" id="tipo"'); ?>
      </div>
  </div>
  
  <div class="form-group">
    <label for="plan" class="col-lg-3 control-label">Plan Social:</label>
    <div class="col-lg-3">
	<?php echo  form_dropdown('plan', $plan, set_value('plan',@$socio[0]->plan),'class="form-control" id="plan"'); ?>
     </div>
  </div>
  
   <div class="form-group">
    <label for="cooperativa" class="col-lg-3 control-label">Cooperativas:</label>
    <div class="col-lg-3">
	<select name="cooperativa" id="cooperativa" class="form-control"></select>
       </div>
  </div>
  
   <div class="form-group">
    <label for="telefono" class="col-lg-3 control-label">Obra Social:</label>
    <div class="col-lg-3">
      <?php echo form_input($obra); ?>
    </div>
  </div>
  
  <div class="form-group">
    <label for="monotributo" class="col-lg-3 control-label">Monotributo:</label>
    <div class="col-lg-3">
  <?php echo  form_dropdown('monotributo', $monotributo, set_value('monotributo',@$socio[0]->monotributo),'class="form-control" id="monotributo"'); ?>
     </div>
  </div>
  
  <div class="form-group">
    <div class="col-lg-offset-3 col-lg-10">
      <button type="button" onclick="regresar()" class="btn btn-default">Regresar</button>
      <button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-saved"></span> Guardar Socio</button>
      <?php if($titulo=="Nuevo Socio"){ ?>
      <button type="reset" class="btn btn-default">Nuevo</button>
      <?php } ?>
    </div>
  </div>
  <hr/>
</form>		
<script type="text/javascript">
  
</script>
