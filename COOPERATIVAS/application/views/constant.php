<?php
	const NOMBRE_EMPRESA            = 'UNIDAD COORDINADORA Y EJECUTORA DE PLANES Y PROGRAMAS';
	const TITULO_PAGINA 		= 'UCEPPI';
	const EXTENSION_IMAGEN_FAVICON  = 'png';//png , gif, jpeg, jpg
	const NOMBRE_IMAGEN_FAVICON     = 'Logo.jpg';
	const LEYENDA_EXTRA             = '';
	const CALLE                     = 'Avda bolivia Nro. 246';
	const CP                        = '4600';
	const CIUDAD                    = 'S. S. de Jujuy';
	const ESTADO                    = 'Jujuy - Argentina';
	const TELEFONO                  = '0388-4315040';

?>