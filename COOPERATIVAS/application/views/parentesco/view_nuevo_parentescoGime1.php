<input type="hidden" id="id" name="id" value="<?php echo @$socio[0]->id_pariente; ?>">
<input type="hidden" id="loca" name="loca" value="<?php echo @$socio[0]->id_localidad; ?>" > 
<input type="hidden" id="coope" name="coope" value="<?php echo @$socio[0]->id_cooperativa; ?>" >

<input type="hidden" id="socio" name="socio" value="<?php echo @$socio[0]->id_socio; ?>" >


<script src="<?php echo base_url()?>js/jquery-1.10.2.js"></script>
<script src="<?php echo base_url()?>js/jquery-ui.js"></script>

<script type="text/javascript">
  var baseurl = "<?php echo base_url(); ?>";
  
  var idloca     = 0;
  var idcoope    = 0;
  var idsocio    = 0;
  var ids        = document.getElementById("id").value;
  ids            = parseInt(ids.length);
  
  if(ids==0){
    idloca       = 0;
		idcoope      = 0;
    idsocio      = 0;
  }else{
    idloca       = document.getElementById("loca").value;
	  idcoope      = document.getElementById("coope").value;
    idsocio      = document.getElementById("socio").value;
  }

  function regresar(){
    window.location="<?php echo base_url()?>parentesco";
  }
</script>




<script type="text/javascript">
  var baseurl = "<?php echo base_url(); ?>";
 
  function regresar(){
    window.location="<?php echo base_url()?>parentesco";
  }
</script>
<?php
$documento       = array(
  'name'        => 'documento',
  'id'          => 'documento',
  'size'        => 50,
  'value'       => set_value('documento',@$socio[0]->documento),
  'type'        => 'text',
  'class'       => 'form-control',
 // 'style'       => 'text-transform:uppercase',
 // 'onkeypress'  => 'return validarn(event);',
  );
$busqueda      = array(
  'name'        => 'buscaSocio',
  'id'          => 'buscaSocio',
  'size'        => 50,
  'value'       => set_value('buscaSocio',@$socio[0]->nombre), //campo de la tabla socios
  'type'        => 'text',
  'class'       => 'form-control',
  'autocomplete'=> 'off',
  );
$parentesco  =  array(
  '0'             => '---Elegir Opción---',
  '1'             => 'Titular',
  '2'             => 'Hijo/a',
  '3'             => 'Esposo/a',
  '4'             => 'Aparente Matrimonio',
  );
$cuit       = array(
  'name'        => 'cuit',
  'id'          => 'cuit',
  'size'        => 50,
  'value'       => set_value('cuit',@$socio[0]->cuit),
  'type'        => 'text',
  'class'       => 'form-control',
 // 'style'       => 'text-transform:uppercase',
 // 'onkeypress'  => 'return validarn(event);',
  );
  $nombre       = array(
  'name'        => 'nombre',
  'id'          => 'nombre',
  'size'        => 50,
  'value'       => set_value('nombre',@$socio[0]->nombre_pariente),
  'type'        => 'text',
  'class'       => 'form-control',
  'style'       => 'text-transform:uppercase',
  'onkeypress'  => 'return validarn(event);',
  );
  $fechanac    = array(
  'name'        => 'fechanac',
  'id'          => 'fechanac',
  'size'        => 50,
  'value'       => set_value('fechanac',@$socio[0]->fechanac),
  'type'        => 'date',
  'class'       => 'form-control',
  //'style'       => 'text-transform:uppercase',
  );
  $sexo = array(
  '0'             => '---Elegir Opción---',
  '1'             => 'Femenino',
  '2'             => 'Masculino',
  );
  $estadoCivil  = array(
  '0'             => '---Elegir Opción---',
  '1'             => 'Casado/a',
  '2'             => 'Soltero/a',
  '3'             => 'Divorciado/a',
  '4'             => 'Viudo/a',
  '5'             => 'Conviviente',
  );
  $caractCelular  = array(
  'name'        => 'caractCelular',
  'id'          => 'caractCelular',
  'size'        => 50,
  'value'       => set_value('caractCelular',@$socio[0]->caractCelular),
  'type'        => 'text',
  'class'       => 'form-control',
  );
  $celular  =  array(
  'name'        => 'celular',
  'id'          => 'celular',
  'size'        => 50,
  'value'       => set_value('celular',@$socio[0]->celular),
  'type'        => 'text',
  'class'       => 'form-control',
  );
  $email  =  array(
  'name'        => 'email',
  'id'          => 'email',
  'size'        => 50,
  'value'       => set_value('email',@$socio[0]->email),
  'type'        => 'text',
  'class'       => 'form-control',
    );
  $fechaIngreso   = array(
  'name'        => 'fechaIngreso',
  'id'          => 'fechaIngreso',
  'size'        => 50,
  'value'       => set_value('fechaIngreso',@$socio[0]->fechaIngreso),
  'type'        => 'date',
  'class'       => 'form-control',
  //'style'       => 'text-transform:uppercase',
  );
  $sitRevista  = array(
  '0'             => '---Elegir Opción---',
  '1'             => 'Planta Permanente',
  '2'             => 'Contratado',
  '3'             => 'Reemplazante/Suplente',
  '4'             => 'Interino',
  '5'             => 'Jornalizado',
  );
  $fechaVenc   = array(
  'name'        => 'fechaVenc',
  'id'          => 'fechaVenc',
  'size'        => 50,
  'value'       => set_value('fechaVenc',@$socio[0]->fechaVencimiento),
  'type'        => 'date',
  'class'       => 'form-control',
  //'style'       => 'text-transform:uppercase',
  );
  $domicilio    = array(
  'name'        => 'domicilio',
  'id'          => 'domicilio',
  'size'        => 50,
  'value'       => set_value('domicilio',@$socio[0]->domicilio),
  'type'        => 'text',
  'class'       => 'form-control',
  'style'       => 'text-transform:uppercase',
  );
  $nro    = array(
  'name'        => 'nro',
  'id'          => 'nro',
  'size'        => 50,
  'value'       => set_value('nro',@$socio[0]->nro),
  'type'        => 'text',
  'class'       => 'form-control',
 // 'style'       => 'text-transform:uppercase',
  );
  $piso         = array(
  'name'        => 'piso',
  'id'          => 'piso',
  'size'        => 50,
  'value'       => set_value('piso',@$socio[0]->piso),
  'type'        => 'text',
  'class'       => 'form-control',
 // 'style'       => 'text-transform:uppercase',
  );
  $dpto   = array(
  'name'        => 'dpto',
  'id'          => 'dpto',
  'size'        => 50,
  'value'       => set_value('dpto',@$socio[0]->dpto),
  'type'        => 'text',
  'class'       => 'form-control',
//  'style'       => 'text-transform:uppercase',
  );
  $bloque    = array(
  'name'        => 'bloque',
  'id'          => 'bloque',
  'size'        => 50,
  'value'       => set_value('bloque',@$socio[0]->bloque),
  'type'        => 'text',
  'class'       => 'form-control',
  //'style'       => 'text-transform:uppercase',
  ); 
  $telefono     = array(
  'name'        => 'telefono',
  'id'          => 'telefono',
  'size'        => 50,
  'value'       => set_value('telefono',@$socio[0]->telefono),
  'type'        => 'text',
  'class'       => 'form-control',
  'onkeypress'  => 'return validarNumeros(event);',
  );
   
 
?>
<script src="<?php echo base_url();?>js/JsonParentesco.js"></script>
<h1 class="page-header"><span class="glyphicon glyphicon-th-list"></span> <?php echo $titulo; ?></h1>
<div id="mensaje"></div>
<form class="form-horizontal" name="formulario" id="formulario" role="form">
  <input type="hidden" value="0" id="validamail" name="validamail">
  <input type="hidden" value="0" id="validarfc" name="validarfc">
  <div class="form-group">
  
  <div class="form-group">
    <label for="documento" class="col-lg-3 control-label">Documento:</label>
    <div class="col-lg-3">
      <?php echo form_input($documento); ?>
    </div>
  </div>
  
  <div class="form-group">
    <label for="buscaSocio" class="col-lg-3 control-label">Busqueda Socio:</label>
    <div class="col-lg-3">
      <?php echo form_input($busqueda); ?>
    </div>
  </div>
  <div class="form-group">
    <label for="cooperativa" class="col-lg-3 control-label">Cooperativa:</label>
      <div class="col-lg-3">
        <select name="cooperativa" id="cooperativa" class="form-control"></select>
      </div>
  </div>

<div class="form-group">
  <label for="parentesco" class="col-lg-3 control-label">Parentesco:</label>
    <div class="col-lg-3">
      <?php echo  form_dropdown('parentesco', $parentesco, set_value('parentesco',@$socio[0]->parentesco),'class="form-control" id="parentesco"'); ?>
    </div>
</div>

<div class="form-group">
    <label for="cuit" class="col-lg-3 control-label">Cuit:</label>
      <div class="col-lg-3">
        <?php echo form_input($cuit); ?>
      </div>
</div> 
  <label for="nombre" class="col-lg-3 control-label">Apellido y Nombre:</label>
    <div class="col-lg-3">
      <?php echo form_input($nombre); ?>
    </div>
</div>
  
  <div class="form-group">
    <label for="fechanac" class="col-lg-3 control-label">Fecha Nacimiento:</label>
      <div class="col-lg-3">
        <?php echo form_input($fechanac); ?>
      </div>
  </div>

<div class="form-group">
  <label for="sexo" class="col-lg-3 control-label">Sexo:</label>
    <div class="col-lg-3">
      <?php echo  form_dropdown('sexo', $sexo, set_value('sexo',@$socio[0]->sexo),'class="form-control" id="sexo"'); ?>
    </div>
</div>

<div class="form-group">
  <label for="estadoCivil" class="col-lg-3 control-label">Estado Civil:</label>
    <div class="col-lg-3">
      <?php echo  form_dropdown('estadoCivil', $estadoCivil, set_value('estadoCivil',@$socio[0]->estadoCivil),'class="form-control" id="estadoCivil"'); ?>
    </div>
</div>

<div class="form-group">
    <label for="caractCelular" class="col-lg-3 control-label">Característica Celular:</label>
      <div class="col-lg-3">
        <?php echo form_input($caractCelular); ?>
      </div>
</div>

<div class="form-group">
  <label for="celular" class="col-lg-3 control-label">Nro. Celular:</label>
    <div class="col-lg-3">
      <?php echo form_input($celular); ?>
    </div>
</div>

<div class="form-group">
    <label for="email" class="col-lg-3 control-label">E-mail:</label>
    <div class="col-lg-3">
      <?php echo form_input($email); ?>
    </div>
  </div>

<div class="form-group">
  <label for="fechaIngreso" class="col-lg-3 control-label">Fecha Ingreso:</label>
    <div class="col-lg-3">
      <?php echo form_input($fechaIngreso); ?>
    </div>
</div>

<div class="form-group">
  <label for="sitRevista" class="col-lg-3 control-label">Situación Revista:</label>
    <div class="col-lg-3">
      <?php echo  form_dropdown('sitRevista', $sitRevista, set_value('sitRevista',@$socio[0]->sitRevista),'class="form-control" id="sitRevista"'); ?>
    </div>
</div>

<div class="form-group">
  <label for="fechaVenc" class="col-lg-3 control-label">Fecha Vencimiento:</label>
    <div class="col-lg-3">
      <?php echo form_input($fechaVenc); ?>
    </div>
</div>

<div class="form-group">
  <label for="localidad" class="col-lg-3 control-label">Localidad:</label>
    <div class="col-lg-3">
        <select name="localidad" id="localidad" class="form-control"></select>
    </div>
</div>

<div class="form-group">
  <label for="domicilio" class="col-lg-3 control-label">Domicilio:</label>
    <div class="col-lg-3">
      <?php echo form_input($domicilio); ?>
    </div>
</div>

  <div class="form-group">
    <label for="nro" class="col-lg-3 control-label">Nro:</label>
    <div class="col-lg-3">
      <?php echo form_input($nro); ?>
    </div>
  </div>
  <div class="form-group">
    <label for="piso" class="col-lg-3 control-label">Piso:</label>
    <div class="col-lg-3">
      <?php echo form_input($piso); ?>
    </div>
  </div>
  <div class="form-group">
    <label for="dpto" class="col-lg-3 control-label">DPTO:</label>
    <div class="col-lg-3">
      <?php echo form_input($dpto); ?>
    </div>
  </div>
    
 <div class="form-group">
    <label for="bloque" class="col-lg-3 control-label">Bloque:</label>
    <div class="col-lg-3">
      <?php echo form_input($bloque); ?>
    </div>
  </div>
	
	<div class="form-group">
    <label for="telefono" class="col-lg-3 control-label">Telefono:</label>
    <div class="col-lg-3">
      <?php echo form_input($telefono); ?>
    </div>
  </div>
	
  <div class="form-group">
    <div class="col-lg-offset-3 col-lg-10">
      <button type="button" onclick="regresar()" class="btn btn-primary"><span class="glyphicon glyphicon-circle-arrow-left"></span> Regresar</button>
      <button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-saved"></span> Guardar Socio</button>
      <?php if($titulo=="Nuevo Pariente"){ ?>
      <button type="reset" class="btn btn-primary"><span class="glyphicon glyphicon-plus"></span> Nuevo Pariente </button>
      <?php } ?>
    </div>
  </div>
  <hr/>
</form>		
<script type="text/javascript">
  
</script>
