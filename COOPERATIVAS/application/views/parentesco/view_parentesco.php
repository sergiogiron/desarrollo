<style type="text/css">
th, td { white-space: nowrap; }
    div.dataTables_wrapper {
        width: 100%;
        margin: 0 auto;
    }
</style>
<script type="text/javascript">
var baseurl = "<?php echo base_url(); ?>";
var currentLocation = window.location;
function Eliminarsocio(soci, id){
    confirmar=confirm("Eliminar a " + soci + ", Recuerda Una vez Eliminado No podras Recuperarlo"); 

    if (confirmar){
    	document.getElementById('mensaje').innerHTML = "<div class='modal1'><div class='center1'> <center> <img src='"+ baseurl +"/img/gif-load.gif'> Eliminando Socio...</center></div></div>";
    	 var socio		 = new Object();
		 socio.Id      	 = id;
		
		var DatosJson = JSON.stringify(socio);
		$.post(currentLocation + '/deletesocio',
		{ 
			SociosPost: DatosJson
		},
		function(data, textStatus) {
			
			$("#mensaje").html(data.error_msg);
		}, 
		"json"		
		);
    } else{
    } 
  }
  
</script>
<h1 class="page-header"><span class="glyphicon glyphicon-list-alt"></span> Parientes de Socios de Cooperativas</h1>
<div id="mensaje"></div>
<p align="right">
 	 <a href="parentesco/nuevo">
 	 	<button type="button" class="btn btn-primary"><span class="glyphicon glyphicon-ok"></span> Nuevo Pariente</button>
 	 </a>  
 	 </p>
 	 <br/>
	<table id="socios" border="0" cellpadding="0" cellspacing="0" width="100%" class="pretty">
		<thead>
			<tr>
				<th></th>
				<th>Documento</th>
				<th>Nombre</th>
				<th>Parentesco</th>
				<th>Cooperativa</th>
			</tr>
		</thead>
		<tbody>
			<?php
				if($socios){
					foreach($socios as $socio){
					  
					   $i=$socio->parentesco;
					   //$sociop="";
					   
                       switch ($i) {
                       case 0:
                              $sociop = '';
                              break;
                       case 1:
                              $sociop = 'Titular';
                              break;
                       case 2:
                                $sociop ='Hijo/a';
                               break;
					     case 3:
					   
                              $sociop = 'Esposo/a';
                              break;
					    case 4:
					   
                              $sociop = 'Aparente Matrimonio';
                              break;
    					   
                    } 
						$idSocio     = base64_encode($socio->id_pariente);
						echo '<tr>';
						echo '<td>';
						echo '<a href="parentesco/editarSocio/'.$idSocio.'"><button type="button" title="Editar Parentesco de Socio" class="btn btn-success btn-xs"><span class="glyphicon glyphicon-edit"></span></button></a> &nbsp;';
						?>
						<button type="button" onclick="Eliminarsocio('<?php echo $socio->nombre_pariente; ?>','<?php echo $idSocio; ?>');" title="Eliminar Socio" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span></button>
						<?php
						echo '</td>';
						echo '<td>'.$socio->documento.'</td>';
						echo '<td>'.$socio->nombre_pariente.'</td>';
						echo '<td>'.$sociop.'</td>'; //campo de la tabla sociosfamilia
						echo '<td>'.$socio->razon.'</td>';
						echo '</tr>';
					}
				}else{
					echo '<tr><td colspan=5><center>No Existe Informacion</center></td></tr>';
				}
			?>
		</tbody>
	</table>
<script type="text/javascript">

            $(document).ready(function() {
    $('#socios').dataTable( {
        "scrollX": false
    } );
} );

</script>
