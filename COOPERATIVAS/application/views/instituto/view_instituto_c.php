<head>
<?php header("Content-Type: text/html;charset=utf-8");?>

 <script type="text/javascript" src="<?php echo base_url()?>js/jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url()?>js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url()?>js/jquery.dataTables.js"></script>
    <script type="text/javascript" src="<?php echo base_url()?>js/JsValidacion.js"></script>
    <script type="text/javascript" src="<?php echo base_url()?>js/treeMenu.js"></script> 


<script type="text/javascript">
baseurl="<?php echo base_url(); ?>";
function regresar(){
    window.location="<?php echo base_url()?>instituto_c";
  }

</script>






<?php
if(!isset($matri)){
$matri="";
}

  $NumeroDoc      = array(
  'name'        => 'NumeroDoc',
  'id'          => 'NumeroDoc',
  'size'        => 20,
   'value'       => set_value('NumeroDoc',$matri),
   'type'        => 'text',
  'class'       => 'form-control',
  );
  
  
   $Localidad     = array(
  'name'        => 'Localidad',
  'id'          => 'Localidad',
  'value'       => set_value('Localidad',@$Adj[0]->Localidad),
  'size'        => 50,
  'type'        => 'text',
   'disabled'    => 'disabled',
  'class'       => 'form-control',
  );
   $Barrio           = array(
  'name'        => 'Barrio',
  'id'          => 'Barrio',
  'size'        => 50,
  'value'       => set_value('Barrio',@$Adj[0]->Barrio),
  'type'        => 'text',
   'disabled'    => 'disabled',
  'class'       => 'form-control',
  );
   $Mza           = array(
  'name'        => 'Mza',
  'id'          => 'Mza',
  'size'        => 50,
  'value'       => set_value('Mza',@$Adj[0]->Mza),
  'type'        => 'text',
  'disabled'    => 'disabled', 
  'class'       => 'form-control',
  );
   $Lote           = array(
  'name'        => 'Lote',
  'id'          => 'Lote',
  'size'        => 50,
  'value'       => set_value('Lote',@$Adj[0]->Lote),
   'disabled'    => 'disabled',
  'type'        => 'text',
  'class'       => 'form-control',
  );

   $Cooperativa           = array(
  'name'        => 'Cooperativa',
  'id'          => 'Cooperativa',
  'size'        => 50,
   'disabled'    => 'disabled',
  'value'       => set_value('Cooperativa',@$Adj[0]->Cooperativa),
  'type'        => 'text',
  'class'       => 'form-control',
  ); 
   
   
 
    



?>
 <script src="<?php echo base_url();?>js/JsonInstituto_c.js"></script> 
<script type="text/javascript">
function imprSelec(muestra)
{var ficha=document.getElementById(muestra);var ventimp=window.open(' ','popimpr');ventimp.document.write(ficha.innerHTML);ventimp.document.close();ventimp.print();ventimp.close();}
</script>

<h1 class="page-header"><span class="glyphicon glyphicon-list-alt"></span> Consulta de Cooperativas Adjudicadas</h1>
       

<form class="navbar-form navbar-center" name="formBusqueda" role="search" action="<?php echo base_url();?>instituto_c/buscarDoc" >
  
   <div class="form-group">
      <label for="NumeroDoc" class="col-lg-7 control-label">Nro. de Matriculas: </label>
        <div class="col-lg-3">
            <?php echo form_input($NumeroDoc); ?>
        </div>
   </div>
   
   
   <div class="form-group">
      <div class="col-lg-7 control-label">
        <div class="col-lg-3">
          <button type="submit" name="buscadorNota" id="buscadorNota" class="btn btn-primary"><span class="glyphicon glyphicon-search"> Buscar </button>
      </div>
    </div>
   </div>


</form>




<?php  


if((isset($Adj))and (isset($mensaje))){


?>
<hr/>
<br>
<br>
 <div id="mensaje"></div> 
 <form class="form-horizontal" name="formulario" id="formulario" role="form">

     
  <div class="form-group">
    <label for="Cooperativa" class="col-lg-3 control-label">Adjudicado a la Cooperativa:</label>
    <div class="col-lg-3">
      <?php echo form_input($Cooperativa); ?>
    </div>
  </div>
  
  
<div class="form-group">
    <label for="Localidad" class="col-lg-3 control-label">Localidad:</label>
    <div class="col-lg-3"> 
	 <?php echo form_input($Localidad); ?>
  	</div>
</div>
 

  <div class="form-group">
    <label for="Barrio" class="col-lg-3 control-label">Barrrio:</label>
    <div class="col-lg-3">
      <?php echo form_input($Barrio); ?>
    </div>
  </div>
 
   <div class="form-group">
    <label for="Mza" class="col-lg-3 control-label">Mza:</label>
    <div class="col-lg-3">
      <?php echo form_input($Mza); ?>
    </div>
  </div>
  
  <div class="form-group">
    <label for="Lote" class="col-lg-3 control-label">Lote:</label>
    <div class="col-lg-3">
      <?php echo form_input($Lote); ?>
    </div>
  </div>
  
   
   
  </div>
</form>	

<?php 

} else {

echo("");
    

}


if(isset($mensaje)){
?>

    <br>
   <br>
   
  
<hr/>
<center>
<h1>  <?php echo "  "; ?></h1>
<h1>  <?php echo $mensaje; ?></h1>
<h1>  <?php echo "  "; ?></h1>
</center>
<hr/>
 <br>
   <br> 
<div class="form-group">
    <div class="col-lg-offset-3 col-lg-10">
	
      <button type="button" onClick="regresar()" class="btn btn-primary"><span class="glyphicon glyphicon-circle-arrow-left"></span> Regresar</button>
      
    </div>
  </div>
  
  
  
  
<?php



}


?>
