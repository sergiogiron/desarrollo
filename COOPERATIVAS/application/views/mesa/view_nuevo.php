<input type="hidden" value="<?php echo @$cupo[0]->id_mesa; ?>" id="id" name="id"> 
<input type="hidden" name="hdesc" id="hdesc" value="<?php echo @$cupo[0]->descripcion;?>"> 
<input type="hidden" name="hmotivo" id="hmotivo" value="<?php echo @$cupo[0]->motivo; ?>"> 
<input type="hidden" name="nuevo" id="nuevo" value=""> 
<input type="hidden" name="user" id="user" value="<?php echo @$cupo[0]->solicitante; ?>"> 
<input type="hidden" name="resp" id="vresp" value="<?php echo @$cupo[0]->respuesta; ?>"> 

<script type="text/javascript">
  var baseurl = "<?php echo base_url(); ?>";
  var nombres ="<?php echo $this->session->userdata('NOMBRE')?>";
  
  
  
  var ids           = document.getElementById("id").value;
  ids               = parseInt(ids.length);
  var d = document.getElementById("hdesc").value;
  var m=document.getElementById("hmotivo").value;
  var u=document.getElementById("user").value;
   var r =document.getElementById("vresp").value;
  if(ids==0){
        idloca       = 0;
		idprogra      = 0;
		document.getElementById("nuevo").value="no";// no es igual a  nueva solicitud
		
  }else{
    
	document.getElementById("nuevo").value="si";	//si es igual a edicion
	  
  }
  
  
 
  function regresar(){
    window.location="<?php echo base_url()?>mesa";
  }
</script>
<?php

$estados  = array(

 '1'             => 'Solicitud',
  '2'             => 'Analizando Solicitud',
  '3'             => 'En Proceso de Resolucion',
  '4'             => 'Consulta Resuelta',
 );




  $Cuposs       = array(
  'name'        => 'Cuposs',
  'id'          => 'Cuposs',
  'size'        => 50,
  'value'       => set_value('Cuposs',@$cupo[0]->cupo),
  'type'        => 'text',
  'class'       => 'form-control',
   );
  
   $Diferencia       = array(
  'name'        => 'Diferencia',
  'id'          => 'Diferencia',
  'size'        => 50,
  'value'       => set_value('Diferencia',@$cupo[0]->diferencia_cupo),
  'type'        => 'text',
  'class'       => 'form-control',
   );

 
?>

<h1 class="page-header"><span class="glyphicon glyphicon-th-list"></span> <?php echo $titulo; ?></h1>
<div id="mensaje" name="mensaje"></div>
<body >
<form class="form-horizontal" name="formulario" id="formulario" role="form">
  
  <div class="form-group">
  
     <div class="form-group">
    <label for="descripcion" class="col-lg-3 control-label">Descripcion:</label>
    <div class="col-lg-3"> 
	<input  text name="desc" id="desc"  class="form-control" maxlength="300"> 
  	</div>
	
  </div>
  
  <div class="form-group">
    <label for="Motivo" class="col-lg-3 control-label">Detalle su problema:</label>
    <div class="col-lg-3"> 
	<textarea  name="mot" id="mot" rows="5" class="form-control" maxlength="300"> </textarea>
  	</div>
	
  </div>
  
   <div class="form-group">
    <label  id ="Respuesta" name="Respuesta" class="col-lg-3 control-label">Respuesta:</label>
    <div class="col-lg-3"> 
	<textarea  name="resp" id="resp" rows="5" class="form-control" maxlength="300"> </textarea>
  	</div>
	
  </div>
  
  
  
   <div class="form-group">
    <label for="localidad" class="col-lg-3 control-label">Solicitante:</label>
    <div class="col-lg-3"> 
	<input text name="solicitante" id="solicitante" disabled  value ="<?php echo $this->session->userdata('NOMBRE') ?>" class="form-control" > 
  	</div>
	
  </div>
  
  <div class="form-group">
    <label for="Estado" class="col-lg-3 control-label">Estado Solicitud:</label>
    <div class="col-lg-3">
	 <?php echo  form_dropdown('estado', $estados, set_value('estado',@$cupo[0]->estado),'class="form-control" id="estado" name="estado"'); ?>
      </div>
  </div>
  
  <div class="form-group">
    <div class="col-lg-offset-3 col-lg-10">
      <button type="button" onclick="regresar()" class="btn btn-default">Regresar</button>
      <button type="submit" class="btn btn-primary" name="btnGuardar" id="btnGuardar"><span class="glyphicon glyphicon-saved"></span> Enviar Solicitud</button>
      <?php if($titulo=="Nueva Consulta"){ ?>
      <button type="reset" class="btn btn-default">Nuevo</button>
      <?php } ?>
    </div>
  </div>
  <hr/>
</form>	


 <script src="<?php echo base_url();?>js/JsonMesa.js"></script>
 
 
 <script type="text/javascript">

            $(document).ready(function() {
    if (document.getElementById("nuevo").value=="si")
		
		{
		
		document.getElementById("desc").value =d;
		document.getElementById("mot").value =m;
		document.getElementById("solicitante").value =u;
		document.getElementById("resp").value =r;
		document.getElementById('resp').style.display = 'block';
		document.getElementById('Respuesta').style.display = 'block';
		 if (nombres!="FEROZO")
	{
		$("#estado").prop("disabled", true);
		$("#mot").prop("disabled", true);
		$("#estado").prop("disabled", true);
		$("#desc").prop("disabled", true);
		$("#resp").prop("disabled", true);
		$("#btnGuardar").remove();
	}		
	
else
{
	$("#estado").prop("disabled", false);
		$("#mot").prop("disabled", false);
		$("#estado").prop("disabled", false);
		$("#desc").prop("disabled", false);
		$("#resp").prop("disabled", false);
	
}	
		 
		 
		}
		
		else {
 $("#estado").prop("disabled", true);
 
 document.getElementById("estado").value =1;
 document.getElementById('resp').style.display = 'none';
		document.getElementById('Respuesta').style.display = 'none';
			
		}
		
	
		
		
} );

</script>
</body>