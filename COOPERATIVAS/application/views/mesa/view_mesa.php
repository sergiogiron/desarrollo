<style type="text/css">
th, td { white-space: nowrap; }
    div.dataTables_wrapper {
        width: 100%;
        margin: 0 auto;
    }
</style>
<script type="text/javascript">
var baseurl = "<?php echo base_url(); ?>";
var Sop ="<?php echo $this->session->userdata('NOMBRE')?>";

var currentLocation = window.location;
function EliminarCupo(nombre, id){
    confirmar=confirm("Eliminar a " + nombre + ", Recuerda Una vez Eliminado No podras Recuperarlo"); 

    if (confirmar){
    	document.getElementById('mensaje').innerHTML = "<div class='modal1'><div class='center1'> <center> <img src='"+ baseurl +"/img/gif-load.gif'> Eliminando Programa...</center></div></div>";
    	 var Cu 		 = new Object();
		Cu.Id      	 = id;
		Cu.Nombre      = nombre;
		var DatosJson = JSON.stringify(Cu);
		$.post(currentLocation + '/deletecupo',
		{ 
			ProgramaPost: DatosJson
		},
		function(data, textStatus) {
			//
			$("#mensaje").html(data.error_msg);
		}, 
		"json"		
		);
    } else{
    } 
  }
  
</script>
<h1 class="page-header"><span class="glyphicon glyphicon-list-alt"></span> Mesa de Ayuda</h1>
<div id="mensaje"></div>
<p align="right">
 	 <a href="./mesa/nuevo">
 	 	<button type="button" class="btn btn-primary"><span class="glyphicon glyphicon-ok"></span> Nueva Solicitud</button>
 	 </a>  
 	 </p>
 	 <br/>
	<table id="cupos" border="0" cellpadding="0" cellspacing="0" width="100%" class="pretty">
		<thead>
			<tr>
				<th></th>
				<th>Id</th>
				<th>Descripcion</th>
				<th>Solicitante</th>
				<th>Estado</th>
				<th>fecha del pedido</th>
				
			</tr>
		</thead>
		<tbody>
			<?php
				if($cupos){
					foreach($cupos as $cupo){
						$idCupo     = base64_encode($cupo->id_mesa);
						echo '<tr>';
						echo '<td>';
						echo '<a href="mesa/editar/'.$idCupo.'"><button type="button"  title="Editar Cupo" class="btn btn-success btn-xs"><span class="glyphicon glyphicon-edit"></span></button></a> &nbsp;';
						?>
						
						<?php
						echo '<a href="mesa/view_img/'.$idCupo.'"><button type="button"  title="SUBIR ARCHIVO" class="btn btn-success btn-xs"><span class="glyphicon glyphicon-open"></span></button></a> &nbsp;';
						echo '<a href="mesa/view_mostrar/'.$idCupo.'" target="_blank"><button type="button" title="MOSTRAR ARCHIVO" class="btn btn-success btn-xs"><span class="glyphicon glyphicon-eye-open"></span></button></a> &nbsp;';
						echo '</td>';
						echo '<td>'.$cupo->id_mesa.'</td>';
						echo '<td>'.$cupo->descripcion.'</td>';
			            echo '<td>'.$cupo->solicitante.'</td>';
						
						switch ($cupo ->estado) 
	{
       
		 case 1:
                $estadoSol= "Solicitud";
                break;
          case 2:
                $estadoSol= "Analizando Solicitud ";
                break;
		  case 3:
                $estadoSol= "En Proceso de Resolucion";
                break;	
          case 4:
                $estadoSol= "Consulta Resuelta";
                break;
				
				
		  };	
						echo '<td>'.$estadoSol.'</td>';
						echo '<td>'.$cupo->fecha.'</td>';
					
						echo '</tr>';
					}
				}else{
					echo '<tr><td colspan=5><center>No Existe Informacion</center></td></tr>';
				}
				
			
				
				
			?>
		</tbody>
	</table>
<script type="text/javascript">

            $(document).ready(function() {
    $('#cupos').dataTable( {
        "scrollX": false
    } );
	
	
	
} );

</script>