<style type="text/css">
th, td { white-space: nowrap; }
    div.dataTables_wrapper {
        width: 100%;
        margin: 0 auto;
    }
</style>
<script type="text/javascript">
var baseurl = "<?php echo base_url(); ?>";
var currentLocation = window.location;
function EliminarArea(nombre, id){
    confirmar=confirm("Eliminar a " + nombre + ", Recuerda Una vez Eliminado No podras Recuperarlo"); 

    if (confirmar){
    	document.getElementById('mensaje').innerHTML = "<div class='modal1'><div class='center1'> <center> <img src='"+ baseurl +"/img/gif-load.gif'> Eliminando Area...</center></div></div>";
    	 var area 		 = new Object();
		area.Id      	 = id;
	//	tari.Codigo      = codigo;
		var DatosJson = JSON.stringify(area);
		$.post(currentLocation + '/deletearea',
		{ 
			AreasPost: DatosJson
		},
		function(data, textStatus) {
			//
			$("#mensaje").html(data.error_msg);
		}, 
		"json"		
		);
    } else{
    } 
  }
  
</script>
<h1 class="page-header"><span class="glyphicon glyphicon-list-alt"></span> Areas</h1>
<div id="mensaje"></div>
<p align="right">
 	 <a href="areas/nuevo">
 	 	<button type="button" class="btn btn-primary"><span class="glyphicon glyphicon-ok"></span> Nueva Area</button>
 	 </a>  
 	 </p>
 	 <br/>
	<table id="areas" border="0" cellpadding="0" cellspacing="0" width="100%" class="pretty">
		<thead>
			<tr>
				<th></th>
				<th>Codigo</th>
				<th>Nombre</th>
				<th>Observaciones</th>				
			
				
			</tr>
		</thead>
		<tbody>
			<?php
				if($areas){
					foreach($areas as $area){
						$id     = base64_encode($area->id_area);
						echo '<tr>';
						echo '<td>';
						echo '<a href="areas/editarArea/'.$id.'"><button type="button" title="Editar Area" class="btn btn-success btn-xs"><span class="glyphicon glyphicon-edit"></span></button></a> &nbsp;';
						?>
						<button type="button" onclick="EliminarArea('<?php echo $area->nombre_area; ?>','<?php echo $id; ?>');" title="Eliminar Area" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span></button>
						<?php
						echo '</td>';
						echo '<td>'.str_pad((int) $area->codigo_area,4,"0",STR_PAD_LEFT).'</td>';
						//echo '<td>'.$area->codigo_area.'</td>';
						echo '<td>'.$area->nombre_area.'</td>';
						echo '<td>'.$area->obs_area.'</td>';
						
						
						echo '</tr>';
					}
				}else{
					echo '<tr><td colspan=5><center>No Existe Informacion</center></td></tr>';
				}
			?>
		</tbody>
	</table>
<script type="text/javascript">

            $(document).ready(function() {
    $('#areas').dataTable( {
        "scrollX": false
    } );
} );

</script>
			