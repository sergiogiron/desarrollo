<input type="hidden" value="<?php echo @$area[0]->id_area; ?>" id="id" name="id"> 
<script type="text/javascript">
  var baseurl = "<?php echo base_url(); ?>";
 
  function regresar(){
    window.location="<?php echo base_url()?>areas";
  }
</script>
<?php

if($titulo =="Nueva Area"){
 //NRO CODIGO nuevo
  
     $codigo = array(
     'name'        => 'codigo',
     'id'          => 'codigo',
     'size'        => 50,
     'value'       => set_value('codigo',str_pad((int) @$max[0]->max + 1,4,"0",STR_PAD_LEFT)),
     'type'        => 'text',
     'class'       => 'form-control',
  
  );
   
   }else{ 
          $codigo = array(
          'name'        => 'codigo',
          'id'          => 'codigo',
          'size'        => 50,

		  'value'       => set_value('codigo',str_pad((int) @$area[0]->codigo_area,4,"0",STR_PAD_LEFT)),
          'type'        => 'text',
          'class'       => 'form-control',
          );
       
     }
 

  $nombre    = array(
  'name'        => 'nombre',
  'id'          => 'nombre',
  'size'        => 50,
  'value'       => set_value('nombre',@$area[0]->nombre_area),
  'type'        => 'text',
  'class'       => 'form-control',
//  'style'       => 'text-transform:uppercase',
  );
  
  $obs    = array(
  'name'        => 'obs',
  'id'          => 'obs',
  'size'        => 50,
  'value'       => set_value('obs',@$area[0]->obs_area),
  'type'        => 'text',
  'class'       => 'form-control',
 // 'onkeypress'  => 'return validarNumeros(event);'
  );
  
 
?>
<script src="<?php echo base_url();?>js/JsonAreas.js"></script>
<h1 class="page-header"><span class="glyphicon glyphicon-th-list"></span> <?php echo $titulo; ?></h1>
<div id="mensaje"></div>
<form class="form-horizontal" name="formulario" id="formulario" role="form">
  
  <div class="form-group">
    <label for="codigo" class="col-lg-3 control-label">Codigo:</label>
    <div class="col-lg-3">
      <?php echo form_input($codigo); ?>
    </div>
  </div>
  
    

  <div class="form-group">
    <label for="nombre" class="col-lg-3 control-label">Nombre del Area:</label>
    <div class="col-lg-3">
      <?php echo form_input($nombre); ?>
    </div>
  </div>

  <div class="form-group">
    <label for="obs" class="col-lg-3 control-label">Observaciones:</label>
    <div class="col-lg-3">
      <?php echo form_input($obs); ?>
    </div>
  </div>

 

 
  <div class="form-group">
    <div class="col-lg-offset-3 col-lg-10">
      <button type="button" onclick="regresar()" class="btn btn-default">Regresar</button>
      <button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-saved"></span> Guardar Area</button>
      <?php if($titulo=="Nueva Area"){ ?>
      <button type="reset" class="btn btn-default">Nuevo</button>
      <?php } ?>
    </div>
  </div>
  <hr/>
</form>		
<script type="text/javascript">
  
</script>
