<input type="hidden" id="id" name="id" value="<?php echo @$resolucion[0]->id_resolucion; ?>" >
<!--<input type="hidden" id="coope"name="coope" value="<?php echo @$denuncia[0]->id_cooperativa; ?>" >-->
 
<script type="text/javascript">

  var baseurl = "<?php echo base_url(); ?>";
/*
  var idcoope        = 0;
  var ids       = document.getElementById("id").value;
  ids           = parseInt(ids.length);
  
  if(ids==0){
        idcoope      = 0;
       
      }else{
     idcoope     = document.getElementById("coope").value;      
  }
*/
  function regresar(){
    window.location="<?php echo base_url()?>resolucion";
  }

</script>

<?php
  $Codigo       = array(
  'name'        => 'codigo',
  'id'          => 'codigo',
  'size'        => 50,
  'value'       => set_value('codigo',@$resolucion[0]->codigo),
  'type'        => 'text',
  'class'       => 'form-control',
  );

  $Año       = array(
  'name'        => 'año',
  'id'          => 'año',
  'size'        => 50,
  'value'       => set_value('año',@$resolucion[0]->año),
  'type'        => 'text',
  'class'       => 'form-control',
   );

  $Asunto     = array(
  'name'        => 'asunto',
  'id'          => 'asunto',
  'size'        => 50,
  'value'       => set_value('asunto',@$resolucion[0]->asunto),
  'type'        => 'text',
  'class'       => 'form-control',
   );

  $Gobierno  = array(
  '0'             => '---Elegir Opción---',
  '1'             => 'Secretaria de Gobierno Instituto de Vivienda',
  '2'             => 'Secretaria de Ordenamiento de Tierra y Vivienda',
  '3'             => 'Otros',
  );
  /*
  $Imagen  = array(
  'name'             => 'imagen',
  'id'               => 'imagen',
  'size'             => 50,
  'value'            => set_value('imagen',@$denuncia[0]->foto_denuncia),
  'type'             => 'text',
  'class'            =>'form-control',
  );
*/
  $Observaciones  = array(
  'name'             => 'observaciones',
  'id'               => 'observaciones',
  'size'             => 50,
  'value'            => set_value('observaciones',@$resolucion[0]->obs),
  'type'             =>'text',
  'class'            =>'form-control',
  );
?>

<script src="<?php echo base_url();?>js/JsonResolucion.js"></script>
<h1 class="page-header"><span class="glyphicon glyphicon-th-list"></span> <?php echo $titulo; ?></h1>
<div id="mensaje"></div>
<form class="form-horizontal" name="formulario" id="formulario" role="form">

<div class="form-group">
    <label for="codigo" class="col-lg-3 control-label">Codigo:</label>
      <div class="col-lg-3">
        <?php echo form_input($Codigo); ?>
      </div>
</div>
<div class="form-group">
  <label for="Año" class="col-lg-3 control-label">Año:</label>
      <div class="col-lg-3">
        <?php echo form_input($Año); ?>
      </div>
</div>
<div class="form-group">
  <label for="asunto" class="col-lg-3 control-label">Asunto:</label>
    <div class="col-lg-3">
	    <!--<select name="cooperativa" id="cooperativa" class="form-control"></select>-->
      <?php echo form_input($Asunto); ?>
    </div>
</div>
<div class="form-group">
  <label for="gobierno" class="col-lg-3 control-label">Gobierno:</label>
    <div class="col-lg-3">
     <?php echo  form_dropdown('gobierno', $Gobierno, set_value('gobierno',@$resolucion[0]->gobierno),'class="form-control" id="gobierno"'); ?>
    </div>
</div>
<div class="form-group">
      <label for="observaciones" class="col-lg-3 control-label">Observaciones:</label>
      <div class="col-lg-3">
        <?php echo form_input($Observaciones); ?>
      </div>
  </div>  
 <!-- 
  <div class="form-group">
    <label for="imagen" class="col-lg-3 control-label"> Adjuntar Denuncia: </label>
    <div class="col-lg-3">
	  <?php echo form_upload('file') ?>
    </div>
  </div>
-->
  <div class="form-group">
    <div class="col-lg-offset-3 col-lg-10">
      <button type="button" onclick="regresar()" class="btn btn-primary"><span class="glyphicon glyphicon-circle-arrow-left"></span> Regresar</button>
      <button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-saved"></span> Guardar </button>
      <?php if($titulo=="Nueva Resolucion"){ ?>
      <button type="reset" class="btn btn-primary"><span class="glyphicon glyphicon-plus"></span> Nuevo</button>
      <?php } ?>
    </div>
  </div>
  <hr/>
</form>		
<script type="text/javascript">
  
</script>
