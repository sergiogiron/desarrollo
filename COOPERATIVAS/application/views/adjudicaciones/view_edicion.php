

<h1 class="page-header"><span class="glyphicon glyphicon-th-list"></span> Edicion de Adjudicacion</h1>
<div id="mensaje"></div>
<script type="text/javascript">
  var baseurl = "<?php echo base_url(); ?>";
  var valorSeleccionado="<?php echo @$adj[0]->id_localidad; ?>";
  var programaSelecionado="<?php echo@$adj[0]->id_programa; ?>";
  var beneficiario_id="<?php echo@$adj[0]->id_beneficio; ?>";
  var resolucion ="<?php echo@$adj[0]->Resolucion; ?>"
  var cooperativa ="<?php echo@$adj[0]->id_coperativa; ?>"
  
</script>
<script src="<?php echo base_url()?>js/jquery-1.10.2.js"></script>
<script src="<?php echo base_url()?>js/jquery-ui.js"></script>
<script src="<?php echo base_url();?>js/JsonABM.js"></script> 
<script type="text/javascript">
function AsignaSession(){
  document.getElementById("idsessionventa").value="<?php echo md5(rand(1000,50000)); ?>";
}
function Regresar(){
  if(confirm("Si Regresas se Perderan los Cambios")) { 
    window.location="<?php echo base_url()."adjudicaciones/todo"?>";
  }else{

    return false;
  }

}
</script>


<table border=0 width="100%">
	<tr>
		<td colspan="2">
			<table>
			<tr>
			<td>Programa:</td>
			<td> <input type="text" disabled  name="programa" id="programa" value="<?php echo @$adj[0]->nombre_programa; ?>">
			</tr> 
			<tr>
			<td>Localidad: </td>
			<td> <input type="text" disabled  name="localidad" id="localidad" value="<?php echo @$adj[0]->nombre_localidad; ?>">
			
	  <td>&nbsp;</td>
			</tr> 
			
				<tr>
					<td>Cooperativa:</td>
					<td><input type="text" disabled name="Coperativa" id="Coperativa" class="form-control input-sm" autocomplete="off" size="50" value="<?php echo @$adj[0]->razonsocial." Matricula :".@$adj[0]->matricula; ?>"/></td>
					
					<td>&nbsp;<label id="lblNombrecliente" name="lblNombrecliente"> </label>
                              <label id="lblidcliente" name="lblidcliente"> </label>
                      </td>
					 
				</tr>
			<tr>
			 
	<td>Beneficiario: </td>
			<td> <input type="text" disabled  name="beneficiarioAct" id="beneficiarioAct" value="<?php echo @$adj[0]->nombre_beneficios." DNI :".@$adj[0]->documento; ?>" size="60">
	 
		<td>&nbsp;<button  id="btnCambiobene"class="btn btn-default" onclick="CambiaBene();"><span class="glyphicon glyphicon-retweet"></span> Cambiar beneficiario</button></td>
			<td> <select name="estadobene" id="estadobene"  class="form-control" >
			<option value="1">inscripcion</option>
            <option value="2">Habilitado</option>
            <option value="4">Pendiente</option>
         <option value="5">observardo</option>
			
			</select>
			</td>
			<td>Observacion: </td>
			<td>  <input type="text"   name="beneficiarioActObs" id="beneficiarioActObs" > </td>
</tr>
		
	</tr>
	
    <tr>
	
     <td id="tdbeneficio">Beneficiarios:</td>
      <td><input type="text" name="BuscaBeneficiario" id="BuscaBeneficiario"  class="form-control input-sm" autocomplete="off" size="30"/></td>
	  <td> <button id="btnconfirmabene" name="btnconfirmabene"  class="btn btn-success" onclick="SelecionarBeneficiario();" ><i class="fa fa-user-plus"></i> Selecionar beneficiario</button></td>
	    <td> <button id="updateBene" name="updateBene"  class="btn btn-success" onclick="UpdateBene();" ><i class="fa fa-floppy-o"></i> Registrar Cambios</button></td>
	  <td>
	
        <input type="hidden" name="nombre_beneficiarios"  id="nombre_beneficiarios" />
       <input type="hidden" name="dni"  id="dni" /> 
        <input type="hidden" name="bene_codigo"  id="bene_codigo" />
        <input type="hidden" name="ciudad_bene"  id="ciudad_bene" />
        <input type="hidden" name="barrio"  id="barrio" />
		<input type="hidden" name="mza"  id="mza" />
		<input type="hidden" name="lote"  id="lote" />
		<input type="hidden" name="obs"  id="obs" />
		<input type="hidden" name="id_ciudad"  id="id_ciudad" />
		</td>
       
	</tr>
	<td id="fecha">fecha:</td>
	 <td> <input type="text" disabled  name="fecha1" id="fecha1" value="<?php echo @$adj[0]->fecha_impresion; ?>"> <td>&nbsp;<button  id="btnCambiofecha"class="btn btn-default" onclick="cambioFecha();"><span class="glyphicon glyphicon-retweet"></span> Cambiar fecha</button></td> </td>
	   
	 <td> <input name='fecha2' disabled id='fecha2'  type='date' > </td>
	 
	  <td> <button id="updatefecha" name="updatefecha"  class="btn btn-success" onclick="Updatefecha();" ><i class="fa fa-floppy-o"></i> Guarda Fecha</button></td>
	  <td>
	 <tr>
	 <td>N° DE NOTA: </td>
			<td> <input type="text" disabled  name="notaAct" id="notaAct" value="<?php echo @$adj[0]->nro_nota ?>">
	 
		<td>&nbsp;<button  id="btnCambionota"class="btn btn-default" onclick="cambiaNota()"><span class="glyphicon glyphicon-retweet"></span> Cambiar nota</button></td>
	 <td> <input type="number"   name="notaNvo" id="notaNvo" value="<?php echo @$adj[0]->nro_nota ?>">
	 <td> <button id="updateNota" name="updateNota"  class="btn btn-success" onclick="UpdateNota();" ><i class="fa fa-floppy-o"></i> Guarda Nota</button></td>
	 </tr>
	 
	  <tr>
	 <td>AÑO: </td>
			<td> <input type="text" disabled  name="anioAct" id="anioAct" value="<?php echo @$adj[0]->anio ?>">
	 
		<td>&nbsp;<button  id="btnCambioanio"class="btn btn-default" onclick="cambiaAnio();"><span class="glyphicon glyphicon-retweet"></span> Cambiar año</button></td>
	 <td> <input type="number"  name="anioNvo" id="anioNvo" value="<?php echo @$adj[0]->anio ?>">
	 <td> <button id="updateanio" name="updateanio"  class="btn btn-success" onclick="UpdateAnio();" ><i class="fa fa-floppy-o"></i> Guardar Año</button></td>
	 </tr>
	  <tr>
	 <td>EXPEDIENTE: </td>
			<td> <input type="text" disabled  name="expteAct" id="expteAct" value="<?php echo @$adj[0]->nro_expediente ?>">
	 
		<td>&nbsp;<button  id="btnCambioexpte"class="btn btn-default" onclick="cambioExpte();"><span class="glyphicon glyphicon-retweet"></span> Cambiar expediente</button></td>
	 <td> <input type="text"   name="expteNvo" id="expteNvo" value="<?php echo @$adj[0]->nro_expediente ?>">
	 <td> <button id="updateExpte" name="updateExpte"  class="btn btn-success" onclick="UpdateExpte();"  ><i class="fa fa-floppy-o"></i> Guardar Expediente</button></td>
	 </tr>
	 
	 
	 
	 
	 
	<tr>
	
      </tr>
    </table>
<br/><hr/><br/>
<input type="hidden" name="ultimo"  id="ultimo" />
<input type="hidden" name="nota"  id="nota" />
<input type="hidden" name="anio"  id="anio" />
 



<form   name="formulario2" id="formulario2" role="form">
 <center>
  <button type="button" class="btn btn-success"" onclick="Regresar()"><span class="glyphicon glyphicon-edit"></span> Volver</button> &nbsp;
  
   <button type="button" class="btn btn-danger"  onclick="Eliminar()"><span class="glyphicon glyphicon-remove"></span> Eliminar Adjudicacion</button>
</form>		
