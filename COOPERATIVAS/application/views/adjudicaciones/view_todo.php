<style type="text/css">
th, td { white-space: nowrap; }
    div.dataTables_wrapper {
        width: 100%;
        margin: 0 auto;
    }
</style>
<script type="text/javascript">

function Regresar(){
  if(confirm("Si Regresas se Perderan los Cambios")) { 
    window.location="<?php echo base_url()?>";
  }else{

    return false;
  }

}
</script>

<h1 class="page-header"><span class="glyphicon glyphicon-list-alt"></span> MODIFICACION DE ADJUDICACIONES</h1>
<div id="mensaje"></div>
	<p align="right">
 	   
 	 </p>
 	 <br/>
 	
	<table id="recibos" border="0" cellpadding="0" cellspacing="0" width="100%" class="pretty">
		<thead>
			<tr>

				<th></th>
				<th>Nombre</th>
				<th>Documento</th>
				<th>Programa</th>
				<th>Expediente</th>
				<th>Nota N°</th>
				<th>Año</th>
				<th>Cooperativa</th>
				<th>Matricula</th>
				<th>Localidad</th>
			<!--	<th>MARCA</th> -->
				
			</tr>
		</thead>
		<tbody>
			<?php
				if($datos){
					foreach($datos as $recibo){
					 if($recibo->Resolucion!= null){
						//$codigo       =  base64_encode($producto->tipo);
						$id           = base64_encode($recibo->Resolucion);

						echo '<tr>';
						echo '<td>';
						echo '<a href="../adjudicaciones/editar/'.$id.'"><button type="button" title="Editar Adjudicacion" class="btn btn-success btn-xs"><span class="glyphicon glyphicon-pencil"></span></button></a> &nbsp;';
						?>

						<?php
						echo '</td>';
						echo '<td>'.$recibo->nombre_beneficios.'</td>';
						echo '<td>'.$recibo->documento.'</td>';
						echo '<td>'.$recibo->nombre_programa.'</td>';
						echo '<td>'.$recibo->nro_expediente.'</td>';
						echo '<td>'.$recibo->nro_nota.'</td>';
						echo '<td>'.$recibo->anio.'</td>';
						echo '<td>'.$recibo->razonsocial.'</td>';
						echo '<td>'.$recibo->matricula.'</td>';
						echo '<td>'.$recibo->nombre_localidad.'</td>';
						echo '</tr>';
					}}
				}else{
					echo '<tr><td colspan=9><center>No Existen Adjudicaciones</center></td></tr>';
				}
			?>
		</tbody>
	</table>
	<br>
	<center>
	<button type="button" class="btn btn-success"" onclick="Regresar()"><span class="glyphicon glyphicon-edit"></span> Volver</button> </center>
<script type="text/javascript">

    $(document).ready(function() {
    $('#recibos').dataTable( {
        "scrollX": true
    } );
	function comprobar(){
    var tipo="<?php echo $this->session->userdata('TIPOUSUARIOMS');?>"
	if (tipo=="Adjudicaciones"||tipo=="Administrador")
	{}
   else 
   {
	  
	   window.location="<?php echo base_url()?>";
   }
  }
	comprobar();
} );

</script>