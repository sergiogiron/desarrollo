<style type="text/css">
th, td { white-space: nowrap; }
    div.dataTables_wrapper {
        width: 100%;
        margin: 0 auto;
    }
</style>
<h1 class="page-header"><span class="glyphicon glyphicon-list-alt"></span> Cooperativas Con Adjudicaciones</h1>
<div id="mensaje"></div>
	<p align="right">
 	 <a href="adjudicaciones/Nuevo">
 	 	<button type="button" class="btn btn-primary"><span class="fa fa-wrench"></span> Nueva Adjudicacion</button>
 	 </a>  
 	 </p>
 	 <br/>
 	
	<table id="recibos" border="0" cellpadding="0" cellspacing="0" width="100%" class="pretty">
		<thead>
			<tr>

				<th></th>
				<th>Nro</th>
				<th>Cooperativa</th>
				<th>Matricula</th>
				<th>Resolucion</th>
				<th>Adjudicaciones</th>
			<!--	<th>MARCA</th> -->
				
			</tr>
		</thead>
		<tbody>
			<?php
				if($recibos){
					foreach($recibos as $recibo){
					 if($recibo->numero!= null){
						//$codigo       =  base64_encode($producto->tipo);
						$id           = base64_encode($recibo->id_coperativa);

						echo '<tr>';
						echo '<td>';
						echo '<a href="adjudicaciones/listarPorCop/'.$id.'"><button type="button" title="Lista de Beneficiarios" class="btn btn-success btn-xs"><span class="glyphicon glyphicon-plus-sign"></span></button></a> &nbsp;';
						?>
					<!--	<a ><button onclick="EliminarRecibo('<?php echo $recibo->Cooperativa; ?>','<?php echo $id; ?>');" type="button" title="Eliminar Recibo" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span></button></a> -->
						<?php
						echo '</td>';
						echo '<td>'.$recibo->numero.'</td>';
						echo '<td>'.$recibo->Cooperativa.'</td>';
						echo '<td>'.$recibo->matricula.'</td>';
						echo '<td>'.$recibo->Resolucion.'</td>';
						echo '<td>'.$recibo->cantidad_bene.'</td>';
						echo '</tr>';
					}}
				}else{
					echo '<tr><td colspan=9><center>No Existe Informacion</center></td></tr>';
				}
			?>
		</tbody>
	</table>
<script type="text/javascript">

    $(document).ready(function() {
    $('#recibos').dataTable( {
        "scrollX": true
    } );
	function comprobar(){
    var tipo="<?php echo $this->session->userdata('TIPOUSUARIOMS');?>"
	if (tipo=="Adjudicaciones"||tipo=="Administrador")
	{}
   else 
   {
	  
	   window.location="<?php echo base_url()?>";
   }
  }
	comprobar();
} );

</script>