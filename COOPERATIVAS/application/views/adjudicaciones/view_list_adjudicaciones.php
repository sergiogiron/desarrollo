<style type="text/css">
th, td { white-space: nowrap; }
    div.dataTables_wrapper {
        width: 100%;
        margin: 0 auto;
    }
</style>

<script type="text/javascript">
function Regresar(){
   
    window.location="<?php echo base_url()?>adjudicaciones";
  
  }


</script>

<h1 class="page-header"><span class="glyphicon glyphicon-list-alt"></span> Lista de Beneficiarios</h1>

 	
	<table id="recibos" border="0" cellpadding="0" cellspacing="0" width="100%" class="pretty">
		<thead>
			<tr>

				<th></th>
				<th>Nombre</th>
				<th>Documento</th>
				<th>Programa</th>
                <th>Localidad</th>
				 <th>Barrio</th>
				  <th>Manzana</th>
				  <th>Lote</th>
			<th>Fecha Adjudicacion</th> 
			<th>Nota</th> 
			<th>a&ntilde;o</th> 
			
				
			</tr>
		</thead>
		<tbody>
			<?php
				if($lista){
					foreach($lista as $beneficio){
					 if($beneficio->resolucion!= null){
						//ID ES LA ID ADJ_MOVIL
						$id           = base64_encode($beneficio->resolucion);
						//ESTA VAR ES LA ID COOP
						$idCop=   base64_encode($beneficio->id_coperativa);
						

						echo '<tr>';
						echo '<td>';
						echo '<a href="../imprimir/'.$id.'/'.$idCop.'" target="_blank"><button type="button" title="Imprimir Resolucion" class="btn btn-success btn-xs"><span class="glyphicon glyphicon-print"></span></button></a> &nbsp;';
						?>
					<!--	<a ><button onclick="EliminarRecibo('<?php echo $recibo->Cooperativa; ?>','<?php echo $id; ?>');" type="button" title="Eliminar Recibo" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span></button></a> -->
						<?php
						echo '</td>';
						echo '<td>'.$beneficio->nombre_beneficios.'</td>';
						echo '<td>'.$beneficio->documento.'</td>';
						echo '<td>'.$beneficio->nombre_programa.'</td>';
						echo '<td>'.$beneficio->nombre_localidad.'</td>';
						echo '<td>'.$beneficio->barrio.'</td>';
						echo '<td>'.$beneficio->mza.'</td>';
						echo '<td>'.$beneficio->lote.'</td>';
						echo '<td>'.$beneficio->fecha_adjudicacion.'</td>';
						echo '<td>'.$beneficio->nro_nota.'</td>';
						echo '<td>'.$beneficio->anio.'</td>';
						echo '</tr>';
					}}
				}else{
					echo '<tr><td colspan=9><center>No Existe Informacion</center></td></tr>';
				}
			?>
		</tbody>
	</table>
	<br>
	<center> <button type="button" class="btn btn-success"" onclick="Regresar()"><span class="glyphicon glyphicon-edit"></span> Volver</button> &nbsp; </center>
<script type="text/javascript">

    $(document).ready(function() {
    $('#recibos').dataTable( {
        "scrollX": true
    } );
	function comprobar(){
    var tipo="<?php echo $this->session->userdata('TIPOUSUARIOMS');?>"
	if (tipo=="Adjudicaciones"||tipo=="Administrador")
	{}
   else 
   {
	   
	   window.location="<?php echo base_url()?>";
   }
  }
	comprobar();
} );

</script>