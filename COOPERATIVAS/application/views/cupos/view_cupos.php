<style type="text/css">
th, td { white-space: nowrap; }
    div.dataTables_wrapper {
        width: 100%;
        margin: 0 auto;
    }
</style>
<script type="text/javascript">
var baseurl = "<?php echo base_url(); ?>";
var currentLocation = window.location;
function EliminarCupo(nombre, id){
    confirmar=confirm("Eliminar a " + nombre + ", Recuerda Una vez Eliminado No podras Recuperarlo"); 

    if (confirmar){
    	document.getElementById('mensaje').innerHTML = "<div class='modal1'><div class='center1'> <center> <img src='"+ baseurl +"/img/gif-load.gif'> Eliminando Programa...</center></div></div>";
    	 var Cu 		 = new Object();
		Cu.Id      	 = id;
		Cu.Nombre      = nombre;
		var DatosJson = JSON.stringify(Cu);
		$.post(currentLocation + '/deletecupo',
		{ 
			ProgramaPost: DatosJson
		},
		function(data, textStatus) {
			//
			$("#mensaje").html(data.error_msg);
		}, 
		"json"		
		);
    } else{
    } 
  }
  
</script>
<h1 class="page-header"><span class="glyphicon glyphicon-list-alt"></span> Cupos</h1>
<div id="mensaje"></div>
<p align="right">
 	 <a href="cupos/nuevo">
 	 	<button type="button" class="btn btn-primary"><span class="glyphicon glyphicon-ok"></span> Nuevo Cupos</button>
 	 </a>  
 	 </p>
 	 <br/>
	<table id="cupos" border="0" cellpadding="0" cellspacing="0" width="100%" class="pretty">
		<thead>
			<tr>
				<th></th>
				<!--<th>Id</th>-->
				<th>Programa</th>
				<th>Localidad</th>
				<th>Cupos</th>
				
			</tr>
		</thead>
		<tbody>
			<?php
				if($cupos){
					foreach($cupos as $cupo){
						$idCupo     = base64_encode($cupo->id);
						echo '<tr>';
						echo '<td>';
						echo '<a href="cupos/editarCupo/'.$idCupo.'"><button type="button" title="Editar Cupo" class="btn btn-success btn-xs"><span class="glyphicon glyphicon-edit"></span></button></a> &nbsp;';
						?>
						<button type="button" onclick="EliminarCupo('<?php echo $cupo->id_programa; ?>','<?php echo $idCupo; ?>');" title="Eliminar Cupo" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span></button>
						<?php
						echo '</td>';
						//echo '<td>'.$cupo->id.'</td>';
						echo '<td>'.$cupo->nombre_programa.'</td>';
			            echo '<td>'.$cupo->nombre_localidad.'</td>';
						echo '<td>'.$cupo->cupo.'</td>';
						
					
						echo '</tr>';
					}
				}else{
					echo '<tr><td colspan=5><center>No Existe Informacion</center></td></tr>';
				}
			?>
		</tbody>
	</table>
<script type="text/javascript">

            $(document).ready(function() {
    $('#cupos').dataTable( {
        "scrollX": false
    } );
} );

</script>