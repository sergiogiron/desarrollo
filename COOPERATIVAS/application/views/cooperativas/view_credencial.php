<html>
<head>
<title>Credencial</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta name="GENERATOR" content="Quanta Plus KDE"> 
<style type="text/css">
.Imprime {
	
	font-family:Arial;
	font-size:16px;
}
@page{
   margin: 0;
}
</style>
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-15">
       
  <style>
  .left {
    float: left;
    border: 1px solid green;
    width: 3em;
  }
  
  .right {
    float: right;
    border: 1px solid blue;
    width: 3em;
  }
  
  .block {
    float: left;
    border: 1px solid red;
    width: 3em;
  }
  
  img {
    outline: 1px dotted red;
  }
  .Estilo4 {font-size: 21px; font-weight: bold; }
  .Estilo5 {font-size: 12px}
.Estilo7 {
	font-size: x-large;
	font-weight: bold;
}
  .Estilo8 {font-size: 10px}
  .Estilo9 {
	font-size: medium;
	color: #FFFFFF;
}
  .Estilo10 {font-size: x-large}
  .Estilo11 {font-size: large; }
  .Estilo12 {color: #0000FF}
  </style>

  
  <script type="text/javascript" src="<?php echo base_url()?>js/jquery-1.3.2.min.js"></script>    
  <script type="text/javascript" src="<?php echo base_url()?>js/jquery-barcode.js"></script> 
 
  
  
</head>

<?php
	
	$matricula   = "";
	$razonsocial = "";
	$fecha = "";
	$Actividad = "";
	$cuit = "";
	$domicilio = "";
	$telefono = "";
	$resolucion = "";

	$id        = "";
	foreach ($DetalleProd as $key => $value) {
		# code...
		$matricula         = $value->matricula;
		$razonsocial       = $value->razonsocial;
		$fecha             = $value->fecha;
	    $tipo              = $value->tipo;
	    $cuit              = $value->cuit;
	    $domicilio         = $value->domicilio;
	    $telefono          = $value->telefono;
	        $celular          = $value->celular;
		$resolucion        = $value->resolucion;	
		$id              = $value->id;
	}
	
 ?>

 <body onLoad="print();">
 <!-- <body>  -->
 <table width="500" border="1" style="border: 1px solid black; width: 730px; text-align: center;">
   <tr>
     <td width="210" valign="middle"><table width="100%" border="0">
       <tr>
         <td colspan="2"><div align="center"><img src="/UCEPPI/fotos/Logo.jpg" width="34" height="43" border="0"></div></td>
       </tr>
       <tr>
         <td colspan="2"><div align="center"><span class="Estilo7">UCEPPI</span></div></td>
       </tr>
       <tr>
         <td colspan="2"><div align="center"><span class="Estilo8">UNIDAD COORDINADORA EJECUTORA DE PLANES Y PROGRAMAS INTERMINISTERIALES</span> </div></td>
       </tr>
       <tr>
         <td colspan="2" bordercolor="#0000FF" bgcolor="#0000FF"><div align="center"><span class="Estilo9">CREDENCIAL DE COOPERATIVAS Y ORGANIZACIONES SOCIALES</span></div></td>
       </tr>
	   <tr>
                   <td valign="middle">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
				   <td valign="middle"><div align="center" id="bcTarget"></div></td>
                   
                   <script>
                         jQuery(function($){
	                     $("#bcTarget").barcode("<?php echo  $cuit; ?>", "codabar");
                     //   $("#bcTarget").barcode("1234567890", "code128");
                          });
                     </script>
        </tr>
       
     </table></td>
     <td width="200" valign="middle"><table width="100%" border="0">
         <tr>
           <td bordercolor="#0000CC" bgcolor="#000099"><div align="center" class="Estilo12"><span class="Estilo4">REGISTRO Nro. </span><span style="BACKGROUND-COLOR: #6f90b8"><?php echo $id ?></span></div></td>
         </tr>
         <tr>
           <td><div align="center" class="Estilo10"><span style="COLOR: #000000; BACKGROUND-COLOR: #6f90b8"><?php echo $razonsocial ?></span></div></td>
         </tr>
       </table>
         <table width="100%" border="0">
           <tr>
        <!--      <td width="33%"><img src="/UCEPPI/fotos/<?php echo $imgmostra;?>" style="float: left; clear: both; padding-left: 0.1em; padding-top: 0.1em;" width="120" border="1" /></td>
             <td width="67%"><table width="100%" border="0"> -->
		<?php	 
			  $Tipo="";		
		$i=$tipo;
        switch ($i) 
	{
          case 0:
                $Tipo= "---Elegir Opción---";
                break;
          case 1:
                $Tipo= "Agropecuario";
                break;
          case 2:
                $Tipo= "Apicola";
                break;
	      case 3:
                $Tipo= "Consumo";
                break;
          case 4:
                $Tipo= "Credito";
                break;
          case 5:
                $Tipo= "Trabajo";
                break;
		  case 6:
                $Tipo= "Servicio Publico";
                break;
          case 7:
                $Tipo= "Vivienda";
                break;
          case 8:
                $Tipo= "Seguros";
                break;
	      case 9:
                $Tipo= "Granjero o Avicola";
                break;
          case 10:
                $Tipo= "Frutihorticola";
                break;
          case 11:
                $Tipo= "Tamberas";
                break;
		  case 12:
                $Tipo= "Mineras";
                break;
          case 13:
                $Tipo= "Vitivinicolas";
                break;
	      case 14:
                $Tipo= "Exportacion";
                break;
          case 15:
                $Tipo= "Forestales";
                break;
          case 16:
                $Tipo= "Pesqueras";
                break;
		  case 17:
                $Tipo= "Carniceras";
                break;
          case 18:
                $Tipo= "Ganaderas";
                break;
	      case 19:
                $Tipo= "Provision";
                break;
          case 20:
                $Tipo= "Otros";
                break;
 
    };
			 
			 
			?> 
           <tr>
		    
                 <td><div align="left" class="Estilo11">Actividad : <?php echo $Tipo ?></div></td> 
           </tr>
           <tr>
                 <td><div align="left" class="Estilo5">Resoluci&oacute;n : <?php echo $resolucion ?></div></td>
           </tr>
           <tr>
                 <td><div align="left" class="Estilo5">Matricula : <?php echo $matricula ?></div></td>
           </tr>
            <tr>
                   <td><span class="Estilo5">Cuit: <?php echo $cuit ?></span></td>
            </tr>
            <tr>
                   <td><span class="Estilo5">Domicilio: <?php echo $domicilio ?></span></td>
            </tr>
                 
				 <tr>
                   <td><span class="Estilo5">Tel&eacute;fono: <?php echo $celular   ?></span></td>
            </tr> 
				 
				 
				 
                 <tr> </tr>
       </table></td>
           </tr>
         </table>
 </table> 
 </table>
</body>

</html>
