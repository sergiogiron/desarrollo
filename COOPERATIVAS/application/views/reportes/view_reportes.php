<script type="text/javascript">
  var baseurl = "<?php echo base_url(); ?>";
</script>
<script src="<?php echo base_url()?>js/jquery-1.10.2.js"></script>
<script src="<?php echo base_url()?>js/jquery-ui.js"></script>
<script src="<?php echo base_url()?>js/JsReportes.js"></script>
<h1 class="page-header" name="primero" id="primero"><span class="glyphicon glyphicon-list"></span> Reportes Por Ciudad</h1>
<div id="mensaje"></div>

<?php $fecha2 = date('Y-m-d H:i:s')?>
<?php 
function obtenerFechaEnLetra($fecha2){
    $dia= conocerDiaSemanaFecha($fecha2);
    $num = date("j", strtotime($fecha2));
    $anno = date("Y", strtotime($fecha2));
    $mes = array('enero', 'febrero', 'marzo', 'abril', 'mayo', 'junio', 'julio', 'agosto', 'septiembre', 'octubre', 'noviembre', 'diciembre');
    $mes = $mes[(date('m', strtotime($fecha2))*1)-1];
    return $dia.', '.$num.' de '.$mes.' del '.$anno;
}
 //funcion que obtiene dia de la semana
function conocerDiaSemanaFecha($fecha2) {
    $dias = array('Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado');
    $dia = $dias[date('w', strtotime($fecha2))];
    return $dia;
}	?>		
<hr/><br/> 
<div id="cabecera" name="cabecera" > <center><img src="/UCEPPI/fotos/Logo.jpg" width="100" height="110" border="0"></center>
  <center><p>UNIDAD COORDINADORA Y  EJECUTORA DE PLANES Y PROGRAMAS
INTERMINISTERIALES</p>

<p>LOS DATOS QUE SE INDICAN A CONTINUACION REPRESENTAN LOS VALORES ALMACENADOS EN EL SISTEMA EL DIA  <?php echo obtenerFechaEnLetra($fecha2); echo " Hora :";     echo date("H:i:s") ?>  </p> 
 </center>
<br>
</div>
<table>
<tr>
<td name="loc" id="loc">Localidad: </td>
<td><select name="localidad" id="localidad"  class="form-control"></select></td>


<td>

</td>
<td>
      &nbsp;<button  onclick ="generar()"type="submit"   id="GeneraReporte" class="btn btn-primary"><i class="fa fa-align-left"></i> Generar Reporte</button> 
      
    </td>
	<td>
     <button  onclick ="imprimir();window.location='<?php echo base_url(); ?>'; "   id="Imprimir" class="btn btn-primary"><i class="	fa fa-file-text-o"></i> Imprimir Reporte
      
    </td>
</tr>
</table>

<hr>
 

<div id="resultados" name="resultados"> </div >

      <div id="resumen" name="resumen"> </div >
	  
	  <div id="foter" name="foter"> <center> <p>Avda. Bolivia Nº 246 (frente del RIM 20) Tel. 4315040 – uceinterministerial@gmail.com</p></center></div>
                