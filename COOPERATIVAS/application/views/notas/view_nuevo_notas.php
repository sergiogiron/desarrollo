<input type="hidden" id="id" name="id" value="<?php echo @$nota[0]->id_nota; ?>" >
<!--<input type="hidden" id="coope"name="coope" value="<?php echo @$denuncia[0]->id_cooperativa; ?>" >-->
 
<script type="text/javascript">

  var baseurl = "<?php echo base_url(); ?>";

  function regresar(){
    window.location="<?php echo base_url()?>notas";
  }

</script>

<?php
if($titulo =="Nueva Nota"){
 //NRO CODIGO nuevo
  $Codigo = array(
  'name'        => 'codigo',
  'id'          => 'codigo',
  'size'        => 50,
  'value'       => set_value('codigo',@$max[0]->max) + 1,
  'type'        => 'text',
  'class'       => 'form-control',
  );
  $Año       = array(
  'name'        => 'anio',
  'id'          => 'anio',
  'size'        => 50,
  'value'       => set_value('anio',date('Y')), //formato para mostrar el año actual
  'type'        => 'text',
  'class'       => 'form-control',
   );
      }else{ 
          $Codigo       = array(
          'name'        => 'codigo',
          'id'          => 'codigo',
          'size'        => 50,
          'value'       => set_value('codigo',@$nota[0]->codigo),
          'type'        => 'text',
          'class'       => 'form-control',
          );
          $Año       = array(
          'name'        => 'anio',
          'id'          => 'anio',
          'size'        => 50,
          'value'       => set_value('anio',@$nota[0]->anio), //Al editar, toma el año de la bd
          'type'        => 'text',
          'class'       => 'form-control',
           );
      }
      
  
  $Fecha      = array(
  'name'        => 'fecha',
  'id'          => 'fecha',
  'size'        => 50,
  'value'       => set_value('fecha',@$nota[0]->fecha_nota),
  'type'        => 'date',
  'class'       => 'form-control',
    );
 $Remitente   = array(
  'name'        => 'remitente',
  'id'          => 'remitente',
  'size'        => 50,
  'value'       => set_value('remitente',@$nota[0]->remitente),
  'type'        => 'text',
  'class'       => 'form-control',
    );
 $Celular     = array(
  'name'        => 'celular',
  'id'          => 'celular',
  'size'        => 50,
  'value'       => set_value('celular',@$nota[0]->celular),
  'type'        => 'text',
  'class'       => 'form-control',
  );
  $Motivo     = array(
  'name'        => 'motivo',
  'id'          => 'motivo',
  'size'        => 50,
  'value'       => set_value('motivo',@$nota[0]->motivo),
  'type'        => 'text',
  'class'       => 'form-control',
   );
  $Tipo  = array(
  '0'             => '---Elegir Opción---',
  '1'             => 'Cooperativa',
  '2'             => 'Planes',
  '3'             => 'Particular',
  '4'             => 'Gobierno',
  '5'             => 'Municipalidad',
  '6'             => 'Otros',
  );
  $Estado     = array(
  '0'             => '---Elegir Opción---',
  '1'             => 'Recibido',
  '2'             => 'Visado',
  '3'             => 'Historico',
  '4'             => 'Otros',
    );
  $Observaciones  = array(
  'name'             => 'observaciones',
  'id'               => 'observaciones',
  'size'             => 50,
  'value'            => set_value('observaciones',@$nota[0]->obs),
  'type'             =>'text',
  'class'            =>'form-control',
  );
?>

<script src="<?php echo base_url();?>js/JsonNotas.js"></script>
<h1 class="page-header"><span class="glyphicon glyphicon-th-list"></span> <?php echo $titulo; ?></h1>
<div id="mensaje"></div>
<form class="form-horizontal" name="formulario" id="formulario" role="form">

<div class="form-group">
    <label for="codigo" class="col-lg-3 control-label">Codigo:</label>
      <div class="col-lg-3">
        <?php echo form_input($Codigo); ?>
      </div>
</div>
<div class="form-group">
  <label for="anio" class="col-lg-3 control-label">Año:</label>
      <div class="col-lg-3">
        <?php echo form_input($Año); ?>
        <!--<input type="fecha" name="fecha" value="<?php echo $Año; ?>"-->
      </div>
</div>
<div class="form-group">
  <label for="fecha" class="col-lg-3 control-label">Fecha:</label>
      <div class="col-lg-3">
        <?php echo form_input($Fecha); ?>
      </div>
</div>
<div class="form-group">
  <label for="remitente" class="col-lg-3 control-label">Remitente:</label>
      <div class="col-lg-3">
        <?php echo form_input($Remitente); ?>
      </div>
</div>
<div class="form-group">
  <label for="celular" class="col-lg-3 control-label">Celular:</label>
      <div class="col-lg-3">
        <?php echo form_input($Celular); ?>
      </div>
</div>
<div class="form-group">
  <label for="motivo" class="col-lg-3 control-label">Motivo:</label>
    <div class="col-lg-3">
      <?php echo form_textarea($Motivo); ?>
    </div>
</div>
<div class="form-group">
  <label for="tipo" class="col-lg-3 control-label">Tipo:</label>
    <div class="col-lg-3">
     <?php echo  form_dropdown('tipo', $Tipo, set_value('tipo',@$nota[0]->tipo),'class="form-control" id="tipo"'); ?>
    </div>
</div>
<div class="form-group">
  <label for="estado" class="col-lg-3 control-label">Estado:</label>
    <div class="col-lg-3">
     <?php echo  form_dropdown('estado', $Estado, set_value('estado',@$nota[0]->estado),'class="form-control" id="estado"'); ?>
    </div>
</div>
<div class="form-group">
      <label for="observaciones" class="col-lg-3 control-label">Observaciones:</label>
      <div class="col-lg-3">
        <?php echo form_input($Observaciones); ?>
      </div>
  </div>  
  <div class="form-group">
    <div class="col-lg-offset-3 col-lg-10">
      <button type="button" onclick="regresar()" class="btn btn-primary"><span class="glyphicon glyphicon-circle-arrow-left"></span> Regresar</button>
      <button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-saved"></span> Guardar </button>
      <?php if($titulo=="Nueva Nota"){ ?>
      <button type="reset" class="btn btn-primary"><span class="glyphicon glyphicon-plus"></span> Nuevo</button>
      <?php } ?>
    </div>
  </div>
  <hr/>
</form>		
<script type="text/javascript">
  
</script>
