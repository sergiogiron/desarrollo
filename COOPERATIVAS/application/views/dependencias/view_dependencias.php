<?php
date_default_timezone_set('America/Argentina/Jujuy');
?>


<style type="text/css">
th, td { white-space: nowrap; }
    div.dataTables_wrapper {
        width: 100%;
        margin: 0 auto;
    }
</style>
<script type="text/javascript">
var baseurl = "<?php echo base_url(); ?>";
var currentLocation = window.location;

function eliminarDependencia(dep, id){
    confirmar=confirm("Realmente desea eliminar Dependencia código:" + dep + "? Una vez eliminado NO podrá ser recuperado"); 

    if (confirmar){
    	 document.getElementById('mensaje').innerHTML = "<div class='modal1'><div class='center1'> <center> <img src='"+ baseurl +"/img/gif-load.gif'> Eliminando Dependencia...</center></div></div>";
    	 var Dep		 = new Object();
		 Dep.Id      	 = id;
		 Dep.Nombre       = dep;
		
		var DatosJson = JSON.stringify(Dep);
		$.post(currentLocation + '/deleteDependencia', //Funcion del controlador
		{ 
			DepPost: DatosJson
		},
		function(data, textStatus) {
			//
			$("#mensaje").html(data.error_msg);
		}, 
		"json"		
		);
    } else{
    } 
  } // viste  ahoa funciona pero no muestra Yes! modelo?
  
</script>
<h1 class="page-header"><span class="glyphicon glyphicon-list-alt"></span> Dependencia</h1>
<div id="mensaje"></div>
<p align="right">
 	 <a href="dependencias/nuevo">
 	 	<button type="button" class="btn btn-primary"><span class="glyphicon glyphicon-ok"></span> Nueva Dependencia</button>
 	 </a>  
 	 </p>
 	 <br/>
	<table id="dep" border="0" cellpadding="0" cellspacing="0" width="100%" class="pretty">
		<thead>
			<tr>
				<th></th>
				<th>Codigo</th>
				<th>Nombre Dependencia</th>
				<th>Observaciones</th>
			</tr>
		</thead>
		<tbody>
			<?php
				if(isset($dep)){
					foreach($dep as $Dep){
						$idDep    = base64_encode($Dep->id_dep);
						echo '<tr>';
						echo '<td>';
						echo '<a href="dependencias/editarDependencia/'.$idDep.'"><button type="button" title="Editar Dependencia" class="btn btn-success btn-xs"><span class="glyphicon glyphicon-edit"></span></button></a> &nbsp;';
						?>
						<button type="button" onclick="eliminarDependencia('<?php echo $Dep->codigo_dep; ?>','<?php echo $idDep; ?>');" title="Eliminar Dependencia" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span></button>
						<?php
						echo '</td>';
						echo '<td>'.str_pad((int) $Dep->codigo_dep,4,"0",STR_PAD_LEFT).'</td>'; //Doy formato 0000 al codigo_adep
						echo '<td>'.$Dep->nombre_dep.'</td>';
						echo '<td>'.$Dep->obs_dep.'</td>';
						echo '</tr>';
					}
				}else{
					echo '<tr><td colspan=5><center>No Existe Informacion</center></td></tr>';
				}
			?>
		</tbody>
	</table>
<script type="text/javascript">

            $(document).ready(function() {
    $('#dep').dataTable( {
        "scrollX": false
    } );
} );

</script>