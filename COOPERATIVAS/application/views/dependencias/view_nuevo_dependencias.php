<input type="hidden" id="id" name="id" value="<?php echo @$dep[0]->id_dep; ?>" >
<!--<input type="hidden" id="coope"name="coope" value="<?php echo @$denuncia[0]->id_cooperativa; ?>" >-->
 
<script type="text/javascript">

  var baseurl = "<?php echo base_url(); ?>";

  function regresar(){
    window.location="<?php echo base_url()?>dependencias";
  }

</script>

<?php
if($titulo =="Nueva Dependencia"){
 //NRO CODIGO nuevo
  $Codigo = array(
  'name'        => 'codigo',
  'id'          => 'codigo',
  'size'        => 50,
  'value'       => set_value('codigo',@$max[0]->max) + 1,
  'type'        => 'text',
  'class'       => 'form-control',
  );
  
      }else{ 
          $Codigo = array(
          'name'        => 'codigo',
          'id'          => 'codigo',
          'size'        => 50,
          'value'       => set_value('codigo',@$dep[0]->codigo_dep),//Al editar, toma el codigo guardado
          'type'        => 'text',
          'class'       => 'form-control',
          );
      }


 $Nombre   = array(
  'name'        => 'nombre',
  'id'          => 'nombre',
  'size'        => 50,
  'value'       => set_value('nombre',@$dep[0]->nombre_dep),
  'type'        => 'text',
  'class'       => 'form-control',
  'onkeypress'  => 'return validarn(event);', // Sólo permite el ingreso de caracteres

  );

  $Observaciones  = array(
  'name'             => 'observaciones',
  'id'               => 'observaciones',
  'size'             => 50,
  'value'            => set_value('observaciones',@$dep[0]->obs_dep),
  'type'             =>'text',
  'class'            =>'form-control',

  );



?>

<script src="<?php echo base_url();?>js/JsonDependencias.js"></script>
<h1 class="page-header"><span class="glyphicon glyphicon-th-list"></span> <?php echo $titulo; ?></h1>
<div id="mensaje"></div>

<form class="form-horizontal" name="formulario" id="formulario" role="form">

<div class="form-group">
    <label for="codigo" class="col-lg-3 control-label">Codigo:</label>
      <div class="col-lg-3">
        <?php echo form_input($Codigo); ?>
      </div>
</div>
<div class="form-group">
  <label for="nombre" class="col-lg-3 control-label">Nombre Dependencia:</label>
      <div class="col-lg-3">
        <?php echo form_input($Nombre); ?>
      </div>
</div>
<div class="form-group">
      <label for="observaciones" class="col-lg-3 control-label">Observaciones:</label>
      <div class="col-lg-3">
        <?php echo form_input($Observaciones); ?>
      </div>
</div>  
  <div class="form-group">
    <div class="col-lg-offset-3 col-lg-10">
      <button type="button" onclick="regresar()" class="btn btn-primary"><span class="glyphicon glyphicon-circle-arrow-left"></span> Regresar</button>
      <button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-saved"></span> Guardar </button>
      <?php if($titulo=="Nueva Dependencia"){ ?>
      <button type="reset" class="btn btn-primary"><span class="glyphicon glyphicon-plus"></span> Nuevo</button>
      <?php } ?>
    </div>
  </div>
  <hr/>
</form>		
<script type="text/javascript">
  
</script>