<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
date_default_timezone_set('America/Argentina/Jujuy');
class areas extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('seguridad_model');
		$this->load->model('areas_model');
		$this->load->model('bitacoras_model');
	}
	public function index(){
	       //Bitácora
		   $RegistrBitacoras = array(
		                'fecha'             => date('Y-m-d H:i:s'),
						'usuario'     		=> $this->session->userdata('ID'),
						'mensaje'	    	=> "Ingresó a Areas" );
			$this->bitacoras_model->AgregarBitacoras($RegistrBitacoras);
			//-//
	
          $url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
          $this->seguridad_model->SessionActivo($url);
          /**/
          $this->load->view('constant');
          $this->load->view('view_header');
          $data['areas'] = $this->areas_model->ListarAreas();
          $this->load->view('areas/view_areas', $data);
          $this->load->view('view_footer');
          
	}
     public function nuevo(){
          $url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
          $this->seguridad_model->SessionActivo($url);
          $this->load->view('constant');
          $this->load->view('view_header');
          $data['titulo'] = "Nueva Area";
		  $data["max"] =  $this->areas_model->maxCodigo();
		  $this->load->view('areas/view_nuevo_areas', $data);
          $this->load->view('view_footer');
     }
     public function editarArea($id){
          $url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
          $this->seguridad_model->SessionActivo($url);
          $id =  base64_decode($id);
          $this->load->view('constant');
          $this->load->view('view_header');
          $data['area'] = $this->areas_model->BuscaAreas($id);
          $data['titulo'] = "Editar Area";
          $this->load->view('areas/view_nuevo_areas', $data);
          $this->load->view('view_footer');
     }
     public function SaveArea(){
	 
	  
	 
	      $url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
          $this->seguridad_model->SessionActivo($url);
          $Areas           = json_decode($this->input->post('AreasPost'));
		  
		   //Bitácora
						 
		                 $RegistrBitacoras = array(
		                'fecha'             => date('Y-m-d H:i:s'),
						'usuario'     		=> $this->session->userdata('ID'),
						'mensaje'	    	=> "Nuevo Area : ");	

                         $this->bitacoras_model->AgregarBitacoras($RegistrBitacoras);
			             //-//				
		  
		  
		  
		  
          $response = array (
                    "estatus"   => false,
                    "campo"     => "",
                    "error_msg" => ""
                             );
            if($Areas->codigo==""){
               $response["campo"]     = "codigo";
               $response["error_msg"]   = "<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>El Codigo es Obligatorio</div>";
               echo json_encode($response);
          }else if($Areas->nombre==""){
               $response["campo"]     = "nombre";
               $response["error_msg"]   = "<div class='alert alert-danger text-center' alert-dismissable><button type='button' class='close' data-dismiss='alert'>&times;</button>La Descripcion es obligatorio</div>";
               echo json_encode($response);
         
          
          }else{
               if($Areas->Id==""){
			         
                     $RegistraArea    = array(
                     'codigo_area'        => $Areas->codigo,
                     'nombre_area'        => $Areas->nombre,
                     'obs_area'            => $Areas->obs,
					
               //      'fecha'              => date('Y-m-j H:i:s'),
					 
                     );
					  //Bitácora
						 $CADENA=print_r($RegistraArea,true);
		                 $RegistrBitacoras = array(
		                'fecha'             => date('Y-m-d H:i:s'),
						'usuario'     		=> $this->session->userdata('ID'),
						'mensaje'	    	=> "Nuevo Area : $CADENA");	

                         $this->bitacoras_model->AgregarBitacoras($RegistrBitacoras);
			             //-//				 
                     $this->areas_model->SaveAreas($RegistraArea);
                     $response["error_msg"]   = "<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Informacion Guardada Correctamente</div>";
                     echo json_encode($response);
               }
               if($Areas->Id!=""){
                 	  $UpdateArea    = array(
                    
                     'codigo_area'        => $Areas->codigo,
                     'nombre_area'        => $Areas->nombre,
                     'obs_area'            => $Areas->obs

					
               //      'fecha'              => date('Y-m-j H:i:s'),
					 
                     );
					 
					 
					 
					 
			   
			          //Bitácora
						 $CADENA=print_r($UpdateArea,true);
		                 $RegistrBitacoras = array(
		                'fecha'             => date('Y-m-d H:i:s'),
						'usuario'     		=> $this->session->userdata('ID'),
						'mensaje'	    	=> "Modificar Area : $CADENA");	

                         $this->bitacoras_model->AgregarBitacoras($RegistrBitacoras);
			             //-//		   
			   
                    $this->areas_model->UpdateAreas($UpdateArea, $Areas->Id);
                    $response["error_msg"]   = "<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button> Informacion Actualizada Correctamente</div>";
                    echo json_encode($response);
               }
          } 
     }
	 
	 
	 public function deletearea(){
		$url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
          $this->seguridad_model->SessionActivo($url);
		$Areas 		= json_decode($this->input->post('AreasPost'));
		$id             = base64_decode($Areas->Id);
		 //Bitácora
		
		 $RegistrBitacoras = array(
		 'fecha'             => date('Y-m-d H:i:s'),
		 'usuario'     		=> $this->session->userdata('ID'),
		 'mensaje'	    	=> "Se borro Areas : $id");	
         $this->bitacoras_model->AgregarBitacoras($RegistrBitacoras);
			             //-//		
		//$codigo 		= base64_decode($Tarifas->codigo);
		/*Array de response*/
		 $response = array (
				"estatus"   => false,
	            "error_msg" => ""
	    );
		 $this->areas_model->EliminarArea($id);
		 $response["error_msg"]   = "<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Cuota Eliminado Correctamente Clave: <strong>".$id."</strong>, La Información de Actualizara en 5 Segundos <meta http-equiv='refresh' content='5'></div>";
		 echo json_encode($response);
	}
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
}