<?php if ( !defined('BASEPATH')) exit('No direct script access allowed');
class instituciones extends CI_Controller {
	public function __construct()
	{ parent::__construct();
	 $this->load->database(); 
	 $this->load->model('seguridad_model');
		$this->load->model('adjudicaciones_model');
		$this->load->model('bitacoras_model');
		$this->load->helper('date');
	 $this->load->library('grocery_CRUD');
	 }
	
	public function index(){
		$RegistrBitacoras = array(
		                'fecha'             => date('Y-m-d H:i:s'),
						'usuario'     		=> $this->session->userdata('ID'),
						'mensaje'	    	=> "ingreso a instituciones" );
				   
			$this->bitacoras_model->AgregarBitacoras($RegistrBitacoras);
	 	$this->grocery_crud = new grocery_CRUD();

 $this->grocery_crud->set_language('spanish');
$this->grocery_crud->set_theme('flexigrid');

$this->grocery_crud->set_table("beneficios");
//$this->grocery_crud->set_subject('institucion');
$this->grocery_crud->where('institucion',2);
$this->grocery_crud->required_fields('nombre_beneficios','direccion','barrio','id_localidad','estado','id_programa');  
$this->grocery_crud->columns('nombre_beneficios','direccion','barrio','id_localidad','estado','id_programa');
$this->grocery_crud->display_as('nombre_beneficios','INSTITUCION');
$this->grocery_crud->display_as('id_localidad','localidad');
$this->grocery_crud->set_relation('id_localidad','localidades','nombre_localidad');
$this->grocery_crud->set_relation('id_programa','programas','nombre_programa');
$this->grocery_crud->field_type('mza', 'hidden', "");
$this->grocery_crud->field_type('lote', 'hidden', "");
$this->grocery_crud->field_type('documento', 'hidden', "institucion");
$this->grocery_crud->field_type('lat', 'hidden', "");
$this->grocery_crud->field_type('longi', 'hidden', "");
$this->grocery_crud->field_type('estado','dropdown',
array('1' => 'inscripcion', '2' => 'habilitado','3' => 'adjudicado' , '4' => 'pendiente', '5' => 'observado'));
$this->grocery_crud->field_type('institucion','dropdown',array('2' => 'institucion'));
//$this->grocery_crud->required_fields('nombre', 'id_localidad');
// seteo de relaciones $this->grocery_crud->set_relation('id_localidad','localidades','nombre_localidad');
$output = $this->grocery_crud->render();
	$this->load->view('constant');
          $this->load->view('view_header');
          $this->load->view('view_footer');
$this->load->view("instituciones/listar",$output); 
 


}

	}
