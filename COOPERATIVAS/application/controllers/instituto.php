<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
date_default_timezone_set('America/Argentina/Jujuy');
class instituto extends CI_Controller {
  function __construct(){
    parent::__construct();
    $this->load->model('instituto_model');
    $this->load->model('seguridad_model');
    $this->load->model('adjudicaciones_model');
    $this->load->model('cooperativas_model');
    $this->load->helper('date');
    $this->load->model('bitacoras_model');
    }
      
      public function index(){
            //Bitácora
            $RegistrarBitacora = array(
                'fecha'           => date('Y-m-d H:i:s'),
                'usuario'         => $this->session->userdata('ID'),
                'mensaje'         => "Ingresó a pantalla IVUJ" );
           $this->bitacoras_model->AgregarBitacoras($RegistrarBitacora);
          
              $url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
              $this->seguridad_model->SessionActivo($url);
              $this->load->view('constant');    //Carga datos constantes
              $this->load->view('view_header'); //Carga datos de encabezado
              $this->load->view('instituto/view_instituto');
              $this->load->view('view_footer'); 
           }

      public function buscarNotaAdj(){
            //Bitácora
            $RegistrarBitacora = array(
                'fecha'           => date('Y-m-d H:i:s'),
                'usuario'         => $this->session->userdata('ID'),
                'mensaje'         => "Buscó un Nro. de Nota" );
           $this->bitacoras_model->AgregarBitacoras($RegistrarBitacora);

              $url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
              $this->seguridad_model->SessionActivo($url);
              $this->load->view('constant');
              $this->load->view('view_header');
              $filtro2   = $this->input->get("NumeroNota");
              $data["titulo"]="";

             if($filtro2!=""){

                 $notaResult= $this->instituto_model->buscarNota($filtro2);

                   if (empty($notaResult)!=true){
                      $data["Nota"]=$notaResult;
                      $data["Bene"] = $this->adjudicaciones_model->TraerBeneficiario($notaResult[0]->Resolucion);
                      $data["Coope"] = $this->adjudicaciones_model->TraerCooperativa($notaResult[0]->id_coperativa);
                
                    }else{
                      $data["Nota"]=" No se han encontrado datos relacionados con el Nro. ingresado...";
                    } 
              }else{
                  $data["Nota"]=" No se han encontrado datos relacionados con el Nro. ingresado...";
              }
              $this->load->view('instituto/view_instituto', $data);
              $this->load->view('view_footer');
           
         }

      public function saveExpediente(){
          //Bitácora
            $RegistrarBitacora = array(
                'fecha'           => date('Y-m-d H:i:s'),
                'usuario'         => $this->session->userdata('ID'),
                'mensaje'         => "Ingresó Nro. de expediente" );
           $this->bitacoras_model->AgregarBitacoras($RegistrarBitacora);

          $url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
          $this->seguridad_model->SessionActivo($url);
          $Viviendas  = json_decode($this->input->post('InstitutoNotaPost'));
          $response = array (
                    "estatus"   => false,
                    "campo"     => "",
                 "error_msg" => ""
          );
          if($Viviendas->Resolucion==""){
            $response["campo"]       = "Resolucion";
            $response["error_msg"]   = "<div class='alert alert-danger text-center' alert-dismissable><button type='button' class='close' data-dismiss='alert'>&times;</button>Nro. de Resolucion es Obligatorio</div>";
            echo json_encode($response);
          }else if($Viviendas->NumeroExpediente==""){
            $response["campo"]       = "NumeroExpediente";
            $response["error_msg"]   = "<div class='alert alert-danger text-center' alert-dismissable><button type='button' class='close' data-dismiss='alert'>&times;</button>Debe completar Nro. de Expediente...</div>";
            echo json_encode($response); 

          }else if($Viviendas->Resolucion!=""){
       
            $RegistraCarga=array(
         
                 'nro_expediente' =>  $Viviendas->NumeroExpediente
             
            );
            //Bitácora
          $RegistrarBitacora = array(
                'fecha'           => date('Y-m-d H:i:s'),
                'usuario'         => $this->session->userdata('ID'),
                'mensaje'         => "Editó Nro. de Expediente" );
          $this->bitacoras_model->AgregarBitacoras($RegistrarBitacora);

          $this->instituto_model->updateCarga($RegistraCarga, $Viviendas->Resolucion);
          $response["error_msg"]   = "<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button> Informacion Guardada Correctamente</div>";
                    echo json_encode($response);
        }
 


    }
  }

