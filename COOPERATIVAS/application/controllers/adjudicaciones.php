<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
date_default_timezone_set('America/Argentina/Jujuy');
class adjudicaciones extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('seguridad_model');
		$this->load->model('adjudicaciones_model');
		$this->load->model('bitacoras_model');
		$this->load->helper('date');
	}
	
	//comentario
	public function index(){
	        //Bitácora
		   $RegistrBitacoras = array(
		                'fecha'             => date('Y-m-d H:i:s'),
						'usuario'     		=> $this->session->userdata('ID'),
						'mensaje'	    	=> "Ingresó a Adjudicaciones" );
			$this->bitacoras_model->AgregarBitacoras($RegistrBitacoras);
			//-//
	
	
	
          $url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
          $this->seguridad_model->SessionActivo($url);
          /**/
          $this->load->view('constant');
          $this->load->view('view_header');
          $data['recibos'] = $this->adjudicaciones_model->ListarAdjudicaciones();
          $this->load->view('adjudicaciones/view_adjudicaciones', $data);
          $this->load->view('view_footer');
          
	}
	/**
      * busca coperativas segun un termino y cuidad
      * @return return objeto json  
      */
	public function BuscarCooperativa(){
		$filtro=$_POST['term'];
		$filtro2= $_POST['Ciudad'];
		$bandera =$_POST['bandera'];
		// $filtro    = $this->input->get("term");
		// $filtro2= $this->input->get("Ciudad");
	    // $bandera =$this->input->get("bandera");
		$clientes = $this->adjudicaciones_model->buscarCooperativa($filtro,$filtro2,$bandera);
		echo json_encode($clientes);
	
	}	 
		 /**
      * filtra beneficiarios segun un termino y cuidad
      * @return objeto json 
      */
		 public function listarBeneficiarios(){
			 $filtro=$_POST['term'];
			  $filtro2=$_POST['Ciudad'];
			  $programa=$_POST['programa'];
			 
		/* $filtro    = $this->input->get("term");
		$filtro2= $this->input->get("Ciudad");
		$programa= $this->input->get("programa"); */
		$productos = $this->adjudicaciones_model->listarBeneficiarios($filtro,$filtro2,$programa);
		echo json_encode($productos);
		 }


		  public function listarBeneficiarios2(){
			 $filtro=$_POST['term'];
			  $filtro2=$_POST['Ciudad'];
			  $programa=$_POST['programa'];
			 
		/* $filtro    = $this->input->get("term");
		$filtro2= $this->input->get("Ciudad");
		$programa= $this->input->get("programa"); */
		$productos = $this->adjudicaciones_model->listarBeneficiarios2($filtro,$filtro2,$programa);
		echo json_encode($productos);
		 }
		 
		 
		  public function listarPorCop($numero){
			$url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
			$numero = base64_decode($numero);
			$data["titulo"]    = "Listado Por Cooperativa";
		$this->load->view('constant');
		$this->load->view('view_header',$data);
          $this->seguridad_model->SessionActivo($url);
		  $data["lista"] = $this->adjudicaciones_model->listarPorCop($numero);
		 $this->load->view('adjudicaciones/view_list_adjudicaciones', $data);
          $this->load->view('view_footer');
		$RegistrBitacoras = array(
		                'fecha'             => date('Y-m-d H:i:s'),
						'usuario'     		=> $this->session->userdata('ID'),
						'mensaje'	    	=> "ingreso a listad x cooperativas");
				   
			$this->bitacoras_model->AgregarBitacoras($RegistrBitacoras);
		 }
	
public function imprimir($id,$idCop)
{
	$url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
			$id = base64_decode($id);
			$idCop= base64_decode($idCop);
			$data["titulo"]    = "Impresion";
	        $this->load->view('constant');
			 $data["Bene"] = $this->adjudicaciones_model->TraerBeneficiario($id);
			 $data["Coope"] = $this->adjudicaciones_model->TraerCooperativa($idCop);
			$this->load->view('adjudicaciones/view_resolucion_adjudicaciones', $data);
         // $this->load->view('view_footer');
			
	//echo json_encode($productos);
	
	 //Bitácora
		   $RegistrBitacoras = array(
		                'fecha'             => date('Y-m-d H:i:s'),
						'usuario'     		=> $this->session->userdata('ID'),
						'mensaje'	    	=> "Se imprimio  adjudicacion id : $id" );
				   
			$this->bitacoras_model->AgregarBitacoras($RegistrBitacoras);
			//-//
	
}	
		 
		 public function localidades(){
		$local = $this->adjudicaciones_model->Localidades();
		echo json_encode($local);
	}
	public function programas(){
		$local = $this->adjudicaciones_model->Programas();
		echo json_encode($local);
	}
	public function ListarAdjudicaciones(){
		$local = $this->adjudicaciones_model->ListarAdjudicaciones();
		echo json_encode($local);
	}
	
	public function Adjudicacion()
	{
		
		$id_adj_movil=$_POST['codigo'];
		$coperativa=$_POST['coperativa'];
		$cantidad=$_POST['cantidad'];
		
		/* $cantidad= $this->input->get("cantidad");
		$id_adj_movil=$this->input->get("codigo");
		$coperativa=$this->input->get("coperativa") */;
		$this->adjudicaciones_model->adjudicacion($id_adj_movil,$cantidad,$coperativa);
		
	}
	
	public function TraerDatos(){
		
		$localidad=$_POST['localidad'];
		$cantidad=$_POST['cantidad'];
		$programa=$_POST['programa'];
		/* $localidad = $this->input->get("localidad");
		$cantidad= $this->input->get("cantidad");
		$programa =$this->input->get("programa"); */
		$recibidos= $this->adjudicaciones_model->TraerDatos($localidad,$cantidad,$programa);
		echo json_encode($recibidos);
	}
	
	public function GuardarDatosAuto(){
		
		
		/* $localidad=$this->input->get("localidad");
		$programa=$this->input->get('programa');
		$cooperativa=$this->input->get('cooperativa');
		$beneficiario=$this->input->get('beneficiario');
		$id_adj_movil=$this->input->get('codigo');
		$nota=$this->input->get('nota');
		$anio=$this->input->get('anio');
		$fecha = date('Y-m-d H:i:s'); 
		$impresion=$this->input->get('impresion'); */
		  $localidad=$_POST['localidad'];
		$programa=$_POST['programa'];
		$cooperativa=$_POST['cooperativa'];
		$beneficiario=$_POST['beneficiario'];
		$id_adj_movil=$_POST['codigo'];
		$nota=$_POST['nota'];
		$anio=$_POST['anio'];
		$fecha = date('Y-m-d H:i:s'); 
		$impresion=$_POST['impresion']; 
		$this->adjudicaciones_model->GuardarDatos($localidad,$programa,$cooperativa,$id_adj_movil,$beneficiario,$nota,$anio,$fecha,$impresion);
		 $RegistrBitacoras = array(
		                'fecha'             => date('Y-m-d H:i:s'),
						'usuario'     		=> $this->session->userdata('ID'),
						'mensaje'	    	=> "Se Realizo adjudicacion  del beneficiario id : $beneficiario" );
				   
			$this->bitacoras_model->AgregarBitacoras($RegistrBitacoras);
		
	}

public function GuardarDatosAuto2(){
		
		
		/* $localidad=$this->input->get("localidad");
		$programa=$this->input->get('programa');
		$cooperativa=$this->input->get('cooperativa');
		$beneficiario=$this->input->get('beneficiario');
		$id_adj_movil=$this->input->get('codigo');
		$nota=$this->input->get('nota');
		$anio=$this->input->get('anio');
		$fecha = date('Y-m-d H:i:s'); 
		$impresion=$this->input->get('impresion'); */
		$localidad=$_POST['localidad'];
		$programa=$_POST['programa'];
		$cooperativa=$_POST['cooperativa'];
		$beneficiario=$_POST['beneficiario'];
		$id_adj_movil=$_POST['codigo'];
		$nota=$_POST['nota'];
		$anio=$_POST['anio'];
		$fecha = date('Y-m-d H:i:s'); 
		$impresion=$_POST['impresion']; 
		$obs=$_POST['obs']; 
		$tipo=$_POST['tipo']; 
		$this->adjudicaciones_model->GuardarDatos2($localidad,$programa,$cooperativa,$id_adj_movil,$beneficiario,$nota,$anio,$fecha,$impresion,$obs,$tipo);
		 $RegistrBitacoras = array(
		                'fecha'             => date('Y-m-d H:i:s'),
						'usuario'     		=> $this->session->userdata('ID'),
						'mensaje'	    	=> "Se Realizo adjudicacion  del beneficiario id : $beneficiario" );
				   
			$this->bitacoras_model->AgregarBitacoras($RegistrBitacoras);
		
	}


	
	public function ActualizaCupo(){
		$cupo=$_POST['cupo'];
		//Id es el ide de programa
		$id=$_POST['programa'];
		$localidad=$_POST['localidad'];;
		$this->adjudicaciones_model->ActualizaCupo($cupo,$id);
		$this->adjudicaciones_model->ActualizaCupoCiudad($cupo,$id,$localidad);
	}
	
	public function UpdateBene(){
		$resolucion=$_POST['Resolucion'];
		$estado=$_POST['Estado'];
		$observacion=$_POST['observacion'];
		$beneficiario1=$_POST['BeneficiarioOld'];
		$beneficiario2=$_POST['BeneficiarioNvo'];
		$this->adjudicaciones_model->UpdateBene($estado,$beneficiario1,$observacion,$resolucion,$beneficiario2);
		$RegistrBitacoras = array(
		                'fecha'             => date('Y-m-d H:i:s'),
						'usuario'     		=> $this->session->userdata('ID'),
						'mensaje'	    	=> "Se modifico el beneficiario :$beneficiario1 en  adjudicacion id : $resolucion" );
				   
			$this->bitacoras_model->AgregarBitacoras($RegistrBitacoras);
		 
	}
	
	public function Updatefecha()
	{
		$resolucion=$_POST['Resolucion'];
		$fecha=$_POST['Fecha'];
		$this->adjudicaciones_model->Updatefecha($resolucion,$fecha);
		$RegistrBitacoras = array(
		                'fecha'             => date('Y-m-d H:i:s'),
						'usuario'     		=> $this->session->userdata('ID'),
						'mensaje'	    	=> "Se modifico  la fecha en  adjudicacion id : $resolucion" );
				   
			$this->bitacoras_model->AgregarBitacoras($RegistrBitacoras);
	}
	
	
	public function UpdateNota()
	{
		$resolucion=$_POST['Resolucion'];
		$nota=$_POST['Nota'];
		$this->adjudicaciones_model->UpdateNota($resolucion,$nota);
		$RegistrBitacoras = array(
		                'fecha'             => date('Y-m-d H:i:s'),
						'usuario'     		=> $this->session->userdata('ID'),
						'mensaje'	    	=> "Se modifico nota en  adjudicacion id : $resolucion" );
				   
			$this->bitacoras_model->AgregarBitacoras($RegistrBitacoras);
	}
	public function UpdateAnio()
	{
		$resolucion=$_POST['Resolucion'];
		$anio=$_POST['Anio'];
		$this->adjudicaciones_model->UpdateAnio($resolucion,$anio);
		$RegistrBitacoras = array(
		                'fecha'             => date('Y-m-d H:i:s'),
						'usuario'     		=> $this->session->userdata('ID'),
						'mensaje'	    	=> "Se modifico anio en  adjudicacion id : $resolucion" );
				   
			$this->bitacoras_model->AgregarBitacoras($RegistrBitacoras);
	}
	public function UpdateExpte()
	{
		$resolucion=$_POST['Resolucion'];
		$expte=$_POST['Expte'];
		$this->adjudicaciones_model->UpdateExpte($resolucion,$expte);
		$RegistrBitacoras = array(
		                'fecha'             => date('Y-m-d H:i:s'),
						'usuario'     		=> $this->session->userdata('ID'),
						'mensaje'	    	=> "Se modifico expediente en  adjudicacion id : $resolucion" );
				   
			$this->bitacoras_model->AgregarBitacoras($RegistrBitacoras);
	}
	
	
	public function Verificar(){
	
		$pass=$this->input->get("pass");
		$user=$this->adjudicaciones_model->Verificar('sergio_77772@hotmail.com');
		$crypt     = crypt($pass, $user->PASSWORD); 
if($user->PASSWORD==$crypt){		
echo json_encode($user);}
else
	
	{	
$user->ID = 999;

	echo json_encode($user);	
		
	}


	}
	
	public function TraerCodigo(){
		$id=$this->adjudicaciones_model->TraerCodigo();
		echo json_encode($id);
         
	}
	
	public function TraerCupo(){
		$programa=$_POST['programa'];
		//$programa = $this->input->get("programa");
		$id=$this->adjudicaciones_model->TraerCupo($programa);
		echo json_encode($id);
		
	}	
		
		public function TraerCupoCiudad(){
			
		$programa=$_POST['programa'];
		$ciudad=$_POST['localidad'];
		/* $programa = $this->input->get("programa");
		$ciudad = $this->input->get("localidad"); */
		$id=$this->adjudicaciones_model->TraerCupoCiudad($programa,$ciudad);
		echo json_encode($id);
         
         
	}
	//funcion que guarda la fecha si una resolucion no fue impresa
	public function FechaNva(){
		$fecha = $this->input->get("Fecha");
		$id=$this->input->get('ID');
		$this->adjudicaciones_model->FechaNva($fecha,$id);
         
	}
	
	public function TraeNota(){
		
		$id=$this->adjudicaciones_model->TraeNota();
         echo json_encode($id);
	}
	
	
	
	public function nuevo(){
		$url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
          $this->seguridad_model->SessionActivo($url);
		$this->load->view('constant');
		$this->load->view('view_header');
		$data["titulo"]    = "Nuevo Producto";
		$this->load->view('adjudicaciones/view_nuevo_adjudicaciones', $data);
		$this->load->view('view_footer');
		$RegistrBitacoras = array(
		                'fecha'             => date('Y-m-d H:i:s'),
						'usuario'     		=> $this->session->userdata('ID'),
						'mensaje'	    	=> "Se ingreso a nueva adjudicacion" );
				   
			$this->bitacoras_model->AgregarBitacoras($RegistrBitacoras);
		
	}
	
	public function todo(){
		$url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
          $this->seguridad_model->SessionActivo($url);
		$this->load->view('constant');
		$this->load->view('view_header');
		$data["datos"] = $this->adjudicaciones_model->TraerTodo();
		$data["titulo"]    = "Nuevo Producto";
		$this->load->view('adjudicaciones/view_todo', $data);
		$this->load->view('view_footer');
		$RegistrBitacoras = array(
		                'fecha'             => date('Y-m-d H:i:s'),
						'usuario'     		=> $this->session->userdata('ID'),
						'mensaje'	    	=> "Se ingreso a listado de adjudicaciones" );
				   
			$this->bitacoras_model->AgregarBitacoras($RegistrBitacoras);
	}
	
	 public function Editar($numero){
			$url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
			$numero = base64_decode($numero);
			$data["titulo"]    = "Edicion";
		$this->load->view('constant');
		$this->load->view('view_header',$data);
          $this->seguridad_model->SessionActivo($url);
		  $data["adj"] = $this->adjudicaciones_model->busca($numero);
		 $this->load->view('adjudicaciones/view_edicion', $data);
          $this->load->view('view_footer');
		$RegistrBitacoras = array(
		                'fecha'             => date('Y-m-d H:i:s'),
						'usuario'     		=> $this->session->userdata('ID'),
						'mensaje'	    	=> "ingreso a edicion de adjudicaciones" );
				   
			$this->bitacoras_model->AgregarBitacoras($RegistrBitacoras);
		 }
		 
		 
		 public function Eliminar()
	{
		$resolucion=$_POST['Resolucion'];
		$beneficiario=$_POST['Beneficiario'];
		$programa=$_POST['Programa'];
		$cuidad=$_POST['Cuidad'];
		$cooperativa=$_POST['Cooperativa'];
		
		$this->adjudicaciones_model->Eliminar($resolucion,$beneficiario,$programa,$cuidad,$cooperativa);
		$RegistrBitacoras = array(
		                'fecha'             => date('Y-m-d H:i:s'),
						'usuario'     		=> $this->session->userdata('ID'),
						'mensaje'	    	=> "Se elimino la  adjudicacion id : $resolucion" );
				   
			$this->bitacoras_model->AgregarBitacoras($RegistrBitacoras);
	}
		 
		 

	public function exportar()
	{
	       //Bitácora
		   $RegistrBitacoras = array(
		                'fecha'             => date('Y-m-d H:i:s'),
						'usuario'     		=> $this->session->userdata('ID'),
						'mensaje'	    	=> "Exporto Datos de Recibos en formato Excel" );	

            $this->bitacoras_model->AgregarBitacoras($RegistrBitacoras);
			//-//
	
		$this->load->helper('mysql_to_excel_helper');
        $this->load->model('recibos_model');
		to_excel($this->recibos_model->get(), "archivoexcel");
	}


public function nuevo2(){
		$url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
          $this->seguridad_model->SessionActivo($url);
		$this->load->view('constant');
		$this->load->view('view_header');
		$data["titulo"]    = "Nuevo Producto";
		$this->load->view('adjudicaciones/view_nuevo_adjudicaciones_2', $data);
		$this->load->view('view_footer');
		$RegistrBitacoras = array(
		                'fecha'             => date('Y-m-d H:i:s'),
						'usuario'     		=> $this->session->userdata('ID'),
						'mensaje'	    	=> "Se ingreso a nueva adjudicacion" );
				   
			$this->bitacoras_model->AgregarBitacoras($RegistrBitacoras);
		
	}

		
}