<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
date_default_timezone_set('America/Argentina/Jujuy');
class login extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('login_model');
		$this->load->model('bitacoras_model');
	}
	public function index()
	{
		if($this->session->userdata('is_logged_in')){
			$this->load->view('constant');
			$this->load->view('view_header');
			$this->load->view('view_home');
			$this->load->view('view_footer');
			 //Bitácora
			 $ip=getenv("REMOTE_ADDR");
			  $RegistrBitacoras = array(
		      'fecha'             => date('Y-m-d H:i:s'),
			  'usuario'     		=> $this->session->userdata('ID'),
			   'mensaje'	    	=> "INGRESO AL SISTEMA  $ip");	
              $this->bitacoras_model->AgregarBitacoras($RegistrBitacoras);
            //-//
			
		}else{
			$this->load->view('constant');
			$this->load->view('view_login');
		}
	}
	 function CerrarSesion(){
          /*destrozamos la sesion activay nos vamos al login de nuevo*/
          if($this->session->userdata('is_logged_in')){
		    //Bitácora
		   $RegistrBitacoras = array(
		                'fecha'             => date('Y-m-d H:i:s'),
						'usuario'     		=> $this->session->userdata('ID'),
						'mensaje'	    	=> "CERRO SISTEMA" );
				   
			$this->bitacoras_model->AgregarBitacoras($RegistrBitacoras);
			//-//	  
		   $this->login_model->descarga($this->session->userdata('ID'));
               $this->session->sess_destroy(); 
			
               redirect('login', 'refresh');
          }
    }
	public function ValidaAcceso(){
		@session_start();
		$Login 		= json_decode($this->input->post('LoginPost'));
		$response = array (
				"campo"     => "",
	            "error_msg" => ""
	    );
	    if($Login->UserName==""){
			$response["campo"]     = "email";
			$response["error_msg"]   = "<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>La Correo es Obligatorio</div>";
		}else if($Login->Password==""){
			$response["campo"]     = "password";
			$response["error_msg"]   = "<div class='alert alert-danger text-center' alert-dismissable><button type='button' class='close' data-dismiss='alert'>&times;</button>La Contraseña es obligatorio</div>";
		}else{
			$user = $this->login_model->LoginBD($Login->UserName);  
			if(count($user) == 1){
				$crypt     = crypt($Login->Password, $user->PASSWORD);  
				if($user->PASSWORD==$crypt){
					
					
				 
				 $i=$user->TIPO;
                        switch ($i) 
	                    {
                          case 1:
                              $tipoUser= "Administrador";
                               break;
                          case 2:
                              $tipoUser= "Adjudicaciones";
                               break;
		                 case 3:
                              $tipoUser= "Operador";
                              break;	
                         case 4:
                             $tipoUser= "IVUJ";
                             break;
						  case 5:
                              $tipoUser= "Sec. O.T.y V";
                               break;
		                 case 6:
                              $tipoUser= "DDES";
                              break;	
                         case 7:
                             $tipoUser= "Consulta";
                             break;	 
							 	case 8:
                             $tipoUser= "Atencion";
                             break;	 
				        	
				        };			
				
								 
					 
					 $session = array(
                         'ID'           => $user->ID,
                         'NOMBRE'       => $user->NOMBRE,
                         'APELLIDOS'    => $user->APELLIDOS,
                         'EMAIL'        => $Login->UserName,
                         'TIPOUSUARIO'  => $user->TIPO,
                         'TIPOUSUARIOMS'=> $tipoUser,
						 'NIVEL'=> $user->PRIVILEGIOS,
						 'SITIO'=>'cooperativas',
                         'is_logged_in' => TRUE               
                         );
						 //Bitácora
						// $CADENA=print_r($session,true);
						// $CADENA=$user->NOMBRE;
		                 $RegistrBitacoras = array(
		                'fecha'             => date('Y-m-d H:i:s'),
						'usuario'     		=> $this->session->userdata('ID'),
						'mensaje'	    	=> "Ingreso al Sistema: ");	

                         $this->bitacoras_model->AgregarBitacoras($RegistrBitacoras);
			             //-//
				//	$Menu = $this->login_model->CreaMenu($user->ID);
				//	$Menu = json_encode($Menu);
					$this->session->set_userdata($session);//Cargamos la sesion de datos del usuario logeado
					//AQUI GENTE ONLINE
					$this->login_model->Carga($user->ID);
	            //    $_SESSION['Menu'] = $Menu;//cargamos la sesion del menu de acuerdo a los permisos
	                $response["error_msg"]   = '<meta http-equiv="refresh" content="0">';
					
					
				}else{
					$response["error_msg"]   = "<div class='alert alert-danger text-center' alert-dismissable><button type='button' class='close' data-dismiss='alert'>&times;</button>La contraseña o email no son correctos  </div>";
				}
				
			}else{
				$response["error_msg"]   = "<div class='alert alert-danger text-center' alert-dismissable><button type='button' class='close' data-dismiss='alert'>&times;</button>La contraseña o email no son correctos </div>";
			}
		}
		echo json_encode($response);
	}
	
	function contar()
	{
		$local = $this->login_model->contar();
		echo json_encode($local);
		
	}
	
	
	function online()
	{
		$local = $this->login_model->online();
		echo json_encode($local);
		
	}
	
}