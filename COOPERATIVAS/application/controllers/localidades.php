<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
date_default_timezone_set('America/Argentina/Jujuy');
class localidades extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('seguridad_model');
		$this->load->model('localidades_model');
		$this->load->model('bitacoras_model');
	}
	public function index(){
	      //Bitácora
		   $RegistrBitacoras = array(
		                'fecha'             => date('Y-m-d H:i:s'),
						'usuario'     		=> $this->session->userdata('ID'),
						'mensaje'	    	=> "Ingresó a Localidad" );
				   
			$this->bitacoras_model->AgregarBitacoras($RegistrBitacoras);
			//-//
	
          $url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
          $this->seguridad_model->SessionActivo($url);
          /**/
          $this->load->view('constant');
          $this->load->view('view_header');
          $data['localidades'] = $this->localidades_model->ListarLocalidades();
          $this->load->view('localidades/view_localidades', $data);
          $this->load->view('view_footer');
          
	}
	public function deletelocalidad(){
	
         $url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
          $this->seguridad_model->SessionActivo($url);
		$Loca		= json_decode($this->input->post('LocalidadesPost'));
		$id             = base64_decode($Loca->Id);
	
	     //Bitácora
		   $RegistrBitacoras = array(
		                'fecha'             => date('Y-m-d H:i:s'),
						'usuario'     		=> $this->session->userdata('ID'),
						'mensaje'	    	=> "Se Borro Localidad id : $id" );
				   
			$this->bitacoras_model->AgregarBitacoras($RegistrBitacoras);
			//-//
		
		/*Array de response*/
		 $response = array (
				"estatus"   => false,
	            "error_msg" => ""
	    );
		 $this->localidades_model->EliminarLocalidad($id);
		 $response["error_msg"]   = "<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Producto Eliminado Correctamente Clave: <strong>".$id."</strong>, La Información de Actualizara en 5 Segundos <meta http-equiv='refresh' content='5'></div>";
		 echo json_encode($response);
	}	
     public function nuevo(){
          $url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
          $this->seguridad_model->SessionActivo($url);
          $this->load->view('constant');
          $this->load->view('view_header');
          $data['titulo'] = "Nuevo Localidad";
          $this->load->view('localidades/view_nuevo_localidades', $data);
          $this->load->view('view_footer');
     }
     public function editarLocalidad($id){
          $url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
          $this->seguridad_model->SessionActivo($url);
          $id =  base64_decode($id);
          $this->load->view('constant');
          $this->load->view('view_header');
          $data['localidad'] = $this->localidades_model->BuscaLocalidades($id);
          $data['titulo'] = "Editar Localidad";
          $this->load->view('localidades/view_nuevo_localidades', $data);
          $this->load->view('view_footer');
     }
     public function SaveLocalidad(){
        $url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
          $this->seguridad_model->SessionActivo($url);
          $Localidades           = json_decode($this->input->post('LocalidadesPost'));
          $response = array (
                    "estatus"   => false,
                    "campo"     => "",
                 "error_msg" => ""
         );
         if($Localidades->Localidad==""){
               $response["campo"]     = "Localidad";
               $response["error_msg"]   = "<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>La Localidad es Obligatorio</div>";
               echo json_encode($response);
          }else if($Localidades->Departamento==""){
               $response["campo"]     = "departamento";
               $response["error_msg"]   = "<div class='alert alert-danger text-center' alert-dismissable><button type='button' class='close' data-dismiss='alert'>&times;</button>Departamento es obligatorio</div>";
               echo json_encode($response);
          }else if($Localidades->Estado==""){
                    $response["campo"]       = "estado";
                    $response["error_msg"]   = "<div class='alert alert-danger text-center' alert-dismissable><button type='button' class='close' data-dismiss='alert'>&times;</button>Estado es obligatorio</div>";
                    echo json_encode($response);
          }else if($Localidades->Poblacion==""){
               $response["campo"]       = "poblacion";
               $response["error_msg"]   = "<div class='alert alert-danger text-center' alert-dismissable><button type='button' class='close' data-dismiss='alert'>&times;</button>Poblacion es Obligatorio</div>";
               echo json_encode($response);
          }else{
               if($Localidades->Id==""){
                     $RegistraLocalidad    = array(
                     'localidad'       => $Localidades->Localidad,
                     'departamento'    => $Localidades->Departamento,
                     'estado'          => $Localidades->Estado,
                     'poblacion'       => $Localidades->Poblacion
                   //  'fecha_registro'  => date('Y-m-j H:i:s')
                     );
					  //Bitácora
						 $CADENA=print_r($RegistraLocalidad,true);
		                 $RegistrBitacoras = array(
		                'fecha'             => date('Y-m-d H:i:s'),
						'usuario'     		=> $this->session->userdata('ID'),
						'mensaje'	    	=> "Alta de Localidad: $CADENA");	

                         $this->bitacoras_model->AgregarBitacoras($RegistrBitacoras);
			             //-//
                     $this->localidades_model->SaveLocalidades($RegistraLocalidad);
                     $response["error_msg"]   = "<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Informacion Guardada Correctamente</div>";
                     echo json_encode($response);
               }
               if($Localidades->Id!=""){
                    $UpdateLocalidad    = array(
                     'localidad'         => $Localidades->Localidad,
                     'departamento'      => $Localidades->Departamento,
                     'estado'            => $Localidades->Estado,
                     'poblacion'         => $Localidades->Poblacion
                //     'fecha_registro'      => date('Y-m-j H:i:s')
                     );
                         //Bitácora
						 $CADENA=print_r($UpdateLocalidad,true);
		                 $RegistrBitacoras = array(
		                'fecha'             => date('Y-m-d H:i:s'),
						'usuario'     		=> $this->session->userdata('ID'),
						'mensaje'	    	=> "Modificación de Localidad: $CADENA");	

                         $this->bitacoras_model->AgregarBitacoras($RegistrBitacoras);
			             //-//
					 
                    $this->localidades_model->UpdateLocalidades($UpdateLocalidad, $Localidades->Id);
                    $response["error_msg"]   = "<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button> Informacion Actualizada Correctamente</div>";
                    echo json_encode($response);
               }
          }
     }
}