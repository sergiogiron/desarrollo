<?php if ( !defined('BASEPATH')) exit('No direct script access allowed');
class referentes extends CI_Controller {
	public function __construct(){ parent::__construct();
	 $this->load->database(); 
	$this->load->database(); 
	 $this->load->model('seguridad_model');
		
		$this->load->model('bitacoras_model');
		$this->load->helper('date');
	 $this->load->library('grocery_CRUD');}
	
	public function index(){
		$url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
          $this->seguridad_model->SessionActivo($url);
		$RegistrBitacoras = array(
		                'fecha'             => date('Y-m-d H:i:s'),
						'usuario'     		=> $this->session->userdata('ID'),
						'mensaje'	    	=> "ingreso a instituciones" );
				   
			$this->bitacoras_model->AgregarBitacoras($RegistrBitacoras);
	 	$this->grocery_crud = new grocery_CRUD();

 $this->grocery_crud->set_language('spanish');
$this->grocery_crud->set_theme('flexigrid');

$this->grocery_crud->set_table("referentes");
//$this->grocery_crud->set_subject('institucion');

$this->grocery_crud->required_fields('dni','id_localidad','nombre');  
$this->grocery_crud->columns('nombre','dni','id_localidad');
$this->grocery_crud->display_as('id_localidad','localidad');
$this->grocery_crud->set_relation('id_localidad','localidades','nombre_localidad');

$output = $this->grocery_crud->render();
	$this->load->view('constant');
          $this->load->view('view_header');
          $this->load->view('view_footer');
$this->load->view("referentes/listar",$output); 



	}
}