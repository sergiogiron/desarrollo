<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
date_default_timezone_set('America/Argentina/Jujuy');
class usuarios extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('seguridad_model');
		$this->load->model('usuarios_model');
    //$this->load->model('expedientes_model');
		$this->load->model('bitacoras_model');
	}
	public function index(){
          $url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
          $this->seguridad_model->SessionActivo($url);
          /**/
          $this->load->view('constant');
          $this->load->view('view_header');
          $data['usuarios'] = $this->usuarios_model->ListarUsuarios();
          $this->load->view('usuarios/view_usuarios', $data);
          $this->load->view('view_footer');
          
	}
     public function nuevo(){
          $url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
          $this->seguridad_model->SessionActivo($url);
          $this->load->view('constant');
          $this->load->view('view_header');
          $data['titulo'] = "Nuevo Usuario";


          $this->load->view('usuarios/view_nuevo_usuarios', $data);
          $this->load->view('view_footer');
     }
     public function editarUsuario($id){
          $url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
          $this->seguridad_model->SessionActivo($url);
          $id =  base64_decode($id);
          $this->load->view('constant');
          $this->load->view('view_header');
          $data['usuario'] = $this->usuarios_model->BuscaUsuario($id);
          $data['titulo'] = "Editar Usuario";
          $this->load->view('usuarios/view_nuevo_usuarios', $data);
          $this->load->view('view_footer');
     }
     public function GUsuario(){
	 
	      $url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
          $this->seguridad_model->SessionActivo($url);
          $Usu         = json_decode($this->input->post('UsuariosPost'));
          $response = array (
                   "campo"     => "",
                 "error_msg" => ""
         );
		 
		//Bitácora
		   $CADENA=print_r($Usu,true);
		   $RegistrBitacoras = array(
		   'fecha'             => date('Y-m-d H:i:s'),
			 'usuario'     		=> $this->session->userdata('ID'),
			 'mensaje'	    	=> "Alta de Usuario: $CADENA");	

        $this->bitacoras_model->AgregarBitacoras($RegistrBitacoras);
			             //-//   
          if($Usu->Nombre==""){
               $response["campo"]     = "nombre";
               $response["error_msg"]   = "<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>El Nombre del Usuario es Obligatorio</div>";
               echo json_encode($response);
         }else if($Usu->Telefono==""){
               $response["campo"]     = "telefono";
               $response["error_msg"]   = "<div class='alert alert-danger text-center' alert-dismissable><button type='button' class='close' data-dismiss='alert'>&times;</button>El  Telefono es obligatorio</div>";
               echo json_encode($response);
		     }else if($Usu->Email==""){
               $response["campo"]       = "email";
               $response["error_msg"]   = "<div class='alert alert-danger text-center' alert-dismissable><button type='button' class='close' data-dismiss='alert'>&times;</button>El Correo Es Obligatorio</div>";
               echo json_encode($response);
	        }else if($Usu->TipoU=="0"){
               $response["campo"]       = "tipou";
               $response["error_msg"]   = "<div class='alert alert-danger text-center' alert-dismissable><button type='button' class='close' data-dismiss='alert'>&times;</button>Tipo de Usuario es Obligatorio</div>";
               echo json_encode($response);
          }else if($Usu->Password1==""){ 
               $response["campo"]     = "password1";
               $response["error_msg"]   = "<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>La contraseña del Usuario es Obligatorio</div>";
               echo json_encode($response);
          }else if ($Usu->Password2==""){
              $response["campo"]     = "password2";
               $response["error_msg"]   = "<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>La confirmacion de contraseña del Usuario es Obligatorio</div>";
               echo json_encode($response);
		      }else if($Usu->Area=="0"){
               $response["campo"]       ="area";
               $response["error_msg"]   ="<div class='alert alert-danger text-center' alert-dismissable><button type='button' class='close' data-dismiss='alert'>&times;</button>La selección de un Área Es Obligatorio</div>";
               echo json_encode($response);
	        }else if($Usu->Estatus=="0"){
               $response["campo"]       = "estatus";
               $response["error_msg"]   = "<div class='alert alert-danger text-center' alert-dismissable><button type='button' class='close' data-dismiss='alert'>&times;</button>El Estatus es Obligatorio</div>";
               echo json_encode($response);
		      }else if($Usu->Password1!=$Usu->Password2){
               $response["campo"]       = "password2";
               $response["error_msg"]   = "<div class='alert alert-danger text-center' alert-dismissable><button type='button' class='close' data-dismiss='alert'>&times;</button>La confirmación de Contraseña Es Incorrecta</div>";
          	   echo json_encode($response); 
 
          }else{  
               if($Usu->Id==""){
                    $ExisteEmail       = $this->usuarios_model->ExisteEmail($Usu->Email);
                    if($ExisteEmail==true){
                         $response["campo"]     = "Email";
                         $response["error_msg"]   = "<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>El Email Ya esta en Uso</div>";
						              echo json_encode($response); 
                  }else{
 					              $RegistraUsuario    = array(
                         'NOMBRE'          => $Usu->Nombre,
                         'APELLIDOS'       => $Usu->Telefono,
                         'EMAIL'           => $Usu->Email,
                         'FECHA_REGISTRO'  => date('Y-m-j H:i:s'),
    					           'ESTATUS'          => $Usu->Estatus,
                         'TIPO'            => $Usu->TipoU,
    			               'PASSWORD'       => crypt($Usu->Password1,$Usu->Nombre),
                         'ID_AREA'           => $Usu->Area,
    					           'PRIVILEGIOS'      => $Usu->Privilegios,
    					             );
				
                     $this->usuarios_model->SaveUs($RegistraUsuario);
                     $response["error_msg"]   = "<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Informacion Guardada Correctamente</div>";
                     echo json_encode($response);
               }}
                if($Usu->Id!=""){
			   
                    if($Usu->Cambiopas==true){ // Si ambos campos estan vacios, la contraseña no cambia, si es falso pasa a la condicion de abajo 
                  
                        $UpdateUsuario    = array(
                        'NOMBRE'         => $Usu->Nombre,
                     	  'APELLIDOS'      => $Usu->Telefono,
    					          'EMAIL'          => $Usu->Email,
                        'FECHA_REGISTRO' => date('Y-m-j H:i:s'),
    					          'ESTATUS'        => $Usu->Estatus,
                        'TIPO'          => $Usu->TipoU,
    					           //'PASSWORD'       =>crypt($Usu->Password1,$Usu->Nombre),
                        'ID_AREA'           => $Usu->Area,
                        'PRIVILEGIOS'    => $Usu->Privilegios,
    					
                        );
                        $this->usuarios_model->UpdateUs($UpdateUsuario, $Usu->Id);
                        $response["error_msg"]   = "<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button> Informacion Actualizada Correctamente</div>";
                        echo json_encode($response);

                    }else{

                        $UpdateUsuario    = array(
                          'NOMBRE'         => $Usu->Nombre,
                          'APELLIDOS'      => $Usu->Telefono,
                          'EMAIL'          => $Usu->Email,
                          'FECHA_REGISTRO' => date('Y-m-j H:i:s'),
                          'ESTATUS'        => $Usu->Estatus,
                          'TIPO'          => $Usu->TipoU,
                          'PASSWORD'       =>crypt($Usu->Password1,$Usu->Nombre),
                          'ID_AREA'           => $Usu->Area,
                          'PRIVILEGIOS'    => $Usu->Privilegios,
                          );
                        
                        $this->usuarios_model->UpdateUs($UpdateUsuario, $Usu->Id);
                        $response["error_msg"]   = "<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button> Informacion Actualizada Correctamente</div>";
                        echo json_encode($response);



                  }
               }
          }
     }
     //Obtiene registros almacenados en Areas...
    public function areas(){
      $local = $this->usuarios_model->traerAreas();
      echo json_encode($local);
    }
	 
	 	public function deleteusuario(){
	
         $url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
         $this->seguridad_model->SessionActivo($url);
		 $Usu	= json_decode($this->input->post('MiUsuario'));
		$id    = base64_decode($Usu->Id);
	
	     //Bitácora
		   $RegistrBitacoras = array(
		                'fecha'             => date('Y-m-d H:i:s'),
						'usuario'     		=> $this->session->userdata('ID'),
						'mensaje'	    	=> "Se Borro Usuario: $id" );
				   
			$this->bitacoras_model->AgregarBitacoras($RegistrBitacoras);
			//-//
		
		/*Array de response*/
		 $response = array (
				"estatus"   => false,
	            "error_msg" => ""
	    );
		 $this->usuarios_model->EliminarUsuario($id);
		 $response["error_msg"]   = "<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Programa Eliminado Correctamente Clave: <strong>".$id."</strong>, La Información de Actualizara en 5 Segundos <meta http-equiv='refresh' content='5'></div>";
		 echo json_encode($response);
	}
 
	 
	 
	 
	 
	 
}