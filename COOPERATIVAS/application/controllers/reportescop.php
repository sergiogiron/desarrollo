<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
date_default_timezone_set('America/Mexico_City');
class reportescop extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('seguridad_model');
		$this->load->model('reportescop_model');
		$this->load->model('bitacoras_model');
	}
	public function index(){
          $url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
          $this->seguridad_model->SessionActivo($url);
          /**/
          $this->load->view('constant');
          $this->load->view('view_header'); 
          $this->load->view('reportescop/view_reportescop');
          $this->load->view('view_footer');
		  $RegistrBitacoras = array(
		                'fecha'             => date('Y-m-d H:i:s'),
						'usuario'     		=> $this->session->userdata('ID'),
						'mensaje'	    	=> "se ingreso a reporte de cooperativas" );
				   
			$this->bitacoras_model->AgregarBitacoras($RegistrBitacoras);
          
	}
     public function GeneraReporte(){
          $url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
          $this->seguridad_model->SessionActivo($url);
          $estado= $_POST['estado'];
		  $localidad=$_POST['localidad'];
		 $local= $this->reportescop_model->GeneraReporte($estado,$localidad);
		echo json_encode($local);
		 //Bitácora
		   $RegistrBitacoras = array(
		                'fecha'             => date('Y-m-d H:i:s'),
						'usuario'     		=> $this->session->userdata('ID'),
						'mensaje'	    	=> "se genero reporte de cooperativas estado: $estado localidad:$localidad" );
				   
			$this->bitacoras_model->AgregarBitacoras($RegistrBitacoras);
			//-//

          
     }
     public function GeneraReporte2(){
          $url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
          $this->seguridad_model->SessionActivo($url);
           $check= $_POST['check'];
		 $local= $this->reportescop_model->GeneraReporte2($check);
		echo json_encode($local);
		 //Bitácora
		   $RegistrBitacoras = array(
		                'fecha'             => date('Y-m-d H:i:s'),
						'usuario'     		=> $this->session->userdata('ID'),
						'mensaje'	    	=> "se genero reporte de todas las cooperativas" );
				   
			$this->bitacoras_model->AgregarBitacoras($RegistrBitacoras);
			//-//

          
     }
	 
	 public function DatosNecesarios(){
          $localidad= $this->input->get("localidad");
		 $local= $this->reportes_model->DatosNecesarios($localidad);
		echo json_encode($local);
	 
	 }
	 
	 
	  public function localidades(){
		$local = $this->reportes_model->Localidades();
		echo json_encode($local);
		
	  }
}