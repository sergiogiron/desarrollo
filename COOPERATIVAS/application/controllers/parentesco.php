<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
date_default_timezone_set('America/Argentina/Jujuy');
class parentesco extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('seguridad_model');
		$this->load->model('parentesco_model');
		$this->load->model('bitacoras_model');
	    $this->load->model('cooperativas_model');
	    $this->load->model('socios_model');
	}
	public function index(){
	      //Bitácora
		  $RegistrBitacoras = array(
		        'fecha'             => date('Y-m-d H:i:s'),
				'usuario'     		=> $this->session->userdata('ID'),
				'mensaje'	    	=> "Ingresó a Parentesco" );
				   
		  $this->bitacoras_model->AgregarBitacoras($RegistrBitacoras);
			//-//
          $url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
          $this->seguridad_model->SessionActivo($url);
          /**/
          $this->load->view('constant');
          $this->load->view('view_header');
          $data['socios'] = $this->parentesco_model->ListarSocios();
          $this->load->view('parentesco/view_parentesco', $data);
          $this->load->view('view_footer');
          
	}
	public function deletesocio(){
	
         $url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
         $this->seguridad_model->SessionActivo($url);
		 $Socio	= json_decode($this->input->post('SociosPost'));
		 $id    = base64_decode($Socio->Id);
	
	     //Bitácora
		 $RegistrBitacoras = array(
		                'fecha'             => date('Y-m-d H:i:s'),
						'usuario'     		=> $this->session->userdata('ID'),
						'mensaje'	    	=> "Se Borro Pariente de Socio id : $id" );
				   
	     $this->bitacoras_model->AgregarBitacoras($RegistrBitacoras);
			//-//
		/*Array de response*/
		 $response = array (
				"estatus"   => false,
	            "error_msg" => ""
	    );
		 $this->parentesco_model->EliminarSocio($id);
		 $response["error_msg"]   = "<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Socio Eliminado Correctamente Clave: <strong>".$id."</strong>, La Información de Actualizara en 5 Segundos <meta http-equiv='refresh' content='5'></div>";
		 echo json_encode($response);
	}

     public function nuevo(){
          $url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
          $this->seguridad_model->SessionActivo($url);
          $this->load->view('constant');
          $this->load->view('view_header');
          $data['titulo'] = "Nuevo Pariente";
          $this->load->view('parentesco/view_nuevo_parentesco', $data);
          $this->load->view('view_footer');
     }

     public function editarSocio($id){
          $url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
          $this->seguridad_model->SessionActivo($url);
          $id =  base64_decode($id);
		    //Bitácora
		  $RegistrarBitacoras = array(
		        'fecha'             => date('Y-m-d H:i:s'),
				'usuario'     		=> $this->session->userdata('ID'),
				'mensaje'	    	=> "Ingresó a Editar Pariente Socio $id" );
				   
		  $this->bitacoras_model->AgregarBitacoras($RegistrarBitacoras);
			//-//
          $this->load->view('constant');
          $this->load->view('view_header');
          $data['socio'] = $this->parentesco_model->BuscaSocios($id);
         // print_r($data['socio']);
          $data['titulo'] = "Editar Socio";
          $this->load->view('parentesco/view_nuevo_parentesco', $data);
          $this->load->view('view_footer');  
     }
	
	/**
      * Busca socios segun Dni o nombre
      * @return return objeto json  
      */
	public function buscarSocio(){
		$filtro    = $this->input->get("buscaSocio"); 
		$socios = $this->parentesco_model->BuscarSocios($filtro);
		echo json_encode($socios);
	
	}
	
     public function SaveSocio(){
        $url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
        $this->seguridad_model->SessionActivo($url);
        $Socios         = json_decode($this->input->post('SociosPost'));
        //print_r($Socios);

          $response = array (
                "estatus"   => false,
                "campo"     => "",
                "error_msg" => ""
         );  
         if($Socios->Documento==""){
               $response["campo"]    	= "documento";
               $response["error_msg"]   = "<div class='alert alert-danger text-center' alert-dismissable><button type='button' class='close' data-dismiss='alert'>&times;</button>El Documento es Obligatorio</div>";
               echo json_encode($response);
          }else if($Socios->Socio ==""){
               $response["campo"]       = "socio";
               $response["error_msg"]   = "<div class='alert alert-danger text-center' alert-dismissable><button type='button' class='close' data-dismiss='alert'>&times;</button>Socio es obligatorio</div>";
               echo json_encode($response);
           }else if($Socios->Cooperativa==""){
           		$response["campo"]       = "cooperativa";
               	$response["error_msg"]   = "<div class='alert alert-danger text-center' alert-dismissable><button type='button' class='close' data-dismiss='alert'>&times;</button>Cooperativa es obligatorio</div>";
               	echo json_encode($response);
           }else if($Socios->Parentesco=="0"){
               $response["campo"]       = "parentesco";
               $response["error_msg"]   = "<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Debe indicar el parentesco del Socio</div>";
               echo json_encode($response);
           }else if($Socios->Cuit==""){
               $response["campo"]       = "cuit";
               $response["error_msg"]   = "<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Cuit es  Obligatorio</div>";
               echo json_encode($response);
	      }else if($Socios->Nombre==""){
               $response["campo"]       = "nombre";
               $response["error_msg"]   = "<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>El Nombre del pariente es Obligatorio</div>";
               echo json_encode($response);
           }else if($Socios->Fechanac==""){
               $response["campo"]       = "fechanac";
               $response["error_msg"]   = "<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Fecha de Nacimiento es Obligatorio</div>";
               echo json_encode($response);
            }else if($Socios->Sexo=="0"){
               $response["campo"]       = "sexo";
               $response["error_msg"]   = "<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Género es Obligatorio</div>";
               echo json_encode($response);
            }else if($Socios->EstadoCivil=="0"){
               $response["campo"]       = "estadoCivil";
               $response["error_msg"]   = "<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Estado Civil es Obligatorio</div>";
               echo json_encode($response);
            }else if($Socios->CaractCelular==""){
               $response["campo"]       = "caractCelular";
               $response["error_msg"]   = "<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Caracteristica es Obligatorio</div>";
               echo json_encode($response);
            }else if($Socios->Celular==""){
               $response["campo"]       = "celular";
               $response["error_msg"]   = "<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Celular es Obligatorio</div>";
               echo json_encode($response);
               //EMAIL (no considerado como obligatorio)
             }else if($Socios->FechaIngreso==""){
               $response["campo"]       = "fechaIngreso";
               $response["error_msg"]   = "<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Fecha de Ingreso es Obligatoria</div>";
               echo json_encode($response);
              }else if($Socios->SitRevista=="0"){
               $response["campo"]       = "sitRevista";
               $response["error_msg"]   = "<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Situacion Revista es Obligatorio</div>";
               echo json_encode($response);
  			  }else if($Socios->FechaVenc==""){
               $response["campo"]       = "fechaVencimiento";
               $response["error_msg"]   = "<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Fecha de Vencimiento es Obligatoria</div>";
               echo json_encode($response);
              }else if($Socios->Localidad=="0"){
               $response["campo"]       = "localidad";
               $response["error_msg"]   = "<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button> Localidad es Obligatorio</div>";
               echo json_encode($response);
	          }else if($Socios->Domicilio==""){
	            $response["campo"]       = "domicilio";
	            $response["error_msg"]   = "<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Domicilio es obligatorio</div>";
	            echo json_encode($response);
			  	//NRO, PISO, DPTO, BLOQUE (no tomados como obligatorios)
	          }else if($Socios->Telefono==""){
	            $response["campo"]       = "telefono";
	            $response["error_msg"]   = "<div class='alert alert-danger text-center' alert-dismissable><button type='button' class='close' data-dismiss='alert'>&times;</button>Telefono es Obligatorio</div>";
	            echo json_encode($response);

		       }else if($Socios->Id==""){
				        $ExisteDocumento   = $this->parentesco_model->ExisteDocumento($Socios->Documento);
						if($ExisteDocumento==true){
							$response["campo"]     = "codigo";
							$response["error_msg"]   = "<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>El Documento Existe</div>";
								echo json_encode($response);
			   }else{
			   
                           $RegistraSocio    = array(
				           'documento'           => $Socios->Documento,
				           'id_socio'			 => $Socios->Socio,
				           'id_cooperativa'      => $Socios->Cooperativa,
				           'parentesco'          => $Socios->Parentesco,
			               'cuit'                => $Socios->Cuit,
			               'nombre_pariente'     => $Socios->Nombre,
					       'fechanac'            => $Socios->Fechanac,
					       'sexo'				 => $Socios->Sexo,
					       'estadoCivil'		 => $Socios->EstadoCivil,
					       'caractCelular'		 => $Socios->CaractCelular,
					       'celular'			 => $Socios->Celular,
					       'email'				 => $Socios->Email,
					       'fechaIngreso'		 => $Socios->FechaIngreso,
					       'sitRevista'			 => $Socios->SitRevista,
					       'fechaVencimiento'    => $Socios->FechaVenc,
					       'id_localidad'        => $Socios->Localidad,
					       'domicilio'           => $Socios->Domicilio,
				           'nro'                 => $Socios->Numero,
				           'piso'                => $Socios->Piso,
					       'dpto'                => $Socios->Dpto,				 
					       'bloque'              => $Socios->Bloque,
                           'telefono'            => $Socios->Telefono,
					 
                     );
					  //Bitácora
						 $CADENA=print_r($RegistraSocio,true);
		                 $RegistrarBitacoras = array(
		                'fecha'             => date('Y-m-d H:i:s'),
						'usuario'     		=> $this->session->userdata('ID'),
						'mensaje'	    	=> "Alta de Pariente Socio: $CADENA");	

                         $this->bitacoras_model->AgregarBitacoras($RegistrarBitacoras);
			             //-//
	                     $this->parentesco_model->SaveSocios($RegistraSocio);
	                     $response["error_msg"]   = "<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Informacion Guardada Correctamente</div>";
	                     echo json_encode($response);
               }
           }


               else if($Socios->Id!=""){
               	

                    $UpdateSocio    = array(
					 'documento'           => $Socios->Documento,
					 'id_socio'			   => $Socios->Socio,
				     'id_cooperativa'      => $Socios->Cooperativa,
				     'parentesco'          => $Socios->Parentesco,
					 'cuit'                => $Socios->Cuit,
					 'nombre_pariente'     => $Socios->Nombre,
					 'fechanac'            => $Socios->Fechanac,
					 'sexo'				   => $Socios->Sexo,
					 'estadoCivil'		   => $Socios->EstadoCivil,
					 'caractCelular'	   => $Socios->CaractCelular,
					 'celular'			   => $Socios->Celular,
					 'email'			   => $Socios->Email,
					 'fechaIngreso'		   => $Socios->FechaIngreso,
					 'sitRevista'		   => $Socios->SitRevista,
					 'fechaVencimiento'	   => $Socios->FechaVenc,
					 'id_localidad'        => $Socios->Localidad,
					 'domicilio'           => $Socios->Domicilio,
					 'nro'                 => $Socios->Numero,
				     'piso'                => $Socios->Piso,
					 'dpto'                => $Socios->Dpto,				 
					 'bloque'              => $Socios->Bloque,
                     'telefono'            => $Socios->Telefono,
                     );
                         //Bitácora
					$CADENA=print_r($UpdateSocio,true);
		            $RegistrarBitacoras = array(
		                'fecha'             => date('Y-m-d H:i:s'),
						'usuario'     		=> $this->session->userdata('ID'),
						'mensaje'	    	=> "Modificación de Pariente Socio: $CADENA");	

                    $this->bitacoras_model->AgregarBitacoras($RegistrarBitacoras);
			             
                    $this->parentesco_model->UpdateSocios($UpdateSocio, $Socios->Id);
                    $response["error_msg"]   = "<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button> Informacion Actualizada Correctamente</div>";
                    echo json_encode($response);

               }
               
          }
     }
