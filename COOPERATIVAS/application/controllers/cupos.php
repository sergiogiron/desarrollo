<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
date_default_timezone_set('America/Mexico_City');
class cupos extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('seguridad_model');
		$this->load->model('cupos_model');
		$this->load->model('bitacoras_model');
	}
	public function index(){
          $url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
          $this->seguridad_model->SessionActivo($url);
          /**/
          $this->load->view('constant');
          $this->load->view('view_header');
          $data['cupos'] = $this->cupos_model->ListarCupos();
          $this->load->view('cupos/view_cupos', $data);
          $this->load->view('view_footer');
          
	}
     public function nuevo(){
          $url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
          $this->seguridad_model->SessionActivo($url);
          $this->load->view('constant');
          $this->load->view('view_header');
          $data['titulo'] = "Nuevo Cupo";
          $this->load->view('cupos/view_nuevo_cupos', $data);
          $this->load->view('view_footer');
     }
     public function editarCupo($id){
          $url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
          $this->seguridad_model->SessionActivo($url);
          $id =  base64_decode($id);
          $this->load->view('constant');
          $this->load->view('view_header');
          $data['cupo'] = $this->cupos_model->BuscaCupos($id);
          $data['titulo'] = "Editar Cupos";
          $this->load->view('cupos/view_nuevo_cupos', $data);
          $this->load->view('view_footer');
     }
     public function SCupo(){
	 
	      $url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
          $this->seguridad_model->SessionActivo($url);
          $Cu         = json_decode($this->input->post('ProgramasPost'));
		  
          $response = array (
                    "estatus"   => false,
                    "campo"     => "",
                 "error_msg" => ""
         );
		 //Bitácora
						 $CADENA=print_r($Cu,true);
		                 $RegistrBitacoras = array(
		                'fecha'             => date('Y-m-d H:i:s'),
						'usuario'     		=> $this->session->userdata('ID'),
						'mensaje'	    	=> "Alta de Cupo: $CADENA");	

                         $this->bitacoras_model->AgregarBitacoras($RegistrBitacoras);
			             //-//
		    		 
		 
		 
		 
		 
         if($Cu->Programa=="0"){
               $response["campo"]     = "Programa";
               $response["error_msg"]   = "<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>El Nombre del Programa es Obligatorio</div>";
               echo json_encode($response);
       
        }else if($Cu->Localidad=="0"){
               $response["campo"]     = "Localidad";
               $response["error_msg"]   = "<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>El Nombre del Localidad es Obligatorio</div>";
               echo json_encode($response);
       
        }else  if($Cu->Id==""){
                     $RegistraCupo    = array(
                     'id_programa'     => $Cu->Programa,
                     'Id_localidad'    => $Cu->Localidad,
                     'cupo'             => $Cu->Cuposs,
				     
                      );
				
				//Bitácora
						 $CADENA=print_r($RegistraCupo,true);
		                 $RegistrBitacoras = array(
		                'fecha'             => date('Y-m-d H:i:s'),
						'usuario'     		=> $this->session->userdata('ID'),
						'mensaje'	    	=> "Control de Cupo: $CADENA");	

                         $this->bitacoras_model->AgregarBitacoras($RegistrBitacoras);
			             //-//
				
				
				
				
				
				
				
				
                     $this->cupos_model->SaveCupos($RegistraCupo);
                     $response["error_msg"]   = "<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Informacion Guardada Correctamente</div>";
                     echo json_encode($response);
               }
               if($Cu->Id!=""){
                    $UpdateCupo    = array(
                        'id_programa'     => $Cu->Programa,
                     'Id_localidad'    => $Cu->Localidad,
                     'cupo'             => $Cu->Cuposs,
				     
                
                     );
                    $this->cupos_model->UpdateCupos($UpdateCupo, $Cu->Id);
                    $response["error_msg"]   = "<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button> Informacion Actualizada Correctamente</div>";
                    echo json_encode($response);
               }
          
     }
	 
	 	public function deletecupo(){
	
         $url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
         $this->seguridad_model->SessionActivo($url);
		 $Cupo	= json_decode($this->input->post('ProgramaPost'));
		$id    = base64_decode($Cupo->Id);
	
	
	     //Bitácora
		   $RegistrBitacoras = array(
		                'fecha'             => date('Y-m-d H:i:s'),
						'usuario'     		=> $this->session->userdata('ID'),
						'mensaje'	    	=> "Se Borro Cupo: $id" );
				   
			$this->bitacoras_model->AgregarBitacoras($RegistrBitacoras);
			//-//
		
		/*Array de response*/
		 $response = array (
				"estatus"   => false,
	            "error_msg" => ""
	    );
		 $this->cupos_model->EliminarCupo($id);
		 $response["error_msg"]   = "<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Programa Eliminado Correctamente Clave: <strong>".$id."</strong>, La Información de Actualizara en 5 Segundos <meta http-equiv='refresh' content='5'></div>";
		 echo json_encode($response);
	}
 
	 public function localidades(){
		$local = $this->cupos_model->Localidades();
		echo json_encode($local);
	}
	 
	public function programas(){
		$local = $this->cupos_model->Programas();
		echo json_encode($local);
	} 
	 
	 public function traeCupo()
	 {
		 
		  $programa= $_POST['programa'];
		  $local= $this->cupos_model->traeCupo($programa);
		 echo json_encode($local);
		 
	 }
	 
	  public function Contar()
	 {
		
		 $localidad= $_POST['localidad'];
		 $programa= $_POST['programa']; 
		  $local= $this->cupos_model->Contar($localidad,$programa);
		 echo json_encode($local);
		 
	 }
	 
	 
	 
}