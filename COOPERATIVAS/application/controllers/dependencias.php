<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
date_default_timezone_set('America/Argentina/Jujuy');
class dependencias extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('seguridad_model');
		$this->load->model('dependencias_model');
		$this->load->model('bitacoras_model');
    //$this->load->helper('date');
	  // ----------exportar------------//
		$this->load->helper('mysql_to_excel_helper');
    $this->load->library('form_validation'); //Para validacion de campos
		
	  }

  
	public function index(){
          //Bitácora
          $RegistrarBitacora = array(
              'fecha'           => date('Y-m-d H:i:s'),
              'usuario'         => $this->session->userdata('ID'),
              'mensaje'         => "Ingresó a Dependencias" );
             
          $this->bitacoras_model->AgregarBitacoras($RegistrarBitacora);

          $url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
          $this->seguridad_model->SessionActivo($url);
          $this->load->view('constant');    //Carga vista títulos de Inicio
          $this->load->view('view_header'); //Carga vista del Menú Horizontal
          $data['dep'] = $this->dependencias_model->listarDep();
          $this->load->view('dependencias/view_dependencias', $data);
          $this->load->view('view_footer'); //Carga vista Pie de Página  
	   }

   public function nuevo(){
          //Bitácora
          $RegistrarBitacora = array(
              'fecha'           => date('Y-m-d H:i:s'),
              'usuario'         => $this->session->userdata('ID'),
              'mensaje'         => "Ingresó a Nueva Dependencia" );
             
          $this->bitacoras_model->AgregarBitacoras($RegistrarBitacora);

          $url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
          $this->seguridad_model->SessionActivo($url);
          $this->load->view('constant'); //Carga vista títulos de Inicio
          $this->load->view('view_header'); //Carga vista del Menú Horizontal
          $data['titulo'] = "Nueva Dependencia";
          $data["max"] =  $this->dependencias_model->maxCodigo();
          $this->load->view('dependencias/view_nuevo_dependencias', $data);
          $this->load->view('view_footer'); //Carga vista Pie de Pagina
     }
     
     public function editarDependencia($id){

          $url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
          $this->seguridad_model->SessionActivo($url);
          $id =  base64_decode($id); //Encripta el id
           //Bitácora
          $RegistrarBitacora = array(
              'fecha'           => date('Y-m-d H:i:s'),
              'usuario'         => $this->session->userdata('ID'),
              'mensaje'         => "Editó Dependencia con id : $id" );
          $this->bitacoras_model->AgregarBitacoras($RegistrarBitacora);

          $this->load->view('constant');      //Carga vista titulos de Inicio 
          $this->load->view('view_header');   //Carga vista del Menú Principal
          $data['dep'] = $this->dependencias_model->buscarDep($id);
          $data['titulo'] = "Editar Dependencia";
          $this->load->view('dependencias/view_nuevo_dependencias', $data);
          $this->load->view('view_footer');   //Carga vista Pie de Página
            
     }


     public function saveDependencia(){
	 
	        $url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
          $this->seguridad_model->SessionActivo($url);
          $Dep = json_decode($this->input->post('DepPost'));
         // echo($Dep);
          $response = array (
                    "estatus"   => false,
                    "campo"     => "",
                    "error_msg" => "" 
                    );

        if($Dep->Nombre==""){
               $response["campo"]       = "nombre"; //Campo del formulario
               $response["error_msg"]   = "<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Debe ingresar Nombre </div>";
               echo json_encode($response); //Retorna la representación JSON del valor dado
           
          //CAMPO OBSERVACIONES NO CONSIDERADO COMO OBLIGATORIO...


        } else if($Dep->Id==""){ //Si el id no existe, se REGISTRA una nueva Área
               
              // var_dump($Dep->Codigo);
          $RegistraDep  = array(
             'codigo_dep'        => $Dep->Codigo,
             'nombre_dep'        => $Dep->Nombre,
             'obs_dep'           => $Dep->Observaciones,
              );

          //Bitácora
          $CADENA=print_r($RegistraDep,true);
          $RegistrarBitacora  = array(
                'fecha'       => date('Y-m-d H:i:s'),
                'usuario'     => $this->session->userdata('ID'),
                'mensaje'     => "Alta de una Dependencia: $CADENA");  
          $this->bitacoras_model->AgregarBitacoras($RegistrarBitacora);
                   
          $this->dependencias_model->SaveDep($RegistraDep); //aqui  esta en esa funcion del modelo? me parece que siveamos buscala  y comp varibles
          $response["error_msg"]   = "<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Informacion Guardada Correctamente</div>"; 
          echo json_encode($response);

          } else if($Dep->Id!=""){ //Si el id ya existe, se realiza la ACTUALIZACION de una Dependencia
                    $UpdateDep    = array(
                    'codigo_dep'        => $Dep->Codigo,
                    'nombre_dep'        => $Dep->Nombre,
                    'obs_dep'           => $Dep->Observaciones,
                      );
                    //Bitácora
                    $CADENA=print_r($UpdateDep,true);
                    $RegistrarBitacora     = array(
                        'fecha'            => date('Y-m-d H:i:s'),
                        'usuario'          => $this->session->userdata('ID'),
                        'mensaje'          => "Editó Dependencia: $CADENA"); 
                    $this->bitacoras_model->AgregarBitacoras($RegistrarBitacora);

                    $this->dependencias_model->updateDep($UpdateDep, $Dep->Id);
                    $response["error_msg"]   = "<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button> Informacion Actualizada Correctamente</div>";
                    echo json_encode($response); 
               }   
     
   }
   

	   	public function deleteDependencia(){
	      
         $url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
         $this->seguridad_model->SessionActivo($url);
		     $Dep	= json_decode($this->input->post('DepPost'));
		     $id        = base64_decode($Dep->Id);
         
         $response = array ( /*Array de response*/
            "estatus"   => false,
            "error_msg" => ""
         );
         //Bitácora
         $RegistrarBitacora = array(
            'fecha'           => date('Y-m-d H:i:s'),
            'usuario'         => $this->session->userdata('ID'),
            'mensaje'         => "Se Borró Dependencia con id: $id" );
	       $this->bitacoras_model->AgregarBitacoras($RegistrarBitacora);

         $this->dependencias_model->eliminarDep($id); 
         $response["error_msg"]   = "<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Nota eliminada correctamente Clave: <strong>".$id."</strong>, La Información de Actualizara en 5 Segundos <meta http-equiv='refresh' content='5'></div>";
         echo json_encode($response); //Retorna la representación JSON del valor dado     
		    
	     }

      //-------exportar----------//
      	public function exportar(){
      	 //Bitácora
      	$RegistrarBitacora = array(
      		    'fecha'             => date('Y-m-d H:i:s'),
      				'usuario'     		=> $this->session->userdata('ID'),
      				'mensaje'	    	=> "Exportó Datos de Nota en formato Excel" );	

        $this->bitacoras_model->AgregarBitacoras($RegistrarBitacora);
      			//-//
      	$this->load->helper('mysql_to_excel_helper');
        $this->load->model('areas_model');
      	to_excel($this->areas_model->get(), "NotasExcel");
      	}

 
}