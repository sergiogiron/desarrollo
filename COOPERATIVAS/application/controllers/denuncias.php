<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
date_default_timezone_set('America/Argentina/Jujuy');
class denuncias extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('seguridad_model');
		$this->load->model('denuncias_model'); //Carga el modelo denuncias
		$this->load->model('bitacoras_model');
        $this->load->model('cooperativas_model');
			
	// ----------exportar------------//
		
		$this->load->helper('mysql_to_excel_helper');
		
		
		//
	
		
	
	}

  
	public function index(){
          //Bitácora
          $RegistrarBitacora = array(
              'fecha'           => date('Y-m-d H:i:s'),
              'usuario'         => $this->session->userdata('ID'),
              'mensaje'         => "Ingresó a Denuncias" ); //Comunica que se ingresó a Denuncias
             
          $this->bitacoras_model->AgregarBitacoras($RegistrarBitacora);

          $url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
          $this->seguridad_model->SessionActivo($url);
          $this->load->view('constant');    //Carga vista títulos de Inicio
          $this->load->view('view_header'); //Carga vista del Menú Horizontal
          $data['denuncias'] = $this->denuncias_model->listarDenuncia(); //Obtengo todas las denuncias cargadas en la BD
          $this->load->view('denuncias/view_denuncias', $data); //Carga vista con Registros de todas las denuncias obtenidos
          $this->load->view('view_footer'); //Carga vista Pie de Página  
	   }

     /**
      * Crea una nueva denuncia
      * @return void
      */
  
     public function nuevo(){
          //Bitácora
          $RegistrarBitacora = array(
              'fecha'           => date('Y-m-d H:i:s'),
              'usuario'         => $this->session->userdata('ID'),
              'mensaje'         => "Ingresó a nueva Denuncia" );
             
          $this->bitacoras_model->AgregarBitacoras($RegistrarBitacora);

          $url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
          $this->seguridad_model->SessionActivo($url);
          $this->load->view('constant'); //Carga vista títulos de Inicio
          $this->load->view('view_header'); //Carga vista del Menú Horizontal
          $data['titulo'] = "Nueva Denuncia";
          $this->load->view('denuncias/view_nuevo_denuncias', $data); //Carga vista para registrar una Nueva Denuncia
          $this->load->view('view_footer'); //Carga vista Pie de Pagina
     }
     /**
      * Edita una Denuncia
      * @param int $id  identificador de la denuncia a editar 
      * @return void
      */
     public function editarDenuncias($id){

          $url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
          $this->seguridad_model->SessionActivo($url);
          $id =  base64_decode($id); //Encripta el id
           //Bitácora
          $RegistrarBitacora = array(
              'fecha'           => date('Y-m-d H:i:s'),
              'usuario'         => $this->session->userdata('ID'),
              'mensaje'         => "Editó Denuncia con id : $id" );
          $this->bitacoras_model->AgregarBitacoras($RegistrarBitacora);

          $this->load->view('constant');      //Carga vista titulos de Inicio 
          $this->load->view('view_header');   //Carga vista del Menú Principal
          $data['denuncia'] = $this->denuncias_model->buscarDenuncia($id); //Llama a la función que realiza la búsqueda
          $data['titulo'] = "Editar Denuncia";
          $this->load->view('denuncias/view_nuevo_denuncias', $data);     //Carga la vista para dar de alta una nueva denuncia.
          $this->load->view('view_footer');   //Carga vista Pie de Página
            
     }

     /**
      * Permite Guardar una nueva Denuncia
      * @return type
      */
     public function saveDenuncias(){
	 
	        $url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
          $this->seguridad_model->SessionActivo($url);
          $Denuncia         = json_decode($this->input->post('DenunciasPost')); //Obtiene los nuevos datos de una denuncia con JSON
          $response = array (
                    "estatus"   => false,
                    "campo"     => "",
                    "error_msg" => ""
          );
		   
		   if($Denuncia->Denunciante==""){  //Si no ingresa un reclamo, muestra un alerta de campo obligatorio.
               $response["campo"]       = "denunciante"; //Campo del formulario
               $response["error_msg"]   = "<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Debe ingresar el Denunciante</div>";
               echo json_encode($response); //Retorna la representación JSON del valor dado
        }else if($Denuncia->Reclamo==""){  //Si no ingresa un reclamo, muestra un alerta de campo obligatorio.
               $response["campo"]       = "reclamo"; //Campo del formulario
               $response["error_msg"]   = "<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Debe ingresar un reclamo</div>";
               echo json_encode($response); //Retorna la representación JSON del valor dado

		}else if($Denuncia->Cooperativa=="0"){  //Si no selecciona una Cooperativa, muestra un alerta de campo obligatorio.
                $response["campo"]      ="razonSocial";
                $response["error_msg"]  ="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Debe seleccionar una Cooperativa</div>";
                echo json_encode($response); //Retorna la representación JSON del valor dado
          }else if($Denuncia->Fecha==""){   //Si no ingresa una fecha, muestra un alerta de campo obligatorio.
                $response["campo"]      ="fecha";
                $response["error_msg"]  ="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Debe ingresar la Fecha del reclamo</div>";
                echo json_encode($response); //Retorna la representación JSON del valor dado
           
		  
		  
		  
          //LOS CAMPOS IMAGEN Y OBSERVACIONES NO SON OBLIGATORIOS!...

          /*}else if($Denuncia->Imagen==""){
                $response["campo"]  ="Imagen";
                $response["error_msg"]  ="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Debe adjuntar una imagen de la denuncia</div>";

          }*/
            }else if($Denuncia->Id==""){ //Si el id no existe, se REGISTRA una nueva denuncia
               
   


					$RegistraDenuncia   = array(
					'denunciante'      => $Denuncia->Denunciante,
                     'reclamo'           => $Denuncia->Reclamo,
                     'id_cooperativa'    => $Denuncia->Cooperativa,
                     'fecha_denuncia'    => $Denuncia->Fecha,
                   'documento'     => $Denuncia->Documento,
                     'observaciones'     => $Denuncia->Observaciones,
                      );
                      //Bitácora
                    $CADENA=print_r($RegistraDenuncia,true);
                    $RegistrarBitacora  = array(
                            'fecha'       => date('Y-m-d H:i:s'),
                            'usuario'     => $this->session->userdata('ID'),
                            'mensaje'     => "Alta de Denuncia: $CADENA");  
                    $this->bitacoras_model->AgregarBitacoras($RegistrarBitacora);
                    
                    $this->denuncias_model->saveDenuncia($RegistraDenuncia);
                    $response["error_msg"]   = "<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Informacion Guardada Correctamente</div>";
                    echo json_encode($response); //Retorna la representación JSON del valor dado
              }
               if($Denuncia->Id!=""){ //Si el id ya existe, se realiza la ACTUALIZACION de una denuncia
                    $UpdateDenuncia    = array(
					
					'denunciante'      => $Denuncia->Denunciante,
                    'reclamo'          => $Denuncia->Reclamo,
                    'id_cooperativa'   => $Denuncia->Cooperativa,
                    'fecha_denuncia'   => $Denuncia->Fecha,
                    'documento'     => $Denuncia->Documento,
                    'observaciones'    => $Denuncia->Observaciones,
                      );
                    //Bitácora
                    $CADENA=print_r($UpdateDenuncia,true);
                    $RegistrarBitacora     = array(
                        'fecha'            => date('Y-m-d H:i:s'),
                        'usuario'          => $this->session->userdata('ID'),
                        'mensaje'          => "Modificación de Denuncia: $CADENA"); 
                    $this->bitacoras_model->AgregarBitacoras($RegistrarBitacora);

                    $this->denuncias_model->updateDenuncia($UpdateDenuncia, $Denuncia->Id);
                    $response["error_msg"]   = "<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button> Informacion Actualizada Correctamente</div>";
                    echo json_encode($response); //Retorna la representación JSON del valor dado
               }
          }
     
	   /**
      * Elimina una denuncia
      * @return type
      */
	   	public function deleteDenuncias(){
	      
         $url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
         $this->seguridad_model->SessionActivo($url);
		     $Denuncia	= json_decode($this->input->post('DenunciasPost')); //Obtiene los nuevos datos de una Denuncia con JSON
		     $id        = base64_decode($Denuncia->Id);
         
         $response = array ( /*Array de response*/
            "estatus"   => false,
            "error_msg" => ""
         );
         //Bitácora
         $RegistrarBitacora = array(
            'fecha'           => date('Y-m-d H:i:s'),
            'usuario'         => $this->session->userdata('ID'),
            'mensaje'         => "Se Borró una Denuncia con id: $id" );
	       $this->bitacoras_model->AgregarBitacoras($RegistrarBitacora);

         $this->denuncias_model->eliminarDenuncia($id); //Llamada a la funcion que ejecuta la consulta de eliminación 
         $response["error_msg"]   = "<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Denuncia Eliminada Correctamente Clave: <strong>".$id."</strong>, La Información de Actualizara en 5 Segundos <meta http-equiv='refresh' content='5'></div>";
         echo json_encode($response); //Retorna la representación JSON del valor dado     
		    
	     }

//-------exportar----------//
	public function exportar()
	{
	      //Bitácora
		   $RegistrBitacoras = array(
		                'fecha'             => date('Y-m-d H:i:s'),
						'usuario'     		=> $this->session->userdata('ID'),
						'mensaje'	    	=> "Exporto Datos de Denuncias en formato Excel" );	

            $this->bitacoras_model->AgregarBitacoras($RegistrBitacoras);
			//-//
		$this->load->helper('mysql_to_excel_helper');
        $this->load->model('denuncias_model');
		to_excel($this->denuncias_model->get(), "DenunciasExcel");
	}
 
 //busca la cooperativa
 public function coope(){
		$local = $this->cooperativas_model->Coope();
		echo json_encode($local);
	}
 

 
 //========================imagien----------------------------
	public function view_img($id){
		$id 			   = base64_decode($id); 
		
		//Bitácora
		 
		  $RegistrBitacoras = array(
		  'fecha'             => date('Y-m-d H:i:s'),
		  'usuario'     		=> $this->session->userdata('ID'),
		  'mensaje'	    	=> "Ingreso a Sube Imagen de Socio  Codigo : $id");	
           $this->bitacoras_model->AgregarBitacoras($RegistrBitacoras);
			             //-//
		
		
		
		
		$data["id"]        = $id;
		$this->load->view('constant');
		$this->load->view('view_header');
		$this->load->view('denuncias/view_img',$data);
		$this->load->view('view_footer');
	}
	public function SubeImg(){
		$img1= "";
		$str = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
        for($i=0;$i<5;$i++) {
            $img1 .= substr($str,rand(0,62),1);
        }
		$idimg     = $this->input->post('id');
		$nombreimg = $idimg."_".$img1;
		$config['upload_path'] = realpath(APPPATH."../fotos");
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size'] = '1048576';
        $config['max_width'] = '900';
        $config['max_height'] = '900';
		$config['file_name']= $idimg."_".$img1;
		$this->load->library('upload', $config);
		if (!$this->upload->do_upload('file')) {
			echo  $this->upload->display_errors();
		}else{
			$file_info = $this->upload->data();
			$data      = array('upload_data'=>$this->upload->data());
			echo "Imagen Subido Correctamente.";
			$img = array("foto_denuncia"=>$nombreimg.$file_info["file_ext"]);
		//	$this->socios_model->GuardaImg($img);
		   $this->denuncias_model->updateDenuncia($img, $idimg);
		}
		}
		//Muestra Denuncia
	 public	function view_mostrar($id) {
	
    $id = base64_decode($id);
	//Bitácora
		 
		  $RegistrBitacoras = array(
		  'fecha'             => date('Y-m-d H:i:s'),
		  'usuario'     		=> $this->session->userdata('ID'),
		  'mensaje'	    	=> "Muestra Denuncia  id : $id");	
           $this->bitacoras_model->AgregarBitacoras($RegistrBitacoras);
			             //-//
	   	 $data = array(
            'title' => 'Denuncia',
           'imgdenuncia' => $this->denuncias_model->BuscarDenuncia($id),
		  
		      );
						
		//	$this->load->view('socios/ventanacodigodebarras_view', $datos);
			$this->load->view('denuncias/view_mostrar', $data);
		}
	
		
		
		
		
		
	
 
}