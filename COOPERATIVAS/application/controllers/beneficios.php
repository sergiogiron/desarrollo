<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
date_default_timezone_set('America/Argentina/Jujuy');
class beneficios extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('seguridad_model');
		$this->load->model('beneficios_model');
		$this->load->model('bitacoras_model');
		// ----------exportar------------//
		
		$this->load->helper('mysql_to_excel_helper');
		
		
		//
		
		
	}
	public function index(){
	      //Bitácora
		   $RegistrBitacoras = array(
		                'fecha'             => date('Y-m-d H:i:s'),
						'usuario'     		=> $this->session->userdata('ID'),
						'mensaje'	    	=> "Ingresó a Beneficios" );
				   
			$this->bitacoras_model->AgregarBitacoras($RegistrBitacoras);
			//-//
	
          $url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
          $this->seguridad_model->SessionActivo($url);
          /**/
          $this->load->view('constant');
          $this->load->view('view_header');
          $data['beneficios'] = $this->beneficios_model->ListarBeneficios();
          $this->load->view('beneficios/view_beneficios', $data);
          $this->load->view('view_footer');
          
	}
	public function deletebeneficios(){
	
         $url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
          $this->seguridad_model->SessionActivo($url);
		$Profe		= json_decode($this->input->post('BeneficiosPost'));
		$id             = base64_decode($Profe->Id);
	
	     //Bitácora
		   $RegistrBitacoras = array(
		                'fecha'             => date('Y-m-d H:i:s'),
						'usuario'     		=> $this->session->userdata('ID'),
						'mensaje'	    	=> "Se Borro Beneficios id : $id" );
				   
			$this->bitacoras_model->AgregarBitacoras($RegistrBitacoras);
			//-//
		
		/*Array de response*/
		 $response = array (
				"estatus"   => false,
	            "error_msg" => ""
	    );
		 $this->beneficios_model->EliminarBeneficios($id);
		 $response["error_msg"]   = "<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Beneficiarios Eliminado Correctamente Clave: <strong>".$id."</strong>, La Información de Actualizara en 5 Segundos <meta http-equiv='refresh' content='5'></div>";
		 echo json_encode($response);
	}	
     public function nuevo(){
          $url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
          $this->seguridad_model->SessionActivo($url);
          $this->load->view('constant');
          $this->load->view('view_header');
          $data['titulo'] = "Nuevo Beneficiarios";
          $this->load->view('beneficios/view_nuevo_beneficios', $data);
          $this->load->view('view_footer');
     }
     public function editarBeneficios($id){
          $url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
          $this->seguridad_model->SessionActivo($url);
          $id =  base64_decode($id);
          $this->load->view('constant');
          $this->load->view('view_header');
          $data['bene'] = $this->beneficios_model->BuscaBeneficios($id);
          $data['titulo'] = "Editar Beneficiorio";
          $this->load->view('beneficios/view_nuevo_beneficios', $data);
          $this->load->view('view_footer');
     }
     public function SaveBeneficios(){
        $url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
          $this->seguridad_model->SessionActivo($url);
          $Profesores           = json_decode($this->input->post('BeneficiosPost'));
          $response = array (
                    "estatus"   => false,
                    "campo"     => "",
                 "error_msg" => ""
         );
                  $Nombre = trim($Profesores->Nombre);
		 $Documento=trim($Profesores->Documento);
		 $Barrio =trim($Profesores->Barrio);
		 $Mza =trim($Profesores->Mza);
		 $Lote =trim($Profesores->Lote);
$ExisteDocumento   = $this->beneficios_model->ExisteDocumento($Documento);
					
					
					if($ExisteDocumento==true &&$Profesores->Id==""){
						$response["campo"]     = "Documento";
						$response["error_msg"]   = "<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>El Documento Existe</div>";
					echo json_encode($response);} else
         if($Nombre==""){
               $response["campo"]     = "Nombre";
               $response["error_msg"]   = "<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>El Nombre  es Obligatorio</div>";
               echo json_encode($response);
           }else if($Documento==""){
               $response["campo"]     = "Documento";
               $response["error_msg"]   = "<div class='alert alert-danger text-center' alert-dismissable><button type='button' class='close' data-dismiss='alert'>&times;</button>El Documento es obligatorio</div>";
               echo json_encode($response);
       }else if($Profesores->Localidad==""){
               $response["campo"]       = "Localidad";
               $response["error_msg"]   = "<div class='alert alert-danger text-center' alert-dismissable><button type='button' class='close' data-dismiss='alert'>&times;</button>La Localidad es obligatorio</div>";
               echo json_encode($response);
         }else if($Barrio==""){
               $response["campo"]       = "Barrio";
               $response["error_msg"]   = "<div class='alert alert-danger text-center' alert-dismissable><button type='button' class='close' data-dismiss='alert'>&times;</button>El Barrio es Obligatorio</div>";
               echo json_encode($response);
	 	  }else if($Mza==""){
               $response["campo"]       = "Mza";
               $response["error_msg"]   = "<div class='alert alert-danger text-center' alert-dismissable><button type='button' class='close' data-dismiss='alert'>&times;</button>Dato del Domicilio es Obligatorio</div>";
               echo json_encode($response); 
	 	 }else if($Lote==""){
               $response["campo"]       = "Lote";
               $response["error_msg"]   = "<div class='alert alert-danger text-center' alert-dismissable><button type='button' class='close' data-dismiss='alert'>&times;</button>Dato del domicilio es Obligatorio</div>";
               echo json_encode($response);   
         
  	   }else if($Profesores->Programa=="0"){
               $response["campo"]       = "Programa";
               $response["error_msg"]   = "<div class='alert alert-danger text-center' alert-dismissable><button type='button' class='close' data-dismiss='alert'>&times;</button>Programa es Obligatorio</div>";
               echo json_encode($response);  
          }else{
               if($Profesores->Id==""){
                     $RegistraProfesor    = array(
                     'nombre_beneficios'    => $Profesores->Nombre,
                     'documento'            => $Profesores->Documento,
                     'id_localidad'         => $Profesores->Localidad,
                     'barrio'               => $Profesores->Barrio,
				 	 'mza'                  => $Profesores->Mza,
                     'lote'                 => $Profesores->Lote,
                     'estado'               => $Profesores->Estado,
				     'id_programa'          => $Profesores->Programa,
                  	 'obs'                  => $Profesores->Obs  
              //       'fecha_registro'      => date('Y-m-j H:i:s')
                     );
					  //Bitácora
						 $CADENA=print_r($RegistraProfesor,true);
		                 $RegistrBitacoras = array(
		                'fecha'             => date('Y-m-d H:i:s'),
						'usuario'     		=> $this->session->userdata('ID'),
						'mensaje'	    	=> "Alta de Beneficios: $CADENA");	

                         $this->bitacoras_model->AgregarBitacoras($RegistrBitacoras);
			             //-//
                     $this->beneficios_model->SaveBene($RegistraProfesor);
                     $response["error_msg"]   = "<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Informacion Guardada Correctamente</div>";
                     echo json_encode($response);
               }
               if($Profesores->Id!=""){
                      $UpdateProfesor    = array(
                      'nombre_beneficios'    => $Profesores->Nombre,
                      'documento'            => $Profesores->Documento,
                      'id_localidad'         => $Profesores->Localidad,
                      'barrio'               => $Profesores->Barrio,
					  'mza'                  => $Profesores->Mza,
                      'lote'                 => $Profesores->Lote,
					  'estado'               => $Profesores->Estado,
					  'id_programa'          => $Profesores->Programa,
                  	  'obs'                  => $Profesores->Obs  

                   //  'fecha_registro'      => date('Y-m-j H:i:s')
                     );
                         //Bitácora
						 $CADENA=print_r($UpdateProfesor,true);
		                 $RegistrBitacoras = array(
		                'fecha'             => date('Y-m-d H:i:s'),
						'usuario'     		=> $this->session->userdata('ID'),
						'mensaje'	    	=> "Modificación de Beneficios: $CADENA");	

                         $this->bitacoras_model->AgregarBitacoras($RegistrBitacoras);
			             //-//
					 
                    $this->beneficios_model->UpdateBeneficios($UpdateProfesor, $Profesores->Id);
                    $response["error_msg"]   = "<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button> Informacion Actualizada Correctamente</div>";
                    echo json_encode($response);
               }
          }
     }
	 
	 public function localidades(){
		$local = $this->beneficios_model->Localidades();
		echo json_encode($local);
	}
	 
	 
	 
	 
	 
	 //-------exportar----------//
	public function exportar()
	{
	      //Bitácora
		   $RegistrBitacoras = array(
		                'fecha'             => date('Y-m-d H:i:s'),
						'usuario'     		=> $this->session->userdata('ID'),
						'mensaje'	    	=> "Exporto Datos de Beneficiarios en formato Excel" );	

            $this->bitacoras_model->AgregarBitacoras($RegistrBitacoras);
			//-//
		$this->load->helper('mysql_to_excel_helper');
        $this->load->model('beneficios_model');
		to_excel($this->beneficios_model->get(), "BeneficiosExcel");
	}
	 
	 
	 
	 public function controlaCalle()
	 {
		 $mza=$_POST['mza'];
		$lote= $_POST['lote'];
		$barrio =$_POST['barrio'];
		$localidad =$_POST['localidad'];
		 $local = $this->beneficios_model->controlaCalle($mza,$lote,$barrio,$localidad);
		echo json_encode($local);
		 
		 
	 } 
	 
	 
	 
	 
}