<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
date_default_timezone_set('America/Argentina/Jujuy');
class construccion extends CI_Controller {
	function __construct(){
		parent::__construct();
	
		$this->load->model('bitacoras_model');
	}
	public function index()
	{
	
			$this->load->view('constant');
			$this->load->view('view_header');
			$this->load->view('view_construccion');
			$this->load->view('view_footer');
			 //Bitácora
			 $ip=getenv("REMOTE_ADDR");
			  $RegistrBitacoras = array(
		      'fecha'             => date('Y-m-d H:i:s'),
			  'usuario'     		=> $this->session->userdata('ID'),
			   'mensaje'	    	=> "construccion  $ip");	
              $this->bitacoras_model->AgregarBitacoras($RegistrBitacoras);
            //-//
			
		
	}
	
}
