$(document).ready(function(){
	//alert(id_categoria);
	$('#codigo').focus();
	$('#subcategoria').prop('disabled', true);
	$('#stock').prop('disabled', true);
	$('#subcategoria').append("<option value='0'>Elige Sub-Categoria...</option>");
	//Cargamos Categorias
	var categoria = $("#categoria");
	categoria.append("<option value='0'>Cargando Categoria...</option>");
	$.getJSON(baseurl + "socios/categorias",function(objetosretorna){
		categoria.empty();
		categoria.append("<option value='0'>---Elige Categoria---</option>");
		$.each(objetosretorna, function(i,ObjetoReturn){
			var seleccion = "";
			if(id_categoria==ObjetoReturn.id){
				seleccion = "selected='selected'";
			}
			var nuevaFila = "<option value='"+ObjetoReturn.id+"' "+seleccion+">" + ObjetoReturn.descripcion+"</option>";
			categoria.append(nuevaFila);
		});
	});
	//Si es diferente a cero hacemos select 
	if(id_subcat!=0){
			$('#subcategoria').empty();
			$("#subcategoria").append("<option value='0'>Cargando Subcategoria...</option>");
			$.getJSON(baseurl + "socios/subcategorias",{filtro: id_categoria},function(objetosretorna){
				$("#subcategoria").empty();
				$("#subcategoria").append("<option value='0'>Elige Sub-Categoria...</option>");
				$.each(objetosretorna, function(i,subcategoria){
					var seleccion2 = "";
					if(id_subcat==subcategoria.id){
						seleccion2 = "selected='selected'";
					}
					$("#subcategoria").append("<option value='"+subcategoria.id+"' "+seleccion2+">" + subcategoria.descripcion+"</option>");
				});
			});
			$('#subcategoria').prop('disabled', false);
			$('#codigo').prop('disabled', true);
	}
	//Cargamos Profesor
	var profesor = $("#profesor");
	profesor.append("<option value='0'>---Elige Profesor---</option>");
	$.getJSON( baseurl + "socios/profesores",function(objetosretorna1){
		$.each(objetosretorna1, function(i,ObjetoReturn1){
			var seleccion3 = "";
			if(idprof==ObjetoReturn1.id){
				seleccion3 = "selected='selected'";
			}
			var nuevaFila = "<option value='"+ObjetoReturn1.id+"' "+seleccion3+">" + ObjetoReturn1.nombre_profesor+"</option>";
			profesor.append(nuevaFila);
		});
	});
	//Guardamos Formulario
	// Al hacer click en el bot�n para guardar
	$("form#formulario").submit(function()
	{
			var Socio 		    = new Object();
			Socio.Id 		    = $('input#id').val();
			Socio.Codigo        = $('input#codigo').val();
			Socio.Nombre        = $('textarea#nombre').val();
			Socio.Documento     = $('input#documento').val();
			Socio.Fechanac      = $('input#fechanac').val();
			Socio.Domicilio     = $('input#domicilio').val();
			Socio.Localidad     = $('input#localidad').val();
			Socio.Telefono      = $('input#telefono').val();
			Socio.Email         = $('input#email').val();
			Socio.Fechains      = $('input#fechains').val();
			Socio.Categoria     = $('select#categoria').val();
			Socio.SubCategoria  = $('select#subcategoria').val();
			Socio.Activo        = $('select#activo').val();
			Socio.Profesor     = $('select#profesor').val();
			Socio.Comentario     = $('input#comentario').val();
			
			
			$("#mensaje").append("<div class='modal1'><div class='center1'> <center> <img src='"+ baseurl +"/img/gif-load.gif'> Guardando Informacion...</center></div></div>");
			var DatosJson = JSON.stringify(Socio);
			$.post(baseurl + 'socios/GuardaSocios',
				{ 
					MiSocios: DatosJson
				},
				function(data, textStatus) {
					$("#"+data.campo+"").focus();
					$("#mensaje").html(data.error_msg);
				}, 
				"json"		
			);
			return false;
	});
	//
	$("#inventario").change(function(){
			var inventario = $('select#inventario').val();
			if(inventario=="1"){
				$('#stock').prop('disabled', false);
				$('#stock').focus();
			}else{
				$('#stock').prop('disabled', true);
			}
	});
	$("#categoria").change(function(){
			var categoria = $("#categoria").val();
			$('#subcategoria').empty();
			if(categoria==0){
				$('#subcategoria').append("<option value='0'>Elige Sub-Categoria...</option>");
				$('#subcategoria').prop('disabled', true);
			}else{
				$("#subcategoria").append("<option value='0'>Cargando Subcategoria...</option>");
				$.getJSON(baseurl + "socios/subcategorias",{filtro: $("#categoria").val()},function(objetosretorna){
					$("#subcategoria").empty();
					$("#subcategoria").append("<option value='0'>Elige Sub-Categoria...</option>");
					$.each(objetosretorna, function(i,subcategoria){
						$("#subcategoria").append("<option value='"+subcategoria.id+"'>" + subcategoria.descripcion+"</option>");
					});
				});
				$('#subcategoria').prop('disabled', false);
			}
			
		});

		/*var activo = $('select#activo').val();
		if(activo=="1"){
		    $('#activo').append("SI");
			//$('#comentario').prop('disabled', false);
		}else{
		//	$('#comentario').prop('disabled', false);
			$('#activo').append("NO");
		} */
});