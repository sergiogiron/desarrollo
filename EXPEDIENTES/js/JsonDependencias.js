$(document).ready(function(){
		
		$('#codigo').prop('disabled', true);
		
		$("form#formulario").submit(function()
		{
				var dep			= new Object();
				dep.Id          		= $('input#id').val(); //Obtiene el id ingresado
				dep.Codigo              = $('input#codigo').val();
		    	dep.Nombre			    = $('input#nombre').val();
				dep.Observaciones       = $('input#observaciones').val();
				 

		

				//Muestra proceso de guardado...
				$("#mensaje").append("<div class='modal1'><div class='center1'> <center> <img src='"+ baseurl +"/img/gif-load.gif'> Guardando Informacion...</center></div></div>");
				var DatosJson = JSON.stringify(dep); //Guardo los datos obtenidos como una cadena Json
				
				$.post(baseurl + 'dependencias/saveDependencia', //Envío los datos obtenidos al controlador
					{ 
						DepPost: DatosJson

					},
					function(data, textStatus) {
						$("#"+data.campo+"").focus();
						$("#mensaje").html(data.error_msg);
					}, 
					"json"		
				);
				return false;		
			
	});

});