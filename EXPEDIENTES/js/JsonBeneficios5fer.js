$(document).ready(function(){
		$("#Documento").focus();
	    $('#Estado').prop('disabled', true);
	
	
	//Cargamos Localidad
	var localidad = $("#localidad");
	localidad.append("<option value='0'>---Elige Localidad---</option>");
	$.getJSON( baseurl + "beneficios/localidades",function(objetosretorna1){
		$.each(objetosretorna1, function(i,ObjetoReturn1){
			var seleccion3 = "";
			if(idloca==ObjetoReturn1.id){
				seleccion3 = "selected='selected'";
			}
			var nuevaFila = "<option value='"+ObjetoReturn1.id+"' "+seleccion3+">" + ObjetoReturn1.nombre_localidad+"</option>";
			localidad.append(nuevaFila);
		});
	}); 
		
	
	//Cargamos Programa
	var programa = $("#programa");
	programa.append("<option value='0'>---Elige Programa---</option>");
	$.getJSON( baseurl + "cupos/programas",function(objetosretorna1){
		$.each(objetosretorna1, function(i,ObjetoReturn1){
			var seleccion3 = "";
			if(idprogra==ObjetoReturn1.id){
				seleccion3 = "selected='selected'";
			}
			var nuevaFila = "<option value='"+ObjetoReturn1.id+"' "+seleccion3+">" + ObjetoReturn1.nombre_programa+"</option>";
			programa.append(nuevaFila);
		});
	});	
		
		
	
		// Al hacer click en el botón para guardar
	$("form#formulario").submit(function()
	{
			
				var bene 		= new Object();
				bene.Id               = $('input#id').val();
				bene.Nombre           = $('input#Nombre').val();
				bene.Documento         = $('input#Documento').val();
				bene.Localidad      = $('select#localidad').val();
		    	bene.Barrio           = $('input#Barrio').val();
				bene.Mza              = $('input#Mza').val();
				bene.Lote             = $('input#Lote').val();
				bene.Programa           = $('select#programa').val();
				bene.Estado           = $('select#Estado').val();
				bene.Obs               = $('input#Obs').val();
				
				$("#mensaje").append("<div class='modal1'><div class='center1'> <center> <img src='"+ baseurl +"/img/gif-load.gif'> Guardando Informacion...</center></div></div>");
				var DatosJson = JSON.stringify(bene);
				$.post(baseurl + 'beneficios/SaveBeneficios',
					{ 
						BeneficiosPost: DatosJson
					},
					function(data, textStatus) {
						$("#"+data.campo+"").focus();
						$("#mensaje").html(data.error_msg);
					}, 
					"json"		
				);
				return false;
			
			
	});

});