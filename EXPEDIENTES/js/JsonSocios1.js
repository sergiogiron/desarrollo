$(document).ready(function(){
		$("#nombre").focus();
		
	//Cargamos Localidad
	var localidad = $("#localidad");
	localidad.append("<option value='0'>---Elige Localidad---</option>");
	$.getJSON( baseurl + "cooperativas/localidades",function(objetosretorna1){
		$.each(objetosretorna1, function(i,ObjetoReturn1){
			var seleccion3 = "";
			if(idloca==ObjetoReturn1.id){
				seleccion3 = "selected='selected'";
			}
			var nuevaFila = "<option value='"+ObjetoReturn1.id+"' "+seleccion3+">" + ObjetoReturn1.nombre_localidad+"</option>";
			localidad.append(nuevaFila);
		});
	}); 	
		
		//Cargamos Coopertativa 
	var cooperativa = $("#cooperativa");
	cooperativa.append("<option value='0'>---Elige Cooperativa---</option>");
	$.getJSON( baseurl + "cooperativas/coope",function(objetosretorna1){
		$.each(objetosretorna1, function(i,ObjetoReturn1){
			var seleccion3 = "";
			if(idcoope==ObjetoReturn1.id){
				seleccion3 = "selected='selected'";
			}
			var nuevaFila = "<option value='"+ObjetoReturn1.id+"' "+seleccion3+">" + ObjetoReturn1.razonsocial+"</option>";
			cooperativa.append(nuevaFila);
		});
	}); 
		
	
		
		// Al hacer click en el botón para guardar
	$("form#formulario").submit(function()
	{
			
				var socios 		= new Object();
				socios.Id            = $('input#id').val();
				socios.Nombre        = $('input#nombre').val();
				socios.Documento     = $('input#documento').val();
				socios.Cuit          = $('input#cuit').val();
				socios.Fechanac      = $('input#fechanac').val();
				socios.Domicilio     = $('input#domicilio').val();
				socios.Nro           = $('input#nro').val();
				socios.Manzana       = $('input#manzana').val();
				socios.Lote          = $('input#lote').val();
				socios.Barrio        = $('input#barrio').val();
				socios.Localidad     = $('select#localidad').val();
				socios.Id_provincia  = $('select#id_provincia').val();
				socios.Telefono      = $('input#telefono').val();
				socios.Cargo	     = $('select#cargo').val();
				socios.Cooperativa   = $('select#cooperativa').val();
			    socios.Tipo          = $('select#tipo').val();
				socios.Plan          = $('select#plan').val();
				
		
				$("#mensaje").append("<div class='modal1'><div class='center1'> <center> <img src='"+ baseurl +"/img/gif-load.gif'> Guardando Informacion...</center></div></div>");
				var DatosJson = JSON.stringify(socios);
				$.post(baseurl + 'socios/SaveSocio',
					{ 
						SociosPost: DatosJson
					},
					function(data, textStatus) {
						$("#"+data.campo+"").focus();
						$("#mensaje").html(data.error_msg);
					}, 
					"json"		
				);
				return false;
			
			
	});

});