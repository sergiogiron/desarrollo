$(document).ready(function(){
		$("#Documento").focus();
		$('#Estado').prop('disabled', true);
		// Al hacer click en el botón para guardar

		//Cargamos Localidad
		
	var localidad = $("#localidad");
	localidad.append("<option value='0'>---Elige Localidad---</option>");
	$.getJSON( baseurl + "inscripciones/localidades",function(objetosretorna1){
		$.each(objetosretorna1, function(i,ObjetoReturn1){
			var seleccion3 = "";
			if(idloca==ObjetoReturn1.id){
				seleccion3 = "selected='selected'";
			}
			var nuevaFila = "<option value='"+ObjetoReturn1.id+"' "+seleccion3+">" + ObjetoReturn1.nombre_localidad+"</option>";
			localidad.append(nuevaFila);
		});
	});
//Cargamos Programa
	var programa = $("#programa");
	programa.append("<option value='0'>---Elige Programa---</option>");
	$.getJSON( baseurl + "cupos/programas",function(objetosretorna1){
		$.each(objetosretorna1, function(i,ObjetoReturn1){
			var seleccion3 = "";
			if(idprogra==ObjetoReturn1.id){
				seleccion3 = "selected='selected'";
			}
			var nuevaFila = "<option value='"+ObjetoReturn1.id+"' "+seleccion3+">" + ObjetoReturn1.nombre_programa+"</option>";
			programa.append(nuevaFila);
		});
	});	
		
		




	$("form#formulario").submit(function()
	{
				var insc 			  = new Object();
				insc.Id               = $('input#id').val(); //Coincide con id de view/nuevo
				insc.Documento        = $('input#Documento').val();
				insc.Nombre           = $('input#Nombre').val();
				insc.Localidad        = $('select#localidad').val();
		    	insc.Barrio           = $('input#Barrio').val();
				insc.Mza              = $('input#Mza').val();
				insc.Lote             = $('input#Lote').val();
		    	insc.Programa         = $('select#programa').val();
				insc.Estado           = $('select#Estado').val();
				insc.Obs              = $('input#Obs').val();
				
				$("#mensaje").append("<div class='modal1'><div class='center1'> <center> <img src='"+ baseurl +"/img/gif-load.gif'> Guardando Informacion...</center></div></div>");
				var DatosJson = JSON.stringify(insc);
				$.post(baseurl + 'inscripciones/saveInscripciones',
					{ 
						InscripcionesPost: DatosJson
					},
					function(data, textStatus) {
						$("#"+data.campo+"").focus();
						$("#mensaje").html(data.error_msg);
					}, 
					"json"		
				);
				return false;
			
			
	});

});