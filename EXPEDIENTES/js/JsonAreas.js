$(document).ready(function(){
		
		$('#codigo').prop('disabled', true);
		
		$("form#formulario").submit(function()
		{
				var area			= new Object(); //Crea el objeto area
				area.Id          		= $('input#id').val(); //Obtiene el id ingresado
				area.Codigo             = $('input#codigo').val();
		    	area.Nombre			    = $('input#nombre').val();
				area.Observaciones      = $('input#observaciones').val();
				 

				//Muestra proceso de guardado...
				$("#mensaje").append("<div class='modal1'><div class='center1'> <center> <img src='"+ baseurl +"/img/gif-load.gif'> Guardando Informacion...</center></div></div>");
				var DatosJson = JSON.stringify(area); //Guardo los datos obtenidos como una cadena Json
				
				$.post(baseurl + 'areas/saveArea', //Envío los datos obtenidos al controlador
					{ 
						AreaPost: DatosJson
					},
					function(data, textStatus) {
						$("#"+data.campo+"").focus();
						$("#mensaje").html(data.error_msg);
					}, 
					"json"		
				);
				return false;		
			
	});

});