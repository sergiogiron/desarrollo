$(document).ready(function(){
		$("#nombre").focus();
		//Carga Areas
		var area = $("#area");
		area.append("<option value='0'>---Eligir Opcion---</option>");
		$.getJSON( baseurl + "usuarios/areas",function(objetosretorna1){
			$.each(objetosretorna1, function(i,ObjetoReturn1){
				var seleccion3 = "";
				if(idarea==ObjetoReturn1.id_area){
					seleccion3 = "selected='selected'";
				}
				var nuevaFila = "<option value='"+ObjetoReturn1.id_area+"' "+seleccion3+">" + ObjetoReturn1.nombre_area+"</option>";
				area.append(nuevaFila);
			});
		});

		// Al hacer click en el botón para guardar
	$("form#formulario").submit(function()
	{
			
				var Usu		= new Object();
				Usu.Id            = $('input#id').val();
			    Usu.Nombre        = $('input#nombre').val();
		    	Usu.Telefono	  = $('input#telefono').val();
				Usu.Email 		  = $('input#email').val();
				Usu.TipoU          = $('select#tipou').val();
				Usu.Password1 	  = $('input#password1').val();
				Usu.Password2 	  = $('input#password2').val();
				Usu.Area 		  = $('select#area').val();
				Usu.Estatus       = $('select#estatus').val();
				Usu.Privilegios   = $('select#privilegios').val();
				
				$("#mensaje").append("<div class='modal1'><div class='center1'> <center> <img src='"+ baseurl +"/img/gif-load.gif'> Guardando Informacion...</center></div></div>");
				var DatosJson = JSON.stringify(Usu);
				$.post(baseurl + 'usuarios/GUsuario',
					{ 
						UsuariosPost: DatosJson
					},
					function(data, textStatus) {
						$("#"+data.campo+"").focus();
						$("#mensaje").html(data.error_msg);
					}, 
					"json"		
				);
				return false;
			
			
	});

});