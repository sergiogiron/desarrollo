$(document).ready(function(){
		$("#codigo").focus();
		$("form#formulario").submit(function()
		{
				var resolucion			= new Object(); //Crea el objeto resolucion
				resolucion.Id          		= $('input#id').val(); //Obtiene el id ingresado
				resolucion.Codigo           = $('input#codigo').val();
				resolucion.Año	            = $('input#año').val();
				resolucion.Asunto     	    = $('input#asunto').val();
		    	resolucion.Gobierno    	    = $('select#gobierno').val();
		    	//resolucion.Imagen			= $('input#imagen').val();		 	//Obtiene imagen de una denuncia
				resolucion.Observaciones    = $('input#observaciones').val();
				 

				//Muestra proceso de guardado...
				$("#mensaje").append("<div class='modal1'><div class='center1'> <center> <img src='"+ baseurl +"/img/gif-load.gif'> Guardando Informacion...</center></div></div>");
				var DatosJson = JSON.stringify(resolucion); //Guardo los datos obtenidos como una cadena Json
				
				$.post(baseurl + 'resolucion/saveResolucion', //Envío los datos obtenidos al controlador
					{ 
						ResolucionPost: DatosJson
					},
					function(data, textStatus) {
						$("#"+data.campo+"").focus();
						$("#mensaje").html(data.error_msg);
					}, 
					"json"		
				);
				return false;		
			
	});

});