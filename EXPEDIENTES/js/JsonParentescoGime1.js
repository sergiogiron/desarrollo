$(document).ready(function(){

 $("#buscaSocio").autocomplete({
	  source: function(request, response) {
        $.ajax({
          url: baseurl + 'parentesco/buscarSocio',
               dataType: "json",
          data: {
              buscaSocio: $('input#buscaSocio').val(),
          },
          success: function(data) {
            response(data);
           // alert(data);
          }
        });
		 minLength: 2
      },
             select: function(event, ui) {  
				//$('#lblNombrecliente').text(ui.item.nombre +' '+ ui.item.documento);
				$('#lblNombrecliente').text(ui.item.id );
				//$('#lblcoop').text(ui.item.id_cooperativa);
				//$('input#socio').text(ui.item.id_socio);
				$('input#socio').val(ui.item.id );  
           }  
    })


	$("#documento").focus();

	//Cargamos Localidad
	var localidad = $("#localidad");
	localidad.append("<option value='0'>---Elige Localidad---</option>");
	$.getJSON( baseurl + "cooperativas/localidades",function(objetosretorna1){
		$.each(objetosretorna1, function(i,ObjetoReturn1){
			var seleccion3 = "";
			if(idloca==ObjetoReturn1.id){
				seleccion3 = "selected='selected'";
			}
			var nuevaFila = "<option value='"+ObjetoReturn1.id+"' "+seleccion3+">" + ObjetoReturn1.nombre_localidad+"</option>";
			localidad.append(nuevaFila);
		});
	}); 	
		
		//Cargamos Coopertativa 
	var cooperativa = $("#cooperativa");
	cooperativa.append("<option value='0'>---Elige Cooperativa---</option>");
	$.getJSON( baseurl + "cooperativas/coope",function(objetosretorna1){
		$.each(objetosretorna1, function(i,ObjetoReturn1){
			var seleccion3 = "";
			if(idcoope==ObjetoReturn1.id){
				seleccion3 = "selected='selected'";
			}
			var nuevaFila = "<option value='"+ObjetoReturn1.id+"' "+seleccion3+">" + ObjetoReturn1.razonsocial+"</option>";
			cooperativa.append(nuevaFila);
		});
	}); 
		
		
	
	// Al hacer click en el botón para guardar
	$("form#formulario").submit(function() 
	{ 

		//alert($('input#id').val());
		//alert($('input#id').val();
	
				var socios 		= new Object();
				socios.Id            = $('input#id').val();
				socios.Documento     = $('input#documento').val();
				socios.Socio       = $('input#socio').val();
				socios.Cooperativa   = $('select#cooperativa').val();
				socios.Parentesco    = $('select#parentesco').val();
				socios.Cuit          = $('input#cuit').val();
				socios.Nombre        = $('input#nombre').val();
				socios.Fechanac      = $('input#fechanac').val();
				socios.Sexo			 = $('select#sexo').val();
				socios.EstadoCivil   = $('select#estadoCivil').val();
				socios.CaractCelular = $('input#caractCelular').val();
				socios.Celular       = $('input#celular').val();
				socios.Email		 = $('input#email').val();
				socios.FechaIngreso  = $('input#fechaIngreso').val(); 
				socios.SitRevista    = $('select#sitRevista').val();
				socios.FechaVenc	 = $('input#fechaVenc').val();
				socios.Localidad     = $('select#localidad').val();
				socios.Domicilio     = $('input#domicilio').val();
				socios.Numero		 = $('input#nro').val();
				socios.Piso          = $('input#piso').val();
				socios.Dpto          = $('input#dpto').val();
				socios.Bloque        = $('input#bloque').val();
				socios.Telefono      = $('input#telefono').val();
				
				$("#mensaje").append("<div class='modal1'><div class='center1'> <center> <img src='"+ baseurl +"/img/gif-load.gif'> Guardando Informacion...</center></div></div>");
				var DatosJson = JSON.stringify(socios);
				$.post(baseurl + 'parentesco/SaveSocio',
					{ 
						SociosPost: DatosJson
					},
					function(data, textStatus) {
						$("#"+data.campo+"").focus();
						$("#mensaje").html(data.error_msg);
					}, 
					"json"		
				);
				return false;	
	});

});
