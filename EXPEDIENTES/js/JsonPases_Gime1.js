$(document).ready(function(){
		
		$('#codigo').prop('disabled', true);
		$('#fecha').prop('disabled',true);
		//$('#anio').prop('disabled',true);
		$("#asunto").prop('disabled', true);

		var area = $("#area");
		area.append("<option value='0'>---Eligir Opcion---</option>");
		$.getJSON( baseurl + "pases/areas",function(objetosretorna1){
			$.each(objetosretorna1, function(i,ObjetoReturn1){
				var seleccion3 = "";
				if(idarea==ObjetoReturn1.id){
					seleccion3 = "selected='selected'";
				}
				var nuevaFila = "<option value='"+ObjetoReturn1.id+"' "+seleccion3+">" + ObjetoReturn1.nombre_area+"</option>";
				area.append(nuevaFila);
			});
		});  


		$("form#formulario").submit(function()
		{
				var pase			= new Object();
				pase.Id          		= $('input#id').val();
				pase.Codigo             = $('input#codigo').val();
				pase.Fecha    	        = $('input#fecha').val();
				//exp.Remitente     	    = $('input#remitente').val();
				//exp.Celular		    = $('input#celular').val();
				pase.Asunto   	        = $('textarea#asunto').val();
		    	pase.Area               = $('select#area').val();
		    	pase.Estado			    = $('select#estado').val();
				pase.Observaciones      = $('input#observaciones').val();
				 

				//Muestra proceso de guardado...
				$("#mensaje").append("<div class='modal1'><div class='center1'> <center> <img src='"+ baseurl +"/img/gif-load.gif'> Guardando Informacion...</center></div></div>");
				var DatosJson = JSON.stringify(pase); //Guardo los datos obtenidos como una cadena Json
				
				$.post(baseurl + 'pases/savePase', //Envío los datos obtenidos al controlador
					{ 
						PasePost: DatosJson
					},
					function(data, textStatus) {
						$("#"+data.campo+"").focus();
						$("#mensaje").html(data.error_msg);
					}, 
					"json"		
				);
				return false;		
			
	});

});