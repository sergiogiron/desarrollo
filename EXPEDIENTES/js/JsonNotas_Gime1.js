$(document).ready(function(){
		$("#codigo").focus();
		$("form#formulario").submit(function()
		{
				var nota			= new Object(); //Crea el objeto nota
				nota.Id          		= $('input#id').val(); //Obtiene el id ingresado
				nota.Codigo             = $('input#codigo').val();
				nota.Año	            = $('input#anio').val();
				nota.Fecha    	        = $('input#fecha').val();
				nota.Remitente     	    = $('input#remitente').val();
				nota.Celular		    = $('input#celular').val();
				nota.Motivo    	        = $('input#motivo').val();
		    	nota.Tipo   	        = $('select#tipo').val();
		    	nota.Estado			    = $('select#estado').val();
				nota.Observaciones      = $('input#observaciones').val();
				 

				//Muestra proceso de guardado...
				$("#mensaje").append("<div class='modal1'><div class='center1'> <center> <img src='"+ baseurl +"/img/gif-load.gif'> Guardando Informacion...</center></div></div>");
				var DatosJson = JSON.stringify(nota); //Guardo los datos obtenidos como una cadena Json
				
				$.post(baseurl + 'notas/saveNota', //Envío los datos obtenidos al controlador
					{ 
						NotaPost: DatosJson
					},
					function(data, textStatus) {
						$("#"+data.campo+"").focus();
						$("#mensaje").html(data.error_msg);
					}, 
					"json"		
				);
				return false;		
			
	});

});