$(document).ready(function(){
	$("#localidad").focus();
	// Al hacer click en el botón para guardar...
	$("form#formulario").submit(function(){		
				var localidad 		     = new Object();        	//Se crea el objeto localidad
				localidad.Id             = $('input#id').val();    //Obtiene el id ingresado
				localidad.Localidad      = $('input#localidad').val();     //Obtiene la localidad seleccionada
				localidad.Departamento   = $('select#departamento').val(); //Obtiene el departamento de una localidads eleccionada
				localidad.Estado         = $('select#estado').val();	  //Obtiene el estado de una localidad seleccionada
				localidad.Poblacion      = $('input#poblacion').val();	 //Obtiene la poblacion de una localidad seleccionada
				
				$("#mensaje").append("<div class='modal1'><div class='center1'> <center> <img src='"+ baseurl +"/img/gif-load.gif'> Guardando Informacion...</center></div></div>");
				var DatosJson = JSON.stringify(localidad);
				$.post(baseurl + 'localidades/saveLocalidades', //Funcion del controlador
					{ 
						LocalidadesPost: DatosJson
					},
					function(data, textStatus) {
						$("#"+data.campo+"").focus();
						$("#mensaje").html(data.error_msg);
					}, 
					"json"		
				);
				return false;	
			
	});

});