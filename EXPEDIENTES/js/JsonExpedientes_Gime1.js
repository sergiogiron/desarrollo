$(document).ready(function(){
		
		$('#codigo').prop('disabled', true);
		$('#fecha').prop('disabled',true);
		//$('#anio').prop('disabled',true);
		//$("#dependencia").focus();

		//Cargamos Dependencias
		var dependencia = $("#dependencia");
		dependencia.append("<option value='0'>---Eligir Opcion---</option>");
		$.getJSON( baseurl + "expedientes/dependencias",function(objetosretorna1){
			$.each(objetosretorna1, function(i,ObjetoReturn1){
				var seleccion3 = "";
				if(iddep==ObjetoReturn1.id){
					seleccion3 = "selected='selected'";
				}
				var nuevaFila = "<option value='"+ObjetoReturn1.id+"' "+seleccion3+">" + ObjetoReturn1.nombre_dep+"</option>";
				dependencia.append(nuevaFila);
			});
		});

		
		var area = $("#area");
		area.append("<option value='0'>---Eligir Opcion---</option>");
		$.getJSON( baseurl + "expedientes/areas",function(objetosretorna1){
			$.each(objetosretorna1, function(i,ObjetoReturn1){
				var seleccion3 = "";
				if(idarea==ObjetoReturn1.id){
					seleccion3 = "selected='selected'";
				}
				var nuevaFila = "<option value='"+ObjetoReturn1.id+"' "+seleccion3+">" + ObjetoReturn1.nombre_area+"</option>";
				area.append(nuevaFila);
			});
		});  


		$("form#formulario").submit(function()
		{
				var exp			= new Object();
				exp.Id          		= $('input#id').val();
				exp.Codigo              = $('input#codigo').val();
				exp.Fecha    	        = $('input#fecha').val();
				exp.Dependencia         = $('select#dependencia').val();
				exp.Remitente     	    = $('input#remitente').val();
				//exp.Celular		    = $('input#celular').val();
				exp.Asunto   	        = $('textarea#asunto').val();
		    	exp.Area                = $('select#area').val();
		    	exp.Estado			    = $('select#estado').val();
				exp.Observaciones       = $('input#observaciones').val();
				 
alert(exp.Area);
alert(exp.Dependencia);


				//Muestra proceso de guardado...
				$("#mensaje").append("<div class='modal1'><div class='center1'> <center> <img src='"+ baseurl +"/img/gif-load.gif'> Guardando Informacion...</center></div></div>");
				var DatosJson = JSON.stringify(exp); //Guardo los datos obtenidos como una cadena Json
				
				$.post(baseurl + 'expedientes/saveExpediente', //Envío los datos obtenidos al controlador
					{ 
						ExpPost: DatosJson
					},
					function(data, textStatus) {
						$("#"+data.campo+"").focus();
						$("#mensaje").html(data.error_msg);
					}, 
					"json"		
				);
				return false;		
			
	});

});