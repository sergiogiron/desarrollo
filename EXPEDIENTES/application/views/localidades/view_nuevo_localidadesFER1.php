<input type="hidden" value="<?php echo @$localidad[0]->id; ?>" id="id" name="id"> 
<script type="text/javascript">
  var baseurl = "<?php echo base_url(); ?>";
  /**
   * Función que redirecciona la vista a localidades
   * @return type
   */
  function regresar(){
    window.location="<?php echo base_url()?>localidades";
  }
  
</script>
<?php

  $Localidad       = array(
  'name'        => 'localidad',
  'id'          => 'localidad',
  'size'        => 50,
  'value'       => set_value('localidad',@$localidad[0]->nombre_localidad),
  'type'        => 'text',
  'class'       => 'form-control',
 
  );
  
  /*$departamento    = array(
  'name'        => 'departamento',
  'id'          => 'departamento',
  'size'        => 50,
  'value'       => set_value('departamento',@$localidad[0]->departamento),
  'type'        => 'text',
  'class'       => 'form-control',*/
  $departamento =array(
    '0'         =>'---Seleccione---',
    '1'         =>'Cochinoca',
    '2'         =>'Dr. Manuel Belgrano',
    '3'         =>'El Carmen',
    '4'         =>'Humahuaca',
    '5'         =>'Ledesma',
    '6'         =>'Palpalá',
    '7'         =>'Rinconada',
    '8'         =>'San Antonio',
    '9'         =>'San Pedro',
    '10'        =>'Santa Bárbara',
    '11'        =>'Santa Catalina',
    '12'        =>'Susques',
    '13'        =>'Tilcara',
    '14'        =>'Tumbaya',
    '15'        =>'Valle Grande',
    '16'        =>'Yavi',
  );
  /*$estado     = array(
  'name'        => 'estado',
  'id'          => 'estado',
  'size'        => 50,
  'value'       => set_value('estado',@$localidad[0]->estado),
  'type'        => 'text',
  'class'       => 'form-control',

  );*/
  
  $estado  = array(
  '0'             => '---Elegir Opción---',
  '1'             => 'Urbana',
  '2'             => 'Rural',
  );
  
   $poblacion           = array(
  'name'        => 'poblacion',
  'id'          => 'poblacion',
  'size'        => 50,
  'value'       => set_value('poblacion',@$localidad[0]->poblacion),
  'type'        => 'text',
  'class'       => 'form-control',
  
  );
 
?>
<script src="<?php echo base_url();?>js/JsonLocalidades.js"></script>
<h1 class="page-header"><span class="glyphicon glyphicon-th-list"></span> <?php echo $titulo; ?></h1>
<div id="mensaje"></div>
<form class="form-horizontal" name="formulario" id="formulario" role="form">
  <input type="hidden" value="0" id="validamail" name="validamail">
  <input type="hidden" value="0" id="validarfc" name="validarfc">
  
  <div class="form-group">
    <label for="localidad" class="col-lg-3 control-label">Localidad:</label>
    <div class="col-lg-3">
      <?php echo form_input($Localidad); ?>
    </div>
  </div>
  

  <div class="form-group">
    <label for="departamento" class="col-lg-3 control-label">Departamento:</label>
    <div class="col-lg-3">
      <!--<?php echo form_input($departamento); ?>-->
      <?php echo  form_dropdown('departamento', $departamento, set_value('departamento',@$localidad[0]->departamento),'class="form-control" id="departamento"'); ?>
    </div>
  </div>

  <div class="form-group">
    <label for="estado" class="col-lg-3 control-label">Estado:</label>
    <div class="col-lg-3">
      <!--<?php echo form_input($estado); ?>-->
      <?php echo  form_dropdown('estado', $estado, set_value('estado',@$localidad[0]->estado),'class="form-control" id="estado"'); ?>
    </div>
  </div>

 <div class="form-group">
    <label for="poblacion" class="col-lg-3 control-label">Poblacion:</label>
    <div class="col-lg-3">
      <?php echo form_input($poblacion); ?>
    </div>
  </div>

  <div class="form-group">
    <div class="col-lg-offset-3 col-lg-10">
      <button type="button" onclick="regresar()" class="btn btn-primary"><span class="glyphicon glyphicon-circle-arrow-left"></span> Regresar</button>
      <button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-saved"></span> Guardar </button>
      <?php if($titulo=="Nueva Localidad"){ ?>
      <button type="reset" class="btn btn-primary"><span class="glyphicon glyphicon-plus"></span>Nuevo</button>
      <?php } ?>
    </div>
  </div>

  <hr/>
</form>		
<script type="text/javascript">
  
</script>
