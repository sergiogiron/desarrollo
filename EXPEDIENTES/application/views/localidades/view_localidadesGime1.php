<style type="text/css">
th, td { white-space: nowrap; }
    div.dataTables_wrapper {
        width: 100%;
        margin: 0 auto;
    }
</style>
<script type="text/javascript">
var baseurl = "<?php echo base_url(); ?>";
var currentLocation = window.location;
function Eliminarlocalidad(localidad, id){
    confirmar=confirm("Eliminar a " + localidad + ", Recuerda Una vez Eliminado No podras Recuperarlo"); 

    if (confirmar){
    	document.getElementById('mensaje').innerHTML = "<div class='modal1'><div class='center1'> <center> <img src='"+ baseurl +"/img/gif-load.gif'> Eliminando Localidad...</center></div></div>";
    	 var loca		 = new Object();
		loca.Id      	 = id;
		
		var DatosJson = JSON.stringify(loca);
		$.post(currentLocation + '/deletelocalidad',
		{ 
			LocalidadesPost: DatosJson
		},
		function(data, textStatus) {
			//
			$("#mensaje").html(data.error_msg);
		}, 
		"json"		
		);
    } else{
    } 
  }
  
</script>
<h1 class="page-header"><span class="glyphicon glyphicon-list-alt"></span> Localidades</h1>
<div id="mensaje"></div>
<p align="right">
 	 <a href="localidades/nuevo">
 	 	<button type="button" class="btn btn-primary"><span class="glyphicon glyphicon-ok"></span> Nuevo Localidad</button>
 	 </a>  
 	 </p>
 	 <br/>
	<table id="localidades" border="0" cellpadding="0" cellspacing="0" width="100%" class="pretty">
		<thead>
			<tr>
				<th></th>
				<th>Localidad</th>
				<th>Departamento</th>
				<th>Estado</th>
				<th>Poblacion</th>
			</tr>
		</thead>
		<tbody>
			<?php
				if($localidades){
					foreach($localidades as $loca){
						$idLoca     = base64_encode($loca->id);
						echo '<tr>';
						echo '<td>';
						echo '<a href="localidades/editarLocalidad/'.$idLoca.'"><button type="button" title="Editar profesor" class="btn btn-success btn-xs"><span class="glyphicon glyphicon-edit"></span></button></a> &nbsp;';
						?>
						<button type="button" onclick="Eliminarlocalidad('<?php echo $loca->nombre_localidad; ?>','<?php echo $idLoca; ?>');" title="Eliminar Localidad" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span></button>
						<?php
						echo '</td>';
						echo '<td>'.$loca->nombre_localidad.'</td>';
						echo '<td>'.$loca->departamento.'</td>';
						echo '<td>'.$loca->estado.'</td>';
						echo '<td>'.$loca->poblacion.'</td>';
						echo '</tr>';
					}
				}else{
					echo '<tr><td colspan=5><center>No Existe Informacion</center></td></tr>';
				}
			?>
		</tbody>
	</table>
<script type="text/javascript">

            $(document).ready(function() {
    $('#localidades').dataTable( {
        "scrollX": false
    } );
} );

</script>
			