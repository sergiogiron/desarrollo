<input type="hidden" value="<?php echo @$programa[0]->id; ?>" id="id" name="id"> 
<script type="text/javascript">
  var baseurl = "<?php echo base_url(); ?>";
 /**
  * Función que redirecciona la vista a programas
  * @return type
  */
  function regresar(){
    window.location="<?php echo base_url()?>programas";
  }
</script>

<?php
  $Nombre       = array(
  'name'        => 'Nombre',
  'id'          => 'Nombre',
  'size'        => 50,
  'value'       => set_value('Nombre',@$programa[0]->nombre_programa),
  'type'        => 'text',
  'class'       => 'form-control',
   );

  $Tipo  = array(
  '0'             => '---Elegir Opción---',
  '1'             => 'Nacional',
  '2'             => 'Provincial',
  '3'             => 'Municipal',
  );
  
  $Cantidad     = array(
  'name'        => 'Cantidad',
  'id'          => 'Cantidad',
  'size'        => 50,
  'value'       => set_value('Cantidad',@$programa[0]->cantidad),
  'type'        => 'text',
  'class'       => 'form-control',
   );
   $Restante     = array(
  'name'        => 'Restante',
  'id'          => 'Restante',
  'size'        => 50,
  'value'       => set_value('Restante',@$programa[0]->restantes),
  'type'        => 'text',
  'class'       => 'form-control',
   );
   
?>

<script src="<?php echo base_url();?>js/JsonProgramas.js"></script>
<h1 class="page-header"><span class="glyphicon glyphicon-th-list"></span> <?php echo $titulo; ?></h1>
<div id="mensaje"></div>
<form class="form-horizontal" name="formulario" id="formulario" role="form">
  

  <div class="form-group">
    <label for="Nombre" class="col-lg-3 control-label">Nombre:</label>
    <div class="col-lg-3">
      <?php echo form_input($Nombre); ?>
    </div>
  </div>
  
  <div class="form-group">
    <label for="Tipo" class="col-lg-3 control-label">Tipo:</label>
    <div class="col-lg-3">
	   <?php echo  form_dropdown('tipo', $Tipo, set_value('tipo',@$programa[0]->tipo),'class="form-control" id="tipo"'); ?>
    </div>
  </div>
  
 <div class="form-group">
    <label for="Cantidad" class="col-lg-3 control-label">Cantidad de Beneficios:</label>
    <div class="col-lg-3">
      <?php echo form_input($Cantidad); ?>
    </div>
  </div>
  <div class="form-group" disabled>
    <label for="Restantes" class="col-lg-3 control-label">Beneficios Restantes:</label>
    <div class="col-lg-3"  >
      <?php echo form_input($Restante);  ?>
    </div>
  </div>
 
  <div class="form-group">
    <div class="col-lg-offset-3 col-lg-10">
      <button type="button" onclick="regresar()" class="btn btn-primary"><span class="glyphicon glyphicon-circle-arrow-left"></span> Regresar</button>
      <button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-saved"></span> Guardar </button>
      <?php if($titulo=="Nuevo Programa"){ ?>
      <button type="reset" class="btn btn-primary"><span class="glyphicon glyphicon-plus"></span> Nuevo</button>
      <?php } ?>
    </div>
  </div>
  <hr/>
</form>		
<script type="text/javascript">
  $(document).ready(function() {
    $("#Restante").prop("disabled", true)
} );

</script>
