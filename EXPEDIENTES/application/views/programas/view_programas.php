<style type="text/css">
th, td { white-space: nowrap; }
    div.dataTables_wrapper {
        width: 100%;
        margin: 0 auto;
    }
</style>
<script type="text/javascript">
var baseurl = "<?php echo base_url(); ?>";
var currentLocation = window.location;
/**
 * Elimina los datos registrados de un programa
 * @param type nombre  nombre del programa
 * @param type id 	   identificador de un programa
 * @return type
 */
function eliminarPrograma(nombre, id){
    confirmar=confirm("Realmente desea eliminar a " + nombre + "? Una vez eliminado NO podrá recuperarlo"); 

    if (confirmar){
    	document.getElementById('mensaje').innerHTML = "<div class='modal1'><div class='center1'> <center> <img src='"+ baseurl +"/img/gif-load.gif'> Eliminando Programa...</center></div></div>";
    	 var Prog 		 = new Object();
		Prog.Id      	 = id;
		Prog.Nombre      = nombre;
		var DatosJson = JSON.stringify(Prog);
		$.post(currentLocation + '/deleteProgramas',
		{ 
			ProgramaPost: DatosJson
		},
		function(data, textStatus) {
			//
			$("#mensaje").html(data.error_msg);
		}, 
		"json"		
		);
    } else{
    } 
  }
  
</script>
<h1 class="page-header"><span class="glyphicon glyphicon-list-alt"></span> Programas</h1>
<div id="mensaje"></div>
<p align="right">
 	 <a href="programas/nuevo">
 	 	<button type="button" class="btn btn-primary"><span class="glyphicon glyphicon-ok"></span> Nuevo Programas</button>
 	 </a>  
 	 </p>
 	 <br/>
	<table id="programas" border="0" cellpadding="0" cellspacing="0" width="100%" class="pretty">
		<thead>
			<tr>
				<th></th>
				<th>Id</th>
				<th>Programas</th>
				<th>Tipo</th>
				<th>Cantidad</th>
				
			</tr>
		</thead>
		<tbody>
			<?php
				if($programas){
					foreach($programas as $programa){
						$idPrograma    = base64_encode($programa->id);
						echo '<tr>';
						echo '<td>';
						echo '<a href="programas/editarPrograma/'.$idPrograma.'"><button type="button" title="Editar Programa" class="btn btn-success btn-xs"><span class="glyphicon glyphicon-edit"></span></button></a> &nbsp;';
						?>
						<button type="button" onclick="eliminarPrograma('<?php echo $programa->nombre_programa; ?>','<?php echo $idPrograma; ?>');" title="Eliminar Programa" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span></button>
						<?php
						echo '</td>';
						echo '<td>'.$programa->id.'</td>';
						echo '<td>'.$programa->nombre_programa.'</td>';
			     
		$Tipo="";		
		$i=$programa->tipo;
        switch ($i) 
	{
          case 0:
                $Tipo= "";
                break;
          case 1:
                $Tipo= "Nacional";
                break;
          case 2:
                $Tipo= "Provincial";
                break;
	      case 3:
                $Tipo= "Municipal";
                break;
          
 
    };

						echo '<td>'.$Tipo.'</td>';
							echo '<td>'.$programa->cantidad.'</td>';
					
						echo '</tr>';
					}
				}else{
					echo '<tr><td colspan=5><center>No Existe Informacion</center></td></tr>';
				}
			?>
		</tbody>
	</table>
<script type="text/javascript">

            $(document).ready(function() {
    $('#programas').dataTable( {
        "scrollX": false
    } );
} );

</script>