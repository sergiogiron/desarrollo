<input type="hidden" value="<?php echo @$usuario[0]->ID; ?>" id="id" name="id">
<input type="hidden" value="<?php echo @$usuario[0]->ID_AREA; ?>" id="ar" name="ar">

<script type="text/javascript">

  var baseurl = "<?php echo base_url(); ?>";
 
 var idarea       = 0;
  var ids           = document.getElementById("id").value;
  ids               = parseInt(ids.length);
  
  if(ids==0){
        idarea      = 0;
  }else{
        idarea          = document.getElementById("ar").value;
  }

  function regresar(){
    window.location="<?php echo base_url()?>usuarios";
  }
</script>
<?php
//Nombre
  $Nombre       = array(
  'name'        => 'nombre',
  'id'          => 'nombre',
  'size'        => 50,
  'value'       => set_value('nombre',@$usuario[0]->NOMBRE),
  'type'        => 'text',
  'class'       => 'form-control',
   );

  $Telefono       = array(
  'name'        => 'telefono',
  'id'          => 'telefono',
  'size'        => 50,
  'value'       => set_value('telefono',@$usuario[0]->APELLIDOS),
  'type'        => 'text',
  'class'       => 'form-control',
   );

$Email        = array(
  'name'        => 'email',
  'id'          => 'email',
  'size'        => 50,
  'value'       => set_value('email',@$usuario[0]->EMAIL),
  'type'        => 'text',
  'class'       => 'form-control',
//  'onblur'      => 'validarEmail(this.value);',

 ); 

 
  $TipoU    = array(
  '0'             => '---Elegir Opción---',
  '1'             => 'Administrador',
  '2'             => 'Adjudicaciones',
  '3'             => 'Operador',
  '4'             => 'IVUJ',
  '5'             => 'Sec. O.T.y V',
  '6'             => 'INAES',
  '7'             => 'Consulta',
  '8'             => 'Atencion',
  );

  $Password1    = array(
  'name'        => 'password1',
  'id'          => 'password1',
  'size'        => 50,
  'value'       => set_value('password1',@$usuario[0]->PASSWORD),
  'type'        => 'password',
  'class'       => 'form-control',
  );

  $Password2    = array(
  'name'        => 'password2',
  'id'          => 'password2',
  'size'        => 50,
  'value'       => set_value('password2',@$usuario[0]->PASSWORD),
  'type'        => 'password',
  'class'       => 'form-control',
  );
  
   $Privilegios    = array(
  '0'             => '---Elegir Opción---',
  '1'             => 'Nivel  1',
  '2'             => 'Nivel  2',
  '3'             => 'Nivel  3',
  ); 
  
  
  
  $Estatus        = array(
  '0'             => '---Elegir Opción---',
  '1'             => 'Activo',
  '2'             => 'Inactivo',
  );
  
  $disabled = "";
  if($this->session->userdata('ID')==@$usuario[0]->ID and $this->session->userdata('TIPOUSUARIO')!=1){
   $disabled = "disabled='disabled'";
  }
 
  
?>
<script src="<?php echo base_url();?>js/JsonUsuarios.js"></script>
<h1 class="page-header"><span class="glyphicon glyphicon-th-list"></span> <?php echo $titulo; ?></h1>
<div id="mensaje"></div>
<form class="form-horizontal" name="formulario" id="formulario" role="form">
  <input type="hidden" value="0" id="validamail" name="validamail">
  <input type="hidden" value="0" id="validarfc" name="validarfc">
  <div class="form-group">
    <label for="Nombre" class="col-lg-3 control-label">Nombre:</label>
    <div class="col-lg-3">
      <?php echo form_input($Nombre); ?>
    </div>
  </div>
  
   
 <div class="form-group">
    <label for="Telefono" class="col-lg-3 control-label">Telefono:</label>
    <div class="col-lg-3">
      <?php echo form_input($Telefono); ?>
    </div>
  </div>
 
   <div class="form-group">
    <label for="Email" class="col-lg-3 control-label">Email:</label>
    <div class="col-lg-3">
      <?php echo form_input($Email); ?>
    </div>
  </div> 
 
 <div class="form-group">
    <label for="TipoU" class="col-lg-3 control-label">Tipo Usuario:</label>
    <div class="col-lg-3">
      <?php echo  form_dropdown('tipou', $TipoU, set_value('tipou',@$usuario[0]->TIPO),'class="form-control" id="tipou" '.$disabled.' '); ?>
    </div>
  </div>

  <div class="form-group">
    <label for="Password1" class="col-lg-3 control-label">Password:</label>
    <div class="col-lg-3">
      <?php echo form_input($Password1); ?>
    </div>
  </div>

   <div class="form-group">
    <label for="Password2" class="col-lg-3 control-label">Confirmar Password:</label>
    <div class="col-lg-3">
      <?php echo form_input($Password2); ?>
    </div>
  </div>

  <div class="form-group">
    <label for="Privilegios" class="col-lg-3 control-label">Privilegios:</label>
    <div class="col-lg-3">
      <?php echo  form_dropdown('privilegios', $Privilegios, set_value('privilegios',@$usuario[0]->PRIVILEGIOS),'class="form-control" id="privilegios"'); ?>
    </div>
  </div>  
  
  <div class="form-group">
  <label for="area" class="col-lg-3 control-label">Area:</label>
      <div class="col-lg-3">
        <select name="area" id="area" class="form-control"></select>
      </div>
  </div>
  
  
  <div class="form-group">
    <label for="Estatus" class="col-lg-3 control-label">Estatus:</label>
    <div class="col-lg-3">
      <?php echo  form_dropdown('estatus', $Estatus, set_value('estatus',@$usuario[0]->ESTATUS),'class="form-control" id="estatus"'); ?>
    </div>
  </div>
 
 
 
 <div class="form-group">
    <div class="col-lg-offset-3 col-lg-10">
      <?php if($this->session->userdata('TIPOUSUARIO')==1){ ?>
         <button type="button" onclick="regresar()" class="btn btn-primary"><span class="glyphicon glyphicon-circle-arrow-left"></span> Regresar</button>
      <?php } ?>
         <button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-saved"></span> Guardar</button>
      <?php if($titulo=="Nuevo Usuario"){ ?>
         <button type="reset" class="btn btn-primary"><span class="glyphicon glyphicon-plus"></span> Nuevo</button>
      <?php } ?>
    </div>
  </div>
  <hr/>
</form>		

 
      
<script type="text/javascript">
  
</script>
