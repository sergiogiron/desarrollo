<style type="text/css">
th, td { white-space: nowrap; }
    div.dataTables_wrapper {
        width: 100%;
        margin: 0 auto;
    }
</style>
<script type="text/javascript">
var baseurl = "<?php echo base_url(); ?>";
var currentLocation = window.location;
function EliminarUsuario(Usuario, id){
    confirmar=confirm("Eliminar a " + Usuario + ", Recuerda Una vez Eliminado No podras Recuperarlo"); 

    if (confirmar){
    	document.getElementById('mensaje').innerHTML = "<div class='modal1'><div class='center1'> <center> <img src='"+ baseurl +"/img/gif-load.gif'> Eliminando Usuario...</center></div></div>";
    	 var Usuario 		 = new Object();
		Usuario.Id      	 = id;
		var DatosJson = JSON.stringify(Usuario);
		$.post(currentLocation + '/deleteusuario',
		{ 
			MiUsuario: DatosJson
		},
		function(data, textStatus) {
			//
			$("#mensaje").html(data.error_msg);
		}, 
		"json"		
		);
    } else{
    } 
  }
  
</script>
<h1 class="page-header"><span class="glyphicon glyphicon-list-alt"></span> Catalogo Usuarios</h1>
<div id="mensaje"></div>
<p align="right">
 	 <a href="usuarios/nuevo">
 	 	<button type="button" class="btn btn-primary"><span class="glyphicon glyphicon-ok"></span> Nuevo Usuario</button>
 	 </a>  
 	 </p>
 	 <br/>
	<table id="usuarios" border="0" cellpadding="0" cellspacing="0" width="100%" class="pretty">
		<thead>
			<tr>
				<th></th>
				<th>Id</th>
				<th>Nombre</th>
				<th>Telefono</th>
				<th>Email</th>
				<th>Tipo Usuario</th>
				<th>Fecha Registro</th> 
				<th>Prilegios</th>
			</tr>
		</thead>
		<tbody>
			<?php
				if($usuarios){
					foreach($usuarios as $usuario){
						$idusuario     = base64_encode($usuario->ID);
						echo '<tr>';
						echo '<td>';
						echo '<a href="usuarios/editarUsuario/'.$idusuario.'"><button type="button" title="Editar Usuario" class="btn btn-success btn-xs"><span class="glyphicon glyphicon-edit"></span></button></a> &nbsp;';
						?>
						<button type="button" onclick="EliminarUsuario('<?php echo $usuario->NOMBRE.' '.$usuario->APELLIDOS; ?>','<?php echo $idusuario; ?>');" title="Eliminar Usuario" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span></button>
						<?php
						echo '</td>';
						echo '<td>'.$usuario->ID.'</td>';
						echo '<td>'.$usuario->NOMBRE.'</td>';
						echo '<td>'.$usuario->APELLIDOS.'</td>';
						echo '<td>'.$usuario->EMAIL.'</td>';
						
				     	$Tipo="";		
		                $i=$usuario->TIPO;
                        switch ($i) 
	                    {
                         case 0:
					     	  $Estado= "Elegir";
                              break;
						  case 1:
                              $Estado= "Administrador";
                               break;
                          case 2:
                              $Estado= "Adjudicaciones";
                               break;
		                 case 3:
                              $Estado= "Operador";
                              break;	
                         case 4:
                             $Estado= "IVUJ";
                             break;
						  case 5:
                              $Estado= "Sec. O.T.y V";
                               break;
		                 case 6:
                              $Estado= "DDES";
                              break;	
                         case 7:
                             $Estado= "Consulta";
                             break;	 
				       case 8:
                             $Estado= "Atencion";
                             break;	 



					   };			
 				
						
						
						echo '<td>'.$Estado.'</td>';
						echo '<td>'.$usuario->FECHA_REGISTRO.'</td>';
						echo '<td>'.$usuario->PRIVILEGIOS.'</td>';
						echo '</tr>';
					}
				}else{
					echo '<tr><td colspan=5><center>No Existe Informacion</center></td></tr>';
				}
			?>
		</tbody>
	</table>
<script type="text/javascript">

            $(document).ready(function() {
    $('#usuarios').dataTable( {
        "scrollX": false
    } );
} );

</script>
			