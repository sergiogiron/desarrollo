<style type="text/css">
th, td { white-space: nowrap; }
    div.dataTables_wrapper {
        width: 100%;
        margin: 0 auto;
    }
</style>
<script type="text/javascript">
var baseurl = "<?php echo base_url(); ?>";
var currentLocation = window.location;
function Eliminarsocio(soci, id){
    confirmar=confirm("Eliminar a " + soci + ", Recuerda Una vez Eliminado No podras Recuperarlo"); 

    if (confirmar){
    	document.getElementById('mensaje').innerHTML = "<div class='modal1'><div class='center1'> <center> <img src='"+ baseurl +"/img/gif-load.gif'> Eliminando Socio...</center></div></div>";
    	 var socio		 = new Object();
		 socio.Id      	 = id;
		
		var DatosJson = JSON.stringify(socio);
		$.post(currentLocation + '/deletesocio',
		{ 
			SociosPost: DatosJson
		},
		function(data, textStatus) {
			
			$("#mensaje").html(data.error_msg);
		}, 
		"json"		
		);
    } else{
    } 
  }
  
</script>
<h1 class="page-header"><span class="glyphicon glyphicon-list-alt"></span> Socios</h1>
<div id="mensaje"></div>
<p align="right">
 	 <a href="socios/nuevo">
 	 	<button type="button" class="btn btn-primary"><span class="glyphicon glyphicon-ok"></span> Nuevo Socios</button>
 	 </a>  
 	 </p>
 	 <br/>
	<table id="socios" border="0" cellpadding="0" cellspacing="0" width="100%" class="pretty">
		<thead>
			<tr>
				<th></th>
				<th>id</th>
				<th>Nombre</th>
				<th>Cargo</th>
				<th>Telefonica</th>
				<th>Cooperativa</th>
			</tr>
		</thead>
		<tbody>
			<?php
				if($socios){
					foreach($socios as $socio){
					   $i=$socio->cargo;
					   $mcargo="";
					   
                       switch ($i) {
                       case 0:
                              $mcargo = '';
                              break;
                       case 1:
                              $mcargo = 'Presidente';
                              break;
                       case 2:
                                $mcargo ='Secretaria';
                               break;
					     case 3:
					   
                              $mcargo = 'Tesorero';
                              break;
					    case 4:
					   
                              $mcargo = 'Vocal Primero';
                              break;
                       case 5:
                              $mcargo = 'Vocal Segundo';
                              break;
                       case 6:
                              $mcargo ='Sindico titular';
                               break;	   
					   case 7:
                              $mcargo = 'Sindico Suplente';
                              break;
                       case 8:
                              $mcargo = 'Socio';
                              break;
    					   
                    } 
							
					
						$idSocio     = base64_encode($socio->id);
						echo '<tr>';
						echo '<td>';
						echo '<a href="socios/editarSocio/'.$idSocio.'"><button type="button" title="Editar Socio" class="btn btn-success btn-xs"><span class="glyphicon glyphicon-edit"></span></button></a> &nbsp;';
						?>
						<button type="button" onclick="Eliminarsocio('<?php echo $socio->nombre; ?>','<?php echo $idSocio; ?>');" title="Eliminar Socio" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span></button>
						<?php
						echo '</td>';
						echo '<td>'.$socio->id.'</td>';
						echo '<td>'.$socio->nombre.'</td>';
						echo '<td>'.$mcargo.'</td>';
						echo '<td>'.$socio->telefono.'</td>';
						echo '<td>'.$socio->razon.'</td>';
						echo '</tr>';
					}
				}else{
					echo '<tr><td colspan=5><center>No Existe Informacion</center></td></tr>';
				}
			?>
		</tbody>
	</table>
<script type="text/javascript">

            $(document).ready(function() {
    $('#socios').dataTable( {
        "scrollX": false
    } );
} );

</script>
			