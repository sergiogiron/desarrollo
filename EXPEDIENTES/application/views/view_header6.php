<!DOCTYPE html>
<html lang="en">


  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="UCEPPI">
    <meta name="author" content="Ing.">
    <link rel="icon" type="image/<?php echo EXTENSION_IMAGEN_FAVICON; ?>" href="<?php echo base_url()?>img/<?php echo NOMBRE_IMAGEN_FAVICON; ?>" />

    <title><?php echo TITULO_PAGINA; ?></title>
    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url()?>css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <script type="text/javascript" src="<?php echo base_url()?>js/jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url()?>js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>js/jquery.dataTables.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>js/JsValidacion.js"></script>
    <script type="text/javascript" src="<?php echo base_url()?>js/treeMenu.js"></script> 
    <link href="<?php echo base_url()?>css/dashboard.css" rel="stylesheet">
	  <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>css/menu.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>css/Tablas.css">
    <link rel="stylesheet" href="<?php echo base_url()?>css/iconos/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo base_url()?>css/jquery-ui.css"> 
  </head>

  <body>

    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Menus</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a style="color:#FFFFFF" class="navbar-brand" href="<?php echo base_url()?>"><?php echo NOMBRE_EMPRESA; ?></a>
        </div>
        <div id="navbar" style="background:#1fa67a" class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
            <li><a style="color:#FFFFFF"><?php echo 'Bienvenido: <strong>'.$this->session->userdata('NOMBRE').'</strong>&nbsp;|&nbsp;';
              echo 'Tipo Usuario: <strong>'.$this->session->userdata('TIPOUSUARIOMS').'</strong>&nbsp;|&nbsp;';
             ?></a></li>
            <li><a style="color:#FFFFFF" href="<?php echo base_url()."usuarios/editarUsuario/".base64_encode($this->session->userdata('ID')); ?>">Mis Datos</a></li>
			<li><a style="color:#FFFFFF" href=""> NIVEL :<?php echo $this->session->userdata('NIVEL')?></a></li>
			
             <li><a style="color:#FFFFFF" href="<?php echo base_url().'login/CerrarSesion'; ?>">Salir</a></li>
          </ul>
        </div>
      </div>
    </nav>

	
	
    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
		   
           
		  <div id="treeMenu">
			
 
  
 
		
		<hr/>
		
		
<ul>


<h3>Menú</h3>
<?php
     $tipo=$this->session->userdata('TIPOUSUARIOMS');
	 switch ($tipo) 
	    {
          case "Administrador":
		  //***********************Administrador*********//
            //INICIO DE CABECERA
              echo '<li>';
                  echo '<a href="#" class="parent">ABM</a><span></span>';
                  echo '<div>';
                  echo '<ul>';
            //FIN DE CABECERA

            //ENLACES                 
              /*     echo '<li><a href="'.base_url().'cooperativas">Cooperativas</a></li>';
       	           echo '<li><a href="'.base_url().'socios">Socios</a></li>';
    		       echo '<li><a href="'.base_url().'beneficios">Beneficiarios</a></li>';
    		       echo '<li><a href="'.base_url().'programas">Programas</a></li>';
    		       echo '<li><a href="'.base_url().'localidades">Localidades</a></li>';
    		       echo '<li><a href="'.base_url().'denuncias">Denuncias</a></li>';
    		       echo '<li><a href="'.base_url().'cupos">Cupos</a></li>';
    		       echo '<li><a href="'.base_url().'Parentesco">Parentesco</a></li>';
    		       echo '<li><a href="'.base_url().'instituciones">Instituciones</a></li>'; */
    		       echo '<li><a href="'.base_url().'areas">Areas</a></li>';
               echo '<li><a href="'.base_url().'dependencias">Dependencias</a></li>';
    		//	     echo '<li><a href="'.base_url().'referentes">Referentes</a></li>';
			     //FIN ENLACES
                  echo '</ul>';//********
                  echo '</div>';//*******CIERRE DE CABECERAS
                  echo '</li>'; //********
          //INICIO DE CABECERA
     /*         echo '<li>';
                  echo '<a href="#" class="parent">ADJUDICACION</a><span></span>';
                  echo '<div>';
                  echo '<ul>';  */
          //FIN DE CABECERA

          //ENLACES                 
         /*     echo '<li><a href="'.base_url().'adjudicaciones">Adjudicar</a></li> ';
		          echo '<li><a href="'.base_url().'adjudicaciones/todo">Modificar Adjudicacion</a></li> ';
			   //FIN ENLACES
                  echo '</ul>';//********
                  echo '</div>';//*******CIERRE DE CABECERAS
                  echo '</li>'; //*     */
        //INICIO DE CABECERA
            echo'<li>';
                  echo '<a href="#" class="parent">MESA DE ENTRADA</a><span></span>';
                  echo '<div>';
                  echo '<ul>';
        //FIN DE CABECERA

        //ENLACES
              echo '<li><a href="'.base_url().'expedientes">Expedientes</a></li>';
        //FIN ENLACES
                  echo '</ul>';//********
                  echo '</div>';//*******CIERRE DE CABECERAS
                  echo '</li>'; //********           
        //INICIO DE CABECERA
              echo '<li>';
                  echo '<a href="#" class="parent">REPORTES</a><span></span>';
                  echo '<div>';
                  echo '<ul>';
 //FIN DE CABECERA
 //ENLACES                 
           echo ' <li><a href="'.base_url().'reportesExp">Expedientes</a></li>';
      /*     echo ' <li><a href="'.base_url().'reportescopciu">Cooperativa individual</a></li>';
	       echo ' <li><a href="'.base_url().'reportesben">Beneficiarios</a></li>';
		    echo ' <li><a href="'.base_url().'reportessoc">Socios.</a></li>';
	       echo ' <li><a href="'.base_url().'reportes">Ajudicaciones</a></li>'; */
                  echo '</ul>';//********
                  echo '</div>';//*******CIERRE DE CABECERAS
                  echo '</li>'; //********
		  		  
		   //INICIO DE CABECERA
         /*     echo '<li>';
                  echo '<a href="#" class="parent">IVUJ</a><span></span>';
                  echo '<div>';
                  echo '<ul>';  */
 //FIN DE CABECERA
 //ENLACES                 
      /*    echo ' <li><a href="'.base_url().'instituto">Carga</a></li>';
		   echo ' <li><a href="'.base_url().'instituto_c">Consulta</a></li>';
                  echo '</ul>';//********
                  echo '</div>';//*******CIERRE DE CABECERAS
                  echo '</li>'; //********
		  */		  
 //INICIO DE CABECERA
         /*     echo '<li>';
                  echo '<a href="#" class="parent">SOTYV</a><span></span>';
                  echo '<div>';
                  echo '<ul>';  */
 //FIN DE CABECERA
 //ENLACES                 
    /*     echo ' <li><a href="'.base_url().'inscripciones">Inscripción</a></li>';
		   echo ' <li><a href="'.base_url().'inscripciones_c">Consulta</a></li>';
                  echo '</ul>';//********
                  echo '</div>';//*******CIERRE DE CABECERAS
                  echo '</li>'; //********
*/
 //INICIO DE CABECERA
    /*          echo '<li>';
                  echo '<a href="#" class="parent">INAES</a><span></span>';
                  echo '<div>';
                  echo '<ul>';
				  */
 //FIN DE CABECERA
 //ENLACES                 
   /*       echo ' <li><a href="'.base_url().'inaes">Carga</a></li>';
		   echo ' <li><a href="'.base_url().'instituto_c">Consulta</a></li>';
                  echo '</ul>';//********
                  echo '</div>';//*******CIERRE DE CABECERAS
                  echo '</li>'; //********
				  */
//INICIO DE CABECERA
              echo '<li>';
                  echo '<a href="#" class="parent">SEGURIDAD</a><span></span>';
                  echo '<div>';
                  echo '<ul>';
 //FIN DE CABECERA
 //ENLACES                 
          echo ' <li><a href="'.base_url().'usuarios">Usuarios</a></li>';
		   echo ' <li><a href="'.base_url().'bitacoras">Bitacora</a></li>'; 
	 /*      echo ' <li><a href="'.base_url().'cooperativas/exportar">Exportar Cooperativas</a></li>';
		   echo ' <li><a href="'.base_url().'socios/exportar">Exportar Socios</a></li>';
		   echo ' <li><a href="'.base_url().'beneficios/exportar">Exportar Beneficiarios</a></li>'; 
		   echo ' <li><a href="'.base_url().'denuncias/exportar">Exportar Denuncias</a></li>'; 
		   echo ' <li><a href="'.base_url().'construccion">Exportar Ajudicaciones</a></li>'; */
                  echo '</ul>';//********
                  echo '</div>';//*******CIERRE DE CABECERAS
                  echo '</li>'; //********
				  
                  //INICIO DE CABECERA
              echo '<li>';
                  echo '<a href="#" class="parent">SOPORTE Y AYUDA </a><span></span>';
                  echo '<div>';
                  echo '<ul>';
 //FIN DE CABECERA
 //ENLACES                 
           echo ' <li><a href="'.base_url().'mesa">Solicitud de Soporte</a></li>';
                  echo '</ul>';//********
                  echo '</div>';//*******CIERRE DE CABECERAS
                  echo '</li>'; //********

        break;	 

    /*    case "Adjudicaciones":
          //INICIO DE CABECERA
              echo '<li>';
                  echo '<a href="#" class="parent">ABM</a><span></span>';
                  echo '<div>';
                  echo '<ul>';
          //FIN DE CABECERA
          //ENLACES                 
           echo '<li><a href="'.base_url().'cooperativas">Cooperativas</a></li>';
   	       echo '<li><a href="'.base_url().'socios">Socios</a></li>';
		       echo '<li><a href="'.base_url().'beneficios">Beneficiarios</a></li>';
		       echo '<li><a href="'.base_url().'denuncias">Denuncias</a></li>';
                  echo '</ul>';//********
                  echo '</div>';//*******CIERRE DE CABECERAS
                  echo '</li>'; //********
                  //INICIO DE CABECERA
                  echo '<li>';
                  echo '<a href="#" class="parent">ADJUDICAR</a><span></span>';
                  echo '<div>';
                  echo '<ul>';
          //FIN DE CABECERA
          //ENLACES                 
          echo '<li><a href="'.base_url().'adjudicaciones">Adjudicar</a></li>';
                  echo '</ul>';//********
                  echo '</div>';//*******CIERRE DE CABECERAS
                  echo '</li>'; //********

          //INICIO DE CABECERA
              echo '<li>';
                  echo '<a href="#" class="parent">REPORTES</a><span></span>';
                  echo '<div>';
                  echo '<ul>';
          //FIN DE CABECERA
          //ENLACES                 
          echo ' <li><a href="'.base_url().'reportes">Ajudicaciones</a></li>';
                  echo '</ul>';//********
                  echo '</div>';//*******CIERRE DE CABECERAS
                  echo '</li>'; //********
          //INICIO DE CABECERA
              echo '<li>';
                  echo '<a href="#" class="parent">Soporte</a><span></span>';
                  echo '<div>';
                  echo '<ul>';
          //FIN DE CABECERA
          //ENLACES                 
          echo ' <li><a href="'.base_url().'mesa">Solicitud de Soporte</a></li>';		
                  echo '</ul>';//********
                  echo '</div>';//*******CIERRE DE CABECERAS
                  echo '</li>'; //********

        break; 
        case "Operador":

        //INICIO DE CABECERA
              echo '<li>';
                  echo '<a href="#" class="parent">ABM</a><span></span>';
                  echo '<div>';
                  echo '<ul>';
        //FIN DE CABECERA
        //ENLACES                 
          echo '<li><a href="'.base_url().'cooperativas">Cooperativas</a></li>';
   	      echo '<li><a href="'.base_url().'socios">Socios</a></li>';
		      echo '<li><a href="'.base_url().'beneficios">Beneficiarios</a></li>';
		      echo '<li><a href="'.base_url().'denuncias">Denuncias</a></li>';  
                  echo '</ul>';//********
                  echo '</div>';//*******CIERRE DE CABECERAS
                  echo '</li>'; //********
        //INICIO DE CABECERA
            echo'<li>';
                  echo '<a href="#" class="parent">MESA DE ENTRADA</a><span></span>';
                  echo '<div>';
                  echo '<ul>';
        //FIN DE CABECERA

        //ENLACES
              echo '<li><a href="'.base_url().'expedientes">Expedientes</a></li>';
        //FIN ENLACES
                  echo '</ul>';//********
                  echo '</div>';//*******CIERRE DE CABECERAS
                  echo '</li>'; //********
        //INICIO DE CABECERA
              echo '<li>';
                  echo '<a href="#" class="parent">Soporte</a><span></span>';
                  echo '<div>';
                  echo '<ul>';
        //FIN DE CABECERA
        //ENLACES                 
          echo ' <li><a href="'.base_url().'mesa">Solicitud de Soporte</a></li>';		
                  echo '</ul>';//********
                  echo '</div>';//*******CIERRE DE CABECERAS
                  echo '</li>'; //********

        break;   */

        case "Consulta":
              case "Administrador":
		  //***********************Administrador*********//
           
        //INICIO DE CABECERA
            echo'<li>';
                  echo '<a href="#" class="parent">MESA DE ENTRADA</a><span></span>';
                  echo '<div>';
                  echo '<ul>';
        //FIN DE CABECERA

        //ENLACES
              echo '<li><a href="'.base_url().'expedientes">Expedientes</a></li>';
        //FIN ENLACES
                  echo '</ul>';//********
                  echo '</div>';//*******CIERRE DE CABECERAS
                  echo '</li>'; //********           
        //INICIO DE CABECERA
              echo '<li>';
                  echo '<a href="#" class="parent">REPORTES</a><span></span>';
                  echo '<div>';
                  echo '<ul>';
 //FIN DE CABECERA
 //ENLACES                 
           echo ' <li><a href="'.base_url().'reportesExp">Expedientes</a></li>';
     
                  echo '</ul>';//********
                  echo '</div>';//*******CIERRE DE CABECERAS
                  echo '</li>'; //********
		  

        break;	
  };			
 				
		
	
	
	
 ?>  
		  
		</ul>
		</div>  
		
		  
		  
		  
		
        </div>
        <div class="col-md-offset-2 main">
		<br/><br/>