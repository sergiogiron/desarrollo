<input type="hidden" value="<?php echo @$bene[0]->id; ?>" id="id" name="id"> 
<script type="text/javascript">
  var baseurl = "<?php echo base_url(); ?>";
 
  function regresar(){
    window.location="<?php echo base_url()?>beneficios";
  }
</script>
<?php
//Nombre
  $Nombre       = array(
  'name'        => 'Nombre',
  'id'          => 'Nombre',
  'size'        => 50,
  'value'       => set_value('Nombre',@$bene[0]->nombre_beneficios),
  'type'        => 'text',
  'class'       => 'form-control',
  'style'       => 'text-transform:uppercase',
  'onkeypress'  => 'return validarn(event);',
  );

  $Documento    = array(
  'name'        => 'Documento',
  'id'          => 'Documento',
  'size'        => 50,
  'value'       => set_value('Documento',@$bene[0]->documento),
  'type'        => 'text',
  'class'       => 'form-control',
  'style'       => 'text-transform:uppercase',
  );
  $Localidad     = array(
  'name'        => 'Localidad',
  'id'          => 'Localidad',
  'size'        => 50,
  'value'       => set_value('Localidad',@$bene[0]->localidad),
  'type'        => 'text',
  'class'       => 'form-control',
 // 'onkeypress'  => 'return validarNumeros(event);',
  );
   $Barrio           = array(
  'name'        => 'Barrio',
  'id'          => 'Barrio',
  'size'        => 50,
  'value'       => set_value('Barrio',@$bene[0]->barrio),
  'type'        => 'text',
  'class'       => 'form-control',
  );
   $Mza           = array(
  'name'        => 'Mza',
  'id'          => 'Mza',
  'size'        => 50,
  'value'       => set_value('Mza',@$bene[0]->mza),
  'type'        => 'text',
  'class'       => 'form-control',
  );
   $Lote           = array(
  'name'        => 'Lote',
  'id'          => 'Lote',
  'size'        => 50,
  'value'       => set_value('Lote',@$bene[0]->lote),
  'type'        => 'text',
  'class'       => 'form-control',
  );
   $Cooperativa     = array(
  'name'        => 'Cooperativa',
  'id'          => 'Cooperativa',
  'size'        => 50,
  'value'       => set_value('Cooperativa',@$bene[0]->cooperativa),
  'type'        => 'text',
  'class'       => 'form-control',
  );
   $Matricula     = array(
  'name'        => 'Matricula',
  'id'          => 'Matricula',
  'size'        => 50,
  'value'       => set_value('Matricula',@$bene[0]->matricula),
  'type'        => 'text',
  'class'       => 'form-control',
  );
     $Obs     = array(
  'name'        => 'Obs',
  'id'          => 'Obs',
  'size'        => 50,
  'value'       => set_value('Obs',@$bene[0]->obs),
  'type'        => 'text',
  'class'       => 'form-control',
  );
?>
<script src="<?php echo base_url();?>js/JsonBeneficios.js"></script>
<h1 class="page-header"><span class="glyphicon glyphicon-th-list"></span> <?php echo $titulo; ?></h1>
<div id="mensaje"></div>
<form class="form-horizontal" name="formulario" id="formulario" role="form">
  <input type="hidden" value="0" id="validamail" name="validamail">
  <input type="hidden" value="0" id="validarfc" name="validarfc">
  <div class="form-group">
    <label for="Nombre" class="col-lg-3 control-label">Beneficiario:</label>
    <div class="col-lg-3">
      <?php echo form_input($Nombre); ?>
    </div>
  </div>
 
  <div class="form-group">
    <label for="Documento" class="col-lg-3 control-label">Documento:</label>
    <div class="col-lg-3">
      <?php echo form_input($Documento); ?>
    </div>
  </div>

  <div class="form-group">
    <label for="Localidad" class="col-lg-3 control-label">Localidad:</label>
    <div class="col-lg-3">
      <?php echo form_input($Localidad); ?>
    </div>
  </div>

  <div class="form-group">
    <label for="Barrio" class="col-lg-3 control-label">Barrrio:</label>
    <div class="col-lg-3">
      <?php echo form_input($Barrio); ?>
    </div>
  </div>
 
   <div class="form-group">
    <label for="Mza" class="col-lg-3 control-label">Mza:</label>
    <div class="col-lg-3">
      <?php echo form_input($Mza); ?>
    </div>
  </div>
  
  <div class="form-group">
    <label for="Lote" class="col-lg-3 control-label">Lote:</label>
    <div class="col-lg-3">
      <?php echo form_input($Lote); ?>
    </div>
  </div>
 

   <div class="form-group">
    <label for="Obs" class="col-lg-3 control-label">Obs:</label>
    <div class="col-lg-3">
      <?php echo form_input($Obs); ?>
    </div>
  </div>
  <!--   -->
  <div class="form-group">
    <div class="col-lg-offset-3 col-lg-10">
      <button type="button" onclick="regresar()" class="btn btn-default">Regresar</button>
      <button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-saved"></span> Guardar Beneficiarios</button>
      <?php if($titulo=="Nuevo Beneficio"){ ?>
      <button type="reset" class="btn btn-default">Nuevo</button>
      <?php } ?>
    </div>
  </div>
  <hr/>
</form>		
<script type="text/javascript">
  
</script>
