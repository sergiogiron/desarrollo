<style type="text/css">
th, td { white-space: nowrap; }
    div.dataTables_wrapper {
        width: 100%;
        margin: 0 auto;
    }
</style>
<script type="text/javascript">
var baseurl = "<?php echo base_url(); ?>";
var currentLocation = window.location;
function Eliminarprofesor(profesor, id){
    confirmar=confirm("Eliminar a " + profesor + ", Recuerda Una vez Eliminado No podras Recuperarlo"); 

    if (confirmar){
    	document.getElementById('mensaje').innerHTML = "<div class='modal1'><div class='center1'> <center> <img src='"+ baseurl +"/img/gif-load.gif'> Eliminando profesor...</center></div></div>";
    	 var profesor 		 = new Object();
		profesor.Id      	 = id;
		profesor.Codigo      = codigo;
		var DatosJson = JSON.stringify(profesor);
		$.post(currentLocation + '/deleteprofesor',
		{ 
			Miprofesor: DatosJson
		},
		function(data, textStatus) {
			//
			$("#mensaje").html(data.error_msg);
		}, 
		"json"		
		);
    } else{
    } 
  }
  
</script>
<h1 class="page-header"><span class="glyphicon glyphicon-list-alt"></span> Catalogo de Profesores</h1>
<div id="mensaje"></div>
<p align="right">
 	 <a href="profesores/nuevo">
 	 	<button type="button" class="btn btn-primary"><span class="glyphicon glyphicon-ok"></span> Nuevo Profesor</button>
 	 </a>  
 	 </p>
 	 <br/>
	<table id="profesores" border="0" cellpadding="0" cellspacing="0" width="100%" class="pretty">
		<thead>
			<tr>
				<th></th>
				<th>Nombre</th>
				<th>Direccion</th>
				<th>Telefono</th>
				<th>CP</th>
			</tr>
		</thead>
		<tbody>
			<?php
				if($profesores){
					foreach($profesores as $profesor){
						$idProfesor     = base64_encode($profesor->id);
						echo '<tr>';
						echo '<td>';
						echo '<a href="profesores/editarProfesor/'.$idProfesor.'"><button type="button" title="Editar profesor" class="btn btn-success btn-xs"><span class="glyphicon glyphicon-edit"></span></button></a> &nbsp;';
						?>
						<button type="button" onclick="Eliminarprofesor('<?php echo $profesor->nombre_profesor; ?>','<?php echo $idProfesor; ?>');" title="Eliminar profesor" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span></button>
						<?php
						echo '</td>';
						echo '<td>'.$profesor->nombre_profesor.'</td>';
						echo '<td>'.$profesor->direccion.'</td>';
						echo '<td>'.$profesor->telefono.'</td>';
						echo '<td>'.$profesor->cp.'</td>';
						echo '</tr>';
					}
				}else{
					echo '<tr><td colspan=5><center>No Existe Informacion</center></td></tr>';
				}
			?>
		</tbody>
	</table>
<script type="text/javascript">

            $(document).ready(function() {
    $('#profesores').dataTable( {
        "scrollX": false
    } );
} );

</script>
			