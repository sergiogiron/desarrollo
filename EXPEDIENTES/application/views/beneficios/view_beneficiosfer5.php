<style type="text/css">
th, td { white-space: nowrap; }
    div.dataTables_wrapper {
        width: 100%;
        margin: 0 auto;
    }
</style>
<script type="text/javascript">
var baseurl = "<?php echo base_url(); ?>";
var currentLocation = window.location;
function Eliminarbeneficios(profesor, id){
    confirmar=confirm("Eliminar a " + profesor + ", Recuerda Una vez Eliminado No podras Recuperarlo"); 

    if (confirmar){
    	document.getElementById('mensaje').innerHTML = "<div class='modal1'><div class='center1'> <center> <img src='"+ baseurl +"/img/gif-load.gif'> Eliminando Beneficiarios...</center></div></div>";
    	 var profe		 = new Object();
		profe.Id      	 = id;
		
		var DatosJson = JSON.stringify(profe);
		$.post(currentLocation + '/deletebeneficios',
		{ 
			BeneficiosPost: DatosJson
		},
		function(data, textStatus) {
			//
			$("#mensaje").html(data.error_msg);
		}, 
		"json"		
		);
    } else{
    } 
  }
  
</script>
<h1 class="page-header"><span class="glyphicon glyphicon-list-alt"></span> Beneficiarios</h1>
<div id="mensaje"></div>
<p align="right">
 	 <a href="beneficios/nuevo">
 	 	<button type="button" class="btn btn-primary"><span class="glyphicon glyphicon-ok"></span> Nuevo Beneficiarios</button>
 	 </a>  
 	 </p>
 	 <br/>
	<table id="beneficios" border="0" cellpadding="0" cellspacing="0" width="100%" class="pretty">
		<thead>
			<tr>
				<th></th>
				<th>Programa</th>
				<th>Estado</th>
				<th>Documento</th>
				<th>Nombre</th>
				<th>Localidad</th>
				<th>Barrio</th>
				<th>Mza</th>
				<th>Lote</th>
	
			
			</tr>
		</thead>
		<tbody>
			<?php
				if($beneficios){
					foreach($beneficios as $bene){
						$id     = base64_encode($bene->id);
						echo '<tr>';
						echo '<td>';
						echo '<a href="beneficios/editarbeneficios/'.$id.'"><button type="button" title="Editar beneficiario" class="btn btn-success btn-xs"><span class="glyphicon glyphicon-edit"></span></button></a> &nbsp;';
						?>
						<button type="button" onclick="Eliminarbeneficios('<?php echo $bene->nombre_beneficios; ?>','<?php echo $id; ?>');" title="Eliminar Beneficiario" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span></button>
						<?php
						echo '</td>';
						
				
						 		
		    $i=$bene->estado;
        switch ($i) 
	{
         case 0:
                $Estado= "Elegir";
                break;
		 case 1:
                $Estado= "Inscripcion";
                break;
          case 2:
                $Estado= "Habilitado";
                break;
		  case 3:
                $Estado= "Adjudicado";
				 break;
		case 4:
                $Estado= "Pendiente";
				 break;	
	    case 5:
                $Estado= "Denuncia";
                break;
		  };			
						
						
						
						
					  echo '<td>'.$bene->nombre_programa.'</td>';	
						
						echo '<td>'.$Estado.'</td>';
						
						echo '<td>'.$bene->documento.'</td>';
						echo '<td>'.$bene->nombre_beneficios.'</td>';
						echo '<td>'.$bene->NLoca.'</td>';
						echo '<td>'.$bene->barrio.'</td>';
						echo '<td>'.$bene->mza.'</td>';
						echo '<td>'.$bene->lote.'</td>';
						//echo '<td>'.$bene->cooperativa.'</td>';
						//echo '<td>'.$bene->matricula.'</td>';
				//		echo '<td>'.$bene->obs.'</td>';
					
						
						
						echo '</tr>';
					}
				}else{
					echo '<tr><td colspan=5><center>No Existe Informacion</center></td></tr>';
				}
			?>
		</tbody>
	</table>
<script type="text/javascript">

            $(document).ready(function() {
    $('#beneficios').dataTable( {
        "scrollX": false
    } );
} );

</script>
			