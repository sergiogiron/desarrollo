<?php
date_default_timezone_set('America/Argentina/Jujuy');
?>


<style type="text/css">
th, td { white-space: nowrap; }
    div.dataTables_wrapper {
        width: 100%;
        margin: 0 auto;
    }
</style>
<script type="text/javascript">
var baseurl = "<?php echo base_url(); ?>";
var currentLocation = window.location;

function eliminarArea(area, id){
    confirmar=confirm("Realmente desea eliminar Área código:" + area + "? Una vez eliminado NO podrá ser recuperado"); 

    if (confirmar){
    	 document.getElementById('mensaje').innerHTML = "<div class='modal1'><div class='center1'> <center> <img src='"+ baseurl +"/img/gif-load.gif'> Eliminando Área...</center></div></div>";
    	 var Area		 = new Object();
		 Area.Id      	 = id;
		 Area.Nota       = area;
		
		var DatosJson = JSON.stringify(Area);
		$.post(currentLocation + '/deleteArea', //Funcion del controlador
		{ 
			AreaPost: DatosJson
		},
		function(data, textStatus) {
			//
			$("#mensaje").html(data.error_msg);
		}, 
		"json"		
		);
    } else{
    } 
  }
  
</script>
<h1 class="page-header"><span class="glyphicon glyphicon-list-alt"></span> Área</h1>
<div id="mensaje"></div>
<p align="right">
 	 <a href="areas/nuevo">
 	 	<button type="button" class="btn btn-primary"><span class="glyphicon glyphicon-ok"></span> Nueva Área</button>
 	 </a>  
 	 </p>
 	 <br/>
	<table id="area" border="0" cellpadding="0" cellspacing="0" width="100%" class="pretty">
		<thead>
			<tr>
				<th></th>
				<th>Codigo</th>
				<th>Nombre Área</th>
				<th>Observaciones</th>
			</tr>
		</thead>
		<tbody>
			<?php
				if($area){
					foreach($area as $Area){
						$idArea    = base64_encode($Area->id_area);
						echo '<tr>';
						echo '<td>';
						echo '<a href="areas/editarArea/'.$idArea.'"><button type="button" title="Editar Área" class="btn btn-success btn-xs"><span class="glyphicon glyphicon-edit"></span></button></a> &nbsp;';
						?>
						<button type="button" onclick="eliminarArea('<?php echo $Area->codigo_area; ?>','<?php echo $idArea; ?>');" title="Eliminar Área" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span></button>
						<?php
						echo '</td>';
						echo '<td>'.str_pad((int) $Area->codigo_area,4,"0",STR_PAD_LEFT).'</td>'; //Doy formato 0000 al codigo_area
						echo '<td>'.$Area->nombre_area.'</td>';
						echo '<td>'.$Area->obs_area.'</td>';
						echo '</tr>';
					}
				}else{
					echo '<tr><td colspan=5><center>No Existe Informacion</center></td></tr>';
				}
			?>
		</tbody>
	</table>
<script type="text/javascript">

            $(document).ready(function() {
    $('#area').dataTable( {
        "scrollX": false
    } );
} );

</script>