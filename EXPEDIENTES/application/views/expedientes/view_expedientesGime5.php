<?php
date_default_timezone_set('America/Argentina/Jujuy');
?>


<style type="text/css">
th, td { white-space: nowrap; }
    div.dataTables_wrapper {
        width: 100%;
        margin: 0 auto;
    }
</style>
<script type="text/javascript">
var baseurl = "<?php echo base_url(); ?>";
var currentLocation = window.location;

function eliminarExpediente(exp, id){
    confirmar=confirm("Realmente desea eliminar Expediente código:" + exp + "? Una vez eliminado NO podrá ser recuperado"); 

    if (confirmar){
    	 document.getElementById('mensaje').innerHTML = "<div class='modal1'><div class='center1'> <center> <img src='"+ baseurl +"/img/gif-load.gif'> Eliminando Expediente...</center></div></div>";
    	 var Exp		 = new Object();
		 Exp.Id      	 = id;
		 Exp.Remitente       = exp;
		
		var DatosJson = JSON.stringify(Exp);
		$.post(currentLocation + '/deleteExpediente', //Funcion del controlador
		{ 
			ExpPost: DatosJson
		},
		function(data, textStatus) {
			//
			$("#mensaje").html(data.error_msg);
		}, 
		"json"		
		);
    } else{
    } 
  }
  

</script>
<h1 class="page-header"><span class="glyphicon glyphicon-list-alt"></span> Expedientes</h1>
<div id="mensaje"></div>
<p align="right">
 	 <a href="expedientes/nuevo/">
 	 	<button type="button" class="btn btn-primary"><span class="glyphicon glyphicon-ok"></span> Nuevo Expediente</button>
 	 </a>  
 	 </p>
 	 <br/>
 	 
	<table id="exp" border="0" cellpadding="0" cellspacing="0" width="100%" class="pretty">
		<thead>
			<tr>
				<th></th>
				<th>Codigo</th>
				<th>Fecha</th>
				<th>Dependencia</th>
				<th>Remitente</th>
				<th>Area</th>
				<th>Estado</th>
				<th>Foto Expediente</th>
				<th>Observaciones</th>
			</tr>
		</thead>
		<tbody>
			<?php
				if($exp){
					foreach($exp as $Exp){
						$idExp    = base64_encode($Exp->id_exp);
						echo '<tr>';
						echo '<td>';
						echo '<a href="expedientes/editarExpediente/'.$idExp.'"><button type="button" title="Editar Expediente" class="btn btn-success btn-xs"><span class="glyphicon glyphicon-edit"></span></button></a> &nbsp;';
						?>
							<button type="button" onclick="eliminarExpediente('<?php echo $Exp->codigo_exp; ?>','<?php echo $idExp; ?>');" title="Eliminar Expediente" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span></button>
						<?php
						echo '<a href="expedientes/listarPases/'.$idExp.'"><button type="button" title="Vista Pases" class="btn btn-warning btn-xs"><span class="glyphicon glyphicon-list"></span></button></a> &nbsp;';
						echo '<a href="expedientes/view_img/'.$idExp.'"><button type="button" title="Cargar Expediente" class="btn btn-primary btn-xs"><span class="glyphicon glyphicon-open"></span></button></a> &nbsp;';                                                                                                                                                                                                                    
						echo '<a href="expedientes/view_mostrar/'.$idExp.'"><button type="button" title="Imprimir Expediente" class="btn btn-primary btn-xs"><span class="glyphicon glyphicon-print"></span></button></a> &nbsp;';
						echo '</td>';
						echo '<td>'.str_pad((int) $Exp->codigo_exp,4,"0",STR_PAD_LEFT).'</td>'; //Doy formato 0000 al codigo_exp
						//echo '<td>'.$Nota->anio.'</td>';
						//echo '<td>'.date('Y',strtotime($Nota->anio)).'</td>';
						//echo '<td>'.$Exp->fecha_exp.'</td>';
						echo '<td>'.date('d/m/Y').'</td>'; //,strtotime($Exp->fecha_exp)
						echo '<td>'.$Exp->nombre_dep.'</td>';
						echo '<td>'.$Exp->remitente_exp.'</td>';
						echo '<td>'.$Exp->nombre_area.'</td>';
						$Estado="";		
						$i=$Exp->estado_exp;
				        switch ($i) 
						{
				          case 0:
				                $Estado= "";
				                break;
				          case 1:
				                $Estado= "Pase Inicial";
				                break;
				          case 2:
				                $Estado= "Archivado";
				                break;
					      case 3:
				                $Estado= "En Reserva";
				                break;
				          case 4: 
				          		$Estado="En Tramite";
				          		break;
				          case 5:
				          		$Estado="Pendiente";
				          		break;
				          case 6:
				          		$Estado="Terminado";
				          		break;
				    	};
						echo '<td>'.$Estado.'</td>';
						echo '<td>'.$Exp->foto_exp.'</td>';
						echo '<td>'.$Exp->obs_exp.'</td>';
						echo '</tr>';
					}
				}else{
					echo '<tr><td colspan=5><center>No Existe Informacion</center></td></tr>';
				}
			?>
		</tbody>
	</table>
<script type="text/javascript">

            $(document).ready(function() {
    $('#exp').dataTable( {
        "scrollX": false
    } );
} );

</script>
