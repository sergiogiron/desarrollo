<style type="text/css">
th, td { white-space: nowrap; }
    div.dataTables_wrapper {
        width: 100%;
        margin: 0 auto;
    }
</style>
<script type="text/javascript">
var baseurl = "<?php echo base_url(); ?>";
var currentLocation = window.location;
function EliminarRecibo(nombre, id){
    confirmar=confirm("Eliminar Recibo: " + nombre + ", Recuerda Una vez Eliminado No podras Recuperarlo"); 

    if (confirmar){
    	document.getElementById('mensaje').innerHTML = "<div class='modal1'><div class='center1'> <center> <img src='"+ baseurl +"/img/gif-load.gif'> Eliminando Recibo...</center></div></div>";
    	 var Recibo 		 = new Object();
		Recibo.Id      	 = id;
	
		var DatosJson = JSON.stringify(Recibo);
		$.post(currentLocation + '/deleterecibo',
		{ 
			MiRecibo: DatosJson
		},
		function(data, textStatus) {
			//
			$("#mensaje").html(data.error_msg);
		}, 
		"json"		
		);
    } else{
    } 
  }
  
</script>
<h1 class="page-header"><span class="glyphicon glyphicon-list-alt"></span> Historial de Expedientes</h1>
<div id="mensaje"></div>
	<p align="right">
 	 <a href="bitacoras">
 	 	<button type="button" class="btn btn-primary"><span class="glyphicon glyphicon-ok"></span> Nuevo hitorial</button>
 	 </a>  
 	 </p>
 	 <br/>
 	 
	<table id="bitacoras" border="0" cellpadding="0" cellspacing="0" width="100%" class="pretty">
		<thead>
			<tr>

				<th>Nº Expediente</th>
				
				<th>fecha</th>
				<th>usuario</th>
				<th>mensaje</th>
				
				
			</tr>
		</thead>
		<tbody>
			<?php
				if($bitacoras){
					foreach($bitacoras as $bita){
					
						//$codigo       =  base64_encode($producto->tipo);
						//$id           = $bitacoras->id;

						echo '<tr>';
				//		echo '<td>';
				//		echo '<a href="ventas/ImprimeVenta/'.$id.'"><button type="button" title="Imprimir Recibo" class="btn btn-success btn-xs"><span class="glyphicon glyphicon-print"></span></button></a> &nbsp;';
						?>
					<!--	<a ><button onclick="EliminarRecibo('<?php echo $bitacoras->fecha; ?>','<?php echo $id; ?>');" type="button" title="Eliminar Recibo" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span></button></a> -->
						<?php
				//		echo '</td>';
						echo '<td>'.$bita->codigo_exp.'</td>';
						echo '<td>'.date("d-m-Y H:i:s",strtotime($bita->fecha)).'</td>';
						echo '<td>'.$bita->nusuario.'</td>';
						echo '<td>'.$bita->mensaje.'</td>';
						echo '</tr>';
					}
				}else{
					echo '<tr><td colspan=9><center>No Existe Informacion</center></td></tr>';
				}
			?>
		</tbody>
	</table>
<script type="text/javascript">

    $(document).ready(function() {
    $('#bitacoras').dataTable( {
        "scrollX": true
    } );
} );

</script>