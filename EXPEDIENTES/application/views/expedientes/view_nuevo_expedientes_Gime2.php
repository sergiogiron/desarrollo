<input type="hidden" id="id" name="id" value="<?php echo @$exp[0]->id_exp; ?>" >
<input type="hidden" id="dep"name="dep" value="<?php echo @$exp[0]->id_dep; ?>">
<input type="hidden" id="ar"name="ar" value="<?php echo @$exp[0]->id_area; ?>">

<script type="text/javascript">

  var baseurl = "<?php echo base_url(); ?>";

  var iddep        = 0;
  var idarea       = 0;
  var ids           = document.getElementById("id").value;
  ids               = parseInt(ids.length);
  
  if(ids==0){
        iddep       = 0;
        idarea      = 0;
  }else{
    
    iddep           = document.getElementById("dep").value;
    idarea          = document.getElementById("ar").value;

  }

  
  function regresar(){
    window.location="<?php echo base_url()?>expedientes";
  }

</script>

<?php
if($titulo =="Nuevo Expediente"){
 //NRO CODIGO nuevo
  $Codigo = array(
  'name'        => 'codigo',
  'id'          => 'codigo',
  'size'        => 50,
  'value'       => set_value('codigo',@$max[0]->max) + 1,
  'type'        => 'text',
  'class'       => 'form-control',
  );
  $Fecha       = array(
  'name'        => 'fecha',
  'id'          => 'fecha',
  'size'        => 50,
  'value'       => set_value('fecha',date('d/m/Y')), //formato para mostrar el año actual
  'type'        => 'text',
  'class'       => 'form-control',
   );
  
  /*$Anio      = array(
  'name'        => 'anio',
  'id'          => 'anio',
  'size'        => 50,
  'value'       => set_value('anio',date('Y')), //formato para mostrar el año actual
  'type'        => 'text',
  'class'       => 'form-control',
   );*/
   
  
      }else{ 
          $Codigo       = array(
          'name'        => 'codigo',
          'id'          => 'codigo',
          'size'        => 50,
          'value'       => set_value('codigo',@$exp[0]->codigo_exp),//Al editar, toma el codigo guardado
          'type'        => 'text',
          'class'       => 'form-control',
          );
          $Fecha       = array(
          'name'        => 'fecha',
          'id'          => 'fecha',
          'size'        => 50,
          'value'       => set_value('fecha',@$exp[0]->fecha_exp), //Al editar, toma el año de la bd
          'type'        => 'text',
          'class'       => 'form-control',
           );
         /* $Anio       = array(
          'name'        => 'anio',
          'id'          => 'anio',
          'size'        => 50,
          'value'       => set_value('anio',@$exp[0]->anio), //Al editar, toma el año de la bd
          'type'        => 'text',
          'class'       => 'form-control',
           );*/
      }
      
 $Remitente   = array(
  'name'        => 'remitente',
  'id'          => 'remitente',
  'size'        => 50,
  'value'       => set_value('remitente',@$exp[0]->remitente_exp),
  'type'        => 'text',
  'class'       => 'form-control',
  'onkeypress'  => 'return validarn(event);', // Sólo permite el ingreso de caracteres
    );
 /*$Celular     = array(
  'name'        => 'celular',
  'id'          => 'celular',
  'size'        => 50,
  'value'       => set_value('celular',@$nota[0]->celular),
  'type'        => 'text',
  'class'       => 'form-control',
  );*/
  $Asunto    = array(
  'name'        => 'asunto',
  'id'          => 'asunto',
  'size'        => 50,
  'value'       => set_value('asunto',@$exp[0]->asunto_exp),
  'type'        => 'text',
  'class'       => 'form-control',
   );
  $Estado     = array(
  '0'             => '---Elegir Opción---',
  '1'             => 'Archivado',
  '2'             => 'En Reserva',
  '3'             => 'En Tramite',
  '4'             => 'Pendiente',
  '5'             => 'Terminado',
    );
  $Observaciones  = array(
  'name'             => 'observaciones',
  'id'               => 'observaciones',
  'size'             => 50,
  'value'            => set_value('observaciones',@$exp[0]->obs_exp),
  'type'             =>'text',
  'class'            =>'form-control',
  );
?>

<script src="<?php echo base_url();?>js/JsonExpedientes.js"></script>
<h1 class="page-header"><span class="glyphicon glyphicon-th-list"></span> <?php echo $titulo; ?></h1>
<div id="mensaje"></div>
<form class="form-horizontal" name="formulario" id="formulario" role="form">

<div class="form-group">
    <label for="codigo" class="col-lg-3 control-label">Codigo:</label>
      <div class="col-lg-3">
       <?php echo form_input($Codigo)?><!--/<?php echo form_input($Anio); ?>-->
      </div>
</div>
<div class="form-group">
  <label for="fecha" class="col-lg-3 control-label">Fecha:</label>
      <div class="col-lg-3">
        <?php echo form_input($Fecha); ?>
      </div>
</div>
<div class="form-group">
  <label for="dependencia" class="col-lg-3 control-label">Dependencia:</label>
      <div class="col-lg-3">
        <select name="dependencia" id="dependencia" class="form-control"></select>
      </div>
</div>
<div class="form-group">
  <label for="remitente" class="col-lg-3 control-label">Remitente:</label>
      <div class="col-lg-3">
        <?php echo form_input($Remitente); ?>
      </div>
</div>
<!--
<div class="form-group">
  <label for="celular" class="col-lg-3 control-label">Celular:</label>
      <div class="col-lg-3">
        <?php echo form_input($Celular); ?>
      </div>
</div>
-->
<div class="form-group">
  <label for="asunto" class="col-lg-3 control-label">Asunto:</label>
    <div class="col-lg-3">
      <?php echo form_textarea($Asunto); ?>
    </div>
</div>
<div class="form-group">
  <label for="area" class="col-lg-3 control-label">Area:</label>
      <div class="col-lg-3">
        <select name="area" id="area" class="form-control"></select>
      </div>
</div>
<div class="form-group">
  <label for="estado" class="col-lg-3 control-label">Estado:</label>
    <div class="col-lg-3">
     <?php echo  form_dropdown('estado', $Estado, set_value('estado',@$exp[0]->estado_exp),'class="form-control" id="estado"'); ?>
    </div>
</div>
<div class="form-group">
      <label for="observaciones" class="col-lg-3 control-label">Observaciones:</label>
      <div class="col-lg-3">
        <?php echo form_input($Observaciones); ?>
      </div>
  </div>  
  <div class="form-group">
    <div class="col-lg-offset-3 col-lg-10">
      <button type="button" onclick="regresar()" class="btn btn-primary"><span class="glyphicon glyphicon-circle-arrow-left"></span> Regresar</button>
      <button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-saved"></span> Guardar </button>
      <?php if($titulo=="Nuevo Expediente"){ ?>
      <button type="reset" class="btn btn-primary"><span class="glyphicon glyphicon-plus"></span> Nuevo</button>
      <?php } ?>
    </div>
  </div>
  <hr/>
</form>		
<script type="text/javascript">
  
</script>
