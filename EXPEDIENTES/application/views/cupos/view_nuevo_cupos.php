<input type="hidden" value="<?php echo @$cupo[0]->id; ?>" id="id" name="id"> 
<input type="hidden" name="loca" id="loca" value="<?php echo @$cupo[0]->id_localidad; ?>"> 
<input type="hidden" name="progra" id="progra" value="<?php echo @$cupo[0]->id_programa; ?>"> 
<input type="hidden" name="nuevo" id="nuevo" value=""> 

<script type="text/javascript">
  var baseurl = "<?php echo base_url(); ?>";
  
  var idloca           = 0;
   var idprogra        = 0;
  var ids           = document.getElementById("id").value;
  ids               = parseInt(ids.length);
  
  if(ids==0){
        idloca       = 0;
		idprogra      = 0;
		document.getElementById("nuevo").value="no";	
  }else{
    
    idloca           = document.getElementById("loca").value;
	 idprogra           = document.getElementById("progra").value;
	document.getElementById("nuevo").value="si";	
	  
  }
  
  
 
  function regresar(){
    window.location="<?php echo base_url()?>cupos";
  }
</script>
<?php

  $Cuposs       = array(
  'name'        => 'Cuposs',
  'id'          => 'Cuposs',
  'size'        => 50,
  'value'       => set_value('Cuposs',@$cupo[0]->cupo),
  'type'        => 'text',
  'class'       => 'form-control',
   );
  
   $Diferencia       = array(
  'name'        => 'Diferencia',
  'id'          => 'Diferencia',
  'size'        => 50,
  'value'       => set_value('Diferencia',@$cupo[0]->diferencia_cupo),
  'type'        => 'text',
  'class'       => 'form-control',
   );

 
?>

<h1 class="page-header" onload ="control();"><span class="glyphicon glyphicon-th-list"></span> <?php echo $titulo; ?></h1>
<div id="mensaje" name="mensaje"></div>
<body onload ="control();">
<form class="form-horizontal" name="formulario" id="formulario" role="form">
  <input type="hidden" value="0" id="validamail" name="validamail">
  <input type="hidden" value="0" id="validarfc" name="validarfc">
  <div class="form-group">
  
    
  
  <div class="form-group">
    <label for="programa" class="col-lg-3 control-label">Programa:</label>
    <div class="col-lg-3"> 
	<select name="programa" id="programa" class="form-control" onchange="traercupodepro()";></select>
  	</div>
	<div name="div1" id="div1"> Cupo Programa<input type="text" disabled="disabled" name="cupo" id="cupo"   class="" size="2" value="" /></div>
  </div>
  
   <div class="form-group">
    <label for="localidad" class="col-lg-3 control-label">Localidad:</label>
    <div class="col-lg-3"> 
	<select name="localidad" id="localidad" class="form-control" onchange="traerCuposRestantes();"> </select> 
  	</div>
	<div name="div2" id="div2"> Cupo Asignable<input type="text" disabled="disabled" name="cupoTotal" id="cupoTotal"   class="" size="2" value="" /></div>
  </div>
  
 <div class="form-group">
    <label for="Cuposs" class="col-lg-3 control-label">Cupos:</label>
    <div class="col-lg-3">
      <?php echo form_input($Cuposs); ?>
    </div>
	<div ><button type="button" onclick="verificar()" class="btn btn-success">confirmar</button></div>
  </div>
  
 
 <div class="form-group">
    <label for="Diferencia" class="col-lg-3 control-label">Cupos Asignados:</label>
    <div class="col-lg-3">
      <?php echo form_input($Diferencia); ?>
    </div>
  </div>
 
 
 
  <div class="form-group">
    <div class="col-lg-offset-3 col-lg-10">
      <button type="button" onclick="regresar()" class="btn btn-default">Regresar</button>
      <button type="submit" class="btn btn-primary" name="btnGuardar" id="btnGuardar"><span class="glyphicon glyphicon-saved"></span> Guardar Programa</button>
      <?php if($titulo=="Nuevo Cupo"){ ?>
      <button type="reset" class="btn btn-default">Nuevo</button>
      <?php } ?>
    </div>
  </div>
  <hr/>
</form>	


 <script src="<?php echo base_url();?>js/JsonCupos.js"></script>
</body>