<?php
date_default_timezone_set('America/Argentina/Jujuy');
?>


<style type="text/css">
th, td { white-space: nowrap; }
    div.dataTables_wrapper {
        width: 100%;
        margin: 0 auto;
    }
</style>
<script type="text/javascript">
var baseurl = "<?php echo base_url(); ?>";
var currentLocation = window.location;
/**
 * Elimina el registro de una denuncia
 * @param type reclamo  denuncia
 * @param type id 	   identificador de una denuncia
 * @return type
 */
function eliminarResolucion(resolucion, id){
    confirmar=confirm("Realmente desea eliminar a " + resolucion + "? Una vez eliminado NO podrá ser recuperado"); 

    if (confirmar){
    	 document.getElementById('mensaje').innerHTML = "<div class='modal1'><div class='center1'> <center> <img src='"+ baseurl +"/img/gif-load.gif'> Eliminando resolucion...</center></div></div>";
    	 var Resolucion		 = new Object();
		 Resolucion.Id      	 = id;
		 Resolucion.Resolucion   = resolucion;
		
		var DatosJson = JSON.stringify(Resolucion);
		$.post(currentLocation + '/deleteResolucion', //Funcion del controlador
		{ 
			ResolucionPost: DatosJson
		},
		function(data, textStatus) {
			//
			$("#mensaje").html(data.error_msg);
		}, 
		"json"		
		);
    } else{
    } 
  }
  
</script>
<h1 class="page-header"><span class="glyphicon glyphicon-list-alt"></span> Resolucion</h1>
<div id="mensaje"></div>
<p align="right">
 	 <a href="resolucion/nuevo">
 	 	<button type="button" class="btn btn-primary"><span class="glyphicon glyphicon-ok"></span> Nueva Resolucion</button>
 	 </a>  
 	 </p>
 	 <br/>
	<table id="resolucion" border="0" cellpadding="0" cellspacing="0" width="100%" class="pretty">
		<thead>
			<tr>
				<th></th>
				<th>Codigo</th> <!--Id -->
				<th>Año</th> <!--Fecha -->
				<th>Asunto</th>	<!--Denunciante -->
				<th>Gobierno</th>	<!--Cooperativa-->
				<!--<th>Denuncia</th>-->
				<th>Foto Resolucion</th>
				<th>Observaciones</th>
			</tr>
		</thead>
		<tbody>
			<?php
				if($resolucion){
					foreach($resolucion as $Resolucion){
						$idResolucion    = base64_encode($Resolucion->id_resolucion);
						echo '<tr>';
						echo '<td>';
						echo '<a href="resolucion/editarResolucion/'.$idResolucion.'"><button type="button" title="Editar Resolucion" class="btn btn-success btn-xs"><span class="glyphicon glyphicon-edit"></span></button></a> &nbsp;';
						echo '<a href="resolucion/view_img/'.$idResolucion.'"><button type="button" title="Cargar Resolucion" class="btn btn-primary btn-xs"><span class="glyphicon glyphicon-open"></span></button></a> &nbsp;';
						echo '<a href="resolucion/view_mostrar/'.$idResolucion.'"><button type="button" title="Vista Resolucion" class="btn btn-success btn-xs"><span class="glyphicon glyphicon-picture"></span></button></a> &nbsp;';
						?>
						<button type="button" onclick="eliminarResolucion('<?php echo $Resolucion->codigo; ?>','<?php echo $idResolucion; ?>');" title="Eliminar Resolucion" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span></button>
						<?php
						echo '</td>';
						echo '<td>'.$Resolucion->codigo.'</td>';
						echo '<td>'.$Resolucion->año.'</td>';
						echo '<td>'.$Resolucion->asunto.'</td>';

						$Gobierno="";		
						$i=$Resolucion->gobierno;
				        switch ($i) 
						{
				          case 0:
				                $Gobierno= "";
				                break;
				          case 1:
				                $Gobierno= "Secretaria de Gobierno Instituto de Vivienda";
				                break;
				          case 2:
				                $Gobierno= "Secretaria de Ordenamiento de Tierra y Vivienda";
				                break;
					      case 3:
				                $Gobierno= "Otros";
				                break;
				    	};
				    	echo '<td>'.$Gobierno.'</td>';
						echo '<td>'.$Resolucion->foto_resolucion.'</td>';
						echo '<td>'.$Resolucion->obs.'</td>';
						
						echo '</tr>';
					}
				}else{
					echo '<tr><td colspan=5><center>No Existe Informacion</center></td></tr>';
				}
			?>
		</tbody>
	</table>
<script type="text/javascript">

            $(document).ready(function() {
    $('#resolucion').dataTable( {
        "scrollX": false
    } );
} );

</script>