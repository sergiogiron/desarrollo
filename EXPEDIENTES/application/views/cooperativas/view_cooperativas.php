<?php
date_default_timezone_set('America/Argentina/Jujuy');
?>
<style type="text/css">
th, td { white-space: nowrap; }
    div.dataTables_wrapper {
        width: 100%;
        margin: 0 auto;
    }
</style>
<script type="text/javascript">
var baseurl = "<?php echo base_url(); ?>";
var currentLocation = window.location;
function EliminarCoop(socio, id,matricula){
    confirmar=confirm("Eliminar Cooperativas: " + socio + ", Recuerda Una vez Eliminado No podras Recuperarlo"); 

    if (confirmar){
    	document.getElementById('mensaje').innerHTML = "<div class='modal1'><div class='center1'> <center> <img src='"+ baseurl +"/img/gif-load.gif'> Eliminando Coopertiva...</center></div></div>";
    	 var Coope 		 = new Object();
		Coope.Id      	 = id;
		Coope.Matricula      = matricula;
		var DatosJson = JSON.stringify(Coope);
		$.post(currentLocation + '/deletecoope',
		{ 
			MiCoops: DatosJson
		},
		function(data, textStatus) {
			//
			$("#mensaje").html(data.error_msg);
		}, 
		"json"		
		);
    } else{
    } 
  }
  
</script>
<!-- Exportar a excel -->
<script>
var currentLocation = window.location;
//alert(currentLocation);
 function exportarr(){
  $.post(currentLocation + '/exportar');
  }
</script>




<h1 class="page-header"><span class="glyphicon glyphicon-list-alt"></span> Cooperativas</h1>

<div id="mensaje"></div>

	<p align="right">
 <!-- <button type="button" onclick="exportarr()" class="btn btn-default">Exportar Excel</button>  -->
		
 	 <a href="cooperativas/nuevo">
	        
	  	 	<button type="button" class="btn btn-primary"><span class="glyphicon glyphicon-ok"></span> Nueva Cooperativa</button>
	</a>  
   </p>
   
 	 <br/>
 	 
	<table id="cooperativas" border="0" cellpadding="0" cellspacing="0" width="100%" class="pretty">
		<thead>
			<tr>

				<th></th>
				<th>Reg.</th>
			   <th>Estado</th> 
			   <th>Resolucion</th> 
			   <th>Matricula</th>
		  	<th>Razon Social</th>
			<!--	<th>Calle</th>
				<th>Nro</th>-->
	<!--			<th>TIPO</th>  -->
				<th>Localidad</th>
           <!--    <th>Presidente</th>
		 		<th>Telefono</th>-->
				<th>Celular</th>   
				
			</tr>
		</thead>
		<tbody>
			<?php
	   
			
			
			
				if($cooperativas){
					foreach($cooperativas as $coop){
						$codigo       = base64_encode($coop->matricula);
						$id           = base64_encode($coop->id);

						echo '<tr>';
						echo '<td>';					
												
						
						echo '<a href="cooperativas/editarCooperativa/'.$id.'/'.$codigo.'"><button type="button" title="Editar Cooperativa" class="btn btn-success btn-xs"><span class="glyphicon glyphicon-edit"></span></button></a> &nbsp;'; 
					?>	
						<a ><button onclick="EliminarCoop('<?php echo $coop->razonsocial; ?>','<?php echo $id; ?>','<?php echo $codigo; ?>');" type="button" title="Eliminar Cooperativa" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span></button></a> 
					<?php	
						echo '<a href="cooperativas/Requisitos/'.$id.'/'.$codigo.'"><button type="button" title="Requisitos" class="btn btn-success btn-xs"><span class="glyphicon glyphicon-list-alt"></span></button></a> &nbsp;';
					   echo '<a href="cooperativas/Credencial/'.$id.'"><button type="button" title="Credencial Cooperativa" class="btn btn-success btn-xs"><span class="glyphicon glyphicon-credit-card"></span></button></a> &nbsp;';
				        echo '<a href="cooperativas/ImprimeCooperativas/'.$id.'"><button type="button" title="Cooperativas" class="btn btn-success btn-xs"><span class="glyphicon glyphicon-print"></span></button></a> &nbsp;';
				 ?>
						
						
						
						<?php
						echo '</td>';
						echo '<td>'.str_pad((int) $coop->numero,4,"0",STR_PAD_LEFT).'</td>';
						
						 $Tipo="";		
		$i=$coop->estado;
        switch ($i) 
	{
         case 0:
                $Estado= "Elegir";
                break;
		 case 1:
                $Estado= "Inscripcion";
                break;
          case 2:
                $Estado= "En Tramite";
                break;
		  case 3:
                $Estado= "Habilitado";
                break;	
          case 4:
                $Estado= "Denuncia";
                break;
		  case 5:
                $Estado= "Suspensión";
                break;	
          case 6:
                $Estado= "Baja";
                break;	
				
				
				
				
		  };			
						
						echo '<td>'.$Estado.'</td>';
						echo '<td>'.$coop->resolucion.'</td>';
						echo '<td>'.$coop->matricula.'</td>';
						echo '<td>'.$coop->razonsocial.'</td>';
						
						 $Tipo="";		
		$i=$coop->tipo;
        switch ($i) 
	{
          case 0:
                $Tipo= "---Elegir Opción---";
                break;
          case 1:
                $Tipo= "Agropecuario";
                break;
          case 2:
                $Tipo= "Apicola";
                break;
	      case 3:
                $Tipo= "Consumo";
                break;
          case 4:
                $Tipo= "Credito";
                break;
          case 5:
                $Tipo= "Trabajo";
                break;
		  case 6:
                $Tipo= "Servicio Publico";
                break;
          case 7:
                $Tipo= "Vivienda";
                break;
          case 8:
                $Tipo= "Seguros";
                break;
	      case 9:
                $Tipo= "Granjero o Avicola";
                break;
          case 10:
                $Tipo= "Frutihorticola";
                break;
          case 11:
                $Tipo= "Tamberas";
                break;
		  case 12:
                $Tipo= "Mineras";
                break;
          case 13:
                $Tipo= "Vitivinicolas";
                break;
	      case 14:
                $Tipo= "Exportacion";
                break;
          case 15:
                $Tipo= "Forestales";
                break;
          case 16:
                $Tipo= "Pesqueras";
                break;
		  case 17:
                $Tipo= "Carniceras";
                break;
          case 18:
                $Tipo= "Ganaderas";
                break;
	      case 19:
                $Tipo= "Provision";
                break;
          case 20:
                $Tipo= "Otros";
                break;
 
    };
						
						
						
						
						
				//		echo '<td> '.$Tipo.'</td>';
					//	echo '<td>'.$coop->nro.'</td>'; 
					//	echo '<td>'.$coop->barrio.'</td>';
						echo '<td>'.$coop->nombre_localidad.'</td>';
				//		echo '<td>'.$coop->presidente.'</td>'; 
				//		echo '<td>'.$coop->telefono.'</td>';
					     echo '<td>'.$coop->celular.'</td>';
					
						
						
						
						
						
						echo '</tr>';
					}
				}else{
					echo '<tr><td colspan=9><center>No Existe Informacion</center></td></tr>';
				}
			?>
		</tbody>
	</table>
<script type="text/javascript">

    $(document).ready(function() {
    $('#cooperativas').dataTable( {
        "scrollX": true
    } );
} );

</script>