<input type="hidden" name="id" id="id" value="<?php echo @$Coope[0]->id; ?>"> 
<!-- <input type="text" name="max" id="max" value="<?php echo @$max[0]->max; ?>"> -->
<input type="hidden" name="loca" id="loca" value="<?php echo @$Coope[0]->id_localidad; ?>"> 


<script type="text/javascript">
  var baseurl = "<?php echo base_url(); ?>";
  
  var idloca        = 0;
   var idadhe        = 0;
  var ids           = document.getElementById("id").value;
  ids               = parseInt(ids.length);
  
  if(ids==0){
        idloca       = 0;
		idadhe      = 0;
  }else{
    
    idloca           = document.getElementById("loca").value;
	 idadhe           = document.getElementById("adhe").value;
  }
  function regresar(){
    window.location="<?php echo base_url()?>cooperativas";
  }
</script>


<?php
  if($titulo =="Nueva Cooperativa")
  {
    

 //NRO REGISTRO nuevo
  $Registro = array(
  'name'        => 'numero',
  'id'          => 'numero',
  'size'        => 50,
   'value'       => set_value('numero',@$max[0]->max) + 1,
  'type'        => 'text',
  'class'       => 'form-control',
  );
  
 }else{ 
  
   //NRO REGISTRO editar
  $Registro = array(
  'name'        => 'numero',
  'id'          => 'numero',
  'size'        => 50,
  'value'       => set_value('numero',@$Coope[0]->numero),
  'type'        => 'text',
  'class'       => 'form-control',
  );
  
  }
  
  
  

  //Matriculas
  $Matricula = array(
  'name'        => 'matricula',
  'id'          => 'matricula',
  'size'        => 50,
  'value'       => set_value('matricula',@$Coope[0]->matricula),
  'type'        => 'text',
  'class'       => 'form-control',

  );
  //Cuit
  $Cuit         = array(
  'name'        => 'cuit',
  'id'          => 'cuit',
  'size'        => 50,
  'value'       => set_value('cuit',@$Coope[0]->cuit),
  'type'        => 'text',
   'class'       => 'form-control',
  );
  //
  $Resolucion   = array(
  'name'        => 'resolucion',
  'id'          => 'resolucion',
  'size'        => 50,
  'value'       => set_value('resolucion',@$Coope[0]->resolucion),
  'type'        => 'text',
  'class'       => 'form-control',
  );
 
   $Cbu   = array(
  'name'        => 'cbu',
  'id'          => 'cbu',
  'size'        => 50,
  'value'       => set_value('cbu',@$Coope[0]->cbu),
  'type'        => 'text',
  'class'       => 'form-control',
  );
   $Banco  = array(
  '0'             => '---Elegir Opción---',
  '1'             => 'Banco Macro Jujuy',
  '2'             => 'Banco Nacion',
  '3'             => 'Banco MasVentas',
  '4'             => 'Banco Santiago del Estero',
  '5'             => 'Banco Credicoop Coop. Ltdo.C',
     
 );
  
    
   $Razonsocial      = array(
  'name'        => 'razonsocial',
  'id'          => 'razonsocial',
  'size'        => 50,
  'value'       => set_value('razonsocial',@$Coope[0]->razonsocial),
  'type'        => 'text',
  'class'       => 'form-control',
  );
    $Domicilio      = array(
  'name'        => 'domicilio',
  'id'          => 'domicilio',
  'size'        => 50,
   'value'       => set_value('domicilio',@$Coope[0]->domicilio),
  'type'        => 'text',
  'class'       => 'form-control',
  );
   
      
  $Telefono      = array(
  'name'        => 'telefono',
  'id'          => 'telefono',
  'size'        => 50,
  'value'       => set_value('telefono',@$Coope[0]->telefono),
  'type'        => 'text',
  'class'       => 'form-control',
  
  );
   $Celular       = array(
  'name'        => 'celular',
  'id'          => 'celular',
  'size'        => 50,
  'value'       => set_value('celular',@$Coope[0]->celular),
  'type'        => 'text',
  'class'       => 'form-control',
  
  ); 
     $Tipo  = array(
  '0'             => '---Elegir Opción---',
  '1'             => 'Agropecuario',
  '2'             => 'Apicola',
  '3'             => 'Consumo',
  '4'             => 'Credito',
  '5'             => 'Trabajo',
  '6'             => 'Servicio Publico',
  '7'             => 'Vivienda',
  '8'             => 'Seguros',
  '9'             => 'Granjero o Avicola',
 '10'             => 'Frutihorticola',
 '11'             => 'Tamberas',
 '12'             => 'Mineras',
 '13'             => 'Vitivinicolas',
 '14'             => 'Exportacion',
 '15'             => 'Forestales',
 '16'             => 'Pesqueras',
 '17'             => 'Carniceras',
 '18'             => 'Ganaderas',
 '19'             => 'Provision',
 '20'             => 'Otros',
  
    );
  
    
    $Estado  = array(
  '0'             => '---Elegir Opción---',
  '1'             => 'Habilitado',
  '2'             => 'En Tramite',
  '3'             => 'Denuncia',
 );

  
 
?>
 <script src="<?php echo base_url();?>js/JsonCooperativas.js"></script> 
<h1 class="page-header"><span class="glyphicon glyphicon-th-list"></span> <?php echo $titulo; ?></h1>
<div id="mensaje"></div> 
<form class="form-horizontal" name="formulario" id="formulario" role="form">
  
  <div class="form-group">
    <label for="numero" class="col-lg-3 control-label">Registro :</label>
    <div class="col-lg-3">
      <?php echo form_input($Registro); ?> 
    </div>
  </div>
  
  
  
  <div class="form-group">
    <label for="razonsocial" class="col-lg-3 control-label">Razon Social:</label>
    <div class="col-lg-3">
      <?php echo form_input($Razonsocial); ?> 
    </div>
  </div>
  
  <div class="form-group">
    <label for="matricula" class="col-lg-3 control-label">Matricula:</label>
    <div class="col-lg-3">
    <?php echo form_input($Matricula); ?>
    </div>
  </div>
  
  <div class="form-group">
    <label for="cuit" class="col-lg-3 control-label">Cuit:</label>
    <div class="col-lg-3">
      <?php  echo form_input($Cuit); ?>
    </div>
  </div>

 <div class="form-group">
    <label for="resolucion" class="col-lg-3 control-label">Resolucion:</label>
    <div class="col-lg-3">
    <?php echo form_input($Resolucion); ?>
    </div>
  </div>
  
 <div class="form-group">
    <label for="cbu" class="col-lg-3 control-label">CBU:</label>
    <div class="col-lg-3">
    <?php echo form_input($Cbu); ?>
    </div>
  </div>
 
 <div class="form-group">
    <label for="banco" class="col-lg-3 control-label">Banco:</label>
    <div class="col-lg-3">
	 <?php echo  form_dropdown('banco', $Banco, set_value('banco',@$Coope[0]->banco),'class="form-control" id="banco"'); ?>
      </div>
  </div>
 

  

  <div class="form-group">
    <label for="domicilio" class="col-lg-3 control-label">Domicilio:</label>
    <div class="col-lg-3">
      <?php echo form_input($Domicilio); ?> 
    </div>
  </div>
  

 <div class="form-group">
    <label for="localidad" class="col-lg-3 control-label">Localidad:</label>
    <div class="col-lg-3"> 
	<select name="localidad" id="localidad" class="form-control"></select>
  	</div>
  </div>


   <div class="form-group">
    <label for="telefono" class="col-lg-3 control-label">Telefono:</label>
    <div class="col-lg-3">
      <?php echo form_input($Telefono); ?> 
    </div>
  </div>  

   <div class="form-group">
    <label for="celular" class="col-lg-3 control-label">Celular:</label>
    <div class="col-lg-3">
      <?php echo form_input($Celular); ?> 
    </div>
  </div> 
 

  <div class="form-group">
    <label for="tipo" class="col-lg-3 control-label">Tipo:</label>
    <div class="col-lg-3">
	 <?php echo  form_dropdown('tipo', $Tipo, set_value('tipo',@$Coope[0]->tipo),'class="form-control" id="tipo"'); ?>
      </div>
  </div>

   
  
  <div class="form-group">
    <label for="estado" class="col-lg-3 control-label">Estado:</label>
    <div class="col-lg-3">
	 <?php echo  form_dropdown('estado', $Estado, set_value('estado',@$Coope[0]->estado),'class="form-control" id="estado"'); ?>
      </div>
  </div>
  
   

   <div class="form-group">
    <div class="col-lg-offset-3 col-lg-10">
      <button type="button" onclick="regresar()" class="btn btn-default">Regresar</button>
      <button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-saved"></span> Guardar Coopeativa</button>
      <?php if($titulo=="Nueva Cooperativa"){ ?>
      <button type="reset" class="btn btn-default">Nuevo</button>
      <?php } ?>
    </div>
  </div>
  <hr/>
</form>   

