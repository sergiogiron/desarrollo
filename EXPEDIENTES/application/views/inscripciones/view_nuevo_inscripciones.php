<input type="hidden" id="id" name="id"value="<?php echo @$insc[0]->id; ?>" > 
<input type="hidden" name="loca" id="loca" value="<?php echo @$insc[0]->id_localidad; ?>"> 
<input type="hidden" name="progra" id="progra" value="<?php echo @$insc[0]->id_programa; ?>"> 
<script type="text/javascript">
  var baseurl = "<?php echo base_url(); ?>";

  var idloca        = 0;
  var idprogra     = 0;
  var ids           = document.getElementById("id").value;
  ids               = parseInt(ids.length);
  if(ids==0){
        idloca      = 0;
		 idprogra       = 0;
  }else{
        idloca      = document.getElementById("loca").value;
        idprogra    = document.getElementById("progra").value;
 }
 
  function regresar(){
    window.location="<?php echo base_url()?>inscripciones";
  }
</script>
<?php
$Documento    = array(
  'name'        => 'Documento',
  'id'          => 'Documento',
  'size'        => 50,
  'value'       => set_value('Documento',@$insc[0]->documento),
  'type'        => 'text',
  'onkeypress'  => "return  validarNumeros(event);",
  'class'       => 'form-control',
  
 
  );
//Nombre
  $Nombre       = array(
  'name'        => 'Nombre',
  'id'          => 'Nombre',
  'size'        => 50,
  'value'       => set_value('Nombre',@$insc[0]->nombre_beneficios),
  'type'        => 'text',
  'class'       => 'form-control',
  //'style'       => 'text-transform:uppercase',
  'onkeypress'  => 'return validarn(event);',
  );
   $Barrio          = array(
  'name'        => 'Barrio',
  'id'          => 'Barrio',
  'size'        => 50,
  'value'       => set_value('Barrio',@$insc[0]->barrio),
  'type'        => 'text',
  'class'       => 'form-control',
  );
   $Mza           = array(
  'name'        => 'Mza',
  'id'          => 'Mza',
  'size'        => 50,
  'value'       => set_value('Mza',@$insc[0]->mza),
  'type'        => 'text',
  'class'       => 'form-control',
  );
   $Lote           = array(
  'name'        => 'Lote',
  'id'          => 'Lote',
  'size'        => 50,
  'value'       => set_value('Lote',@$insc[0]->lote),
  'type'        => 'text',
  'class'       => 'form-control',
  );
  $Estado  = array(
   '1'             => 'Incripción',
   '2'             => 'Habilitado',
   '3'             => 'Adjudicado',
   '4'             => 'Pendiente',
   '5'             => 'Observado', 
 );
   $Obs     = array(
  'name'        => 'Obs',
  'id'          => 'Obs',
  'size'        => 50,
   'rows'        => '4',
 'value'       => set_value('Obs',@$insc[0]->obs),
  'type'        => 'text',
  
  'class'       => 'form-control',
  );
   //'style'       => 'text-transform:uppercase',
  //'onkeypress'  => 'return letras(event)',
 // 'onkeypress'  => "return  SoloNumerosDecimales3(event, '0.0', 4, 2);",
?>
<script src="<?php echo base_url();?>js/JsonInscripciones.js"></script>
<h1 class="page-header"><span class="glyphicon glyphicon-th-list"></span> <?php echo $titulo; ?></h1>
<div id="mensaje"></div>
<form class="form-horizontal" name="formulario" id="formulario" role="form">
  <input type="hidden" value="0" id="validamail" name="validamail">
  <input type="hidden" value="0" id="validarfc" name="validarfc">
  <div class="form-group">

    <div class="form-group">
    <label for="Documento" class="col-lg-3 control-label">Documento:</label>
    <div class="col-lg-3">
      <?php echo form_input($Documento); ?>
    </div>
  </div>

    <label for="Nombre" class="col-lg-3 control-label">Nombre y Apellido:</label>
    <div class="col-lg-3">
      <?php echo form_input($Nombre); ?>
    </div>
  </div>
 
  <div class="form-group">
    <label for="localidad" class="col-lg-3 control-label">Localidad:</label>
    <div class="col-lg-3">
      <select name="localidad"id="localidad" class="form-control"></select>
    </div>
  </div>

  <div class="form-group">
    <label for="Barrio" class="col-lg-3 control-label">Barrio:</label>
    <div class="col-lg-3">
      <?php echo form_input($Barrio); ?>
    </div>
  </div>
 
   <div class="form-group">
    <label for="Mza" class="col-lg-3 control-label">Mza:</label>
    <div class="col-lg-3">
      <?php echo form_input($Mza); ?>
    </div>
  </div>
  
  <div class="form-group">
    <label for="Lote" class="col-lg-3 control-label">Lote:</label>
    <div class="col-lg-3">
      <?php echo form_input($Lote); ?>
    </div>
  </div>

   <div class="form-group">
    <label for="programa" class="col-lg-3 control-label">Programa:</label>
    <div class="col-lg-3"> 
	<select name="programa" id="programa" class="form-control"></select>
  	</div>
  </div>
  
  
  
  <div class="form-group">
    <label for="Estado" class="col-lg-3 control-label">Estado:</label>
    <div class="col-lg-3">
   <?php echo  form_dropdown('Estado', $Estado, set_value('Estado',@$insc[0]->estado),'class="form-control" id="Estado"'); ?>
      </div>
  </div>

   <div class="form-group">
    <label for="Obs" class="col-lg-3 control-label">Obs:</label>
    <div class="col-lg-3">
      <?php echo form_input($Obs); ?>
    </div>
  </div>
  
  <div class="form-group">
    <div class="col-lg-offset-3 col-lg-10">
      <button type="button" onclick="regresar()" class="btn btn-primary"><span class="glyphicon glyphicon-circle-arrow-left"></span> Regresar</button>
      <button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-saved"></span> Guardar </button>
      <?php if($titulo=="Nuevo Beneficiario"){ ?>
      <button type="reset" class="btn btn-primary"><span class="glyphicon glyphicon-plus"></span> Nuevo</button>
      <?php } ?>
    </div>
  </div>
  <hr/>
</form>		
<script type="text/javascript">
  
</script>
