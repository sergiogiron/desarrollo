<input type="hidden" id="id" name="id" value="<?php echo @$denuncia[0]->id_denuncia; ?>" >
<input type="hidden" id="coope"name="coope" value="<?php echo @$denuncia[0]->id_cooperativa; ?>" >
 
<script type="text/javascript">

  var baseurl = "<?php echo base_url(); ?>";

  var idcoope        = 0;
  var ids       = document.getElementById("id").value;
  ids           = parseInt(ids.length);
  
  if(ids==0){
        idcoope      = 0;
       
      }else{
     idcoope     = document.getElementById("coope").value;      
  }

 /**
  * Función que redirecciona la vista a programas
  * @return type
  */
  function regresar(){
    window.location="<?php echo base_url()?>denuncias";
  }

</script>

<?php
   $Denunciante      = array(
  'name'        => 'denunciante',
  'id'          => 'denunciante',
  'size'        => 50,
  'value'       => set_value('denunciante',@$denuncia[0]->denunciante),
  'type'        => 'text',
  'class'       => 'form-control',
  );




 $Reclamo       = array(
  'name'        => 'reclamo',
  'id'          => 'reclamo',
  'size'        => 50,
  'value'       => set_value('reclamo',@$denuncia[0]->reclamo),
  'type'        => 'text',
  'class'       => 'form-control',
   );



  $Fecha     = array(
  'name'        => 'fecha',
  'id'          => 'fecha',
  'size'        => 50,
  'value'       => set_value('fecha',@$denuncia[0]->fecha_denuncia),
  'type'        => 'date',
  'class'       => 'form-control',
   );

  $Imagen  = array(
  'name'             => 'imagen',
  'id'               => 'imagen',
  'size'             => 50,
  'value'            => set_value('imagen',@$denuncia[0]->foto_denuncia),
  'type'             => 'text',
  'class'            =>'form-control',
  );

  $Observaciones  = array(
  'name'             => 'observaciones',
  'id'               => 'observaciones',
  'size'             => 50,
  'value'            => set_value('observaciones',@$denuncia[0]->observaciones),
  'type'             =>'text',
  'class'            =>'form-control',
  );
?>

<script src="<?php echo base_url();?>js/JsonDenuncias.js"></script>
<h1 class="page-header"><span class="glyphicon glyphicon-th-list"></span> <?php echo $titulo; ?></h1>
<div id="mensaje"></div>
<form class="form-horizontal" name="formulario" id="formulario" role="form">


 
 <div class="form-group">
    <label for="fecha" class="col-lg-3 control-label">Fecha:</label>
    <div class="col-lg-3">
      <?php echo form_input($Fecha); ?>
    </div>
  </div>


<div class="form-group">
      <label for="Denunciante" class="col-lg-3 control-label">Denunciante:</label>
      <div class="col-lg-3">
        <?php echo form_input($Denunciante); ?>
      </div>
  </div>
  
   <div class="form-group">
    <label for="cooperativa" class="col-lg-3 control-label">Cooperativas Denunciada:</label>
    <div class="col-lg-3">
	<select name="cooperativa" id="cooperativa" class="form-control"></select>
       </div>
  </div>
  <div class="form-group">
    <label for="reclamo" class="col-lg-3 control-label">Denuncia:</label>
    <div class="col-lg-3">
      <?php echo form_textarea($Reclamo); ?>
    </div>
  </div>
   
  
 <!-- 
  <div class="form-group">
    <label for="imagen" class="col-lg-3 control-label"> Adjuntar Denuncia: </label>
    <div class="col-lg-3">
	  <?php echo form_upload('file') ?>
    </div>
  </div>
-->
  <div class="form-group">
      <label for="observaciones" class="col-lg-3 control-label">Observaciones:</label>
      <div class="col-lg-3">
        <?php echo form_input($Observaciones); ?>
      </div>
  </div>

  <div class="form-group">
    <div class="col-lg-offset-3 col-lg-10">
      <button type="button" onclick="regresar()" class="btn btn-primary"><span class="glyphicon glyphicon-circle-arrow-left"></span> Regresar</button>
      <button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-saved"></span> Guardar </button>
      <?php if($titulo=="Nueva Denuncia"){ ?>
      <button type="reset" class="btn btn-primary"><span class="glyphicon glyphicon-plus"></span> Nuevo</button>
      <?php } ?>
    </div>
  </div>
  <hr/>
</form>		
<script type="text/javascript">
  
</script>
