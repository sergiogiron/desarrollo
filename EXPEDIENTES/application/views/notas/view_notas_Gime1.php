<?php
date_default_timezone_set('America/Argentina/Jujuy');
?>


<style type="text/css">
th, td { white-space: nowrap; }
    div.dataTables_wrapper {
        width: 100%;
        margin: 0 auto;
    }
</style>
<script type="text/javascript">
var baseurl = "<?php echo base_url(); ?>";
var currentLocation = window.location;

function eliminarNota(nota, id){
    confirmar=confirm("Realmente desea eliminar Nota código:" + nota + "? Una vez eliminado NO podrá ser recuperado"); 

    if (confirmar){
    	 document.getElementById('mensaje').innerHTML = "<div class='modal1'><div class='center1'> <center> <img src='"+ baseurl +"/img/gif-load.gif'> Eliminando nota...</center></div></div>";
    	 var Nota		 = new Object();
		 Nota.Id      	 = id;
		 Nota.Nota       = nota;
		
		var DatosJson = JSON.stringify(Nota);
		$.post(currentLocation + '/deleteNota', //Funcion del controlador
		{ 
			NotaPost: DatosJson
		},
		function(data, textStatus) {
			//
			$("#mensaje").html(data.error_msg);
		}, 
		"json"		
		);
    } else{
    } 
  }
  
</script>
<h1 class="page-header"><span class="glyphicon glyphicon-list-alt"></span> Nota</h1>
<div id="mensaje"></div>
<p align="right">
 	 <a href="notas/nuevo">
 	 	<button type="button" class="btn btn-primary"><span class="glyphicon glyphicon-ok"></span> Nueva Nota</button>
 	 </a>  
 	 </p>
 	 <br/>
	<table id="nota" border="0" cellpadding="0" cellspacing="0" width="100%" class="pretty">
		<thead>
			<tr>
				<th></th>
				<th>Codigo</th>
				<th>Año</th>
				<th>Fecha</th>
				<th>Remitente</th>
				<th>Celular</th>
				<th>Motivo</th>
				<th>Tipo</th>
				<th>Estado</th>
				<th>Foto Nota</th>
				<th>Observaciones</th>
			</tr>
		</thead>
		<tbody>
			<?php
				if($nota){
					foreach($nota as $Nota){
						$idNota    = base64_encode($Nota->id_nota);
						echo '<tr>';
						echo '<td>';
						echo '<a href="notas/editarNota/'.$idNota.'"><button type="button" title="Editar Nota" class="btn btn-success btn-xs"><span class="glyphicon glyphicon-edit"></span></button></a> &nbsp;';
						echo '<a href="notas/view_img/'.$idNota.'"><button type="button" title="Cargar Nota" class="btn btn-primary btn-xs"><span class="glyphicon glyphicon-open"></span></button></a> &nbsp;';
						echo '<a href="notas/view_mostrar/'.$idNota.'"><button type="button" title="Vista Nota" class="btn btn-success btn-xs"><span class="glyphicon glyphicon-picture"></span></button></a> &nbsp;';
						?>
						<button type="button" onclick="eliminarNota('<?php echo $Nota->codigo; ?>','<?php echo $idNota; ?>');" title="Eliminar Nota" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span></button>
						<?php
						echo '</td>';
						echo '<td>'.$Nota->codigo.'</td>';
						echo '<td>'.$Nota->anio.'</td>';
						echo '<td>'.$Nota->fecha_nota.'</td>';
						echo '<td>'.$Nota->remitente.'</td>';
						echo '<td>'.$Nota->celular.'</td>';
						echo '<td>'.$Nota->motivo.'</td>';
						//echo '<td>'.$Resolucion->asunto.'</td>';

						$Tipo="";		
						$i=$Nota->tipo;
				        switch ($i) 
						{
				          case 0:
				                $Tipo= "";
				                break;
				          case 1:
				                $Tipo= "Cooperativa";
				                break;
				          case 2:
				                $Tipo= "Planes";
				                break;
					      case 3:
				                $Tipo= "Particular";
				                break;
				          case 4: 
				          		$Tipo="Gobierno";
				          		break;
				          case 5:
				          		$Tipo="Municipal";
				          		break;
				          case 6:
				          		$Tipo="Otros";
				          		break;
				    	};

						$Estado="";		
						$i=$Nota->estado;
				        switch ($i) 
						{
				          case 0:
				                $Estado= "";
				                break;
				          case 1:
				                $Estado= "Recibido";
				                break;
				          case 2:
				                $Estado= "Visado";
				                break;
					      case 3:
				                $Estado= "Historico";
				                break;
				          case 4: 
				          		$Estado="Otros";
				          		break;
				        };
				    	echo '<td>'.$Tipo.'</td>';
				    	echo '<td>'.$Estado.'</td>';
						echo '<td>'.$Nota->foto_nota.'</td>';
						echo '<td>'.$Nota->obs.'</td>';
						echo '</tr>';
					}
				}else{
					echo '<tr><td colspan=5><center>No Existe Informacion</center></td></tr>';
				}
			?>
		</tbody>
	</table>
<script type="text/javascript">

            $(document).ready(function() {
    $('#nota').dataTable( {
        "scrollX": false
    } );
} );

</script>