<input type="hidden" id="id" name="id" value="<?php echo @$pase[0]->id_pase; ?>" >
<input type="hidden" id="ar"name="ar" value="<?php echo @$pase[0]->id_area; ?>">

<script type="text/javascript">

  var baseurl = "<?php echo base_url(); ?>";

  var idarea       = 0;
  var ids           = document.getElementById("id").value;
  ids               = parseInt(ids.length);
  
  if(ids==0){
        idarea      = 0;
  }else{
        idarea          = document.getElementById("ar").value;
  }
  function regresar(){
    window.location="<?php echo base_url()?>expedientes/listarPases";
  }

function traerVistaPase() {
     window.location="<?php echo base_url()?>expedientes/listarPases";
}
</script>

<?php

if($titulo =="Nuevo Pase"){  //Titulo diferente al del controlador para que no sean iguales, de lo contrario no obtiene los campos
 //NRO CODIGO nuevo

  $Codigo = array(
  'name'        => 'codigo',
  'id'          => 'codigo',
  'size'        => 50,
  'value'       => set_value('codigo',@$max[0]->max) + 1,
  'type'        => 'text',
  'class'       => 'form-control',
  );
  $Fecha       = array(
  'name'        => 'fecha',
  'id'          => 'fecha',
  'size'        => 50,
  'value'       => set_value('fecha',date('d/m/y')), //formato para mostrar el año actual
  'type'        => 'text',
  'class'       => 'form-control',
   );
  $Asunto    = array(
  'name'        => 'asunto',
  'id'          => 'asunto',
  'size'        => 50,
  'value'       => set_value('asunto',@$pase[0]->asunto_exp),
  'type'        => 'text',
  'class'       => 'form-control',
   );
  $FechaPase     = array(
  'name'        => 'fechaPase',
  'id'          => 'fechaPase',
  'size'        => 50,
  'value'       => set_value('fechaPase',@$pase[0]->fecha_pase),
  'type'        => 'date',
  'class'       => 'form-control',
   );
  
  
      }else{ 
          $Codigo       = array(
          'name'        => 'codigo',
          'id'          => 'codigo',
          'size'        => 50,
          'value'       => set_value('codigo',@$pase[0]->codigo_exp),//Al editar, toma el codigo guardado
          'type'        => 'text',
          'class'       => 'form-control',
          );
          $Fecha       = array(
          'name'        => 'fecha',
          'id'          => 'fecha',
          'size'        => 50,
          'value'       => set_value('fecha',@$pase[0]->fecha_exp), //Al editar, toma el año de la bd
          'type'        => 'text',
          'class'       => 'form-control',
           );
          $Asunto    = array(
          'name'        => 'asunto',
          'id'          => 'asunto',
          'size'        => 50,
          'value'       => set_value('asunto',@$pase[0]->asunto_exp), //obtengo los campos de expedientes aqui el controller
          'type'        => 'text',
          'class'       => 'form-control',
           );
          
          $FechaPase     = array(
          'name'        => 'fechaPase',
          'id'          => 'fechaPase',
          'size'        => 50,
          'value'       => set_value('fechaPase',@$pase[0]->fecha_pase),
          'type'        => 'date',
          'class'       => 'form-control',
           );
          /*$Año       = array(
          'name'        => 'anio',
          'id'          => 'anio',
          'size'        => 50,
          'value'       => set_value('anio',@$nota[0]->anio), //Al editar, toma el año de la bd
          'type'        => 'text',
          'class'       => 'form-control',
           );*/
      }
  
  
  $Estado     = array(
  '0'             => '---Elegir Opción---',
  '1'             => 'Archivado',
  '2'             => 'En Reserva',
  '3'             => 'En Tramite',
  '4'             => 'Pendiente',
  '5'             => 'Terminado',
    );
  $Observaciones  = array(
  'name'             => 'observaciones',
  'id'               => 'observaciones',
  'size'             => 50,
  'value'            => set_value('observaciones',@$pase[0]->obs_pase),
  'type'             =>'text',
  'class'            =>'form-control',
  );
?>

<script src="<?php echo base_url();?>js/JsonPases.js"></script>
<h1 class="page-header"><span class="glyphicon glyphicon-th-list"></span> <?php echo $titulo; ?></h1>
<h4 class="page-header"><span class="glyphicon glyphicon-option-vertical"></span> Datos de Expedientes</h4>
<div id="mensaje"></div>
<form class="form-horizontal" name="formulario" id="formulario" role="form">

<div class="form-group">
    <label for="codigo" class="col-lg-3 control-label">Codigo:</label>
      <div class="col-lg-3">
        <?php echo form_input($Codigo); ?>
      </div>
</div>
<div class="form-group">
  <label for="fecha" class="col-lg-3 control-label">Fecha:</label>
      <div class="col-lg-3">
        <?php echo form_input($Fecha); ?>
      </div>
</div>
<div class="form-group">
  <label for="asunto" class="col-lg-3 control-label">Asunto:</label>
    <div class="col-lg-3">
      <?php echo form_textarea($Asunto); ?>
    </div>
</div>

<h4 class="page-header"><span class="glyphicon glyphicon-option-vertical"></span> Registro Pase</h4>

<div class="form-group">
  <label for="fechaPase" class="col-lg-3 control-label">Fecha de Pase:</label>
      <div class="col-lg-3">
        <?php echo form_input($FechaPase); ?>
      </div>
</div>

<div class="form-group">
  <label for="area" class="col-lg-3 control-label">Area:</label>
      <div class="col-lg-3">
        <select name="area" id="area" class="form-control"></select>
      </div>
</div>

<div class="form-group">
  <label for="estado" class="col-lg-3 control-label">Estado:</label>
    <div class="col-lg-3">
     <?php echo  form_dropdown('estado', $Estado, set_value('estado',@$pase[0]->estado_pase),'class="form-control" id="estado"'); ?>
    </div>
</div>

<div class="form-group">
      <label for="observaciones" class="col-lg-3 control-label">Observaciones:</label>
      <div class="col-lg-3">
        <?php echo form_input($Observaciones); ?>
      </div>
  </div>  
  <div class="form-group">
    <div class="col-lg-offset-3 col-lg-10">

      <button type="button" onclick="regresar()" class="btn btn-primary"><span class="glyphicon glyphicon-circle-arrow-left"></span> Regresar</button>
      <button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-saved"></span> Guardar </button>
      <?php if($titulo=="Nuevo Pase"){ ?>
      <button type="reset" class="btn btn-primary"><span class="glyphicon glyphicon-plus"></span> Nuevo</button>
      <?php } ?>
       
    </div>
  </div>
  <hr/>
</form>
  <div class="col-lg-offset-3 col-lg-10">
    <!-- Boton fuera del form para que no se ejecute la funcion guardar del js dos veces y se dupliquen los registros-->
    <button id="btnVista"  onclick="traerVistaPase()" class="btn btn-primary"><span class="glyphicon glyphicon-menu-hamburger"></span> Listado de Pases</button>
  </div>

<script type="text/javascript">
  
</script>
