<?php
date_default_timezone_set('America/Argentina/Jujuy');
?>


<style type="text/css">
th, td { white-space: nowrap; }
    div.dataTables_wrapper {
        width: 100%;
        margin: 0 auto;
    }
</style>
<script type="text/javascript">
var baseurl = "<?php echo base_url(); ?>";
var currentLocation = window.location;

function regresar(){
    window.location="<?php echo base_url()?>expedientes";
  }

function eliminarPase(pase,id){
    confirmar=confirm("Realmente desea eliminar Pase código:" + pase + "? Una vez eliminado NO podrá ser recuperado"); 

    if (confirmar){
     document.getElementById('mensaje').innerHTML = "<div class='modal1'><div class='center1'> <center> <img src='"+ baseurl +"/img/gif-load.gif'> Eliminando Pase...</center></div></div>";
    	 var Pase		 = new Object();
		 Pase.Id      	 =id;
		 Pase.Codigo     = pase;

	
		var DatosJson = JSON.stringify(Pase);
		$.post(baseurl +'pases/deletePase', //Funcion del controlador
		{ 
			PasePost: DatosJson
		},
		function(data, textStatus) {
			$("#mensaje").html(data.error_msg);
		}, 
		"json"		
		);
    } else{

    } 
  }
  
</script>
<h1 class="page-header"><span class="glyphicon glyphicon-list-alt"></span> Pases</h1>
<?php
        $baseurl= base_url();
      $exp=str_pad((int) $pase[0]->codigo_exp,4,"0",STR_PAD_LEFT);
		
echo '<h4 class="page-header">Nro. Expediente: <strong> '.$exp.'</strong></h4>';
//echo '<h4 class="page-header">Dependencia:<strong>      '.$pase[0]->nombre_dep.' </strong>  </h4>';
//echo '<h4 class="page-header">Remitente:<strong>        '.$pase[0]->remitente_exp.' </strong>  </h4>';
?>


<div id="mensaje"></div>
<p align="right">

 	  <!--<a href="<?php echo base_url();?>pases/nuevoPase"> //Tengo que enviar el codigo de exp, por eso trabajo de esa forma
 	 	<button type="button" class="btn btn-primary"><span class="glyphicon glyphicon-ok"></span> Nuevo Pase</button>
 	 
 	 </a>  !-->
 	 <?php

     //   $baseurl= base_url();

		 $codexp    = $pase[0]->codigo_exp;
		//print_r($pase);
 	 	echo '<a href="'.$baseurl.'pases/nuevoPase/'.$codexp.'"><button type="button" class="btn btn-primary"><span class="glyphicon glyphicon-ok"></span> Nuevo Pase</button></a> &nbsp;';
 	?>

 	 </p>
 	 <br/>
	<table id="pase" border="0" cellpadding="0" cellspacing="0" width="100%" class="pretty">
		<thead>
			<tr>
				<th></th>
				<th>Nro.</th>
				<th>Fecha</th>
				<th>Area</th>
				<th>Estado</th>
				<!--<th>Remitente</th>-->
				<th>Observaciones</th>
			</tr>
		</thead>
			<?php
//print_r($pase);
             $contador=1;
			 $baseurl = base_url();
				if($pase){
					foreach($pase as $Pase){
						$idPase    = base64_encode($Pase->id_pase);

						echo '<tr>';
						echo '<td>';
						echo '<a href="'.$baseurl.'pases/editarPase/'.$idPase.'"><button type="button" title="Editar Pase" class="btn btn-success btn-xs"><span class="glyphicon glyphicon-edit"></span></button></a> &nbsp;';
							?>
							<button type="button" onclick="eliminarPase('<?php echo $Pase->codigo_exp; ?>','<?php echo $idPase; ?>');" title="Eliminar Pase" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span></button>
							<?php
						echo '</td>';
						echo '<td>'.str_pad((int) $contador,4,"0",STR_PAD_LEFT).'</td>';
						$contador=$contador +1;
					//	echo '<td>''</td>'; //Doy formato 0000 al codigo_pase
						echo '<td>'.date_format(new DateTime ($Pase->fecha_pase),'d/m/Y').'</td>';
						//echo '<td>'.$Exp->remitente_exp.'</td>';
						echo '<td>'.$Pase->nombre_area.'</td>';
						$Estado="";		
						$i=$Pase->estado_pase;
				        switch ($i) 
						{
				          case 0:
				                $Estado= "";
				                break;
				          case 1:
				                $Estado= "Archivado";
				                break;
				          case 2:
				                $Estado= "En Reserva";
				                break;
					      case 3:
				                $Estado= "En Tramite";
				                break;
				          case 4: 
				          		$Estado="Pendiente";
				          		break;
				          case 5:
				          		$Estado="Terminado";
				          		break;
				    	};
						echo '<td>'.$Estado.'</td>';
						echo '<td>'.$Pase->obs_pase.'</td>';
						echo '</tr>';
					}
				}else{
					echo '<tr><td colspan=5><center>No Existe Informacion</center></td></tr>';
				}
			?>
		</tbody>


	</table>
	</br>
	<center>
				<button type="button" onclick="regresar()" class="btn btn-primary"><span class="glyphicon glyphicon-circle-arrow-left"></span> Volver a Expedientes</button>
			</center>
<script type="text/javascript">

            $(document).ready(function() {
    $('#pase').dataTable( {
        "scrollX": false
    } );
} );

</script>