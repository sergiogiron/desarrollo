<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
date_default_timezone_set('America/Argentina/Jujuy');
class instituto_c extends CI_Controller {
  function __construct(){
    parent::__construct();
    $this->load->model('instituto_c_model');
    $this->load->model('seguridad_model');

    $this->load->helper('date');
    $this->load->model('bitacoras_model');
    }
      
      public function index(){
            //Bitácora
            $RegistrarBitacora = array(
                'fecha'           => date('Y-m-d H:i:s'),
                'usuario'         => $this->session->userdata('ID'),
                'mensaje'         => "Ingresó a pantalla IVUJ" );
           $this->bitacoras_model->AgregarBitacoras($RegistrarBitacora);
          
              $url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
              $this->seguridad_model->SessionActivo($url);
              $this->load->view('constant');    //Carga datos constantes
              $this->load->view('view_header'); //Carga datos de encabezado
              $this->load->view('instituto/view_instituto_c');
              $this->load->view('view_footer'); 
           }

      public function buscarDoc(){

             $url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
             $this->seguridad_model->SessionActivo($url);
             $this->load->view('constant');
             $this->load->view('view_header');
             $filtro2   = $this->input->get("NumeroDoc");
			 
			 $RegistrarBitacora = array(
                'fecha'           => date('Y-m-d H:i:s'),
                'usuario'         => $this->session->userdata('ID'),
                'mensaje'         => "Ingresó a pantalla instituto $filtro2" );
              $this->bitacoras_model->AgregarBitacoras($RegistrarBitacora);
				 $data["matri"] = " ";
			 
             if($filtro2!=""){
			         $data["matri"] = $filtro2;
                    $idResult= $this->instituto_c_model->buscar($filtro2);
                   if (empty($idResult)!=true)
						{	
				          $bus =$this->instituto_c_model->BuscarAdj($idResult[0]->id);
						
				         if($bus!=null)
						   {
				               $data["Adj"] = $bus;
							  $data["mensaje"]=" ";
							   
				           }
				           else{
						      $data["Adj"] =null;
						      $data["mensaje"]="MATRICULA SIN  ADJUDICACION ";
                           
						   
				            }
				 
				        
			            } 
                     else{
					     $data["mensaje"]="MATRICULA SIN COOPERTIVA ";
                        
				          }     
 }
             else{
			     $data["mensaje"]="INGRESE UNA MATRICULA";
                
              }
             
			  $this->load->view('instituto/view_instituto_c',$data);
               $this->load->view('view_footer');
			 
			 
         }

      public function saveExpediente(){

         

      }
    





}