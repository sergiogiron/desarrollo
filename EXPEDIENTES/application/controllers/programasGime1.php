<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
date_default_timezone_set('America/Mexico_City');
class programas extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('seguridad_model');
		$this->load->model('programas_model');
		$this->load->model('bitacoras_model');
	}
	public function index(){
          $url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
          $this->seguridad_model->SessionActivo($url);
          /**/
          $this->load->view('constant');
          $this->load->view('view_header');
          $data['programas'] = $this->programas_model->ListarProgramas();
          $this->load->view('programas/view_programas', $data);
          $this->load->view('view_footer');
          
	}
     public function nuevo(){
          $url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
          $this->seguridad_model->SessionActivo($url);
          $this->load->view('constant');
          $this->load->view('view_header');
          $data['titulo'] = "Nuevo Programa";
          $this->load->view('programas/view_nuevo_programas', $data);
          $this->load->view('view_footer');
     }
     public function editarProgramas($id){
          $url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
          $this->seguridad_model->SessionActivo($url);
          $id =  base64_decode($id);
          $this->load->view('constant');
          $this->load->view('view_header');
          $data['programa'] = $this->programas_model->BuscaProgramas($id);
          $data['titulo'] = "Editar Programas";
          $this->load->view('programas/view_nuevo_programas', $data);
          $this->load->view('view_footer');
     }
     public function SaveProgra(){
	 
	      $url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
          $this->seguridad_model->SessionActivo($url);
          $Pro         = json_decode($this->input->post('ProgramasPost'));
          $response = array (
                    "estatus"   => false,
                    "campo"     => "",
                 "error_msg" => ""
         );
		 
		    //Bitácora
		/* $RegistrBitacoras = array(
		               'fecha'             => date('Y-m-d H:i:s'),
						'usuario'     		=> $this->session->userdata('ID'),
						'mensaje'	    	=> "progrmas" );
				   
			$this->bitacoras_model->AgregarBitacoras($RegistrBitacoras);
			//-//
		 
		 
		 */
		// $Pro->Nombre="unnombre";
		 
         if($Pro->Nombre==""){
               $response["campo"]     = "Nombre";
               $response["error_msg"]   = "<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>El Nombre del Programa es Obligatorio</div>";
               echo json_encode($response);
       
        }else{  
               if($Pro->Id==""){
                     $RegistraPrograma    = array(
                     'nombre_programa'     => $Pro->Nombre,
                     'tipo'                => $Pro->Tipo,
                    'cantidad'                => $Pro->Cantidad,
                );
				
                     $this->programas_model->SaveProgramas($RegistraPrograma);
                     $response["error_msg"]   = "<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Informacion Guardada Correctamente</div>";
                     echo json_encode($response);
               }
               if($Pro->Id!=""){
                    $UpdatePrograma    = array(
                    'nombre_programa'     => $Pro->Nombre,
                    'tipo'                => $Pro->Tipo,
					'cantidad'                => $Pro->Cantidad,
                );
                    $this->programas_model->UpdateProgramas($UpdatePrograma, $Pro->Id);
                    $response["error_msg"]   = "<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button> Informacion Actualizada Correctamente</div>";
                    echo json_encode($response);
               }
          }
     }
	 
	 	public function deleteprograma(){
	
         $url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
         $this->seguridad_model->SessionActivo($url);
		 $Prog	= json_decode($this->input->post('ProgramaPost'));
		$id    = base64_decode($Prog->Id);
	//	$id    = $Socio->Id;
	
	     //Bitácora
		   $RegistrBitacoras = array(
		                'fecha'             => date('Y-m-d H:i:s'),
						'usuario'     		=> $this->session->userdata('ID'),
						'mensaje'	    	=> "Se Borro Programa: $id" );
				   
			$this->bitacoras_model->AgregarBitacoras($RegistrBitacoras);
			//-//
		
		/*Array de response*/
		 $response = array (
				"estatus"   => false,
	            "error_msg" => ""
	    );
		 $this->programas_model->EliminarPrograma($id);
		 $response["error_msg"]   = "<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Programa Eliminado Correctamente Clave: <strong>".$id."</strong>, La Información de Actualizara en 5 Segundos <meta http-equiv='refresh' content='5'></div>";
		 echo json_encode($response);
	}
 
	 
	 
	 
	 
	 
}