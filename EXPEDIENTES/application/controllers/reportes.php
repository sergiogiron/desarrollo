<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
date_default_timezone_set('America/Mexico_City');
class reportes extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('seguridad_model');
		$this->load->model('reportes_model');
	}
	public function index(){
          $url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
          $this->seguridad_model->SessionActivo($url);
          /**/
          $this->load->view('constant');
          $this->load->view('view_header'); 
          $this->load->view('reportes/view_reportes');
          $this->load->view('view_footer');
          
	}
     public function GeneraReporte(){
          $url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
          $this->seguridad_model->SessionActivo($url);
          $localidad= $_POST['localidad'];
		 $local= $this->reportes_model->GeneraReporte($localidad);
		echo json_encode($local);
		

          
     }
	 
	 public function DatosNecesarios(){
          $localidad= $this->input->get("localidad");
		 $local= $this->reportes_model->DatosNecesarios($localidad);
		echo json_encode($local);
	 
	 }
	 
	 
	  public function localidades(){
		$local = $this->reportes_model->Localidades();
		echo json_encode($local);
		
	  }
}