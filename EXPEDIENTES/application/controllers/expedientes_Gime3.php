<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
date_default_timezone_set('America/Argentina/Jujuy');
class expedientes extends CI_Controller {
  function __construct(){
    parent::__construct();
    $this->load->model('seguridad_model');
    $this->load->model('expedientes_model');
    $this->load->model('bitacoras_model');
    $this->load->model('pases_model');
    //$this->load->helper('date');
    // ----------exportar------------//
    $this->load->helper('mysql_to_excel_helper');
    
  }

  
  public function index(){
          //Bitácora
          $RegistrarBitacora = array(
              'fecha'           => date('Y-m-d H:i:s'),
              'usuario'         => $this->session->userdata('ID'),
              'mensaje'         => "Ingresó a Expedientes" );
             
          $this->bitacoras_model->AgregarBitacoras($RegistrarBitacora);

          $url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
          $this->seguridad_model->SessionActivo($url);
          $this->load->view('constant');    //Carga vista títulos de Inicio
          $this->load->view('view_header'); //Carga vista del Menú Horizontal
          $data['exp'] = $this->expedientes_model->listarExp();
          $this->load->view('expedientes/view_expedientes', $data);
          $this->load->view('view_footer'); //Carga vista Pie de Página  
     }

   public function nuevo(){
          //Bitácora
          $RegistrarBitacora = array(
              'fecha'           => date('Y-m-d H:i:s'),
              'usuario'         => $this->session->userdata('ID'),
              'mensaje'         => "Ingresó a Nuevo Expediente" );
             
          $this->bitacoras_model->AgregarBitacoras($RegistrarBitacora);

          $url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
          $this->seguridad_model->SessionActivo($url);
          $this->load->view('constant'); //Carga vista títulos de Inicio
          $this->load->view('view_header'); //Carga vista del Menú Horizontal
          $data['titulo'] = "Nuevo Expediente";
          $data["max"] =  $this->expedientes_model->MaxExp();
          $this->load->view('expedientes/view_nuevo_expedientes', $data);
          $this->load->view('view_footer');
     }
    
     public function editarExpediente($id){

          $url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
          $this->seguridad_model->SessionActivo($url);
          $id =  base64_decode($id); //Encripta el id
           //Bitácora
          $RegistrarBitacora = array(
              'fecha'           => date('Y-m-d H:i:s'),
              'usuario'         => $this->session->userdata('ID'),
              'mensaje'         => "Editó Expediente con id : $id" );
          $this->bitacoras_model->AgregarBitacoras($RegistrarBitacora);

          $this->load->view('constant');      //Carga vista titulos de Inicio 
          $this->load->view('view_header');   //Carga vista del Menú Principal
          $data['exp'] = $this->expedientes_model->buscarExp($id);
          $data['titulo'] = "Editar Expediente";
          $this->load->view('expedientes/view_nuevo_expedientes', $data);
          $this->load->view('view_footer');   //Carga vista Pie de Página
            
     }

     public function saveExpediente(){
   
          $url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
          $this->seguridad_model->SessionActivo($url);
          $Exp = json_decode($this->input->post('ExpPost'));
          $Pase = json_decode($this->input->post('PasePost'));
          $response = array (
                    "estatus"   => false,
                    "campo"     => "",
                    "error_msg" => ""
          );
          /*
        if($Exp->Fecha==""){
               $response["campo"]       = "fecha"; //Campo del formulario
               $response["error_msg"]   = "<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Debe ingresar Fecha</div>";
               echo json_encode($response);
           */    
        if($Exp->Dependencia=="0"){  
               $response["campo"]       = "dependencia"; //Campo del formulario
               $response["error_msg"]   = "<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Debe seleccionar una Dependencia</div>";
               echo json_encode($response); //Retorna la representación JSON del valor dado
               
        }else if($Exp->Remitente==""){  
               $response["campo"]       = "remitente"; //Campo del formulario
               $response["error_msg"]   = "<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Debe ingresar Remitente</div>";
               echo json_encode($response); //Retorna la representación JSON del valor dado
              
        }else if($Exp->Asunto==""){  
                $response["campo"]      ="asunto";
                $response["error_msg"]  ="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Debe ingresar Asunto</div>";
                echo json_encode($response); //Retorna la representación JSON del valor dado
        
        }else if($Exp->Area=="0"){  
               $response["campo"]       = "area"; //Campo del formulario
               $response["error_msg"]   = "<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Debe seleccionar un Área</div>";
               echo json_encode($response);
               
        }else if($Exp->Estado=="0"){   //Si no selecciona una opc, muestra un alerta de campo obligatorio.
                $response["campo"]      ="estado";
                $response["error_msg"]  ="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Debe seleccionar un Estado</div>";
                echo json_encode($response); //Retorna la representación JSON del valor dado
           
          //CAMPO OBSERVACIONES NO CONSIDERADO COMO OBLIGATORIO...

        }else if($Exp->Id==""){ //Si el id no existe, se REGISTRA un nuevo Expediente
               
          $RegistraExp  = array(
             'codigo_exp'        => $Exp->Codigo,
             //'anio'              => date('Y'),
             'fecha_exp'         => date('d/m/Y'),
             'id_dep'            => $Exp->Dependencia,
             'remitente_exp'     => $Exp->Remitente,
             'asunto_exp'        => $Exp->Asunto,
             'id_area'           => $Exp->Area,
             'estado_exp'        => $Exp->Estado,
             'obs_exp'           => $Exp->Observaciones,
              );

          //Bitacora
          $CADENA=print_r($RegistraExp,  true);
          $RegistrarBitacora  = array(
                'fecha'       => date('Y-m-d H:i:s'),
                'usuario'     => $this->session->userdata('ID'),
                'mensaje'     => "Alta de Expediente: $CADENA");  
          $this->bitacoras_model->AgregarBitacoras($RegistrarBitacora);
          $this->expedientes_model->SaveExp($RegistraExp);

          //Al generar un nuevo exp, paralelamente se crea un nuevo pase
          $RegistraPase  = array(
             'codigo_exp'        => $Exp->Codigo,
             //'anio'              => date('Y'),
             //'remitente_exp'     => $Pase->Remitente,
             'fecha_pase'        => date('d/m/Y'),
             'id_area'           => $Exp->Area,
             'estado_pase'       => $Exp->Estado,
             'obs_pase'          => $Exp->Observaciones,
              );
          
          //Bitacora
          $CADENA=print_r($RegistraPase,true);
          $RegistrarBitacora  = array(
                'fecha'       => date('Y-m-d H:i:s'),
                'usuario'     => $this->session->userdata('ID'),
                'mensaje'     => "Alta de Pase: $CADENA");  
          $this->bitacoras_model->AgregarBitacoras($RegistrarBitacora);
          $this->pases_model->SavePase($RegistraPase);

          $response["error_msg"]   = "<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Informacion Guardada Correctamente</div>";
          echo json_encode($response); //Retorna la representación JSON del valor dado
              }
              else if($Exp->Id!=""){ //Si el id ya existe, se realiza la ACTUALIZACION de un expediente
                    $UpdateExp    = array(
                       'codigo_exp'        => $Exp->Codigo,
                       //'anio'              => date('Y'),
                       'fecha_exp'         => date('d/m/Y'),
                       'id_dep'            => $Exp->Dependencia,
                       'remitente_exp'     => $Exp->Remitente,
                       'asunto_exp'        => $Exp->Asunto,
                       'id_area'           => $Exp->Area,
                       'estado_exp'        => $Exp->Estado,
                       'obs_exp'           => $Exp->Observaciones,
                      );
                    //Bitácora
                    $CADENA=print_r($UpdateExp,true);
                    $RegistrarBitacora     = array(
                        'fecha'            => date('Y-m-d H:i:s'),
                        'usuario'          => $this->session->userdata('ID'),
                        'mensaje'          => "Editó Expediente: $CADENA"); 
                    $this->bitacoras_model->AgregarBitacoras($RegistrarBitacora);

                    $this->expedientes_model->updateExp($UpdateExp, $Exp->Id);
                    $response["error_msg"]   = "<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button> Informacion Actualizada Correctamente</div>";
                    echo json_encode($response); //Retorna la representación JSON del valor dado
               }
          
     
   }


   //Obtiene registros almacenados en Dependencias...
   public function dependencias(){
    $local = $this->expedientes_model->traerDependencias();
    echo json_encode($local);
  }

  //Obtiene registros almacenados en Areas...
  public function areas(){
    $local = $this->expedientes_model->traerAreas();
    echo json_encode($local);
  }


  public function listarPases($id){ 
      $url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
      $data["titulo"]    ="Pases";
      $this->load->view('constant');
      $this->load->view('view_header',$data);
    
      $id =  base64_decode($id);
      //print_r($id);
      $this->seguridad_model->SessionActivo($url);
      $data["pase"] = $this->expedientes_model->listaPases($id);
      $this->load->view('pases/view_pases', $data);
      $this->load->view('view_footer');
    
     } 
      public function deleteExpediente(){
        
         $url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
         $this->seguridad_model->SessionActivo($url);
         $Exp = json_decode($this->input->post('ExpPost'));
         $id        = base64_decode($Exp->Id);
         
         $response = array ( /*Array de response*/
            "estatus"   => false,
            "error_msg" => ""
         );
         //Bitácora
         $RegistrarBitacora = array(
            'fecha'           => date('Y-m-d H:i:s'),
            'usuario'         => $this->session->userdata('ID'),
            'mensaje'         => "Se Borró Expediente con id: $id" );
         $this->bitacoras_model->AgregarBitacoras($RegistrarBitacora);

         $this->expedientes_model->eliminarExp($id); 
         $response["error_msg"]   = "<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Expediente eliminado correctamente Clave: <strong>".$id."</strong>, La Información de Actualizara en 5 Segundos <meta http-equiv='refresh' content='5'></div>";
         echo json_encode($response); //Retorna la representación JSON del valor dado     
        
       }

      //-------exportar----------//
        public function exportar(){
         //Bitácora
        $RegistrarBitacora = array(
              'fecha'           => date('Y-m-d H:i:s'),
              'usuario'         => $this->session->userdata('ID'),
              'mensaje'         => "Exportó Datos de Expediente en formato Excel" );  

        $this->bitacoras_model->AgregarBitacoras($RegistrarBitacora);
            //-//
        $this->load->helper('mysql_to_excel_helper');
        $this->load->model('expedientes_model');
        to_excel($this->expedientes_model->get(), "ExpedientesExcel");
        }
 
     //========================imagien----------------------------
      public function view_img($id){
      $id          = base64_decode($id); 
        //Bitácora
      $RegistrarBitacora   = array(
          'fecha'          => date('Y-m-d H:i:s'),
          'usuario'        => $this->session->userdata('ID'),
          'mensaje'        => "Ingresó a Cargar Imagen de Expediente Codigo : $id");  
      $this->bitacoras_model->AgregarBitacoras($RegistrarBitacora);
                       //-//
      $data["id"]        = $id;
      $this->load->view('constant');
      $this->load->view('view_header');
      $this->load->view('expedientes/view_img',$data);
      $this->load->view('view_footer');
      
      }

      public function SubeImg(){
        $img1= "";
        $str = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
            for($i=0;$i<5;$i++) {
                $img1 .= substr($str,rand(0,62),1);
            }
        $idimg     = $this->input->post('id');
        $nombreimg = $idimg."_".$img1;
        $config['upload_path'] = realpath(APPPATH."../fotos");
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size'] = '1048576';
        $config['max_width'] = '900';
        $config['max_height'] = '900';
        $config['file_name']= $idimg."_".$img1;
        $this->load->library('upload', $config);
        if (!$this->upload->do_upload('file')) {
          echo  $this->upload->display_errors();
        }else{
          $file_info = $this->upload->data();
          $data      = array('upload_data'=>$this->upload->data());
          echo "Imagen Subida Correctamente.";
          $img = array("foto_exp"=>$nombreimg.$file_info["file_ext"]);
        //  $this->socios_model->GuardaImg($img);
           $this->expedientes_model->updateExp($img, $idimg);
        }
        }

        //Muestra Expediente
        public  function view_mostrar($id) {
        
        $id = base64_decode($id);
        //Bitácora
        $RegistrarBitacora = array(
            'fecha'        => date('Y-m-d H:i:s'),
            'usuario'      => $this->session->userdata('ID'),
            'mensaje'     => "Muestra   Expediente  id : $id"); 
        $this->bitacoras_model->AgregarBitacoras($RegistrarBitacora);
                         //-//
        $data = array(
                'title' => 'Expediente',
                'imgexp' => $this->expedientes_model->buscarExp($id),
                );
                  
          //  $this->load->view('socios/ventanacodigodebarras_view', $datos);
            $this->load->view('expedientes/view_mostrar', $data);
          }
  
 
}