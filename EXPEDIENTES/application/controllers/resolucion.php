<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
date_default_timezone_set('America/Argentina/Jujuy');
class resolucion extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('seguridad_model');
		$this->load->model('resolucion_model');
		$this->load->model('bitacoras_model');
    //$this->load->model('cooperativas_model');
			
	// ----------exportar------------//
		
		$this->load->helper('mysql_to_excel_helper');
		
	}

  
	public function index(){
          //Bitácora
          $RegistrarBitacora = array(
              'fecha'           => date('Y-m-d H:i:s'),
              'usuario'         => $this->session->userdata('ID'),
              'mensaje'         => "Ingresó a Resolucion" );
             
          $this->bitacoras_model->AgregarBitacoras($RegistrarBitacora);

          $url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
          $this->seguridad_model->SessionActivo($url);
          $this->load->view('constant');    //Carga vista títulos de Inicio
          $this->load->view('view_header'); //Carga vista del Menú Horizontal
          $data['resolucion'] = $this->resolucion_model->listarResolucion();
          $this->load->view('resolucion/view_resolucion', $data);
          $this->load->view('view_footer'); //Carga vista Pie de Página  
	   }

   public function nuevo(){
          //Bitácora
          $RegistrarBitacora = array(
              'fecha'           => date('Y-m-d H:i:s'),
              'usuario'         => $this->session->userdata('ID'),
              'mensaje'         => "Ingresó a nueva Resolucion" );
             
          $this->bitacoras_model->AgregarBitacoras($RegistrarBitacora);

          $url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
          $this->seguridad_model->SessionActivo($url);
          $this->load->view('constant'); //Carga vista títulos de Inicio
          $this->load->view('view_header'); //Carga vista del Menú Horizontal
          $data['titulo'] = "Nueva Resolucion";
          $this->load->view('resolucion/view_nuevo_resolucion', $data);
          $this->load->view('view_footer'); //Carga vista Pie de Pagina
     }
     /**
      * Edita una Denuncia
      * @param int $id  identificador de la denuncia a editar 
      * @return void
      */
     public function editarResolucion($id){

          $url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
          $this->seguridad_model->SessionActivo($url);
          $id =  base64_decode($id); //Encripta el id
           //Bitácora
          $RegistrarBitacora = array(
              'fecha'           => date('Y-m-d H:i:s'),
              'usuario'         => $this->session->userdata('ID'),
              'mensaje'         => "Editó Resolucion con id : $id" );
          $this->bitacoras_model->AgregarBitacoras($RegistrarBitacora);

          $this->load->view('constant');      //Carga vista titulos de Inicio 
          $this->load->view('view_header');   //Carga vista del Menú Principal
          $data['resolucion'] = $this->resolucion_model->buscarResolucion($id);
          $data['titulo'] = "Editar Resolucion";
          $this->load->view('resolucion/view_nuevo_resolucion', $data);
          $this->load->view('view_footer');   //Carga vista Pie de Página
            
     }

     public function saveResolucion(){
	 
	        $url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
          $this->seguridad_model->SessionActivo($url);
          $Resolucion  = json_decode($this->input->post('ResolucionPost')); //Obtiene los nuevos datos de una resolucion con JSON
          $response = array (
                    "estatus"   => false,
                    "campo"     => "",
                    "error_msg" => ""
          );
		   if($Resolucion->Codigo==""){  //Si no ingresa un codigo, muestra un alerta de campo obligatorio.
               $response["campo"]       = "codigo"; //Campo del formulario
               $response["error_msg"]   = "<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Debe ingresar Codigo</div>";
               echo json_encode($response); //Retorna la representación JSON del valor dado
        }else if($Resolucion->Año==""){  //Si no ingresa un reclamo, muestra un alerta de campo obligatorio.
               $response["campo"]       = "año"; //Campo del formulario
               $response["error_msg"]   = "<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Debe ingresar el Año</div>";
               echo json_encode($response); //Retorna la representación JSON del valor dado
		    }else if($Resolucion->Asunto==""){  //Si no ingresa Asunto, muestra un alerta de campo obligatorio.
                $response["campo"]      ="asunto";
                $response["error_msg"]  ="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Debe ingresar Asunto</div>";
                echo json_encode($response); //Retorna la representación JSON del valor dado
        }else if($Resolucion->Gobierno=="0"){   //Si no selecciona una opc, muestra un alerta de campo obligatorio.
                $response["campo"]      ="gobierno";
                $response["error_msg"]  ="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Debe seleccionar una opcion</div>";
                echo json_encode($response); //Retorna la representación JSON del valor dado
           
          //CAMPO OBSERVACIONES NO CONSIDERADO COMO OBLIGATORIO...

        }else if($Resolucion->Id==""){ //Si el id no existe, se REGISTRA una nueva resolucion
               
					$RegistraResolucion   = array(
					   'codigo'        => $Resolucion->Codigo,
             'año'           => $Resolucion->Año,
             'asunto'        => $Resolucion->Asunto,
             'gobierno'      => $Resolucion->Gobierno,
             'obs'     => $Resolucion->Observaciones,
              );
          //Bitácora
          $CADENA=print_r($RegistraResolucion,true);
          $RegistrarBitacora  = array(
                'fecha'       => date('Y-m-d H:i:s'),
                'usuario'     => $this->session->userdata('ID'),
                'mensaje'     => "Alta de Resolucion: $CADENA");  
          $this->bitacoras_model->AgregarBitacoras($RegistrarBitacora);
                    
          $this->resolucion_model->SaveResolucion($RegistraResolucion);
          $response["error_msg"]   = "<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Informacion Guardada Correctamente</div>";
          echo json_encode($response); //Retorna la representación JSON del valor dado
              }
              if($Resolucion->Id!=""){ //Si el id ya existe, se realiza la ACTUALIZACION de una denuncia
                    $UpdateResolucion    = array(
					          'codigo'        => $Resolucion->Codigo,
                    'año'           => $Resolucion->Año,
                    'asunto'        => $Resolucion->Asunto,
                    'gobierno'      => $Resolucion->Gobierno,
                    'obs'     => $Resolucion->Observaciones,
                      );
                    //Bitácora
                    $CADENA=print_r($UpdateResolucion,true);
                    $RegistrarBitacora     = array(
                        'fecha'            => date('Y-m-d H:i:s'),
                        'usuario'          => $this->session->userdata('ID'),
                        'mensaje'          => "Editó Resolucion: $CADENA"); 
                    $this->bitacoras_model->AgregarBitacoras($RegistrarBitacora);

                    $this->resolucion_model->updateResolucion($UpdateResolucion, $Resolucion->Id);
                    $response["error_msg"]   = "<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button> Informacion Actualizada Correctamente</div>";
                    echo json_encode($response); //Retorna la representación JSON del valor dado
               }
          
     
   }
   
	   	public function deleteResolucion(){
	      
         $url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
         $this->seguridad_model->SessionActivo($url);
		     $Resolucion	= json_decode($this->input->post('ResolucionPost'));
		     $id        = base64_decode($Resolucion->Id);
         
         $response = array ( /*Array de response*/
            "estatus"   => false,
            "error_msg" => ""
         );
         //Bitácora
         $RegistrarBitacora = array(
            'fecha'           => date('Y-m-d H:i:s'),
            'usuario'         => $this->session->userdata('ID'),
            'mensaje'         => "Se Borró Resolucion con id: $id" );
	       $this->bitacoras_model->AgregarBitacoras($RegistrarBitacora);

         $this->resolucion_model->eliminarResolucion($id); 
         $response["error_msg"]   = "<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Resolucion Eliminada Correctamente Clave: <strong>".$id."</strong>, La Información de Actualizara en 5 Segundos <meta http-equiv='refresh' content='5'></div>";
         echo json_encode($response); //Retorna la representación JSON del valor dado     
		    
	     }

      //-------exportar----------//
      	public function exportar(){
      	 //Bitácora
      	$RegistrarBitacora = array(
      		    'fecha'             => date('Y-m-d H:i:s'),
      				'usuario'     		=> $this->session->userdata('ID'),
      				'mensaje'	    	=> "Exportó Datos de Resolucion en formato Excel" );	

        $this->bitacoras_model->AgregarBitacoras($RegistrarBitacora);
      			//-//
      	$this->load->helper('mysql_to_excel_helper');
        $this->load->model('resolucion_model');
      	to_excel($this->resolucion_model->get(), "ResolucionExcel");
      	}
 
     //========================imagien----------------------------
    	public function view_img($id){
    	$id 			   = base64_decode($id); 
    		//Bitácora
    	$RegistrarBitacora   = array(
    		  'fecha'          => date('Y-m-d H:i:s'),
    		  'usuario'     	 => $this->session->userdata('ID'),
    		  'mensaje'	    	 => "Ingresó a Cargar Imagen de Resolucion Codigo : $id");	
      $this->bitacoras_model->AgregarBitacoras($RegistrarBitacora);
    			             //-//
    	$data["id"]        = $id;
    	$this->load->view('constant');
    	$this->load->view('view_header');
    	$this->load->view('resolucion/view_img',$data);
    	$this->load->view('view_footer');
    	
      }

    	public function SubeImg(){
    		$img1= "";
    		$str = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
            for($i=0;$i<5;$i++) {
                $img1 .= substr($str,rand(0,62),1);
            }
    		$idimg     = $this->input->post('id');
    		$nombreimg = $idimg."_".$img1;
    		$config['upload_path'] = realpath(APPPATH."../fotos");
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size'] = '1048576';
        $config['max_width'] = '900';
        $config['max_height'] = '900';
    		$config['file_name']= $idimg."_".$img1;
    		$this->load->library('upload', $config);
    		if (!$this->upload->do_upload('file')) {
    			echo  $this->upload->display_errors();
    		}else{
    			$file_info = $this->upload->data();
    			$data      = array('upload_data'=>$this->upload->data());
    			echo "Imagen Subida Correctamente.";
    			$img = array("foto_resolucion"=>$nombreimg.$file_info["file_ext"]);
    		//	$this->socios_model->GuardaImg($img);
    		   $this->resolucion_model->updateResolucion($img, $idimg);
    		}
    		}

      	//Muestra Denuncia
      	public	function view_mostrar($id) {
      	
        $id = base64_decode($id);
      	//Bitácora
      	$RegistrarBitacora = array(
      		  'fecha'        => date('Y-m-d H:i:s'),
      		  'usuario'      => $this->session->userdata('ID'),
      		  'mensaje'	    => "Muestra Resolucion  id : $id");	
        $this->bitacoras_model->AgregarBitacoras($RegistrarBitacora);
      			             //-//
      	$data = array(
                'title' => 'Resolucion',
                'imgdenuncia' => $this->resolucion_model->buscarResolucion($id),
      		      );
      						
      		//	$this->load->view('socios/ventanacodigodebarras_view', $datos);
      			$this->load->view('resolucion/view_mostrar', $data);
      		}
	
 
}