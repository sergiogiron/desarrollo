<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
date_default_timezone_set('America/Mexico_City');
class mesa extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('seguridad_model');
		$this->load->model('mesa_model');
		$this->load->model('bitacoras_model');
	}
	public function index(){
          $url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
         $this->seguridad_model->SessionActivo($url);
          
          $this->load->view('constant');
          $this->load->view('view_header');
          $data['cupos'] = $this->mesa_model->ListarSolicitudes();
          $this->load->view('mesa/view_mesa', $data);
          $this->load->view('view_footer');
          
	}
     public function nuevo(){
          $url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
          $this->seguridad_model->SessionActivo($url);
          $this->load->view('constant');
          $this->load->view('view_header');
          $data['titulo'] = "Nueva Consulta";
          $this->load->view('mesa/view_nuevo', $data);
          $this->load->view('view_footer');
     }
     public function editar($id){
          $url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
          $this->seguridad_model->SessionActivo($url);
          $id =  base64_decode($id);
          $this->load->view('constant');
          $this->load->view('view_header');
          $data['cupo'] = $this->mesa_model->Busca($id);
          $data['titulo'] = "Detalle ";
          $this->load->view('mesa/view_nuevo', $data);
          $this->load->view('view_footer');
     }
     public function SMesa(){
	 
	      $url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
          $this->seguridad_model->SessionActivo($url);
          $Cu         = json_decode($this->input->post('mesaPost'));
		  
          $response = array (
                    "estatus"   => false,
                    "campo"     => "",
                 "error_msg" => ""
         );
		 //Bitácora
						 $CADENA=print_r($Cu,true);
		                 $RegistrBitacoras = array(
		                'fecha'             => date('Y-m-d H:i:s'),
						'usuario'     		=> $this->session->userdata('ID'),
						'mensaje'	    	=> "Nueva Mesa de Ayuda: $CADENA");	

                         $this->bitacoras_model->AgregarBitacoras($RegistrBitacoras);
			             //-//
		    		 
		 
		 
		 
		 
         if($Cu->Desc==""){
               $response["campo"]     = "desc";
               $response["error_msg"]   = "<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>escriba una descrpcion</div>";
               echo json_encode($response);
       
        }else if($Cu->Motivo==""){
               $response["campo"]     = "Localidad";
               $response["error_msg"]   = "<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>necesitamos que describa su problema</div>";
               echo json_encode($response);
       
        }else  if($Cu->Id==""){
                     $RegistraConsulta    = array(
                     'motivo'     => $Cu->Motivo,
                     'descripcion'    => $Cu->Desc,
                     'solicitante'             => $Cu->Solicitante,
				     'estado' => $Cu->Estado,
					 'fecha'=> date('Y-m-d H:i:s'),
                      );
				
				//Bitácora
						 $CADENA=print_r($RegistraConsulta,true);
		                 $RegistrBitacoras = array(
		                'fecha'             => date('Y-m-d H:i:s'),
						'usuario'     		=> $this->session->userdata('ID'),
						'mensaje'	    	=> "guarda consulta: $CADENA");	

                         $this->bitacoras_model->AgregarBitacoras($RegistrBitacoras);
			             //-//
				
				
				
				
				
				
				
				
                     $this->mesa_model->Save($RegistraConsulta);
                     $response["error_msg"]   = "<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Informacion Guardada Correctamente</div>";
                     echo json_encode($response);
               }
                if($Cu->Id!=""){
                    $Update    = array(
                     'descripcion'     => $Cu->Desc,
                     'solicitante'    => $Cu->Solicitante,
                     'estado'             => $Cu->Estado,
					 'motivo'             => $Cu->Motivo,
		              'respuesta'         =>$Cu->Respuesta,
                     );
                    $this->mesa_model->Update($Update, $Cu->Id);
                    $response["error_msg"]   = "<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button> Informacion Actualizada Correctamente</div>";
                    echo json_encode($response);
               }
           
     }
	 
	 	public function deletecupo(){
	
         $url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
         $this->seguridad_model->SessionActivo($url);
		 $Cupo	= json_decode($this->input->post('ProgramaPost'));
		$id    = base64_decode($Cupo->Id);
	
	
	     //Bitácora
		   $RegistrBitacoras = array(
		                'fecha'             => date('Y-m-d H:i:s'),
						'usuario'     		=> $this->session->userdata('ID'),
						'mensaje'	    	=> "Se Borro Cupo: $id" );
				   
			$this->bitacoras_model->AgregarBitacoras($RegistrBitacoras);
			//-//
		
		/*Array de response*/
		 $response = array (
				"estatus"   => false,
	            "error_msg" => ""
	    );
		 $this->cupos_model->EliminarCupo($id);
		 $response["error_msg"]   = "<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Programa Eliminado Correctamente Clave: <strong>".$id."</strong>, La Información de Actualizara en 5 Segundos <meta http-equiv='refresh' content='5'></div>";
		 echo json_encode($response);
	}
 
	 public function localidades(){
		$local = $this->cupos_model->Localidades();
		echo json_encode($local);
	}
	 
	public function programas(){
		$local = $this->cupos_model->Programas();
		echo json_encode($local);
	} 
	 
		 public	function view_mostrar($id) {
	
    $id = base64_decode($id);
	//Bitácora
		 
		  $RegistrBitacoras = array(
		  'fecha'             => date('Y-m-d H:i:s'),
		  'usuario'     		=> $this->session->userdata('ID'),
		  'mensaje'	    	=> "Muestra ARCHIVO MESA  id : $id");	
           $this->bitacoras_model->AgregarBitacoras($RegistrBitacoras);
			             //-//
	   	 $data = array(
            'title' => 'Mesa',
           'imgdenuncia' => $this->mesa_model->Buscar($id),
		  
		      );
						
		//	$this->load->view('socios/ventanacodigodebarras_view', $datos);
			$this->load->view('mesa/view_mostrar', $data);
		}
	
	
	public function view_img($id){
		$id 			   = base64_decode($id); 
		
		//Bitácora
		 
		  $RegistrBitacoras = array(
		  'fecha'             => date('Y-m-d H:i:s'),
		  'usuario'     		=> $this->session->userdata('ID'),
		  'mensaje'	    	=> "Ingreso a Sube Imagen de Socio  Codigo : $id");	
           $this->bitacoras_model->AgregarBitacoras($RegistrBitacoras);
			             //-//
		
		
		
		
		$data["id"]        = $id;
		$this->load->view('constant');
		$this->load->view('view_header');
		$this->load->view('mesa/view_img',$data);
		$this->load->view('view_footer');
	}
	 
	 public function SubeImg(){
		$img1= "mesa";
		$str = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
        for($i=0;$i<5;$i++) {
            $img1 .= substr($str,rand(0,62),1);
        }
		$idimg     = $this->input->post('id');
		$nombreimg = $idimg."_".$img1;
		$config['upload_path'] = realpath(APPPATH."../fotos");
        $config['allowed_types'] = 'gif|jpg|png|doc|docx|pdf';
        $config['max_size'] = '5048576';
        $config['max_width'] = '2000';
        $config['max_height'] = '2000';
		$config['file_name']= $idimg."_".$img1;
		$this->load->library('upload', $config);
		if (!$this->upload->do_upload('file')) {
			echo  $this->upload->display_errors();
		}else{
			$file_info = $this->upload->data();
			$data      = array('upload_data'=>$this->upload->data());
			echo "Imagen Subido Correctamente.";
			$img = array("imagen"=>$nombreimg.$file_info["file_ext"]);
		//	$this->socios_model->GuardaImg($img);
		   $this->mesa_model->updateMesa($img, $idimg);
		}
		}
	 
}