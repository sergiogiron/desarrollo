<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
date_default_timezone_set('America/Argentina/Jujuy');
class socios extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('seguridad_model');
		$this->load->model('socios_model');
		$this->load->model('bitacoras_model');
	    $this->load->model('cooperativas_model');
	}
	public function index(){
	      //Bitácora
		   $RegistrBitacoras = array(
		                'fecha'             => date('Y-m-d H:i:s'),
						'usuario'     		=> $this->session->userdata('ID'),
						'mensaje'	    	=> "Ingresó a Socios" );
				   
			$this->bitacoras_model->AgregarBitacoras($RegistrBitacoras);
			//-//
	
          $url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
          $this->seguridad_model->SessionActivo($url);
          /**/
          $this->load->view('constant');
          $this->load->view('view_header');
          $data['socios'] = $this->socios_model->ListarSocios();
          $this->load->view('socios/view_socios', $data);
          $this->load->view('view_footer');
          
	}
	public function deletesocio(){
	
         $url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
         $this->seguridad_model->SessionActivo($url);
		 $Socio	= json_decode($this->input->post('SociosPost'));
		$id    = base64_decode($Socio->Id);
	//	$id    = $Socio->Id;
	
	     //Bitácora
		   $RegistrBitacoras = array(
		                'fecha'             => date('Y-m-d H:i:s'),
						'usuario'     		=> $this->session->userdata('ID'),
						'mensaje'	    	=> "Se Borro Socio id : $id" );
				   
			$this->bitacoras_model->AgregarBitacoras($RegistrBitacoras);
			//-//
		
		/*Array de response*/
		 $response = array (
				"estatus"   => false,
	            "error_msg" => ""
	    );
		 $this->socios_model->EliminarSocio($id);
		 $response["error_msg"]   = "<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Socio Eliminado Correctamente Clave: <strong>".$id."</strong>, La Información de Actualizara en 5 Segundos <meta http-equiv='refresh' content='5'></div>";
		 echo json_encode($response);
	}	
     public function nuevo(){
          $url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
          $this->seguridad_model->SessionActivo($url);
          $this->load->view('constant');
          $this->load->view('view_header');
          $data['titulo'] = "Nuevo Socio";
          $this->load->view('socios/view_nuevo_socios', $data);
          $this->load->view('view_footer');
     }
     public function editarSocio($id){
          $url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
          $this->seguridad_model->SessionActivo($url);
          $id =  base64_decode($id);
		  
		    //Bitácora
		   $RegistrBitacoras = array(
		                'fecha'             => date('Y-m-d H:i:s'),
						'usuario'     		=> $this->session->userdata('ID'),
						'mensaje'	    	=> "Ingresó edita $id" );
				   
			$this->bitacoras_model->AgregarBitacoras($RegistrBitacoras);
			//-//
		   
		   
		   
		   
          $this->load->view('constant');
          $this->load->view('view_header');
          $data['socio'] = $this->socios_model->BuscaSocios($id);
          $data['titulo'] = "Editar Socio";
          $this->load->view('socios/view_nuevo_socios', $data);
          $this->load->view('view_footer');
		  
		  
     }
	 public function coope(){
		$local = $this->cooperativas_model->Coope();
		echo json_encode($local);
	}
	 
	 
	 
     public function SaveSocio(){
        $url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
          $this->seguridad_model->SessionActivo($url);
          $Socios         = json_decode($this->input->post('SociosPost'));
          $response = array (
                    "estatus"   => false,
                    "campo"     => "",
                 "error_msg" => ""
         );
         if($Socios->Nombre==""){
               $response["campo"]     = "nombre";
               $response["error_msg"]   = "<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>El Nombre del Socio es Obligatorio</div>";
               echo json_encode($response);
          }else if($Socios->Domicilio==""){
               $response["campo"]     = "domicilio";
               $response["error_msg"]   = "<div class='alert alert-danger text-center' alert-dismissable><button type='button' class='close' data-dismiss='alert'>&times;</button>La Domicilio es obligatorio</div>";
               echo json_encode($response);
		  }else if($Socios->Cooperativa=="0"){
               $response["campo"]     = "cooperativa";
               $response["error_msg"]   = "<div class='alert alert-danger text-center' alert-dismissable><button type='button' class='close' data-dismiss='alert'>&times;</button>La Cooperativa es obligatorio</div>";
               echo json_encode($response);			   
          }else if($Socios->Cargo==""){
               $response["campo"]       = "cargo";
               $response["error_msg"]   = "<div class='alert alert-danger text-center' alert-dismissable><button type='button' class='close' data-dismiss='alert'>&times;</button>El Cargo es Obligatorio</div>";
               echo json_encode($response);
          }else{
               if($Socios->Id==""){
                     $RegistraSocio    = array(
                     'nombre'              => $Socios->Nombre,
					 'documento'           => $Socios->Documento,
					  'cuit'                => $Socios->Cuit,
					 'fechanac'            => $Socios->Fechanac,
					 'domicilio'           => $Socios->Domicilio,
					 'nro'                 => $Socios->Nro,
					 'manzana'             => $Socios->Manzana,
					 'lote'                => $Socios->Lote,
					 'barrio'              => $Socios->Barrio,				 
					 'id_localidad'        => $Socios->Localidad,
					 'id_provincia'        => $Socios->Id_provincia,
                     'telefono'            => $Socios->Telefono,
					 'cargo'               => $Socios->Cargo,
					 'id_cooperativa'     => $Socios->Cooperativa,
					 'tipo'               => $Socios->Tipo,
					 'plan'               => $Socios->Plan,
                   		 
					 
                     'fecha'      => date('Y-m-j H:i:s')
                     );
					  //Bitácora
						 $CADENA=print_r($RegistraSocio,true);
		                 $RegistrBitacoras = array(
		                'fecha'             => date('Y-m-d H:i:s'),
						'usuario'     		=> $this->session->userdata('ID'),
						'mensaje'	    	=> "Alta de Socio: $CADENA");	

                         $this->bitacoras_model->AgregarBitacoras($RegistrBitacoras);
			             //-//
                     $this->socios_model->SaveSocios($RegistraSocio);
                     $response["error_msg"]   = "<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Informacion Guardada Correctamente</div>";
                     echo json_encode($response);
               }
               if($Socios->Id!=""){
                    $UpdateSocio    = array(
					 'nombre'              => $Socios->Nombre,
					  'documento'           => $Socios->Documento,
					 'cuit'                => $Socios->Cuit,
					 'fechanac'            => $Socios->Fechanac,
					 'domicilio'           => $Socios->Domicilio,
					 'nro'                 => $Socios->Nro,
					 'manzana'             => $Socios->Manzana,
					 'lote'                => $Socios->Lote,
					 'barrio'              => $Socios->Barrio,				 
					 'id_localidad'        => $Socios->Localidad,
					 'id_provincia'        => $Socios->Id_provincia,
                     'telefono'            => $Socios->Telefono,
					 'cargo'               => $Socios->Cargo,
					 'id_cooperativa'     => $Socios->Cooperativa,
					 'tipo'               => $Socios->Tipo,
					 'plan'               => $Socios->Plan,
                   		 
                   		 
					 
                     'fecha'      => date('Y-m-j H:i:s')
					
					
                     );
                         //Bitácora
						 $CADENA=print_r($UpdateSocio,true);
		                 $RegistrBitacoras = array(
		                'fecha'             => date('Y-m-d H:i:s'),
						'usuario'     		=> $this->session->userdata('ID'),
						'mensaje'	    	=> "Modificación de Socio: $CADENA");	

                         $this->bitacoras_model->AgregarBitacoras($RegistrBitacoras);
			             //-//
					 
                    $this->socios_model->UpdateSocios($UpdateSocio, $Socios->Id);
                    $response["error_msg"]   = "<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button> Informacion Actualizada Correctamente</div>";
                    echo json_encode($response);
               }
          }
     }
}