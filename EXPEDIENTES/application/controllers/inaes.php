<?php ob_start();
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
date_default_timezone_set('America/Argentina/Jujuy');
class inaes extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('seguridad_model');
		$this->load->model('cooperativas_model');
		$this->load->model('requisitos_model');
		$this->load->model('bitacoras_model');
		
		$this->load->helper('date');
		
			
		
	// ----------exportar------------//
		
		$this->load->helper('mysql_to_excel_helper');
		
		
		//
	}
	public function index(){
	
	       //Bitácora
		   $RegistrBitacoras = array(
		                'fecha'             => date('Y-m-d H:i:s'),
						'usuario'     		=> $this->session->userdata('ID'),
						'mensaje'	    	=> "Ingresó a INAES" );
				   
			$this->bitacoras_model->AgregarBitacoras($RegistrBitacoras);
			//-//	
          $url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
          $this->seguridad_model->SessionActivo($url);
          /**/
          $this->load->view('constant');
          $this->load->view('view_header');
          $data['cooperativas'] = $this->cooperativas_model->ListarCooperativas();
          $this->load->view('inaes/view_inaes', $data);
          $this->load->view('view_footer');
          
	}
	public function deletecoope(){
	            
		$url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
          $this->seguridad_model->SessionActivo($url);
		$Coope 		= json_decode($this->input->post('MiCoops'));
		$id             = base64_decode($Coope->Id);
		$codigo 		= base64_decode($Coope->Matricula);
		/*Array de response*/
		 $response = array (
				"estatus"   => false,
	            "error_msg" => ""
	    );
		 $this->cooperativas_model->EliminarCoope($id);
		 //Bitácora
		 
		                 $RegistrBitacoras = array(
		                'fecha'             => date('Y-m-d H:i:s'),
						'usuario'     		=> $this->session->userdata('ID'),
						'mensaje'	    	=> "Borrado Cooperativa Id : $id Codigo : $codigo");	

                         $this->bitacoras_model->AgregarBitacoras($RegistrBitacoras);
			             //-//
		  $response["error_msg"]   = "<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Producto Eliminado Correctamente Clave: <strong>".$codigo."</strong>, La Información de Actualizara en 5 Segundos <meta http-equiv='refresh' content='5'></div>";
		 echo json_encode($response);
	}
	public function editarCooperativa($id = NULL){
		$url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
          $this->seguridad_model->SessionActivo($url);
		$id 			   = base64_decode($id); 
	//	$this->socios->subir();
		$data["Coope"] = $this->cooperativas_model->BuscarCooperativa($id);
		$data["titulo"]    = "Editar Cooperativa";
		$this->load->view('constant');
		$this->load->view('view_header',$data);
		$this->load->view('cooperativas/view_nuevo_cooperativas',$data);
		$this->load->view('view_footer');
	}
	public function nuevo(){
		$url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
          $this->seguridad_model->SessionActivo($url);
	//	  $this->socios->subir();
		$this->load->view('constant');
		$this->load->view('view_header');
		$data["titulo"]    = "Nueva Cooperativa";
		$data["max"] =  $this->cooperativas_model->MaxCooperativa();
		$this->load->view('cooperativas/view_nuevo_cooperativas',$data);
		$this->load->view('view_footer');
	}
	
	public function localidades(){
		$local = $this->cooperativas_model->Localidades();
		echo json_encode($local);
	}
	
	public function coope(){
		$local = $this->cooperativas_model->Coope();
		echo json_encode($local);
	}
		
	
	public function GuardaCooperativas(){
		$url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
        $this->seguridad_model->SessionActivo($url);
	    $Coope		= json_decode($this->input->post('MiCoops'));
			
		/*Array de response*/
		 $response = array (
				"estatus"   => false,
				"campo"     => "",
	            "error_msg" => ""
	    );//
		
		 if($Coope->Razonsocial==""){
				$response["campo"]       = "razonsocial";
				$response["error_msg"]   = "<div class='alert alert-danger text-center' alert-dismissable><button type='button' class='close' data-dismiss='alert'>&times;</button>Razon Social es Obligatorio</div>";
				echo json_encode($response);
		}else if($Coope->Matricula==""){
			   $response["campo"]     = "matricula";
			   $response["error_msg"]   = "<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>La Matricula es obligatoria</div>";
			   echo json_encode($response);
		}else if($Coope->Cuit==""){
			   $response["campo"]     = "cuit";
		       $response["error_msg"]   = "<div class='alert alert-danger text-center' alert-dismissable><button type='button' class='close' data-dismiss='alert'>&times;</button>Cuit es obligatorio</div>";
		       echo json_encode($response);
	//	}else if($Coope->Fecha!=""){
//		       $response["campo"]       = "fecha";
//			   $response["error_msg"]   = "<div class='alert alert-danger text-center' alert-dismissable><button type='button' class='close' data-dismiss='alert'>&times;</button>Fecha de Balance es Obligatorio</div>";
//			   echo json_encode($response);
		}else if($Coope->Cupo==""){
		       $response["campo"]       = "cupo";
			   $response["error_msg"]   = "<div class='alert alert-danger text-center' alert-dismissable><button type='button' class='close' data-dismiss='alert'>&times;</button>Cupo de Obra es Obligatorio</div>";
			   echo json_encode($response);
		}else if($Coope->Domicilio==""){
				$response["campo"]       = "domicilio";
				$response["error_msg"]   = "<div class='alert alert-danger text-center' alert-dismissable><button type='button' class='close' data-dismiss='alert'>&times;</button>Domicilio es Obligatorio</div>";
				echo json_encode($response);
		}else if($Coope->Localidad=="0"){
				$response["campo"]       = "id_localidad";
				$response["error_msg"]   = "<div class='alert alert-danger text-center' alert-dismissable><button type='button' class='close' data-dismiss='alert'>&times;</button>Localidad es Obligatorio</div>";
				echo json_encode($response);				
 	//	}else if($Coope->Telefono==""){
	//			$response["campo"]       = "telefono";
	//			$response["error_msg"]   = "<div class='alert alert-danger text-center' alert-dismissable><button type='button' class='close' data-dismiss='alert'>&times;</button>Telefono es Obligatorio</div>";
//				echo json_encode($resonse);	
       }else if($Coope->Celular==""){
				$response["campo"]       = "celular";
				$response["error_msg"]   = "<div class='alert alert-danger text-center' alert-dismissable><button type='button' class='close' data-dismiss='alert'>&times;</button>Celular es Obligatorio</div>";
				echo json_encode($response);
	     }else if($Coope->Tipo=="0"){
				$response["campo"]       = "tipo";
				$response["error_msg"]   = "<div class='alert alert-danger text-center' alert-dismissable><button type='button' class='close' data-dismiss='alert'>&times;</button>Tipo es Obligatorio</div>";
				echo json_encode($response);	
					
		}else{  
				/*Verificamos si Existe el codigo de barras*/
				if($Coope->Id==""){
					$ExisteCoop         = $this->cooperativas_model->ExisteCodigo($Coope->Matricula);
					if($ExisteCoop==true){
						$response["campo"]     = "matricula";
						$response["error_msg"]   = "<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>La Matricula Ya esta en Uso</div>";
						echo json_encode($response);
					}else{
						$RegistrCoop 		= array(
						'numero'            => $Coope->Numero,
						'matricula'	    	=> $Coope->Matricula,
						'cuit'		        => $Coope->Cuit,
						'resolucion'	    => $Coope->Resolucion,
						'cbu'       	    => $Coope->Cbu,
						'banco'	            => $Coope->Banco,
						'fecha'	            => date('Y-m-j H:i:s'),	
						'fecha_balance'	    => $Coope->Fecha,
						'cupo'	            => $Coope->Cupo,
						'razonsocial'	    => $Coope->Razonsocial,
						'domicilio'	        => $Coope->Domicilio,
						'id_localidad'	    => $Coope->Localidad,
						'telefono'	        => $Coope->Telefono,
						'celular'		    => $Coope->Celular,
			    		'tipo'	            => $Coope->Tipo,
					    'estado'            => $Coope->Estado,
					
						'usuario'           =>$this->session->userdata('NOMBRE').' '.$this->session->userdata('APELLIDOS')
						
						);
						//Bitácora
						 $CADENA=print_r($RegistrCoop,true);
		                 $RegistrBitacoras = array(
		                'fecha'             => date('Y-m-d H:i:s'),
						'usuario'     		=> $this->session->userdata('ID'),
						'mensaje'	    	=> "Alta de Coopetivas: $CADENA");	

                         $this->bitacoras_model->AgregarBitacoras($RegistrBitacoras);
			             //-//
						
						$this->cooperativas_model->SaveCooperativas($RegistrCoop);
						$response["error_msg"]   = "<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Informacion Guardada Correctamente</div>";
						echo json_encode($response);
					}
				}else{
				     
                         							  
				
					$UpdateCooperativas 		= array(
					    'numero'            => $Coope->Numero,
						'matricula'	    	=> $Coope->Matricula,
						'cuit'		        => $Coope->Cuit,
						'resolucion'	    => $Coope->Resolucion,
						'resolucion'	    => $Coope->Resolucion,
						'cbu'       	    => $Coope->Cbu,	
						'banco'	            => $Coope->Banco,
						'fecha'	            => date('Y-m-j H:i:s'),
						'fecha_balance'	    => $Coope->Fecha,
						'cupo'	            => $Coope->Cupo,
						'razonsocial'	    => $Coope->Razonsocial,
						'domicilio'	        => $Coope->Domicilio,
						'id_localidad'	    => $Coope->Localidad,
						'telefono'	        => $Coope->Telefono,
						'celular'		    => $Coope->Celular,
						'tipo'	            => $Coope->Tipo,
						'estado'            => $Coope->Estado ,
					   
			        //    'FechaEdicion'		=> date('Y-m-j H:i:s'),
						'usuario'           =>$this->session->userdata('NOMBRE').' '.$this->session->userdata('APELLIDOS')
						);
						//Bitácora
						 $CADENA=print_r($UpdateCooperativas,true);
		                 $RegistrBitacoras = array(
		                'fecha'             => date('Y-m-d H:i:s'),
						'usuario'     		=> $this->session->userdata('ID'),
						'mensaje'	    	=> "Modificación de Coopertivas: $CADENA");	

                         $this->bitacoras_model->AgregarBitacoras($RegistrBitacoras);
			             //-//
						
						
						
						
					$this->cooperativas_model->UpdateCooperativas($UpdateCooperativas,$Coope->Id);
					$response["error_msg"]   = "<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Informacion Actualizada Correctamente</div>";
					echo json_encode($response);
				}
		}
	}

	    
	//================================================================================
	//                       REQUISITOS de la Cooperativa
	//================================================================================
	//  tiene que  verificar si existe el requisito si existe en caso contrario crear.
	public function Requisitos($id){
		  $id = base64_decode($id);
	
	      
	    	
     	
		  $data = array(
         'titulo' => 'Requisitos',
         'DetalleCoop'    => $this->cooperativas_model->BuscarCooperativa($id),
		 'Req' => $this->requisitos_model->BuscarRequisitos($id),
	     'ID_Coop' => $id ,
	    );
						
		  $this->load->view('constant');
          $this->load->view('view_header');
		 
		  $this->load->view('inaes/view_requisitos_inaes', $data);
         
          $this->load->view('view_footer');
	
	}
	
	//================================================================================
	//                       REQUISITOS de la Cooperativa  grabar
	//================================================================================
	
	 public function GrabarRequisitos(){
	 
	      $url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
          $this->seguridad_model->SessionActivo($url);
          $Req         = json_decode($this->input->post('RequisitosPost'));
          $response = array (
                    "estatus"   => false,
                    "campo"     => "",
                 "error_msg" => "Graba/Modifica Requisito"
         );
		 
		   
		
            if($Req->Id==""){
                     $RegistraRequisito    = array(
                          'comentario'         => $Req->Comentario,
                          'id_cooperativa'     => $Req->Id_Coop,
						  'requisito1'         => $Req->Requisito1,
						  'requisito2'         => $Req->Requisito2,
						  'requisito3'         => $Req->Requisito3
						  
						    
                     );
				
                     $this->requisitos_model->SaveRequisitos($RegistraRequisito);
					 
					 //	cambiar el ESTADO de la cooperativa en caso que cumpla los requisitos	a qui fer
				       $EstadoAr = array('estado' => '2');
				      $requi=$Req->Requisito1 + $Req->Requisito2 + $Req->Requisito3;
					//  echo $requi;
					  if($requi==3){
		        	    $this->cooperativas_model->EstadoCooperativa($EstadoAr,$Req->Id_Coop);
					 }					
					
					
					
					 //Bitácora
					   $CADENA=print_r($RegistraRequisito,true);
		                $RegistrBitacoras = array(
		                'fecha'             => date('Y-m-d H:i:s'),
						'usuario'     		=> $this->session->userdata('ID'),
						'mensaje'	    	=> "Requisitos INAES $CADENA" );
				   
		             	$this->bitacoras_model->AgregarBitacoras($RegistrBitacoras);
		         	//-//
					 
					 
					 
                     $response["error_msg"]   = "<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Informacion Guardada Correctamente</div>";
                     echo json_encode($response);
               }
               if($Req->Id!=""){
                    $UpdateRequisito    = array(
					     'comentario'         => $Req->Comentario,
                          'id_cooperativa'     => $Req->Id_Coop,
						  'requisito1'         => $Req->Requisito1,
						  'requisito2'         => $Req->Requisito2,
						  'requisito3'         => $Req->Requisito3
						 					
                      					 
                    );
                    $this->requisitos_model->UpdateRequisitos($UpdateRequisito, $Req->Id_Coop);
					
				//	cambiar el ESTADO de la cooperativa en caso que cumpla los requisitos	a qui fer
				       $EstadoAr = array('estado' => '2');
				      $requi=$Req->Requisito1 + $Req->Requisito2 + $Req->Requisito3;
					//  echo $requi;
					  if($requi==3){
		        	    $this->cooperativas_model->EstadoCooperativa($EstadoAr,$Req->Id_Coop);
					 }					
					
					
					
					 //Bitácora
					   $CADENA=print_r($UpdateRequisito,true);
		                $RegistrBitacoras = array(
		                'fecha'             => date('Y-m-d H:i:s'),
						'usuario'     		=> $this->session->userdata('ID'),
						'mensaje'	    	=> "Requisitos INAES $CADENA" );
				   
		             	$this->bitacoras_model->AgregarBitacoras($RegistrBitacoras);
		         	//-//
					
					
					
                    $response["error_msg"]   = "<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button> Informacion Actualizada Correctamente</div>";
                    echo json_encode($response);
               }
          
     }
	
	
	
	
    
	//================================================================================
	//                       CREDENCIAL DE Cooperativa
	//================================================================================
	 public	function Credencial($id) {
	
    $id = base64_decode($id);
	      //Bitácora
		  $RegistrBitacoras = array(
		  'fecha'             => date('Y-m-d H:i:s'),
		  'usuario'     		=> $this->session->userdata('ID'),
		  'mensaje'	    	=> "Ingreso Credencial  id : $id");	
           $this->bitacoras_model->AgregarBitacoras($RegistrBitacoras);
			             //-//
	  $data = array(
      'title' => 'Credencial',
      'DetalleProd' => $this->cooperativas_model->BuscarCooperativa($id),
	    );
						
	
			$this->load->view('cooperativas/view_credencial', $data);
		}
	   
	//================================================================================
	//                       IMPRIME  Cooperativa con socios
	//================================================================================
	
	
		public function ImprimeCooperativas($numOrder){
		$numOrder         = $numOrder;
		$numOrder			   = base64_decode($numOrder);
		
		//Bitácora
		 
		  $RegistrBitacoras = array(
		  'fecha'             => date('Y-m-d H:i:s'),
		  'usuario'     		=> $this->session->userdata('ID'),
		  'mensaje'	    	=> "Ingreso Cooperativas  numOrden : $numOrder");	
           $this->bitacoras_model->AgregarBitacoras($RegistrBitacoras);
			             //-//
	///cargar coooperativas
     $data["DocCoop"] = $this->cooperativas_model->TraeCoop($numOrder); 
    ///cargar socios	
	 $data["ListSocios"]= $this->cooperativas_model->TraeSocios($numOrder);	
		
		
	//	$numOrder = 1096;
		
	///	$data["DocOrder"] = $this->ventas_model->TraeDoc_1($numOrder);
	//	if ($data["DocOrder"] == null) { $data["DocOrder"] = "";}
		$data["NumOrder"] = $numOrder;
		
	///	$data["ListOrder"]= $this->ventas_model->TraeVenta_1($numOrder);
		
		 $this->load->view('constant');
        $this->load->view('cooperativas/view_print_cooperativas',$data);
	}

	
	
	//-------exportar----------//
	public function exportar()
	{
	      //Bitácora
		   $RegistrBitacoras = array(
		                'fecha'             => date('Y-m-d H:i:s'),
						'usuario'     		=> $this->session->userdata('ID'),
						'mensaje'	    	=> "Exporto Datos de Cooperativa en formato Excel" );	

            $this->bitacoras_model->AgregarBitacoras($RegistrBitacoras);
			//-//
		$this->load->helper('mysql_to_excel_helper');
        $this->load->model('cooperativas_model');
		to_excel($this->cooperativas_model->get(), "CooperativasExcel");
	}

}