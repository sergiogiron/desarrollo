<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
date_default_timezone_set('America/Mexico_City');
class usuarios extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('seguridad_model');
		$this->load->model('usuarios_model');
		$this->load->model('bitacoras_model');
	}
	public function index(){
          $url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
          $this->seguridad_model->SessionActivo($url);
          /**/
          $this->load->view('constant');
          $this->load->view('view_header');
          $data['usuarios'] = $this->usuarios_model->ListarUsuarios();
          $this->load->view('usuarios/view_usuarios', $data);
          $this->load->view('view_footer');
          
	}
     public function deleteuser(){
        $url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
          $this->seguridad_model->SessionActivo($url);
          $Usuarios       = json_decode($this->input->post('MiUsuario'));
          $id             = base64_decode($Usuarios->Id);
          /*Array de response*/
           $response = array (
               "error_msg" => ""
         );
           $this->usuarios_model->EliminarUsuario($id);
           $response["error_msg"]   = "<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Usuario Eliminado Correctamente, La Información de Actualizara en 5 Segundos <meta http-equiv='refresh' content='5'></div>";
           echo json_encode($response);
     }
     public function nuevo(){
          $url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
          $this->seguridad_model->SessionActivo($url);
        //  $url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
          //$this->seguridad_model->SessionActivo($url);
          $this->load->view('constant');
          $this->load->view('view_header');
		  $data["titulo"] = "Nuevo Usuario";
          $this->load->view('usuarios/view_nuevo_usuario',$data);
          $this->load->view('view_footer');
     }
     public function Editar($id){
          $url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
          $this->seguridad_model->SessionActivo($url);
          $id                = base64_decode($id);
          $data["usuarios"]  = $this->usuarios_model->BuscarUsuario($id);
          $data["titulo"]    = "Editar Usuario";
          $this->load->view('constant');
          $this->load->view('view_header');
          $this->load->view('usuarios/view_nuevo_usuario',$data);
          $this->load->view('view_footer');
     }
     public function Save(){
          $url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
          $this->seguridad_model->SessionActivo($url);
          $Usuarios           = json_decode($this->input->post('UsuariosPost'));
		  
		  
		   //Bitácora
		   $CADENA=print_r($Usuarios,true);
		   $RegistrBitacoras = array(
		   'fecha'             => date('Y-m-d H:i:s'),
			'usuario'     		=> $this->session->userdata('ID'),
			'mensaje'	    	=> "Alta de Usuario: $CADENA");	

            $this->bitacoras_model->AgregarBitacoras($RegistrBitacoras);
			             //-//
	
		  
		  
		  
          $response = array (
                    "campo"     => "",
                    "error_msg" => ""
         );
          if($Usuarios->Nombre==""){
               $response["campo"]     = "Nombre";
               $response["error_msg"]   = "<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>El Nombre es Obligatorio</div>";
echo json_encode($response);
          }else if($Usuarios->Apellidos==""){
               $response["campo"]     = "Apellidos";
               $response["error_msg"]   = "<div class='alert alert-danger text-center' alert-dismissable><button type='button' class='close' data-dismiss='alert'>&times;</button>El Campo Apellido es obligatorio</div>";
echo json_encode($response);
          }else if($Usuarios->Email==""){
               $response["campo"]       = "Email";
               $response["error_msg"]   = "<div class='alert alert-danger text-center' alert-dismissable><button type='button' class='close' data-dismiss='alert'>&times;</button>El Correo Es Obligatorio</div>";
echo json_encode($response);
          }else if($Usuarios->TipoUsuario=="0"){
               $response["campo"]       = "TipoUsuario";
               $response["error_msg"]   = "<div class='alert alert-danger text-center' alert-dismissable><button type='button' class='close' data-dismiss='alert'>&times;</button>Tipo de Usuario es Obligatorio</div>";
echo json_encode($response);
          }else if($Usuarios->Password1==""){
               $response["campo"]       = "Password1";
               $response["error_msg"]   = "<div class='alert alert-danger text-center' alert-dismissable><button type='button' class='close' data-dismiss='alert'>&times;</button>La Contraseña Es Obligatorio</div>";
echo json_encode($response);
          }else if($Usuarios->Password2==""){
               $response["campo"]       = "Password2";
               $response["error_msg"]   = "<div class='alert alert-danger text-center' alert-dismissable><button type='button' class='close' data-dismiss='alert'>&times;</button>La confirmación de Contraseña Es Obligatorio</div>";
echo json_encode($response);
          }else if($Usuarios->Estatus=="0"){
               $response["campo"]       = "Estatus";
               $response["error_msg"]   = "<div cla$Usuarios->TipoUsuario,ss='alert alert-danger text-center' alert-dismissable><button type='button' class='close' data-dismiss='alert'>&times;</button>El Estatus es Obligatorio</div>";
echo json_encode($response);
          }else if($Usuarios->Password1!=$Usuarios->Password2){
               $response["campo"]       = "Password2";
               $response["error_msg"]   = "<div class='alert alert-danger text-center' alert-dismissable><button type='button' class='close' data-dismiss='alert'>&times;</button>La confirmación de Contraseña Es Incorrecta</div>";
echo json_encode($response);
          }else{
			  //verfificar codigo ???? /////////////VEEERRRR
               if($Usuarios->Id==""){
                    $ExisteEmail       = $this->usuarios_model->ExisteEmail($Usuarios->Email);
                    if($ExisteEmail==true){
                         $response["campo"]     = "Email";
                         $response["error_msg"]   = "<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>El Email Ya esta en Uso</div>";
echo json_encode($response);
                    }else{

                         $RegistraUser    = array(
                         'NOMBRE'         => ucwords($Usuarios->Nombre),
                         'APELLIDOS'      => ucwords($Usuarios->Apellidos),
                         'EMAIL'          => $Usuarios->Email,
                         'FECHA_REGISTRO' => date('Y-m-j H:i:s'),
                         'ESTATUS'        => $Usuarios->Estatus,
                         'TIPO'           => $Usuarios->Tipo,
                         'PASSWORD'       => crypt($Usuarios->Password1),
						 'PRIVILEGIOS'    => $Usuarios->Privilegios,
                         );    
						 
						 //Bitácora
						 $CADENA=print_r($RegistraUser,true);
		                 $RegistrBitacoras = array(
		                'fecha'             => date('Y-m-d H:i:s'),
						'usuario'     		=> $this->session->userdata('ID'),
						'mensaje'	    	=> "Alta de Usuario: $CADENA");	

                                            $this->bitacoras_model->AgregarBitacoras($RegistrBitacoras);
			             //-//
			 
						 					 
                         $this->usuarios_model->SaveUsuarios($RegistraUser);
                         $response["error_msg"]   = "<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button> Informacion Guardada Correctamente</div>";
						 echo json_encode($response);
                    }
               }else{
                    $newPassword     = $Usuarios->Password1;
                    $newPassword     = strlen($newPassword);
                    if($newPassword >= 20){
                         $newPassword = $Usuarios->Password1;
                    }else{
                         $newPassword = crypt($Usuarios->Password1);
                    }
                    $UpdateUser      = array(
                    'NOMBRE'         => ucwords($Usuarios->Nombre),
                    'APELLIDOS'      => ucwords($Usuarios->Apellidos),
                    'EMAIL'          => $Usuarios->Email,
                    'ESTATUS'        => $Usuarios->Estatus,
                    'TIPO'           => $Usuarios->TipoUsuario,
                    'PASSWORD'       => $newPassword,
					'PRIVILEGIOS'    => $Usuarios->Privilegios,
                    );    
					//Bitácora
						 $CADENA=print_r($RegistraUser,true);
		                 $RegistrBitacoras = array(
		                'fecha'             => date('Y-m-d H:i:s'),
						'usuario'     		=> $this->session->userdata('ID'),
						'mensaje'	    	=> "Modificación de Usuarios: $CADENA");	

                         $this->bitacoras_model->AgregarBitacoras($RegistrBitacoras);
			             //-//
			
                    $this->usuarios_model->UpdateUsers($UpdateUser,$Usuarios->Id);
                    $response["error_msg"]   = "<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button> Informacion Actualizada Correctamente</div>";
                     echo json_encode($response);
			 }
          }
          
     }
}