<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
date_default_timezone_set('America/Argentina/Jujuy');
class localidades extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('seguridad_model');
		$this->load->model('localidades_model');
		$this->load->model('bitacoras_model');
	}
      /**
       * Carga datos de iniciación
       * @return type
       */
    	public function index(){
    	      //Bitácora
    		    $RegistrarBitacora = array(
    		        'fecha'           => date('Y-m-d H:i:s'),
    						'usuario'     		=> $this->session->userdata('ID'),
    						'mensaje'	    	  => "Ingresó a Localidad" );
    				   
    			 $this->bitacoras_model->AgregarBitacoras($RegistrarBitacora);
    			
              $url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
              $this->seguridad_model->SessionActivo($url);
              $this->load->view('constant');    //Carga datos constantes
              $this->load->view('view_header'); //Carga datos de encabezado
              $data['localidades'] = $this->localidades_model->listarLocalidad(); //Almacena en un array las localidades obtenidas
              $this->load->view('localidades/view_localidades', $data); //Muestro los datos obtenidos en una vista
              $this->load->view('view_footer');
              
    	     }
           /**
          * Editar los datos de una localidad
          * @param type $id identificador de cada localidad
          * @return type
          */
         public function editarLocalidades($id){

              $url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
              $this->seguridad_model->SessionActivo($url);
              $id =  base64_decode($id);
              //Bitácora
              $RegistrarBitacora = array(
                  'fecha'           => date('Y-m-d H:i:s'),
                  'usuario'         => $this->session->userdata('ID'),
                  'mensaje'         => "Editó una localidad con id : $id" );

              $this->bitacoras_model->AgregarBitacoras($RegistrarBitacora);
              $this->load->view('constant');
              $this->load->view('view_header');
              $data['localidad'] = $this->localidades_model->buscarLocalidad($id); //'localidad'nombre del objeto creado en JSON
              $data['titulo'] = "Editar Localidad";
              $this->load->view('localidades/view_nuevo_localidades', $data);
              $this->load->view('view_footer');

              

         }
           /**
            * Elimina una localidad registrada
            * @return type
            */
	    public function deletelocalidades(){
	
        $url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
        $this->seguridad_model->SessionActivo($url);
		    $Loca	= json_decode($this->input->post('LocalidadesPost'));
		    $id   = base64_decode($Loca->Id);
        //Bitácora
        $RegistrarBitacora = array(
            'fecha'           => date('Y-m-d H:i:s'),
            'usuario'         => $this->session->userdata('ID'),
            'mensaje'         => "Se Borró Localidad con id : $id" );
         $this->bitacoras_model->AgregarBitacoras($RegistrarBitacora);
    		/*Array de response*/
    		 $response = array (
    				"estatus"   => false,
    	      "error_msg" => ""
    	    );

    		 $this->localidades_model->eliminarLocalidad($id);// Llama a la función del modelo, para eliminar un registro
    		 $response["error_msg"]   = "<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Localidad eliminada Correctamente Clave: <strong>".$id."</strong>, La Información de Actualizara en 5 Segundos <meta http-equiv='refresh' content='5'></div>";
    		 echo json_encode($response);
	       }
         /**
          * Crear una nueva localidad
          * @return type
          */
         public function nuevo(){

              $url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
              $this->seguridad_model->SessionActivo($url);
              $this->load->view('constant');
              $this->load->view('view_header');
              $data['titulo'] = "Nueva Localidad";
              $this->load->view('localidades/view_nuevo_localidades', $data);
              $this->load->view('view_footer');

              $RegistrarBitacora = array(
                'fecha'           => date('Y-m-d H:i:s'),
                'usuario'         => $this->session->userdata('ID'),
                'mensaje'         => "Creó una nueva localidad" );
                $this->bitacoras_model->AgregarBitacoras($RegistrarBitacora);
         }
         
         /**
          * Guardar una localidad 
          * @return type
          */
        public function saveLocalidades(){
          
          $url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
          $this->seguridad_model->SessionActivo($url);
          $Localidades           = json_decode($this->input->post('LocalidadesPost')); //Obtiene los datos de localidades por JSON
          $response = array (
                    "estatus"   => false,
                    "campo"     => "",
                    "error_msg" => ""
                    );
          if($Localidades->Localidad==""){
               $response["campo"]       = "localidad"; //Campo del formulario
               $response["error_msg"]   = "<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button> Localidad es Obligatorio</div>";
               echo json_encode($response);
          }else if($Localidades->Departamento=="0"){
               $response["campo"]       = "departamento";
               $response["error_msg"]   = "<div class='alert alert-danger text-center' alert-dismissable><button type='button' class='close' data-dismiss='alert'>&times;</button>Debe seleccionar un Departamento</div>";
               echo json_encode($response);
          }else if($Localidades->Estado=="0"){
                    $response["campo"]       = "estado";
                    $response["error_msg"]   = "<div class='alert alert-danger text-center' alert-dismissable><button type='button' class='close' data-dismiss='alert'>&times;</button>Debe selleccionar un estado</div>";
                    echo json_encode($response);
          }else if($Localidades->Poblacion==""){
               $response["campo"]       = "poblacion";
               $response["error_msg"]   = "<div class='alert alert-danger text-center' alert-dismissable><button type='button' class='close' data-dismiss='alert'>&times;</button>Poblacion es Obligatorio</div>";
               echo json_encode($response);
          }else if($Localidades->Id==""){
                    $RegistraLocalidad = array(
                     'nombre_localidad'       => $Localidades->Localidad, //campo nombre_localidad de la BD
                     'departamento'    => $Localidades->Departamento,
                     'estado'          => $Localidades->Estado,
                     'poblacion'       => $Localidades->Poblacion
                    //  'fecha_registro'  => date('Y-m-j H:i:s')
                     );
                  $CADENA=print_r($RegistraLocalidad,true);

                  $RegistrarBitacora = array(
                        'fecha'           => date('Y-m-d H:i:s'),
                        'usuario'         => $this->session->userdata('ID'),
                        'mensaje'         => "Alta de Localidad: $CADENA");
                  $this->bitacoras_model->AgregarBitacoras($RegistrarBitacora); 

                  $this->localidades_model->saveLocalidad($RegistraLocalidad);
                  $response["error_msg"]   = "<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Informacion Guardada Correctamente</div>";
                  echo json_encode($response);

          }if($Localidades->Id!=""){ //Se editan los datos existentes
                    $UpdateLocalidad    = array(
                     'nombre_localidad'         => $Localidades->Localidad, //Campo nombre_localidad de la BD
                     'departamento'      => $Localidades->Departamento,
                     'estado'            => $Localidades->Estado,
                     'poblacion'         => $Localidades->Poblacion
                //     'fecha_registro'      => date('Y-m-j H:i:s')
                     );
                         
						          $CADENA=print_r($UpdateLocalidad,true);
                      //Bitácora
                      $RegistrarBitacora  = array(
                      'fecha'          => date('Y-m-d H:i:s'),
                      'usuario'        => $this->session->userdata('ID'),
                      'mensaje'        => "Modificación de Localidad: $CADENA");  

                      $this->bitacoras_model->AgregarBitacoras($RegistrarBitacora);

                      $this->localidades_model->updateLocalidad($UpdateLocalidad, $Localidades->Id);
                      $response["error_msg"]   = "<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button> Informacion Actualizada Correctamente</div>";
                      echo json_encode($response);     
                  }

             }
        }