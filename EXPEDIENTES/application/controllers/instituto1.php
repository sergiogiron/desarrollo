<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
date_default_timezone_set('America/Argentina/Jujuy');
class instituto extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('instituto_model');
		$this->load->model('seguridad_model');
		$this->load->model('bitacoras_model');
	  }
      
    	public function index(){
    	      //Bitácora
    		    $RegistrarBitacora = array(
    		        'fecha'           => date('Y-m-d H:i:s'),
    						'usuario'     		=> $this->session->userdata('ID'),
    						'mensaje'	    	  => "Ingresó a pantalla IVUJ" );
    				   
    			 $this->bitacoras_model->AgregarBitacoras($RegistrarBitacora);
    			
              $url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
              $this->seguridad_model->SessionActivo($url);
              $this->load->view('constant');    //Carga datos constantes
              $this->load->view('view_header'); //Carga datos de encabezado
              $data['viviendas'] = $this->instituto_model->listarAdjudicaciones();
              $this->load->view('instituto/view_instituto', $data);
              $this->load->view('view_footer');
              
    	     }

           public function imprimir(){

              $url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
              $id = base64_decode($id);
              $idCop= base64_decode($idCop);
              $data["titulo"]    = "Impresion";
              $this->load->view('constant');
              $data["Bene"] = $this->adjudicaciones_model->TraerBeneficiario($id);
              $data["Coope"] = $this->adjudicaciones_model->TraerCooperativa($idCop);
              $this->load->view('adjudicaciones/view_resolucion_adjudicaciones', $data);
                 // $this->load->view('view_footer');
              //echo json_encode($productos);
        } 
         public function buscarNotaAdj(){

              $url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
              $this->seguridad_model->SessionActivo($url);

              $filtro2   = $this->input->get("NumeroNota"); //Obtengo el dato del input

              $data= $this->instituto_model->buscarNota($filtro2);

              $this->load->view('instituto/view_instituto', $data);
  
              echo json_encode($data);

              //$this->load->view('constant');
              //$filtro    = $this->input->get("term");
              //$res   = $this->input->get("Resolucion");
              //$bandera   = $this->input->get("bandera");
              //$notas     = $this->instituto_model->buscarNota($res);
              //$this->load->view('instituto/view_instituto', $data);
              //$this->load->view('view_footer');
              //echo json_encode($notas);
              //Bitácora
            $RegistrarBitacora = array(
                'fecha'           => date('Y-m-d H:i:s'),
                'usuario'         => $this->session->userdata('ID'),
                'mensaje'         => "Buscar nota!" );  
           $this->bitacoras_model->AgregarBitacoras($RegistrarBitacora);
         }


         /**
      * busca coperativas segun un termino y cuidad
      * @return return objeto json  
      */
  /*public function BuscarCooperativa(){
    $filtro    = $this->input->get("term");
    $filtro2= $this->input->get("Ciudad");
      $bandera =$this->input->get("bandera");
    $clientes = $this->adjudicaciones_model->buscarCooperativa($filtro,$filtro2,$bandera);
    echo json_encode($clientes);
  
  }      
         /**
          * Guardar una localidad 
          * @return type
          */
        public function saveCargas(){
          
          $url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
          $this->seguridad_model->SessionActivo($url);
          $Viviendas           = json_decode($this->input->post('ViviendasPost'));
          $response = array (
                    "estatus"   => false,
                    "campo"     => "",
                    "error_msg" => ""
                    );
          if($Viviendas->NumeroNota==""){
               $response["campo"]       = "NumeroNota"; //Campo del formulario
               $response["error_msg"]   = "<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button> Número de Nota de Adjucación es Obligatorio</div>";
               echo json_encode($response);
          }else if($Viviendas->NumeroExpediente==""){
               $response["campo"]       = "NumeroExpediente";
               $response["error_msg"]   = "<div class='alert alert-danger text-center' alert-dismissable><button type='button' class='close' data-dismiss='alert'>&times;</button>Debe ingresar un número de Expediente</div>";
               echo json_encode($response);

          }else if($Viviendas->Id==""){
                    $RegistraCarga = array(
                     'nro_nota'       => $Viviendas->NumeroNota,
                     'nro_expediente' => $Viviendas->NumeroExpediente
                     );
                  $CADENA=print_r($RegistraCarga,true);

                  /*$RegistrarBitacora = array(
                        'fecha'           => date('Y-m-d H:i:s'),
                        'usuario'         => $this->session->userdata('ID'),
                        'mensaje'         => "Alta de Localidad: $CADENA");
                  $this->bitacoras_model->AgregarBitacoras($RegistrarBitacora);*/

                  $this->instituto_model->saveCarga($RegistraCarga);
                  $response["error_msg"]   = "<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Informacion Guardada Correctamente</div>";
                  echo json_encode($response);

          }if($Viviendas->Id!=""){ //Se editan los datos existentes
                    $UpdateCarga  = array(
                     'nro_nota'         => $Viviendas->NumeroNota, //Campo nombre_localidad de la BD
                     'nro_expediente'   => $Viviendas->NumeroExpediente
                     );
                         
						          $CADENA=print_r($UpdateCarga,true);
                      //Bitácora
                      /*$RegistrarBitacora  = array(
                      'fecha'          => date('Y-m-d H:i:s'),
                      'usuario'        => $this->session->userdata('ID'),
                      'mensaje'        => "Modificación de Localidad: $CADENA");  

                      $this->bitacoras_model->AgregarBitacoras($RegistrarBitacora);*/

                      $this->instituto_model->updateCarga($UpdateCarga, $Viviendas->Id);
                      $response["error_msg"]   = "<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button> Informacion Actualizada Correctamente</div>";
                      echo json_encode($response);     
                  }

             }
        }