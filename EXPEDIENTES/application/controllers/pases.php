<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
date_default_timezone_set('America/Argentina/Jujuy');

class pases extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('seguridad_model');
		$this->load->model('pases_model');
		$this->load->model('bitacoras_model');
    $this->load->model('expedientes_model');
    $this->load->model('historial_expediente_model');
	  // ----------exportar------------//
		$this->load->helper('mysql_to_excel_helper');
		
	}


	public function index(){ 
          //Bitácora
          $RegistrarBitacora = array(
              'fecha'           => date('Y-m-d H:i:s'),
              'usuario'         => $this->session->userdata('ID'),
              'mensaje'         => "Ingresó a Pases" );
             
          $this->bitacoras_model->AgregarBitacoras($RegistrarBitacora);

          $url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
          $this->seguridad_model->SessionActivo($url);
          $this->load->view('constant');    //Carga vista títulos de Inicio
          $this->load->view('view_header'); //Carga vista del Menú Horizontal
          $data['pase'] = $this->pases_model->listarPase();
          $this->load->view('pases/view_pases', $data);
          $this->load->view('view_footer'); //Carga vista Pie de Página  
	   }


   public function nuevoPase($id){
          
          $url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; 
          $this->seguridad_model->SessionActivo($url);
   
          $id =$id;
          //Bitácora
          $RegistrarBitacora = array(
              'fecha'           => date('Y-m-d H:i:s'),
              'usuario'         => $this->session->userdata('ID'),
              'mensaje'         => "Ingresó a Nuevo Pase" );
             
          $this->bitacoras_model->AgregarBitacoras($RegistrarBitacora);

          $this->load->view('constant'); //Carga vista títulos de Inicio
          $this->load->view('view_header'); //Carga vista del Menú Horizontal
          $data['titulo'] ="Nuevo  Pase"; // El titulo no tiene que ser igual al de la vista, por eso el espacio entre las palabras
          $data['pase'] =$this->expedientes_model->buscarExp($id);
          $this->load->view('pases/view_nuevo_pases', $data);
          $this->load->view('view_footer');
     }

     public function editarPase($id){

          $url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
          $this->seguridad_model->SessionActivo($url);
          $id =  base64_decode($id);
           //Bitácora
          $RegistrarBitacora = array(
              'fecha'           => date('Y-m-d H:i:s'),
              'usuario'         => $this->session->userdata('ID'),
              'mensaje'         => "Editó Pase con id : $id" );
          $this->bitacoras_model->AgregarBitacoras($RegistrarBitacora);

          $this->load->view('constant');      //Carga vista titulos de Inicio 
          $this->load->view('view_header');   //Carga vista del Menú Principal
          //print_r($id);
          $data['pase'] = $this->expedientes_model->buscarPase($id); 
         
          $data['titulo'] = "Editar Pase";
          $this->load->view('pases/view_nuevo_pases', $data);
          $this->load->view('view_footer');   //Carga vista Pie de Página
            
     }


     public function savePase(){
	 
	        $url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
          $this->seguridad_model->SessionActivo($url);
          $Pase = json_decode($this->input->post('PasePost')); 
          $response = array (
                    "estatus"   => false,
                    "campo"     => "",
                    "error_msg" => ""
          );    
       
          //CÓDIGO, FECHA Y ASUNTO SE MUESTRAN DE EXPEDIENTES
           /*    
        }else if($Exp->Remitente==""){  
               $response["campo"]       = "remitente"; //Campo del formulario
               $response["error_msg"]   = "<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Debe ingresar Remitente</div>";
               echo json_encode($response); //Retorna la representación JSON del valor dado
         */  
        if($Pase->FechaPase==""){
              $response["campo"]       = "fechaPase"; //Campo del formulario
              $response["error_msg"]   = "<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Debe ingresar Fecha</div>";
               echo json_encode($response);

        }else if($Pase->Are=="0"){  
               $response["campo"]       = "area"; //Campo del formulario
               $response["error_msg"]   = "<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Debe seleccionar un Área</div>";
               echo json_encode($response);
               
        }else if($Pase->Estado=="0"){   //Si no selecciona una opc, muestra un alerta de campo obligatorio.
                $response["campo"]      ="estado";
                $response["error_msg"]  ="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Debe seleccionar un Estado</div>";
                echo json_encode($response); //Retorna la representación JSON del valor dado
           
          //CAMPO OBSERVACIONES NO CONSIDERADO COMO OBLIGATORIO...

        }else if($Pase->Id==""){ //Si el id no existe, se REGISTRA un nuevo Pase

					$RegistraPase  = array(
					   'codigo_exp'        => $Pase->Codigo,
             //'anio'              => date('Y'),
             //'remitente_exp'     => $Pase->Remitente,
             'fecha_pase'        => $Pase->FechaPase,
             'id_area'           => $Pase->Are,
             'estado_pase'       => $Pase->Estado,
             'obs_pase'          => $Pase->Observaciones,
              );

          //asignacion

          $codigo_exp=$Pase->Codigo;
          $fecha_exp= $Pase->FechaPase;
          //$id_dep= $Exp->Dependencia;
          //$remitente_exp= $Exp->Remitente;
          //$asunto_exp=$Exp->Asunto;
          $id_area=$Pase->Are;
          $estado_exp=$Pase->Estado;
          $obs_exp=$Pase->Observaciones;

          //Bitacora
          $CADENA=print_r($RegistraPase,true);
          $RegistrarBitacora  = array(
                'fecha'       => date('Y-m-d H:i:s'),
                'usuario'     => $this->session->userdata('ID'),
                'mensaje'     => "Alta de Pase: $CADENA");  
          $this->bitacoras_model->AgregarBitacoras($RegistrarBitacora);

          //historial expediente
          $RegistraExpH="Nº: ".$codigo_exp." Area: ".$id_area." Estado: ".$estado_exp." Observaciones: ".$obs_exp;
          $CADENA_H=print_r($RegistraExpH,  true);
          $RegistrarHistorial  = array(
                'fecha'       => date('Y-m-d H:i:s'),
                'usuario'     => $this->session->userdata('ID'),
                'mensaje'     => "Se agrego un pase al expediente $CADENA_H",
                'codigo_exp'  => $codigo_exp,
                //'id_dep'      => $id_dep,
                'id_area'     => $id_area);
          $this->historial_expediente_model->AgregarBitacoras($RegistrarHistorial);
                    
          $this->pases_model->SavePase($RegistraPase);
          $response["error_msg"]   = "<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Informacion Guardada Correctamente</div>";
          echo json_encode($response); //Retorna la representación JSON del valor dado
              }
              else if($Pase->Id!=""){ //Si el id ya existe, se realiza la ACTUALIZACION de un pase
                    $UpdatePase    = array(
                        //'codigo_exp'        => $Pase->Codigo,
                         //'anio'              => date('Y'),
                        // 'remitente_exp'     => $Pase->Remitente,
                         'fecha_pase'        => $Pase->FechaPase,
                         'id_area'           => $Pase->Are,
                         'estado_pase'       => $Pase->Estado,
                         'obs_pase'          => $Pase->Observaciones,
                          );
                    //Bitácora
                    $CADENA=print_r($UpdatePase,true);
                    $RegistrarBitacora     = array(
                        'fecha'            => date('Y-m-d H:i:s'),
                        'usuario'          => $this->session->userdata('ID'),
                        'mensaje'          => "Editó Pase: $CADENA"); 
                    $this->bitacoras_model->AgregarBitacoras($RegistrarBitacora);

                    //asignacion

                    $codigo_exp=$Pase->Codigo;
                    $fecha_exp= $Pase->FechaPase;
                    //$id_dep= $Exp->Dependencia;
                    //$remitente_exp= $Exp->Remitente;
                    //$asunto_exp=$Exp->Asunto;
                    $id_area=$Pase->Are;
                    $estado_exp=$Pase->Estado;
                    $obs_exp=$Pase->Observaciones;

                    //historial expediente
                    $RegistraExpH="Nº: ".$codigo_exp." Area: ".$id_area." Estado: ".$estado_exp." Observaciones: ".$obs_exp;
                    $CADENA_H=print_r($RegistraExpH,  true);
                    $RegistrarHistorial  = array(
                          'fecha'       => date('Y-m-d H:i:s'),
                          'usuario'     => $this->session->userdata('ID'),
                          'mensaje'     => "edito pase al expediente $CADENA_H",
                          'codigo_exp'  => $codigo_exp,
                          //'id_dep'      => $id_dep,
                          'id_area'     => $id_area);
                    $this->historial_expediente_model->AgregarBitacoras($RegistrarHistorial);

                    $this->pases_model->updatePase($UpdatePase, $Pase->Id);
                    $response["error_msg"]   = "<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button> Informacion Actualizada Correctamente</div>";
                    echo json_encode($response); //Retorna la representación JSON del valor dado
               }
          
   }


  public function areas(){
    $local = $this->expedientes_model->traerAreas();
    echo json_encode($local);

  }
   
	   	public function deletePase(){
	      
         $url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
         $this->seguridad_model->SessionActivo($url);
		     $Pase	= json_decode($this->input->post('PasePost'));
		     $id    = base64_decode($Pase->Id);

         $response = array ( /*Array de response*/
            "estatus"   => false,
            "error_msg" => ""
         );
         //Bitácora
         $RegistrarBitacora = array(
            'fecha'           => date('Y-m-d H:i:s'),
            'usuario'         => $this->session->userdata('ID'),
            'mensaje'         => "Se Borró Pase con id: $id" );
	       $this->bitacoras_model->AgregarBitacoras($RegistrarBitacora);
         $this->pases_model->borrarPase($id);

         //asignacion

         $codigo_exp=$Pase->Codigo;
         /*$fecha_exp= $Pase->FechaPase;
         //$id_dep= $Exp->Dependencia;
         //$remitente_exp= $Exp->Remitente;
         //$asunto_exp=$Exp->Asunto;
         $id_area=$Pase->Are;
         $estado_exp=$Pase->Estado;
         $obs_exp=$Pase->Observaciones;*/

         $RegistrarHistorial  = array(
          'fecha'       => date('Y-m-d H:i:s'),
          'usuario'     => $this->session->userdata('ID'),
          'mensaje'     => "Se Borró pase del expediente con el Nº: $codigo_exp",
          'codigo_exp'  => $codigo_exp);
          //'id_dep'      => $id_dep,
          //'id_area'     => $id_area);
         $this->historial_expediente_model->AgregarBitacoras($RegistrarHistorial);

         $this->pases_model->borrarPase($id); 
         $response["error_msg"]   = "<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Pase eliminado correctamente Clave: <strong>".$id."</strong>, La Información de Actualizara en 5 Segundos <meta http-equiv='refresh' content='5'></div>";
         echo json_encode($response); //Retorna la representación JSON del valor dado     
		    
	     }



      //-------exportar----------//
      	public function exportar(){
      	 //Bitácora
      	$RegistrarBitacora = array(
      		    'fecha'           => date('Y-m-d H:i:s'),
      				'usuario'     		=> $this->session->userdata('ID'),
      				'mensaje'	    	  => "Exportó Datos de Pases en formato Excel" );	

        $this->bitacoras_model->AgregarBitacoras($RegistrarBitacora);
      			//-//
      	$this->load->helper('mysql_to_excel_helper');
        $this->load->model('pases_model');
      	to_excel($this->pases_model->get(), "PasesExcel");
      	}
	
 
}