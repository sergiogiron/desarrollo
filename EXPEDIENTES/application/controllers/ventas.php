<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
date_default_timezone_set('America/Argentina/Jujuy');
class ventas extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('seguridad_model');
		$this->load->model('ventas_model');
		$this->load->model('bitacoras_model');
	 
	}

	 public  function getLocalidad(){ 
      return $this->$Localidad; 
    
   } 
   public function setLocalidad($Localidad){
 $this->Localidad = $Localidad;
}
	 
	public function index(){
          $url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
          $this->seguridad_model->SessionActivo($url);
          /**/
          $this->load->view('constant');
          $this->load->view('view_header');
          $this->load->view('ventas/view_ventas');
          $this->load->view('view_footer');
		
          
	}
	public function BuscarCliente(){
		$filtro    = $this->input->get("term");
		$filtro2= $this->input->get("Ciudad");
		$clientes = $this->ventas_model->buscarcliente($filtro,$filtro2);
		echo json_encode($clientes);
		 }
	 

	public function buscarproductos(){
		$filtro    = $this->input->get("term");
		$filtro2= $this->input->get("Ciudad");
		$productos = $this->ventas_model->listarproducto($filtro,$filtro2);
		echo json_encode($productos);
	}
	public function ImprimeVenta($numOrder){
		$numOrder         = $numOrder;
	//	$numOrder			   = base64_decode($numOrder);
	
		$data["DocOrder"] = $this->ventas_model->TraeDoc($numOrder);
		$data["NumOrder"] = $numOrder;
		
		$data["ListOrder"]= $this->ventas_model->TraeVenta($numOrder);
	
		
		 $this->load->view('constant');
        $this->load->view('ventas/view_print_venta',$data);
	}
	public function localidades(){
		$local = $this->ventas_model->Localidades();
		echo json_encode($local);
	}
	public function programas(){
		$local = $this->ventas_model->Programas();
		echo json_encode($local);
	}
	
	public function saveOrder(){
		session_start();
		$url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
          $this->seguridad_model->SessionActivo($url);
		$arrayResponse = array("NumOrden"=>"0","Msg"=>"Error: Ocurrio Un Error Intente de Nuevo", "TipoMsg"=>"Error");

		$OrderVenta    = json_decode($this->input->post('MiCarrito'));
		$RecuperaOrder = $_SESSION["CarritoVenta".$OrderVenta->IdSession];
		$RecuperaOrder = $_SESSION["CarritoVenta".$OrderVenta->IdSession];
		$impuesto      = 0;
		$arrayDocumento= array(
				"TIPO"				=>"Entrada", 
				"FECHA"				=>date('Y-m-j H:i:s'),
				"CLIENTE"			=>$OrderVenta->Cliente,
				"BASEIMPUESTO"		=>$impuesto,
				"TOTAL_IMPUESTO"	=>$OrderVenta->IVA,
				"BRUTO"				=>$OrderVenta->Subtotal,
				"TOTAL"				=>$OrderVenta->Total,
				"USUARIO"			=>$this->session->userdata('ID'),
				"MARCA"             =>"0000-00-00"   //MARCA PARA IDENTIFICA LOS DIAS PARA EL CIEERRE DE CAJA
				);
				$saveOrderDocument = $this->ordencompra_model->saveOrderDocumento($arrayDocumento);
                       //Bitácora
						 $CADENA=print_r($arrayDocumento,true);
		                 $RegistrBitacoras = array(
		                'fecha'             => date('Y-m-d H:i:s'),
						'usuario'     		=> $this->session->userdata('ID'),
						'mensaje'	    	=> "Recibo :$saveOrderDocument - $CADENA");	

                         $this->bitacoras_model->AgregarBitacoras($RegistrBitacoras);
			             //-//			
		
		if($saveOrderDocument!=0){
			foreach ($RecuperaOrder as $key => $value) {
				$arrayPartidas = array(
					"ID_LINK"			=> $saveOrderDocument,
					"TIPO"				=> "Entrada",
					"FECHA"				=> date('Y-m-j H:i:s'),
					"CLIENTE"			=> $OrderVenta->Cliente,
					"PROVEEDOR"			=> $value["IdProveedor"],
					"CLAVE"				=> $value["txtCodigo"],
					"COSTO"				=> $value["costo"],
					"PRECIO"			=> $value["precio"],
					"DESCRIPCION"		=> $value["descripcion"],
					"SALIDAS"			=> $value["cantidad"]
					);
					 //Bitácora
						 $CADENA=print_r($arrayPartidas,true);
		                 $RegistrBitacoras = array(
		                'fecha'             => date('Y-m-d H:i:s'),
						'usuario'     		=> $this->session->userdata('ID'),
						'mensaje'	    	=> "Detalle : $CADENA");	

                         $this->bitacoras_model->AgregarBitacoras($RegistrBitacoras);
			             //-//
					
		
					
				# code...
				$this->ordencompra_model->saveOrderPartidas($arrayPartidas);
				$this->ventas_model->UpdateExistenciasProducto($value["txtCodigo"],$value["cantidad"]);

			}
			$arrayResponse = array("NumOrden"=>base64_encode($saveOrderDocument),"Msg"=>"<strong>Folio: ".$saveOrderDocument."</strong>, La Venta se Guardado Correctamente", "TipoMsg"=>"Sucefull");
		}
		echo json_encode($arrayResponse);
		// $this->ImprimeVenta($saveOrderDocument);
	}
	public function addcarrito(){
		session_start();
		$url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
          $this->seguridad_model->SessionActivo($url);
		$CarritoNewVenta   = json_decode($this->input->post('MiCarrito'));
		if(isset($_SESSION['CarritoVenta'.$CarritoNewVenta->IdSession])){
				$carrito_orderventa=$_SESSION['CarritoVenta'.$CarritoNewVenta->IdSession];
				if(isset($CarritoNewVenta->Codigo)){
					$txtCodigo = $CarritoNewVenta->Codigo;
					$precio    = $CarritoNewVenta->Pcompra;
					$cantidad  = $CarritoNewVenta->Cantidad;
					$descripcio= $CarritoNewVenta->Descripcion;
					$Proveedor = $CarritoNewVenta->Proveedor;
					$Costo     = $CarritoNewVenta->Costo;
					$IdProveedo= $CarritoNewVenta->IdProveedor;
					$donde     = -1;
					for($i=0;$i<=count($carrito_orderventa)-1;$i ++){
					if($txtCodigo==$carrito_orderventa[$i]['txtCodigo']){
						$donde=$i;
					}
					}
					if($donde != -1){
						$cuanto=$carrito_orderventa[$donde]['cantidad'] + $cantidad;
						$carrito_orderventa[$donde]=array("txtCodigo"=>$txtCodigo,"precio"=>$precio,"cantidad"=>$cuanto,"descripcion"=>$descripcio,"proveedor"=>$Proveedor,"costo"=>$Costo,"IdProveedor"=>$IdProveedo);
					}else{
						$carrito_orderventa[]=array("txtCodigo"=>$txtCodigo,"precio"=>$precio,"cantidad"=>$cantidad,"descripcion"=>$descripcio,"proveedor"=>$Proveedor,"costo"=>$Costo,"IdProveedor"=>$IdProveedo);
						
					}
				}
		}else{
				$txtCodigo = $CarritoNewVenta->Codigo;
				$precio    = $CarritoNewVenta->Pcompra;
				$cantidad  = $CarritoNewVenta->Cantidad;
				$descripcio= $CarritoNewVenta->Descripcion;
				$Proveedor = $CarritoNewVenta->Proveedor;
				$Costo     = $CarritoNewVenta->Costo;
				$IdProveedo= $CarritoNewVenta->IdProveedor;
				$carrito_orderventa[]=array("txtCodigo"=>$txtCodigo,"precio"=>$precio,"cantidad"=>$cantidad,"descripcion"=>$descripcio,"proveedor"=>$Proveedor,"costo"=>$Costo,"IdProveedor"=>$IdProveedo);	
		}
		$_SESSION['CarritoVenta'.$CarritoNewVenta->IdSession]=$carrito_orderventa;
		echo json_encode($_SESSION['CarritoVenta'.$CarritoNewVenta->IdSession]);
	}

}