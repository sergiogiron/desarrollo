<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
date_default_timezone_set('America/Argentina/Jujuy');
class expedientes extends CI_Controller {
  function __construct(){
    parent::__construct();
    $this->load->model('seguridad_model');
    $this->load->model('expedientes_model');
    $this->load->model('bitacoras_model');
    //agreado joako
    $this->load->model("historial_expediente_model");
    $this->load->model('pases_model');
    //$this->load->helper('date');
    // ----------exportar------------//
    $this->load->helper('mysql_to_excel_helper');
    
  }

  
  public function index(){
          //Bitácora
          $RegistrarBitacora = array(
              'fecha'           => date('Y-m-d H:i:s'),
              'usuario'         => $this->session->userdata('ID'),
              'mensaje'         => "Ingresó a Expedientes" );
             
          $this->bitacoras_model->AgregarBitacoras($RegistrarBitacora);

          $url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
          $this->seguridad_model->SessionActivo($url);
          $this->load->view('constant');    //Carga vista títulos de Inicio
          $this->load->view('view_header'); //Carga vista del Menú Horizontal
          $data['exp'] = $this->expedientes_model->listarExp();
          //joako
           $result = $this->db->get('historial_expediente');
            $datax = array('consulta' => $result);
          //$datax['hist'] = $this->expedientes_model->listarExpH();
          $this->load->view('expedientes/view_expedientes', $data, $datax);
          $this->load->view('view_footer'); //Carga vista Pie de Página  
     }

   public function nuevo(){
          //Bitácora
          $RegistrarBitacora = array(
              'fecha'           => date('Y-m-d H:i:s'),
              'usuario'         => $this->session->userdata('ID'),
              'mensaje'         => "Ingresó a Nuevo Expediente" );
             
          $this->bitacoras_model->AgregarBitacoras($RegistrarBitacora);

          $url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
          $this->seguridad_model->SessionActivo($url);
          $this->load->view('constant'); //Carga vista títulos de Inicio
          $this->load->view('view_header'); //Carga vista del Menú Horizontal
          $data['titulo'] = "Nuevo Expediente";
          $data["max"] =  $this->expedientes_model->MaxExp();
          $this->load->view('expedientes/view_nuevo_expedientes', $data);
          $this->load->view('view_footer');

          //agregado joako
          echo json_encode($data);
          //$this->output->set_output(json_encode($data));
     }
    
     public function editarExpediente($id){

          $url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
          $this->seguridad_model->SessionActivo($url);
          $id =  $id; //Encripta el id
           //Bitácora
          $RegistrarBitacora = array(
              'fecha'           => date('Y-m-d H:i:s'),
              'usuario'         => $this->session->userdata('ID'),
              'mensaje'         => "Editó Expediente con id : $id" );
          $this->bitacoras_model->AgregarBitacoras($RegistrarBitacora);

          $this->load->view('constant');      //Carga vista titulos de Inicio 
          $this->load->view('view_header');   //Carga vista del Menú Principal
          $data['exp'] = $this->expedientes_model->buscarExp($id);
          $data['titulo'] = "Editar Expediente";
          $this->load->view('expedientes/view_nuevo_expedientes', $data);
          $this->load->view('view_footer');   //Carga vista Pie de Página
            
     }

     public function saveExpediente(){
   
          $url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
          $this->seguridad_model->SessionActivo($url);
          $Exp = json_decode($this->input->post('ExpPost'));
          $Pase = json_decode($this->input->post('PasePost'));
          $response = array (
                    "estatus"   => false,
                    "campo"     => "",
                    "error_msg" => ""
          );
          /*
        if($Exp->Fecha==""){
               $response["campo"]       = "fecha"; //Campo del formulario
               $response["error_msg"]   = "<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Debe ingresar Fecha</div>";
               echo json_encode($response);
           */    
        if($Exp->Dependencia=="0"){  
               $response["campo"]       = "dependencia"; //Campo del formulario
               $response["error_msg"]   = "<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Debe seleccionar una Dependencia</div>";
               echo json_encode($response); //Retorna la representación JSON del valor dado
               
        }else if($Exp->Remitente==""){  
               $response["campo"]       = "remitente"; //Campo del formulario
               $response["error_msg"]   = "<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Debe ingresar Remitente</div>";
               echo json_encode($response); //Retorna la representación JSON del valor dado
              
        }else if($Exp->Asunto==""){  
                $response["campo"]      ="asunto";
                $response["error_msg"]  ="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Debe ingresar Asunto</div>";
                echo json_encode($response); //Retorna la representación JSON del valor dado
        
        }else if($Exp->Area=="0"){  
               $response["campo"]       = "area"; //Campo del formulario
               $response["error_msg"]   = "<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Debe seleccionar un Área</div>";
               echo json_encode($response);
               
        }else if($Exp->Estado=="0"){   //Si no selecciona una opc, muestra un alerta de campo obligatorio.
                $response["campo"]      ="estado";
                $response["error_msg"]  ="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Debe seleccionar un Estado</div>";
                echo json_encode($response); //Retorna la representación JSON del valor dado
           
          //CAMPO OBSERVACIONES NO CONSIDERADO COMO OBLIGATORIO...

        }else if($Exp->Id==""){ //Si el id no existe, se REGISTRA un nuevo Expediente
               
          $RegistraExp  = array(
             'codigo_exp'        => $Exp->Codigo,
             //'anio'              => date('Y'),
             'fecha_exp'         => date("Y/m/d"),
             'id_dep'            => $Exp->Dependencia,
             'remitente_exp'     => $Exp->Remitente,
             'asunto_exp'        => $Exp->Asunto,
             'id_area'           => $Exp->Area,
             'estado_exp'        => $Exp->Estado,
             'obs_exp'           => $Exp->Observaciones,
              );

          ///probando
          $codigo_exp=$Exp->Codigo;
          $fecha_exp= date("Y/m/d");
          $id_dep= $Exp->Dependencia;
          $remitente_exp= $Exp->Remitente;
          $asunto_exp=$Exp->Asunto;
          $id_area=$Exp->Area;
          $estado_exp=$Exp->Estado;
          $obs_exp=$Exp->Observaciones;

          //Bitacora
          $CADENA=print_r($RegistraExp,  true);
          $RegistrarBitacora  = array(
                'fecha'       => date('Y-m-d H:i:s'),
                'usuario'     => $this->session->userdata('ID'),
                'mensaje'     => "Alta de Expediente: $CADENA");  
          $this->bitacoras_model->AgregarBitacoras($RegistrarBitacora);
          $this->expedientes_model->SaveExp($RegistraExp);

          //agregar aca el historial de expediente
          $RegistraExpH="Nº: ".$codigo_exp." Remitente: ".$remitente_exp." Asunto: ".$asunto_exp." Observaciones: ".$obs_exp;
          $CADENA_H=print_r($RegistraExpH,  true);
          $RegistrarHistorial  = array(
                'fecha'       => date('Y-m-d H:i:s'),
                'usuario'     => $this->session->userdata('ID'),
                'mensaje'     => "Alta de Expediente $CADENA_H",
                'codigo_exp'  => $codigo_exp,
                'id_dep'      => $id_dep,
                'id_area'     => $id_area);
          $this->historial_expediente_model->AgregarBitacoras($RegistrarHistorial);
          //$this->expedientes_model->SaveExp($RegistraExp);


          //Al generar un nuevo exp, paralelamente se crea un nuevo pase
          $RegistraPase  = array(
             'codigo_exp'        => $Exp->Codigo,
             //'anio'              => date('Y'),
             //'remitente_exp'     => $Pase->Remitente,
             'fecha_pase'        => date('Y/m/d'),
             'id_area'           => $Exp->Area,
             'estado_pase'       => $Exp->Estado,
             'obs_pase'          => $Exp->Observaciones,
              );
          
          //Bitacora
          $CADENA=print_r($RegistraPase,true);
          $RegistrarBitacora  = array(
                'fecha'       => date('Y-m-d H:i:s'),
                'usuario'     => $this->session->userdata('ID'),
                'mensaje'     => "Alta de Pase: $CADENA");  
          $this->bitacoras_model->AgregarBitacoras($RegistrarBitacora);
          $this->pases_model->SavePase($RegistraPase);

          $response["error_msg"]   = "<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Informacion Guardada Correctamente</div>";
          echo json_encode($response); //Retorna la representación JSON del valor dado
              }
              else if($Exp->Id!=""){ //Si el id ya existe, se realiza la ACTUALIZACION de un expediente
                    $UpdateExp    = array(
                       'codigo_exp'        => $Exp->Codigo,
                       //'anio'              => date('Y'),
                       'fecha_exp'         => date('Y/m/d'),
                       'id_dep'            => $Exp->Dependencia,
                       'remitente_exp'     => $Exp->Remitente,
                       'asunto_exp'        => $Exp->Asunto,
                       'id_area'           => $Exp->Area,
                       'estado_exp'        => $Exp->Estado,
                       'obs_exp'           => $Exp->Observaciones,
                      );
                    //Bitácora
                    $CADENA=print_r($UpdateExp,true);
                    $RegistrarBitacora     = array(
                        'fecha'            => date('Y-m-d H:i:s'),
                        'usuario'          => $this->session->userdata('ID'),
                        'mensaje'          => "Editó Expediente: $CADENA"); 
                    $this->bitacoras_model->AgregarBitacoras($RegistrarBitacora);
                    //$this->expedientes_model->updateExp($UpdateExp, $Exp->Codigo);
                    $this->expedientes_model->updateExp($UpdateExp, $Exp->Id);
                    ///probando
                    $codigo_exp=$Exp->Codigo;
                    $fecha_exp= date("Y/m/d");
                    $id_dep= $Exp->Dependencia;
                    $remitente_exp= $Exp->Remitente;
                    $asunto_exp=$Exp->Asunto;
                    $id_area=$Exp->Area;
                    $estado_exp=$Exp->Estado;
                    $obs_exp=$Exp->Observaciones;

                    //agregar aca otra ves el historial pero de ACTUALIZACION
                    //$RegistraExpH_A="Nº: ".$codigo_exp." Fecha: ".$fecha_exp." Remitente: ".$remitente_exp." Asunto: ".$asunto_exp." Observaciones: ".$obs_exp;
                    $RegistraExpH_A="Nº: ".$codigo_exp." Remitente: ".$remitente_exp." Asunto: ".$asunto_exp." Observaciones: ".$obs_exp;
                    $CADENA_H=print_r($RegistraExpH_A,  true);
                    $RegistrarHistorial  = array(
                          'fecha'       => date('Y-m-d H:i:s'),
                          'usuario'     => $this->session->userdata('ID'),
                          'mensaje'     => "Edicion de Expediente $CADENA_H",
                          'codigo_exp'  => $codigo_exp,
                          'id_dep'      => $id_dep,
                          'id_area'     => $id_area);
                    $this->historial_expediente_model->AgregarBitacoras($RegistrarHistorial);

                    
                    $response["error_msg"]   = "<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button> Informacion Actualizada Correctamente</div>";
                    echo json_encode($response); //Retorna la representación JSON del valor dado
               }
          
     
   }


   //Obtiene registros almacenados en Dependencias...
   public function dependencias(){
    $local = $this->expedientes_model->traerDependencias();
    echo json_encode($local);
  }

  //Obtiene registros almacenados en Areas...
  public function areas(){
    $local = $this->expedientes_model->traerAreas();
    echo json_encode($local);
  }

  //obtener los id de expediente (validacion de no repetidos)
  public function id_expediente(){
    $text = $this->input->post('texto');
    $local = $this->expedientes_model->traerId($text);
    echo json_encode($local);
  }

  public function listarPases($id){ 
      $url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
      $data['titulo']    ="Pases";
      $this->load->view('constant');
      $this->load->view('view_header');
      $id = $id;
      $this->seguridad_model->SessionActivo($url);
      $data['pase'] = $this->expedientes_model->listaPases($id);
      $this->load->view('pases/view_pases', $data);
      $this->load->view('view_footer');
    
     } 
      public function deleteExpediente(){
        
         $url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
         $this->seguridad_model->SessionActivo($url);
         $Exp = json_decode($this->input->post('ExpPost'));
         $id        = $Exp->Id;
         
         $response = array ( /*Array de response*/
            "estatus"   => false,
            "error_msg" => ""
         );
         //Bitácora
         $RegistrarBitacora = array(
            'fecha'           => date('Y-m-d H:i:s'),
            'usuario'         => $this->session->userdata('ID'),
            'mensaje'         => "Se Borró Expediente con id: $id" );
         $this->bitacoras_model->AgregarBitacoras($RegistrarBitacora);

        //registro de historial
         //$RegistraExpH_A="Nº: ".$codigo_exp." Fecha: ".$fecha_exp." Remitente: ".$remitente_exp." Asunto: ".$asunto_exp." Observaciones: ".$obs_exp;
                    //$CADENA_H=print_r($RegistraExpH_A,  true);

         //$codigo_exp=$Exp->Codigo;//tomo el campo codigo expediente para bitacora de historial
                    $RegistrarHistorial  = array(
                          'fecha'       => date('Y-m-d H:i:s'),
                          'usuario'     => $this->session->userdata('ID'),
                          'mensaje'     => "Se Borró Expediente con id: $id" );
                          //'codigo_exp'  => $codigo_exp); //verificar si se esta tomando este camp en la bitacora de exp
                          /*'id_dep'      => $id_dep,
                          'id_area'     => $id_area);*/
                    $this->historial_expediente_model->AgregarBitacoras($RegistrarHistorial);

         $this->expedientes_model->eliminarExp($id); 
         $response["error_msg"]   = "<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Expediente eliminado correctamente Clave: <strong>".$id."</strong>, La Información de Actualizara en 5 Segundos <meta http-equiv='refresh' content='5'></div>";
         echo json_encode($response); //Retorna la representación JSON del valor dado     
        
       }

      //-------exportar----------//
        public function exportar(){
         //Bitácora
        $RegistrarBitacora = array(
              'fecha'           => date('Y-m-d H:i:s'),
              'usuario'         => $this->session->userdata('ID'),
              'mensaje'         => "Exportó Datos de Expediente en formato Excel" );  

        $this->bitacoras_model->AgregarBitacoras($RegistrarBitacora);
            //-//
        $this->load->helper('mysql_to_excel_helper');
        $this->load->model('expedientes_model');
        to_excel($this->expedientes_model->get(), "ExpedientesExcel");
        }
 
     //========================imagien----------------------------
      public function view_img($id){
      $id          = $id; 
        //Bitácora
      $RegistrarBitacora   = array(
          'fecha'          => date('Y-m-d H:i:s'),
          'usuario'        => $this->session->userdata('ID'),
          'mensaje'        => "Ingresó a Cargar Imagen de Expediente Codigo : $id");  
      $this->bitacoras_model->AgregarBitacoras($RegistrarBitacora);

      //historial de expediente (duda si dejar o quitar eso)
      $RegistrarHistorial  = array(
          'fecha'       => date('Y-m-d H:i:s'),
          'usuario'     => $this->session->userdata('ID'),
          'mensaje'     => "Se Borró Expediente con id: $id");
          /*'codigo_exp'  => $codigo_exp,
          'id_dep'      => $id_dep,
          'id_area'     => $id_area);*/
      $this->historial_expediente_model->AgregarBitacoras($RegistrarHistorial);

                       //-//
      $data["id"]        = $id;
      $this->load->view('constant');
      $this->load->view('view_header');
      $this->load->view('expedientes/view_img',$data);
      $this->load->view('view_footer');
      
      }

      public function SubeImg(){
        $img1= "";
        $str = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
            for($i=0;$i<5;$i++) {
                $img1 .= substr($str,rand(0,62),1);
            }
        $idimg     = $this->input->post('id');
        $nombreimg = $idimg."_".$img1;
        $config['upload_path'] = realpath(APPPATH."../fotos");
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size'] = '1048576';
        $config['max_width'] = '9000';
        $config['max_height'] = '9000';
        $config['file_name']= $idimg."_".$img1;
        $this->load->library('upload', $config);
        if (!$this->upload->do_upload('file')) {
          echo  $this->upload->display_errors();
        }else{
          $file_info = $this->upload->data();
          $data      = array('upload_data'=>$this->upload->data());
          echo "Imagen Subida Correctamente.";
          $img = array("foto_exp"=>$nombreimg.$file_info["file_ext"]);
        //  $this->socios_model->GuardaImg($img);
           $this->expedientes_model->updateExp($img, $idimg);
        }
        }

        //Muestra Expediente
        public  function view_mostrar($id) {
        
        $id = $id;
        //Bitácora
        $RegistrarBitacora = array(
            'fecha'        => date('Y-m-d H:i:s'),
            'usuario'      => $this->session->userdata('ID'),
            'mensaje'     => "Muestra   Expediente  id : $id"); 
        $this->bitacoras_model->AgregarBitacoras($RegistrarBitacora);
                         //-//
        $data = array(
                'title' => 'Expediente',
                'imgexp' => $this->expedientes_model->buscarExp($id),
                'consulta' => $this->expedientes_model->buscarExpH($id)
                //'historial_expediente' => $this->expedientes_model->buscarExpH($id),
                );
                  
          //  $this->load->view('socios/ventanacodigodebarras_view', $datos);
            /*$historial['hist'] = $this->expedientes_model->listarExpH($id);
            $datax = array('historial'  => $historial);*/

            $result = $this->db->get('historial_expediente');
            //$datax = array('consulta' => $result);

            $this->load->view('expedientes/view_mostrar', $data);
          }

  /*public function traerHistorial($id){
    $local = $this->expedientes_model->buscarExpH($id);
    echo json_encode($local);
  }*/
  
 
}