<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
date_default_timezone_set('America/Argentina/Jujuy');
class programas extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('seguridad_model');
		$this->load->model('programas_model');
		$this->load->model('bitacoras_model');
	}
	public function index(){
          //Bitácora
        $RegistrarBitacora = array(
            'fecha'           => date('Y-m-d H:i:s'),
            'usuario'         => $this->session->userdata('ID'),
            'mensaje'         => "Ingresó a Programas" );
           
        $this->bitacoras_model->AgregarBitacoras($RegistrarBitacora);

          $url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
          $this->seguridad_model->SessionActivo($url);
          $this->load->view('constant');    //Carga vista títulos de Inicio
          $this->load->view('view_header'); //Carga vista del Menú Horizontal
          $data['programas'] = $this->programas_model->listarPrograma(); //Obtengo todos los programas cargados a la BD
          $this->load->view('programas/view_programas', $data); //Carga vista con Registros de todos los Programas obtenidos
          $this->load->view('view_footer'); //Carga vista Pie de Página  
	   }

     /**
      * Crea un nuevo Programa
      * @return void
      */
  
     public function nuevo(){
        //Bitácora
        $RegistrarBitacora = array(
            'fecha'           => date('Y-m-d H:i:s'),
            'usuario'         => $this->session->userdata('ID'),
            'mensaje'         => "Creó un nuevo Programa" );
           
        $this->bitacoras_model->AgregarBitacoras($RegistrarBitacora);

          $url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
          $this->seguridad_model->SessionActivo($url);
          $this->load->view('constant'); //Carga vista títulos de Inicio
          $this->load->view('view_header'); //Carga vista del Menú Horizontal
          $data['titulo'] = "Nuevo Programa";
          $this->load->view('programas/view_nuevo_programas', $data); //Carga vista para registrar un Nuevo Programa
          $this->load->view('view_footer'); //Carga vista Pie de Pagina
     }
     /**
      * Modifica un Programa
      * @param int $id  identificador del Programa a editar 
      * @return void
      */
     public function editarPrograma($id){
          //Bitácora
        $RegistrarBitacora = array(
            'fecha'           => date('Y-m-d H:i:s'),
            'usuario'         => $this->session->userdata('ID'),
            'mensaje'         => "Editó Programas: $id" );
           
          $this->bitacoras_model->AgregarBitacoras($RegistrarBitacora);

          $url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
          $this->seguridad_model->SessionActivo($url);
          $id =  base64_decode($id); //Encripta el id 
          $this->load->view('constant'); //Carga vista titulos de Inicio 
          $this->load->view('view_header'); //Carga vista del Menú Principal
          $data['programa'] = $this->programas_model->buscarPrograma($id); //Llama a la función que realiza la búsqueda
          $data['titulo'] = "Editar Programa";
          $this->load->view('programas/view_nuevo_programas', $data); //Carga la vista para dar de alta un nuevo programa.
          $this->load->view('view_footer'); //Carga vista Pie de Página

          
     }
     /**
      * Permite Guardar un nuevo Programa
      * @return type
      */
     public function savePrograma(){
        //Bitácora
        $RegistrarBitacora = array(
            'fecha'           => date('Y-m-d H:i:s'),
            'usuario'         => $this->session->userdata('ID'),
            'mensaje'         => "Guardó Programas" );
           
            $this->bitacoras_model->AgregarBitacoras($RegistrarBitacora);
	 
	      $url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
          $this->seguridad_model->SessionActivo($url);
          $Pro         = json_decode($this->input->post('ProgramasPost')); //Obtiene los nuevos datos de un Programa con JSON
          $response = array (
                    "estatus"   => false,
                    "campo"     => "",
                    "error_msg" => ""
          );

		      if($Pro->Nombre==""){  //Si no ingresa un nombre, muestra un alerta de campo obligatorio.
               $response["campo"]     = "Nombre";
               $response["error_msg"]   = "<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Nombre es Obligatorio</div>";
               echo json_encode($response); //Retorna la representación JSON del valor dado
          }else if($Pro->Tipo=="0"){  //Si no selecciona un Tipo, muestra un alerta de campo obligatorio.
                $response["campo"]    ="Tipo";
                $response["error_msg"]  ="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Debe seleccionar un Tipo</div>";
                echo json_encode($response); //Retorna la representación JSON del valor dado
          }else if($Pro->Cantidad==""){   //Si no ingresa una cantidad, muestra un alerta de campo obligatorio.
                $response["campo"]  ="Tipo";
                $response["error_msg"]  ="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Cantidad es Obligatorio</div>";
                echo json_encode($response); //Retorna la representación JSON del valor dado
            }else if($Pro->Id==""){ //Si el id no existe, se REGISTRA un nuevo programa
                     $RegistraPrograma    = array(
                     'nombre_programa'     => $Pro->Nombre,
                     'tipo'                => $Pro->Tipo,
                     'cantidad'            => $Pro->Cantidad,
					 'restantes'           => $Pro->Cantidad,
                      );
                     $this->programas_model->savePrograma($RegistraPrograma);
                     $response["error_msg"]   = "<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Informacion Guardada Correctamente</div>";
                     echo json_encode($response); //Retorna la representación JSON del valor dado
               }

               if($Pro->Id!=""){ //Si el id ya existe, se realiza la ACTUALIZACION de un programa
                    $UpdatePrograma    = array(
                    'nombre_programa'     => $Pro->Nombre,
                    'tipo'                => $Pro->Tipo,
                    'cantidad'            => $Pro->Cantidad,
                      );
                    $this->programas_model->updatePrograma($UpdatePrograma, $Pro->Id);
                    $response["error_msg"]   = "<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button> Informacion Actualizada Correctamente</div>";
                    echo json_encode($response); //Retorna la representación JSON del valor dado
               }
          }
     
	   /**
      * Elimina un programa
      * @return type
      */
	   	public function deleteProgramas(){
	     
         $response = array ( /*Array de response*/
            "estatus"   => false,
            "error_msg" => ""
         );
         $url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
         $this->seguridad_model->SessionActivo($url);
		     $Prog	= json_decode($this->input->post('ProgramaPost')); //Obtiene los nuevos datos de un Programa con JSON
		     $id    = base64_decode($Prog->Id);
         
	       $this->programas_model->eliminarPrograma($id); //Llamada a la funcion que ejecuta la consulta de eliminación


  //Bitácora
         $RegistrarBitacora = array(
            'fecha'           => date('Y-m-d H:i:s'),
            'usuario'         => $this->session->userdata('ID'),
            'mensaje'         => "Se Borró Programa: $id" );
           
         $this->bitacoras_model->AgregarBitacoras($RegistrarBitacora);




         $response["error_msg"]   = "<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Programa Eliminado Correctamente Clave: <strong>".$id."</strong>, La Información de Actualizara en 5 Segundos <meta http-equiv='refresh' content='5'></div>";
         echo json_encode($response); //Retorna la representación JSON del valor dado     
		    
	     }




 
}