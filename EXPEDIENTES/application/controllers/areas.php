<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
date_default_timezone_set('America/Argentina/Jujuy');
class areas extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('seguridad_model');
		$this->load->model('areas_model');
		$this->load->model('bitacoras_model');
    //$this->load->helper('date');
	  // ----------exportar------------//
		$this->load->helper('mysql_to_excel_helper');
    $this->load->library('form_validation'); //Para validacion de campos
		
	  }

  
	public function index(){
          //Bitácora
          $RegistrarBitacora = array(
              'fecha'           => date('Y-m-d H:i:s'),
              'usuario'         => $this->session->userdata('ID'),
              'mensaje'         => "Ingresó a Áreas" );
             
          $this->bitacoras_model->AgregarBitacoras($RegistrarBitacora);

          $url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
          $this->seguridad_model->SessionActivo($url);
          $this->load->view('constant');    //Carga vista títulos de Inicio
          $this->load->view('view_header'); //Carga vista del Menú Horizontal
          $data['area'] = $this->areas_model->listarArea();
          $this->load->view('areas/view_areas', $data);
          $this->load->view('view_footer'); //Carga vista Pie de Página  
	   }

   public function nuevo(){
          //Bitácora
          $RegistrarBitacora = array(
              'fecha'           => date('Y-m-d H:i:s'),
              'usuario'         => $this->session->userdata('ID'),
              'mensaje'         => "Ingresó a Nueva Área" );
             
          $this->bitacoras_model->AgregarBitacoras($RegistrarBitacora);

          $url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
          $this->seguridad_model->SessionActivo($url);
          $this->load->view('constant'); //Carga vista títulos de Inicio
          $this->load->view('view_header'); //Carga vista del Menú Horizontal
          $data['titulo'] = "Nueva Área";
          $data["max"] =  $this->areas_model->maxCodigo();
          $this->load->view('areas/view_nuevo_areas', $data);
          $this->load->view('view_footer'); //Carga vista Pie de Pagina
     }
     
     public function editarArea($id){

          $url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
          $this->seguridad_model->SessionActivo($url);
          $id =  base64_decode($id); //Encripta el id
           //Bitácora
          $RegistrarBitacora = array(
              'fecha'           => date('Y-m-d H:i:s'),
              'usuario'         => $this->session->userdata('ID'),
              'mensaje'         => "Editó Área con id : $id" );
          $this->bitacoras_model->AgregarBitacoras($RegistrarBitacora);

          $this->load->view('constant');      //Carga vista titulos de Inicio 
          $this->load->view('view_header');   //Carga vista del Menú Principal
          $data['area'] = $this->areas_model->buscarArea($id);
          $data['titulo'] = "Editar Área";
          $this->load->view('areas/view_nuevo_areas', $data);
          $this->load->view('view_footer');   //Carga vista Pie de Página
            
     }

     public function saveArea(){
	 
	        $url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
          $this->seguridad_model->SessionActivo($url);
          $Area = json_decode($this->input->post('AreaPost'));
          $response = array (
                    "estatus"   => false,
                    "campo"     => "",
                    "error_msg" => ""  
          );
          
          /*
		   if($Nota->Codigo==""){  //Si no ingresa un codigo, muestra un alerta de campo obligatorio.
               $response["campo"]       = "codigo"; //Campo del formulario
               $response["error_msg"]   = "<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Debe ingresar Codigo</div>";
               echo json_encode($response); //Retorna la representación JSON del valor dado
        }else 
        
        */
  // $this->form_validation->set_rules('nombre', 'Nombre', 'required');


        if($Area->Nombre==""){
               $response["campo"]       = "nombre"; //Campo del formulario
               $response["error_msg"]   = "<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Debe ingresar Nombre del Área</div>";
               echo json_encode($response); //Retorna la representación JSON del valor dado
           
          //CAMPO OBSERVACIONES NO CONSIDERADO COMO OBLIGATORIO...


        } else if($Area->Id==""){ //Si el id no existe, se REGISTRA una nueva Área
               
          $RegistraArea  = array(
             'codigo_area'        => $Area->Codigo,
             'nombre_area'        => $Area->Nombre,
             'obs_area'           => $Area->Observaciones,
              );
      
          //Bitácora
          $CADENA=print_r($RegistraArea,true);
          $RegistrarBitacora  = array(
                'fecha'       => date('Y-m-d H:i:s'),
                'usuario'     => $this->session->userdata('ID'),
                'mensaje'     => "Alta de Área: $CADENA");  
          $this->bitacoras_model->AgregarBitacoras($RegistrarBitacora);
                   
          $this->areas_model->SaveArea($RegistraArea);
          $response["error_msg"]   = "<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Informacion Guardada Correctamente</div>";
          
          echo json_encode($response); //Retorna la representación JSON del valor dado
       


        //$this->form_validation->set_message("nombre", "El campo Nombre es requerido");
        
        //if ($this->form_validation->run() == TRUE) { //sicumple con la condicion

          }else if($Area->Id!=""){ //Si el id ya existe, se realiza la ACTUALIZACION de un Área
                    $UpdateArea    = array(
                    'codigo_area'        => $Area->Codigo,
                    'nombre_area'        => $Area->Nombre,
                    'obs_area'           => $Area->Observaciones,
                      );
                    //Bitácora
                    $CADENA=print_r($UpdateArea,true);
                    $RegistrarBitacora     = array(
                        'fecha'            => date('Y-m-d H:i:s'),
                        'usuario'          => $this->session->userdata('ID'),
                        'mensaje'          => "Editó Área: $CADENA"); 
                    $this->bitacoras_model->AgregarBitacoras($RegistrarBitacora);

                    $this->areas_model->updateArea($UpdateArea, $Area->Id);
                    $response["error_msg"]   = "<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button> Informacion Actualizada Correctamente</div>";
                    echo json_encode($response); //Retorna la representación JSON del valor dado
               }
/*
            if($this->areas_model->SaveArea($RegistraArea)==TRUE)
   
                echo "Registro Guardado";
            else
                echo "No se pudo guardar los datos";
      }
      else{
          echo validation_errors();
      
      }
      */
    

/*
        
          */
              
          
     
   }
   
	   	public function deleteArea(){
	      
         $url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
         $this->seguridad_model->SessionActivo($url);
		     $Area	= json_decode($this->input->post('AreaPost'));
		     $id        = base64_decode($Area->Id);
         
         $response = array ( /*Array de response*/
            "estatus"   => false,
            "error_msg" => ""
         );
         //Bitácora
         $RegistrarBitacora = array(
            'fecha'           => date('Y-m-d H:i:s'),
            'usuario'         => $this->session->userdata('ID'),
            'mensaje'         => "Se Borró Nota con id: $id" );
	       $this->bitacoras_model->AgregarBitacoras($RegistrarBitacora);

         $this->areas_model->eliminarArea($id); 
         $response["error_msg"]   = "<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Nota eliminada correctamente Clave: <strong>".$id."</strong>, La Información de Actualizara en 5 Segundos <meta http-equiv='refresh' content='5'></div>";
         echo json_encode($response); //Retorna la representación JSON del valor dado     
		    
	     }

      //-------exportar----------//
      	public function exportar(){
      	 //Bitácora
      	$RegistrarBitacora = array(
      		    'fecha'             => date('Y-m-d H:i:s'),
      				'usuario'     		=> $this->session->userdata('ID'),
      				'mensaje'	    	=> "Exportó Datos de Nota en formato Excel" );	

        $this->bitacoras_model->AgregarBitacoras($RegistrarBitacora);
      			//-//
      	$this->load->helper('mysql_to_excel_helper');
        $this->load->model('areas_model');
      	to_excel($this->areas_model->get(), "NotasExcel");
      	}

 
}