<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
date_default_timezone_set('America/Argentina/Jujuy');
class inscripciones extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('seguridad_model');
		$this->load->model('inscripciones_model');
		$this->load->model('bitacoras_model');
	}
  
	public function index(){
	      //Bitácora
		   $RegistrarBitacora = array(
		        'fecha'           => date('Y-m-d H:i:s'),
						'usuario'     		=> $this->session->userdata('ID'),
						'mensaje'	    	  => "Ingresó a Inscripciones" );
				   
			 $this->bitacoras_model->AgregarBitacoras($RegistrarBitacora);
	
       $url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
       $this->seguridad_model->SessionActivo($url);
        
       $this->load->view('constant');
       $this->load->view('view_header');
       $data['inscripciones'] = $this->inscripciones_model->ListarInscripciones();
       $this->load->view('inscripciones/view_inscripciones', $data);
       $this->load->view('view_footer');
          
	}
  public function localidades(){
    $local = $this->inscripciones_model->Localidades();
    echo json_encode($local);
  }

	public function deleteInscripciones(){
	
      $url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
      $this->seguridad_model->SessionActivo($url);
		  $Beneficiario		= json_decode($this->input->post('InscripcionesPost'));
		  $id             = base64_decode($Beneficiario->Id);
	    /*Array de response*/
         $response = array (
            "estatus"   => false,
            "error_msg" => ""
          );
	     //Bitácora
		  $RegistrarBitacora = array(
		        'fecha'           => date('Y-m-d H:i:s'),
						'usuario'     		=> $this->session->userdata('ID'),
						'mensaje'	    	  => "Se Borro Inscripcion con  id : $id" );
			$this->bitacoras_model->AgregarBitacoras($RegistrarBitacora);
    	
		 $this->inscripciones_model->eliminarInscripcion($id);
		 $response["error_msg"]   = "<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Inscripcion Eliminada Correctamente Clave: <strong>".$id."</strong>, La Información de Actualizara en 5 Segundos <meta http-equiv='refresh' content='5'></div>";
		 echo json_encode($response);

	}	

  public function nuevo(){
      //Bitácora
          $RegistrarBitacora = array(
              'fecha'           => date('Y-m-d H:i:s'),
              'usuario'         => $this->session->userdata('ID'),
              'mensaje'         => "Ingresó a nueva Inscripcion" );
             
          $this->bitacoras_model->AgregarBitacoras($RegistrarBitacora);

      $url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
      $this->seguridad_model->SessionActivo($url);
      $this->load->view('constant');
      $this->load->view('view_header');
      $data['titulo'] = "Nueva Inscripcion";
      $this->load->view('inscripciones/view_nuevo_inscripciones', $data);
      $this->load->view('view_footer');
  }
  public function editarInscripciones($id){
      $url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
      $this->seguridad_model->SessionActivo($url);
      $id =  base64_decode($id);
      //Bitácora
          $RegistrarBitacora = array(
              'fecha'           => date('Y-m-d H:i:s'),
              'usuario'         => $this->session->userdata('ID'),
              'mensaje'         => "Editó Inscripcion con id : $id" );
          $this->bitacoras_model->AgregarBitacoras($RegistrarBitacora);

      $this->load->view('constant');
      $this->load->view('view_header');
      $data['insc'] = $this->inscripciones_model->BuscaInscripciones($id);
      $data['titulo'] = "Editar Inscripcion";
      $this->load->view('inscripciones/view_nuevo_inscripciones', $data);
      $this->load->view('view_footer');
  }


  public function saveInscripciones(){
      $url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
      $this->seguridad_model->SessionActivo($url);
      $Beneficiario   = json_decode($this->input->post('InscripcionesPost'));
      $response = array (
              "estatus"   => false,
              "campo"     => "",
              "error_msg" => "",
         );
         if($Beneficiario->Documento==""){
               $response["campo"]       = "Documento";
               $response["error_msg"]   = "<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>El documento  es Obligatorio</div>";
               echo json_encode($response);
           }else if($Beneficiario->Nombre==""){
               $response["campo"]       = "Nombre";
               $response["error_msg"]   = "<div class='alert alert-danger text-center' alert-dismissable><button type='button' class='close' data-dismiss='alert'>&times;</button>El nombre es obligatorio</div>";
               echo json_encode($response);
           }else if($Beneficiario->Localidad=="0"){
               $response["campo"]       = "Localidad";
               $response["error_msg"]   = "<div class='alert alert-danger text-center' alert-dismissable><button type='button' class='close' data-dismiss='alert'>&times;</button>La Localidad es obligatorio</div>";
               echo json_encode($response);
           }else if($Beneficiario->Barrio==""){
               $response["campo"]       = "Barrio";
               $response["error_msg"]   = "<div class='alert alert-danger text-center' alert-dismissable><button type='button' class='close' data-dismiss='alert'>&times;</button>El Barrio es Obligatorio</div>";
               echo json_encode($response);
	 	       }else if($Beneficiario->Mza==""){
               $response["campo"]       = "Mza";
               $response["error_msg"]   = "<div class='alert alert-danger text-center' alert-dismissable><button type='button' class='close' data-dismiss='alert'>&times;</button>Dato del Domicilio es Obligatorio</div>";
               echo json_encode($response); 
	 	       }else if($Beneficiario->Lote==""){
               $response["campo"]       = "Lote";
               $response["error_msg"]   = "<div class='alert alert-danger text-center' alert-dismissable><button type='button' class='close' data-dismiss='alert'>&times;</button>Dato del domicilio es Obligatorio</div>";
               echo json_encode($response);   
         }
			   /*else if($Beneficiario->Estado=="0"){
               $response["campo"]       = "Estado";
               $response["error_msg"]   = "<div class='alert alert-danger text-center' alert-dismissable><button type='button' class='close' data-dismiss='alert'>&times;</button>Estado es obligatorio</div>";
               echo json_encode($response);  
          }*/
          else{
               if($Beneficiario->Id==""){
                     $RegistraInscripcion   = array(
                     'documento'            => $Beneficiario->Documento,
                     'nombre_beneficiario'  => $Beneficiario->Nombre,
                     'id_localidad'         => $Beneficiario->Localidad,
                     'barrio'               => $Beneficiario->Barrio,
				 	           'mza'                  => $Beneficiario->Mza,
                     'lote'                 => $Beneficiario->Lote,
                     'estado'               => $Beneficiario->Estado,
					           'obs'                  => $Beneficiario->Obs  
                     );
      					  //Bitácora
      						  $CADENA=print_r($RegistraInscripcion,true);
      		          $RegistrarBitacora = array(
      		                'fecha'           => date('Y-m-d H:i:s'),
      						        'usuario'     		=> $this->session->userdata('ID'),
      						        'mensaje'	    	  => "Alta Inscripcion: $CADENA");	

                    $this->bitacoras_model    ->AgregarBitacoras($RegistrarBitacora);
                    $this->inscripciones_model->saveInscripcion($RegistraInscripcion);

                    $response["error_msg"]   = "<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Informacion Guardada Correctamente</div>";
      	
                    echo json_encode($response);

                     } if($Beneficiario->Id!=""){
                          $UpdateInscripcion     = array(
                          'documento'            => $Beneficiario->Documento,
                          'nombre_beneficiario'  => $Beneficiario->Nombre,
                          'id_localidad'         => $Beneficiario->Localidad,
                          'barrio'               => $Beneficiario->Barrio,
    					            'mza'                  => $Beneficiario->Mza,
                          'lote'                 => $Beneficiario->Lote,
                          'estado'               => $Beneficiario->Estado,
    					            'obs'                  => $Beneficiario->Obs 
                        );
                         //Bitácora
						          $CADENA=print_r($UpdateInscripcion,true);
		                  $RegistrarBitacora      = array(
		                      'fecha'             => date('Y-m-d H:i:s'),
						              'usuario'     		  => $this->session->userdata('ID'),
						              'mensaje'	    	    => "Modificación de Inscripcion: $CADENA");	

                      $this->bitacoras_model->AgregarBitacoras($RegistrarBitacora);
                      $this->inscripciones_model->updateInscripciones($UpdateInscripcion, $Beneficiario->Id);
                      $response["error_msg"]   = "<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button> Informacion Actualizada Correctamente</div>";
                      echo json_encode($response);
               }
          }
     }
}