<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
date_default_timezone_set('America/Argentina/Jujuy');
class notas extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('seguridad_model');
		$this->load->model('notas_model');
		$this->load->model('bitacoras_model');
	  // ----------exportar------------//
		$this->load->helper('mysql_to_excel_helper');
		
	}

  
	public function index(){
          //Bitácora
          $RegistrarBitacora = array(
              'fecha'           => date('Y-m-d H:i:s'),
              'usuario'         => $this->session->userdata('ID'),
              'mensaje'         => "Ingresó a Notas" );
             
          $this->bitacoras_model->AgregarBitacoras($RegistrarBitacora);

          $url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
          $this->seguridad_model->SessionActivo($url);
          $this->load->view('constant');    //Carga vista títulos de Inicio
          $this->load->view('view_header'); //Carga vista del Menú Horizontal
          $data['nota'] = $this->notas_model->listarNota();
          $this->load->view('notas/view_notas', $data);
          $this->load->view('view_footer'); //Carga vista Pie de Página  
	   }

   public function nuevo(){
          //Bitácora
          $RegistrarBitacora = array(
              'fecha'           => date('Y-m-d H:i:s'),
              'usuario'         => $this->session->userdata('ID'),
              'mensaje'         => "Ingresó a nueva Nota" );
             
          $this->bitacoras_model->AgregarBitacoras($RegistrarBitacora);

          $url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
          $this->seguridad_model->SessionActivo($url);
          $this->load->view('constant'); //Carga vista títulos de Inicio
          $this->load->view('view_header'); //Carga vista del Menú Horizontal
          $data['titulo'] = "Nueva Nota";
          $this->load->view('notas/view_nuevo_notas', $data);
          $this->load->view('view_footer'); //Carga vista Pie de Pagina
     }
     
     public function editarNota($id){

          $url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
          $this->seguridad_model->SessionActivo($url);
          $id =  base64_decode($id); //Encripta el id
           //Bitácora
          $RegistrarBitacora = array(
              'fecha'           => date('Y-m-d H:i:s'),
              'usuario'         => $this->session->userdata('ID'),
              'mensaje'         => "Editó Nota con id : $id" );
          $this->bitacoras_model->AgregarBitacoras($RegistrarBitacora);

          $this->load->view('constant');      //Carga vista titulos de Inicio 
          $this->load->view('view_header');   //Carga vista del Menú Principal
          $data['nota'] = $this->notas_model->buscarNota($id);
          $data['titulo'] = "Editar Nota";
          $this->load->view('notas/view_nuevo_notas', $data);
          $this->load->view('view_footer');   //Carga vista Pie de Página
            
     }

     public function saveNota(){
	 
	        $url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
          $this->seguridad_model->SessionActivo($url);
          $Nota = json_decode($this->input->post('NotaPost')); //Obtiene los nuevos datos de una resolucion con JSON
          $response = array (
                    "estatus"   => false,
                    "campo"     => "",
                    "error_msg" => ""
          );
		   if($Nota->Codigo==""){  //Si no ingresa un codigo, muestra un alerta de campo obligatorio.
               $response["campo"]       = "codigo"; //Campo del formulario
               $response["error_msg"]   = "<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Debe ingresar Codigo</div>";
               echo json_encode($response); //Retorna la representación JSON del valor dado
        }else if($Nota->Año==""){  
               $response["campo"]       = "anio"; //Campo del formulario
               $response["error_msg"]   = "<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Debe ingresar el Año</div>";
               echo json_encode($response); //Retorna la representación JSON del valor dado
        }else if($Nota->Fecha==""){
               $response["campo"]       = "fecha"; //Campo del formulario
               $response["error_msg"]   = "<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Debe ingresar Fecha</div>";
               echo json_encode($response); //Retorna la representación JSON del valor dado
        }else if($Nota->Remitente==""){  
               $response["campo"]       = "remitente"; //Campo del formulario
               $response["error_msg"]   = "<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Debe ingresar Remitente</div>";
               echo json_encode($response); //Retorna la representación JSON del valor dado
        }else if($Nota->Celular==""){
               $response["campo"]       = "celular"; //Campo del formulario
               $response["error_msg"]   = "<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Debe ingresar Nro. de Celular</div>";
               echo json_encode($response); //Retorna la representación JSON del valor dado
		    }else if($Nota->Motivo==""){  
                $response["campo"]      ="motivo";
                $response["error_msg"]  ="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Debe ingresar Motivo</div>";
                echo json_encode($response); //Retorna la representación JSON del valor dado
        }else if($Nota->Tipo=="0"){  
               $response["campo"]       = "tipo"; //Campo del formulario
               $response["error_msg"]   = "<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Debe seleccionar un Tipo</div>";
               echo json_encode($response); //Retorna la representación JSON del valor dado
        }else if($Nota->Estado=="0"){   //Si no selecciona una opc, muestra un alerta de campo obligatorio.
                $response["campo"]      ="estado";
                $response["error_msg"]  ="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Debe seleccionar un Estado</div>";
                echo json_encode($response); //Retorna la representación JSON del valor dado
           
          //CAMPO OBSERVACIONES NO CONSIDERADO COMO OBLIGATORIO...

        }else if($Nota->Id==""){ //Si el id no existe, se REGISTRA una nueva nota
               
					$RegistraNota  = array(
					   'codigo'             => $Nota->Codigo,
             'anio'                => $Nota->Año,
             'fecha_nota'         => $Nota->Fecha,
             'remitente'     => $Nota->Remitente,
             'celular'       => $Nota->Celular,
             'motivo'        => $Nota->Motivo,
             'tipo'          => $Nota->Tipo,
             'estado'        => $Nota->Estado,
             'obs'     => $Nota->Observaciones,
              );
          //Bitácora
          $CADENA=print_r($RegistraNota,true);
          $RegistrarBitacora  = array(
                'fecha'       => date('Y-m-d H:i:s'),
                'usuario'     => $this->session->userdata('ID'),
                'mensaje'     => "Alta de Nota: $CADENA");  
          $this->bitacoras_model->AgregarBitacoras($RegistrarBitacora);
                    
          $this->notas_model->SaveNota($RegistraNota);
          $response["error_msg"]   = "<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Informacion Guardada Correctamente</div>";
          echo json_encode($response); //Retorna la representación JSON del valor dado
              }
              if($Nota->Id!=""){ //Si el id ya existe, se realiza la ACTUALIZACION de una nota
                    $UpdateNota    = array(
					          'codigo'              => $Nota->Codigo,
                    'anio'                => $Nota->Año,
                    'fecha_nota'          => $Nota->Fecha,
                     'remitente'     => $Nota->Remitente,
                     'celular'       => $Nota->Celular,
                     'motivo'        => $Nota->Motivo,
                     'tipo'          => $Nota->Tipo,
                     'estado'        => $Nota->Estado,
                     'obs'           => $Nota->Observaciones,
                      );
                    //Bitácora
                    $CADENA=print_r($UpdateNota,true);
                    $RegistrarBitacora     = array(
                        'fecha'            => date('Y-m-d H:i:s'),
                        'usuario'          => $this->session->userdata('ID'),
                        'mensaje'          => "Editó Nota: $CADENA"); 
                    $this->bitacoras_model->AgregarBitacoras($RegistrarBitacora);

                    $this->notas_model->updateNota($UpdateNota, $Nota->Id);
                    $response["error_msg"]   = "<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button> Informacion Actualizada Correctamente</div>";
                    echo json_encode($response); //Retorna la representación JSON del valor dado
               }
          
     
   }
   
	   	public function deleteNota(){
	      
         $url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
         $this->seguridad_model->SessionActivo($url);
		     $Nota	= json_decode($this->input->post('NotaPost'));
		     $id        = base64_decode($Nota->Id);
         
         $response = array ( /*Array de response*/
            "estatus"   => false,
            "error_msg" => ""
         );
         //Bitácora
         $RegistrarBitacora = array(
            'fecha'           => date('Y-m-d H:i:s'),
            'usuario'         => $this->session->userdata('ID'),
            'mensaje'         => "Se Borró Nota con id: $id" );
	       $this->bitacoras_model->AgregarBitacoras($RegistrarBitacora);

         $this->notas_model->eliminarNota($id); 
         $response["error_msg"]   = "<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Nota eliminada correctamente Clave: <strong>".$id."</strong>, La Información de Actualizara en 5 Segundos <meta http-equiv='refresh' content='5'></div>";
         echo json_encode($response); //Retorna la representación JSON del valor dado     
		    
	     }

      //-------exportar----------//
      	public function exportar(){
      	 //Bitácora
      	$RegistrarBitacora = array(
      		    'fecha'             => date('Y-m-d H:i:s'),
      				'usuario'     		=> $this->session->userdata('ID'),
      				'mensaje'	    	=> "Exportó Datos de Nota en formato Excel" );	

        $this->bitacoras_model->AgregarBitacoras($RegistrarBitacora);
      			//-//
      	$this->load->helper('mysql_to_excel_helper');
        $this->load->model('notas_model');
      	to_excel($this->notas_model->get(), "NotasExcel");
      	}
 
     //========================imagien----------------------------
    	public function view_img($id){
    	$id 			   = base64_decode($id); 
    		//Bitácora
    	$RegistrarBitacora   = array(
    		  'fecha'          => date('Y-m-d H:i:s'),
    		  'usuario'     	 => $this->session->userdata('ID'),
    		  'mensaje'	    	 => "Ingresó a Cargar Imagen de Nota Codigo : $id");	
      $this->bitacoras_model->AgregarBitacoras($RegistrarBitacora);
    			             //-//
    	$data["id"]        = $id;
    	$this->load->view('constant');
    	$this->load->view('view_header');
    	$this->load->view('notas/view_img',$data);
    	$this->load->view('view_footer');
    	
      }

    	public function SubeImg(){
    		$img1= "";
    		$str = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
            for($i=0;$i<5;$i++) {
                $img1 .= substr($str,rand(0,62),1);
            }
    		$idimg     = $this->input->post('id');
    		$nombreimg = $idimg."_".$img1;
    		$config['upload_path'] = realpath(APPPATH."../fotos");
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size'] = '1048576';
        $config['max_width'] = '900';
        $config['max_height'] = '900';
    		$config['file_name']= $idimg."_".$img1;
    		$this->load->library('upload', $config);
    		if (!$this->upload->do_upload('file')) {
    			echo  $this->upload->display_errors();
    		}else{
    			$file_info = $this->upload->data();
    			$data      = array('upload_data'=>$this->upload->data());
    			echo "Imagen Subida Correctamente.";
    			$img = array("foto_nota"=>$nombreimg.$file_info["file_ext"]);
    		//	$this->socios_model->GuardaImg($img);
    		   $this->notas_model->updateNota($img, $idimg);
    		}
    		}

      	//Muestra Nota
      	public	function view_mostrar($id) {
      	
        $id = base64_decode($id);
      	//Bitácora
      	$RegistrarBitacora = array(
      		  'fecha'        => date('Y-m-d H:i:s'),
      		  'usuario'      => $this->session->userdata('ID'),
      		  'mensaje'	    => "Muestra   Nota  id : $id");	
        $this->bitacoras_model->AgregarBitacoras($RegistrarBitacora);
      			             //-//
      	$data = array(
                'title' => 'Nota',
                'imgdenuncia' => $this->notas_model->buscarNota($id),
      		      );
      						
      		//	$this->load->view('socios/ventanacodigodebarras_view', $datos);
      			$this->load->view('notas/view_mostrar', $data);
      		}
	
 
}