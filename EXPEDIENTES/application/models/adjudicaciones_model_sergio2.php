<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class adjudicaciones_model extends CI_Model {
	function __construct()
     {
          parent::__construct();
     }
	 
	  /**
      * implementacion de adjudicacion controller buscarCooperativa
      * @return type query  
      */
     public function buscarCooperativa($filtro,$filtro2){
		$sql="SELECT concat(MATRICULA ,' - ', RAZONSOCIAL) AS label, RAZONSOCIAL, MATRICULA,ID,id_localidad FROM cooperativas   WHERE (MATRICULA LIKE  '%".$filtro."%' or RAZONSOCIAL LIKE '%".$filtro."%') AND id_localidad='".$filtro2."' ";
		$query=$this->db->query($sql);
		return $query->result();
		
	}
	 /**
      * implementacion de adjudicacion controller listarBeneficiarios
      * @return type query  
      */
	public function listarBeneficiarios($filtro,$filtro2){
		$sql="SELECT concat(documento,' - ', nombre_beneficios, ' - ', localidad) AS label, id , nombre_beneficios,localidad  FROM beneficios WHERE (nombre_beneficios LIKE  '%".$filtro."%' or ID LIKE '%".$filtro."%') AND localidad='".$filtro2."' ";
		$query=$this->db->query($sql);
		return $query->result();
	}
	 /**
      * implementacion de adjudicacion controller localidades
      * @return type query  
      */
	public function Localidades(){
		$sql="SELECT  * FROM localidad ";
		$query=$this->db->query($sql);
	return $query->result();}
	 /**
      * implementacion de adjudicacion controller programas
      * @return type query  
      */
	public function Programas(){
		$sql="SELECT  * FROM programas ";
		$query=$this->db->query($sql);
	return $query->result();}
	 
	 
	 
	public function ListarAdjudicaciones(){

	
$sql="SELECT        adjudicaciones.id_adjudicado, localidad.nombre_localidad, programas.nombre_programa, adjudicaciones.id_adj_moviles AS `Detalle ID`, cooperativas.razonsocial, beneficios.nombre_beneficios, 
                         beneficios.documento
FROM            localidad, adjudicaciones, adj_moviles, cooperativas, beneficios, programas
WHERE        localidad.id = adj_moviles.id_localidad AND adjudicaciones.id_adj_moviles = adj_moviles.id_adj_moviles AND adj_moviles.id_coperativa = cooperativas.id AND adjudicaciones.id_beneficio = beneficios.id AND 
                         adj_moviles.id_programa = programas.id";
	$query=$this->db->query($sql);
		return $query->result();
	}
	public function BuscarProducto($id){
		$sql="SELECT * FROM productos  where id='".$id."'";
		$query=$this->db->query($sql);
		return $query->result();
	}
	public function EliminarRecibo($arrayRecibos,$id)
	{
		# code...
		$this->db->where('id',$id);
		return $this->db->delete('documentos'); 
		//$this->db->trans_start();
		//$this->db->where('id', $id);
		//$tipo="borrado";
		//$this->db->update('documentos',$arrayRecibos); 
		//$this->db->trans_complete();
	}
	public function Categorias(){
		$sql="select * from categorias";
		$query=$this->db->query($sql);
		return $query->result();
	}
	public function Subcategorias($id){
		$sql="select * from subcategoria where id_categoria='".$id."'";
		$query=$this->db->query($sql);
		return $query->result();
	}
	public function Proveedores(){
		$sql="SELECT  * FROM proveedores where estatus=1";
		$query=$this->db->query($sql);
		return $query->result();
	}
	public function ExisteCodigo($codigo){
		$this->db->where("codigo",$codigo);
        $check_exists = $this->db->get("productos");
        if($check_exists->num_rows() == 0){
            return false;
        }else{
            return true;
        }
	}
	public function SaveProductos($arrayProductos){
		$this->db->trans_start();
     	$this->db->insert('productos', $arrayProductos);
     	$this->db->trans_complete();
	}
	public function UpdateProductos($arrayProductos, $id){
		$this->db->trans_start();
		$this->db->where('id', $id);
		$this->db->update('productos', $arrayProductos); 
		$this->db->trans_complete();
	}
	
	//-----------------exportar--------------// 
	public function get()
	{
		$fields = $this->db->field_data('documentos');
		$query = $this->db->select('*')->get('documentos');
		return array("fields" => $fields, "query" => $query);
	}
	
	
	
}