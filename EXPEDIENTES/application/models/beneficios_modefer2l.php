<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class beneficios_model extends CI_Model {
	function __construct()
     {
          parent::__construct();
     }
	public function ListarBeneficios(){
		$sql="SELECT * from beneficios order by nombre_beneficios asc";
		$query=$this->db->query($sql);
		return $query->result();
	}
	public function BuscaBeneficios($id){
		$sql="SELECT * from beneficios where id='".$id."' limit 1";
		$query=$this->db->query($sql);
		return $query->result();
	}
	public function SaveBene($RegistraProfesor){
		$this->db->trans_start();
     	$this->db->insert('beneficios', $RegistraProfesor);
     	$this->db->trans_complete();
	}
	public function UpdateBeneficios($UpdateProfesor,$id){
		$this->db->trans_start();
		$this->db->where('id', $id);
		$this->db->update('beneficios', $UpdateProfesor); 
		$this->db->trans_complete();
	}
	public function EliminarBeneficios($id)
	{
		# code...
		$this->db->where('id',$id);
		return $this->db->delete('beneficios');
	}
	
	//-----------------exportar--------------// 
	public function get()
	{
		$fields = $this->db->field_data('beneficios');
		$query = $this->db->select('*')->get('beneficios');
		return array("fields" => $fields, "query" => $query);
	}
	
}