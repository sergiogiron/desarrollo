<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class inscripciones_model extends CI_Model {
	function __construct()
     {
          parent::__construct();
     }
	public function ListarInscripciones(){
		
		$sql="SELECT I.id,I.documento,I.estado,I.nombre_beneficiario,I.barrio,I.mza,I.lote,L.nombre_localidad  
		FROM inscripciones I , localidades L 
		WHERE I.id_localidad=L.id";
		$query=$this->db->query($sql);
		return $query->result();
	}
	public function BuscaInscripciones($id){
		$sql="SELECT * from inscripciones where id='".$id."' limit 1";
		$query=$this->db->query($sql);
		return $query->result();
	}
	public function saveInscripcion($RegistraInscripcion){
		$this->db->trans_start();
     	$this->db->insert('inscripciones', $RegistraInscripcion);
     	$this->db->trans_complete();
	}
	public function updateInscripciones($UpdateInscripcion,$id){
		$this->db->trans_start();
		$this->db->where('id', $id);
		$this->db->update('inscripciones', $UpdateInscripcion); 
		$this->db->trans_complete();
	}
	public function Localidades(){
		$sql="SELECT  * FROM localidades ";
		$query=$this->db->query($sql);
		return $query->result();
	}
	public function eliminarInscripcion($id){
		$this->db->where('id',$id);
		return $this->db->delete('inscripciones');
	}
	
}