<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class usuarios_model extends CI_Model {
	function __construct()
     {
          parent::__construct();
     }
     /*
	public function ListarUsuarios(){
		$sql="SELECT * from usuarios order by NOMBRE asc";
		$query=$this->db->query($sql);
		return $query->result();
	}
	*/
	public function ListarUsuarios(){
		$sql="SELECT U.ID, U.NOMBRE, U.APELLIDOS, U.EMAIL, U.FECHA_REGISTRO, U.ESTATUS, U.TIPO, U.PASSWORD, A.nombre_area, U.PRIVILEGIOS, U.ONLINE
		FROM usuarios as U INNER JOIN areas as A ON U.ID_AREA=A.id_area ORDER BY ID ASC";
		$query=$this->db->query($sql);
		return $query->result();
	}
	public function BuscaUsuario($id){
		$sql="SELECT * from usuarios where id='".$id."' limit 1";
		$query=$this->db->query($sql);
		return $query->result();
	}
	public function SaveUs($RegistraUsuarios){
		$this->db->trans_start();
     	$this->db->insert('usuarios', $RegistraUsuarios);
     	$this->db->trans_complete();
	}
	public function UpdateUs($UpdateUsuario,$id){
		$this->db->trans_start();
		$this->db->where('id', $id);
		$this->db->update('usuarios', $UpdateUsuario); 
		$this->db->trans_complete();
	}
	public function EliminarUsuario($id)
	{
		# code...
		$this->db->where('id',$id);
		return $this->db->delete('usuarios');
	}
	
	public function ExisteEmail($email){
		$this->db->where("EMAIL",$email);
        $check_exists = $this->db->get("usuarios");
        if($check_exists->num_rows() == 0){
            return false;
        }else{
            return true;
        }
	}
	
	
	
	
	
	
	
	
	
	
	
}