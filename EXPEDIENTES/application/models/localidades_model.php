<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class localidades_model extends CI_Model {
	function __construct()
     {
          parent::__construct();
     }
		 
	public function ListarLocalidades(){
		$sql="SELECT * from localidades order by nombre_localidad asc";
		$query=$this->db->query($sql);
		return $query->result();
	}
	public function BuscaLocalidades($id){
		$sql="SELECT * from localidades where id='".$id."' limit 1";
		$query=$this->db->query($sql);
		return $query->result();
	}
	public function SaveLocalidades($Registra){
		$this->db->trans_start();
     	$this->db->insert('localidades', $Registra);
     	$this->db->trans_complete();
	}
	public function UpdateLocalidades($Update,$id){
		$this->db->trans_start();
		$this->db->where('id', $id);
		$this->db->update('localidades', $Update); 
		$this->db->trans_complete();
	}
	public function EliminarLocalidad($id)
	{
		# code...
		$this->db->where('id',$id);
		return $this->db->delete('localidades');
	}
	
}