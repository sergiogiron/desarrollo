<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class pases_model extends CI_Model {
	function __construct()
     {
          parent::__construct();
     }

	public function traerExp(){
		$sql="SELECT  * FROM expedientes";
		$query=$this->db->query($sql);
		return $query->result();
	}
	public function SavePase($RegistraPase){ 
		$this->db->trans_start();
     	$this->db->insert('pases', $RegistraPase);
     	$this->db->trans_complete();
	}
	public function updatePase($UpdatePase,$id){
		$this->db->trans_start();
		$this->db->where('id_pase', $id);
		$this->db->update('pases', $UpdatePase); 
		$this->db->trans_complete();
	}
	public function borrarPase($id){
		$this->db->where('id_pase',$id);
		return $this->db->delete('pases');
	}
	

	//-----------------exportar--------------// 
	public function get(){
		$fields = $this->db->field_data('pases');
		$query = $this->db->select('*')->get('pases');
		return array("fields" => $fields, "query" => $query);
	}
	
	//--------------------------busca el valor maximo para sacar el codigo------//
	public function MaxCodigo(){
		$sql="SELECT max(codigo_exp) as max FROM pases ";
		$query=$this->db->query($sql);
		return $query->result();	
	}
	
	
}