<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class areas_model extends CI_Model {
	function __construct()
     {
          parent::__construct();
     }
     public function listarArea(){
     	$sql="SELECT * from areas order by codigo_area asc";
		$query=$this->db->query($sql);
		return $query->result();
     }
     /*
	public function listarResolucion(){
		$sql="SELECT a.id_denuncia,a.denunciante, a.reclamo, b.razonsocial, a.fecha_denuncia ,a.observaciones, a.foto_denuncia FROM cooperativas b, denuncias a WHERE b.id=a.id_cooperativa order by a.id_denuncia DESC";
		$query=$this->db->query($sql);
		return $query->result();
	}*/
	public function buscarArea($id){
		//Obtiene todos los registros de la tabla areas por id, pero muestra un registro.
		$sql="SELECT * from areas where id_area='".$id."' limit 1"; 
		$query=$this->db->query($sql); 
		return $query->result();	   
	}
	public function buscarArea1($id){
		//Obtiene todos los registros de la tabla areas por id, pero muestra un registro.
		$sql="SELECT nombre_area from areas where id_area='".$id."' limit 1"; 
		$query=$this->db->query($sql); 
		return $query->result();	   
	}
	public function SaveArea($RegistraArea){
		$this->db->trans_start();
     	$this->db->insert('areas', $RegistraArea);
     	$this->db->trans_complete();
	}
	public function updateArea($UpdateArea,$id){
		$this->db->trans_start();
		$this->db->where('id_area', $id);
		$this->db->update('areas', $UpdateArea); 
		$this->db->trans_complete();
	}
	public function eliminarArea($id){
		$this->db->where('id_area',$id);
		return $this->db->delete('areas');
	}
	
	//-----------------exportar--------------// 
	public function get(){
		$fields = $this->db->field_data('areas');
		$query = $this->db->select('*')->get('areas');
		return array("fields" => $fields, "query" => $query);
	}
	
	//--------------------------busca el valor maximo para sacar el codigo------//
	public function maxCodigo(){
		$sql="SELECT max(codigo_area) as max FROM areas ";
		$query=$this->db->query($sql);
		return $query->result();	
	}
	
	
}