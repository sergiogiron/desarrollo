<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class beneficios_model extends CI_Model {
	function __construct()
     {
          parent::__construct();
     }
		 
	 
	public function ListarBeneficios(){
		$sql="SELECT B.id_localidad,B.documento,B.estado,B.nombre_beneficios,B.barrio,B.mza,
		B.lote,L.nombre_localidad as NLoca , L.id , B.id_programa,P.nombre_programa as nombre_programa 
		from beneficios as B
		INNER JOIN localidades AS L ON B.id_localidad = L.id 
		INNER JOIN programas AS P ON B.id_programa = P.id 
		order by nombre_beneficios asc";
		$query=$this->db->query($sql);
		return $query->result();
	}
	public function BuscaBeneficios($id){
		$sql="SELECT * from beneficios where id='".$id."' limit 1";
		$query=$this->db->query($sql);
		return $query->result();
	}
	public function SaveBene($RegistraProfesor){
		$this->db->trans_start();
     	$this->db->insert('beneficios', $RegistraProfesor);
     	$this->db->trans_complete();
	}
	public function UpdateBeneficios($UpdateProfesor,$id){
		$this->db->trans_start();
		$this->db->where('id', $id);
		$this->db->update('beneficios', $UpdateProfesor); 
		$this->db->trans_complete();
	}
	public function EliminarBeneficios($id)
	{
		# code...
		$this->db->where('id',$id);
		return $this->db->delete('beneficios');
	}
	
	public function Localidades(){
		$sql="SELECT  * FROM localidades ";
		$query=$this->db->query($sql);
		return $query->result();
	}
	
	
	
	//-----------------exportar--------------// 
	public function get()
	{
		$fields = $this->db->field_data('beneficios');
		$query = $this->db->select('*')->get('beneficios');
		return array("fields" => $fields, "query" => $query);
	}
	
	
	
}