<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class pases_model extends CI_Model {
	function __construct()
     {
          parent::__construct();
     }
     public function listarPase(){
     	$sql="SELECT * from pases order by codigo_pase asc";
		$query=$this->db->query($sql);
		return $query->result();
     }
     /*
	public function listarResolucion(){
		$sql="SELECT a.id_denuncia,a.denunciante, a.reclamo, b.razonsocial, a.fecha_denuncia ,a.observaciones, a.foto_denuncia FROM cooperativas b, denuncias a WHERE b.id=a.id_cooperativa order by a.id_denuncia DESC";
		$query=$this->db->query($sql);
		return $query->result();
	}*/
	
	public function traerAreas(){
		$sql="SELECT  * FROM areas ";
		$query=$this->db->query($sql);
		return $query->result();
	}
	public function buscarPase($id){
		//Obtiene todos los registros de la tabla notas por id, pero muestra un registro.
		$sql="SELECT * from pases where id_pase='".$id."' limit 1"; 
		$query=$this->db->query($sql); //Realiza la consulta a la BD
		return $query->result();	   //Obtiene el resultado de la consulta
	}
	public function SavePase($RegistraPase){
		$this->db->trans_start();
     	$this->db->insert('pases', $RegistraPase);
     	$this->db->trans_complete();
	}
	public function updatePase($UpdatePase,$id){
		$this->db->trans_start();
		$this->db->where('id_pase', $id);
		$this->db->update('pases', $UpdatePase); 
		$this->db->trans_complete();
	}
	public function eliminarPase($id){
		$this->db->where('id_pase',$id);
		return $this->db->delete('pases');
	}
	
	//-----------------exportar--------------// 
	public function get(){
		$fields = $this->db->field_data('pases');
		$query = $this->db->select('*')->get('pases');
		return array("fields" => $fields, "query" => $query);
	}
	
	//--------------------------busca el valor maximo para sacar el codigo------//
	public function MaxCodigo(){
		$sql="SELECT max(codigo_exp) as max FROM pases ";
		$query=$this->db->query($sql);
		return $query->result();	
	}
	
	
}