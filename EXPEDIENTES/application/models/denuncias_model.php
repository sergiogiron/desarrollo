<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class denuncias_model extends CI_Model {
	function __construct()
     {
          parent::__construct();
     }
     /**
      * Lista todas las denuncias...
      * @return type
      */
	public function listarDenuncia(){
		$sql="SELECT a.id_denuncia,a.denunciante, a.reclamo, b.razonsocial, a.fecha_denuncia ,a.observaciones, a.foto_denuncia FROM cooperativas b, denuncias a WHERE b.id=a.id_cooperativa order by a.id_denuncia DESC";
		$query=$this->db->query($sql);
		return $query->result();
	}
	/**
	 * Permite realizar una busqueda de denuncias efectuadas
	 * @param integer  $id  identificador de la denuncia
	 * @return type
	 */
	public function buscarDenuncia($id){
		//Obtiene todos los registros de la tabla Denuncias por id, pero muestra un registro.
		$sql="SELECT * from denuncias where id_denuncia='".$id."' limit 1"; 
		$query=$this->db->query($sql); //Realiza la consulta a la BD
		return $query->result();	   //Obtiene el resultado de la consulta
	}
	/**
	 * Guarda los datos de una nueva denuncia
	 * @param type $RegistraDenuncia  Obtiene los campos de la BD
	 * @return type
	 */
	public function saveDenuncia($RegistraDenuncia){
		$this->db->trans_start();
     	$this->db->insert('denuncias', $RegistraDenuncia);
     	$this->db->trans_complete();
	}
	/**
	 * Guarda la actualizacion de los datos 
	 * @param type  $UpdateDenuncia  Actualiza los campos de la BD
	 * @param type 	$id   			 identificador de la denuncia a actualizar
	 * @return type
	 */
	public function updateDenuncia($UpdateDenuncia,$id){
		$this->db->trans_start();
		$this->db->where('id_denuncia', $id);
		$this->db->update('denuncias', $UpdateDenuncia); 
		$this->db->trans_complete();
	}
	/**
	 * Elimina el registro de una denuncia
	 * @param type $id  identificador de una denuncia a actualizar
	 * @return type
	 */
	public function eliminarDenuncia($id){
		$this->db->where('id_denuncia',$id);
		return $this->db->delete('denuncias');
	}
	
	//-----------------exportar--------------// 
	public function get()
	{
		$fields = $this->db->field_data('denuncias');
		$query = $this->db->select('*')->get('denuncias');
		return array("fields" => $fields, "query" => $query);
	}
	
	
	
	
}