<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class instituto_model extends CI_Model {
	function __construct()
     {
          parent::__construct();
     }

	public function buscarNota($res){
		//El nro. de nota es el campo Resolucion en la tabla adj_moviles
		$sql="SELECT * from adj_moviles where Resolucion='".$res."' limit 1";
		$query=$this->db->query($sql);
		return $query->result();
	}
	public function saveCarga($RegistraCarga, $Resolucion){  
		$this->db->trans_start();  
		$this->db->where('Resolucion', $Resolucion);  
     	$this->db->update('adj_moviles', $RegistraCarga); //Guardo el Nro. de exp en adj_moviles
     	$this->db->trans_complete();
	}
	}