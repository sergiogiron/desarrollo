<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class reportes_model extends CI_Model {
	function __construct()
     {
          parent::__construct();
     }
     public function GeneraReporte($filtro){
		 
		$sql="SELECT        adj_moviles.id_localidad, cooperativas.resolucion, cooperativas.razonsocial, cooperativas.cantidad_bene, adj_moviles.id_coperativa, adj_moviles.id_beneficio, beneficios.nombre_beneficios, 
                         beneficios.documento, beneficios.barrio, beneficios.mza, beneficios.lote
FROM            adj_moviles, cooperativas, beneficios
WHERE        adj_moviles.id_coperativa = cooperativas.id AND adj_moviles.id_beneficio = beneficios.id
GROUP BY adj_moviles.id_localidad, cooperativas.resolucion, cooperativas.razonsocial, cooperativas.cantidad_bene, adj_moviles.id_coperativa, adj_moviles.id_beneficio, beneficios.nombre_beneficios, 
                         beneficios.documento, beneficios.barrio, beneficios.mza, beneficios.lote
HAVING        (adj_moviles.id_localidad = '".$filtro."')";
		$query=$this->db->query($sql);
		return $query->result();
	}
	
	 public function DatosNecesarios($localidad)
	 {
		 
		 $sql="SELECT        adj_moviles.id_localidad, cooperativas.resolucion, cooperativas.razonsocial, cooperativas.cantidad_bene, localidades.nombre_localidad, adj_moviles.id_coperativa
FROM            adj_moviles, cooperativas, localidades
WHERE        adj_moviles.id_coperativa = cooperativas.id AND adj_moviles.id_localidad = localidades.id
GROUP BY adj_moviles.id_localidad, cooperativas.resolucion, cooperativas.razonsocial, cooperativas.cantidad_bene, localidades.nombre_localidad, adj_moviles.id_coperativa
HAVING        (adj_moviles.id_localidad = '".$localidad."')";
		 $query=$this->db->query($sql);
		return $query->result();
		 
	 }
	
	
	
	public function UpdateExistenciasProducto($codigo,$cantidad){
		$sql="update productos set cantidad= cantidad - '".$cantidad."' where codigo='".$codigo."'";
		$query=$this->db->query($sql);
		return True;
	}
	public function reportesGenera($FInicial, $FFinal, $Documento){
		$sql="SELECT * FROM documentos   WHERE FECHA between '".$FInicial."' AND '".$FFinal."' AND TIPO='".$Documento."'";
		//echo $sql;
		$query=$this->db->query($sql);
		return $query->result();
	}
	
	public function Localidades(){
		$sql="SELECT *	from localidades";
		$query=$this->db->query($sql);
	return $query->result();}

}