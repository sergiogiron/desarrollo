<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class expedientes_model extends CI_Model {
	function __construct()
     {
          parent::__construct();
     }

    public function listaPases(){
        $sql="SELECT P.id_pase , P.codigo_exp, P.remitente_exp,P.fecha_pase,P.estado_pase,P.obs_pase , A.nombre_area 
        	  FROM pases as P INNER JOIN  areas  as A  ON A.id_area =P.id_area ORDER BY `id_pase` DESC ";
		$query=$this->db->query($sql);
		return $query->result();	
		}

	public function buscarPase($id){
		$sql="SELECT * from pases where id_pase='".$id."' limit 1"; 
		$query=$this->db->query($sql); 
		return $query->result();	   
	}

	public function traerDependencias(){
		$sql="SELECT  * FROM dependencias ";
		$query=$this->db->query($sql);
		return $query->result();
	}

	public function traerAreas(){
		$sql="SELECT  * FROM areas ";
		$query=$this->db->query($sql);
		return $query->result();
	}

	public function listarExp(){
     	$sql="SELECT E.id_exp, E.codigo_exp, E.fecha_exp, E.remitente_exp, E.asunto_exp, E.obs_exp, E.estado_exp, E.foto_exp, D.nombre_dep, A.nombre_area
     	FROM expedientes as E INNER JOIN dependencias as D ON D.id_dep=E.id_dep
     	INNER JOIN areas as A ON A.id_area=E.id_area order by id_exp DESC";
		$query=$this->db->query($sql);
		return $query->result();
     }

	public function buscarExp($id){
		$sql="SELECT * from expedientes where id_exp='".$id."' limit 1"; 
		$query=$this->db->query($sql); //Realiza la consulta a la BD
		return $query->result();	   //Obtiene el resultado de la consulta
	}

	public function SaveExp($RegistraExp){
		$this->db->trans_start();
     	$this->db->insert('expedientes', $RegistraExp);
     	$this->db->trans_complete();
	}

	public function updateExp($UpdateExp,$id){
		$this->db->trans_start();
		$this->db->where('id_exp', $id);
		$this->db->update('expedientes', $UpdateExp); 
		$this->db->trans_complete();
	}
	
	public function eliminarExp($id){
		$this->db->where('id_exp',$id);
		return $this->db->delete('expedientes');
	}
	
	//-----------------exportar--------------// 
	public function get(){
		$fields = $this->db->field_data('expedientes');
		$query = $this->db->select('*')->get('expedientes');
		return array("fields" => $fields, "query" => $query);
	}
	
	//--------------------------busca el valor maximo para sacar el codigo------//
	public function MaxExp(){
		$sql="SELECT max(codigo_exp) as max FROM expedientes ";
		$query=$this->db->query($sql);
		return $query->result();	
	}
	
	
}