<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class socios_model extends CI_Model {
	function __construct()
     {
          parent::__construct();
     }
	public function ListarSocios(){
		$sql="SELECT *,S.id, S.documento,S.telefono,C.razonsocial as razon from socios as S 
        INNER JOIN cooperativas  AS C ON S.id_cooperativa = C.id
		order by nombre asc";
		$query=$this->db->query($sql);
		return $query->result();
	}
	public function BuscaSocios($id){
		$sql="SELECT * from socios where id='".$id."' limit 1";
		$query=$this->db->query($sql);
		return $query->result();
	}
	public function SaveSocios($RegistraSocio){
		$this->db->trans_start();
     	$this->db->insert('socios', $RegistraSocio);
     	$this->db->trans_complete();
	}
	public function UpdateSocios($UpdateSocio,$id){
		$this->db->trans_start();
		$this->db->where('id', $id);
		$this->db->update('socios', $UpdateSocio); 
		$this->db->trans_complete();
	}
	public function EliminarSocio($id)
	{
		# code...
		$this->db->where('id',$id);
		return $this->db->delete('socios');
	}
	public function ExisteDocumento($codigo){
		$this->db->where("documento",$codigo);
        $check_exists = $this->db->get("socios");
        if($check_exists->num_rows() == 0){
            return false;
        }else{
            return true;
        }
       }
    public function ExisteCargo($codigo,$codigo1){
	   $this->db->where("id_cooperativa",$codigo);
	   $this->db->where("cargo",$codigo1);
        $check_exists = $this->db->get("socios");
        if($check_exists->num_rows() == 0){
            return false;
        }else{
            return true;
        }
       }

	//-----------------exportar--------------// 
	public function get()
	{
		$sql="SELECT S.id,S.nombre, S.documento,S.cuit,S.fechanac,S.domicilio,S.nro,S.manzana,S.lote,
		S.barrio,	S.id_localidad,S.provincia,S.id_provincia,S.telefono,S.email,S.cargo ,
C.razonsocial ,S.tipo,S.plan  from socios as S 
        INNER JOIN cooperativas  AS C ON S.id_cooperativa = C.id
		
		order by C.razonsocial asc";
		$query=$this->db->query($sql);
			
	//	$fields = ('ID');
		$fields = $this->db->field_data('socios');
	//	$query = $this->db->select('*')->get('socios');
		return array("fields" => $fields, "query" => $query);
	}     
	   
	   
	   
	   
	   
	   
	   
	   
}