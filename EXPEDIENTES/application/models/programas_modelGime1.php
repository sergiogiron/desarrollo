<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class programas_model extends CI_Model {
	function __construct()
     {
          parent::__construct();
     }
	public function ListarProgramas(){
		$sql="SELECT * from programas order by nombre_programa asc";
		$query=$this->db->query($sql);
		return $query->result();
	}
	public function BuscaProgramas($id){
		$sql="SELECT * from programas where id='".$id."' limit 1";
		$query=$this->db->query($sql);
		return $query->result();
	}
	public function SaveProgramas($RegistraProveedor){
		$this->db->trans_start();
     	$this->db->insert('programas', $RegistraProveedor);
     	$this->db->trans_complete();
	}
	public function UpdateProgramas($UpdateProveedor,$id){
		$this->db->trans_start();
		$this->db->where('id', $id);
		$this->db->update('programas', $UpdateProveedor); 
		$this->db->trans_complete();
	}
		public function EliminarPrograma($id)
	{
		# code...
		$this->db->where('id',$id);
		return $this->db->delete('programas');
	}
}