<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class notas_model extends CI_Model {
	function __construct()
     {
          parent::__construct();
     }
     public function listarNota(){
     	$sql="SELECT * from notas order by codigo asc";
		$query=$this->db->query($sql);
		return $query->result();
     }
     /*
	public function listarResolucion(){
		$sql="SELECT a.id_denuncia,a.denunciante, a.reclamo, b.razonsocial, a.fecha_denuncia ,a.observaciones, a.foto_denuncia FROM cooperativas b, denuncias a WHERE b.id=a.id_cooperativa order by a.id_denuncia DESC";
		$query=$this->db->query($sql);
		return $query->result();
	}*/
	public function buscarNota($id){
		//Obtiene todos los registros de la tabla notas por id, pero muestra un registro.
		$sql="SELECT * from notas where id_nota='".$id."' limit 1"; 
		$query=$this->db->query($sql); //Realiza la consulta a la BD
		return $query->result();	   //Obtiene el resultado de la consulta
	}
	public function SaveNota($RegistraNota){
		$this->db->trans_start();
     	$this->db->insert('notas', $RegistraNota);
     	$this->db->trans_complete();
	}
	public function updateNota($UpdateNota,$id){
		$this->db->trans_start();
		$this->db->where('id_nota', $id);
		$this->db->update('notas', $UpdateNota); 
		$this->db->trans_complete();
	}
	public function eliminarNota($id){
		$this->db->where('id_nota',$id);
		return $this->db->delete('notas');
	}
	
	//-----------------exportar--------------// 
	public function get()
	{
		$fields = $this->db->field_data('notas');
		$query = $this->db->select('*')->get('notas');
		return array("fields" => $fields, "query" => $query);
	}
	
	
	
	
}