<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class localidades_model extends CI_Model {
	function __construct()
     {
          parent::__construct();
     }
     /**
      * Obtiene un listado de las localidades registradas
      * @return type
      */
	public function listarLocalidad(){
		$sql="SELECT * from localidad order by nombre_localidad asc";
		$query=$this->db->query($sql);
		return $query->result();
	}
	/**
	 * Busca localidades por id
	 * @param type $id identificador de una localidad
	 * @return type
	 */
	public function buscarLocalidad($id){
		$sql="SELECT * from localidad where id='".$id."' limit 1";
		$query=$this->db->query($sql);
		return $query->result();
	}
	/**
	 * Guarda cambios realizados sobre una locoalidad
	 * @param type $RegistraLocalidad array con los campos obtenidos de una localidad
	 * @return type
	 */
	public function saveLocalidad($RegistraLocalidad){
		$this->db->trans_start();
     	$this->db->insert('localidad', $RegistraLocalidad);
     	$this->db->trans_complete();
	}
	/**
	 * Elimina una localidad especificada por id
	 * @param type $id identificador de una localidad
	 * @return type
	 */
	public function eliminarLocalidad($id){
		$this->db->where('id',$id);
		return $this->db->delete('localidad');
	}
	
	/**
	 * Realiza los cambios 
	 * @param type $Update 
	 * @param type $id 
	 * @return type
	 */
	public function updateLocalidad($UpdateLocalidad,$id){
		$this->db->trans_start();
		$this->db->where('id', $id);
		$this->db->update('localidad', $UpdateLocalidad); 
		$this->db->trans_complete();
	}
	
	
}