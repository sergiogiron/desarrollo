<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class bitacoras_model extends CI_Model {
	function __construct()
     {
          parent::__construct();
     }
	 
	  
	public function ListarBitacoras(){
//$sql="SELECT P.id, P.codigo, P.descripcion, P.precio_compra, P.precio_venta, P.cantidad, C.descripcion AS DesCategoria, Pro.nombre_proveedor FROM recibos AS P INNER JOIN categorias AS C ON P.id_categoria = C.id INNER JOIN proveedores AS Pro ON P.id_proveedor = Pro.id order by P.descripcion asc";
	
//$sql="SELECT R.id,R.tipo, R.fecha, R.cliente, R.total, R.usuario_nulo FROM documentos AS R  INNER JOIN proveedores AS Pro ON P.id_proveedor = Pro.id order by P.descripcion asc";
	
$sql="SELECT B.id,  B.fecha, U.nombre as nusuario ,B.mensaje  FROM bitacoras  AS B INNER JOIN usuarios AS U ON U.id = B.usuario  order by B.fecha desc";      
		//echo $sql;

	$query=$this->db->query($sql);
		return $query->result();
	}
	public function BuscarProducto($id){
		$sql="SELECT * FROM productos  where id='".$id."'";
		$query=$this->db->query($sql);
		return $query->result();
	}
	public function EliminarRecibo($arrayRecibos,$id)
	{
		# code...
		$this->db->where('id',$id);
		return $this->db->delete('documentos'); 
		//$this->db->trans_start();
		//$this->db->where('id', $id);
		//$tipo="borrado";
		//$this->db->update('documentos',$arrayRecibos); 
		//$this->db->trans_complete();
	}
	public function Categorias(){
		$sql="select * from categorias";
		$query=$this->db->query($sql);
		return $query->result();
	}
	public function Subcategorias($id){
		$sql="select * from subcategoria where id_categoria='".$id."'";
		$query=$this->db->query($sql);
		return $query->result();
	}
	public function Proveedores(){
		$sql="SELECT  * FROM proveedores where estatus=1";
		$query=$this->db->query($sql);
		return $query->result();
	}
	public function ExisteCodigo($codigo){
		$this->db->where("codigo",$codigo);
        $check_exists = $this->db->get("productos");
        if($check_exists->num_rows() == 0){
            return false;
        }else{
            return true;
        }
	}
	public function SaveProductos($arrayProductos){
		$this->db->trans_start();
     	$this->db->insert('productos', $arrayProductos);
     	$this->db->trans_complete();
	}
	public function UpdateProductos($arrayProductos, $id){
		$this->db->trans_start();
		$this->db->where('id', $id);
		$this->db->update('productos', $arrayProductos); 
		$this->db->trans_complete();
	}
	
	//-----------------exportar--------------// 
	public function get()
	{
		$fields = $this->db->field_data('documentos');
		$query = $this->db->select('*')->get('documentos');
		return array("fields" => $fields, "query" => $query);
	}
	
	//---------------bitacora-----------------//
	
	function agregaraa($idusuario,$texto) {
		$fechahora = date('Y-m-d H:i:s');

		$resultado=mysql_query("insert into bitacoras 
			(fecha,usuario,mensaje) values 
			('". $fechahora ."',". $idusuario .",".$texto.")"
		);
	}

	function borraRegistrosViejos($meses) {
		$resultado=mysql_query("delete from bitacora 
			where DATE_SUB(CURDATE(), INTERVAL ". $meses ." MONTH) >= fechahora
		");
	}
	public function AgregarBitacoras($arrayBitacoras){
		$this->db->trans_start();
     	$this->db->insert('bitacoras', $arrayBitacoras);
     	$this->db->trans_complete();
	}
	
	
}