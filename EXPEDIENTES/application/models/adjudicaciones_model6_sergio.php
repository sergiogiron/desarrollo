<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class adjudicaciones_model extends CI_Model {
	function __construct()
     {
          parent::__construct();
     }
	 
	  /**
      * implementacion de adjudicacion controller buscarCooperativa
      * @return type query  
      */
     public function buscarCooperativa($filtro,$filtro2,$bandera){
		 
		 
		 
		 if ($bandera==0){
		$sql="SELECT concat(MATRICULA ,' - ', RAZONSOCIAL) AS label, RAZONSOCIAL, MATRICULA,ID,id_localidad FROM cooperativas   WHERE (MATRICULA LIKE  '%".$filtro."%' or RAZONSOCIAL LIKE '%".$filtro."%') AND id_localidad='".$filtro2."' ";
		$query=$this->db->query($sql);
		 return $query->result();}
		 else
		 {$sql="SELECT concat(MATRICULA ,' - ', RAZONSOCIAL) AS label, RAZONSOCIAL, MATRICULA,ID,id_localidad FROM cooperativas   WHERE (MATRICULA LIKE  '%".$filtro."%' or RAZONSOCIAL LIKE '%".$filtro."%') ";
		$query=$this->db->query($sql);
		 return $query->result();}
		
	}
	 /**
      * implementacion de adjudicacion controller listarBeneficiarios
      * @return type query  
      */
	public function listarBeneficiarios($filtro,$filtro2,$bandera)
	{
		$sql="SELECT concat(documento,' - ', nombre_beneficios, ' - ', localidad) AS label, id ,documento,localidad ,nombre_beneficios,localidad,barrio,mza,lote,obs , Estado  FROM beneficios WHERE (nombre_beneficios LIKE  '%".$filtro."%' or documento LIKE '%".$filtro."%') AND localidad='".$filtro2."' AND Estado=2 ";
		$query=$this->db->query($sql);
		return $query->result();}
	
	 /**
      * implementacion de adjudicacion controller localidades
      * @return type query  
      */
	public function Localidades(){
		$sql="SELECT  * FROM localidad ";
		$query=$this->db->query($sql);
	return $query->result();}
	 /**
      * implementacion de adjudicacion controller programas
      * @return type query  
      */
	public function Programas(){
		$sql="SELECT  * FROM programas ";
		$query=$this->db->query($sql);
	return $query->result();}
	 
public function TraerDatos($localidad,$cantidad){

//Crear conexión
//mysqli_connect (host, nombre de usuario, contraseña, dbname);
$dir="localhost";
$user="p1000543";
$pass="19tozuBUno";
$bd="p1000543_uceppi";
// Consultar la base de datos
$consulta_mysql="SELECT * FROM beneficios WHERE beneficios.localidad='".$localidad."'  AND Estado=2  
 ORDER BY RAND() LIMIT 0, ".$cantidad." ";
$con = mysqli_connect($dir,$user,$pass,$bd);
if (mysqli_connect_errno($con))
   {
   echo "No se pudo conectar a MySQL: " . mysqli_connect_error ();
   }
   $resultado=mysqli_query($con,$consulta_mysql);
		$arreglo = array();
			while ($fila = mysqli_fetch_array($resultado)) {
				$arreglo[]=$fila;
			}
			mysqli_close($con);
			return $arreglo;

}
	
	 
	public function ListarAdjudicaciones(){

	
$sql="SELECT        beneficios.nombre_beneficios AS Nombre, cooperativas.razonsocial AS Coperativa, programas.nombre_programa AS Programa, localidad.nombre_localidad AS Localidad, beneficios.documento AS DNI, 
                         adj_moviles.Resolucion
FROM            adjudicaciones, adj_moviles, beneficios, cooperativas, programas, localidad
WHERE        adjudicaciones.id_adj_moviles = adj_moviles.id_adj_moviles AND adj_moviles.id_beneficio = beneficios.id AND adj_moviles.id_coperativa = cooperativas.id AND adj_moviles.id_programa = programas.id AND 
                         adj_moviles.id_localidad = localidad.id";
	$query=$this->db->query($sql);
		return $query->result();
	}
	
	public function EliminarRecibo($arrayRecibos,$id)
	{
		# code...
		$this->db->where('id',$id);
		return $this->db->delete('documentos'); 
	
	}
	
	public function Adjudicacion($id_adj_movil,$cantidad){
		$sql="insert into adjudicaciones(id_adj_moviles,cantidad) values 
                       (".$id_adj_movil." ,".$cantidad.")";
	$query=$this->db->query($sql);
	}
	
	public function TraerCodigo(){
		$sql="SELECT MAX(id_adj_moviles) AS id FROM adj_moviles";
		$query=$this->db->query($sql);
		return $query->result();
	}
	
	public function TraerCupo($id){
		$sql="SELECT restantes FROM programas where id='".$id."'";
		$query=$this->db->query($sql);
		return $query->result();
	}
	
	public function GuardarDatos($localidad,$programa,$cooperativa,$recibidos,$id_adj_movil,$beneficiario){
	$sql="insert into adj_moviles(id_localidad,id_coperativa,id_programa,id_beneficio,id_adj_moviles) values 
                       (".$localidad." ,".$cooperativa.",".$programa.",".$beneficiario.",".$id_adj_movil.")";
	$query=$this->db->query($sql);
	$sql="Update beneficios Set Estado=3 Where id='".$beneficiario."'";
    $query=$this->db->query($sql);
	
	}
	public function ActualizaCupo($cupo,$programa){
		$sql="Update programas Set restantes=restantes-".$cupo."  Where id='".$programa."'";
    $query=$this->db->query($sql);
	}
	
	public function Verificar($username){
          $this->db->where('EMAIL', $username);
          return $this->db->get('usuarios')->row();
     
	}
	
	
	
	//-----------------exportar--------------// 
	public function get()
	{
		$fields = $this->db->field_data('documentos');
		$query = $this->db->select('*')->get('documentos');
		return array("fields" => $fields, "query" => $query);
	}
	
	
	
}