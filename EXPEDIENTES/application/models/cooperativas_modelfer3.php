<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class cooperativas_model extends CI_Model {
	function __construct()
     {
          parent::__construct();
     }
	public function ListarCooperativas(){
		/*$sql="SELECT P.id, P.codigo, P.nombre,P.documento , P.telefono,
		C.descripcion AS DesCategoria,
		S.descripcion AS DesSubCategoria ,
		Pro.nombre_profesor
		FROM socios AS P
		INNER JOIN categorias    AS C ON P.id_categoria    = C.id
		INNER JOIN subcategoria AS S ON P.id_subcategoria = S.id
		INNER JOIN profesores AS Pro ON P.id_profesor = Pro.id 
		order by P.nombre asc"; */
		
		$sql="SELECT C.id, C.estado ,C.matricula, C.cuit, C.razonsocial, C.domicilio, L.nombre_localidad
   				,C.telefono, C.celular, C.tipo , C.resolucion
		FROM cooperativas as C
		INNER JOIN localidad AS L ON C.id_localidad = L.id 
		order by id asc";	
		$query=$this->db->query($sql);
		return $query->result();
	}

	public function BuscarCooperativa($id){
		$sql="SELECT * FROM cooperativas where id='".$id."'";
		$query=$this->db->query($sql);
		return $query->result();
		
	}
	public function UpdateCooperativas($arraySocios, $id){
		$this->db->trans_start();
		$this->db->where('id', $id);
		$this->db->update('cooperativas', $arraySocios);
		$this->db->trans_complete();
		
	}
	/**
	 * Actualiza en cooperativas e incrementa en denuncias 
	 * @param type $id  Indentificador de una cooperativa
	 * @return type
	 */
	public function UpdateCooperativaDenuncia ($id){
		$this->db->trans_start();
		$this->db->where('id', $id);
		$this->db->set('nro_denuncia', 'nro_denuncia+1', FALSE);
		$this->db->update('cooperativas');
		$this->db->trans_complete();
	
	}
	//--------------------------busca el valor maximo para sacar el codigo------//
	Public function MaxCooperativa(){
		$sql="SELECT max(numero) as max FROM cooperativas ";
		$query=$this->db->query($sql);
		return $query->result();
	}
	
	
	public function EliminarCoope($id){
		# code...
		$this->db->where('id',$id);
		return $this->db->delete('cooperativas');
	}
	
	public function Localidades(){
		$sql="SELECT  * FROM localidad ";
		$query=$this->db->query($sql);
		return $query->result();
	}

	public function Coope(){
		$sql="SELECT * FROM cooperativas ";
		$query=$this->db->query($sql);
		return $query->result();
	}
		
	public function ExisteCodigo($codigo){
		$this->db->where("matricula",$codigo);
        $check_exists = $this->db->get("cooperativas");
        if($check_exists->num_rows() == 0){
            return false;
        }else{
            return true;
        }
	}

	public function SaveCooperativas($arrayCooperativas){
		$this->db->trans_start();
     	$this->db->insert('cooperativas', $arrayCooperativas);
     	$this->db->trans_complete();
	}


	public function GuardaImg($arrays){
		$this->db->trans_start();
     	$this->db->insert('img_productos', $arrays);
     	$this->db->trans_complete();
	
	}	
	public function TraeImagenDetalle($id){
		$sql="select * from img_productos where ID_PRODUCTO='".$id."' ORDER BY RAND() limit 1 ";
		$query=$this->db->query($sql);
		return $query->result();
	}
	public function CarruselSocios(){
		$sql="select * from socios ORDER BY RAND() limit 9";
		$query=$this->db->query($sql);
		return $query->result();
	}
	public function TraeImagenes(){
		$sql="select * from img_productos  GROUP BY ID_PRODUCTO ORDER BY RAND() ";
		$query=$this->db->query($sql);
		return $query->result();
	}
	
	//-----------------exportar--------------// 
	public function get()
	{
		$fields = $this->db->field_data('denuncias');
		$query = $this->db->select('*')->get('denuncias');
		return array("fields" => $fields, "query" => $query);
	}

	///Listado de Cooperativas
	public function TraeCoop($order){
		$sql="SELECT *,C.id,C.estado as NEsta ,L.nombre_localidad as NLoca,L.id FROM cooperativas as C
		INNER JOIN localidad AS L ON C.id_localidad = L.id 
		WHERE C.id = '".$order."'";
		$query=$this->db->query($sql);
		return $query->result();
	}
	
	public function TraeSocios($order){
		$sql="SELECT * FROM socios  WHERE  id_cooperativa = '".$order."'";
		$query=$this->db->query($sql);
		return $query->result();
	}
	
	//---------Para realizar Denuncias -------//
	public function TraerCooperativas(){
		$sql="SELECT * FROM cooperativas";
		$query=$this->db->query($sql);
		return $query->result();
	}
	
	
}