<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class expedientes_model extends CI_Model {
	function __construct()
     {
          parent::__construct();
     }

	public function listaPases($id){
        $sql="SELECT pases.id_pase, pases.fecha_pase, pases.estado_pase, pases.obs_pase, areas.nombre_area, expedientes.codigo_exp
FROM pases, expedientes, areas
WHERE pases.codigo_exp = expedientes.codigo_exp
AND pases.id_area = areas.id_area
AND (
expedientes.codigo_exp ='".$id."'
)
LIMIT 0 , 30";
		$query=$this->db->query($sql);
		return $query->result();	
	}

	public function buscarPase($id){
		$sql="SELECT P.id_pase , P.codigo_exp,P.fecha_pase,P.estado_pase,P.obs_pase ,P.id_area , E.asunto_exp
        	  FROM pases as P INNER JOIN expedientes as E ON E.codigo_exp=P.codigo_exp WHERE P.id_pase='".$id."' limit 1 "; 
		$query=$this->db->query($sql); 
		return $query->result();	   
	}

	public function traerDependencias(){
		$sql="SELECT  * FROM dependencias ";
		$query=$this->db->query($sql);
		return $query->result();
	}

	public function traerAreas(){
		$sql="SELECT  * FROM areas ";
		$query=$this->db->query($sql);
		return $query->result();
	}

	public function listarExp(){
     	$sql="SELECT E.id_exp, E.codigo_exp, E.fecha_exp, E.remitente_exp, E.asunto_exp,
		E.obs_exp, E.estado_exp, E.foto_exp, D.nombre_dep, A.nombre_area,E.id_area
     		  FROM expedientes as E
			  INNER JOIN dependencias as D ON D.id_dep=E.id_dep
     	      INNER JOIN areas as A ON A.id_area=E.id_area
			  order by id_exp ASC";
		$query=$this->db->query($sql);
		return $query->result();
     } 

	public function buscarExp($id){
		$sql="SELECT * from expedientes where codigo_exp='".$id."' limit 1"; 
		$query=$this->db->query($sql); //Realiza la consulta a la BD
		return $query->result();	   //Obtiene el resultado de la consulta
	}

	//joako
	public function buscarExpH($id){
		$sql="SELECT H.codigo_exp, H.fecha, U.nombre AS nusuario ,H.mensaje  FROM historial_expediente  AS H 
				INNER JOIN usuarios AS U ON U.id = H.usuario  
				WHERE H.codigo_exp='".$id."' ORDER BY H.fecha DESC LIMIT 50";
		//$sql="SELECT * FROM historial_expediente where codigo_exp='".$id."' LIMIT 50"; 
		$query=$this->db->query($sql); //Realiza la consulta a la BD
		return $query->result();	   //Obtiene el resultado de la consulta
		//return $query->row();
		//return $this->db->get();
	}
	public function getHistorial(){
		return $this->db->get('historial_expediente');
	}

	public function getHistorialbyID($id=""){
		$this->db->query("SELECT * FROM historial_expediente WHERE codigo_exp = '".$id."'LIMIT 1");
		return $result->row();
	}
	/*public function traerId($text){
		//$sql="SELECT codigo_exp FROM expedientes ";
		//$query=$this->db->query($sql);
		//return $query->result();

		$this->db->like('codigo_exp',$text,'both');
		$this->db->from('historial_expediente');
		$query = $this->db->get();
		return $query->result();
	}*/

	public function SaveExp($RegistraExp){
		$this->db->trans_start();
     	$this->db->insert('expedientes', $RegistraExp);
     	$this->db->trans_complete();
	}


	public function updateExp($UpdateExp,$id){
		$this->db->trans_start();
		//$this->db->where('codigo_exp', $id); linea sergio
		$this->db->where('id_exp', $id); //linea joako
		$this->db->update('expedientes', $UpdateExp); 
		$this->db->trans_complete();
	}
	
	public function eliminarExp($id){
		$this->db->where('codigo_exp',$id);
		return $this->db->delete('expedientes');
	}
	
	//-----------------exportar--------------// 
	public function get(){
		$fields = $this->db->field_data('expedientes');
		$query = $this->db->select('*')->get('expedientes');
		return array("fields" => $fields, "query" => $query);
	}
	
	//--------------------------busca el valor maximo para sacar el codigo------// 
	public function MaxExp(){
		$sql="SELECT max(codigo_exp) as max FROM expedientes ";
		$query=$this->db->query($sql);
		return $query->result();	
	}



	public function MaxExpid(){
		$sql="SELECT max(id_exp) as max FROM expedientes ";
		$query=$this->db->query($sql);
		return $query->result();	
	}

	public function traerId($text){
		/*$sql="SELECT codigo_exp FROM expedientes ";
		$query=$this->db->query($sql);
		return $query->result();*/

		$this->db->like('codigo_exp',$text,'both');
		$this->db->from('expedientes');
		$query = $this->db->get();
		//return $r->result();

		// $sql = "SELECT count(*) as cuenta FROM deta_call_gestion";      
      //$query = $this->db->query($sql);
      if($query->num_rows()>=1){
            //$fila=$query->row();
            //return $fila->cuenta;
      		return false;
        }else{
            return true;
        }
	}
}