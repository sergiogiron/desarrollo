<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class instituto_model extends CI_Model {
	function __construct()
     {
          parent::__construct();
     }
    
	public function listarAdjudicaciones(){
		$sql="SELECT * from instituto order by fecha asc";
		$query=$this->db->query($sql);
		return $query->result();
	}
	public function buscarNota($res){
		//El nro. de nota es el campo Resolucion en la tabla adj_moviles
		$sql="SELECT * from adj_moviles where Resolucion='".$res."' limit 1";
		$query=$this->db->query($sql);
		return $query->result();
	}
	/*public function TraerBeneficiario($id)
	{
		$sql="SELECT        adj_moviles.Resolucion, beneficios.id, beneficios.nombre_beneficios, beneficios.documento, localidades.nombre_localidad, programas.nombre_programa, beneficios.barrio, beneficios.mza, 
                         beneficios.lote ,adj_moviles.fecha_impresion
FROM            programas, localidades, adj_moviles, beneficios
WHERE        programas.id = adj_moviles.id_programa AND localidades.id = adj_moviles.id_localidad AND adj_moviles.id_beneficio = beneficios.id AND (adj_moviles.Resolucion ='".$id."')";
		$query=$this->db->query($sql);
		return $query->result();
		
	}
	/**
      * implementacion de adjudicacion controller buscarCooperativa
      * @return type query  
      */
     /*public function buscarCooperativa($filtro,$filtro2,$bandera){
		 
		 
		 
		 if ($bandera==0){
		$sql="SELECT concat(MATRICULA ,' - ', RAZONSOCIAL) AS label, RAZONSOCIAL, MATRICULA,ID,id_localidad,estado FROM cooperativas   WHERE (MATRICULA LIKE  '%".$filtro."%' or RAZONSOCIAL LIKE '%".$filtro."%') AND(Estado=3) AND id_localidad='".$filtro2."' LIMIT 0 , 10 ";
		$query=$this->db->query($sql);
		 return $query->result();}
		 else
		 {$sql="SELECT concat(MATRICULA ,' - ', RAZONSOCIAL) AS label, RAZONSOCIAL, MATRICULA,ID,id_localidad,estado FROM cooperativas   WHERE (MATRICULA LIKE  '%".$filtro."%' or RAZONSOCIAL LIKE '%".$filtro."%') AND(estado=3) LIMIT 0 , 10 ";
		$query=$this->db->query($sql);
		 return $query->result();}
		
	}*/
	public function saveCarga($RegistraCarga){
		$this->db->trans_start();
     	$this->db->insert('instituto', $RegistraCarga);
     	$this->db->trans_complete();
	}
	public function updateCarga($UpdateCarga,$id){
		$this->db->trans_start();
		$this->db->where('id', $id);
		$this->db->update('instituto', $UpdateCarga); 
		$this->db->trans_complete();
	}
	/**
	 * Elimina una localidad especificada por id
	 * @param type $id identificador de una localidad
	 * @return type
	 */
	/*public function eliminarLocalidad($id){
		$this->db->where('id',$id);
		return $this->db->delete('localidad');
	}
	*/
	
	
	
}