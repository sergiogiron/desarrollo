<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class ventas_model extends CI_Model {
	function __construct()
     {
          parent::__construct();
     }
	 //COOPERATIV
     public function buscarcliente($filtro,$filtro2){
		$sql="SELECT concat(MATRICULA ,' - ', RAZONSOCIAL) AS label, RAZONSOCIAL, MATRICULA,ID,id_localidad FROM cooperativas   WHERE (MATRICULA LIKE  '%".$filtro."%' or RAZONSOCIAL LIKE '%".$filtro."%') AND id_localidad='".$filtro2."' ";
		$query=$this->db->query($sql);
		return $query->result();
		
	}
	//BENEFICIARIO
	public function listarproducto($filtro,$filtro2){
		$sql="SELECT concat(documento,' - ', nombre_beneficios, ' - ', localidad) AS label, id , nombre_beneficios,localidad  FROM beneficios WHERE (nombre_beneficios LIKE  '%".$filtro."%' or ID LIKE '%".$filtro."%') AND localidad='".$filtro2."' ";
		$query=$this->db->query($sql);
		return $query->result();
	}
	public function Localidades(){
		$sql="SELECT  * FROM localidad ";
		$query=$this->db->query($sql);
	return $query->result();}
	
	public function Programas(){
		$sql="SELECT  * FROM programas ";
		$query=$this->db->query($sql);
	return $query->result();}
	
	
	public function UpdateExistenciasProducto($codigo,$cantidad){
		$sql="update tarifas set cantidad= cantidad - '".$cantidad."' where codigo='".$codigo."'";
		$query=$this->db->query($sql);
		return True;
	}
	//TIKET
	public function TraeVenta($order){
		$sql="SELECT * FROM partidas   WHERE ID_LINK = '".$order."'";
		$query=$this->db->query($sql);
		return $query->result();
	}
	public function TraeDoc($order){
	//	$sql="SELECT D.id, S.nombre as NombreS FROM documentos as D INNER JOIN socios AS S ON D.cliente = S.id   WHERE D.ID = '".$order."'";
	    $sql="SELECT D.id, S.nombre as NombreS, S.id_categoria, C.descripcion as Mcategoria ,  S.id_subcategoria, SU.descripcion as Msubcategoria  FROM documentos as D INNER JOIN socios AS S ON D.cliente = S.id  INNER JOIN categorias AS C ON  C.id = S.id_categoria INNER JOIN subcategoria AS SU ON  SU.id = S.id_subcategoria  WHERE D.ID = '".$order."'";

	$query=$this->db->query($sql);
		return $query->result();
	}

	////cta cte
	
	public function TraeVenta_1($order){
		$sql="SELECT * FROM partidas   WHERE cliente = '".$order."'";
		$query=$this->db->query($sql);
		return $query->result();
	}
	public function TraeDoc_1($order){
		$sql="SELECT D.id, S.nombre as NombreS FROM documentos as D INNER JOIN socios AS S ON D.cliente = S.id   WHERE D.cliente = '".$order."'";
		$query=$this->db->query($sql);
		return $query->result();
	}
	
}
	
