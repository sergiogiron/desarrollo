<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class socios_model extends CI_Model {
	function __construct()
     {
          parent::__construct();
     }
	public function ListarSocios(){
		$sql="SELECT *,S.id, S.telefono,C.razonsocial as razon from socios as S 
        INNER JOIN cooperativas  AS C ON S.id_cooperativa = C.id
		order by nombre asc";
		$query=$this->db->query($sql);
		return $query->result();
	}
	public function BuscaSocios($id){
		$sql="SELECT * from socios where id='".$id."' limit 1";
		$query=$this->db->query($sql);
		return $query->result();
	}
	public function SaveSocios($RegistraSocio){
		$this->db->trans_start();
     	$this->db->insert('socios', $RegistraSocio);
     	$this->db->trans_complete();
	}
	public function UpdateSocios($UpdateSocio,$id){
		$this->db->trans_start();
		$this->db->where('id', $id);
		$this->db->update('socios', $UpdateSocio); 
		$this->db->trans_complete();
	}
	public function EliminarSocio($id)
	{
		# code...
		$this->db->where('id',$id);
		return $this->db->delete('socios');
	}
	
}