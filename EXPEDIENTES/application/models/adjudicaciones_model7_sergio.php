<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class adjudicaciones_model extends CI_Model {
	function __construct()
     {
          parent::__construct();
     }
	 
	  /**
      * implementacion de adjudicacion controller buscarCooperativa
      * @return type query  
      */
     public function buscarCooperativa($filtro,$filtro2,$bandera){
		 
		 
		 
		 if ($bandera==0){
		$sql="SELECT concat(MATRICULA ,' - ', RAZONSOCIAL) AS label, RAZONSOCIAL, MATRICULA,ID,id_localidad FROM cooperativas   WHERE (MATRICULA LIKE  '%".$filtro."%' or RAZONSOCIAL LIKE '%".$filtro."%') AND id_localidad='".$filtro2."' ";
		$query=$this->db->query($sql);
		 return $query->result();}
		 else
		 {$sql="SELECT concat(MATRICULA ,' - ', RAZONSOCIAL) AS label, RAZONSOCIAL, MATRICULA,ID,id_localidad FROM cooperativas   WHERE (MATRICULA LIKE  '%".$filtro."%' or RAZONSOCIAL LIKE '%".$filtro."%')LIMIT 0 , 10 ";
		$query=$this->db->query($sql);
		 return $query->result();}
		
	}
	 /**
      * implementacion de adjudicacion controller listarBeneficiarios
      * @return type query  
      */
	public function listarBeneficiarios($filtro,$filtro2,$bandera)
	{
		$sql="SELECT        CONCAT(beneficios.documento, ' - ', beneficios.nombre_beneficios, ' - ', localidades.nombre_localidad) AS label, beneficios.id, beneficios.documento, beneficios.nombre_beneficios, beneficios.barrio, 
                         beneficios.mza, beneficios.lote, beneficios.obs, beneficios.estado, localidades.nombre_localidad AS localidad, beneficios.id_localidad
FROM            beneficios, localidades
WHERE        beneficios.id_localidad = localidades.id AND (nombre_beneficios LIKE  '%".$filtro."%' or documento LIKE '%".$filtro."%') AND (beneficios.estado=2) LIMIT 0 , 10";
		$query=$this->db->query($sql);
		return $query->result();}
	
	 /**
      * implementacion de adjudicacion controller localidades
      * @return type query  
      */
	public function Localidades(){
		$sql="SELECT  * FROM localidades ";
		$query=$this->db->query($sql);
	return $query->result();}
	 /**
      * implementacion de adjudicacion controller programas
      * @return type query  
      */
	public function Programas(){
		$sql="SELECT  * FROM programas ";
		$query=$this->db->query($sql);
	return $query->result();}
	 
public function TraerDatos($localidad,$cantidad){

//Crear conexión
//mysqli_connect (host, nombre de usuario, contraseña, dbname);
$dir="localhost";
$user="p1000543";
$pass="19tozuBUno";
$bd="p1000543_uceppi";
// Consultar la base de datos
$consulta_mysql ="SELECT beneficios.id, beneficios.nombre_beneficios, beneficios.documento, beneficios.id_localidad, beneficios.barrio, beneficios.mza, beneficios.lote, beneficios.obs, beneficios.estado, localidades.nombre_localidad
FROM            beneficios, localidades
WHERE beneficios.id_localidad = localidades.id AND (beneficios.id_localidad = ".$localidad.") AND (beneficios.estado = 2)
ORDER BY RAND() LIMIT 0,".$cantidad."";
$con = mysqli_connect($dir,$user,$pass,$bd);
if (mysqli_connect_errno($con))
   {
   echo "No se pudo conectar a MySQL: " . mysqli_connect_error ();
   }
   $resultado=mysqli_query($con,$consulta_mysql);
		$arreglo = array();
			while ($fila = mysqli_fetch_array($resultado)) {
				$arreglo[]=$fila;
			}
			mysqli_close($con);
			return $arreglo;

}
	
	 
	public function ListarAdjudicaciones(){

	
$sql="SELECT        MIN(DISTINCT cooperativas.razonsocial) AS Cooperativa, adj_moviles.id_coperativa, cooperativas.matricula, cooperativas.numero, cooperativas.resolucion AS Resolucion
FROM            adj_moviles, cooperativas, adjudicaciones
WHERE        adj_moviles.id_coperativa = cooperativas.id
GROUP BY adj_moviles.id_coperativa, cooperativas.matricula, cooperativas.numero, cooperativas.resolucion";
	$query=$this->db->query($sql);
		return $query->result();
	}
	
	public function EliminarRecibo($arrayRecibos,$id)
	{
		# code...
		$this->db->where('id',$id);
		return $this->db->delete('documentos'); 
	
	}
	
	public function Adjudicacion($id_adj_movil,$cantidad){
		$fecha = date('Y-m-d H:i:s');
		$sql="insert into adjudicaciones(id_adj_moviles,cantidad,fecha) values 
                       (".$id_adj_movil." ,".$cantidad.",'".$fecha."')";
	$query=$this->db->query($sql);
	}
	
	public function TraerCodigo(){
		$sql="SELECT MAX(id_adj_moviles) AS id FROM adj_moviles";
		$query=$this->db->query($sql);
		return $query->result();
	}
	
	public function TraerCupo($id){
		$sql="SELECT restantes FROM programas where id='".$id."'";
		$query=$this->db->query($sql);
		return $query->result();
	}
	
	public function GuardarDatos($localidad,$programa,$cooperativa,$recibidos,$id_adj_movil,$beneficiario){
	$sql="insert into adj_moviles(id_localidad,id_coperativa,id_programa,id_beneficio,id_adj_moviles) values 
                       (".$localidad." ,".$cooperativa.",".$programa.",".$beneficiario.",".$id_adj_movil.")";
	$query=$this->db->query($sql);
	$sql="Update beneficios Set estado=3 Where id='".$beneficiario."'";
    $query=$this->db->query($sql);
	
	}
	public function ActualizaCupo($cupo,$programa){
		$sql="Update programas Set restantes=restantes-".$cupo."  Where id='".$programa."'";
    $query=$this->db->query($sql);
	}
	
	public function Verificar($username){
          $this->db->where('EMAIL', $username);
          return $this->db->get('usuarios')->row();
     
	}
	
	
	
	//-----------------exportar--------------// 
	public function get()
	{
		$fields = $this->db->field_data('documentos');
		$query = $this->db->select('*')->get('documentos');
		return array("fields" => $fields, "query" => $query);
	}
	
	
	
}