<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class expedientes_model extends CI_Model {
	function __construct()
     {
          parent::__construct();
     }

    public function listaPases(){
        $sql="SELECT * from pases order by codigo_pase asc";
		$query=$this->db->query($sql);
		return $query->result();	
		}

	public function buscarPase($id){
		$sql="SELECT * from pases where id_pase='".$id."' limit 1"; 
		$query=$this->db->query($sql); 
		return $query->result();	   
	}

	public function traerDependencias(){
		$sql="SELECT  * FROM dependencias ";
		$query=$this->db->query($sql);
		return $query->result();
	}

	public function traerAreas(){
		$sql="SELECT  * FROM areas ";
		$query=$this->db->query($sql);
		return $query->result();
	}

	public function listarExp(){
     	$sql="SELECT * from expedientes order by codigo_exp asc";
		$query=$this->db->query($sql);
		return $query->result();
     }

	public function buscarExp($id){
		$sql="SELECT * from expedientes where id_exp='".$id."' limit 1"; 
		$query=$this->db->query($sql); //Realiza la consulta a la BD
		return $query->result();	   //Obtiene el resultado de la consulta
	}

	public function SaveExp($RegistraExp){
		$this->db->trans_start();
     	$this->db->insert('expedientes', $RegistraExp);
     	$this->db->trans_complete();
	}

	public function updateExp($UpdateExp,$id){
		$this->db->trans_start();
		$this->db->where('id_exp', $id);
		$this->db->update('expedientes', $UpdateExp); 
		$this->db->trans_complete();
	}
	
	public function eliminarExp($id){
		$this->db->where('id_exp',$id);
		return $this->db->delete('expedientes');
	}
	
	//-----------------exportar--------------// 
	public function get(){
		$fields = $this->db->field_data('expedientes');
		$query = $this->db->select('*')->get('expedientes');
		return array("fields" => $fields, "query" => $query);
	}
	
	//--------------------------busca el valor maximo para sacar el codigo------//
	public function MaxExp(){
		$sql="SELECT max(codigo_exp) as max FROM expedientes ";
		$query=$this->db->query($sql);
		return $query->result();	
	}
	
	
}